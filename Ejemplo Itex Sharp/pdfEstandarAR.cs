﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//Referenciar y usar.
using System.Data;
using System.IO;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Windows.Forms;
using System.Web;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Drawing;
using System.Net;

namespace RulesServicesDIAN2.Adquiriente
{
    public class pdfEstandarAR
    {
        #region "Constructor"

        public pdfEstandarAR()
        {
            NomArchivo = string.Empty;
            strError = string.Empty;
        }

        #endregion

        #region "Atributos"

        private string NomArchivo;
        private string strError;

        #endregion

        #region "Propiedades"

        public string NomArchivoPDF
        {
            get { return NomArchivo; }
        }

        public string Error
        {
            get { return strError; }
        }

        #endregion

        #region "Metodos Publicos"

        public bool EstandarFacturaAR(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType)
        {
            NomArchivo = string.Empty;            
            string Ruta = HttpContext.Current.Server.MapPath("~/PDF/" + NIT+"/");

            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            string RutaImg ="http://"+LocalIP+ ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";
            
            NomArchivo =NombreInvoice + ".pdf";
            
            try
            {                                                
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);
                
                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.
                Document document = new Document(iTextSharp.text.PageSize.LETTER);                          
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;

                // step 3: we open the document
                document.Open();

                PdfDiv divLogo = new PdfDiv();
                divLogo.Float = iTextSharp.text.pdf.PdfDiv.FloatType.LEFT;
                divLogo.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divLogo.BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID;
                divLogo.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                divLogo.Height = 100;
                divLogo.Width = 90;
                //RutaImg = HttpContext.Current.Server.MapPath("~/IMG/" + NIT);
                //divLogo.BackgroundImage = iTextSharp.text.Image.GetInstance(RutaImg+"/Logo.png");
                divLogo.BackgroundImage = iTextSharp.text.Image.GetInstance(RutaImg);

                PdfDiv divTitle = new PdfDiv();
                divTitle.Float = iTextSharp.text.pdf.PdfDiv.FloatType.LEFT;
                divTitle.Position = iTextSharp.text.pdf.PdfDiv.PositionType.STATIC;
                divTitle.BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID;
                divTitle.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                divTitle.Height = 100;
                divTitle.Width = 360;
                divTitle.BackgroundColor = BaseColor.WHITE;
                iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 10, iTextSharp.text.Font.NORMAL);
                Paragraph prgTitle = new Paragraph("FACTURA ELECTRONICA " + DsInvoiceAR.Tables["Company"].Rows[0]["Name"] + "\nREALIZADA POR " + DsInvoiceAR.Tables["Company"].Rows[0]["Name"] + " NIT: " + DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"], fontTitle);
                prgTitle.Alignment = Element.ALIGN_CENTER;
                divTitle.AddElement(prgTitle);

                PdfDiv divLegalNum = new PdfDiv();
                divLegalNum.Float = iTextSharp.text.pdf.PdfDiv.FloatType.LEFT;
                divLegalNum.Position = iTextSharp.text.pdf.PdfDiv.PositionType.STATIC;
                divLegalNum.BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID;
                divLegalNum.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                divLegalNum.Height = 100;
                divLegalNum.Width = 90;
                divLegalNum.BackgroundColor = BaseColor.WHITE;                
                iTextSharp.text.Font fontTitleLegalNum = FontFactory.GetFont(FontFactory.TIMES_BOLD, 9, iTextSharp.text.Font.BOLD);
                iTextSharp.text.Font fontLegalNum = FontFactory.GetFont(FontFactory.TIMES_BOLD, 9, iTextSharp.text.Font.NORMAL);
                string TipoFactura = string.Empty;
                if (InvoiceType == "InvoiceType")
                    TipoFactura = "FACTURA VENTA";
                else if(InvoiceType == "CreditNoteType")
                    TipoFactura = "NOTA CREDITO";
                else
                    TipoFactura = "NOTA DEBITO";

                Paragraph prgTitleLegalNum = new Paragraph(TipoFactura + ":\n", fontTitleLegalNum);
                prgTitleLegalNum.Alignment = Element.ALIGN_LEFT;
                Paragraph prgLegalNum = new Paragraph(DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"].ToString(), fontLegalNum);
                prgLegalNum.Alignment = Element.ALIGN_LEFT;
                divLegalNum.AddElement(prgTitleLegalNum);
                divLegalNum.AddElement(prgLegalNum);

                iTextSharp.text.Font fontAdquirienteTitle = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 9, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontAdquiriente = FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL);
                PdfDiv divAdquiruente = new PdfDiv();
                divAdquiruente.Float = iTextSharp.text.pdf.PdfDiv.FloatType.LEFT;
                divAdquiruente.Position = iTextSharp.text.pdf.PdfDiv.PositionType.STATIC;
                divAdquiruente.BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID;
                divAdquiruente.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                divAdquiruente.Height = 100;
                divAdquiruente.Width = 200;
                divAdquiruente.BackgroundColor = BaseColor.WHITE;
                string strInfoAdquiriente = DsInvoiceAR.Tables["Customer"].Rows[0]["Name"].ToString() + "\n"+
                                            "" + DsInvoiceAR.Tables["Customer"].Rows[0]["Address1"].ToString() + "\n" +
                                            "" + DsInvoiceAR.Tables["Customer"].Rows[0]["City"].ToString() + "\n" +
                                            "" + DsInvoiceAR.Tables["Customer"].Rows[0]["State"].ToString() + "\n" +
                                            "NIT " + DsInvoiceAR.Tables["Customer"].Rows[0]["ResaleID"].ToString();
                Paragraph prgAdquirienteTitle = new Paragraph("SEÑORES:", fontAdquirienteTitle);
                Paragraph prgAdquiriente = new Paragraph(strInfoAdquiriente, fontAdquiriente);
                divAdquiruente.AddElement(prgAdquirienteTitle);
                divAdquiruente.AddElement(prgAdquiriente);

                iTextSharp.text.Font fontPedidoTitle = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 9, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontPedido = FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL);
                PdfDiv divInfo = new PdfDiv();
                divInfo.Float = iTextSharp.text.pdf.PdfDiv.FloatType.LEFT;
                divInfo.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divInfo.BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID;
                divInfo.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                divInfo.Height = 100;
                divInfo.Width = 200;
                divInfo.BackgroundColor = BaseColor.WHITE;
                string strInfoPedido = "Nro Pedido: " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["OrderNum"].ToString() + "\n";
                if (InvoiceType == "InvoiceType")
                   strInfoPedido +="Nro.FAC. REF: " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceNum"].ToString() + "\n";
                else if(InvoiceType == "CreditNoteType")
                    strInfoPedido += "Nro.FAC. REF: " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceRef"].ToString() + "\n";
                strInfoPedido += "Fecha Factura: " + Convert.ToDateTime(DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).ToString("yyyy/MM/dd HH:mm:ss") + "\n" +
                                 "Fecha Vencimiento: " + Convert.ToDateTime(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"]).ToString("yyyy/MM/dd HH:mm:ss");
                Paragraph prgPedidoTitle = new Paragraph("INFORMACIÓN FACTURA:", fontPedidoTitle);
                prgPedidoTitle.Alignment = Element.ALIGN_LEFT;
                Paragraph prgPedido = new Paragraph(strInfoPedido, fontPedido);
                prgPedido.Alignment = Element.ALIGN_LEFT;
                divInfo.AddElement(prgPedidoTitle);
                divInfo.AddElement(prgPedido);

                PdfDiv divQR = new PdfDiv();
                divQR.Float = iTextSharp.text.pdf.PdfDiv.FloatType.RIGHT;
                divQR.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divQR.BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID;
                divQR.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                divQR.Height = 100;
                divQR.Width = 140;
                divQR.BackgroundColor = BaseColor.WHITE;
                iTextSharp.text.Image ImgQR = iTextSharp.text.Image.GetInstance(QRInvoice, BaseColor.WHITE);
                ImgQR.Alignment = Element.ALIGN_RIGHT;
                divQR.BackgroundImage = ImgQR;

                //Tabla - Items.
                iTextSharp.text.pdf.PdfPTable tableItems = new iTextSharp.text.pdf.PdfPTable(7);
                tableItems.HorizontalAlignment = Element.ALIGN_CENTER;

                //Dimenciones.
                float[] Dimencion = new float[7];
                Dimencion[0] = 3.0F;//InvoiceLine
                Dimencion[1] = 5.0F;//Codigo
                Dimencion[2] = 5.0F;//Descripcion
                Dimencion[3] = 4.0F;//Cantidad
                Dimencion[4] = 4.0F;//IVA
                Dimencion[5] = 4.0F;//Descuento
                Dimencion[6] = 5.0F;//Total Linea

                //Configuracion y creacion de la tabla Comparativa.
                tableItems.TotalWidth = 540;//writer.DirectContent.PdfDocument.PageSize.Width;
                tableItems.SetWidths(Dimencion);
                tableItems.SpacingBefore = 1.0F;
                tableItems.SpacingAfter = 1.0F;

                //Configuramos las columnas.
                if (!AddColumnsInvoiceLine(ref tableItems))                                    
                    return false;                
               
                //Filas 
                iTextSharp.text.Font fuenteDatos = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 8, iTextSharp.text.Font.NORMAL);
                //foreach (DataRow oFila in DtItems.Rows)                
                foreach (DataRow InvoiceLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
                {
                    if (!AddRowInvoice(InvoiceLine, ref tableItems, fuenteDatos,DsInvoiceAR.Tables["InvcTax"]))                    
                        return false;                    
                }

                tableItems.HeadersInEvent = true;
                tableItems.CompleteRow();

                iTextSharp.text.pdf.PdfPTable tableTotales = new iTextSharp.text.pdf.PdfPTable(3);
                tableTotales.HorizontalAlignment = Element.ALIGN_CENTER;

                //Dimenciones.
                float[] DimencionTotales = new float[3];
                DimencionTotales[0] = 6.0F;
                DimencionTotales[1] = 3.0F;
                DimencionTotales[2] = 6.0F;
                
                //Configuracion y creacion de la tabla Comparativa.
                tableTotales.TotalWidth = 208;//writer.DirectContent.PdfDocument.PageSize.Width;
                tableTotales.SetWidths(DimencionTotales);
                tableTotales.SpacingBefore = 1.0F;
                tableTotales.SpacingAfter = 1.0F;

                //Totales.
                if (!AddTotalesInvoice(ref tableTotales, DsInvoiceAR))
                    return false;

                iTextSharp.text.Font fontObsTitle = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 9, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontObs = FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL);
                PdfDiv divObs = new PdfDiv();
                divObs.Float = iTextSharp.text.pdf.PdfDiv.FloatType.LEFT;
                divObs.Position = iTextSharp.text.pdf.PdfDiv.PositionType.STATIC;
                divObs.BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID;
                divObs.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                divObs.Height = 100;
                divObs.Width = 300;
                divObs.BackgroundColor = BaseColor.WHITE;
                string ObsFactura = DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceComment"].ToString();                                 

                Paragraph prgObsTitle = new Paragraph("OBSERVACIONES:", fontObsTitle);
                prgObsTitle.Alignment = Element.ALIGN_LEFT;
                Paragraph prgObs = new Paragraph(ObsFactura, fontObs);
                prgObs.Alignment = Element.ALIGN_LEFT;
                divObs.AddElement(prgObsTitle);
                divObs.AddElement(prgObs);

                iTextSharp.text.Font fontTotales = FontFactory.GetFont(FontFactory.HELVETICA, 9, iTextSharp.text.Font.NORMAL);
                PdfDiv divTotales = new PdfDiv();
                divTotales.Float = iTextSharp.text.pdf.PdfDiv.FloatType.LEFT;
                divTotales.Position = iTextSharp.text.pdf.PdfDiv.PositionType.STATIC;
                divTotales.BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID;
                divTotales.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                divTotales.Height = 150;
                divTotales.Width = 208;
                divTotales.BackgroundColor = BaseColor.WHITE;
                divTotales.AddElement(tableTotales);             

                document.Add(divLogo);
                document.Add(divTitle);
                document.Add(divLegalNum);
                document.Add(divAdquiruente);
                document.Add(divInfo);
                document.Add(divQR);
                document.Add(separator);
                document.Add(tableItems);
                document.Add(separator);
                document.Add(divObs);
                document.Add(divTotales);

                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();                           
                document.Close();
                //RutaPdf = Ruta + NomArchivo;
                RutaPdf =NomArchivo;
                return true;
            }
            catch (Exception ex)
            {
                strError +="1. Error al crear PDF: " + ex.Message;
                return false;
            }            
        }

        #region "Formatos FacturaAR RIMAX"

        public bool FacturaNacionalRimax(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType)
        {
            NomArchivo = string.Empty;
            string Ruta = HttpContext.Current.Server.MapPath("~/PDF/" + NIT + "/");
            //string RutaImg = ""; //RulesServicesDIAN2.Properties.Settings.Default.UrlFE + "/images/EMPRESAS/Empresa_/" + NIT + "/Logo_" + NIT + ".png";                                                     
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            string RutaImg = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";
            NomArchivo = NombreInvoice + ".pdf";

            try
            {
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.
                Document document = new Document(iTextSharp.text.PageSize.LETTER);
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;

                // step 3: we open the document
                document.Open();

                PdfDiv divLogo = new PdfDiv();
                divLogo.Float = iTextSharp.text.pdf.PdfDiv.FloatType.LEFT;
                divLogo.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divLogo.BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID;
                divLogo.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                divLogo.Height = 30;
                divLogo.Width = 120;                
                divLogo.BackgroundImage = iTextSharp.text.Image.GetInstance(RutaImg);

                PdfDiv divTitle = new PdfDiv();
                divTitle.Float = iTextSharp.text.pdf.PdfDiv.FloatType.LEFT;
                divTitle.Position = iTextSharp.text.pdf.PdfDiv.PositionType.STATIC;
                divTitle.BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID;
                divTitle.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                divTitle.Height = 100;
                divTitle.Width = 170;//200;
                divTitle.BackgroundColor = BaseColor.WHITE;
                iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 10, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontSubTitle = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                Paragraph prgTitle = new Paragraph(DsInvoiceAR.Tables["Company"].Rows[0]["Name"].ToString(),fontTitle);
                prgTitle.Alignment = Element.ALIGN_CENTER;
                Paragraph prgSubTitle = new Paragraph("NIT: " + DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"].ToString() + "\nwww.rimax.com.co", fontSubTitle);
                prgSubTitle.Alignment = Element.ALIGN_CENTER;

                //Direccion Rimax:
                iTextSharp.text.Font fontAddres = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);
                string Address = "\n" + DsInvoiceAR.Tables["Company"].Rows[0]["Address1"].ToString() + " Yumbo - Colombia.\n" +                                 
                                 "PBX:" + DsInvoiceAR.Tables["Company"].Rows[0]["PhoneNum"].ToString() + "\n" +
                                 //"FAX:" + DsInvoiceAR.Tables["Company"].Rows[0]["FaxNum"].ToString() + "\n" +
                                 "ventas.serviciocliente@rimax.com.co";
                Paragraph prgAddres = new Paragraph(Address, fontAddres);
                prgAddres.Alignment = Element.ALIGN_CENTER;
                
                divTitle.AddElement(prgTitle);
                divTitle.AddElement(prgSubTitle);
                divTitle.AddElement(prgAddres);

                PdfDiv divLegalNum = new PdfDiv();
                divLegalNum.Float = iTextSharp.text.pdf.PdfDiv.FloatType.LEFT;
                divLegalNum.Position = iTextSharp.text.pdf.PdfDiv.PositionType.STATIC;
                divLegalNum.BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID;
                divLegalNum.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                divLegalNum.Height = 100;
                divLegalNum.Width = 250; //90;
                divLegalNum.BackgroundColor = BaseColor.WHITE;
                iTextSharp.text.Font fontTitleLegalNum = FontFactory.GetFont(FontFactory.TIMES_BOLD, 9, iTextSharp.text.Font.BOLD);
                iTextSharp.text.Font fontLegalNum = FontFactory.GetFont(FontFactory.TIMES_BOLD, 9, iTextSharp.text.Font.NORMAL);
                string TipoFactura = string.Empty;
                if (InvoiceType == "InvoiceType")
                    TipoFactura = "FACTURA VENTA";
                else if (InvoiceType == "CreditNoteType")
                    TipoFactura = "NOTA CREDITO";
                else
                    TipoFactura = "NOTA DEBITO";

                Paragraph prgTitleLegalNum = new Paragraph(TipoFactura + ":", fontTitleLegalNum);
                prgTitleLegalNum.Alignment = Element.ALIGN_LEFT;
                Paragraph prgLegalNum = new Paragraph(DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"].ToString(), fontLegalNum);
                prgLegalNum.Alignment = Element.ALIGN_LEFT;                

                //Responsabilidad Legal
                iTextSharp.text.Font fontResponsable = FontFactory.GetFont(FontFactory.HELVETICA, 5, iTextSharp.text.Font.BOLD);
                string Responseble = "\n\nPRODUCTOR RESPONSABLE DEL I.V.A. AUTORETENEDORES SEGÚN RESOLUCIÓN No 1360 DE NOV. 1 DE 1989."+
                                     " GRANDES CONTRIBUYENTES. RESOLUCIÓN No. 2030 DE DIC. 20 DE 1990. NO RETENER I.C.A. "+
                                     " INGRESO CAUSADO EN YUMBO. "+
                                     " No. DE RESOLUCIÓN 50000415997 DE 2016/05/10 TIPO 02 DESDE 516001 HASTA 564000.";

                Paragraph prgResponsable = new Paragraph(Responseble, fontResponsable);
                prgResponsable.Alignment = Element.ALIGN_JUSTIFIED;
                
                divLegalNum.AddElement(prgTitleLegalNum);
                divLegalNum.AddElement(prgLegalNum);
                divLegalNum.AddElement(prgResponsable);

                //Tabla - Headers.
                iTextSharp.text.pdf.PdfPTable tableHead = new iTextSharp.text.pdf.PdfPTable(3);
                tableHead.HorizontalAlignment = Element.ALIGN_JUSTIFIED;

                //Dimenciones.
                float[] DimencionHead = new float[3];
                DimencionHead[0] = 5.0F;//Vendido A
                DimencionHead[1] = 5.0F;//Informacion Factura 
                DimencionHead[2] = 5.0F;//QR               

                //Configuracion y creacion de la tabla Comparativa.
                tableHead.WidthPercentage = 100;//writer.DirectContent.PdfDocument.PageSize.Width;//540
                tableHead.SetWidths(DimencionHead);
                tableHead.SpacingBefore = 1.0F;
                tableHead.SpacingAfter = 1.0F;

                //Columnas
                iTextSharp.text.Font fontAdquirienteTitle = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 9, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.pdf.PdfPCell celVendidoA = new iTextSharp.text.pdf.PdfPCell(new Phrase("VENDIDO A:", fontAdquirienteTitle));
                celVendidoA.Colspan = 1;
                celVendidoA.Padding = 3;
                celVendidoA.BackgroundColor = iTextSharp.text.pdf.ExtendedColor.WHITE;
                celVendidoA.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                celVendidoA.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableHead.AddCell(celVendidoA);

                iTextSharp.text.Font fontPedidoTitle = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 9, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.pdf.PdfPCell celPedido = new iTextSharp.text.pdf.PdfPCell(new Phrase("INFORMACIÓN FACTURA:", fontPedidoTitle));
                celPedido.Colspan = 1;
                celPedido.Padding = 3;
                celPedido.BackgroundColor = iTextSharp.text.pdf.ExtendedColor.WHITE;
                celPedido.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                celPedido.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableHead.AddCell(celPedido);

                iTextSharp.text.Font fontQR = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 9, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.pdf.PdfPCell celQR = new iTextSharp.text.pdf.PdfPCell(new Phrase("Código QR:", fontQR));
                celQR.Colspan = 1;
                celQR.Padding = 3;
                celQR.BackgroundColor = iTextSharp.text.pdf.ExtendedColor.WHITE;
                celQR.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celQR.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableHead.AddCell(celQR);
                
                //VENDIDO A                
                iTextSharp.text.Font fontAdquiriente = FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL);                               
                string strInfoAdquiriente = DsInvoiceAR.Tables["Customer"].Rows[0]["Name"].ToString() + "\n" +
                                            "" + DsInvoiceAR.Tables["Customer"].Rows[0]["Address1"].ToString() + "\n" +
                                            "" + DsInvoiceAR.Tables["Customer"].Rows[0]["City"].ToString() + "\n" +
                                            "" + DsInvoiceAR.Tables["Customer"].Rows[0]["State"].ToString() + "\n" +
                                            "NIT " + DsInvoiceAR.Tables["Customer"].Rows[0]["ResaleID"].ToString();                         

                iTextSharp.text.pdf.PdfPCell celVendidoAText = new iTextSharp.text.pdf.PdfPCell(new Phrase(strInfoAdquiriente, fontAdquiriente));
                celVendidoAText.Colspan = 1;
                celVendidoAText.Padding = 3;
                celVendidoAText.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                celVendidoAText.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableHead.AddCell(celVendidoAText);

                //FACTURADO POR                
                iTextSharp.text.Font fontPedido = FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL);                
                string strInfoPedido = "Ciudad:" + DsInvoiceAR.Tables["Company"].Rows[0]["City"].ToString() + "\n" +
                                       "Nro Pedido: " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["OrderNum"].ToString() + "\n";
                if (InvoiceType == "InvoiceType")
                    strInfoPedido += "Nro.FAC. REF: " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceNum"].ToString() + "\n";
                else if (InvoiceType == "CreditNoteType")
                    strInfoPedido += "Nro.FAC. REF: " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceRef"].ToString() + "\n";
                strInfoPedido += "Fecha Expedicion: " + Convert.ToDateTime(DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).ToString("yyyy/MM/dd HH:mm:ss") + "\n" +
                                 "Fecha Vencimiento: " + Convert.ToDateTime(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"]).ToString("yyyy/MM/dd HH:mm:ss") + "\n" +
                                 "Condiciones de pago:" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["TermsCodeDescription"].ToString();                                 
            
                iTextSharp.text.pdf.PdfPCell celPedidoText = new iTextSharp.text.pdf.PdfPCell(new Phrase(strInfoPedido, fontPedido));
                celPedidoText.Colspan = 1;
                celPedidoText.Padding = 3;
                celPedidoText.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                celPedidoText.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableHead.AddCell(celPedidoText);

                iTextSharp.text.pdf.PdfPCell celQRImage = new iTextSharp.text.pdf.PdfPCell(iTextSharp.text.Image.GetInstance(QRInvoice, BaseColor.WHITE));
                celQRImage.Colspan = 1;
                celQRImage.Padding = 3;
                celQRImage.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celQRImage.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableHead.AddCell(celQRImage);
                
                //Tabla - Items.
                iTextSharp.text.pdf.PdfPTable tableItems = new iTextSharp.text.pdf.PdfPTable(7);
                tableItems.HorizontalAlignment = Element.ALIGN_CENTER;

                //Dimenciones.
                float[] Dimencion = new float[7];
                Dimencion[0] = 2.0F;//InvoiceLine
                Dimencion[1] = 3.0F;//Codigo
                Dimencion[2] = 13.0F;//Descripcion
                Dimencion[3] = 3.0F;//Cantidad
                Dimencion[4] = 3.0F;//Precio Unitario
                Dimencion[5] = 3.0F;//IVA                
                Dimencion[6] = 5.0F;//Total Linea

                //Configuracion y creacion de la tabla Comparativa.
                tableItems.WidthPercentage = 100;//writer.DirectContent.PdfDocument.PageSize.Width;
                tableItems.SetWidths(Dimencion);
                tableItems.SpacingBefore = 1.0F;
                tableItems.SpacingAfter = 1.0F;

                //Configuramos las columnas.
                if (!AddColumnsInvoiceLineRimax(ref tableItems))
                    return false;

                //Filas 
                iTextSharp.text.Font fuenteDatos = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 8, iTextSharp.text.Font.NORMAL);
                //foreach (DataRow oFila in DtItems.Rows)                
                foreach (DataRow InvoiceLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
                {
                    if (!AddRowInvoiceRimax(InvoiceLine, ref tableItems, fuenteDatos, DsInvoiceAR.Tables["InvcTax"]))
                        return false;
                }

                tableItems.HeadersInEvent = true;
                tableItems.CompleteRow();                                            

                iTextSharp.text.Font fontObsTitle = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 9, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontObs = FontFactory.GetFont(FontFactory.HELVETICA, 5, iTextSharp.text.Font.BOLD);
                PdfDiv divObs = new PdfDiv();
                divObs.Float = iTextSharp.text.pdf.PdfDiv.FloatType.LEFT;
                divObs.Position = iTextSharp.text.pdf.PdfDiv.PositionType.STATIC;
                divObs.BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID;
                divObs.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                divObs.Height = 70;//100;
                divObs.Width = 330;//320
                divObs.BackgroundColor = BaseColor.WHITE;                               

                string ObsFactura = "DESPACHADO A:\n\n" +
                                    "ShipToNum: " + DsInvoiceAR.Tables["InvcDtl"].Rows[0]["ShipToNum"].ToString() + "\n" +
                                    DsInvoiceAR.Tables["InvcDtl"].Rows[0]["DispShipToAddr"].ToString();

                Paragraph prgObs = new Paragraph(ObsFactura, fontObs);
                prgObs.Alignment = Element.ALIGN_LEFT;
                //divObs.AddElement(prgObsTitle);
                divObs.AddElement(prgObs);

                //TOTALES FACTURA
                iTextSharp.text.pdf.PdfPTable tableTotales = new iTextSharp.text.pdf.PdfPTable(3);
                tableTotales.HorizontalAlignment = Element.ALIGN_CENTER;

                //Dimenciones.
                float [] DimencionTotales = new float[3];
                DimencionTotales[0] = 5.0F;
                DimencionTotales[1] = 3.0F;
                DimencionTotales[2] = 8.0F;

                //Configuracion y creacion de la tabla Comparativa.
                tableTotales.TotalWidth = 310; //208;//writer.DirectContent.PdfDocument.PageSize.Width;
                tableTotales.SetWidths(DimencionTotales);
                tableTotales.SpacingBefore = 1.0F;
                tableTotales.SpacingAfter = 1.0F;
                tableTotales.HorizontalAlignment = Element.ALIGN_RIGHT;

                //Totales.
                if (!AddTotalesInvoice(ref tableTotales, DsInvoiceAR))
                    return false;

                iTextSharp.text.Font fontTotales = FontFactory.GetFont(FontFactory.HELVETICA, 9, iTextSharp.text.Font.NORMAL);
                PdfDiv divTotales = new PdfDiv();
                divTotales.Float = iTextSharp.text.pdf.PdfDiv.FloatType.LEFT;
                divTotales.Position = iTextSharp.text.pdf.PdfDiv.PositionType.STATIC;
                divTotales.BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID;
                divTotales.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                divTotales.Height = 70;//150;
                divTotales.Width = 208;
                divTotales.BackgroundColor = BaseColor.WHITE;
                divTotales.AddElement(tableTotales);

                /****************TABLA DESPACHO:****************/
                iTextSharp.text.pdf.PdfPTable tableDespacho = new iTextSharp.text.pdf.PdfPTable(1);
                tableDespacho.HorizontalAlignment = Element.ALIGN_LEFT;

                //Dimenciones.
                float[] DimencionDespacho = new float[1];
                DimencionDespacho[0] = 10.0F;//Line Desc              

                //Configuracion y creacion de la tabla Comparativa.
                tableDespacho.WidthPercentage = 100;
                tableDespacho.SetWidths(DimencionDespacho);
                tableDespacho.SpacingBefore = 1.0F;
                tableDespacho.SpacingAfter = 1.0F;

                //Info Recepcion factura
                if (!AddDespachoInfo(ref tableDespacho, DsInvoiceAR.Tables["InvcDtl"]))
                    return false;

                PdfDiv divDespacho = new PdfDiv();
                divDespacho.Float = iTextSharp.text.pdf.PdfDiv.FloatType.LEFT;
                divDespacho.Position = iTextSharp.text.pdf.PdfDiv.PositionType.STATIC;
                divDespacho.BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID;
                divDespacho.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                divDespacho.Height = 70;//100;
                divDespacho.Width = 540;
                divDespacho.BackgroundColor = BaseColor.WHITE;
                divDespacho.AddElement(tableDespacho);    
                              
                document.Add(divLogo);
                document.Add(divTitle);
                document.Add(divLegalNum);
                document.Add(tableHead);                
                document.Add(separator);
                document.Add(tableItems);
                document.Add(separator);
                document.Add(divObs);
                document.Add(divTotales);                                                
                document.Add(divDespacho);

                /*PIE DE PAGINA*/
                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();
                //RutaPdf = Ruta + NomArchivo;
                RutaPdf = NomArchivo;
                return true;
            }
            catch (Exception ex)
            {
                strError = "1. Error al crear PDF: " + ex.Message;
                return false;
            }
        }

        private bool AddColumnsInvoiceLineRimax(ref iTextSharp.text.pdf.PdfPTable table)
        {
            try
            {
                iTextSharp.text.Font fuenteEncabezado = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 10, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.pdf.PdfPCell celda0 = new iTextSharp.text.pdf.PdfPCell(new Phrase("Linea", fuenteEncabezado));
                celda0.Colspan = 1;
                celda0.Padding = 3;
                celda0.BackgroundColor = iTextSharp.text.pdf.ExtendedColor.LIGHT_GRAY;
                celda0.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celda0.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                table.AddCell(celda0);

                iTextSharp.text.pdf.PdfPCell celda1 = new iTextSharp.text.pdf.PdfPCell(new Phrase("Código", fuenteEncabezado));
                celda1.Colspan = 1;
                celda1.Padding = 3;
                celda1.BackgroundColor = iTextSharp.text.pdf.ExtendedColor.LIGHT_GRAY;
                celda1.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celda1.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                table.AddCell(celda1);

                iTextSharp.text.pdf.PdfPCell celda2 = new iTextSharp.text.pdf.PdfPCell(new Phrase("Descripción", fuenteEncabezado));
                celda2.Colspan = 1;
                celda2.Padding = 3;
                celda2.BackgroundColor = iTextSharp.text.pdf.ExtendedColor.LIGHT_GRAY;
                celda2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celda2.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                table.AddCell(celda2);

                iTextSharp.text.pdf.PdfPCell celda3 = new iTextSharp.text.pdf.PdfPCell(new Phrase("Cantidad", fuenteEncabezado));
                celda3.Colspan = 1;
                celda3.Padding = 3;
                celda3.BackgroundColor = iTextSharp.text.pdf.ExtendedColor.LIGHT_GRAY;
                celda3.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celda3.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                table.AddCell(celda3);

                iTextSharp.text.pdf.PdfPCell celda4 = new iTextSharp.text.pdf.PdfPCell(new Phrase("Precio Unit", fuenteEncabezado));
                celda4.Colspan = 1;
                celda4.Padding = 3;
                celda4.BackgroundColor = iTextSharp.text.pdf.ExtendedColor.LIGHT_GRAY;
                celda4.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celda4.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                table.AddCell(celda4);

                iTextSharp.text.pdf.PdfPCell celda5 = new iTextSharp.text.pdf.PdfPCell(new Phrase("%IVA", fuenteEncabezado));
                celda5.Colspan = 1;
                celda5.Padding = 3;
                celda5.BackgroundColor = iTextSharp.text.pdf.ExtendedColor.LIGHT_GRAY;
                celda5.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celda5.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                table.AddCell(celda5);

                iTextSharp.text.pdf.PdfPCell celda6 = new iTextSharp.text.pdf.PdfPCell(new Phrase("TOTAL", fuenteEncabezado));
                celda6.Colspan = 1;
                celda6.Padding = 3;
                celda6.BackgroundColor = iTextSharp.text.pdf.ExtendedColor.LIGHT_GRAY;
                celda6.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celda6.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                table.AddCell(celda6);

                return true;
            }
            catch (Exception ex)
            {
                strError = "Error al crear columnas de tabla Items: " + ex.Message;
                return false;
            }
        }

        private bool AddRowInvoiceRimax(DataRow InvoiceLine, ref iTextSharp.text.pdf.PdfPTable table, iTextSharp.text.Font fuenteDatos, DataTable DtInvcTax)
        {
            try
            {                
                iTextSharp.text.pdf.PdfPCell celLinea = new iTextSharp.text.pdf.PdfPCell(new Phrase(InvoiceLine["InvoiceLine"].ToString(), fuenteDatos));
                celLinea.Colspan = 1;
                celLinea.Padding = 3;
                celLinea.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celLinea.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                table.AddCell(celLinea);                
                
                iTextSharp.text.pdf.PdfPCell celCodigo = new iTextSharp.text.pdf.PdfPCell(new Phrase(InvoiceLine["PartNum"].ToString(), fuenteDatos));
                celCodigo.Colspan = 1;
                celCodigo.Padding = 3;
                celCodigo.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celCodigo.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                table.AddCell(celCodigo);                
                
                iTextSharp.text.pdf.PdfPCell celDesc = new iTextSharp.text.pdf.PdfPCell(new Phrase(InvoiceLine["LineDesc"].ToString(), fuenteDatos));
                celDesc.Colspan = 1;
                celDesc.Padding = 3;
                celDesc.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                celDesc.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                table.AddCell(celDesc);                
                
                decimal Qty = Convert.ToDecimal(InvoiceLine["SellingShipQty"].ToString());
                iTextSharp.text.pdf.PdfPCell celCant = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:N0}", Qty), fuenteDatos));
                celCant.Colspan = 1;
                celCant.Padding = 3;
                celCant.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celCant.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                table.AddCell(celCant);                
                
                decimal UnitPrice = Convert.ToDecimal(InvoiceLine["DocUnitPrice"].ToString());
                iTextSharp.text.pdf.PdfPCell celUnitPrice = new iTextSharp.text.pdf.PdfPCell(new Phrase("$" + string.Format("{0:N2}", UnitPrice), fuenteDatos));
                celUnitPrice.Colspan = 1;
                celUnitPrice.Padding = 3;
                celUnitPrice.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celUnitPrice.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                table.AddCell(celUnitPrice);                
                
                decimal Percent = Convert.ToDecimal(GetPercentIdImpDIAN(InvoiceLine["InvoiceNum"].ToString(), InvoiceLine["InvoiceLine"].ToString(), DtInvcTax));
                iTextSharp.text.pdf.PdfPCell celIva = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:N1}", Percent), fuenteDatos));
                celIva.Colspan = 1;
                celIva.Padding = 3;
                celIva.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celIva.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                table.AddCell(celIva);                
                
                decimal PriceLine = Convert.ToDecimal(InvoiceLine["DocExtPrice"].ToString());
                iTextSharp.text.pdf.PdfPCell celExtPrise = new iTextSharp.text.pdf.PdfPCell(new Phrase("$" + string.Format("{0:N2}", PriceLine), fuenteDatos));
                celExtPrise.Colspan = 1;
                celExtPrise.Padding = 3;
                celExtPrise.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celExtPrise.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                table.AddCell(celExtPrise);                

                return true;
            }
            catch (Exception ex)
            {
                strError = "Error addLineInvoice: " + ex.Message;
                return false;
            }
        }

        private bool AddReceipveInvoiceInfo(ref iTextSharp.text.pdf.PdfPTable tableTransport)
        {
            try
            {                
                iTextSharp.text.Font fuenteDatos = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);

                //PdfPCell cell10 = new PdfPCell(new Phrase(" ", fuenteDatos));
                //cell10.HorizontalAlignment = Element.ALIGN_LEFT;
                ////cell10.BorderColor = BaseColor.WHITE;
                //cell10.BorderColorTop = BaseColor.BLACK;
                //cell10.BorderColorRight = BaseColor.WHITE;
                //cell10.DisableBorderSide(0);
                //tableTransport.AddCell(cell10);

                //PdfPCell cell102 = new PdfPCell(new Phrase(" ", fuenteDatos));
                //cell102.HorizontalAlignment = Element.ALIGN_LEFT;
                ////cell102.BorderColor = BaseColor.WHITE;
                //cell10.BorderColorTop = BaseColor.BLACK;
                //cell10.BorderColorLeft = BaseColor.WHITE;
                //cell102.DisableBorderSide(0);
                //tableTransport.AddCell(cell102);

                string cell11text = "\n\n_____________________________________________________\n\n" +
                                    "RECIBI CONFORME Y EN BUEN ESTADO (ADQUIRIENTE)";

                PdfPCell cell11 = new PdfPCell(new Phrase(cell11text, fuenteDatos));
                //cell11.Rowspan = 2;                
                cell11.HorizontalAlignment = Element.ALIGN_LEFT;
                cell11.BorderColorRight = BaseColor.WHITE;
                cell11.BorderColorBottom = BaseColor.WHITE;
                //cell11.Border = 0;
                tableTransport.AddCell(cell11);


                string cell12text = "\n\n_____________________________________________________\n\n" +
                                    "FECHA DE RECIBO:";
                PdfPCell cell12 = new PdfPCell(new Phrase(cell12text, fuenteDatos));
                //cell12.Rowspan = 2;
                cell12.BorderColorLeft = BaseColor.WHITE;
                cell12.BorderColorBottom = BaseColor.WHITE;
                cell12.HorizontalAlignment = Element.ALIGN_LEFT;
                //cell12.Border = 0;
                tableTransport.AddCell(cell12);

                //PdfPCell cell201 = new PdfPCell(new Phrase(" ", fuenteDatos));
                //cell201.HorizontalAlignment = Element.ALIGN_LEFT;
                //cell201.BorderColor = BaseColor.WHITE;
                //cell201.DisableBorderSide(0);
                //tableTransport.AddCell(cell201);

                //PdfPCell cell202 = new PdfPCell(new Phrase(" ", fuenteDatos));
                //cell202.HorizontalAlignment = Element.ALIGN_LEFT;
                //cell202.BorderColor = BaseColor.WHITE;
                //cell202.DisableBorderSide(0);
                //tableTransport.AddCell(cell202);

                string cell21text = "\n\n_____________________________________________________\n\n" +
                              "NOMBRE:";
                PdfPCell cell21 = new PdfPCell(new Phrase(cell21text, fuenteDatos));
                //cell21.Rowspan = 2;
                cell21.BorderColorRight = BaseColor.WHITE;
                cell21.BorderColorBottom = BaseColor.WHITE;
                cell21.HorizontalAlignment = Element.ALIGN_LEFT;
                //cell21.Border = 0;
                tableTransport.AddCell(cell21);


                string cell22text = "\n\n_____________________________________________________\n\n" +
                                  "LUGAR DE CUMPLIMIENTO:";
                PdfPCell cell22 = new PdfPCell(new Phrase(cell22text, fuenteDatos));
                //cell22.Rowspan = 2;
                cell22.BorderColorLeft = BaseColor.WHITE;
                cell22.BorderColorBottom = BaseColor.WHITE;
                cell22.HorizontalAlignment = Element.ALIGN_LEFT;
                //cell22.Border = 0;
                tableTransport.AddCell(cell22);

                //PdfPCell cell301 = new PdfPCell(new Phrase(" ", fuenteDatos));
                //cell301.HorizontalAlignment = Element.ALIGN_LEFT;
                //cell301.BorderColor = BaseColor.WHITE;
                //cell301.DisableBorderSide(0);
                //tableTransport.AddCell(cell301);

                //PdfPCell cell302 = new PdfPCell(new Phrase(" ", fuenteDatos));
                //cell302.HorizontalAlignment = Element.ALIGN_LEFT;
                //cell302.BorderColor = BaseColor.WHITE;
                //cell302.DisableBorderSide(0);
                //tableTransport.AddCell(cell302);

                string cell31text = "\n\n_____________________________________________________\n\n" +
                                    "CEDULA:";
                PdfPCell cell31 = new PdfPCell(new Phrase(cell31text, fuenteDatos));
                //cell31.Rowspan=2;
                cell31.BorderColorRight = BaseColor.WHITE;
                cell31.BorderColorBottom = BaseColor.WHITE;
                cell31.HorizontalAlignment = Element.ALIGN_LEFT;
                //cell31.Border = 0;
                tableTransport.AddCell(cell31);

                string cell32text = "\n\n_____________________________________________________\n\n" +
                                  "FIRMA FACTURA:";
                PdfPCell cell32 = new PdfPCell(new Phrase(cell32text, fuenteDatos));                
                cell32.BorderColorLeft = BaseColor.WHITE;
                cell32.BorderColorBottom = BaseColor.WHITE;
                cell32.HorizontalAlignment = Element.ALIGN_LEFT;
                //cell32.Border = 0;
                tableTransport.AddCell(cell32);

                return true;
            }
            catch (Exception ex)
            {
                strError = "Error al add Receipt Info Invoice: " + ex.Message;
                return false;
            }
        }

        private bool AddDespachoInfo(ref iTextSharp.text.pdf.PdfPTable tableDespacho,DataTable DtCustomer)
        {
            try
            {
                iTextSharp.text.Font fuenteDatos = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);

                string cell11text = "DESPUÉS DEL VENCIMIENTO DE ESTA FACTURA SE CAUSARAN INTERESES DE MORA A LA TASA MÁXIMA LEGALMENTE " +
                                    "PERMITIDA, SIN PERJUICIO DE LA ACCIÓN LEGAL POR INCUMPLIMIENTO EN EL PAGO. EXPRESAMENTE AUTORIZO A " +
                                    "PLÁSTICOS RIMAX S.A.S. PARA QUE OBTENGA DE CUALQUIER FUENTE DE INFORMACIÓN REFERENCIAS DE MI (NUESTRO) " +
                                    "CRÉDITO, HÁBITOS DE PAGO Y EN GENERAL EL CUMPLIMIENTO DE MI (NUESTRAS) OBLIGACIONES. AUTORIZO " +
                                    "IRREVOCABLEMENTE A QUE EN CASO DE INCUMPLIMIENTO SE INCORPORE MI (NUESTRO) NOMBRE, NIT. (C.C.) EN " +
                                    "ARCHIVO DE DEUDORES MOROSOS A CUALQUIER ENTIDAD DEDICADA A ESTE MENESTER EXONERANDO DE TODA " +
                                    "RESPONSABILIDAD AL OTORGANTE DEL CRÉDITO.";
                    


                PdfPCell cell11 = new PdfPCell(new Phrase(cell11text, fuenteDatos));
                //cell11.Rowspan = 2;                
                cell11.HorizontalAlignment = Element.ALIGN_LEFT;
                cell11.BorderColorRight = BaseColor.WHITE;
                cell11.BorderColorBottom = BaseColor.WHITE;
                //cell11.Border = 0;
                tableDespacho.AddCell(cell11);


                //string cell12text = "DIRECCION:\n\n";
                //                    //DtCustomer.Rows[0]["Address1"].ToString();
                //PdfPCell cell12 = new PdfPCell(new Phrase(cell12text, fuenteDatos));
                //cell12.BorderColorLeft = BaseColor.WHITE;
                //cell12.BorderColorBottom = BaseColor.WHITE;
                //cell12.HorizontalAlignment = Element.ALIGN_LEFT;
                //tableDespacho.AddCell(cell12);

                //string cell13text = "CIUDAD:\n\n";
                //                    //DtCustomer.Rows[0]["City"].ToString();
                //PdfPCell cell13 = new PdfPCell(new Phrase(cell13text, fuenteDatos));
                //cell13.BorderColorRight = BaseColor.WHITE;
                //cell13.BorderColorBottom = BaseColor.WHITE;
                //cell13.HorizontalAlignment = Element.ALIGN_LEFT;
                //tableDespacho.AddCell(cell13);


                //string cell21text = "TRANSPORTADOR:\n\n" +
                //                    "";
                //PdfPCell cell21 = new PdfPCell(new Phrase(cell21text, fuenteDatos));
                //cell21.BorderColorLeft = BaseColor.WHITE;
                //cell21.BorderColorBottom = BaseColor.WHITE;
                //cell21.HorizontalAlignment = Element.ALIGN_LEFT;
                //tableDespacho.AddCell(cell21);

                //string cell22text = "IDENTIFICACION TRIBUTARIA:\n\n" +
                //                    " ";
                //PdfPCell cell22 = new PdfPCell(new Phrase(cell22text, fuenteDatos));
                //cell22.BorderColorRight = BaseColor.WHITE;
                //cell22.BorderColorBottom = BaseColor.WHITE;
                //cell22.HorizontalAlignment = Element.ALIGN_LEFT;
                //tableDespacho.AddCell(cell22);

                //string cell23text = "PLACA VEHICULO No:\n\n" +
                //                   " ";
                //PdfPCell cell23 = new PdfPCell(new Phrase(cell23text, fuenteDatos));
                //cell23.BorderColorLeft = BaseColor.WHITE;
                //cell23.BorderColorBottom = BaseColor.WHITE;
                //cell23.HorizontalAlignment = Element.ALIGN_LEFT;
                //tableDespacho.AddCell(cell23);


                //string cell31text = "CONDUCTOR:\n\n" +
                //                  " ";
                //PdfPCell cell31 = new PdfPCell(new Phrase(cell31text, fuenteDatos));
                //cell31.BorderColorLeft = BaseColor.WHITE;
                //cell31.BorderColorBottom = BaseColor.WHITE;
                //cell31.HorizontalAlignment = Element.ALIGN_LEFT;
                //tableDespacho.AddCell(cell31);

                //string cell32text = "TELEFONO:\n\n" +
                //                " ";
                //PdfPCell cell32 = new PdfPCell(new Phrase(cell32text, fuenteDatos));
                //cell32.BorderColorLeft = BaseColor.WHITE;
                //cell32.BorderColorBottom = BaseColor.WHITE;
                //cell32.HorizontalAlignment = Element.ALIGN_LEFT;
                //tableDespacho.AddCell(cell32);


                //string cell33text = "C.C. No:\n\n" +
                //                    " ";
                //PdfPCell cell33 = new PdfPCell(new Phrase(cell33text, fuenteDatos));
                //cell33.BorderColorLeft = BaseColor.WHITE;
                //cell33.BorderColorBottom = BaseColor.WHITE;
                //cell33.HorizontalAlignment = Element.ALIGN_LEFT;
                //tableDespacho.AddCell(cell33);

                return true;
            }
            catch (Exception ex)
            {
                strError = "Error al add Despacho Info Invoice: " + ex.Message;
                return false;
            }
        }

        private bool AddFooterInfo(ref iTextSharp.text.pdf.PdfPTable tableFooter)
        {
            try
            {
                iTextSharp.text.Font fuenteDatos = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);

                string cell11text = "LOS FLETES DEL PRESENTE DESPACHO SERAN PAGADOS EN YUMBO PREVIA PRESENTACION DE LA PRESENTE FACTURA DEBIDAMENTE FIRMADA Y SELLADA POR EL DESTINATARIO.";                                 

                PdfPCell cell11 = new PdfPCell(new Phrase(cell11text, fuenteDatos));                              
                cell11.HorizontalAlignment = Element.ALIGN_LEFT;
                cell11.BorderColorRight = BaseColor.WHITE;
                cell11.BorderColorBottom = BaseColor.WHITE;
                cell11.Border = 0;
                tableFooter.AddCell(cell11);


                string cell12text = "\n\n ";
                PdfPCell cell12 = new PdfPCell(new Phrase(cell12text, fuenteDatos));
                //cell12.Rowspan = 2;
                cell12.BorderColorLeft = BaseColor.WHITE;
                cell12.BorderColorBottom = BaseColor.WHITE;
                cell12.HorizontalAlignment = Element.ALIGN_LEFT;
                //cell12.Border = 0;
                tableFooter.AddCell(cell12);

               

                string cell21text = "VALOR FLETE CONTRATADO: $\n\n"+
                                    "EN PLANILLA DE CARGUE No.";                              
                PdfPCell cell21 = new PdfPCell(new Phrase(cell21text, fuenteDatos));
                //cell21.Rowspan = 2;
                cell21.BorderColorRight = BaseColor.WHITE;
                cell21.BorderColorBottom = BaseColor.WHITE;
                cell21.HorizontalAlignment = Element.ALIGN_LEFT;
                //cell21.Border = 0;
                tableFooter.AddCell(cell21);


                string cell22text = "RECIBI CONFORME Y EN BUEN ESTADO(TRANSPORTADOR)";
                PdfPCell cell22 = new PdfPCell(new Phrase(cell22text, fuenteDatos));
                //cell22.Rowspan = 2;
                cell22.BorderColorLeft = BaseColor.WHITE;
                cell22.BorderColorBottom = BaseColor.WHITE;
                cell22.HorizontalAlignment = Element.ALIGN_LEFT;
                //cell22.Border = 0;
                tableFooter.AddCell(cell22);              

                return true;
            }
            catch (Exception ex)
            {
                strError = "Error al add Receipt Info Invoice: " + ex.Message;
                return false;
            }
        }

        #endregion

        #region "Formatos FacturaAR ALICO"

        public bool FacturaNacionalALICO(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType)
        {
            NomArchivo = string.Empty;
            string Ruta = HttpContext.Current.Server.MapPath("~/PDF/" + NIT + "/");

            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            string RutaImg = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";

            NomArchivo = NombreInvoice + ".pdf";

            try
            {
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.
                Document document = new Document(iTextSharp.text.PageSize.LETTER);
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;

                // step 3: we open the document
                document.Open();

                /*TABLA FACTURA*/
                iTextSharp.text.pdf.PdfPTable tableInvoice = new iTextSharp.text.pdf.PdfPTable(6);
                tableInvoice.HorizontalAlignment = Element.ALIGN_CENTER;

                //Dimenciones.
                float[] DimencionInvoice = new float[6];
                DimencionInvoice[0] = 1.0F;//
                DimencionInvoice[1] = 5.0F;//
                DimencionInvoice[2] = 5.0F;//
                DimencionInvoice[3] = 5.0F;//
                DimencionInvoice[4] = 4.0F;//
                DimencionInvoice[5] = 4.0F;//                

                tableInvoice.WidthPercentage = 100;
                tableInvoice.SetWidths(DimencionInvoice);          

                //Celda Logo
                System.Drawing.Image Logo;
                var request = WebRequest.Create(RutaImg);

                using (var response = request.GetResponse())
                using (var stream = response.GetResponseStream())
                {
                    Logo = Bitmap.FromStream(stream);
                }

                iTextSharp.text.Image LogoPdf = iTextSharp.text.Image.GetInstance(Logo, BaseColor.WHITE);
                //LogoPdf.ScaleAbsolute(60f, 60f);
                LogoPdf.ScaleAbsolute(60f, 40f);
                iTextSharp.text.Font fontLogo = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);             

                string InfoNit = "Soluciones Integrales\nen Empaques\n" +
                               "NIT: " + DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"].ToString();
                Paragraph prgNit = new Paragraph(InfoNit, fontLogo);
                prgNit.Alignment = Element.ALIGN_LEFT;
                //divLogo.AddElement(prgNit);

                iTextSharp.text.pdf.PdfPCell celLogoImg = new iTextSharp.text.pdf.PdfPCell(LogoPdf);
                //iTextSharp.text.pdf.PdfPCell celLogoImg = new iTextSharp.text.pdf.PdfPCell();
                celLogoImg.AddElement(LogoPdf);
                //celLogoImg.AddElement(prgNit);
                celLogoImg.Colspan = 2;
                celLogoImg.Padding = 3;
                celLogoImg.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celLogoImg.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                celLogoImg.BorderWidthRight = 0;
                celLogoImg.BorderColorRight = BaseColor.WHITE;                
                tableInvoice.AddCell(celLogoImg);               

                //Celda Titulo
                string strTitulo = "AUTORIZADOS AUTORRETENCIÓN\n" +
                                   "RES No 971 DE JULIO 29 DE 1987 M.H.C.P\n" +
                                   "NIT " + DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"] + "  RÉGIMEN COMÚN\n" +
                                   "IMPORTADOR DISTRIBUIDOR\n" +
                                   "GRANDES CONTROBUYENTES\n" +
                                   "RES No. 000076 de Dic 01-2016 RETENEDOR DE IVA\n" +
                                   "ACT. CIAL. MEDELLIN 203 TARIFA 4*1000/ACT. INDUSTRIAL. MEDELLIN 105 TARIFA 7*1000\n";
                                                        

                iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.pdf.PdfPCell celTittle = new iTextSharp.text.pdf.PdfPCell(new Phrase(strTitulo, fontTitle));
                celTittle.Colspan = 2;
                celTittle.Padding = 3;
                celTittle.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                celTittle.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                celTittle.BorderWidthRight = 0;
                celTittle.BorderWidthLeft = 0;
                celTittle.BorderColorLeft = BaseColor.WHITE;     
                celTittle.BorderColorRight = BaseColor.WHITE;
                tableInvoice.AddCell(celTittle);

                //Celda Internal Num                
                iTextSharp.text.Font fontInternal= FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.pdf.PdfPCell celInternal = new iTextSharp.text.pdf.PdfPCell(new Phrase("Número Interno:" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceNum"].ToString() + "\nSOMOS AUTORRETENEDORES RENTA\nBODEGA " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["Plant"].ToString(), fontInternal));
                celInternal.Colspan = 1;
                celInternal.Padding = 3;
                celInternal.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                celInternal.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                celInternal.BorderWidthRight = 0;
                celInternal.BorderWidthLeft = 0;
                celInternal.BorderColorLeft = BaseColor.WHITE;
                celInternal.BorderColorRight = BaseColor.WHITE;                
                tableInvoice.AddCell(celInternal);

                string TipoFactura = "FACTURA DE VENTA\n";                           

                //Celda LegalNum
                iTextSharp.text.Font fontLegalNum = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.pdf.PdfPCell celLegalNum = new iTextSharp.text.pdf.PdfPCell(new Phrase(TipoFactura + " No. " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"].ToString(), fontLegalNum));
                celLegalNum.Colspan = 2;
                celLegalNum.Padding = 3;
                celLegalNum.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                celLegalNum.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                //celLegalNum.BorderWidthRight = 0;
                celLegalNum.BorderWidthLeft = 0;
                celLegalNum.BorderColorLeft = BaseColor.WHITE;
                //celLegalNum.BorderColorRight = BaseColor.WHITE;  
                tableInvoice.AddCell(celLegalNum);              
                

                /*VENDIDO A*/
                iTextSharp.text.Font fontAdquiriente = FontFactory.GetFont(FontFactory.HELVETICA,7, iTextSharp.text.Font.NORMAL);
                string strInfoAdquiriente = "SEÑORES:\n" +
                                            DsInvoiceAR.Tables["Customer"].Rows[0]["Name"].ToString() + "\n" +
                                            "" + DsInvoiceAR.Tables["Customer"].Rows[0]["Address1"].ToString() + "\n" +
                                            "" + DsInvoiceAR.Tables["Customer"].Rows[0]["City"].ToString() + "\n" +
                                            "(" + DsInvoiceAR.Tables["Customer"].Rows[0]["State"].ToString() + "- COLOMBIA)\n" +
                                            "TELEFONO " + DsInvoiceAR.Tables["Customer"].Rows[0]["PhoneNum"].ToString() + "\n" +
                                            "NIT " + DsInvoiceAR.Tables["Customer"].Rows[0]["ResaleID"].ToString() + "\n" +
                                            "Email " + DsInvoiceAR.Tables["Customer"].Rows[0]["EMailAddress"].ToString();

               
                iTextSharp.text.pdf.PdfPCell celVendidoA = new iTextSharp.text.pdf.PdfPCell(new Phrase(strInfoAdquiriente, fontAdquiriente));
                celVendidoA.Colspan = 2;
                celVendidoA.Padding = 3;
                celVendidoA.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                celVendidoA.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableInvoice.AddCell(celVendidoA);


                /*TABLA INFO PEDIDO*/
                iTextSharp.text.Font fontPedido = FontFactory.GetFont(FontFactory.HELVETICA,7, iTextSharp.text.Font.NORMAL);                
                string strInfoPedido ="INFORMACIÓN FACTURA:\n"+ 
                                      "Nro Pedido: " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["OrderNum"].ToString() + "\n";
                if (InvoiceType == "InvoiceType")
                    strInfoPedido += "Nro.FAC. REF: " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceNum"].ToString() + "\n";
                else if (InvoiceType == "CreditNoteType")
                    strInfoPedido += "Nro.FAC. REF: " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceRef"].ToString() + "\n";
                strInfoPedido += "Fecha Factura: " + Convert.ToDateTime(DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).ToString("yyyy/MM/dd HH:mm:ss") + "\n" +
                                 "Fecha Vencimiento: " + Convert.ToDateTime(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"]).ToString("yyyy/MM/dd HH:mm:ss");

                
                iTextSharp.text.pdf.PdfPTable tableInfoPedido = new iTextSharp.text.pdf.PdfPTable(4);
                tableInvoice.HorizontalAlignment = Element.ALIGN_CENTER;

                //Dimenciones.
                float[] DimencionPedido = new float[4];
                DimencionPedido[0] = 5.0F;//
                DimencionPedido[1] = 5.0F;//
                DimencionPedido[2] = 5.0F;//
                DimencionPedido[3] = 5.0F;//

                tableInfoPedido.WidthPercentage = 100;
                tableInfoPedido.SetWidths(DimencionPedido);  

                //1-0 Fecha Factura
                iTextSharp.text.pdf.PdfPCell cellFechaInvoice = new iTextSharp.text.pdf.PdfPCell(new Phrase("FECHA\n" + Convert.ToDateTime(DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).ToString("dd/MM/yyyy"), fontPedido));
                cellFechaInvoice.Colspan = 1;
                cellFechaInvoice.Padding = 3;
                cellFechaInvoice.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellFechaInvoice.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableInfoPedido.AddCell(cellFechaInvoice);

                //1-1 Orden de venta / Embarque
                iTextSharp.text.pdf.PdfPCell cellOVShip = new iTextSharp.text.pdf.PdfPCell(new Phrase("OV / EMB\n" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["OrderNum"].ToString() + " / " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["PackSlipNum"].ToString(), fontPedido));
                cellOVShip.Colspan = 1;
                cellOVShip.Padding = 3;
                cellOVShip.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                cellOVShip.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableInfoPedido.AddCell(cellOVShip);

                //1-2 Orden de Compra
                iTextSharp.text.pdf.PdfPCell cellOC = new iTextSharp.text.pdf.PdfPCell(new Phrase("PEDIDO No." + DsInvoiceAR.Tables["InvcHead"].Rows[0]["PONum"].ToString(), fontPedido));
                cellOC.Colspan = 1;
                cellOC.Padding = 3;
                cellOC.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellOC.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableInfoPedido.AddCell(cellOC);

                //1-3 GUIA TRANS
                iTextSharp.text.pdf.PdfPCell cellGuiaTrans = new iTextSharp.text.pdf.PdfPCell(new Phrase("GUIA TRANS.", fontPedido));
                cellGuiaTrans.Colspan = 1;
                cellGuiaTrans.Padding = 3;
                cellGuiaTrans.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellGuiaTrans.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableInfoPedido.AddCell(cellGuiaTrans);

                //2-0  NIT Comprador
                iTextSharp.text.pdf.PdfPCell cellNitComprador = new iTextSharp.text.pdf.PdfPCell(new Phrase("NIT COMPRADOR\n" + DsInvoiceAR.Tables["Customer"].Rows[0]["ResaleID"].ToString(), fontPedido));
                cellNitComprador.Colspan = 1;
                cellNitComprador.Padding = 3;
                cellNitComprador.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellNitComprador.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableInfoPedido.AddCell(cellNitComprador);

                //2-1  ZONA
                iTextSharp.text.pdf.PdfPCell cellZona = new iTextSharp.text.pdf.PdfPCell(new Phrase("ZONA\n" + DsInvoiceAR.Tables["Customer"].Rows[0]["TerritoryTerritoryDesc"].ToString(), fontPedido));
                cellZona.Colspan = 1;
                cellZona.Padding = 3;
                cellZona.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellZona.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableInfoPedido.AddCell(cellZona);

                //2-2  Transportador
                iTextSharp.text.pdf.PdfPCell cellTransportador = new iTextSharp.text.pdf.PdfPCell(new Phrase("TRANSPORTADOR", fontPedido));
                cellTransportador.Colspan = 2;
                cellTransportador.Padding = 3;
                cellTransportador.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellTransportador.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableInfoPedido.AddCell(cellTransportador);

                //3-0  VENDEDOR
                iTextSharp.text.pdf.PdfPCell cellVendedor = new iTextSharp.text.pdf.PdfPCell(new Phrase("VENDEDOR\n" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["SalesRepCode1"].ToString(), fontPedido));
                cellVendedor.Colspan = 1;
                cellVendedor.Padding = 3;
                cellVendedor.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellVendedor.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableInfoPedido.AddCell(cellVendedor);

                //3-1  FORMA PAGO
                iTextSharp.text.pdf.PdfPCell cellFormaPago = new iTextSharp.text.pdf.PdfPCell(new Phrase("FORMA PAGO\n" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["TermsCodeDescription"].ToString(), fontPedido));
                cellFormaPago.Colspan = 1;
                cellFormaPago.Padding = 3;
                cellFormaPago.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellFormaPago.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableInfoPedido.AddCell(cellFormaPago);

                //3-2 VENCIMIENTO
                iTextSharp.text.pdf.PdfPCell cellVencimiento = new iTextSharp.text.pdf.PdfPCell(new Phrase("VENCIMIENTO\n" + Convert.ToDateTime(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"]).ToString("dd/MM/yyyy"), fontPedido));
                cellVencimiento.Colspan = 2;
                cellVencimiento.Padding = 3;
                cellVencimiento.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellVencimiento.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableInfoPedido.AddCell(cellVencimiento);

                tableInfoPedido.WidthPercentage = 100;
                tableInfoPedido.SetWidths(DimencionPedido);

                //cel Info Pedido
                iTextSharp.text.pdf.PdfPCell celInfoPedido = new iTextSharp.text.pdf.PdfPCell(tableInfoPedido);
                celInfoPedido.Colspan = 2;
                celInfoPedido.Padding = 3;
                celInfoPedido.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                celInfoPedido.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableInvoice.AddCell(celInfoPedido);                

                /*CELL QR*/
                iTextSharp.text.Image QRPdf = iTextSharp.text.Image.GetInstance(QRInvoice, BaseColor.WHITE);                
                iTextSharp.text.pdf.PdfPCell celImgQR = new iTextSharp.text.pdf.PdfPCell(QRPdf);
                celImgQR.Colspan = 2;
                celImgQR.Padding = 3;
                celImgQR.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celImgQR.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableInvoice.AddCell(celImgQR);

                tableInvoice.HeadersInEvent = true;
                tableInvoice.CompleteRow();

                /*Tabla - ITEMS FACTURA.*/
                iTextSharp.text.pdf.PdfPTable tableItems = new iTextSharp.text.pdf.PdfPTable(6);
                tableItems.HorizontalAlignment = Element.ALIGN_CENTER;

                //Dimenciones.
                float[] Dimencion = new float[6];
                Dimencion[0] = 3.0F;//REFERENCIA
                Dimencion[1] = 13.0F;//Descripcion
                Dimencion[2] = 2.0F;//Cantidad
                Dimencion[3] = 1.0F;//UOM
                Dimencion[4] = 3.0F;//VR UNITARIO
                Dimencion[5] = 3.0F;//VR TOTAL                

                //Configuracion y creacion de la tabla Comparativa.
                tableItems.WidthPercentage = 100;//writer.DirectContent.PdfDocument.PageSize.Width;
                tableItems.SetWidths(Dimencion);                

                //Configuramos las columnas.
                if (!AddColumnsInvoiceLineALICO(ref tableItems))
                    return false;

                //Filas Factura
                iTextSharp.text.Font fuenteDatos = FontFactory.GetFont(FontFactory.HELVETICA,7, iTextSharp.text.Font.NORMAL);
                    
                decimal TotalDescountInvc = 0;
                
                foreach (DataRow InvoiceLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
                {
                    TotalDescountInvc += Convert.ToDecimal(InvoiceLine["DspDocDiscount"]);                    
                    if (!AddRowInvoiceALICO(InvoiceLine, ref tableItems, fuenteDatos, DsInvoiceAR.Tables["InvcTax"]))
                        return false;
                }

                //Lineas de cargos Miscelaneos.
                decimal TotalMissCharges = 0;//Valor de Flete.
                if (DsInvoiceAR.Tables.Contains("InvcMisc"))
                { 
                    foreach (DataRow InvoiceMissLine in DsInvoiceAR.Tables["InvcMisc"].Rows)
                    {
                        AddRowMissInvoiceALICO(InvoiceMissLine, ref tableItems, fuenteDatos, ref TotalMissCharges);
                    }
                }

                tableItems.HeadersInEvent = true;
                tableItems.CompleteRow();

                iTextSharp.text.pdf.PdfPCell celItems = new iTextSharp.text.pdf.PdfPCell(tableItems);
                celItems.Colspan = 6;
                celItems.Padding = 3;
                celItems.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                celItems.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableInvoice.AddCell(celItems);

                //OBS1 Factura
                iTextSharp.text.Font fontObs1= FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                string Obs1 = "SEGÚN EL ARTICULO 86 DE LA LEY 1676 DE 2013, ESTA FACTURA\n" +
                              "SE CONSIDERA IRREVOCABLEMENTE ACEPTADA SI EL\n" +
                              "COMPRADOR NO RECLAMA EN CONTRA DE SU CONTENIDO\n" +
                              "DENTRO DE LOS TRES (3) DIAS HÁBILES SIGUIENTES A SU RECEPCIÓN.";
                iTextSharp.text.pdf.PdfPCell celObs1 = new iTextSharp.text.pdf.PdfPCell(new Phrase(Obs1, fontObs1));
                celObs1.Colspan = 3;
                celObs1.Padding = 3;
                celObs1.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                celObs1.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableInvoice.AddCell(celObs1);

                //Aceptacion Factura.
                iTextSharp.text.Font fontAcepta = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                string Acepta = "Aceptación, recibo factura y recibo de\n" +
                                "mercancia\n\n\n" +
                                "Firma y/o Nombre\n\n" +
                                "_________________________";
                iTextSharp.text.pdf.PdfPCell celAcepta = new iTextSharp.text.pdf.PdfPCell(new Phrase(Acepta, fontAcepta));
                celAcepta.Colspan = 1;
                celAcepta.Padding = 3;
                celAcepta.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                celAcepta.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableInvoice.AddCell(celAcepta);


                /*TOTALES FACTURA*/
                iTextSharp.text.pdf.PdfPTable tableTotales = new iTextSharp.text.pdf.PdfPTable(3);
                tableTotales.HorizontalAlignment = Element.ALIGN_CENTER;

                //Dimenciones.
                float[] DimencionTotales = new float[3];
                DimencionTotales[0] = 6.0F;
                DimencionTotales[1] = 3.0F;
                DimencionTotales[2] = 6.0F;

                //Configuracion y creacion de la tabla Comparativa.
                tableTotales.TotalWidth = 208;//writer.DirectContent.PdfDocument.PageSize.Width;
                tableTotales.SetWidths(DimencionTotales);
                tableTotales.SpacingBefore = 1.0F;
                tableTotales.SpacingAfter = 1.0F;

                //Totales.
                if (!AddTotalesInvoiceaLICO(ref tableTotales, DsInvoiceAR, TotalDescountInvc,TotalMissCharges))
                    return false;


                iTextSharp.text.pdf.PdfPCell celTotales = new iTextSharp.text.pdf.PdfPCell(tableTotales);
                celTotales.Colspan = 2;
                celTotales.Padding = 3;
                celTotales.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                celTotales.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableInvoice.AddCell(celTotales);
                

                /*OBSERVACIONES2 FACTURA*/
                iTextSharp.text.Font fontObs2 = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                string Obs2 = "OBSERVACION:\n" +
                              "SI SU PAGO ES EN CHEQUE, DEBE SER A NOMBRE DE ALICO S.A\n" +
                              "CON SELLO RESTRINGIDO AL PRIMER BENEFICIARIO Y ENTREGAR\n" +
                              "ÚNICAMENTE AL PERSONAL AUTORIZADO EN LAS CIRCULARES\n" +
                              "DE NUESTRA COMPAÑIA.";
                iTextSharp.text.pdf.PdfPCell celObs2 = new iTextSharp.text.pdf.PdfPCell(new Phrase(Obs2, fontObs2));
                celObs2.Colspan = 3;
                celObs2.Padding = 3;
                celObs2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                celObs2.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableInvoice.AddCell(celObs2);


                //Fecha Recibido
                iTextSharp.text.Font fontFecha = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                string FechaRec = "Fecha:_______________________";
                iTextSharp.text.pdf.PdfPCell celFechaRec = new iTextSharp.text.pdf.PdfPCell(new Phrase(FechaRec, fontFecha));
                celFechaRec.Colspan = 3;
                celFechaRec.Padding = 3;
                celFechaRec.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                celFechaRec.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableInvoice.AddCell(celFechaRec);

                //INFO ALICO
                iTextSharp.text.Font fontInfAlico = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);

                string InfoALICO = DsInvoiceAR.Tables["InvcHead"].Rows[0]["EntryPerson"].ToString() + " " + DateTime.Now + "                                     ALICO " + DsInvoiceAR.Tables["Company"].Rows[0]["Address1"].ToString() + " " + DsInvoiceAR.Tables["Company"].Rows[0]["City"].ToString() + " TEL: +(054)" + DsInvoiceAR.Tables["Company"].Rows[0]["PhoneNum"].ToString() + " FAX: +(054)" + DsInvoiceAR.Tables["Company"].Rows[0]["FaxNum"].ToString();
                iTextSharp.text.pdf.PdfPCell celInfAlico = new iTextSharp.text.pdf.PdfPCell(new Phrase(InfoALICO, fontInfAlico));
                celInfAlico.Colspan = 6;
                celInfAlico.Padding = 3;
                celInfAlico.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                celInfAlico.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableInvoice.AddCell(celInfAlico);
                                             
                document.Add(tableInvoice);                
                document.Add(separator);                
                document.Add(separator);                

                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();
                //RutaPdf = Ruta + NomArchivo;
                RutaPdf = NomArchivo;
                return true;
            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.Message;
                return false;
            }
        }

        public bool NotaCreditoALICO(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType)
        {
            NomArchivo = string.Empty;
            string Ruta = HttpContext.Current.Server.MapPath("~/PDF/" + NIT + "/");

            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            string RutaImg = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";

            NomArchivo = NombreInvoice + ".pdf";

            try
            {
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.
                Document document = new Document(iTextSharp.text.PageSize.LETTER);
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;

                // step 3: we open the document
                document.Open();

                iTextSharp.text.Font fontDiv = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                PdfDiv divLogo = new PdfDiv();
                divLogo.Float = iTextSharp.text.pdf.PdfDiv.FloatType.LEFT;
                divLogo.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divLogo.BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID;
                divLogo.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                divLogo.Height = 100;
                divLogo.Width = 110;//90;
                iTextSharp.text.Image imgLogo =iTextSharp.text.Image.GetInstance(RutaImg);
                imgLogo.ScaleAbsolute(60f, 60f);
                divLogo.BackgroundImage = imgLogo;
                string InfoNit = "\n\n\n\n\n\n\n\n\nSoluciones Integrales\nen Empaques\n" +
                               "NIT: " + DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"];
                Paragraph prgNit = new Paragraph(InfoNit, fontDiv);
                prgNit.Alignment = Element.ALIGN_CENTER;
                //divLogo.AddElement(prgNit);

                divLogo.TextAlignment = Element.ALIGN_TOP;

                PdfDiv divTitle = new PdfDiv();
                divTitle.Float = iTextSharp.text.pdf.PdfDiv.FloatType.LEFT;
                divTitle.Position = iTextSharp.text.pdf.PdfDiv.PositionType.STATIC;
                divTitle.BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID;
                divTitle.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                divTitle.Height = 100;
                divTitle.Width = 340;//360;
                divTitle.BackgroundColor = BaseColor.WHITE;
                iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                string Title = //""+DsInvoiceAR.Tables["Company"].Rows[0]["Name"]+"\n"+
                               //"NIT: "+DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"]+"\n"+
                               "NOTA CREDITO Nro. " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"].ToString()+"\n"+
                               "Nro INTERNO. "+DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceNum"].ToString()+"\n\n"+
                               ""+DsInvoiceAR.Tables["Company"].Rows[0]["Address1"].ToString()+" CONM: 285 44 13\n"+
                               "FAX: "+DsInvoiceAR.Tables["Company"].Rows[0]["FaxNum"].ToString()+" AA 51040 MEDELLIN\n\n"+
                               "AUTORETENEDORES RES. 971 DE JULIO 29 DE 1987\n"+
                               "GRANDES CONTRIBUYENTES RES. 2509 DE DIC. 03 DE 1993";
                Paragraph prgTitle = new Paragraph(Title, fontTitle);
                prgTitle.Alignment = Element.ALIGN_JUSTIFIED;
                divTitle.AddElement(prgTitle);

                PdfDiv divLegalNum = new PdfDiv();
                divLegalNum.Float = iTextSharp.text.pdf.PdfDiv.FloatType.LEFT;
                divLegalNum.Position = iTextSharp.text.pdf.PdfDiv.PositionType.STATIC;
                divLegalNum.BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID;
                divLegalNum.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                divLegalNum.Height = 100;
                divLegalNum.Width = 90;
                divLegalNum.BackgroundColor = BaseColor.WHITE;
                iTextSharp.text.Font fontTitleLegalNum = FontFactory.GetFont(FontFactory.TIMES, 7, iTextSharp.text.Font.BOLD);                
                DateTime DateCN = Convert.ToDateTime(DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]);
                string TipoFactura = "AÑO  MES  DIA\n" +
                                     DateCN.Year.ToString() + "      " + DateCN.Month.ToString() + "      " + DateCN.Day.ToString();                
                Paragraph prgTitleLegalNum = new Paragraph(TipoFactura, fontTitleLegalNum);
                prgTitleLegalNum.Alignment = Element.ALIGN_LEFT;                
                divLegalNum.AddElement(prgTitleLegalNum);


                //PdfDiv divsp1 = new PdfDiv();
                //divsp1.Float = iTextSharp.text.pdf.PdfDiv.FloatType.LEFT;
                //divsp1.Position = iTextSharp.text.pdf.PdfDiv.PositionType.STATIC;
                //divsp1.BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID;
                //divsp1.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divsp1.Height = 10;
                //divsp1.PercentageWidth = 100;
                //divsp1.BackgroundColor = BaseColor.WHITE;                               
                string sp1 = "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------";
                Paragraph prgsp1 = new Paragraph(sp1, fontTitle);
                prgsp1.Alignment = Element.ALIGN_LEFT;
                //divsp1.AddElement(prgsp1);


                //iTextSharp.text.Font fontAdquirienteTitle = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 9, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontAdquiriente = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                PdfDiv divAdquiruente = new PdfDiv();
                divAdquiruente.Float = iTextSharp.text.pdf.PdfDiv.FloatType.LEFT;
                divAdquiruente.Position = iTextSharp.text.pdf.PdfDiv.PositionType.STATIC;
                divAdquiruente.BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID;
                divAdquiruente.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                divAdquiruente.Height = 100;
                divAdquiruente.Width = 400;
                divAdquiruente.BackgroundColor = BaseColor.WHITE;        
                string Concepto=string.Empty;
                string strInfoAdquiriente = "--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n" +
                                            "SEÑORES: " + DsInvoiceAR.Tables["Customer"].Rows[0]["Name"].ToString() + "                                                                                                                                     $" + string.Format("{0:N2}", Convert.ToDecimal(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"].ToString())) + "\n" +
                                            "NIT:" + DsInvoiceAR.Tables["Customer"].Rows[0]["ResaleID"].ToString() + "\n" +
                                            "--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n" +
                                            "EN LA FECHA HEMOS ABONADO A SU CUENTA LA SUMA DE:\n" +
                                            Nroenletras(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"].ToString()) + "\n" +
                                            "--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n" +
                                            "POR CONCEPTO DE:\n";
                        //                    foreach (DataRow fila in DsInvoiceAR.Tables["InvcHead"].Rows)
                        //                        Concepto += DsInvoiceAR.Tables["InvcDtl"].Rows[0]["LineDesc"].ToString() + "\n";
                        //strInfoAdquiriente += Concepto;                   
                        //strInfoAdquiriente+="ENVIADA: "+DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceRef"].ToString()+"\n\n";
                                                                                                       
                
                Paragraph prgAdquiriente = new Paragraph(strInfoAdquiriente, fontAdquiriente);
                //divAdquiruente.AddElement(prgAdquirienteTitle);
                divAdquiruente.AddElement(prgAdquiriente);                

                PdfDiv divQR = new PdfDiv();
                divQR.Float = iTextSharp.text.pdf.PdfDiv.FloatType.RIGHT;
                divQR.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divQR.BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID;
                divQR.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                divQR.Height = 100;
                divQR.Width = 140;
                divQR.BackgroundColor = BaseColor.WHITE;
                iTextSharp.text.Image ImgQR = iTextSharp.text.Image.GetInstance(QRInvoice, BaseColor.WHITE);
                ImgQR.Alignment = Element.ALIGN_RIGHT;
                ImgQR.ScaleAbsolute(60f, 60f);
                divQR.BackgroundImage = ImgQR;

                //Tabla - Items.
                iTextSharp.text.pdf.PdfPTable tableItems = new iTextSharp.text.pdf.PdfPTable(6);
                tableItems.HorizontalAlignment = Element.ALIGN_CENTER;

                //Dimenciones.
                float[] Dimencion = new float[6];
                Dimencion[0] = 3.0F;//REFERENCIA
                Dimencion[1] = 10.0F;//Descripcion
                Dimencion[2] = 3.0F;//Cantidad
                Dimencion[3] = 2.0F;//UOM
                Dimencion[4] = 4.0F;//VR Unidad
                Dimencion[5] = 4.0F;//VR Total                

                //Configuracion y creacion de la tabla Comparativa.
                tableItems.WidthPercentage = 100;//writer.DirectContent.PdfDocument.PageSize.Width;
                tableItems.SetWidths(Dimencion);
                tableItems.SpacingBefore = 1.0F;
                tableItems.SpacingAfter = 1.0F;

                //Configuramos las columnas.
                if (!AddColumnsInvoiceLineALICO(ref tableItems))
                    return false;

                //Filas 
                iTextSharp.text.Font fuenteDatos = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 8, iTextSharp.text.Font.NORMAL);
                //foreach (DataRow oFila in DtItems.Rows) 
                decimal TotalDescountInvc = 0;
                decimal TotalMissCharges = 0;
                foreach (DataRow InvoiceLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
                {
                    TotalDescountInvc += Convert.ToDecimal(InvoiceLine["DspDocLessDiscount"]);
                    TotalMissCharges += Convert.ToDecimal(InvoiceLine["DspDocTotalMiscChrg"]);
               
                    if (!AddRowInvoiceALICO(InvoiceLine, ref tableItems, fuenteDatos, DsInvoiceAR.Tables["InvcTax"]))
                        return false;
                }

                tableItems.HeadersInEvent = true;
                tableItems.CompleteRow();

                iTextSharp.text.pdf.PdfPTable tableTotales = new iTextSharp.text.pdf.PdfPTable(3);
                tableTotales.HorizontalAlignment = Element.ALIGN_RIGHT;

                //Dimenciones.
                float[] DimencionTotales = new float[3];
                DimencionTotales[0] = 6.0F;
                DimencionTotales[1] = 3.0F;
                DimencionTotales[2] = 6.0F;

                //Configuracion y creacion de la tabla Comparativa.
                tableTotales.WidthPercentage = 30; //208;
                tableTotales.SetWidths(DimencionTotales);
                tableTotales.SpacingBefore = 1.0F;
                tableTotales.SpacingAfter = 1.0F;

                //Totales.
                if (!AddTotalesInvoiceaLICO(ref tableTotales, DsInvoiceAR,TotalDescountInvc,TotalMissCharges))
                    return false;

                tableTotales.HeadersInEvent = true;
                tableTotales.CompleteRow();

                //iTextSharp.text.Font fontTotales = FontFactory.GetFont(FontFactory.HELVETICA, 9, iTextSharp.text.Font.NORMAL);
                //PdfDiv divTotales = new PdfDiv();
                //divTotales.Float = iTextSharp.text.pdf.PdfDiv.FloatType.LEFT;
                //divTotales.Position = iTextSharp.text.pdf.PdfDiv.PositionType.STATIC;
                //divTotales.BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID;
                //divTotales.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divTotales.Height = 150;
                //divTotales.Width = 300;
                //divTotales.BackgroundColor = BaseColor.WHITE;
                //divTotales.AddElement(tableTotales);
                //divTotales.TextAlignment = Element.ALIGN_RIGHT;

                PdfDiv divElaboro = new PdfDiv();
                divElaboro.Float = iTextSharp.text.pdf.PdfDiv.FloatType.LEFT;
                divElaboro.Position = iTextSharp.text.pdf.PdfDiv.PositionType.STATIC;
                divElaboro.BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID;
                divElaboro.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                divElaboro.Height = 100;
                divElaboro.Width = 270;
                divElaboro.BackgroundColor = BaseColor.WHITE;
                iTextSharp.text.Font fontElaboro = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.BOLD);
                string Elaboro = "__________________________________________________________\n";
                Paragraph prgElaboro1 = new Paragraph(Elaboro, fontElaboro);
                prgElaboro1.Alignment = Element.ALIGN_CENTER;
                Paragraph prgElaboro2 = new Paragraph("ELABORO", fontElaboro);
                string PagNum = document.PageNumber+1.ToString(); ;
                Paragraph prgPagNum = new Paragraph("Pag. " +PagNum, fontElaboro);
                prgPagNum.Alignment = Element.ALIGN_LEFT;
                prgElaboro2.Alignment = Element.ALIGN_CENTER;
                divElaboro.AddElement(prgElaboro1);
                divElaboro.AddElement(prgElaboro2);
                divElaboro.AddElement(prgPagNum);

                PdfDiv divReviso = new PdfDiv();
                divReviso.Float = iTextSharp.text.pdf.PdfDiv.FloatType.RIGHT;
                divReviso.Position = iTextSharp.text.pdf.PdfDiv.PositionType.STATIC;
                divReviso.BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID;
                divReviso.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                divReviso.Height = 100;
                divReviso.Width = 270;
                divReviso.BackgroundColor = BaseColor.WHITE;
                iTextSharp.text.Font fontReviso = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.BOLD);
                string Reviso = "__________________________________________________________\n";
                Paragraph prgReviso1 = new Paragraph(Reviso, fontReviso);
                prgReviso1.Alignment = Element.ALIGN_CENTER;
                Paragraph prgReviso2 = new Paragraph("REVISO", fontReviso);
                prgReviso2.Alignment = Element.ALIGN_CENTER;
                Paragraph prgDesc = new Paragraph("ORIGINAL", fontReviso);
                prgDesc.Alignment = Element.ALIGN_RIGHT;   
                divReviso.AddElement(prgReviso1);
                divReviso.AddElement(prgReviso2);
                divReviso.AddElement(prgDesc);

                document.Add(divLogo);
                document.Add(divTitle);
                document.Add(divLegalNum);
                document.Add(prgsp1);
                document.Add(divAdquiruente);                
                document.Add(divQR);                
                document.Add(tableItems);
                document.Add(tableTotales);
                document.Add(prgsp1);
                document.Add(separator);
                document.Add(divElaboro);
                document.Add(divReviso);

                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();
                //RutaPdf = Ruta + NomArchivo;
                RutaPdf = NomArchivo;
                return true;
            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.Message;
                return false;
            }
        }

        public bool FacturaExportacionALICO(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType)
        {
            NomArchivo = string.Empty;
            string Ruta = HttpContext.Current.Server.MapPath("~/PDF/" + NIT + "/");

            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            string RutaImg = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";
            string RutaImgIcontec = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Icontec_" + NIT + ".png";

            NomArchivo = NombreInvoice + ".pdf";

            try
            {
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.
                Document document = new Document(iTextSharp.text.PageSize.LETTER);
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;

                // step 3: we open the document
                document.Open();

                /*TABLA FACTURA*/
                iTextSharp.text.pdf.PdfPTable tableInvoice = new iTextSharp.text.pdf.PdfPTable(7);
                tableInvoice.HorizontalAlignment = Element.ALIGN_CENTER;

                //Dimenciones.
                float[] DimencionInvoice = new float[7];
                DimencionInvoice[0] = 1.0F;//
                DimencionInvoice[1] = 1.0F;//
                DimencionInvoice[2] = 4.0F;//5
                DimencionInvoice[3] = 3.0F;//
                DimencionInvoice[4] = 3.0F;//
                DimencionInvoice[5] = 3.0F;//                
                DimencionInvoice[6] = 3.0F;//

                tableInvoice.WidthPercentage = 100;
                tableInvoice.SetWidths(DimencionInvoice);

                //Celda Logo
                System.Drawing.Image Logo;
                System.Drawing.Image LogoIcontec;

                var request = WebRequest.Create(RutaImg);
                var requestIcontec = WebRequest.Create(RutaImgIcontec);

                //Logo
                using (var response = request.GetResponse())
                using (var stream = response.GetResponseStream())
                {
                    Logo = Bitmap.FromStream(stream);
                }

                //Logo Icontec
                using (var responseIcontec = requestIcontec.GetResponse())
                using (var streamIcontec = responseIcontec.GetResponseStream())
                {
                    LogoIcontec = Bitmap.FromStream(streamIcontec);
                }

                //Instancia PDF Logo ALICO
                iTextSharp.text.Image LogoPdf = iTextSharp.text.Image.GetInstance(Logo, BaseColor.WHITE);
                LogoPdf.ScaleAbsolute(60f, 60f);
                iTextSharp.text.Font fontLogo = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);

                string InfoCompany = DsInvoiceAR.Tables["Company"].Rows[0]["Address1"] + "\n" +
                                     DsInvoiceAR.Tables["Company"].Rows[0]["PhoneNum"] + "\n" +
                                     DsInvoiceAR.Tables["Company"].Rows[0]["City"] + "-" + DsInvoiceAR.Tables["Company"].Rows[0]["Country"] + "\n" +
                                     "Carolinaa@alico-sa.com\n" +
                                     "dagudelo@alico-sa.com\n" +
                                     "WWW.alico-sa.com\n";

                //Celda Titulo
                string strTitulo = "\nAUTORIZADOS AUTORRETENCIÓN RS. 971 DE JULIO 29 DE 1987 M.H.C.P\n" +
                                   "RÉGIMEN COMÚN IMPORTADOR DISTRIBUIDOR\n" +
                                   "GRANDES CONTROBUYENTES RES No. 2509 de Dic 03/93 RETENEDOR DE IVA\n";

                Paragraph prgInfoCompany = new Paragraph(InfoCompany, fontLogo);

                Paragraph prgTitulo = new Paragraph(strTitulo, fontLogo);
                prgTitulo.Alignment = iTextSharp.text.Element.ALIGN_CENTER;

                //LOGO ALICO                
                //iTextSharp.text.pdf.PdfPCell celLogoImg = new iTextSharp.text.pdf.PdfPCell(LogoPdf);               
                iTextSharp.text.pdf.PdfPCell celLogoImg = new iTextSharp.text.pdf.PdfPCell();
                celLogoImg.AddElement(LogoPdf);
                celLogoImg.AddElement(prgInfoCompany);
                celLogoImg.AddElement(prgTitulo);
                celLogoImg.Colspan = 3;//4                
                celLogoImg.Padding = 3;
                celLogoImg.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                celLogoImg.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                celLogoImg.BorderWidthRight = 0;
                celLogoImg.BorderWidthBottom = 0;
                celLogoImg.BorderColorRight = BaseColor.WHITE;
                tableInvoice.AddCell(celLogoImg);                
                
                //LOGO ICONTEC
                //Instancia PDF Logo Icontec

                iTextSharp.text.Font fontLogoIcontec = FontFactory.GetFont(FontFactory.HELVETICA, 5, iTextSharp.text.Font.BOLD);

                iTextSharp.text.Image LogoIcontecpdf = iTextSharp.text.Image.GetInstance(LogoIcontec, BaseColor.WHITE);
                LogoIcontecpdf.ScaleAbsolute(60f, 60f);

                string InfoIcontec = "Diseño, desarrollo, fabricación y\n" +
                                     "comercialización de empaques de\n" +
                                     "barra flexibles y termoformados,\n" +
                                     "para el sector alimenticio, \n" +
                                     "farmaceutico, Quimico e industrial.";

                Paragraph prgInfoIcontec = new Paragraph(InfoIcontec, fontLogoIcontec);
                prgInfoIcontec.Alignment = iTextSharp.text.Element.ALIGN_CENTER;

                //iTextSharp.text.pdf.PdfPCell celLogoImg = new iTextSharp.text.pdf.PdfPCell(LogoPdf);               
                iTextSharp.text.pdf.PdfPCell celImgIcontec = new iTextSharp.text.pdf.PdfPCell();
                celImgIcontec.AddElement(LogoIcontecpdf);
                celImgIcontec.AddElement(prgInfoIcontec);
                celImgIcontec.Colspan = 1;
                celImgIcontec.Padding = 3;
                celImgIcontec.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celImgIcontec.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                celImgIcontec.BorderWidthLeft = 0;
                celImgIcontec.BorderWidthRight = 0;
                celImgIcontec.BorderWidthBottom = 0;
                celImgIcontec.BorderColorRight = BaseColor.WHITE;
                tableInvoice.AddCell(celImgIcontec);   




                //TABLA INFO HEAD
                iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.pdf.PdfPTable tableInfoHead = new iTextSharp.text.pdf.PdfPTable(2);
                tableInfoHead.HorizontalAlignment = Element.ALIGN_CENTER;

                //Dimenciones.
                float[] DimencionInfoHead = new float[2];
                DimencionInfoHead[0] = 5.0F;//
                DimencionInfoHead[1] = 5.0F;//                

                tableInfoHead.WidthPercentage = 100;
                tableInfoHead.SetWidths(DimencionInfoHead);

                //1-0 Factura de Venta
                iTextSharp.text.pdf.PdfPCell cellFacturaVenta = new iTextSharp.text.pdf.PdfPCell(new Phrase("FACTURA DE VENTA No. "+DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"].ToString(),fontTitle));
                cellFacturaVenta.Colspan = 2;
                cellFacturaVenta.Padding = 3;
                cellFacturaVenta.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellFacturaVenta.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableInfoHead.AddCell(cellFacturaVenta);

                //1-1 Comprador
                string strInfoAdquiriente = "COMPRADOR:\n" +
                                            DsInvoiceAR.Tables["Customer"].Rows[0]["Name"].ToString() + "\n" +
                                            "RUT. " + DsInvoiceAR.Tables["Customer"].Rows[0]["ResaleID"].ToString() + "\n" +
                                            "Dir. " + DsInvoiceAR.Tables["Customer"].Rows[0]["Address1"].ToString() + "\n" +
                                            "Tel. " + DsInvoiceAR.Tables["Customer"].Rows[0]["PhoneNum"].ToString() + "\n" +
                                            "" + DsInvoiceAR.Tables["Customer"].Rows[0]["City"].ToString() + DsInvoiceAR.Tables["Customer"].Rows[0]["Country"].ToString();

                iTextSharp.text.pdf.PdfPCell cellComprador = new iTextSharp.text.pdf.PdfPCell(new Phrase(strInfoAdquiriente, fontTitle));
                cellComprador.Colspan = 2;
                cellComprador.Padding = 3;
                cellComprador.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellComprador.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableInfoHead.AddCell(cellComprador);

                //2-0 Fecha Factura                
                iTextSharp.text.pdf.PdfPCell cellFechaInvoice = new iTextSharp.text.pdf.PdfPCell(new Phrase("FECHA\n" + Convert.ToDateTime(DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).ToString("yyyy/MM/dd"), fontTitle));
                cellFechaInvoice.Colspan = 1;
                cellFechaInvoice.Padding = 3;
                cellFechaInvoice.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellFechaInvoice.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableInfoHead.AddCell(cellFechaInvoice);

                //2-1 Fecha Vencimiento
                iTextSharp.text.pdf.PdfPCell cellFechaVence = new iTextSharp.text.pdf.PdfPCell(new Phrase("FECHA DE VENCIMIENTO\n" + Convert.ToDateTime(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"]).ToString("yyyy/MM/dd"), fontTitle));
                cellFechaVence.Colspan = 1;
                cellFechaVence.Padding = 3;
                cellFechaVence.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellFechaVence.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableInfoHead.AddCell(cellFechaVence);

                //3-0 Orden de Compra
                iTextSharp.text.pdf.PdfPCell cellOC = new iTextSharp.text.pdf.PdfPCell(new Phrase("Orden de Compra\n" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["PONum"].ToString(), fontTitle));
                cellOC.Colspan = 2;
                cellOC.Padding = 3;
                cellOC.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellOC.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableInfoHead.AddCell(cellOC);

                //4-0 Forma de pago
                iTextSharp.text.pdf.PdfPCell cellFormaPago = new iTextSharp.text.pdf.PdfPCell(new Phrase("FORMA DE PAGO\n" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["TermsCodeDescription"].ToString(), fontTitle));
                cellFormaPago.Colspan = 1;
                cellFormaPago.Padding = 3;
                cellFormaPago.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellFormaPago.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableInfoHead.AddCell(cellFormaPago);

                //4-1 TRM
                iTextSharp.text.pdf.PdfPCell cellTrm = new iTextSharp.text.pdf.PdfPCell(new Phrase("TRM\n" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["ExchangeRate"].ToString(), fontTitle));
                cellTrm.Colspan = 2;
                cellTrm.Padding = 3;
                cellTrm.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellTrm.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableInfoHead.AddCell(cellTrm);

                /*CELL QR*/
                iTextSharp.text.Image QRPdf = iTextSharp.text.Image.GetInstance(QRInvoice, BaseColor.WHITE);
                iTextSharp.text.pdf.PdfPCell celImgQR = new iTextSharp.text.pdf.PdfPCell(QRPdf);
                celImgQR.Colspan = 2;
                celImgQR.Padding = 3;
                celImgQR.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celImgQR.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableInfoHead.AddCell(celImgQR);

                iTextSharp.text.pdf.PdfPCell cellInfoHead = new iTextSharp.text.pdf.PdfPCell(tableInfoHead);
                cellInfoHead.Colspan = 3;
                cellInfoHead.Padding = 3;
                cellInfoHead.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellInfoHead.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                //cellInfoHead.BorderWidthRight = 0;
                //cellInfoHead.BorderWidthLeft = 0;
                //cellInfoHead.BorderColorLeft = BaseColor.WHITE;
                //cellInfoHead.BorderColorRight = BaseColor.WHITE;
                tableInvoice.AddCell(cellInfoHead);
              

                /*Tabla - ITEMS FACTURA.*/
                iTextSharp.text.pdf.PdfPTable tableItems = new iTextSharp.text.pdf.PdfPTable(7);
                tableItems.HorizontalAlignment = Element.ALIGN_CENTER;

                //Dimenciones.
                float[] Dimencion = new float[7];
                Dimencion[0] = 2.0F;//ITEM
                Dimencion[1] = 3.0F;//REFERENCIA
                Dimencion[2] = 13.0F;//Descripcion
                Dimencion[3] = 2.0F;//Cantidad
                Dimencion[4] = 1.0F;//UOM
                Dimencion[5] = 3.0F;//VR UNITARIO
                Dimencion[6] = 3.0F;//VR TOTAL                

                //Configuracion y creacion de la tabla Comparativa.
                tableItems.WidthPercentage = 100;//writer.DirectContent.PdfDocument.PageSize.Width;
                tableItems.SetWidths(Dimencion);

                //Configuramos las columnas.
                if (!AddColumnsInvoiceLineExportacionALICO(ref tableItems))
                    return false;

                //Filas Factura
                iTextSharp.text.Font fuenteDatos = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);

                decimal TotalDescountInvc = 0;
                decimal TotalMissCharges = 0;
                foreach (DataRow InvoiceLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
                {
                    TotalDescountInvc += Convert.ToDecimal(InvoiceLine["DspDocLessDiscount"]);
                    TotalMissCharges += Convert.ToDecimal(InvoiceLine["DspDocTotalMiscChrg"]);

                    if (!AddRowInvoiceExportacionALICO(InvoiceLine, ref tableItems, fuenteDatos, DsInvoiceAR.Tables["InvcTax"]))
                        return false;
                }

                tableItems.HeadersInEvent = true;
                tableItems.CompleteRow();

                iTextSharp.text.pdf.PdfPCell celItems = new iTextSharp.text.pdf.PdfPCell(tableItems);
                celItems.Colspan = 7;
                celItems.Padding = 3;
                celItems.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                celItems.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableInvoice.AddCell(celItems);                

                iTextSharp.text.pdf.PdfPCell celSeparator1 = new iTextSharp.text.pdf.PdfPCell(new Phrase("\n", fontTitle));
                celSeparator1.Colspan = 7;
                celSeparator1.Padding = 3;
                celSeparator1.Border = 0;
                celSeparator1.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                celSeparator1.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableInvoice.AddCell(celSeparator1);

                iTextSharp.text.pdf.PdfPCell celSeparator2 = new iTextSharp.text.pdf.PdfPCell(new Phrase("\n", fontTitle));
                celSeparator2.Colspan = 7;
                celSeparator2.Padding = 3;
                celSeparator2.Border = 0;
                celSeparator2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                celSeparator2.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableInvoice.AddCell(celSeparator2);

                //TOTALES Y RESUMEN FACTURA.                
                iTextSharp.text.pdf.PdfPTable tableTotales = new iTextSharp.text.pdf.PdfPTable(8);
                tableTotales.HorizontalAlignment = Element.ALIGN_CENTER;

                //Dimenciones.
                float[] DimencionTotales = new float[8];
                DimencionTotales[0] = 3.0F;
                DimencionTotales[1] = 3.0F;
                DimencionTotales[2] = 3.0F;
                DimencionTotales[3] = 3.0F;
                DimencionTotales[4] = 3.0F;
                DimencionTotales[5] = 3.0F;
                DimencionTotales[6] = 3.0F;
                DimencionTotales[7] = 3.0F;

                tableTotales.WidthPercentage = 100;
                tableTotales.SetWidths(DimencionTotales);

                //1-O MARCA
                iTextSharp.text.pdf.PdfPCell cellMarca= new iTextSharp.text.pdf.PdfPCell(new Phrase("MARCA\nALICO", fontTitle));
                cellMarca.Colspan = 1;
                cellMarca.Padding = 3;
                cellMarca.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellMarca.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableTotales.AddCell(cellMarca);
                //1-1 PAIS DE ORIGEN
                iTextSharp.text.pdf.PdfPCell cellPaisOrigen = new iTextSharp.text.pdf.PdfPCell(new Phrase("PAIS ORIGEN\nCOLOMBIA", fontTitle));
                cellPaisOrigen.Colspan = 1;
                cellPaisOrigen.Padding = 3;
                cellPaisOrigen.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellPaisOrigen.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableTotales.AddCell(cellPaisOrigen);
                //1-2 PESO BRUTO
                iTextSharp.text.pdf.PdfPCell cellPesoBruto = new iTextSharp.text.pdf.PdfPCell(new Phrase("PESO BRUTO\n" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["PesoBruto_c"].ToString(), fontTitle));
                cellPesoBruto.Colspan = 1;
                cellPesoBruto.Padding = 3;
                cellPesoBruto.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellPesoBruto.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableTotales.AddCell(cellPesoBruto);
                //1-3 PESO NETO
                iTextSharp.text.pdf.PdfPCell cellPesoNeto = new iTextSharp.text.pdf.PdfPCell(new Phrase("PESO NETO\n" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["PesoNeto_c"].ToString(), fontTitle));
                cellPesoNeto.Colspan = 1;
                cellPesoNeto.Padding = 3;
                cellPesoNeto.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellPesoNeto.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableTotales.AddCell(cellPesoNeto);
                //1-4 NRO CAJAS
                iTextSharp.text.pdf.PdfPCell cellNroCajas = new iTextSharp.text.pdf.PdfPCell(new Phrase("N°. DE CAJAS\n" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["Ncajas_c"].ToString(), fontTitle));
                cellNroCajas.Colspan = 1;
                cellNroCajas.Padding = 3;
                cellNroCajas.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellNroCajas.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableTotales.AddCell(cellNroCajas);
                //1-5 VOLUMEN M3
                iTextSharp.text.pdf.PdfPCell cellVolumen = new iTextSharp.text.pdf.PdfPCell(new Phrase("VOLUMEN M3\n" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["VolumenM3_c"].ToString(), fontTitle));
                cellVolumen.Colspan = 1;
                cellVolumen.Padding = 3;
                cellVolumen.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellVolumen.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableTotales.AddCell(cellVolumen);
                //1-6 SUBTOTALES Tittle
                string SubTotales = "SUBTOTAL\nFletes Exportacion\n\n\n";
                iTextSharp.text.pdf.PdfPCell cellSubTotalesTittle = new iTextSharp.text.pdf.PdfPCell(new Phrase(SubTotales, fontTitle));
                cellSubTotalesTittle.Colspan = 1;
                cellSubTotalesTittle.Rowspan = 3;
                cellSubTotalesTittle.Padding = 3;
                cellSubTotalesTittle.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellSubTotalesTittle.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableTotales.AddCell(cellSubTotalesTittle);

                //1-7 SUBTOTALES Values
                decimal SubTotal = Convert.ToDecimal(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubTotal"].ToString());

                string SubTotalesValues = "$" + string.Format("{0:N2}", SubTotal) + "\n" + "$" + string.Format("{0:N2}", TotalMissCharges) + "\n\n\n";
                iTextSharp.text.pdf.PdfPCell cellSubTotalesValues = new iTextSharp.text.pdf.PdfPCell(new Phrase(SubTotalesValues, fontTitle));
                cellSubTotalesValues.Colspan = 1;
                cellSubTotalesValues.Rowspan = 3;
                cellSubTotalesValues.Padding = 3;
                cellSubTotalesValues.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellSubTotalesValues.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableTotales.AddCell(cellSubTotalesValues);

                //2-(0-4) REVISO Y FIRMA                
                iTextSharp.text.pdf.PdfPCell cellSubReviso = new iTextSharp.text.pdf.PdfPCell(new Phrase("Revison                                                     Firma Autorizada\n.", fontTitle));
                cellSubReviso.Colspan = 5;
                cellSubReviso.Padding = 3;
                cellSubReviso.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellSubReviso.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableTotales.AddCell(cellSubReviso);
                //2-5 VIA EMBARQUE                
                iTextSharp.text.pdf.PdfPCell cellVia = new iTextSharp.text.pdf.PdfPCell(new Phrase("VIA\n" + DsInvoiceAR.Tables["InvcDtl"].Rows[0]["ShipViaCodeDescription"].ToString(), fontTitle));
                cellVia.Colspan = 1;
                cellVia.Padding = 3;
                cellVia.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellVia.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableTotales.AddCell(cellVia);

                //3-(0-6) OBSERVACIONES               
                string ObsFactura ="OBSERVACIONES\n" + 
                                   DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceComment"].ToString()+"\n\n"+
                                   "Nro. Interno: "+DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceNum"].ToString()+"                                                      Embarque: "+DsInvoiceAR.Tables["InvcHead"].Rows[0]["PackSlipNum"].ToString();

                iTextSharp.text.pdf.PdfPCell cellObservaciones = new iTextSharp.text.pdf.PdfPCell(new Phrase(ObsFactura, fontTitle));
                cellObservaciones.Colspan = 6;
                cellObservaciones.Padding = 3;
                cellObservaciones.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellObservaciones.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableTotales.AddCell(cellObservaciones);

                //4-(0-6) CERTIFICADOS   
                string Certificatos = "CERTIFICAMOS, BAJO GRAVEDAD DE JURAMENTO QUE LOS\n" +
                                      "PRECIOS CANTIDADES Y DEMAS CONDICIONES, SON LAS\n" +
                                      "CORRECTAS Y PACTADAS CON NUESTRO CLIENTE EN EL EXTERIOR";

                iTextSharp.text.pdf.PdfPCell cellCertificados = new iTextSharp.text.pdf.PdfPCell(new Phrase(Certificatos, fontTitle));
                cellCertificados.Colspan = 6;
                cellCertificados.Padding = 3;
                cellCertificados.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                cellCertificados.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableTotales.AddCell(cellCertificados);

                //4-6 TOTAL USD             
                decimal Total = Convert.ToDecimal(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"].ToString());                
                iTextSharp.text.pdf.PdfPCell cellTotalUSD = new iTextSharp.text.pdf.PdfPCell(new Phrase("TOTAL USD                   $" + string.Format("{0:N2}", Total), fontTitle));
                cellTotalUSD.Colspan = 2;
                cellTotalUSD.Padding = 3;
                cellTotalUSD.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellTotalUSD.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableTotales.AddCell(cellTotalUSD);

                tableTotales.HeadersInEvent = true;
                tableTotales.CompleteRow();

                iTextSharp.text.pdf.PdfPCell celTotales = new iTextSharp.text.pdf.PdfPCell(tableTotales);
                celTotales.Colspan = 7;
                celTotales.Padding = 3;
                celTotales.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                celTotales.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableInvoice.AddCell(celTotales);

                //Pie de pagina.
                string Footer = "SEGUN LA LEY 1231 DE 2008 DESPUÉS DE 10 DÍAS DE RECIBIDA ESTA FACTURA SE ENTIENDE COMO ACEPTADA\n" +
                                "RESOLUCION DE LA DIAN No. 11000066 DE 2016/01/18 DEL CX 6001 AL 8000 AUTORIZA";
                iTextSharp.text.pdf.PdfPCell cellFooter = new iTextSharp.text.pdf.PdfPCell(new Phrase(Footer, fontTitle));
                cellFooter.Colspan = 7;
                cellFooter.Padding = 3;
                cellFooter.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                cellFooter.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableInvoice.AddCell(cellFooter);  

                tableInvoice.HeadersInEvent = true;
                tableInvoice.CompleteRow();

                document.Add(tableInvoice);
                document.Add(separator);
                document.Add(separator);

                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();
                //RutaPdf = Ruta + NomArchivo;
                RutaPdf = NomArchivo;
                return true;
            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.Message;
                return false;
            }
        }

        private bool AddColumnsInvoiceLineALICO(ref iTextSharp.text.pdf.PdfPTable table)
        {
            try
            {
                iTextSharp.text.Font fuenteEncabezado = FontFactory.GetFont(FontFactory.HELVETICA_BOLD,6, iTextSharp.text.Font.NORMAL);
                //iTextSharp.text.pdf.PdfPCell celda0 = new iTextSharp.text.pdf.PdfPCell(new Phrase("Linea", fuenteEncabezado));
                //celda0.Colspan = 1;
                //celda0.Padding = 3;
                //celda0.BackgroundColor = iTextSharp.text.pdf.ExtendedColor.LIGHT_GRAY;
                //celda0.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                //celda0.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                //table.AddCell(celda0);

                iTextSharp.text.pdf.PdfPCell celda1 = new iTextSharp.text.pdf.PdfPCell(new Phrase("REFERENCIA", fuenteEncabezado));
                celda1.Colspan = 1;
                celda1.Padding = 3;
                celda1.BackgroundColor = iTextSharp.text.pdf.ExtendedColor.LIGHT_GRAY;
                celda1.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celda1.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                table.AddCell(celda1);

                iTextSharp.text.pdf.PdfPCell celda2 = new iTextSharp.text.pdf.PdfPCell(new Phrase("DESCRIPCIÓN", fuenteEncabezado));
                celda2.Colspan = 1;
                celda2.Padding = 3;
                celda2.BackgroundColor = iTextSharp.text.pdf.ExtendedColor.LIGHT_GRAY;
                celda2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celda2.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                table.AddCell(celda2);

                iTextSharp.text.pdf.PdfPCell celda3 = new iTextSharp.text.pdf.PdfPCell(new Phrase("CANTIDAD", fuenteEncabezado));
                celda3.Colspan = 1;
                celda3.Padding = 3;
                celda3.BackgroundColor = iTextSharp.text.pdf.ExtendedColor.LIGHT_GRAY;
                celda3.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celda3.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                table.AddCell(celda3);

                iTextSharp.text.pdf.PdfPCell celda4 = new iTextSharp.text.pdf.PdfPCell(new Phrase("UNID", fuenteEncabezado));
                celda4.Colspan = 1;
                celda4.Padding = 3;
                celda4.BackgroundColor = iTextSharp.text.pdf.ExtendedColor.LIGHT_GRAY;
                celda4.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celda4.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                table.AddCell(celda4);

                iTextSharp.text.pdf.PdfPCell celda5 = new iTextSharp.text.pdf.PdfPCell(new Phrase("V/R UNITARIO", fuenteEncabezado));
                celda5.Colspan = 1;
                celda5.Padding = 3;
                celda5.BackgroundColor = iTextSharp.text.pdf.ExtendedColor.LIGHT_GRAY;
                celda5.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celda5.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                table.AddCell(celda5);

                iTextSharp.text.pdf.PdfPCell celda6 = new iTextSharp.text.pdf.PdfPCell(new Phrase("VALOR TOTAL", fuenteEncabezado));
                celda6.Colspan = 1;
                celda6.Padding = 3;
                celda6.BackgroundColor = iTextSharp.text.pdf.ExtendedColor.LIGHT_GRAY;
                celda6.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celda6.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                table.AddCell(celda6);

                return true;
            }
            catch (Exception ex)
            {
                strError = "Error al crear columnas de tabla Items: " + ex.Message;
                return false;
            }
        }

        private bool AddColumnsInvoiceLineExportacionALICO(ref iTextSharp.text.pdf.PdfPTable table)
        {
            try
            {
                iTextSharp.text.Font fuenteEncabezado = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 6, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.pdf.PdfPCell celda0 = new iTextSharp.text.pdf.PdfPCell(new Phrase("ITEM", fuenteEncabezado));
                celda0.Colspan = 1;
                celda0.Padding = 3;
                celda0.BackgroundColor = iTextSharp.text.pdf.ExtendedColor.LIGHT_GRAY;
                celda0.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celda0.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                table.AddCell(celda0);

                iTextSharp.text.pdf.PdfPCell celda1 = new iTextSharp.text.pdf.PdfPCell(new Phrase("REFERENCIA", fuenteEncabezado));
                celda1.Colspan = 1;
                celda1.Padding = 3;
                celda1.BackgroundColor = iTextSharp.text.pdf.ExtendedColor.LIGHT_GRAY;
                celda1.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celda1.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                table.AddCell(celda1);

                iTextSharp.text.pdf.PdfPCell celda2 = new iTextSharp.text.pdf.PdfPCell(new Phrase("DESCRIPCIÓN", fuenteEncabezado));
                celda2.Colspan = 1;
                celda2.Padding = 3;
                celda2.BackgroundColor = iTextSharp.text.pdf.ExtendedColor.LIGHT_GRAY;
                celda2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celda2.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                table.AddCell(celda2);

                iTextSharp.text.pdf.PdfPCell celda3 = new iTextSharp.text.pdf.PdfPCell(new Phrase("CANTIDAD", fuenteEncabezado));
                celda3.Colspan = 1;
                celda3.Padding = 3;
                celda3.BackgroundColor = iTextSharp.text.pdf.ExtendedColor.LIGHT_GRAY;
                celda3.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celda3.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                table.AddCell(celda3);

                iTextSharp.text.pdf.PdfPCell celda4 = new iTextSharp.text.pdf.PdfPCell(new Phrase("UNID.", fuenteEncabezado));
                celda4.Colspan = 1;
                celda4.Padding = 3;
                celda4.BackgroundColor = iTextSharp.text.pdf.ExtendedColor.LIGHT_GRAY;
                celda4.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celda4.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                table.AddCell(celda4);

                iTextSharp.text.pdf.PdfPCell celda5 = new iTextSharp.text.pdf.PdfPCell(new Phrase("V/R UNITARIO", fuenteEncabezado));
                celda5.Colspan = 1;
                celda5.Padding = 3;
                celda5.BackgroundColor = iTextSharp.text.pdf.ExtendedColor.LIGHT_GRAY;
                celda5.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celda5.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                table.AddCell(celda5);

                iTextSharp.text.pdf.PdfPCell celda6 = new iTextSharp.text.pdf.PdfPCell(new Phrase("VALOR TOTAL", fuenteEncabezado));
                celda6.Colspan = 1;
                celda6.Padding = 3;
                celda6.BackgroundColor = iTextSharp.text.pdf.ExtendedColor.LIGHT_GRAY;
                celda6.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celda6.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                table.AddCell(celda6);

                return true;
            }
            catch (Exception ex)
            {
                strError = "Error al crear columnas de tabla Items: " + ex.Message;
                return false;
            }
        }

        private bool AddRowInvoiceALICO(DataRow InvoiceLine, ref iTextSharp.text.pdf.PdfPTable table, iTextSharp.text.Font fuenteDatos, DataTable DtInvcTax)
        {
            try
            {
                //iTextSharp.text.pdf.PdfPCell celLinea = new iTextSharp.text.pdf.PdfPCell(new Phrase(InvoiceLine["InvoiceLine"].ToString(), fuenteDatos));
                //celLinea.Colspan = 1;
                //celLinea.Padding = 3;
                //celLinea.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                //celLinea.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                //table.AddCell(celLinea);

                //REFERENCIA
                iTextSharp.text.pdf.PdfPCell celCodigo = new iTextSharp.text.pdf.PdfPCell(new Phrase(InvoiceLine["PartNum"].ToString(), fuenteDatos));
                celCodigo.Colspan = 1;
                celCodigo.Padding = 3;
                celCodigo.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celCodigo.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                celCodigo.Border = 0;
                table.AddCell(celCodigo);

                //DESCRIPCION
                iTextSharp.text.pdf.PdfPCell celDesc = new iTextSharp.text.pdf.PdfPCell(new Phrase(InvoiceLine["LineDesc"].ToString(), fuenteDatos));
                celDesc.Colspan = 1;
                celDesc.Padding = 3;
                celDesc.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                celDesc.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                celDesc.Border = 0;
                table.AddCell(celDesc);

                //CANTIDAD
                decimal Qty = Convert.ToDecimal(InvoiceLine["SellingShipQty"].ToString());
                iTextSharp.text.pdf.PdfPCell celCant = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:N0}", Qty), fuenteDatos));
                celCant.Colspan = 1;
                celCant.Padding = 3;
                celCant.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celCant.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                celCant.Border = 0;
                table.AddCell(celCant);

                //decimal Percent = Convert.ToDecimal(GetPercentIdImpDIAN(InvoiceLine["InvoiceNum"].ToString(), InvoiceLine["InvoiceLine"].ToString(), DtInvcTax));
                string UOM = InvoiceLine["SalesUM"].ToString();
                iTextSharp.text.pdf.PdfPCell celIva = new iTextSharp.text.pdf.PdfPCell(new Phrase(UOM, fuenteDatos));
                celIva.Colspan = 1;
                celIva.Padding = 3;
                celIva.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celIva.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                celIva.Border = 0;
                table.AddCell(celIva);

                decimal UnitPrice =Convert.ToDecimal(InvoiceLine["DocUnitPrice"].ToString());
                iTextSharp.text.pdf.PdfPCell celDcto = new iTextSharp.text.pdf.PdfPCell(new Phrase("$" + string.Format("{0:N2}", UnitPrice), fuenteDatos));
                celDcto.Colspan = 1;
                celDcto.Padding = 3;
                celDcto.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celDcto.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                celDcto.Border = 0;
                table.AddCell(celDcto);

                decimal PriceLine = Convert.ToDecimal(InvoiceLine["DocExtPrice"].ToString());
                iTextSharp.text.pdf.PdfPCell celExtPrise = new iTextSharp.text.pdf.PdfPCell(new Phrase("$" + string.Format("{0:N2}", PriceLine), fuenteDatos));
                celExtPrise.Colspan = 1;
                celExtPrise.Padding = 3;
                celExtPrise.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celExtPrise.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                celExtPrise.Border = 0;
                table.AddCell(celExtPrise);

                return true;
            }
            catch (Exception ex)
            {
                strError = "Error addLineInvoice: " + ex.Message;
                return false;
            }
        }

        private bool AddRowMissInvoiceALICO(DataRow InvoiceLineMiss, ref iTextSharp.text.pdf.PdfPTable table, iTextSharp.text.Font fuenteDatos, ref decimal TotalMissCharges)
        {
            try
            {                
                //REFERENCIA
                iTextSharp.text.pdf.PdfPCell celCodigo = new iTextSharp.text.pdf.PdfPCell(new Phrase("", fuenteDatos));
                celCodigo.Colspan = 1;
                celCodigo.Padding = 3;
                celCodigo.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celCodigo.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                celCodigo.Border = 0;
                table.AddCell(celCodigo);

                //DESCRIPCION
                iTextSharp.text.pdf.PdfPCell celDesc = new iTextSharp.text.pdf.PdfPCell(new Phrase(InvoiceLineMiss["Description"].ToString(), fuenteDatos));
                celDesc.Colspan = 1;
                celDesc.Padding = 3;
                celDesc.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                celDesc.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                celDesc.Border = 0;
                table.AddCell(celDesc);

                //CANTIDAD
                //decimal Qty = Convert.ToDecimal(InvoiceLine["SellingShipQty"].ToString());
                iTextSharp.text.pdf.PdfPCell celCant = new iTextSharp.text.pdf.PdfPCell(new Phrase("", fuenteDatos));
                celCant.Colspan = 1;
                celCant.Padding = 3;
                celCant.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celCant.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                celCant.Border = 0;
                table.AddCell(celCant);

                //decimal Percent = Convert.ToDecimal(GetPercentIdImpDIAN(InvoiceLine["InvoiceNum"].ToString(), InvoiceLine["InvoiceLine"].ToString(), DtInvcTax));
                //string UOM = InvoiceLine["SalesUM"].ToString();
                iTextSharp.text.pdf.PdfPCell celIva = new iTextSharp.text.pdf.PdfPCell(new Phrase("", fuenteDatos));
                celIva.Colspan = 1;
                celIva.Padding = 3;
                celIva.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celIva.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                celIva.Border = 0;
                table.AddCell(celIva);

                //decimal UnitPrice = Convert.ToDecimal(InvoiceLine["DocUnitPrice"].ToString());
                iTextSharp.text.pdf.PdfPCell celDcto = new iTextSharp.text.pdf.PdfPCell(new Phrase("", fuenteDatos));
                celDcto.Colspan = 1;
                celDcto.Padding = 3;
                celDcto.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celDcto.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                celDcto.Border = 0;
                table.AddCell(celDcto);

                decimal PriceLine = Convert.ToDecimal(InvoiceLineMiss["DocMiscAmt"].ToString());

                if (InvoiceLineMiss["Description"].ToString().Contains("FLETES"))
                    TotalMissCharges += PriceLine;
                
                iTextSharp.text.pdf.PdfPCell celExtPrise = new iTextSharp.text.pdf.PdfPCell(new Phrase("$" + string.Format("{0:N2}", PriceLine), fuenteDatos));
                celExtPrise.Colspan = 1;
                celExtPrise.Padding = 3;
                celExtPrise.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celExtPrise.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                celExtPrise.Border = 0;
                table.AddCell(celExtPrise);

                return true;
            }
            catch (Exception ex)
            {
                strError = "Error addLineInvoice: " + ex.Message;
                return false;
            }
        }

        private bool AddRowInvoiceExportacionALICO(DataRow InvoiceLine, ref iTextSharp.text.pdf.PdfPTable table, iTextSharp.text.Font fuenteDatos, DataTable DtInvcTax)
        {
            try
            {
                //ITEM
                iTextSharp.text.pdf.PdfPCell celLinea = new iTextSharp.text.pdf.PdfPCell(new Phrase(InvoiceLine["InvoiceLine"].ToString(), fuenteDatos));
                celLinea.Colspan = 1;
                celLinea.Padding = 3;
                celLinea.Border = 0;
                celLinea.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celLinea.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                table.AddCell(celLinea);

                //REFERENCIA
                iTextSharp.text.pdf.PdfPCell celCodigo = new iTextSharp.text.pdf.PdfPCell(new Phrase(InvoiceLine["PartNum"].ToString(), fuenteDatos));
                celCodigo.Colspan = 1;
                celCodigo.Padding = 3;
                celCodigo.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celCodigo.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                celCodigo.Border = 0;
                table.AddCell(celCodigo);

                //DESCRIPCION
                iTextSharp.text.pdf.PdfPCell celDesc = new iTextSharp.text.pdf.PdfPCell(new Phrase(InvoiceLine["LineDesc"].ToString(), fuenteDatos));
                celDesc.Colspan = 1;
                celDesc.Padding = 3;
                celDesc.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                celDesc.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                celDesc.Border = 0;
                table.AddCell(celDesc);

                //CANTIDAD
                decimal Qty = Convert.ToDecimal(InvoiceLine["SellingShipQty"].ToString());
                iTextSharp.text.pdf.PdfPCell celCant = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:N0}", Qty), fuenteDatos));
                celCant.Colspan = 1;
                celCant.Padding = 3;
                celCant.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celCant.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                celCant.Border = 0;
                table.AddCell(celCant);

                //decimal Percent = Convert.ToDecimal(GetPercentIdImpDIAN(InvoiceLine["InvoiceNum"].ToString(), InvoiceLine["InvoiceLine"].ToString(), DtInvcTax));
                string UOM = InvoiceLine["IUM"].ToString();
                iTextSharp.text.pdf.PdfPCell celIva = new iTextSharp.text.pdf.PdfPCell(new Phrase(UOM, fuenteDatos));
                celIva.Colspan = 1;
                celIva.Padding = 3;
                celIva.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celIva.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                celIva.Border = 0;
                table.AddCell(celIva);

                decimal UnitPrice = Convert.ToDecimal(InvoiceLine["DocUnitPrice"].ToString());
                iTextSharp.text.pdf.PdfPCell celDcto = new iTextSharp.text.pdf.PdfPCell(new Phrase("$" + string.Format("{0:N2}", UnitPrice), fuenteDatos));
                celDcto.Colspan = 1;
                celDcto.Padding = 3;
                celDcto.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celDcto.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                celDcto.Border = 0;
                table.AddCell(celDcto);

                decimal PriceLine = Convert.ToDecimal(InvoiceLine["DocExtPrice"].ToString());
                iTextSharp.text.pdf.PdfPCell celExtPrise = new iTextSharp.text.pdf.PdfPCell(new Phrase("$" + string.Format("{0:N2}", PriceLine), fuenteDatos));
                celExtPrise.Colspan = 1;
                celExtPrise.Padding = 3;
                celExtPrise.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celExtPrise.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                celExtPrise.Border = 0;
                table.AddCell(celExtPrise);

                return true;
            }
            catch (Exception ex)
            {
                strError = "Error addLineInvoice: " + ex.Message;
                return false;
            }
        }

        private bool AddTotalesInvoiceaLICO(ref iTextSharp.text.pdf.PdfPTable tableTotales, DataSet DsInvoiceAR,decimal TotalDiscount,decimal TotalMissCharges)
        {
            decimal SubTotal = 0;
            decimal Iva = 0;
            decimal ImpConsBolsa = 0;
            string Curency;
            decimal Retenciones = 0;
            decimal Total = 0;

            try
            {
                iTextSharp.text.Font fuenteTitle = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 8, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fuenteDatos = FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL);

                //SubTotal
                PdfPCell cellSubTotal = new PdfPCell(new Phrase("SUBTOTAL", fuenteTitle));
                cellSubTotal.Colspan = 2;
                cellSubTotal.HorizontalAlignment = Element.ALIGN_LEFT;
                tableTotales.AddCell(cellSubTotal);

                SubTotal = Convert.ToDecimal(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubTotal"].ToString());
                PdfPCell cellSubTotalValue = new PdfPCell(new Phrase("$" + string.Format("{0:N2}", SubTotal), fuenteDatos));
                cellSubTotalValue.HorizontalAlignment = Element.ALIGN_RIGHT;
                tableTotales.AddCell(cellSubTotalValue);

                //IVA
                PdfPCell cellIva = new PdfPCell(new Phrase("IVA +\nIMPTO CONS (B.P)", fuenteTitle));
                cellIva.Colspan = 2;
                cellIva.HorizontalAlignment = Element.ALIGN_LEFT;
                tableTotales.AddCell(cellIva);

                //Iva = Convert.ToDecimal(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DocTaxAmt"].ToString());

                Iva = GetValImpuestoByID("01", DsInvoiceAR);
                ImpConsBolsa = GetValImpuestoByID("02", DsInvoiceAR);

                PdfPCell cellIvaValue = new PdfPCell(new Phrase("$" + string.Format("{0:N2}", Iva) + "\n" + "$" + string.Format("{0:N2}", ImpConsBolsa), fuenteDatos));
                cellIvaValue.HorizontalAlignment = Element.ALIGN_RIGHT;
                tableTotales.AddCell(cellIvaValue);

                //Retenciones
                PdfPCell cellReteIva = new PdfPCell(new Phrase("RETENCIONES", fuenteTitle));
                cellReteIva.Colspan = 2;
                cellReteIva.HorizontalAlignment = Element.ALIGN_LEFT;
                tableTotales.AddCell(cellReteIva);                

                Retenciones = Convert.ToDecimal(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DocWHTaxAmt"].ToString());
                PdfPCell cellReteIvaValue = new PdfPCell(new Phrase("$ " + string.Format("{0:N2}", Retenciones), fuenteDatos));
                cellReteIvaValue.HorizontalAlignment = Element.ALIGN_RIGHT;
                tableTotales.AddCell(cellReteIvaValue);

                //Total Descuento
                PdfPCell cellDesc = new PdfPCell(new Phrase("DESCUENTO", fuenteTitle));
                cellDesc.Colspan = 2;
                cellDesc.HorizontalAlignment = Element.ALIGN_LEFT;
                tableTotales.AddCell(cellDesc);

                PdfPCell cellDescValue = new PdfPCell(new Phrase("$ " + string.Format("{0:N2}", TotalDiscount), fuenteDatos));
                cellDescValue.HorizontalAlignment = Element.ALIGN_RIGHT;
                tableTotales.AddCell(cellDescValue);

                //Total FLETES
                PdfPCell cellFlete = new PdfPCell(new Phrase("FLETE", fuenteTitle));
                cellFlete.Colspan = 2;
                cellFlete.HorizontalAlignment = Element.ALIGN_LEFT;
                tableTotales.AddCell(cellFlete);

                PdfPCell cellFleteValue = new PdfPCell(new Phrase("$ " + string.Format("{0:N2}", TotalMissCharges), fuenteDatos));
                cellFleteValue.HorizontalAlignment = Element.ALIGN_RIGHT;
                tableTotales.AddCell(cellFleteValue);

                //TOTAL
                PdfPCell cellTotal = new PdfPCell(new Phrase("TOTAL", fuenteTitle));
                cellTotal.Colspan = 1;
                cellTotal.HorizontalAlignment = Element.ALIGN_LEFT;
                tableTotales.AddCell(cellTotal);

                Curency = DsInvoiceAR.Tables["InvcHead"].Rows[0]["CurrencyCode"].ToString();
                PdfPCell cellCurrency = new PdfPCell(new Phrase(Curency, fuenteDatos));
                cellCurrency.Colspan = 1;
                cellCurrency.HorizontalAlignment = Element.ALIGN_LEFT;
                tableTotales.AddCell(cellCurrency);

                Total = Convert.ToDecimal(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"].ToString());
                PdfPCell cellTotalValue = new PdfPCell(new Phrase(string.Format("{0:N2}", Total), fuenteDatos));
                cellTotalValue.Colspan = 1;
                cellTotalValue.HorizontalAlignment = Element.ALIGN_RIGHT;
                tableTotales.AddCell(cellTotalValue);

                return true;
            }
            catch (Exception ex)
            {
                strError = "Error al add Totales Facturas: " + ex.Message;
                return false;
            }
        }        

        public string Nroenletras(string num)
        {
            string res, dec = "";
            Int64 entero;
            int decimales;
            double nro;

            try
            {
                nro = Convert.ToDouble(num);
            }
            catch
            {
                return "";
            }

            entero = Convert.ToInt64(Math.Truncate(nro));
            decimales = Convert.ToInt32(Math.Round((nro - entero) * 100, 2));
            if (decimales > 0)
            {
                dec = " CON " + decimales.ToString();
            }
            //else
            //{
            //    dec = " CON 00";
            //}

            res = toText(Convert.ToDouble(entero)) + dec + " PESOS";
            return res;
        }        

        #endregion

        #region "Formatos Factuas TECNAS"

        public bool FacturaNacionalTECNAS(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType)
        {
            NomArchivo = string.Empty;
            string Ruta = HttpContext.Current.Server.MapPath("~/PDF/" + NIT + "/");

            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            string RutaImg = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";

            NomArchivo = NombreInvoice + ".pdf";

            try
            {
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.
                Document document = new Document(iTextSharp.text.PageSize.LETTER);
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;

                // step 3: we open the document
                document.Open();

                /*TABLA FACTURA*/
                iTextSharp.text.pdf.PdfPTable tableInvoice = new iTextSharp.text.pdf.PdfPTable(10);
                tableInvoice.HorizontalAlignment = Element.ALIGN_CENTER;

                //Dimenciones.
                float[] DimencionInvoice = new float[10];
                DimencionInvoice[0] = 3.0F;//
                DimencionInvoice[1] = 3.0F;//
                DimencionInvoice[2] = 3.0F;//
                DimencionInvoice[3] = 3.0F;//
                DimencionInvoice[4] = 3.0F;//
                DimencionInvoice[5] = 3.0F;//                
                DimencionInvoice[6] = 3.0F;//
                DimencionInvoice[7] = 3.0F;//
                DimencionInvoice[8] = 3.0F;//
                DimencionInvoice[9] = 3.0F;//                

                tableInvoice.WidthPercentage = 100;
                tableInvoice.SetWidths(DimencionInvoice);

                //Celda Logo
                System.Drawing.Image Logo;
                var request = WebRequest.Create(RutaImg);

                using (var response = request.GetResponse())
                using (var stream = response.GetResponseStream())
                {
                    Logo = Bitmap.FromStream(stream);
                }

                iTextSharp.text.Image LogoPdf = iTextSharp.text.Image.GetInstance(Logo, BaseColor.WHITE);
                LogoPdf.ScaleAbsolute(60f, 60f);
                iTextSharp.text.Font fontLogo = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);

                string InfoLogo = DsInvoiceAR.Tables["Company"].Rows[0]["Name"] + "\n" +
                                 "NIT" + DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"] + "\n" +
                                 "IVA REGIMEN COMUN";
                Paragraph prgLogo = new Paragraph(InfoLogo, fontLogo);
                prgLogo.Alignment = Element.ALIGN_LEFT;                

                //iTextSharp.text.pdf.PdfPCell celLogoImg = new iTextSharp.text.pdf.PdfPCell(LogoPdf);
                iTextSharp.text.pdf.PdfPCell celLogoImg = new iTextSharp.text.pdf.PdfPCell();
                celLogoImg.AddElement(LogoPdf);
                celLogoImg.AddElement(prgLogo);
                celLogoImg.Colspan = 2;
                celLogoImg.Padding = 3;
                celLogoImg.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celLogoImg.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                celLogoImg.Border = 0;
                celLogoImg.BorderColorRight = BaseColor.WHITE;
                celLogoImg.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableInvoice.AddCell(celLogoImg);

                //Celda Titulo
                string strTitulo1 = "ITAGUI-OF. PRINCIPAL\n" +
                                    "Cra 50G No. 12 Sur 29- A.A 51040\n" +
                                    "PBX. 285 4290 - Fax. 255 3809\n" +
                                    "tecnas@tecnas.com.co\n\n" +
                                    "CALI\n" +
                                    "Cra 1 No. 45 A 71\n" +
                                    "Barrio Popular Tel. 431 3030\n" +
                                    "cali@Tecnas.com.co\n\n" +
                                    "CÚCUTA\n" +
                                    "Av. 0 No. 2N - 08 Barrio LLeras\n" +
                                    "TEL. 577 41 11\n" +
                                    "ventascucuta@tecnas.com.co";
                
                iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.pdf.PdfPCell celTittle1 = new iTextSharp.text.pdf.PdfPCell(new Phrase(strTitulo1, fontTitle));
                celTittle1.Colspan = 2;
                celTittle1.Padding = 3;
                celTittle1.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celTittle1.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                celLogoImg.BackgroundColor = BaseColor.LIGHT_GRAY;
                celTittle1.Border = 0;
                celTittle1.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableInvoice.AddCell(celTittle1);


                string strTitulo2 = "BOGOTÁ D.C - OF.PRINCIPAL\n" +
                                    "Calle 9 No. 58 - 71\n" +
                                    "servicioalclientebogota@tecnas.com.co\n\n" +
                                    "DOS QUEBRADAS\n" +
                                    "Cr 10 # 15 - 45 Bod 2\n" +
                                    "ventaspereira@tecnas.com.co\n\n" +
                                    "BARRANQUILLA\n" +
                                    "Carrera 44 # 76-71\n" +
                                    "Tel. 369 8398 - 369 0339\n" +
                                    "ventasbarranquilla@tecnas.com.co";                                   

                iTextSharp.text.pdf.PdfPCell celTittle2 = new iTextSharp.text.pdf.PdfPCell(new Phrase(strTitulo2, fontTitle));
                celTittle2.Colspan = 2;
                celTittle2.Padding = 3;
                celTittle2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celTittle2.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                celTittle2.Border = 0;
                celTittle2.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableInvoice.AddCell(celTittle2);


                string strTitulo3 = "BUCARAMANGA\n" +
                                    "Cra. 23 No 21 - 30 Barrio San Francisco\n"+
                                    "Tel. 532 9249\n\n"+
                                    "www.tecnas.com.co\n\n" +
                                    "Tipo Doc:" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["TranDocTypeID"].ToString();

                iTextSharp.text.pdf.PdfPCell celTittle3 = new iTextSharp.text.pdf.PdfPCell(new Phrase(strTitulo3, fontTitle));
                celTittle3.Colspan = 2;
                celTittle3.Padding = 3;
                celTittle3.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celTittle3.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                celTittle3.Border = 0;
                celTittle3.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableInvoice.AddCell(celTittle3);

                string TipoFactura = "FACTURA DE VENTA\n\n\n\n\n\n" +
                                     "No. " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"].ToString();

                //Celda LegalNum
                iTextSharp.text.Font fontLegalNum = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.pdf.PdfPCell celLegalNum = new iTextSharp.text.pdf.PdfPCell(new Phrase(TipoFactura, fontLegalNum));
                celLegalNum.Colspan = 2;
                celLegalNum.Padding = 3;
                celLegalNum.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                celLegalNum.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                celLegalNum.Border = 0;
                celLegalNum.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableInvoice.AddCell(celLegalNum);

                /*VENDIDO A*/
                iTextSharp.text.Font fontVendidoA = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.pdf.PdfPTable tableVendidoA = new iTextSharp.text.pdf.PdfPTable(3);
                tableVendidoA.HorizontalAlignment = Element.ALIGN_CENTER;

                //Dimenciones.
                float[] DimencionVendidoA = new float[3];
                DimencionVendidoA[0] = 5.0F;//
                DimencionVendidoA[1] = 5.0F;//
                DimencionVendidoA[2] = 5.0F;//

                tableVendidoA.WidthPercentage = 100;
                tableVendidoA.SetWidths(DimencionVendidoA);

                //1-(1,2) SEÑORES
                iTextSharp.text.pdf.PdfPCell cellSeniores = new iTextSharp.text.pdf.PdfPCell(new Phrase("SEÑORES\n" + DsInvoiceAR.Tables["Customer"].Rows[0]["Name"].ToString(), fontVendidoA));
                cellSeniores.Colspan = 2;
                cellSeniores.Padding = 3;
                cellSeniores.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellSeniores.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableVendidoA.AddCell(cellSeniores);

                //1-3 NIT
                iTextSharp.text.pdf.PdfPCell cellNIT = new iTextSharp.text.pdf.PdfPCell(new Phrase("NIT\n" + DsInvoiceAR.Tables["Customer"].Rows[0]["ResaleID"].ToString(), fontVendidoA));
                cellNIT.Colspan = 1;
                cellNIT.Padding = 3;
                cellNIT.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellNIT.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableVendidoA.AddCell(cellNIT);

                //2-1 Direccion
                iTextSharp.text.pdf.PdfPCell cellDireccion = new iTextSharp.text.pdf.PdfPCell(new Phrase("Dirección:\n" + DsInvoiceAR.Tables["Customer"].Rows[0]["Address1"].ToString(), fontVendidoA));
                cellDireccion.Colspan = 1;
                cellDireccion.Padding = 3;
                cellDireccion.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellDireccion.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableVendidoA.AddCell(cellDireccion);

                //2-2 Tel
                iTextSharp.text.pdf.PdfPCell cellTel = new iTextSharp.text.pdf.PdfPCell(new Phrase("Tel:\n" + DsInvoiceAR.Tables["Customer"].Rows[0]["PhoneNum"].ToString(), fontVendidoA));
                cellTel.Colspan = 1;
                cellTel.Padding = 3;
                cellTel.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellTel.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableVendidoA.AddCell(cellTel);

                //2-3 Planta
                iTextSharp.text.pdf.PdfPCell cellPlanta = new iTextSharp.text.pdf.PdfPCell(new Phrase("Planta:\n" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["Plant"].ToString(), fontVendidoA));
                cellPlanta.Colspan = 1;
                cellPlanta.Padding = 3;
                cellPlanta.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellPlanta.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableVendidoA.AddCell(cellPlanta);

                tableVendidoA.WidthPercentage = 100;
                tableVendidoA.SetWidths(DimencionVendidoA);

                iTextSharp.text.pdf.PdfPCell cellVendidoA = new iTextSharp.text.pdf.PdfPCell(tableVendidoA);
                cellVendidoA.Colspan = 3;
                cellVendidoA.Padding = 3;
                cellVendidoA.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellVendidoA.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                cellVendidoA.Border = 0;             
                tableInvoice.AddCell(cellVendidoA);

                /*INFO PEDIDO*/
                iTextSharp.text.Font fontInfoPedido = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.pdf.PdfPTable tableInfoPedido = new iTextSharp.text.pdf.PdfPTable(3);
                tableInfoPedido.HorizontalAlignment = Element.ALIGN_CENTER;

                //Dimenciones.
                float[] DimencionInfoPedido = new float[3];
                DimencionInfoPedido[0] = 5.0F;//
                DimencionInfoPedido[1] = 5.0F;//
                DimencionInfoPedido[2] = 5.0F;//                

                tableInfoPedido.WidthPercentage = 100;
                tableInfoPedido.SetWidths(DimencionInfoPedido);

                //1-1 Forma de pago.
                iTextSharp.text.pdf.PdfPCell cellFormaPago = new iTextSharp.text.pdf.PdfPCell(new Phrase("Forma de pago:\n" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["TermsCodeDescription"].ToString(), fontVendidoA));
                cellFormaPago.Colspan = 1;
                cellFormaPago.Padding = 3;
                cellFormaPago.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellFormaPago.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableInfoPedido.AddCell(cellFormaPago);

                //1-2 Vendedor
                iTextSharp.text.pdf.PdfPCell cellVendedor = new iTextSharp.text.pdf.PdfPCell(new Phrase("Vendedor:\n" + DsInvoiceAR.Tables["Customer"].Rows[0]["SalesRepName"].ToString(), fontVendidoA));
                cellVendedor.Colspan = 1;
                cellVendedor.Padding = 3;
                cellVendedor.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellVendedor.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableInfoPedido.AddCell(cellVendedor);

                //1-3 Fecha Factura
                iTextSharp.text.pdf.PdfPCell cellInvoiceDate = new iTextSharp.text.pdf.PdfPCell(new Phrase("Fecha Factura:\n" + Convert.ToDateTime(DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).ToString("dd/MM/yyyy"), fontVendidoA));
                cellInvoiceDate.Colspan = 1;
                cellInvoiceDate.Padding = 3;
                cellInvoiceDate.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellInvoiceDate.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableInfoPedido.AddCell(cellInvoiceDate);

                //2-(1,2)  Direccion de Embarque
                iTextSharp.text.pdf.PdfPCell cellDirEmbarque = new iTextSharp.text.pdf.PdfPCell(new Phrase("Dirección Emabarque:\n" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["SoldToAddressList"].ToString(), fontVendidoA));
                cellDirEmbarque.Colspan = 2;
                cellDirEmbarque.Padding = 3;
                cellDirEmbarque.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellDirEmbarque.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableInfoPedido.AddCell(cellDirEmbarque);

                //2-3 Fecha Vencimiento
                iTextSharp.text.pdf.PdfPCell cellDueDate = new iTextSharp.text.pdf.PdfPCell(new Phrase("Fecha Vencimiento:\n" + Convert.ToDateTime(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"]).ToString("dd/MM/yyyy"), fontVendidoA));
                cellDueDate.Colspan = 1;
                cellDueDate.Padding = 3;
                cellDueDate.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellDueDate.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableInfoPedido.AddCell(cellDueDate);

                tableInfoPedido.WidthPercentage = 100;
                tableInfoPedido.SetWidths(DimencionVendidoA);

                iTextSharp.text.pdf.PdfPCell cellInfoPedido = new iTextSharp.text.pdf.PdfPCell(tableInfoPedido);
                cellInfoPedido.Colspan = 5;
                cellInfoPedido.Padding = 3;
                cellInfoPedido.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellInfoPedido.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                cellInfoPedido.Border = 0;
                tableInvoice.AddCell(cellInfoPedido);
                                
                /*CELL QR*/
                iTextSharp.text.Image QRPdf = iTextSharp.text.Image.GetInstance(QRInvoice, BaseColor.WHITE);
                iTextSharp.text.pdf.PdfPCell celImgQR = new iTextSharp.text.pdf.PdfPCell(QRPdf);
                celImgQR.Colspan = 2;
                celImgQR.Padding = 3;
                celImgQR.Border = 0;
                celImgQR.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celImgQR.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableInvoice.AddCell(celImgQR);

                tableInvoice.HeadersInEvent = true;
                tableInvoice.CompleteRow();

                /*Tabla - ITEMS FACTURA.*/
                iTextSharp.text.pdf.PdfPTable tableItems = new iTextSharp.text.pdf.PdfPTable(10);
                tableItems.HorizontalAlignment = Element.ALIGN_CENTER;

                //Dimenciones.
                float[] Dimencion = new float[10];
                Dimencion[0] = 4.0F;//REFERENCIA
                Dimencion[1] = 8.0F;//Descripcion
                Dimencion[2] = 3.0F;//Lote
                Dimencion[3] = 3.0F;//Vence Lote
                Dimencion[4] = 3.0F;//Cantidad
                Dimencion[5] = 3.0F;//UOM         
                Dimencion[6] = 4.0F;//Val Unitario
                Dimencion[7] = 2.0F;//Iva %
                Dimencion[8] = 3.0F;//Dcto %
                Dimencion[9] = 5.0F;//VR TOTAL   

                //Configuracion y creacion de la tabla Comparativa.
                tableItems.WidthPercentage = 100;//writer.DirectContent.PdfDocument.PageSize.Width;
                tableItems.SetWidths(Dimencion);

                //Configuramos las columnas.
                if (!AddColumnsInvoiceLineTECNAS(ref tableItems))
                    return false;

                //Filas Factura
                iTextSharp.text.Font fuenteDatos = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);

                decimal TotalDescountInvc = 0;
                decimal TotalMissCharges = 0;
                string SalesOrder =string.Empty;
                string Embarques = string.Empty;

                foreach (DataRow InvoiceLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
                {
                    //Totales de las Lineas
                    TotalDescountInvc += Convert.ToDecimal(InvoiceLine["DspDocLessDiscount"]);
                    TotalMissCharges += Convert.ToDecimal(InvoiceLine["DspDocTotalMiscChrg"]);

                    //Ordenes de Venta Tecnas en la factura.
                    if(string.IsNullOrEmpty(SalesOrder))
                    {
                        SalesOrder +=InvoiceLine["OrderNum"].ToString();
                    }
                    else
                    {
                        if(!SalesOrder.Contains(InvoiceLine["OrderNum"].ToString()))
                            SalesOrder +=","+InvoiceLine["OrderNum"];                        
                    }

                    //Numeros de Embarque.
                    if(string.IsNullOrEmpty(Embarques))
                    {
                        Embarques +=InvoiceLine["PackNum"].ToString();
                    }
                    else
                    {
                        if(!Embarques.Contains(InvoiceLine["PackNum"].ToString()))
                            Embarques +=","+InvoiceLine["PackNum"];                        
                    }


                    if (!AddRowInvoiceTECNAS(InvoiceLine, ref tableItems, fuenteDatos, DsInvoiceAR.Tables["InvcTax"]))
                        return false;
                }               

                tableItems.HeadersInEvent = true;
                tableItems.CompleteRow();

                //ITEMS FACTURA
                iTextSharp.text.pdf.PdfPCell celItems = new iTextSharp.text.pdf.PdfPCell(tableItems);
                celItems.Colspan = 10;
                celItems.Padding = 3;
                celItems.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                celItems.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                celItems.Border = 0;
                tableInvoice.AddCell(celItems);

                //Embarque y Pedido INFO   
                string EmbarquePedido = "Ordenes de Venta " + SalesOrder + "                                     Remisiones: " + Embarques + "                                                    OcClie:" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["PONum"].ToString() + "                                                   Interno: " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceNum"].ToString();
                iTextSharp.text.pdf.PdfPCell cellEmbarquePedido = new iTextSharp.text.pdf.PdfPCell(new Phrase(EmbarquePedido, fuenteDatos));
                cellEmbarquePedido.Colspan = 10;
                cellEmbarquePedido.Padding = 3;
                cellEmbarquePedido.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellEmbarquePedido.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableInvoice.AddCell(cellEmbarquePedido);

                //OBS FACTURA
                string ObsInvoice = "OBSERVACIONES:\n" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceComment"].ToString();
                iTextSharp.text.pdf.PdfPCell cellObs = new iTextSharp.text.pdf.PdfPCell(new Phrase(ObsInvoice, fuenteDatos));                
                cellObs.Colspan = 6;                
                cellObs.Padding = 3;
                cellObs.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellObs.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableInvoice.AddCell(cellObs);

                /*TOTALES FACTURA*/
                iTextSharp.text.Font fontTotales = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.pdf.PdfPTable tableTotales = new iTextSharp.text.pdf.PdfPTable(2);
                tableTotales.HorizontalAlignment = Element.ALIGN_CENTER;

                //Dimenciones.
                float[] DimencionTotales = new float[2];
                DimencionTotales[0] = 5.0F;//
                DimencionTotales[1] = 5.0F;//                          

                tableTotales.WidthPercentage = 100;
                tableTotales.SetWidths(DimencionTotales);

                //1-1 SUBTOTAL
                iTextSharp.text.pdf.PdfPCell cellSubTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase("SUBTOTAL", fontTotales));
                cellSubTotal.Colspan = 1;
                cellSubTotal.Padding = 3;
                cellSubTotal.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellSubTotal.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableTotales.AddCell(cellSubTotal);

                //1-2 SUBTOTAL VALUE
                decimal SubTotal = Convert.ToDecimal(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubTotal"].ToString())+(TotalDescountInvc*-1);
                iTextSharp.text.pdf.PdfPCell cellSubTotalValue = new iTextSharp.text.pdf.PdfPCell(new Phrase("$" + string.Format("{0:N2}", SubTotal), fontTotales));
                cellSubTotalValue.Colspan = 1;
                cellSubTotalValue.Padding = 3;
                cellSubTotalValue.HorizontalAlignment = iTextSharp.text.Element.ALIGN_RIGHT;
                cellSubTotalValue.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableTotales.AddCell(cellSubTotalValue);

                //2-1 DESCUENTO
                iTextSharp.text.pdf.PdfPCell cellDescuento = new iTextSharp.text.pdf.PdfPCell(new Phrase("DESCUENTO", fontTotales));
                cellDescuento.Colspan = 1;
                cellDescuento.Padding = 3;
                cellDescuento.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellDescuento.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableTotales.AddCell(cellDescuento);

                //2-2 DESCUENTO VALUE
                iTextSharp.text.pdf.PdfPCell cellDescuentoValue = new iTextSharp.text.pdf.PdfPCell(new Phrase("$" + string.Format("{0:N2}", TotalDescountInvc), fontTotales));
                cellDescuentoValue.Colspan = 1;
                cellDescuentoValue.Padding = 3;
                cellDescuentoValue.HorizontalAlignment = iTextSharp.text.Element.ALIGN_RIGHT;
                cellDescuentoValue.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableTotales.AddCell(cellDescuentoValue);

                //3-1 FLETES
                iTextSharp.text.pdf.PdfPCell cellFletes = new iTextSharp.text.pdf.PdfPCell(new Phrase("FLETES", fontTotales));
                cellFletes.Colspan = 1;
                cellFletes.Padding = 3;
                cellFletes.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellFletes.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableTotales.AddCell(cellFletes);

                //3-2 FLETES VALUE
                iTextSharp.text.pdf.PdfPCell cellFletesValue = new iTextSharp.text.pdf.PdfPCell(new Phrase("$" + string.Format("{0:N2}", TotalMissCharges), fontTotales));
                cellFletesValue.Colspan = 1;
                cellFletesValue.Padding = 3;
                cellFletesValue.HorizontalAlignment = iTextSharp.text.Element.ALIGN_RIGHT;
                cellFletesValue.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableTotales.AddCell(cellFletesValue);

                //4-1 IVA
                iTextSharp.text.pdf.PdfPCell cellIVA = new iTextSharp.text.pdf.PdfPCell(new Phrase("IVA", fontTotales));
                cellIVA.Colspan = 1;
                cellIVA.Padding = 3;
                cellIVA.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellIVA.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableTotales.AddCell(cellIVA);

                //4-2 IVA VALUE
                decimal Iva = Convert.ToDecimal(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DocTaxAmt"].ToString());
                iTextSharp.text.pdf.PdfPCell cellIvaValue = new iTextSharp.text.pdf.PdfPCell(new Phrase("$" + string.Format("{0:N2}", Iva), fontTotales));
                cellIvaValue.Colspan = 1;
                cellIvaValue.Padding = 3;
                cellIvaValue.HorizontalAlignment = iTextSharp.text.Element.ALIGN_RIGHT;
                cellIvaValue.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableTotales.AddCell(cellIvaValue);

                //5-1 TOTAL
                iTextSharp.text.pdf.PdfPCell cellTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase("TOTAL", fontTotales));
                cellTotal.Colspan = 1;
                cellTotal.Padding = 3;
                cellTotal.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellTotal.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableTotales.AddCell(cellTotal);

                //5-2 TOTAL VALUE
                decimal Total = Convert.ToDecimal(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"].ToString());
                iTextSharp.text.pdf.PdfPCell cellTotalValue = new iTextSharp.text.pdf.PdfPCell(new Phrase("$" + string.Format("{0:N2}", Total), fontTotales));
                cellTotalValue.Colspan = 1;
                cellTotalValue.Padding = 3;
                cellTotalValue.HorizontalAlignment = iTextSharp.text.Element.ALIGN_RIGHT;
                cellTotalValue.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableTotales.AddCell(cellTotalValue);

                tableTotales.WidthPercentage = 100;
                tableTotales.SetWidths(DimencionTotales);

                //Add table Totales to Invoice Table
                iTextSharp.text.pdf.PdfPCell cellTotales = new iTextSharp.text.pdf.PdfPCell(tableTotales);
                cellTotales.Colspan = 4;
                cellTotales.Rowspan = 2;
                cellTotales.Padding = 3;
                cellTotales.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellTotales.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;                
                cellTotales.BorderWidthLeft = 0;
                cellTotales.BorderColorLeft = BaseColor.WHITE;                
                tableInvoice.AddCell(cellTotales);

                //Valor Letras
                string NumLetras = "";
                NroenletrasTecnas(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"].ToString(),ref NumLetras);

                string ValueInLetters = "VALOR EN PESOS:\n" + NumLetras;
                iTextSharp.text.pdf.PdfPCell cellValPesos = new iTextSharp.text.pdf.PdfPCell(new Phrase(ValueInLetters, fontTotales));
                cellValPesos.Colspan = 6;
                cellValPesos.Padding = 3;
                cellValPesos.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellValPesos.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableInvoice.AddCell(cellValPesos);

                //OBS 2
                string Obs2 =DsInvoiceAR.Tables["InvcHead"].Rows[0]["InfoResolucion"].ToString();
                iTextSharp.text.pdf.PdfPCell cellObs2 = new iTextSharp.text.pdf.PdfPCell(new Phrase(Obs2, fontTotales));
                cellObs2.Colspan = 3;
                cellObs2.Padding = 3;
                cellObs2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellObs2.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableInvoice.AddCell(cellObs2);

                //AUTO RETENEDORES
                string AutoRet = "* SOMOS - AUTO-RETENEDORES DE ICA EN ITAGUI ACUERDO\n" +
                                 "  030 DEL 27 DE DICIEMBRE DE 2012.\n" +
                                 "* SOMOS GRANDES CONTRIBUYENTES Y AGENTES\n" +
                                 "  RETENEDORES DE IVA SEGÚN RESOLUCIÓN No 000076 DE\n" +
                                 "  DICIEMBRE 01/2016.\n" +
                                 "* SOMOS AUTO-RETENEDORES DE RENTA SEGÚN\n" +
                                 "  RESOLUCIÓN No 12204 DICIEMBRE 16/2002.";
                iTextSharp.text.pdf.PdfPCell cellAutoRte = new iTextSharp.text.pdf.PdfPCell(new Phrase(AutoRet, fontTotales));
                cellAutoRte.Colspan = 3;
                cellAutoRte.Padding = 3;
                cellAutoRte.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellAutoRte.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableInvoice.AddCell(cellAutoRte);

                //EMISOR
                iTextSharp.text.Image LogoPdf2 = iTextSharp.text.Image.GetInstance(Logo, BaseColor.WHITE);
                LogoPdf2.ScaleAbsolute(45f, 45f);
                //iTextSharp.text.Font fontLogo = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);

                string InfoLogo2 = "Emisor:";
                Paragraph prgLogo2 = new Paragraph(InfoLogo2, fontLogo);
                prgLogo2.Alignment = Element.ALIGN_LEFT;

                iTextSharp.text.pdf.PdfPCell celEmisor = new iTextSharp.text.pdf.PdfPCell();
                celEmisor.AddElement(prgLogo2);
                celEmisor.AddElement(LogoPdf2);
                celEmisor.Colspan = 1;
                celEmisor.Padding = 3;
                celEmisor.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celEmisor.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                celEmisor.BorderWidthRight = 0;
                celEmisor.BorderColorRight = BaseColor.WHITE;
                tableInvoice.AddCell(celEmisor);

                //Aceptacion               
                iTextSharp.text.pdf.PdfPCell cellAceptacion = new iTextSharp.text.pdf.PdfPCell(new Phrase("Aceptacion:", fontTotales));
                cellAceptacion.Colspan = 1;
                cellAceptacion.Padding = 3;
                cellAceptacion.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellAceptacion.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableInvoice.AddCell(cellAceptacion);

                //Cecibo de Mercancia               
                iTextSharp.text.pdf.PdfPCell cellRecibido = new iTextSharp.text.pdf.PdfPCell(new Phrase("Recibido de Mcia y\nFactura:", fontTotales));
                cellRecibido.Colspan = 2;
                cellRecibido.Padding = 3;
                cellRecibido.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellRecibido.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableInvoice.AddCell(cellRecibido);

                //Footer   
                string Foorter = "IMPRESO POR: " + DsInvoiceAR.Tables["Company"].Rows[0]["Name"] + "                                                                     ORIGINAL                                                                  Pagina " + document.PageNumber;
                iTextSharp.text.pdf.PdfPCell cellFooter = new iTextSharp.text.pdf.PdfPCell(new Phrase(Foorter, fontTotales));
                cellFooter.Colspan = 10;
                cellFooter.Padding = 3;
                cellFooter.HorizontalAlignment = iTextSharp.text.Element.ALIGN_JUSTIFIED_ALL;
                cellFooter.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableInvoice.AddCell(cellFooter);
                
                document.Add(tableInvoice);
                document.Add(separator);
                document.Add(separator);

                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();
                //RutaPdf = Ruta + NomArchivo;
                RutaPdf = NomArchivo;
                return true;
            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.Message;
                return false;
            }
        }

        public bool NotaCreditoTECNAS(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType)
        {
            NomArchivo = string.Empty;
            string Ruta = HttpContext.Current.Server.MapPath("~/PDF/" + NIT + "/");

            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            string RutaImg = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";

            NomArchivo = NombreInvoice + ".pdf";

            try
            {
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.
                Document document = new Document(iTextSharp.text.PageSize.LETTER);
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;

                // step 3: we open the document
                document.Open();

                /*TABLA FACTURA*/
                iTextSharp.text.pdf.PdfPTable tableInvoice = new iTextSharp.text.pdf.PdfPTable(10);
                tableInvoice.HorizontalAlignment = Element.ALIGN_CENTER;

                //Dimenciones.
                float[] DimencionInvoice = new float[10];
                DimencionInvoice[0] = 3.0F;//
                DimencionInvoice[1] = 3.0F;//
                DimencionInvoice[2] = 3.0F;//
                DimencionInvoice[3] = 3.0F;//
                DimencionInvoice[4] = 3.0F;//
                DimencionInvoice[5] = 3.0F;//                
                DimencionInvoice[6] = 3.0F;//
                DimencionInvoice[7] = 3.0F;//
                DimencionInvoice[8] = 3.0F;//
                DimencionInvoice[9] = 3.0F;//                

                tableInvoice.WidthPercentage = 100;
                tableInvoice.SetWidths(DimencionInvoice);

                //Celda Logo
                System.Drawing.Image Logo;
                var request = WebRequest.Create(RutaImg);

                using (var response = request.GetResponse())
                using (var stream = response.GetResponseStream())
                {
                    Logo = Bitmap.FromStream(stream);
                }

                iTextSharp.text.Image LogoPdf = iTextSharp.text.Image.GetInstance(Logo, BaseColor.WHITE);
                LogoPdf.ScaleAbsolute(60f, 60f);
                iTextSharp.text.Font fontLogo = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);

                string InfoLogo = DsInvoiceAR.Tables["Company"].Rows[0]["Name"] + "\n" +
                                 "NIT " + DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"] + "\n" +
                                 "IVA REGIMEN COMUN";
                Paragraph prgLogo = new Paragraph(InfoLogo, fontLogo);
                prgLogo.Alignment = Element.ALIGN_LEFT;

                //iTextSharp.text.pdf.PdfPCell celLogoImg = new iTextSharp.text.pdf.PdfPCell(LogoPdf);
                iTextSharp.text.pdf.PdfPCell celLogoImg = new iTextSharp.text.pdf.PdfPCell();
                celLogoImg.AddElement(LogoPdf);
                celLogoImg.AddElement(prgLogo);
                celLogoImg.Colspan = 2;
                celLogoImg.Padding = 3;
                celLogoImg.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celLogoImg.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                celLogoImg.Border = 0;
                celLogoImg.BorderColorRight = BaseColor.WHITE;
                celLogoImg.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableInvoice.AddCell(celLogoImg);

                //Celda Titulo
                string strTitulo1 = "ITAGUI-OF. PRINCIPAL\n" +
                                    "Cra 50G No. 12 Sur 29- A.A 51040\n" +
                                    "PBX. 285 4290 - Fax. 255 3809\n" +
                                    "tecnas@tecnas.com.co\n\n" +
                                    "CALI\n" +
                                    "Cra 1 No. 45 A 71\n" +
                                    "Barrio Popular Tel. 431 3030\n" +
                                    "cali@Tecnas.com.co\n\n" +
                                    "CÚCUTA\n" +
                                    "Av. 0 No. 2N - 08 Barrio LLeras\n" +
                                    "TEL. 577 41 11\n" +
                                    "ventascucuta@tecnas.com.co";

                iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.pdf.PdfPCell celTittle1 = new iTextSharp.text.pdf.PdfPCell(new Phrase(strTitulo1, fontTitle));
                celTittle1.Colspan = 2;
                celTittle1.Padding = 3;
                celTittle1.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celTittle1.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                celLogoImg.BackgroundColor = BaseColor.LIGHT_GRAY;
                celTittle1.Border = 0;
                celTittle1.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableInvoice.AddCell(celTittle1);

                string strTitulo2 = "BOGOTÁ D.C - OF.PRINCIPAL\n" +
                                    "Calle 9 No. 58 - 71\n" +
                                    "servicioalclientebogota@tecnas.com.co\n\n" +
                                    "DOS QUEBRADAS\n" +
                                    "Cr 10 # 15 - 45 Bod 2\n" +
                                    "ventaspereira@tecnas.com.co\n\n" +
                                    "BARRANQUILLA\n" +
                                    "Carrera 44 # 76-71\n" +
                                    "Tel. 369 8398 - 369 0339\n" +
                                    "ventasbarranquilla@tecnas.com.co";

                iTextSharp.text.pdf.PdfPCell celTittle2 = new iTextSharp.text.pdf.PdfPCell(new Phrase(strTitulo2, fontTitle));
                celTittle2.Colspan = 2;
                celTittle2.Padding = 3;
                celTittle2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celTittle2.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                celTittle2.Border = 0;
                celTittle2.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableInvoice.AddCell(celTittle2);

                string strTitulo3 = "BUCARAMANGA\n" +
                                    "Cra. 23 No 21 - 30 Barrio San Francisco\n" +
                                    "Tel. 532 9249\n\n" +
                                    "WWW.tecnas.com.co";

                iTextSharp.text.pdf.PdfPCell celTittle3 = new iTextSharp.text.pdf.PdfPCell(new Phrase(strTitulo3, fontTitle));
                celTittle3.Colspan = 2;
                celTittle3.Padding = 3;
                celTittle3.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celTittle3.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                celTittle3.Border = 0;
                celTittle3.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableInvoice.AddCell(celTittle3);

                string TipoFactura = "NOTA CREDITO\n\n\n\n\n\n" +
                                     "No. " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"].ToString();

                //Celda LegalNum
                iTextSharp.text.Font fontLegalNum = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.pdf.PdfPCell celLegalNum = new iTextSharp.text.pdf.PdfPCell(new Phrase(TipoFactura, fontLegalNum));
                celLegalNum.Colspan = 2;
                celLegalNum.Padding = 3;
                celLegalNum.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                celLegalNum.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                celLegalNum.Border = 0;
                celLegalNum.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableInvoice.AddCell(celLegalNum);

                /*VENDIDO A*/
                iTextSharp.text.Font fontVendidoA = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);               
                string VendidoA = "SEÑORES: " + DsInvoiceAR.Tables["Customer"].Rows[0]["Name"].ToString() + "\n" +
                                "NIT: " + DsInvoiceAR.Tables["Customer"].Rows[0]["ResaleID"].ToString();

                iTextSharp.text.pdf.PdfPCell cellVendidoA = new iTextSharp.text.pdf.PdfPCell(new Phrase(VendidoA, fontVendidoA));
                cellVendidoA.Colspan = 3;
                cellVendidoA.Padding = 3;
                cellVendidoA.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellVendidoA.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                cellVendidoA.Border = 0;
                tableInvoice.AddCell(cellVendidoA);

                /*INFO PEDIDO*/
                iTextSharp.text.Font fontInfoPedido = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);                
                string InfoPedido = "Fecha: " + Convert.ToDateTime(DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).ToString("dd/MM/yyyy") + "\n" +
                                    "Número interno: " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceNum"].ToString();

                iTextSharp.text.pdf.PdfPCell cellInfoPedido = new iTextSharp.text.pdf.PdfPCell(new Phrase(InfoPedido, fontInfoPedido));
                cellInfoPedido.Colspan = 5;
                cellInfoPedido.Padding = 3;
                cellInfoPedido.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellInfoPedido.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                cellInfoPedido.Border = 0;
                tableInvoice.AddCell(cellInfoPedido);                

                /*CELL QR*/
                iTextSharp.text.Image QRPdf = iTextSharp.text.Image.GetInstance(QRInvoice, BaseColor.WHITE);
                iTextSharp.text.pdf.PdfPCell celImgQR = new iTextSharp.text.pdf.PdfPCell(QRPdf);
                celImgQR.Colspan = 2;
                celImgQR.Padding = 3;
                celImgQR.Border = 0;
                celImgQR.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celImgQR.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableInvoice.AddCell(celImgQR);

                /*VALOR EN LETRAS Y POR CONCEPTO DE*/
                string NumLetras = "";
                NroenletrasTecnas(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"].ToString(), ref NumLetras);

                string NroEnLetras = "En la fecha hemos abonado a su apreciable cuenta la suma de:\n" + NumLetras + "\n\n" +
                                     "Por concepto de:" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceComment"].ToString();

                iTextSharp.text.pdf.PdfPCell CellNroLetras = new iTextSharp.text.pdf.PdfPCell(new Phrase(NroEnLetras, fontInfoPedido));
                CellNroLetras.Colspan = 10;
                CellNroLetras.Padding = 3;
                CellNroLetras.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                CellNroLetras.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                CellNroLetras.Border = 0;
                tableInvoice.AddCell(CellNroLetras);

                tableInvoice.HeadersInEvent = true;
                tableInvoice.CompleteRow();

                /*Tabla - ITEMS FACTURA.*/
                iTextSharp.text.pdf.PdfPTable tableItems = new iTextSharp.text.pdf.PdfPTable(5);
                tableItems.HorizontalAlignment = Element.ALIGN_CENTER;

                //Dimenciones.
                float[] Dimencion = new float[5];
                Dimencion[0] = 4.0F;//REFERENCIA
                Dimencion[1] = 8.0F;//Descripcion                                
                Dimencion[2] = 3.0F;//Cantidad                
                Dimencion[3] = 4.0F;//Val Unitario                
                Dimencion[4] = 5.0F;//VR TOTAL   

                //Configuracion y creacion de la tabla Comparativa.
                tableItems.WidthPercentage = 100;//writer.DirectContent.PdfDocument.PageSize.Width;
                tableItems.SetWidths(Dimencion);

                //Configuramos las columnas.
                if (!AddColumnsCreditNoteLineTECNAS(ref tableItems))
                    return false;

                //Filas Factura
                iTextSharp.text.Font fuenteDatos = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);

                decimal TotalDescountInvc = 0;
                decimal TotalMissCharges = 0;
                foreach (DataRow InvoiceLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
                {
                    TotalDescountInvc += Convert.ToDecimal(InvoiceLine["DspDocLessDiscount"]);
                    TotalMissCharges += Convert.ToDecimal(InvoiceLine["DspDocTotalMiscChrg"]);

                    if (!AddRowCreditNoteTECNAS(InvoiceLine, ref tableItems, fuenteDatos, DsInvoiceAR.Tables["InvcTax"]))
                        return false;
                }

                tableItems.HeadersInEvent = true;
                tableItems.CompleteRow();

                //ITEMS FACTURA
                iTextSharp.text.pdf.PdfPCell celItems = new iTextSharp.text.pdf.PdfPCell(tableItems);
                celItems.Colspan = 10;
                celItems.Padding = 3;
                celItems.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                celItems.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                celItems.Border = 0;
                tableInvoice.AddCell(celItems);

                //OBS FACTURA
                string ObsInvoice = "ELABORÓ:";
                iTextSharp.text.pdf.PdfPCell cellObs = new iTextSharp.text.pdf.PdfPCell(new Phrase(ObsInvoice, fuenteDatos));
                cellObs.Colspan = 6;
                cellObs.Padding = 3;
                cellObs.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellObs.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableInvoice.AddCell(cellObs);

                /*TOTALES FACTURA*/
                iTextSharp.text.Font fontTotales = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.pdf.PdfPTable tableTotales = new iTextSharp.text.pdf.PdfPTable(2);
                tableTotales.HorizontalAlignment = Element.ALIGN_CENTER;

                //Dimenciones.
                float[] DimencionTotales = new float[2];
                DimencionTotales[0] = 5.0F;//
                DimencionTotales[1] = 5.0F;//                          

                tableTotales.WidthPercentage = 100;
                tableTotales.SetWidths(DimencionTotales);

                //1-1 SUBTOTAL
                iTextSharp.text.pdf.PdfPCell cellSubTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase("SUBTOTAL", fontTotales));
                cellSubTotal.Colspan = 1;
                cellSubTotal.Padding = 3;
                cellSubTotal.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellSubTotal.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableTotales.AddCell(cellSubTotal);

                //1-2 SUBTOTAL VALUE
                decimal SubTotal = Convert.ToDecimal(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubTotal"].ToString()) + (TotalDescountInvc * -1);
                iTextSharp.text.pdf.PdfPCell cellSubTotalValue = new iTextSharp.text.pdf.PdfPCell(new Phrase("$" + string.Format("{0:N2}", SubTotal), fontTotales));
                cellSubTotalValue.Colspan = 1;
                cellSubTotalValue.Padding = 3;
                cellSubTotalValue.HorizontalAlignment = iTextSharp.text.Element.ALIGN_RIGHT;
                cellSubTotalValue.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableTotales.AddCell(cellSubTotalValue);

                //2-1 DESCUENTO
                iTextSharp.text.pdf.PdfPCell cellDescuento = new iTextSharp.text.pdf.PdfPCell(new Phrase("DESCUENTO", fontTotales));
                cellDescuento.Colspan = 1;
                cellDescuento.Padding = 3;
                cellDescuento.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellDescuento.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableTotales.AddCell(cellDescuento);

                //2-2 DESCUENTO VALUE
                iTextSharp.text.pdf.PdfPCell cellDescuentoValue = new iTextSharp.text.pdf.PdfPCell(new Phrase("$" + string.Format("{0:N2}", TotalDescountInvc), fontTotales));
                cellDescuentoValue.Colspan = 1;
                cellDescuentoValue.Padding = 3;
                cellDescuentoValue.HorizontalAlignment = iTextSharp.text.Element.ALIGN_RIGHT;
                cellDescuentoValue.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableTotales.AddCell(cellDescuentoValue);

                //3-1 FLETES
                iTextSharp.text.pdf.PdfPCell cellFletes = new iTextSharp.text.pdf.PdfPCell(new Phrase("FLETES", fontTotales));
                cellFletes.Colspan = 1;
                cellFletes.Padding = 3;
                cellFletes.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellFletes.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableTotales.AddCell(cellFletes);

                //3-2 FLETES VALUE
                iTextSharp.text.pdf.PdfPCell cellFletesValue = new iTextSharp.text.pdf.PdfPCell(new Phrase("$" + string.Format("{0:N2}", TotalMissCharges), fontTotales));
                cellFletesValue.Colspan = 1;
                cellFletesValue.Padding = 3;
                cellFletesValue.HorizontalAlignment = iTextSharp.text.Element.ALIGN_RIGHT;
                cellFletesValue.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableTotales.AddCell(cellFletesValue);

                //4-1 IVA
                iTextSharp.text.pdf.PdfPCell cellIVA = new iTextSharp.text.pdf.PdfPCell(new Phrase("IVA", fontTotales));
                cellIVA.Colspan = 1;
                cellIVA.Padding = 3;
                cellIVA.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellIVA.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableTotales.AddCell(cellIVA);

                //4-2 IVA VALUE
                decimal Iva = Convert.ToDecimal(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DocTaxAmt"].ToString());
                iTextSharp.text.pdf.PdfPCell cellIvaValue = new iTextSharp.text.pdf.PdfPCell(new Phrase("$" + string.Format("{0:N2}", Iva), fontTotales));
                cellIvaValue.Colspan = 1;
                cellIvaValue.Padding = 3;
                cellIvaValue.HorizontalAlignment = iTextSharp.text.Element.ALIGN_RIGHT;
                cellIvaValue.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableTotales.AddCell(cellIvaValue);

                //5-1 TOTAL
                iTextSharp.text.pdf.PdfPCell cellTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase("TOTAL", fontTotales));
                cellTotal.Colspan = 1;
                cellTotal.Padding = 3;
                cellTotal.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellTotal.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableTotales.AddCell(cellTotal);

                //5-2 TOTAL VALUE
                decimal Total = Convert.ToDecimal(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"].ToString());
                iTextSharp.text.pdf.PdfPCell cellTotalValue = new iTextSharp.text.pdf.PdfPCell(new Phrase("$" + string.Format("{0:N2}", Total), fontTotales));
                cellTotalValue.Colspan = 1;
                cellTotalValue.Padding = 3;
                cellTotalValue.HorizontalAlignment = iTextSharp.text.Element.ALIGN_RIGHT;
                cellTotalValue.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableTotales.AddCell(cellTotalValue);

                tableTotales.WidthPercentage = 100;
                tableTotales.SetWidths(DimencionTotales);

                //Add table Totales to Invoice Table
                iTextSharp.text.pdf.PdfPCell cellTotales = new iTextSharp.text.pdf.PdfPCell(tableTotales);
                cellTotales.Colspan = 4;
                cellTotales.Rowspan = 2;
                cellTotales.Padding = 3;
                cellTotales.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellTotales.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                cellTotales.BorderWidthLeft = 0;
                cellTotales.BorderColorLeft = BaseColor.WHITE;
                tableInvoice.AddCell(cellTotales);                

                //OBS 2
                string Obs2 = "CONTABILIZO:";
                iTextSharp.text.pdf.PdfPCell cellObs2 = new iTextSharp.text.pdf.PdfPCell(new Phrase(Obs2, fontTotales));
                cellObs2.Colspan = 3;
                cellObs2.Padding = 3;
                cellObs2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellObs2.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableInvoice.AddCell(cellObs2);

                //AUTO RETENEDORES
                string AutoRet = "APROBO";
                iTextSharp.text.pdf.PdfPCell cellAutoRte = new iTextSharp.text.pdf.PdfPCell(new Phrase(AutoRet, fontTotales));
                cellAutoRte.Colspan = 3;
                cellAutoRte.Padding = 3;
                cellAutoRte.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellAutoRte.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableInvoice.AddCell(cellAutoRte);                

                document.Add(tableInvoice);
                document.Add(separator);
                document.Add(separator);

                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();
                //RutaPdf = Ruta + NomArchivo;
                RutaPdf = NomArchivo;
                return true;
            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.Message;
                return false;
            }
        }

        private bool AddColumnsInvoiceLineTECNAS(ref iTextSharp.text.pdf.PdfPTable table)
        {
            try
            {
                iTextSharp.text.Font fuenteEncabezado = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 6, iTextSharp.text.Font.NORMAL);               

                iTextSharp.text.pdf.PdfPCell celda1 = new iTextSharp.text.pdf.PdfPCell(new Phrase("REFERENCIA", fuenteEncabezado));
                celda1.Colspan = 1;
                celda1.Padding = 3;
                celda1.BackgroundColor = iTextSharp.text.pdf.ExtendedColor.LIGHT_GRAY;
                celda1.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celda1.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                table.AddCell(celda1);

                iTextSharp.text.pdf.PdfPCell celda2 = new iTextSharp.text.pdf.PdfPCell(new Phrase("DESCRIPCIÓN", fuenteEncabezado));
                celda2.Colspan = 1;
                celda2.Padding = 3;
                celda2.BackgroundColor = iTextSharp.text.pdf.ExtendedColor.LIGHT_GRAY;
                celda2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celda2.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                table.AddCell(celda2);

                iTextSharp.text.pdf.PdfPCell cellLote = new iTextSharp.text.pdf.PdfPCell(new Phrase("LOTE", fuenteEncabezado));
                cellLote.Colspan = 1;
                cellLote.Padding = 3;
                cellLote.BackgroundColor = iTextSharp.text.pdf.ExtendedColor.LIGHT_GRAY;
                cellLote.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                cellLote.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                table.AddCell(cellLote);

                iTextSharp.text.pdf.PdfPCell cellVenceLote = new iTextSharp.text.pdf.PdfPCell(new Phrase("VENCE LOTE", fuenteEncabezado));
                cellVenceLote.Colspan = 1;
                cellVenceLote.Padding = 3;
                cellVenceLote.BackgroundColor = iTextSharp.text.pdf.ExtendedColor.LIGHT_GRAY;
                cellVenceLote.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                cellVenceLote.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                table.AddCell(cellVenceLote);

                iTextSharp.text.pdf.PdfPCell celda3 = new iTextSharp.text.pdf.PdfPCell(new Phrase("CANTIDAD", fuenteEncabezado));
                celda3.Colspan = 1;
                celda3.Padding = 3;
                celda3.BackgroundColor = iTextSharp.text.pdf.ExtendedColor.LIGHT_GRAY;
                celda3.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celda3.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                table.AddCell(celda3);

                iTextSharp.text.pdf.PdfPCell celda4 = new iTextSharp.text.pdf.PdfPCell(new Phrase("UND.", fuenteEncabezado));
                celda4.Colspan = 1;
                celda4.Padding = 3;
                celda4.BackgroundColor = iTextSharp.text.pdf.ExtendedColor.LIGHT_GRAY;
                celda4.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celda4.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                table.AddCell(celda4);

                iTextSharp.text.pdf.PdfPCell celda5 = new iTextSharp.text.pdf.PdfPCell(new Phrase("VAL UNITARIO", fuenteEncabezado));
                celda5.Colspan = 1;
                celda5.Padding = 3;
                celda5.BackgroundColor = iTextSharp.text.pdf.ExtendedColor.LIGHT_GRAY;
                celda5.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celda5.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                table.AddCell(celda5);

                iTextSharp.text.pdf.PdfPCell cellIVA = new iTextSharp.text.pdf.PdfPCell(new Phrase("IVA %", fuenteEncabezado));
                cellIVA.Colspan = 1;
                cellIVA.Padding = 3;
                cellIVA.BackgroundColor = iTextSharp.text.pdf.ExtendedColor.LIGHT_GRAY;
                cellIVA.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                cellIVA.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                table.AddCell(cellIVA);

                iTextSharp.text.pdf.PdfPCell cellDcto = new iTextSharp.text.pdf.PdfPCell(new Phrase("DCTO %", fuenteEncabezado));
                cellDcto.Colspan = 1;
                cellDcto.Padding = 3;
                cellDcto.BackgroundColor = iTextSharp.text.pdf.ExtendedColor.LIGHT_GRAY;
                cellDcto.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                cellDcto.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                table.AddCell(cellDcto);

                iTextSharp.text.pdf.PdfPCell celda6 = new iTextSharp.text.pdf.PdfPCell(new Phrase("VALOR TOTAL", fuenteEncabezado));
                celda6.Colspan = 1;
                celda6.Padding = 3;
                celda6.BackgroundColor = iTextSharp.text.pdf.ExtendedColor.LIGHT_GRAY;
                celda6.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celda6.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                table.AddCell(celda6);

                return true;
            }
            catch (Exception ex)
            {
                strError = "Error al crear columnas de tabla Items: " + ex.Message;
                return false;
            }
        }

        private bool AddColumnsCreditNoteLineTECNAS(ref iTextSharp.text.pdf.PdfPTable table)
        {
            try
            {
                iTextSharp.text.Font fuenteEncabezado = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 6, iTextSharp.text.Font.NORMAL);

                iTextSharp.text.pdf.PdfPCell celda1 = new iTextSharp.text.pdf.PdfPCell(new Phrase("REFERENCIA", fuenteEncabezado));
                celda1.Colspan = 1;
                celda1.Padding = 3;
                celda1.BackgroundColor = iTextSharp.text.pdf.ExtendedColor.LIGHT_GRAY;
                celda1.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celda1.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                table.AddCell(celda1);

                iTextSharp.text.pdf.PdfPCell celda2 = new iTextSharp.text.pdf.PdfPCell(new Phrase("DESCRIPCIÓN", fuenteEncabezado));
                celda2.Colspan = 1;
                celda2.Padding = 3;
                celda2.BackgroundColor = iTextSharp.text.pdf.ExtendedColor.LIGHT_GRAY;
                celda2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celda2.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                table.AddCell(celda2);               

                iTextSharp.text.pdf.PdfPCell celda3 = new iTextSharp.text.pdf.PdfPCell(new Phrase("CANTIDAD", fuenteEncabezado));
                celda3.Colspan = 1;
                celda3.Padding = 3;
                celda3.BackgroundColor = iTextSharp.text.pdf.ExtendedColor.LIGHT_GRAY;
                celda3.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celda3.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                table.AddCell(celda3);                

                iTextSharp.text.pdf.PdfPCell celda5 = new iTextSharp.text.pdf.PdfPCell(new Phrase("VAL UNITARIO", fuenteEncabezado));
                celda5.Colspan = 1;
                celda5.Padding = 3;
                celda5.BackgroundColor = iTextSharp.text.pdf.ExtendedColor.LIGHT_GRAY;
                celda5.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celda5.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                table.AddCell(celda5);                                

                iTextSharp.text.pdf.PdfPCell celda6 = new iTextSharp.text.pdf.PdfPCell(new Phrase("VALOR TOTAL", fuenteEncabezado));
                celda6.Colspan = 1;
                celda6.Padding = 3;
                celda6.BackgroundColor = iTextSharp.text.pdf.ExtendedColor.LIGHT_GRAY;
                celda6.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celda6.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                table.AddCell(celda6);

                return true;
            }
            catch (Exception ex)
            {
                strError = "Error al crear columnas de tabla Items: " + ex.Message;
                return false;
            }
        }

        private bool AddRowInvoiceTECNAS(DataRow InvoiceLine, ref iTextSharp.text.pdf.PdfPTable table, iTextSharp.text.Font fuenteDatos, DataTable DtInvcTax)
        {
            try
            {                
                //REFERENCIA
                iTextSharp.text.pdf.PdfPCell celCodigo = new iTextSharp.text.pdf.PdfPCell(new Phrase(InvoiceLine["PartNum"].ToString(), fuenteDatos));
                celCodigo.Colspan = 1;
                celCodigo.Padding = 3;
                celCodigo.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celCodigo.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                celCodigo.BorderWidthBottom = 0;
                celCodigo.BorderWidthTop = 0;
                celCodigo.BorderWidthRight = 0;
                table.AddCell(celCodigo);

                //DESCRIPCION
                iTextSharp.text.pdf.PdfPCell celDesc = new iTextSharp.text.pdf.PdfPCell(new Phrase(InvoiceLine["LineDesc"].ToString(), fuenteDatos));
                celDesc.Colspan = 1;
                celDesc.Padding = 3;
                celDesc.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                celDesc.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                celDesc.BorderWidthBottom = 0;
                celDesc.BorderWidthTop = 0;
                celDesc.BorderWidthRight = 0;
                table.AddCell(celDesc);

                //LOTE
                iTextSharp.text.pdf.PdfPCell cellLote = new iTextSharp.text.pdf.PdfPCell(new Phrase(InvoiceLine["LotNum"].ToString(), fuenteDatos));
                cellLote.Colspan = 1;
                cellLote.Padding = 3;
                cellLote.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellLote.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;                
                cellLote.BorderWidthBottom = 0;
                cellLote.BorderWidthTop = 0;
                cellLote.BorderWidthRight = 0;
                table.AddCell(cellLote);

                //VENCE LOTE
                iTextSharp.text.pdf.PdfPCell cellLoteVence = new iTextSharp.text.pdf.PdfPCell(new Phrase(Convert.ToDateTime(InvoiceLine["Date01"]).ToString("dd/MM/yyyy"), fuenteDatos));
                cellLoteVence.Colspan = 1;
                cellLoteVence.Padding = 3;
                cellLoteVence.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellLoteVence.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                cellLoteVence.BorderWidthBottom = 0;
                cellLoteVence.BorderWidthTop = 0;
                cellLoteVence.BorderWidthRight = 0;
                table.AddCell(cellLoteVence);

                //CANTIDAD
                decimal Qty = Convert.ToDecimal(InvoiceLine["SellingShipQty"].ToString());
                iTextSharp.text.pdf.PdfPCell celCant = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:N0}", Qty), fuenteDatos));
                celCant.Colspan = 1;
                celCant.Padding = 3;
                celCant.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celCant.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                celCant.BorderWidthBottom = 0;
                celCant.BorderWidthTop = 0;
                celCant.BorderWidthRight = 0;
                table.AddCell(celCant);

                //UOM
                string UOM = InvoiceLine["IUM"].ToString();
                iTextSharp.text.pdf.PdfPCell cellUOM = new iTextSharp.text.pdf.PdfPCell(new Phrase(UOM, fuenteDatos));
                cellUOM.Colspan = 1;
                cellUOM.Padding = 3;
                cellUOM.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                cellUOM.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                cellUOM.BorderWidthBottom = 0;
                cellUOM.BorderWidthTop = 0;
                cellUOM.BorderWidthRight = 0;
                table.AddCell(cellUOM);

                //UNIT PRICE
                decimal UnitPrice = Convert.ToDecimal(InvoiceLine["DocUnitPrice"].ToString());
                iTextSharp.text.pdf.PdfPCell cellUnitPrice = new iTextSharp.text.pdf.PdfPCell(new Phrase("$" + string.Format("{0:N2}", UnitPrice), fuenteDatos));
                cellUnitPrice.Colspan = 1;
                cellUnitPrice.Padding = 3;
                cellUnitPrice.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                cellUnitPrice.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                cellUnitPrice.BorderWidthBottom = 0;
                cellUnitPrice.BorderWidthTop = 0;
                cellUnitPrice.BorderWidthRight = 0;
                table.AddCell(cellUnitPrice);

                //IVA %
                decimal PercentIva = Convert.ToDecimal(GetPercentIdImpDIAN(InvoiceLine["InvoiceNum"].ToString(), InvoiceLine["InvoiceLine"].ToString(), DtInvcTax));
                iTextSharp.text.pdf.PdfPCell celIva = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:N1}", PercentIva), fuenteDatos));
                celIva.Colspan = 1;
                celIva.Padding = 3;
                celIva.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celIva.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                celIva.BorderWidthBottom = 0;
                celIva.BorderWidthTop = 0;
                celIva.BorderWidthRight = 0;
                table.AddCell(celIva);

                //DISCOUNT %
                iTextSharp.text.pdf.PdfPCell celDcto = new iTextSharp.text.pdf.PdfPCell(new Phrase(InvoiceLine["DiscountPercent"].ToString(), fuenteDatos));
                celDcto.Colspan = 1;
                celDcto.Padding = 3;
                celDcto.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celDcto.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                celDcto.BorderWidthBottom = 0;
                celDcto.BorderWidthTop = 0;
                celDcto.BorderWidthRight = 0;
                table.AddCell(celDcto);

                //EXT PRICE
                decimal PriceLine = Convert.ToDecimal(InvoiceLine["DocExtPrice"].ToString());
                iTextSharp.text.pdf.PdfPCell celExtPrise = new iTextSharp.text.pdf.PdfPCell(new Phrase("$" + string.Format("{0:N2}", PriceLine), fuenteDatos));
                celExtPrise.Colspan = 1;
                celExtPrise.Padding = 3;
                celExtPrise.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celExtPrise.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                celExtPrise.BorderWidthBottom = 0;
                celExtPrise.BorderWidthTop = 0;
                table.AddCell(celExtPrise);

                return true;
            }
            catch (Exception ex)
            {
                strError = "Error addLineInvoice: " + ex.Message;
                return false;
            }
        }

        private bool AddRowCreditNoteTECNAS(DataRow InvoiceLine, ref iTextSharp.text.pdf.PdfPTable table, iTextSharp.text.Font fuenteDatos, DataTable DtInvcTax)
        {
            try
            {
                //REFERENCIA
                iTextSharp.text.pdf.PdfPCell celCodigo = new iTextSharp.text.pdf.PdfPCell(new Phrase(InvoiceLine["PartNum"].ToString(), fuenteDatos));
                celCodigo.Colspan = 1;
                celCodigo.Padding = 3;
                celCodigo.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celCodigo.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                celCodigo.BorderWidthBottom = 0;
                celCodigo.BorderWidthTop = 0;
                celCodigo.BorderWidthRight = 0;
                table.AddCell(celCodigo);

                //DESCRIPCION
                iTextSharp.text.pdf.PdfPCell celDesc = new iTextSharp.text.pdf.PdfPCell(new Phrase(InvoiceLine["LineDesc"].ToString(), fuenteDatos));
                celDesc.Colspan = 1;
                celDesc.Padding = 3;
                celDesc.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                celDesc.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                celDesc.BorderWidthBottom = 0;
                celDesc.BorderWidthTop = 0;
                celDesc.BorderWidthRight = 0;
                table.AddCell(celDesc);

                //CANTIDAD
                decimal Qty = Convert.ToDecimal(InvoiceLine["SellingShipQty"].ToString());
                iTextSharp.text.pdf.PdfPCell celCant = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:N0}", Qty), fuenteDatos));
                celCant.Colspan = 1;
                celCant.Padding = 3;
                celCant.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celCant.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                celCant.BorderWidthBottom = 0;
                celCant.BorderWidthTop = 0;
                celCant.BorderWidthRight = 0;
                table.AddCell(celCant);                

                //UNIT PRICE
                decimal UnitPrice = Convert.ToDecimal(InvoiceLine["DocUnitPrice"].ToString());
                iTextSharp.text.pdf.PdfPCell cellUnitPrice = new iTextSharp.text.pdf.PdfPCell(new Phrase("$" + string.Format("{0:N2}", UnitPrice), fuenteDatos));
                cellUnitPrice.Colspan = 1;
                cellUnitPrice.Padding = 3;
                cellUnitPrice.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                cellUnitPrice.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                cellUnitPrice.BorderWidthBottom = 0;
                cellUnitPrice.BorderWidthTop = 0;
                cellUnitPrice.BorderWidthRight = 0;
                table.AddCell(cellUnitPrice);                               

                //EXT PRICE
                decimal PriceLine = Convert.ToDecimal(InvoiceLine["DocExtPrice"].ToString());
                iTextSharp.text.pdf.PdfPCell celExtPrise = new iTextSharp.text.pdf.PdfPCell(new Phrase("$" + string.Format("{0:N2}", PriceLine), fuenteDatos));
                celExtPrise.Colspan = 1;
                celExtPrise.Padding = 3;
                celExtPrise.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celExtPrise.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                celExtPrise.BorderWidthBottom = 0;
                celExtPrise.BorderWidthTop = 0;
                table.AddCell(celExtPrise);

                return true;
            }
            catch (Exception ex)
            {
                strError = "Error addLineInvoice: " + ex.Message;
                return false;
            }
        }

        public bool NroenletrasTecnas(string num,ref string numLetras)
        {
            string res, dec = "";
            Int64 entero;
            int decimales;
            decimal nro;
            numLetras = "";

            try
            {
                nro = Convert.ToDecimal(num);

                entero = Convert.ToInt64(Math.Truncate(nro));
                decimales = Convert.ToInt32(Math.Round((nro - entero) * 100, 2));
                if (decimales > 0)
                {
                    dec = " CON " + decimales.ToString();
                }
                else
                {
                    dec = " CON 00";
                }

                res = toText(Convert.ToDouble(entero)) + dec + " PESOS";
                numLetras = res;
                
                return true;
            }
            catch (Exception ex)
            {
                numLetras ="Error:"+ ex.Message;
                return false;
            }
        }

        #endregion

        #region "Formatos Facturas TALSA"

        public bool FacturaNacionalTALSA(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType)
        {
            NomArchivo = string.Empty;
            string Ruta = HttpContext.Current.Server.MapPath("~/PDF/" + NIT + "/");

            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            string RutaImg = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";
            string RutaBanner = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Pata_" + NIT + ".png";            

            NomArchivo = NombreInvoice + ".pdf";

            try
            {
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.
                Document document = new Document(iTextSharp.text.PageSize.LETTER);
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;

                // step 3: we open the document
                document.Open();           

                /*TABLA FACTURA*/
                iTextSharp.text.pdf.PdfPTable tableInvoice = new iTextSharp.text.pdf.PdfPTable(7);
                tableInvoice.HorizontalAlignment = Element.ALIGN_CENTER;

                //Dimenciones.
                float[] DimencionInvoice = new float[7];
                DimencionInvoice[0] = 2.0F;//CODIGO
                DimencionInvoice[1] = 5.0F;//Descripcion
                DimencionInvoice[2] = 2.0F;//CANTIDAD
                DimencionInvoice[3] = 3.0F;//Precio Unitario               
                DimencionInvoice[4] = 1.0F;//Iva %
                DimencionInvoice[5] = 2.0F;//Dcto %
                DimencionInvoice[6] = 4.0F;//VR TOTAL                                

                tableInvoice.WidthPercentage = 100;
                tableInvoice.SetWidths(DimencionInvoice);

                //Celda Logo
                System.Drawing.Image Logo;
                System.Drawing.Image LogoBanner = null; ;
                
                var request = WebRequest.Create(RutaImg);

                using (var response = request.GetResponse())
                using (var stream = response.GetResponseStream())
                {
                    Logo = Bitmap.FromStream(stream);
                }

                //Banner
                try
                {
                    var requestBanner = WebRequest.Create(RutaBanner);

                    using (var responseBanner = requestBanner.GetResponse())
                    using (var streamBanner = responseBanner.GetResponseStream())
                    {
                        LogoBanner = Bitmap.FromStream(streamBanner);
                    }
                }
                catch (Exception)
                {
                    strError += "Error al cargar Logo Banner, not foud\n";
                }
                

                iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 10, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitleSubtittle = FontFactory.GetFont(FontFactory.HELVETICA, 5, iTextSharp.text.Font.NORMAL);
                Paragraph prgTitle = new Paragraph(DsInvoiceAR.Tables["Company"].Rows[0]["Name"].ToString(), fontTitle);
                prgTitle.Alignment = Element.ALIGN_CENTER;
                Paragraph prgTitle2 = new Paragraph("REALIZADA POR " + DsInvoiceAR.Tables["Company"].Rows[0]["Name"] + " NIT: " + DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"], fontTitleSubtittle);
                prgTitle2.Alignment = Element.ALIGN_CENTER;

                iTextSharp.text.pdf.PdfPCell cellTitle = new iTextSharp.text.pdf.PdfPCell();
                cellTitle.AddElement(prgTitle);
                cellTitle.AddElement(prgTitle2);
                cellTitle.Colspan = 7;
                cellTitle.Padding = 3;
                cellTitle.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                cellTitle.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                cellTitle.Border = 0;
                cellTitle.BorderColorRight = BaseColor.WHITE;                
                tableInvoice.AddCell(cellTitle);

                iTextSharp.text.Image LogoPdf = iTextSharp.text.Image.GetInstance(Logo, BaseColor.WHITE);
                LogoPdf.ScaleAbsolute(60f, 60f);
                iTextSharp.text.Font fontLogo = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.BOLD);

                string InfoLogo = "NIT. " + DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"];                                                                  
                Paragraph prgLogo = new Paragraph(InfoLogo, fontLogo);
                prgLogo.Alignment = Element.ALIGN_LEFT;

                //iTextSharp.text.pdf.PdfPCell celLogoImg = new iTextSharp.text.pdf.PdfPCell(LogoPdf);
                iTextSharp.text.pdf.PdfPCell celLogoImg = new iTextSharp.text.pdf.PdfPCell();
                celLogoImg.AddElement(LogoPdf);
                celLogoImg.AddElement(prgLogo);
                celLogoImg.Colspan = 1;
                celLogoImg.Padding = 3;
                celLogoImg.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celLogoImg.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                celLogoImg.Border = 0;
                celLogoImg.BorderColorRight = BaseColor.WHITE;                
                tableInvoice.AddCell(celLogoImg);

                //Celda Titulo
                string strTitulo1 = "Principal Itaguí\n" +
                                    "Cra 50 GG No. 12 Sur 07\n" +
                                    "Tel: (057)(4) 285 44 00\n" +
                                    "E-mail: info@citalsa.com\n\n" +
                                    "Barranquilla\n" +
                                    "Calle 93 No.46 - 168\n" +
                                    "Tel: (57)(5) 319 98 80\n" +
                                    "E-mail: superbarranquilla@citalsa.com";
                                                                        
                iTextSharp.text.Font fontTitle1 = FontFactory.GetFont(FontFactory.HELVETICA, 5, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.pdf.PdfPCell celTittle1 = new iTextSharp.text.pdf.PdfPCell(new Phrase(strTitulo1, fontTitle1));
                celTittle1.Colspan = 1;
                celTittle1.Padding = 3;
                celTittle1.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celTittle1.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;                
                celTittle1.Border = 0;                
                tableInvoice.AddCell(celTittle1);

                string strTitulo2 = "Bogotá D.C\n" +
                                    "Avenida 68 No.8 - 05 Esquina\n" +
                                    "Tel:(57)(1) 492 60 50\n" +
                                    "E-mail: bogota@citalsa.com\n\n" +
                                    "Pereira\n" +
                                    "Cra. 16 No. 15 - 42\n" +
                                    "Dos Quebradas Risaralda\n" +
                                    "Tel:(57)(6) 330 66 90\n" +
                                    "E-mail: pereira@citalsa.com";

                iTextSharp.text.pdf.PdfPCell celTittle2 = new iTextSharp.text.pdf.PdfPCell(new Phrase(strTitulo2, fontTitle1));
                celTittle2.Colspan = 1;
                celTittle2.Padding = 3;
                celTittle2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celTittle2.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                celTittle2.Border = 0;                
                tableInvoice.AddCell(celTittle2);


                string strTitulo3 = "Cali\n" +
                                    "Carrera 1 No.45A - 71\n" +
                                    "Tel: (57)(2) 431 30 30\n" +
                                    "E-mail: cali@citalsa.com\n\n" +
                                    "Cúcuta\n" +
                                    "Avenida 0 No. 2N - 08\n" +
                                    "Tel: (57)(7) 577 41 11\n" +
                                    "E-mail: cucuta@citalsa.com";

                iTextSharp.text.pdf.PdfPCell celTittle3 = new iTextSharp.text.pdf.PdfPCell(new Phrase(strTitulo3, fontTitle1));
                celTittle3.Colspan = 1;
                celTittle3.Padding = 3;
                celTittle3.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celTittle3.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                celTittle3.Border = 0;                
                tableInvoice.AddCell(celTittle3);


                string strTitulo4 = "Bucaramanga\n" +
                                    "Carrera 23 No.21 - 30 San Francisco\n" +
                                    "Tel: (57)(7) 635 02 74\n" +
                                    "E-mail: superbucaramanga@citalsa.com\n\n" +
                                    "Servicio al cliente C.I TALSA\n" +                                    
                                    "Tel: (57)(4) 285 44 00 Ext. 158\n" +
                                    "Cel: (318) 571 35 17\n" +
                                    "E-mail: servicioalcliente@citalsa.com";

                iTextSharp.text.pdf.PdfPCell celTittle4 = new iTextSharp.text.pdf.PdfPCell(new Phrase(strTitulo4, fontTitle1));
                celTittle4.Colspan = 2;
                celTittle4.Padding = 3;
                celTittle4.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celTittle4.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                celTittle4.Border = 0;                
                tableInvoice.AddCell(celTittle4);

                string TipoFactura = "FACTURA DE VENTA N°\n\n" +
                                     "" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"].ToString();

                //Celda LegalNum
                iTextSharp.text.Font fontLegalNum = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.pdf.PdfPCell celLegalNum = new iTextSharp.text.pdf.PdfPCell(new Phrase(TipoFactura, fontLegalNum));
                celLegalNum.Colspan = 1;
                celLegalNum.Padding = 3;
                celLegalNum.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                celLegalNum.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                celLegalNum.Border = 0;                
                tableInvoice.AddCell(celLegalNum);

                //Celda Regimen Comun
                iTextSharp.text.Font fontRegComun = FontFactory.GetFont(FontFactory.HELVETICA, 5, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.pdf.PdfPCell celRegComun = new iTextSharp.text.pdf.PdfPCell(new Phrase("IVA RÉGIMEN COMÚN 11220207 MARZO 15/88\n\n", fontRegComun));
                celRegComun.Colspan = 7;
                celRegComun.Padding = 3;
                celRegComun.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                celRegComun.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;                  
                celRegComun.BorderWidthTop = 0;
                celRegComun.BorderWidthLeft = 0;
                celRegComun.BorderWidthRight = 0;
                tableInvoice.AddCell(celRegComun);

                /*VENDIDO A*/
                iTextSharp.text.Font fontVendidoA = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 6, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontVendidoASubTittle = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);                

                Paragraph prgVendidoA = new Paragraph("SEÑORES", fontVendidoA);
                prgVendidoA.Alignment = Element.ALIGN_LEFT;

                string VendidoA = DsInvoiceAR.Tables["Customer"].Rows[0]["Name"].ToString() + "\n" +
                                  DsInvoiceAR.Tables["Customer"].Rows[0]["Address1"].ToString() + "\n" +
                                  DsInvoiceAR.Tables["Customer"].Rows[0]["City"].ToString() + "\n" +
                                  DsInvoiceAR.Tables["Customer"].Rows[0]["State"].ToString() + "\n\n" +
                                  "NIT/CEDULA   " + DsInvoiceAR.Tables["Customer"].Rows[0]["ResaleID"].ToString();

                Paragraph prgVendidoASubTittle = new Paragraph(VendidoA, fontVendidoASubTittle);
                prgVendidoASubTittle.Alignment = Element.ALIGN_LEFT;

                iTextSharp.text.pdf.PdfPCell cellVendidoA = new iTextSharp.text.pdf.PdfPCell();
                cellVendidoA.AddElement(prgVendidoA);
                cellVendidoA.AddElement(prgVendidoASubTittle);
                cellVendidoA.Colspan = 2;                
                cellVendidoA.Padding = 3;
                cellVendidoA.BorderWidthTop = 0;
                cellVendidoA.BorderWidthRight = 0;                
                cellVendidoA.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellVendidoA.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;                
                tableInvoice.AddCell(cellVendidoA);

                /*INFO PEDIDO*/
                iTextSharp.text.Font fontInfoPedido = FontFactory.GetFont(FontFactory.HELVETICA, 5, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.pdf.PdfPTable tableInfoPedido = new iTextSharp.text.pdf.PdfPTable(5);
                tableInfoPedido.HorizontalAlignment = Element.ALIGN_CENTER;

                //Dimenciones.
                float[] DimencionInfoPedido = new float[5];
                DimencionInfoPedido[0] = 5.0F;//
                DimencionInfoPedido[1] = 5.0F;//
                DimencionInfoPedido[2] = 5.0F;//                
                DimencionInfoPedido[3] = 5.0F;//
                DimencionInfoPedido[4] = 5.0F;// 

                tableInfoPedido.WidthPercentage = 100;
                tableInfoPedido.SetWidths(DimencionInfoPedido);

                //1-1 Confirma Pedido.
                iTextSharp.text.pdf.PdfPCell cellConfirmaPedido = new iTextSharp.text.pdf.PdfPCell(new Phrase("CONFIRMACION PEDIDO\n\n\n\n", fontInfoPedido));
                cellConfirmaPedido.Colspan = 1;
                cellConfirmaPedido.Padding = 3;
                cellConfirmaPedido.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellConfirmaPedido.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableInfoPedido.AddCell(cellConfirmaPedido);

                //1-2 No Pedido
                iTextSharp.text.pdf.PdfPCell cellNoPedido = new iTextSharp.text.pdf.PdfPCell(new Phrase("No. PEDIDO\n" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["OrderNum"].ToString() + "\n\n", fontInfoPedido));
                cellNoPedido.Colspan = 1;
                cellNoPedido.Padding = 3;
                cellNoPedido.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellNoPedido.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableInfoPedido.AddCell(cellNoPedido);

                //1-3 Orden de Compra                
                iTextSharp.text.pdf.PdfPCell cellOrdenCompra = new iTextSharp.text.pdf.PdfPCell(new Phrase("ORDEN DE COMPRA\n" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["PONum"].ToString() + "\n\n", fontInfoPedido));
                cellOrdenCompra.Colspan = 1;
                cellOrdenCompra.Padding = 3;
                cellOrdenCompra.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellOrdenCompra.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableInfoPedido.AddCell(cellOrdenCompra);
             
                //1-4 Fecha Vencimiento
                iTextSharp.text.pdf.PdfPCell cellDueDate = new iTextSharp.text.pdf.PdfPCell(new Phrase("FECHA VENCIMIENTO:\n" + Convert.ToDateTime(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"]).ToString("dd/MM/yyyy") + "\n\n", fontInfoPedido));
                cellDueDate.Colspan = 1;
                cellDueDate.Padding = 3;
                cellDueDate.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellDueDate.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableInfoPedido.AddCell(cellDueDate);

                //1-5 Fecha Factura
                iTextSharp.text.pdf.PdfPCell cellInvoiceDate = new iTextSharp.text.pdf.PdfPCell(new Phrase("FECHA FACTURA:\n" + Convert.ToDateTime(DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).ToString("dd/MM/yyyy") + "\n\n", fontInfoPedido));
                cellInvoiceDate.Colspan = 1;
                cellInvoiceDate.Padding = 3;
                cellInvoiceDate.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellInvoiceDate.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableInfoPedido.AddCell(cellInvoiceDate);

                //2-1 Telefono
                iTextSharp.text.pdf.PdfPCell cellTelefono = new iTextSharp.text.pdf.PdfPCell(new Phrase("TELÉFONO\n" + DsInvoiceAR.Tables["Customer"].Rows[0]["PhoneNum"].ToString(), fontInfoPedido));
                cellTelefono.Colspan = 1;
                cellTelefono.Padding = 3;
                cellTelefono.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellTelefono.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableInfoPedido.AddCell(cellTelefono);

                //2-2 InvoiceNum
                iTextSharp.text.pdf.PdfPCell cellNoFacRef = new iTextSharp.text.pdf.PdfPCell(new Phrase("Nro. Fact. REF\n" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceNum"].ToString(), fontInfoPedido));
                cellNoFacRef.Colspan = 1;
                cellNoFacRef.Padding = 3;
                cellNoFacRef.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellNoFacRef.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableInfoPedido.AddCell(cellNoFacRef);

                //2-3 PLAZO MESES
                iTextSharp.text.pdf.PdfPCell cellTermsCode = new iTextSharp.text.pdf.PdfPCell(new Phrase("PLAZO MESES\n" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["TermsCodeDescription"].ToString(), fontInfoPedido));
                cellTermsCode.Colspan = 1;
                cellTermsCode.Padding = 3;
                cellTermsCode.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellTermsCode.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableInfoPedido.AddCell(cellTermsCode);

                //2-4 VENDEDOR
                iTextSharp.text.pdf.PdfPCell cellVendedor = new iTextSharp.text.pdf.PdfPCell(new Phrase("VENDEDOR\n" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["SalesRepName1"].ToString(), fontInfoPedido));
                cellVendedor.Colspan = 1;
                cellVendedor.Padding = 3;
                cellVendedor.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellVendedor.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableInfoPedido.AddCell(cellVendedor);

                //2-4 FECHA ENTREGA
                iTextSharp.text.pdf.PdfPCell cellFechaEntrega = new iTextSharp.text.pdf.PdfPCell(new Phrase("FECHA ENTREGA", fontInfoPedido));
                cellFechaEntrega.Colspan = 1;
                cellFechaEntrega.Padding = 3;
                cellFechaEntrega.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellFechaEntrega.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableInfoPedido.AddCell(cellFechaEntrega);

                iTextSharp.text.pdf.PdfPCell cellInfoPedido = new iTextSharp.text.pdf.PdfPCell(tableInfoPedido);
                cellInfoPedido.Colspan = 4;
                cellInfoPedido.Padding = 3;
                cellInfoPedido.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellInfoPedido.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;               
                cellInfoPedido.BorderWidthTop = 0;
                tableInvoice.AddCell(cellInfoPedido);

                /*CELL QR*/
                iTextSharp.text.Image QRPdf = iTextSharp.text.Image.GetInstance(QRInvoice, BaseColor.WHITE);
                iTextSharp.text.pdf.PdfPCell celImgQR = new iTextSharp.text.pdf.PdfPCell(QRPdf);
                celImgQR.Colspan = 1;
                celImgQR.Padding = 3;
                celImgQR.BorderWidthTop = 0;
                celImgQR.BorderWidthLeft = 0;                
                celImgQR.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celImgQR.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableInvoice.AddCell(celImgQR);

                iTextSharp.text.pdf.PdfPCell cellBlank = new iTextSharp.text.pdf.PdfPCell(new Phrase("\n", fontInfoPedido));
                cellBlank.Colspan = 7;
                cellBlank.Padding = 3;
                cellBlank.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellBlank.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                cellBlank.BorderWidth = 0;
                tableInvoice.AddCell(cellBlank);

                /*Tabla - ITEMS FACTURA.*/                

                //Configuramos las columnas.
                if (!AddColumnsInvoiceLineTALSA(ref tableInvoice))
                    return false;

                //Filas Factura
                iTextSharp.text.Font fuenteDatos = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);

                decimal TotalDescountInvc = 0;
                decimal TotalMissCharges = 0;
                string SalesOrder = string.Empty;
                string Embarques = string.Empty;
                string PartDescLoteAndSerie = string.Empty;

                //Lineas de Factura.
                foreach (DataRow InvoiceLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
                {
                    //Totales de las Lineas
                    TotalDescountInvc += Convert.ToDecimal(InvoiceLine["DspDocLessDiscount"]);
                    //TotalMissCharges += Convert.ToDecimal(InvoiceLine["DspDocTotalMiscChrg"]);                                       

                    if (!AddRowInvoiceTALSA(InvoiceLine, ref tableInvoice, fuenteDatos, DsInvoiceAR.Tables["InvcTax"]))
                        return false;

                    if (!AddInfoByLoteAndSerie(DsInvoiceAR, Convert.ToInt32(InvoiceLine["InvoiceLine"]), ref PartDescLoteAndSerie))
                        return false;
                }

                //Lines de Materiales
                if(!AddRowMaterialesTALSA(DsInvoiceAR,fuenteDatos, ref tableInvoice))
                    return false;

                //Enviar a: ShitoAddres.                
                iTextSharp.text.pdf.PdfPCell cellShipToAddres = new iTextSharp.text.pdf.PdfPCell(new Phrase(DsInvoiceAR.Tables["Customer"].Rows[0]["ShipToAddres"].ToString()+ "\n", fuenteDatos));
                cellShipToAddres.Colspan = 7;
                cellShipToAddres.Padding = 3;
                cellShipToAddres.BorderWidthLeft = 0;
                cellShipToAddres.BorderWidthRight = 0;
                cellShipToAddres.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellShipToAddres.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableInvoice.AddCell(cellShipToAddres);

                iTextSharp.text.Font fuenteObs = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 6, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.pdf.PdfPCell cellObs = new iTextSharp.text.pdf.PdfPCell(new Phrase("OBSERVACIONES:" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceComment"].ToString(),fuenteObs));
                cellObs.Colspan = 4;
                cellObs.Padding = 3;
                cellObs.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellObs.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;                
                tableInvoice.AddCell(cellObs);

                //Info Resolucion.
                //string Resolucion = "HAB 18762006023112 FECHA2017-12-07 VTO 2019-12-07 \n" +
                //                    "DEL CTBQ 4.450-6.000 POR COMPUTADOR\n";

                string Resolucion = DsInvoiceAR.Tables["Encabezado"].Rows[0]["Prefijo1"].ToString() + "\n" +
                                    DsInvoiceAR.Tables["Encabezado"].Rows[0]["Prefijo2"].ToString();

                iTextSharp.text.pdf.PdfPCell cellResolucion = new iTextSharp.text.pdf.PdfPCell(new Phrase(Resolucion, fuenteDatos));
                cellResolucion.Colspan = 3;
                cellResolucion.Padding = 3;
                cellResolucion.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellResolucion.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableInvoice.AddCell(cellResolucion);


                //Observacion 2 factura
                string Obs2 = "PARA SU SEGURIDAD GIRAR CHEQUE CON SELLO DE PAGUESE AL PRIMER BENEFICIARIO.\n" +
                              "POR CANCELACIÓN DE ESTE PEDIDO SE EFECTUARÁ UNA SANCIÓN DEL 20%.\n" +
                              "CON LA ACEPTACIÓN DE ESTA FACTURA, SE DAN POR APROBADAS LAS CONDICIONES EXPRESADAS EN LA GARANTÍA\n" +
                              "COMERCIAL.\n" +
                              "PARA SER EFECTIVA LA GARANTÍA LEGAL C.I. TALSA SE DEBE PRESENTAR LA FACTURA DE COMPRA.\n";

                iTextSharp.text.Font fuenteObs2 = FontFactory.GetFont(FontFactory.HELVETICA, 5, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.pdf.PdfPCell cellObs2 = new iTextSharp.text.pdf.PdfPCell(new Phrase(Obs2, fuenteObs2));
                cellObs2.Colspan = 4;
                cellObs2.Padding = 3;
                cellObs2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellObs2.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableInvoice.AddCell(cellObs2);

                /*TOTAL Factura.*/
                iTextSharp.text.Font fontTotalesTittle = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 6, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTotales = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.pdf.PdfPTable tableTotales = new iTextSharp.text.pdf.PdfPTable(3);
                tableTotales.HorizontalAlignment = Element.ALIGN_CENTER;

                //Dimenciones.
                float[] DimencionTotales = new float[3];
                DimencionTotales[0] = 5.0F;//
                DimencionTotales[1] = 2.0F;//                          
                DimencionTotales[2] = 5.0F;// 

                tableTotales.WidthPercentage = 100;
                tableTotales.SetWidths(DimencionTotales);

                //1-1 SUBTOTAL
                iTextSharp.text.pdf.PdfPCell cellSubTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase("SUBTOTAL", fontTotalesTittle));
                cellSubTotal.Colspan = 2;
                cellSubTotal.Padding = 3;
                cellSubTotal.BorderWidthTop = 0;
                cellSubTotal.BorderWidthLeft = 0;
                cellSubTotal.BorderWidthBottom = 0;
                cellSubTotal.HorizontalAlignment = iTextSharp.text.Element.ALIGN_RIGHT;
                cellSubTotal.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableTotales.AddCell(cellSubTotal);

                //1-2 SUBTOTAL VALUE
                decimal SubTotal = Convert.ToDecimal(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubTotal"].ToString());
                iTextSharp.text.pdf.PdfPCell cellSubTotalValue = new iTextSharp.text.pdf.PdfPCell(new Phrase("$" + string.Format("{0:N2}", SubTotal), fontTotales));
                cellSubTotalValue.Colspan = 1;
                cellSubTotalValue.Padding = 3;
                cellSubTotalValue.BorderWidthTop = 0;
                cellSubTotalValue.BorderWidthRight = 0;
                cellSubTotalValue.BorderWidthBottom = 0;
                cellSubTotalValue.HorizontalAlignment = iTextSharp.text.Element.ALIGN_RIGHT;
                cellSubTotalValue.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableTotales.AddCell(cellSubTotalValue);

                //2-1 DESCUENTO
                iTextSharp.text.pdf.PdfPCell cellDescuento = new iTextSharp.text.pdf.PdfPCell(new Phrase("DESCTO.CCIAL", fontTotalesTittle));
                cellDescuento.Colspan = 2;
                cellDescuento.Padding = 3;
                cellDescuento.BorderWidthTop = 0;
                cellDescuento.BorderWidthLeft = 0;
                cellDescuento.BorderWidthBottom = 0;
                cellDescuento.HorizontalAlignment = iTextSharp.text.Element.ALIGN_RIGHT;
                cellDescuento.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableTotales.AddCell(cellDescuento);

                //2-2 DESCUENTO VALUE
                iTextSharp.text.pdf.PdfPCell cellDescuentoValue = new iTextSharp.text.pdf.PdfPCell(new Phrase("$" + string.Format("{0:N2}", TotalDescountInvc), fontTotales));
                cellDescuentoValue.Colspan = 1;
                cellDescuentoValue.Padding = 3;
                cellDescuentoValue.BorderWidthTop = 0;
                cellDescuentoValue.BorderWidthRight = 0;
                cellDescuentoValue.BorderWidthBottom = 0;
                cellDescuentoValue.HorizontalAlignment = iTextSharp.text.Element.ALIGN_RIGHT;
                cellDescuentoValue.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableTotales.AddCell(cellDescuentoValue);

                //4-1 IVA
                iTextSharp.text.pdf.PdfPCell cellIVA = new iTextSharp.text.pdf.PdfPCell(new Phrase("IVA", fontTotalesTittle));
                cellIVA.Colspan = 2;
                cellIVA.Padding = 3;
                cellIVA.BorderWidthTop = 0;
                cellIVA.BorderWidthLeft = 0;
                cellIVA.BorderWidthBottom = 0;
                cellIVA.HorizontalAlignment = iTextSharp.text.Element.ALIGN_RIGHT;
                cellIVA.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableTotales.AddCell(cellIVA);

                //4-2 IVA VALUE                                
                decimal Iva = Convert.ToDecimal(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DocTaxAmt"].ToString());
                //Iva = GetValImpuestoByID("01", DsInvoiceAR);
                iTextSharp.text.pdf.PdfPCell cellIvaValue = new iTextSharp.text.pdf.PdfPCell(new Phrase("$" + string.Format("{0:N2}", Iva), fontTotales));
                cellIvaValue.Colspan = 1;
                cellIvaValue.Padding = 3;
                cellIvaValue.BorderWidthTop = 0;
                cellIvaValue.BorderWidthRight = 0;
                cellIvaValue.BorderWidthBottom = 0;
                cellIvaValue.HorizontalAlignment = iTextSharp.text.Element.ALIGN_RIGHT;
                cellIvaValue.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableTotales.AddCell(cellIvaValue);

                //4-1 RETE IVA
                iTextSharp.text.pdf.PdfPCell cellReteIva = new iTextSharp.text.pdf.PdfPCell(new Phrase("RETE IVA", fontTotalesTittle));
                cellReteIva.Colspan = 2;
                cellReteIva.Padding = 3;
                cellReteIva.BorderWidthTop = 0;
                cellReteIva.BorderWidthLeft = 0;
                cellReteIva.BorderWidthBottom = 0;
                cellReteIva.HorizontalAlignment = iTextSharp.text.Element.ALIGN_RIGHT;
                cellReteIva.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableTotales.AddCell(cellReteIva);

                //4-2 RETE IVA VALUE
                decimal ReteIva = 0; //Convert.ToDecimal(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DocTaxAmt"].ToString());
                iTextSharp.text.pdf.PdfPCell cellReteIvaValue = new iTextSharp.text.pdf.PdfPCell(new Phrase("$" + string.Format("{0:N2}", ReteIva), fontTotales));
                cellReteIvaValue.Colspan = 1;
                cellReteIvaValue.Padding = 3;
                cellReteIvaValue.BorderWidthTop = 0;
                cellReteIvaValue.BorderWidthRight = 0;
                cellReteIvaValue.BorderWidthBottom = 0;
                cellReteIvaValue.HorizontalAlignment = iTextSharp.text.Element.ALIGN_RIGHT;
                cellReteIvaValue.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableTotales.AddCell(cellReteIvaValue);

                //4-1 RETE ICA
                iTextSharp.text.pdf.PdfPCell cellReteIca = new iTextSharp.text.pdf.PdfPCell(new Phrase("RETE ICA", fontTotalesTittle));
                cellReteIca.Colspan = 2;
                cellReteIca.Padding = 3;
                cellReteIca.BorderWidthTop = 0;
                cellReteIca.BorderWidthLeft = 0;
                cellReteIca.BorderWidthBottom = 0;
                cellReteIca.HorizontalAlignment = iTextSharp.text.Element.ALIGN_RIGHT;
                cellReteIca.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableTotales.AddCell(cellReteIca);

                //4-2 RETE ICA VALUE
                decimal ReteIca = 0; //Convert.ToDecimal(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DocTaxAmt"].ToString());
                iTextSharp.text.pdf.PdfPCell cellReteIcaValue = new iTextSharp.text.pdf.PdfPCell(new Phrase("$" + string.Format("{0:N2}", ReteIca), fontTotales));
                cellReteIcaValue.Colspan = 1;
                cellReteIcaValue.Padding = 3;
                cellReteIcaValue.BorderWidthTop = 0;
                cellReteIcaValue.BorderWidthRight = 0;
                cellReteIcaValue.BorderWidthBottom = 0;
                cellReteIcaValue.HorizontalAlignment = iTextSharp.text.Element.ALIGN_RIGHT;
                cellReteIcaValue.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableTotales.AddCell(cellReteIcaValue);

                //4-1 INC BOLSA
                //ImpConsBolsa = GetValImpuestoByID("02", DsInvoiceAR);
                iTextSharp.text.pdf.PdfPCell cellIncBolsa = new iTextSharp.text.pdf.PdfPCell(new Phrase("INC BOLSA", fontTotalesTittle));
                cellIncBolsa.Colspan = 2;
                cellIncBolsa.Padding = 3;
                cellIncBolsa.BorderWidthTop = 0;
                cellIncBolsa.BorderWidthLeft = 0;
                //cellIncBolsa.BorderWidthBottom = 0;
                cellIncBolsa.HorizontalAlignment = iTextSharp.text.Element.ALIGN_RIGHT;
                cellIncBolsa.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableTotales.AddCell(cellIncBolsa);

                //4-2 INC BOLSA VALUE
                decimal IncBolsa = 0; //Convert.ToDecimal(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DocTaxAmt"].ToString());
                iTextSharp.text.pdf.PdfPCell cellIncBolsaValue = new iTextSharp.text.pdf.PdfPCell(new Phrase("$" + string.Format("{0:N2}", IncBolsa), fontTotales));
                cellIncBolsaValue.Colspan = 1;
                cellIncBolsaValue.Padding = 3;
                cellIncBolsaValue.BorderWidthTop = 0;
                cellIncBolsaValue.BorderWidthRight = 0;
                //cellIncBolsaValue.BorderWidthBottom = 0;
                cellIncBolsaValue.HorizontalAlignment = iTextSharp.text.Element.ALIGN_RIGHT;
                cellIncBolsaValue.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableTotales.AddCell(cellIncBolsaValue);

                //5-1 TOTAL
                iTextSharp.text.Font fontTotal = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 8, iTextSharp.text.Font.NORMAL,BaseColor.WHITE);
                iTextSharp.text.pdf.PdfPCell cellTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase("TOTAL", fontTotal));
                cellTotal.Colspan = 1;
                cellTotal.Padding = 3;
                cellTotal.BackgroundColor = BaseColor.BLACK;
                cellTotal.BorderWidth = 0;
                cellTotal.HorizontalAlignment = iTextSharp.text.Element.ALIGN_RIGHT;
                cellTotal.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableTotales.AddCell(cellTotal);

                //5-1 Moneda
                iTextSharp.text.pdf.PdfPCell cellCurrency = new iTextSharp.text.pdf.PdfPCell(new Phrase(DsInvoiceAR.Tables["InvcHead"].Rows[0]["CurrencyCodeCurrencyID"].ToString(), fontTotalesTittle));
                cellCurrency.Colspan = 1;
                cellCurrency.Padding = 3;
                cellCurrency.BorderWidth = 0;   
                cellCurrency.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellCurrency.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableTotales.AddCell(cellCurrency);

                //5-2 TOTAL VALUE
                decimal Total = Convert.ToDecimal(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"].ToString());
                iTextSharp.text.pdf.PdfPCell cellTotalValue = new iTextSharp.text.pdf.PdfPCell(new Phrase("$" + string.Format("{0:N2}", Total), fontTotales));
                cellTotalValue.Colspan = 1;
                cellTotalValue.Padding = 3;
                cellTotalValue.BorderWidth = 0;
                cellTotalValue.HorizontalAlignment = iTextSharp.text.Element.ALIGN_RIGHT;
                cellTotalValue.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableTotales.AddCell(cellTotalValue);

                //Blank1
                iTextSharp.text.pdf.PdfPCell cellBlank1 = new iTextSharp.text.pdf.PdfPCell(new Phrase("\n\n\n\n", fontTotalesTittle));
                cellBlank1.Colspan = 3;
                cellBlank1.Padding = 3;
                cellBlank1.BorderWidth = 0;
                cellBlank1.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellBlank1.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableTotales.AddCell(cellBlank1);

                tableTotales.WidthPercentage = 100;
                tableTotales.SetWidths(DimencionTotales);

                iTextSharp.text.pdf.PdfPCell cellTotales = new iTextSharp.text.pdf.PdfPCell(tableTotales);
                cellTotales.Colspan = 3;
                cellTotales.Rowspan = 3;
                cellTotales.Padding = 3;
                cellTotales.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellTotales.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableInvoice.AddCell(cellTotales);

                //Observacion 3 Factura.                
                iTextSharp.text.pdf.PdfPCell cellObs3 = new iTextSharp.text.pdf.PdfPCell(new Phrase("Somos grandes contribuyentes según la resolución Nº 000076 del 01/12/2016.; Somo autorretenedores en renta resolución 7836 de 5/09/2001", fuenteObs2));
                cellObs3.Colspan = 4;
                cellObs3.Padding = 3;
                cellObs3.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellObs3.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableInvoice.AddCell(cellObs3);

                /*INFORMACION ENTREGA.*/
                iTextSharp.text.Font fontInfoEntrega = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.pdf.PdfPTable tableInfoEntrega = new iTextSharp.text.pdf.PdfPTable(3);
                tableInfoEntrega.HorizontalAlignment = Element.ALIGN_CENTER;

                //Dimenciones.
                float[] DimencionInfoEntrega = new float[3];
                DimencionInfoEntrega[0] = 5.0F;//
                DimencionInfoEntrega[1] = 5.0F;//
                DimencionInfoEntrega[2] = 5.0F;//                               

                tableInfoEntrega.WidthPercentage = 100;
                tableInfoEntrega.SetWidths(DimencionInfoEntrega);

                //Emisor Factura.
                Paragraph prgEmisor = new Paragraph("EMISOR DE FACTURA", fontInfoEntrega);
                prgEmisor.Alignment = Element.ALIGN_LEFT;

                iTextSharp.text.Image LogoPdf2 = iTextSharp.text.Image.GetInstance(Logo, BaseColor.WHITE);
                LogoPdf2.ScaleAbsolute(60f, 60f);

                iTextSharp.text.pdf.PdfPCell cellEmisor = new iTextSharp.text.pdf.PdfPCell();
                cellEmisor.AddElement(prgEmisor);
                cellEmisor.AddElement(LogoPdf2);
                cellEmisor.Colspan = 1;
                cellEmisor.Padding = 3;
                cellEmisor.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellEmisor.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableInfoEntrega.AddCell(cellEmisor);

                //Aceptacion
                iTextSharp.text.pdf.PdfPCell cellAceptacion = new iTextSharp.text.pdf.PdfPCell(new Phrase("ACEPTACIÓN\n",fontInfoEntrega));
                cellAceptacion.Colspan = 1;
                cellAceptacion.Padding = 3;
                cellAceptacion.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellAceptacion.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableInfoEntrega.AddCell(cellAceptacion);               

                //RECIBO DE MERCANCIA.
                string ReciboMercancia = "RECIBO DE MERCANCÍA\n\n" +
                                         "FIRMA\n\n" +
                                         "_________________________\n\n" +
                                         "NOMBRE\n\n" +
                                         "_________________________\n\n" +
                                         "FECHA dd/mm/aaaa\n\n" +
                                         "_________________________\n\n" +
                                         "CC";
                
                iTextSharp.text.pdf.PdfPCell cellRecibido = new iTextSharp.text.pdf.PdfPCell(new Phrase(ReciboMercancia, fontInfoEntrega));
                cellRecibido.Colspan = 1;
                cellRecibido.Padding = 3;
                cellRecibido.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellRecibido.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableInfoEntrega.AddCell(cellRecibido);                

                iTextSharp.text.pdf.PdfPCell cellInfoEntrega = new iTextSharp.text.pdf.PdfPCell(tableInfoEntrega);
                cellInfoEntrega.Colspan = 4;
                cellInfoEntrega.Padding = 3;                
                cellInfoEntrega.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellInfoEntrega.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableInvoice.AddCell(cellInfoEntrega);


                //Pie Factura.
                iTextSharp.text.pdf.PdfPCell cellIcaBarranquilla = new iTextSharp.text.pdf.PdfPCell(new Phrase("Somos autorretenedores de ICA en Barranquilla según decreto 0123 del 2.009.", fuenteObs2));
                cellIcaBarranquilla.Colspan = 4;
                cellIcaBarranquilla.Padding = 3;
                cellIcaBarranquilla.BorderWidth = 0;
                cellIcaBarranquilla.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellIcaBarranquilla.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableInvoice.AddCell(cellIcaBarranquilla);

                iTextSharp.text.pdf.PdfPCell cellIObs3 = new iTextSharp.text.pdf.PdfPCell(new Phrase("ORIGINAL", fuenteDatos));
                cellIObs3.Colspan = 3;
                cellIObs3.Padding = 3;
                cellIObs3.BorderWidth = 0;
                cellIObs3.HorizontalAlignment = iTextSharp.text.Element.ALIGN_RIGHT;
                cellIObs3.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableInvoice.AddCell(cellIObs3);


                iTextSharp.text.Image BannerPdf = iTextSharp.text.Image.GetInstance(LogoBanner, BaseColor.WHITE);
                BannerPdf.ScaleAbsolute(535f, 90f);

                iTextSharp.text.pdf.PdfPCell cellBanner = new iTextSharp.text.pdf.PdfPCell(BannerPdf);            
                cellBanner.Colspan = 7;
                cellBanner.Padding = 3;
                cellBanner.BorderWidth = 0;
                cellBanner.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                cellBanner.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableInvoice.AddCell(cellBanner);

                tableInvoice.HeadersInEvent = true;
                tableInvoice.CompleteRow();                
                
                document.Add(tableInvoice);
                document.Add(separator);
                document.Add(separator);

                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();                
                RutaPdf = NomArchivo;
                return true;
            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.Message;
                return false;
            }
        }

        private bool AddColumnsInvoiceLineTALSA(ref iTextSharp.text.pdf.PdfPTable table)
        {
            try
            {
                iTextSharp.text.Font fuenteEncabezado = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 6, iTextSharp.text.Font.NORMAL);

                iTextSharp.text.pdf.PdfPCell celda1 = new iTextSharp.text.pdf.PdfPCell(new Phrase("CÓDIGO", fuenteEncabezado));
                celda1.Colspan = 1;
                celda1.Padding = 3;                
                celda1.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celda1.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                table.AddCell(celda1);

                iTextSharp.text.pdf.PdfPCell celda2 = new iTextSharp.text.pdf.PdfPCell(new Phrase("DESCRIPCIÓN", fuenteEncabezado));
                celda2.Colspan = 1;
                celda2.Padding = 3;                
                celda2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celda2.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                table.AddCell(celda2);               

                iTextSharp.text.pdf.PdfPCell celda3 = new iTextSharp.text.pdf.PdfPCell(new Phrase("CANTIDAD", fuenteEncabezado));
                celda3.Colspan = 1;
                celda3.Padding = 3;                
                celda3.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celda3.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                table.AddCell(celda3);               

                iTextSharp.text.pdf.PdfPCell celda5 = new iTextSharp.text.pdf.PdfPCell(new Phrase("PRECIO", fuenteEncabezado));
                celda5.Colspan = 1;
                celda5.Padding = 3;                
                celda5.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celda5.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                table.AddCell(celda5);

                iTextSharp.text.pdf.PdfPCell cellIVA = new iTextSharp.text.pdf.PdfPCell(new Phrase("% IVA", fuenteEncabezado));
                cellIVA.Colspan = 1;
                cellIVA.Padding = 3;                
                cellIVA.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                cellIVA.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                table.AddCell(cellIVA);

                iTextSharp.text.pdf.PdfPCell cellDcto = new iTextSharp.text.pdf.PdfPCell(new Phrase("% DCTO", fuenteEncabezado));
                cellDcto.Colspan = 1;
                cellDcto.Padding = 3;                
                cellDcto.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                cellDcto.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                table.AddCell(cellDcto);

                iTextSharp.text.pdf.PdfPCell celda6 = new iTextSharp.text.pdf.PdfPCell(new Phrase("TOTAL NETO", fuenteEncabezado));
                celda6.Colspan = 1;
                celda6.Padding = 3;                
                celda6.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celda6.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                table.AddCell(celda6);

                return true;
            }
            catch (Exception ex)
            {
                strError = "Error al crear columnas de tabla Items: " + ex.Message;
                return false;
            }
        }        

        private bool AddRowInvoiceTALSA(DataRow InvoiceLine, ref iTextSharp.text.pdf.PdfPTable table, iTextSharp.text.Font fuenteDatos, DataTable DtInvcTax)
        {
            try
            {
                //CODIGO
                iTextSharp.text.pdf.PdfPCell celCodigo = new iTextSharp.text.pdf.PdfPCell(new Phrase(InvoiceLine["PartNum"].ToString(), fuenteDatos));
                celCodigo.Colspan = 1;
                celCodigo.Padding = 3;
                celCodigo.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celCodigo.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                celCodigo.BorderWidthBottom = 0;
                celCodigo.BorderWidthTop = 0;
                celCodigo.BorderWidthRight = 0;
                table.AddCell(celCodigo);

                //DESCRIPCION
                iTextSharp.text.pdf.PdfPCell celDesc = new iTextSharp.text.pdf.PdfPCell(new Phrase(InvoiceLine["LineDesc"].ToString(), fuenteDatos));
                celDesc.Colspan = 1;
                celDesc.Padding = 3;
                celDesc.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                celDesc.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                celDesc.BorderWidthBottom = 0;
                celDesc.BorderWidthTop = 0;
                celDesc.BorderWidthRight = 0;
                table.AddCell(celDesc);               

                //CANTIDAD
                decimal Qty = Convert.ToDecimal(InvoiceLine["SellingShipQty"].ToString());
                iTextSharp.text.pdf.PdfPCell celCant = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:N0}", Qty), fuenteDatos));
                celCant.Colspan = 1;
                celCant.Padding = 3;
                celCant.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celCant.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                celCant.BorderWidthBottom = 0;
                celCant.BorderWidthTop = 0;
                celCant.BorderWidthRight = 0;
                table.AddCell(celCant);               

                //PRECIO
                decimal UnitPrice = Convert.ToDecimal(InvoiceLine["DocUnitPrice"].ToString());
                iTextSharp.text.pdf.PdfPCell cellUnitPrice = new iTextSharp.text.pdf.PdfPCell(new Phrase("$" + string.Format("{0:N2}", UnitPrice), fuenteDatos));
                cellUnitPrice.Colspan = 1;
                cellUnitPrice.Padding = 3;
                cellUnitPrice.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                cellUnitPrice.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                cellUnitPrice.BorderWidthBottom = 0;
                cellUnitPrice.BorderWidthTop = 0;
                cellUnitPrice.BorderWidthRight = 0;
                table.AddCell(cellUnitPrice);

                //IVA %
                decimal PercentIva = Convert.ToDecimal(GetPercentIdImpDIAN(InvoiceLine["InvoiceNum"].ToString(), InvoiceLine["InvoiceLine"].ToString(), DtInvcTax));
                iTextSharp.text.pdf.PdfPCell celIva = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:N1}", PercentIva), fuenteDatos));
                celIva.Colspan = 1;
                celIva.Padding = 3;
                celIva.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celIva.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                celIva.BorderWidthBottom = 0;
                celIva.BorderWidthTop = 0;
                celIva.BorderWidthRight = 0;
                table.AddCell(celIva);

                //DISCOUNT %
                iTextSharp.text.pdf.PdfPCell celDcto = new iTextSharp.text.pdf.PdfPCell(new Phrase(InvoiceLine["DiscountPercent"].ToString(), fuenteDatos));
                celDcto.Colspan = 1;
                celDcto.Padding = 3;
                celDcto.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celDcto.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                celDcto.BorderWidthBottom = 0;
                celDcto.BorderWidthTop = 0;
                celDcto.BorderWidthRight = 0;
                table.AddCell(celDcto);

                //EXT PRICE
                decimal PriceLine = Convert.ToDecimal(InvoiceLine["DocExtPrice"].ToString());
                iTextSharp.text.pdf.PdfPCell celExtPrise = new iTextSharp.text.pdf.PdfPCell(new Phrase("$" + string.Format("{0:N2}", PriceLine), fuenteDatos));
                celExtPrise.Colspan = 1;
                celExtPrise.Padding = 3;
                celExtPrise.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celExtPrise.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                celExtPrise.BorderWidthBottom = 0;
                celExtPrise.BorderWidthTop = 0;
                table.AddCell(celExtPrise);

                return true;
            }
            catch (Exception ex)
            {
                strError = "Error addLineInvoice: " + ex.Message;
                return false;
            }
        }

        //a.
        private bool AddInfoByLoteAndSerie(DataSet DsInvoiceAR,int LineNum, ref string PartDescLoteAndSerie)
        {
            try
            {
                //Buscamos Lotes por Linea de factura
                if (DsInvoiceAR.Tables.Contains("Detalle"))
                {
                    foreach (DataRow fila in DsInvoiceAR.Tables["Detalle"].Rows)
                    {
                        if (Convert.ToInt32(fila["Linea"]) == LineNum)
                        {
                            PartDescLoteAndSerie += fila["N_x00BA__x0020_Lote"].ToString()+"\n";                            
                        }
                    }
                }

                //Buscamos Series por Linea de factura
                if (DsInvoiceAR.Tables.Contains("Seriales_x0020_Facturados"))
                {
                    foreach (DataRow fila in DsInvoiceAR.Tables["Seriales_x0020_Facturados"].Rows)
                    {
                        if (Convert.ToInt32(fila["Linea"]) == LineNum)
                        {
                            PartDescLoteAndSerie += "   Nº Serie: " + fila["Serie"].ToString() +"\n";                            
                        }
                    }
                }

                return true;
            }
            catch (Exception)
            {
                return false;                                
            }
        }

        //b.
        private bool AddRowMaterialesTALSA(DataSet DsInvoiceAR,iTextSharp.text.Font fuenteDatos, ref iTextSharp.text.pdf.PdfPTable table)
        {
            try
            {

                //Buscamos Materiales asociados a la linea
                string PartNum=string.Empty;
                string PartDesc=string.Empty;
                decimal MtlQty=0;
                decimal MtlUnitPrice=0;
                decimal MtlExtPrice=0;

                if (DsInvoiceAR.Tables.Contains("Materiales"))
                {

                    if (DsInvoiceAR.Tables["Materiales"].Rows.Count > 0)
                    {
                        iTextSharp.text.pdf.PdfPCell celExtPrise = new iTextSharp.text.pdf.PdfPCell(new Phrase("Descripción de los Materiales", fuenteDatos));
                        celExtPrise.Colspan = 7;
                        celExtPrise.Padding = 3;
                        celExtPrise.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        celExtPrise.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                        celExtPrise.BorderWidthBottom = 0;
                        celExtPrise.BorderWidthTop = 0;
                        table.AddCell(celExtPrise);   
                    }

                    foreach (DataRow fila in DsInvoiceAR.Tables["Materiales"].Rows)
                    {
                        PartNum = fila["N_x00BA__x0020_Parte"].ToString();
                        PartDesc = fila["Description"].ToString();
                        MtlQty =Convert.ToDecimal(fila["Cantiadad"]); 
                        MtlUnitPrice = Convert.ToDecimal(fila["Precio_x0020_Unitario"]);
                        MtlExtPrice = MtlQty * MtlUnitPrice; //Convert.ToDecimal(fila["Precio_x0020_Unitario"]);

                        //CODIGO
                        iTextSharp.text.pdf.PdfPCell celCodigo = new iTextSharp.text.pdf.PdfPCell(new Phrase(PartNum, fuenteDatos));
                        celCodigo.Colspan = 1;
                        celCodigo.Padding = 3;
                        celCodigo.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        celCodigo.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                        celCodigo.BorderWidthBottom = 0;
                        celCodigo.BorderWidthTop = 0;
                        celCodigo.BorderWidthRight = 0;
                        table.AddCell(celCodigo);

                        //DESCRIPCION
                        iTextSharp.text.pdf.PdfPCell celDesc = new iTextSharp.text.pdf.PdfPCell(new Phrase(PartDesc, fuenteDatos));
                        celDesc.Colspan = 1;
                        celDesc.Padding = 3;
                        celDesc.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                        celDesc.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                        celDesc.BorderWidthBottom = 0;
                        celDesc.BorderWidthTop = 0;
                        celDesc.BorderWidthRight = 0;
                        table.AddCell(celDesc);

                        //CANTIDAD
                        //decimal Qty = Convert.ToDecimal(InvoiceLine["SellingShipQty"].ToString());
                        iTextSharp.text.pdf.PdfPCell celCant = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:N0}", MtlQty), fuenteDatos));
                        celCant.Colspan = 1;
                        celCant.Padding = 3;
                        celCant.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        celCant.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                        celCant.BorderWidthBottom = 0;
                        celCant.BorderWidthTop = 0;
                        celCant.BorderWidthRight = 0;
                        table.AddCell(celCant);

                        //PRECIO
                        //decimal UnitPrice = Convert.ToDecimal(InvoiceLine["DocUnitPrice"].ToString());
                        iTextSharp.text.pdf.PdfPCell cellUnitPrice = new iTextSharp.text.pdf.PdfPCell(new Phrase("$" + string.Format("{0:N2}", MtlUnitPrice), fuenteDatos));
                        cellUnitPrice.Colspan = 1;
                        cellUnitPrice.Padding = 3;
                        cellUnitPrice.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        cellUnitPrice.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                        cellUnitPrice.BorderWidthBottom = 0;
                        cellUnitPrice.BorderWidthTop = 0;
                        cellUnitPrice.BorderWidthRight = 0;
                        table.AddCell(cellUnitPrice);

                        //IVA %                        
                        iTextSharp.text.pdf.PdfPCell celIva = new iTextSharp.text.pdf.PdfPCell(new Phrase("", fuenteDatos));
                        celIva.Colspan = 1;
                        celIva.Padding = 3;
                        celIva.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        celIva.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                        celIva.BorderWidthBottom = 0;
                        celIva.BorderWidthTop = 0;
                        celIva.BorderWidthRight = 0;
                        table.AddCell(celIva);

                        //DISCOUNT %
                        iTextSharp.text.pdf.PdfPCell celDcto = new iTextSharp.text.pdf.PdfPCell(new Phrase(InvoiceLine["DiscountPercent"].ToString(), fuenteDatos));
                        celDcto.Colspan = 1;
                        celDcto.Padding = 3;
                        celDcto.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        celDcto.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                        celDcto.BorderWidthBottom = 0;
                        celDcto.BorderWidthTop = 0;
                        celDcto.BorderWidthRight = 0;
                        table.AddCell(celDcto);

                        //EXT PRICE
                        //decimal PriceLine = Convert.ToDecimal(InvoiceLine["DocExtPrice"].ToString());
                        iTextSharp.text.pdf.PdfPCell celExtPrise = new iTextSharp.text.pdf.PdfPCell(new Phrase("$" + string.Format("{0:N2}", MtlExtPrice), fuenteDatos));
                        celExtPrise.Colspan = 1;
                        celExtPrise.Padding = 3;
                        celExtPrise.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        celExtPrise.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                        celExtPrise.BorderWidthBottom = 0;
                        celExtPrise.BorderWidthTop = 0;
                        table.AddCell(celExtPrise);                                            
                    }
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        #endregion

        #endregion

        #region "Metodos Privados"

        public string GetLocalIPv4(NetworkInterfaceType _type)
        {
            string output = "";
            foreach (NetworkInterface item in NetworkInterface.GetAllNetworkInterfaces())
            {
                if (item.NetworkInterfaceType == _type && item.OperationalStatus == OperationalStatus.Up)
                {
                    foreach (UnicastIPAddressInformation ip in item.GetIPProperties().UnicastAddresses)
                    {
                        if (ip.Address.AddressFamily == AddressFamily.InterNetwork)
                        {
                            output = ip.Address.ToString();
                        }
                    }
                }
            }
            return output;
        }

        private bool AddColumnsInvoiceLine(ref iTextSharp.text.pdf.PdfPTable table)
        {
            try
            {
                iTextSharp.text.Font fuenteEncabezado = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 10, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.pdf.PdfPCell celda0 = new iTextSharp.text.pdf.PdfPCell(new Phrase("Linea", fuenteEncabezado));
                celda0.Colspan = 1;
                celda0.Padding = 3;
                celda0.BackgroundColor = iTextSharp.text.pdf.ExtendedColor.LIGHT_GRAY;
                celda0.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celda0.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                table.AddCell(celda0);

                iTextSharp.text.pdf.PdfPCell celda1 = new iTextSharp.text.pdf.PdfPCell(new Phrase("Código", fuenteEncabezado));
                celda1.Colspan = 1;
                celda1.Padding = 3;
                celda1.BackgroundColor = iTextSharp.text.pdf.ExtendedColor.LIGHT_GRAY;
                celda1.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celda1.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                table.AddCell(celda1);

                iTextSharp.text.pdf.PdfPCell celda2 = new iTextSharp.text.pdf.PdfPCell(new Phrase("Descripción", fuenteEncabezado));
                celda2.Colspan = 1;
                celda2.Padding = 3;
                celda2.BackgroundColor = iTextSharp.text.pdf.ExtendedColor.LIGHT_GRAY;
                celda2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celda2.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                table.AddCell(celda2);

                iTextSharp.text.pdf.PdfPCell celda3 = new iTextSharp.text.pdf.PdfPCell(new Phrase("Cantidad", fuenteEncabezado));
                celda3.Colspan = 1;
                celda3.Padding = 3;
                celda3.BackgroundColor = iTextSharp.text.pdf.ExtendedColor.LIGHT_GRAY;
                celda3.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celda3.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                table.AddCell(celda3);

                iTextSharp.text.pdf.PdfPCell celda4 = new iTextSharp.text.pdf.PdfPCell(new Phrase("%IVA", fuenteEncabezado));
                celda4.Colspan = 1;
                celda4.Padding = 3;
                celda4.BackgroundColor = iTextSharp.text.pdf.ExtendedColor.LIGHT_GRAY;
                celda4.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celda4.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                table.AddCell(celda4);

                iTextSharp.text.pdf.PdfPCell celda5 = new iTextSharp.text.pdf.PdfPCell(new Phrase("%DCTO", fuenteEncabezado));
                celda5.Colspan = 1;
                celda5.Padding = 3;
                celda5.BackgroundColor = iTextSharp.text.pdf.ExtendedColor.LIGHT_GRAY;
                celda5.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celda5.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                table.AddCell(celda5);

                iTextSharp.text.pdf.PdfPCell celda6 = new iTextSharp.text.pdf.PdfPCell(new Phrase("TOTAL", fuenteEncabezado));
                celda6.Colspan = 1;
                celda6.Padding = 3;
                celda6.BackgroundColor = iTextSharp.text.pdf.ExtendedColor.LIGHT_GRAY;
                celda6.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celda6.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                table.AddCell(celda6);

                return true;
            }
            catch (Exception ex)
            {
                strError = "Error al crear columnas de tabla Items: " + ex.Message;
                return false;
            }
        }

        private bool AddRowInvoice(DataRow InvoiceLine, ref iTextSharp.text.pdf.PdfPTable table, iTextSharp.text.Font fuenteDatos, DataTable DtInvcTax)
        {
            try
            {
                iTextSharp.text.pdf.PdfPCell celLinea = new iTextSharp.text.pdf.PdfPCell(new Phrase(InvoiceLine["InvoiceLine"].ToString(), fuenteDatos));
                celLinea.Colspan = 1;
                celLinea.Padding = 3;
                celLinea.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celLinea.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                table.AddCell(celLinea);

                iTextSharp.text.pdf.PdfPCell celCodigo = new iTextSharp.text.pdf.PdfPCell(new Phrase(InvoiceLine["PartNum"].ToString(), fuenteDatos));
                celCodigo.Colspan = 1;
                celCodigo.Padding = 3;
                celCodigo.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celCodigo.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                table.AddCell(celCodigo);

                iTextSharp.text.pdf.PdfPCell celDesc = new iTextSharp.text.pdf.PdfPCell(new Phrase(InvoiceLine["LineDesc"].ToString(), fuenteDatos));
                celDesc.Colspan = 1;
                celDesc.Padding = 3;
                celDesc.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celDesc.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                table.AddCell(celDesc);

                decimal Qty = Convert.ToDecimal(InvoiceLine["SellingShipQty"].ToString());
                iTextSharp.text.pdf.PdfPCell celCant = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:N0}", Qty), fuenteDatos));
                celCant.Colspan = 1;
                celCant.Padding = 3;
                celCant.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celCant.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                table.AddCell(celCant);

                decimal Percent = Convert.ToDecimal(GetPercentIdImpDIAN(InvoiceLine["InvoiceNum"].ToString(), InvoiceLine["InvoiceLine"].ToString(), DtInvcTax));
                iTextSharp.text.pdf.PdfPCell celIva = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:N1}", Percent), fuenteDatos));
                celIva.Colspan = 1;
                celIva.Padding = 3;
                celIva.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celIva.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                table.AddCell(celIva);

                iTextSharp.text.pdf.PdfPCell celDcto = new iTextSharp.text.pdf.PdfPCell(new Phrase(InvoiceLine["DiscountPercent"].ToString(), fuenteDatos));
                celDcto.Colspan = 1;
                celDcto.Padding = 3;
                celDcto.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celDcto.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                table.AddCell(celDcto);

                decimal PriceLine = Convert.ToDecimal(InvoiceLine["DocExtPrice"].ToString());
                iTextSharp.text.pdf.PdfPCell celExtPrise = new iTextSharp.text.pdf.PdfPCell(new Phrase("$" + string.Format("{0:N2}", PriceLine), fuenteDatos));
                celExtPrise.Colspan = 1;
                celExtPrise.Padding = 3;
                celExtPrise.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celExtPrise.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                table.AddCell(celExtPrise);

                return true;
            }
            catch (Exception ex)
            {
                strError = "Error addLineInvoice: " + ex.Message;
                return false;
            }
        }

        private bool AddTotalesInvoice(ref iTextSharp.text.pdf.PdfPTable tableTotales, DataSet DsInvoiceAR)
        {
            decimal SubTotal = 0;
            decimal Iva = 0;
            string Curency;
            decimal Retenciones = 0;
            decimal Total = 0;

            try
            {
                iTextSharp.text.Font fuenteTitle = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 8, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fuenteDatos = FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL);

                PdfPCell cellSubTotal = new PdfPCell(new Phrase("SUBTOTAL", fuenteTitle));
                cellSubTotal.Colspan = 2;
                cellSubTotal.HorizontalAlignment = Element.ALIGN_LEFT;
                tableTotales.AddCell(cellSubTotal);

                SubTotal = Convert.ToDecimal(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubTotal"].ToString());
                PdfPCell cellSubTotalValue = new PdfPCell(new Phrase("$" + string.Format("{0:N2}", SubTotal), fuenteDatos));
                cellSubTotalValue.HorizontalAlignment = Element.ALIGN_RIGHT;
                tableTotales.AddCell(cellSubTotalValue);

                PdfPCell cellIva = new PdfPCell(new Phrase("IVA", fuenteTitle));
                cellIva.Colspan = 2;
                cellIva.HorizontalAlignment = Element.ALIGN_LEFT;
                tableTotales.AddCell(cellIva);

                Iva = Convert.ToDecimal(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DocTaxAmt"].ToString());
                PdfPCell cellIvaValue = new PdfPCell(new Phrase("$" + string.Format("{0:N2}", Iva), fuenteDatos));
                cellIvaValue.HorizontalAlignment = Element.ALIGN_RIGHT;
                tableTotales.AddCell(cellIvaValue);

                PdfPCell cellReteIva = new PdfPCell(new Phrase("RETENCIONES", fuenteTitle));
                cellReteIva.Colspan = 2;
                cellReteIva.HorizontalAlignment = Element.ALIGN_LEFT;
                tableTotales.AddCell(cellReteIva);

                Retenciones = Convert.ToDecimal(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DocWHTaxAmt"].ToString());
                PdfPCell cellReteIvaValue = new PdfPCell(new Phrase("$ " + string.Format("{0:N2}", Retenciones), fuenteDatos));
                cellReteIvaValue.HorizontalAlignment = Element.ALIGN_RIGHT;
                tableTotales.AddCell(cellReteIvaValue);

                PdfPCell cellTotal = new PdfPCell(new Phrase("TOTAL", fuenteTitle));
                cellTotal.Colspan = 1;
                cellTotal.HorizontalAlignment = Element.ALIGN_LEFT;
                tableTotales.AddCell(cellTotal);

                Curency = DsInvoiceAR.Tables["InvcHead"].Rows[0]["CurrencyCodeCurrencyID"].ToString();
                PdfPCell cellCurrency = new PdfPCell(new Phrase(Curency, fuenteDatos));
                cellCurrency.Colspan = 1;
                cellCurrency.HorizontalAlignment = Element.ALIGN_LEFT;
                tableTotales.AddCell(cellCurrency);

                Total = Convert.ToDecimal(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"].ToString());
                PdfPCell cellTotalValue = new PdfPCell(new Phrase(string.Format("{0:N2}", Total), fuenteDatos));
                cellTotalValue.Colspan = 1;
                cellTotalValue.HorizontalAlignment = Element.ALIGN_RIGHT;
                tableTotales.AddCell(cellTotalValue);

                return true;
            }
            catch (Exception ex)
            {
                strError = "Error al add Totales Facturas: " + ex.Message;
                return false;
            }
        }        

        private void PieDePagina(ref PdfContentByte Pcb)
        {
            try
            {
                //Pie de pagina  iTextSharp.text.pdf.ExtendedColor.LIGHT_GRAY;
                Pcb.BeginText();
                Pcb.SetFontAndSize(BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.WINANSI, false), 8);
                Pcb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "©Copy right " + DateTime.Now.Year + ". Documento creado por bythewave.co sujeto a condiciones y restricciones", 300, 15, 0);
                Pcb.EndText();
            }
            catch (Exception)
            {
            }
        }

        private string GetPercentIdImpDIAN(string InvoiceNum, string InvoiceLine, DataTable DtInvcTax)
        {
            string Percent = "0";

            try
            {
                DataRow[] RowsImpuestos = DtInvcTax.Select("InvoiceNum='" + InvoiceNum + "' and InvoiceLine='" + InvoiceLine + "'");

                if (RowsImpuestos != null && RowsImpuestos.GetLength(0) > 0)
                { 
                    foreach (var Tax in RowsImpuestos)
                    {                     
                        //Percent = string.Format("{0:N}", RowsImpuestos[0]["Percent"]);
                        if (Tax["TaxDescription"].ToString().ToUpper().Contains("IVA"))
                        {
                            Percent = string.Format("{0:N}", Tax["Percent"]);
                            break;
                        }
                    }

                }

                return Percent;
            }
            catch (Exception ex)
            {
                strError = "GetPercentIdImpDIAN Error: " + ex.Message;
                return string.Empty;
            }
        }

        private decimal GetValImpuestoByID(string IdImPDIAN, DataSet DsInvcHed)
        {
            decimal ValImp = 0;

            try
            {
                //Trasa += "Valor Impuesto ID" + IdImPDIAN + "\n";
                //Recorremos los Impuestos y acumulamos por IdImpDIAN.                
                foreach (DataRow RowTax in DsInvcHed.Tables["InvcTax"].Rows)
                {
                    //Buscamos el IdImpDIAN por el RateCode del InvcTax.      
                    DataRow[] RowImpDIAN = DsInvcHed.Tables["SalesTRC"].Select("RateCode='" + RowTax["RateCode"].ToString() + "'");
                    if (RowImpDIAN != null && RowImpDIAN.GetLength(0) > 0 && RowImpDIAN[0]["IdImpDIAN_c"].ToString() == IdImPDIAN)
                    {
                        //Trasa += "RateCode =" + RowTax["RateCode"].ToString() + "IdImpDIAN =" + RowImpDIAN[0]["IdImpDIAN_c"].ToString() + " Valor" + Convert.ToDecimal(RowTax["DocTaxAmt"]) + "\n";                        
                        ValImp += Convert.ToDecimal(RowTax["DocTaxAmt"]);
                    }
                }
                
                return ValImp;
            }
            catch (Exception)
            {
                return 0;
            }
        }        


        public string toText(double value)
        {
            string Num2Text = "";
            value = Math.Truncate(value);
            if (value == 0) Num2Text = "CERO";
            else if (value == 1) Num2Text = "UNO";
            else if (value == 2) Num2Text = "DOS";
            else if (value == 3) Num2Text = "TRES";
            else if (value == 4) Num2Text = "CUATRO";
            else if (value == 5) Num2Text = "CINCO";
            else if (value == 6) Num2Text = "SEIS";
            else if (value == 7) Num2Text = "SIETE";
            else if (value == 8) Num2Text = "OCHO";
            else if (value == 9) Num2Text = "NUEVE";
            else if (value == 10) Num2Text = "DIEZ";
            else if (value == 11) Num2Text = "ONCE";
            else if (value == 12) Num2Text = "DOCE";
            else if (value == 13) Num2Text = "TRECE";
            else if (value == 14) Num2Text = "CATORCE";
            else if (value == 15) Num2Text = "QUINCE";
            else if (value < 20) Num2Text = "DIECI" + toText(value - 10);
            else if (value == 20) Num2Text = "VEINTE";
            else if (value < 30) Num2Text = "VEINTI" + toText(value - 20);
            else if (value == 30) Num2Text = "TREINTA";
            else if (value == 40) Num2Text = "CUARENTA";
            else if (value == 50) Num2Text = "CINCUENTA";
            else if (value == 60) Num2Text = "SESENTA";
            else if (value == 70) Num2Text = "SETENTA";
            else if (value == 80) Num2Text = "OCHENTA";
            else if (value == 90) Num2Text = "NOVENTA";
            else if (value < 100) Num2Text = toText(Math.Truncate(value / 10) * 10) + " Y " + toText(value % 10);
            else if (value == 100) Num2Text = "CIEN";
            else if (value < 200) Num2Text = "CIENTO " + toText(value - 100);
            else if ((value == 200) || (value == 300) || (value == 400) || (value == 600) || (value == 800)) Num2Text = toText(Math.Truncate(value / 100)) + "CIENTOS";
            else if (value == 500) Num2Text = "QUINIENTOS";
            else if (value == 700) Num2Text = "SETECIENTOS";
            else if (value == 900) Num2Text = "NOVECIENTOS";
            else if (value < 1000) Num2Text = toText(Math.Truncate(value / 100) * 100) + " " + toText(value % 100);
            else if (value == 1000) Num2Text = "MIL";
            else if (value < 2000) Num2Text = "MIL " + toText(value % 1000);
            else if (value < 1000000)
            {
                Num2Text = toText(Math.Truncate(value / 1000)) + " MIL";
                if ((value % 1000) > 0) Num2Text = Num2Text + " " + toText(value % 1000);
            }

            else if (value == 1000000) Num2Text = "UN MILLON";
            else if (value < 2000000) Num2Text = "UN MILLON " + toText(value % 1000000);
            else if (value < 1000000000000)
            {
                Num2Text = toText(Math.Truncate(value / 1000000)) + " MILLONES ";
                double truncate = Math.Truncate(value / 1000000) * 1000000;
                if ((value - truncate) > 0) Num2Text = Num2Text + " " + toText(value - truncate);
            }

            else if (value == 1000000000000) Num2Text = "UN BILLON";
            else if (value < 2000000000000) Num2Text = "UN BILLON " + toText(value - Math.Truncate(value / 1000000000000) * 1000000000000);

            else
            {
                Num2Text = toText(Math.Truncate(value / 1000000000000)) + " BILLONES";
                if ((value - Math.Truncate(value / 1000000000000)) > 0) Num2Text = Num2Text + " " + toText(value - Math.Truncate(value / 1000000000000));
            }
            return Num2Text;
        }

        #endregion

        #region "Ejemplos Crear Documentos PDF"

        //1.
        public void PdfConParrafos()
        {

            string NomArchivo = string.Empty;
            string Ruta = AppDomain.CurrentDomain.BaseDirectory.Replace("\\", "/") + "TEST/PDF/";
            NomArchivo = "/TestPdf.pdf";

            try
            {
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);


                //Dimenciones del documento.
                Document Documento = new Document(iTextSharp.text.PageSize.LETTER);
                //Se crea el documento.                
                PdfWriter Writer = PdfWriter.GetInstance(Documento, new FileStream(Ruta + NomArchivo, FileMode.Create));

                Documento.Open();

                Paragraph pr1 = new Paragraph();
                pr1.Add("Linea 1\nLinea2\nLinea3\nLinea4\nFinal Lineas");
                pr1.Alignment = PdfContentByte.ALIGN_LEFT;
                pr1.PaddingTop = 10;

                Paragraph pr2 = new Paragraph();
                pr2.Add("Linea 1\nLinea2\nLinea3\nLinea4\nFinal Lineas");
                pr2.Alignment = PdfContentByte.ALIGN_CENTER;
                pr2.PaddingTop = 10;

                Paragraph pr3 = new Paragraph();
                pr3.Add("Linea 1\nLinea2\nLinea3\nLinea4\nFinal Lineas");
                pr3.Alignment = PdfContentByte.ALIGN_RIGHT;
                pr3.PaddingTop = 10;

                Documento.Add(pr1);
                Documento.Add(pr2);
                Documento.Add(pr3);

                Writer.Flush();
                //Final de la tabla comparativa.

                //Final del documento pdf.                
                Documento.Close();
            }
            catch (Exception ex)
            {
                strError="Error al crear Pdf:" + ex.Message;
            }
        }

        //2.
        public void PdfCapitulosAndSections()
        {

            string NomArchivo = string.Empty;
            string Ruta = AppDomain.CurrentDomain.BaseDirectory.Replace("\\", "/") + "TEST/PDF/";
            NomArchivo = "/TestPdf.pdf";

            try
            {
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);


                //Dimenciones del documento.
                Document Documento = new Document(iTextSharp.text.PageSize.LETTER);
                //Se crea el documento.                
                PdfWriter Writer = PdfWriter.GetInstance(Documento, new FileStream(Ruta + NomArchivo, FileMode.Create));

                Documento.Open();

                iTextSharp.text.Font chapterFont = FontFactory.GetFont(FontFactory.HELVETICA, 24, iTextSharp.text.Font.NORMAL, new BaseColor(255, 0, 0));
                iTextSharp.text.Font sectionFont = FontFactory.GetFont(FontFactory.HELVETICA, 20, iTextSharp.text.Font.NORMAL, new BaseColor(0, 0, 255));
                iTextSharp.text.Font subsectionFont = FontFactory.GetFont(FontFactory.HELVETICA, 18, iTextSharp.text.Font.BOLD, new BaseColor(0, 64, 64));
                // we create some paragraphs
                Paragraph blahblah = new Paragraph("blah blah blah blah blah blah blaah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah");
                Paragraph blahblahblah = new Paragraph("blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blaah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah");
                // this loop will create 7 chapters
                for (int i = 1; i < 8; i++)
                {
                    Paragraph cTitle = new Paragraph("This is chapter " + i, chapterFont);
                    Chapter chapter = new Chapter(cTitle, i);

                    if (i == 4)
                    {
                        blahblahblah.Alignment = Element.ALIGN_JUSTIFIED;
                        blahblah.Alignment = Element.ALIGN_JUSTIFIED;
                        chapter.Add(blahblah);
                    }
                    if (i == 5)
                    {
                        blahblahblah.Alignment = Element.ALIGN_CENTER;
                        blahblah.Alignment = Element.ALIGN_RIGHT;
                        chapter.Add(blahblah);
                    }
                    // Add a table in the 6th chapter
                    if (i == 6)
                    {
                        blahblah.Alignment = Element.ALIGN_JUSTIFIED;
                    }
                    // in every chapter 3 sections will be Added
                    for (int j = 1; j < 4; j++)
                    {
                        Paragraph sTitle = new Paragraph("This is section " + j + " in chapter " + i, sectionFont);
                        Section section = chapter.AddSection(sTitle, 1);
                        // in all chapters except the 1st one, some extra text is Added to section 3
                        if (j == 3 && i > 1)
                        {
                            section.Add(blahblah);
                        }
                        // in every section 3 subsections are Added
                        for (int k = 1; k < 4; k++)
                        {
                            Paragraph subTitle = new Paragraph("This is subsection " + k + " of section " + j, subsectionFont);
                            Section subsection = section.AddSection(subTitle, 3);
                            if (k == 1 && j == 3)
                            {
                                subsection.Add(blahblahblah);
                            }
                            subsection.Add(blahblah);
                        }
                        if (j == 2 && i > 2)
                        {
                            section.Add(blahblahblah);
                        }
                    }

                    Documento.Add(chapter);
                }

                Writer.Flush();
                //Final de la tabla comparativa.

                //Final del documento pdf.                
                Documento.Close();
            }
            catch (Exception ex)
            {
                strError="Error al crear Pdf:" + ex.Message;
            }
        }

        //3.
        public void PdfTable1()
        {
            string NomArchivo = string.Empty;
            string Ruta = AppDomain.CurrentDomain.BaseDirectory.Replace("\\", "/") + "TEST/PDF/";
            NomArchivo = "/TestPdf.pdf";

            try
            {
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);


                //Dimenciones del documento.
                Document document = new Document(iTextSharp.text.PageSize.LETTER);

                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));

                // step 3: we open the document
                document.Open();

#if true
                iTextSharp.text.pdf.PdfPTable table = new iTextSharp.text.pdf.PdfPTable(3);
                iTextSharp.text.pdf.PdfPCell cell = new iTextSharp.text.pdf.PdfPCell();
                cell.Colspan = 3;
                table.AddCell(cell);
                iTextSharp.text.pdf.PdfPCell cell2 = new iTextSharp.text.pdf.PdfPCell();
                cell.Rowspan = 2;
                cell.BorderColor = new BaseColor(255, 0, 0);
                table.AddCell(cell);
                table.AddCell("1.1");
                table.AddCell("2.1");
                table.AddCell("1.2");
                table.AddCell("2.2");
                table.AddCell("cell test1");
                iTextSharp.text.pdf.PdfPCell cell3 = new iTextSharp.text.pdf.PdfPCell();
                cell3.Rowspan = 2;
                cell3.Colspan = 2;
                cell3.BackgroundColor = new BaseColor(0xC0, 0xC0, 0xC0);
                table.AddCell(cell);
                table.AddCell("cell test2");
                document.Add(new Paragraph("repeating the same table 10 times, but with different offsets:"));
                document.Add(table);
                document.Add(new Paragraph("blah blah."));
                document.Add(table);
                document.Add(new Paragraph("we increase the offset."));
                document.Add(table);
                document.Add(new Paragraph("blah blah."));
                document.Add(table);
                document.Add(new Paragraph("blah blah."));
                document.Add(table);
                document.Add(new Paragraph("we use an offset 0."));
                document.Add(table);
                document.Add(new Paragraph("blah blah."));
                document.Add(table);
                document.Add(new Paragraph("blah blah."));
                document.Add(table);
                document.Add(new Paragraph("A negative offset."));
                document.Add(table);
                document.Add(new Paragraph("blah blah."));
                document.Add(table);
#endif

                document.Close();

            }
            catch (DocumentException)
            {

            }
            catch (IOException )
            {

            }


        }

        //4
        public void PdfTextField()
        {
            string NomArchivo = string.Empty;
            string Ruta = AppDomain.CurrentDomain.BaseDirectory.Replace("\\", "/") + "TEST/PDF/";
            NomArchivo = "/TestPdf.pdf";

            try
            {
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);


                //Dimenciones del documento.
                Document document = new Document(iTextSharp.text.PageSize.LETTER);

                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));

                // step 3: we open the document
                document.Open();

                iTextSharp.text.Rectangle rect1 = new iTextSharp.text.Rectangle(100, 100);
                iTextSharp.text.Rectangle rect2 = new iTextSharp.text.Rectangle(100, 100);
                iTextSharp.text.Rectangle rect3 = new iTextSharp.text.Rectangle(100, 100);

                iTextSharp.text.pdf.TextField txtAdquiriente = new iTextSharp.text.pdf.TextField(writer, rect1, "Adquiriente");
                txtAdquiriente.Text = "Linea1\nLinea2\nLine3\n....";
                txtAdquiriente.Alignment = PdfContentByte.ALIGN_LEFT;

                iTextSharp.text.pdf.TextField txt2 = new iTextSharp.text.pdf.TextField(writer, rect2, "Section 2");
                txt2.Text = "Linea1\nLinea2\nLine3\n....";
                txt2.Alignment = PdfContentByte.ALIGN_CENTER;

                iTextSharp.text.pdf.TextField txt3 = new iTextSharp.text.pdf.TextField(writer, rect3, "Section 3");
                txt3.Text = "Linea1\nLinea2\nLine3\n....";
                txt3.Alignment = PdfContentByte.ALIGN_RIGHT;

                document.Add(rect1);
                document.Add(rect2);
                document.Add(rect3);

                document.Close();

            }
            catch (Exception)
            {

            }



        }

        //5.
        public void PdfDiv()
        {
            string NomArchivo = string.Empty;
            string Ruta = AppDomain.CurrentDomain.BaseDirectory.Replace("\\", "/") + "TEST/PDF/";
            NomArchivo = "/TestPdf.pdf";

            try
            {
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);


                //Dimenciones del documento.
                Document document = new Document(iTextSharp.text.PageSize.LETTER);
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;

                // step 3: we open the document
                document.Open();

                PdfDiv divLogo = new PdfDiv();
                divLogo.Float = iTextSharp.text.pdf.PdfDiv.FloatType.LEFT;
                divLogo.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divLogo.BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID;
                divLogo.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                divLogo.Height = 100;
                divLogo.Width = 90;
                divLogo.BackgroundImage = iTextSharp.text.Image.GetInstance(@"C:\Users\NelsonBTW\Desktop\App_InformarAdquiriente\App_InformarAdquiriente\IMG\LogoTalsa.png");

                PdfDiv divTitle = new PdfDiv();
                divTitle.Float = iTextSharp.text.pdf.PdfDiv.FloatType.LEFT;
                divTitle.Position = iTextSharp.text.pdf.PdfDiv.PositionType.STATIC;
                divTitle.BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID;
                divTitle.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                divTitle.Height = 100;
                divTitle.Width = 360;
                divTitle.BackgroundColor = BaseColor.WHITE;
                iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 10, iTextSharp.text.Font.NORMAL);
                Paragraph prgTitle = new Paragraph("FACTURA ELECTRONICA CI TALSA TEGNOLOGIA ALIMENTARIA\nREALIZADA POR CI TALSA NIT 800.027.374-9", fontTitle);
                prgTitle.Alignment = Element.ALIGN_CENTER;
                divTitle.AddElement(prgTitle);

                PdfDiv divLegalNum = new PdfDiv();
                divLegalNum.Float = iTextSharp.text.pdf.PdfDiv.FloatType.LEFT;
                divLegalNum.Position = iTextSharp.text.pdf.PdfDiv.PositionType.STATIC;
                divLegalNum.BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID;
                divLegalNum.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                divLegalNum.Height = 100;
                divLegalNum.Width = 90;
                divLegalNum.BackgroundColor = BaseColor.WHITE;
                //divLegalNum.BackgroundImage = iTextSharp.text.Image.GetInstance(@"C:\Users\NelsonBTW\Desktop\App_InformarAdquiriente\App_InformarAdquiriente\IMG\FV_Logo.png");
                iTextSharp.text.Font fontTitleLegalNum = FontFactory.GetFont(FontFactory.TIMES_BOLD, 9, iTextSharp.text.Font.BOLD);
                iTextSharp.text.Font fontLegalNum = FontFactory.GetFont(FontFactory.TIMES_BOLD, 9, iTextSharp.text.Font.NORMAL);
                Paragraph prgTitleLegalNum = new Paragraph("FACTURA VENTA:\n", fontTitleLegalNum);
                prgTitleLegalNum.Alignment = Element.ALIGN_LEFT;
                Paragraph prgLegalNum = new Paragraph("10000001234", fontLegalNum);
                prgLegalNum.Alignment = Element.ALIGN_LEFT;
                divLegalNum.AddElement(prgTitleLegalNum);
                divLegalNum.AddElement(prgLegalNum);


                iTextSharp.text.Font fontAdquiriente = FontFactory.GetFont(FontFactory.HELVETICA, 9, iTextSharp.text.Font.NORMAL);
                PdfDiv divAdquiruente = new PdfDiv();
                divAdquiruente.Float = iTextSharp.text.pdf.PdfDiv.FloatType.LEFT;
                divAdquiruente.Position = iTextSharp.text.pdf.PdfDiv.PositionType.STATIC;
                divAdquiruente.BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID;
                divAdquiruente.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                divAdquiruente.Height = 100;
                divAdquiruente.Width = 200;
                divAdquiruente.BackgroundColor = BaseColor.WHITE;
                Paragraph prg1 = new Paragraph("SEÑORES:\nNombre Adquiriente\nDireccion\nCiudad\nDepartamento\nNIT adquiriente", fontAdquiriente);
                divAdquiruente.AddElement(prg1);

                iTextSharp.text.Font fontPedido = FontFactory.GetFont(FontFactory.HELVETICA, 9, iTextSharp.text.Font.NORMAL);
                PdfDiv divInfo = new PdfDiv();
                divInfo.Float = iTextSharp.text.pdf.PdfDiv.FloatType.LEFT;
                divInfo.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divInfo.BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID;
                divInfo.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                divInfo.Height = 100;
                divInfo.Width = 200;
                divInfo.BackgroundColor = BaseColor.WHITE;
                Paragraph prgPedido = new Paragraph("INFORMACION DEL PEDIDO", fontPedido);
                prgPedido.Alignment = Element.ALIGN_LEFT;
                divInfo.AddElement(prgPedido);

                PdfDiv divQR = new PdfDiv();
                divQR.Float = iTextSharp.text.pdf.PdfDiv.FloatType.RIGHT;
                divQR.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divQR.BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID;
                divQR.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                divQR.Height = 100;
                divQR.Width = 140;
                divQR.BackgroundColor = BaseColor.LIGHT_GRAY;
                iTextSharp.text.Image ImgQR = iTextSharp.text.Image.GetInstance(@"C:\Users\NelsonBTW\Desktop\App_InformarAdquiriente\App_InformarAdquiriente\IMG\ImagenQR.png");
                ImgQR.Alignment = Element.ALIGN_RIGHT;
                divQR.BackgroundImage = ImgQR;

                //Tabla - Items.
                iTextSharp.text.pdf.PdfPTable table = new iTextSharp.text.pdf.PdfPTable(7);
                table.HorizontalAlignment = Element.ALIGN_CENTER;

                //Dimenciones.
                float[] Dimencion = new float[7];
                Dimencion[0] = 6.0F;
                Dimencion[1] = 4.0F;
                Dimencion[2] = 4.0F;
                Dimencion[3] = 4.0F;
                Dimencion[4] = 4.0F;
                Dimencion[5] = 4.0F;
                Dimencion[6] = 4.0F;

                //Configuracion y creacion de la tabla Comparativa.
                table.TotalWidth = 540;//writer.DirectContent.PdfDocument.PageSize.Width;
                table.SetWidths(Dimencion);
                table.SpacingBefore = 1.0F;
                table.SpacingAfter = 1.0F;

                //Configuramos el tamano de las columnas.
                iTextSharp.text.Font fuenteEncabezado = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 10, iTextSharp.text.Font.NORMAL);
                for (int i = 0; i < 7; i++)
                {
                    iTextSharp.text.pdf.PdfPCell celda = new iTextSharp.text.pdf.PdfPCell(new Phrase("Columna " + i, fuenteEncabezado));
                    celda.Colspan = 1;
                    celda.Padding = 3;
                    celda.BackgroundColor = iTextSharp.text.pdf.ExtendedColor.LIGHT_GRAY;
                    celda.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                    celda.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    table.AddCell(celda);
                }

                //foreach (DataColumn oColumna in DtItems.Columns)
                //{
                //    iTextSharp.text.pdf.PdfPCell celda = new iTextSharp.text.pdf.PdfPCell(new Phrase(oColumna.Caption, fuenteEncabezado));
                //    celda.Colspan = 1;
                //    celda.Padding = 3;
                //    celda.BackgroundColor = iTextSharp.text.pdf.ExtendedColor.LIGHT_GRAY;
                //    celda.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                //    celda.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                //    table.AddCell(celda);
                //}

                //Filas 
                iTextSharp.text.Font fuenteDatos = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 8, iTextSharp.text.Font.NORMAL);
                //foreach (DataRow oFila in DtItems.Rows)
                for (int i = 0; i < 100; i++)
                {
                    //foreach (DataColumn oColumna in DtItems.Columns)
                    for (int j = 0; j < 7; j++)
                    {
                        iTextSharp.text.pdf.PdfPCell celda = new iTextSharp.text.pdf.PdfPCell(new Phrase("Dato" + i, fuenteDatos));

                        celda.Colspan = 1;

                        celda.Padding = 3;

                        celda.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                        celda.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;

                        table.AddCell(celda);
                    }
                }

                table.HeadersInEvent = true;
                table.CompleteRow();

                iTextSharp.text.Font fontObs = FontFactory.GetFont(FontFactory.HELVETICA, 9, iTextSharp.text.Font.NORMAL);
                PdfDiv divObs = new PdfDiv();
                divObs.Float = iTextSharp.text.pdf.PdfDiv.FloatType.LEFT;
                divObs.Position = iTextSharp.text.pdf.PdfDiv.PositionType.STATIC;
                divObs.BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID;
                divObs.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                divObs.Height = 100;
                divObs.Width = 300;
                divObs.BackgroundColor = BaseColor.LIGHT_GRAY;
                Paragraph prgObs = new Paragraph("Observaciones de facturacion:", fontObs);
                prgObs.Alignment = Element.ALIGN_LEFT;
                divObs.AddElement(prgObs);

                iTextSharp.text.Font fontTotales = FontFactory.GetFont(FontFactory.HELVETICA, 9, iTextSharp.text.Font.NORMAL);
                PdfDiv divTotales = new PdfDiv();
                divTotales.Float = iTextSharp.text.pdf.PdfDiv.FloatType.LEFT;
                divTotales.Position = iTextSharp.text.pdf.PdfDiv.PositionType.STATIC;
                divTotales.BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID;
                divTotales.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                divTotales.Height = 100;
                divTotales.Width = 240;
                divTotales.BackgroundColor = BaseColor.DARK_GRAY;
                Paragraph prgTotales = new Paragraph("TOTALES:", fontObs);
                prgTotales.Alignment = Element.ALIGN_LEFT;
                divTotales.AddElement(prgTotales);

                document.Add(divLogo);
                document.Add(divTitle);
                document.Add(divLegalNum);
                document.Add(divAdquiruente);
                document.Add(divInfo);
                document.Add(divQR);
                document.Add(separator);
                document.Add(table);
                document.Add(separator);
                document.Add(divObs);
                document.Add(divTotales);

                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);
                writer.Flush();
                document.Close();

            }
            catch (Exception ex)
            {
                strError="Error al crear PDF: " + ex.Message;
            }
        }

        #endregion

    }
}
