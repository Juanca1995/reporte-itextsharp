
//Referenciar y usar.
using System.Web;
using System.Xml;
using System.Xml.Linq;
using System.Text;
using System.Net;


//Leer PDF
string RutaXmlInvoice = AppDomain.CurrentDomain.BaseDirectory.ToString() + "XmlInvoiceNum1617650.xml";                
XmlDocument XmlInvoice = new XmlDocument();
XmlInvoice.Load(RutaXmlInvoice);

//Convertir XML a Dataset.
private DataSet GenerarDataSet(string xmlfile)
{
	try
	{
		StringReader xmlSR = new StringReader(xmlfile);
		///Cargamos el DataSet..
		oDSXML.ReadXml(xmlSR);
		return oDSXML;
	}
	catch (Exception ex)
	{
		PeticionWCF.Tracer += "Estatus:Error en GenerarDataSet: " + ex.Message + " \n";
		PeticionWCF.Tracer += "Hora:" + DateTime.Now.ToString() + " \n\n";
		return null;
	}
}
