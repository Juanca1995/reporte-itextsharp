#region Formatos Facturacion BETANIA
        public bool FacturaNacionalBetania(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType)
        {
            NomArchivo = string.Empty;
            string Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            string RutaImg = $@"{AppDomain.CurrentDomain.BaseDirectory}\Logo_800027374.png";
            NomArchivo = NombreInvoice + ".pdf";

            try
            {
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.

                Document document = new Document(iTextSharp.text.PageSize.LETTER, 20f, 15f, 30f, 30f);
                //Document document = new Document(iTextSharp.text.PageSize.LETTER);
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;

                // step 3: we open the document     
                document.Open();

                PdfDiv divEspacio = new PdfDiv();
                divEspacio.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio.Height = 10;
                divEspacio.Width = 130;

                PdfDiv divEspacio2 = new PdfDiv();
                divEspacio2.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio2.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio2.Height = 2;
                divEspacio2.Width = 130;

                //FUENTES----------------------------------------------------------------------------------------------------------
                iTextSharp.text.Font fontTitleBold = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 8.5f, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA, 8.5f, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustom = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustomBold = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 6, iTextSharp.text.Font.NORMAL);
                //FUENTES---------------------------------------------------------------------------------------------------------

                #region ENCABEZADO
                PdfPTable tableEncabezado = new PdfPTable(new float[] { 0.745f, 2.75f, 1, 1.3f, 1.3f });
                tableEncabezado.WidthPercentage = 100;

                System.Drawing.Image logo = null;
                var requestLogo = WebRequest.Create($@"{AppDomain.CurrentDomain.BaseDirectory}\LogoBetania.png");

                using (var responseLogo = requestLogo.GetResponse())
                using (var streamLogo = responseLogo.GetResponseStream())
                {
                    logo = Bitmap.FromStream(streamLogo);
                }

                PdfDiv divLogo = new PdfDiv();
                divLogo.Float = iTextSharp.text.pdf.PdfDiv.FloatType.LEFT;
                divLogo.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divLogo.BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID;
                divLogo.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                divLogo.Height = 60;
                divLogo.Width = 60;
                //divLogo.BackgroundColor = BaseColor.LIGHT_GRAY;
                iTextSharp.text.Image ImgLogo = iTextSharp.text.Image.GetInstance(logo, BaseColor.WHITE);
                divLogo.BackgroundImage = ImgLogo;

                PdfPCell celLogo = new PdfPCell()
                {
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    VerticalAlignment = Element.ALIGN_CENTER,
                    Padding = 0,
                    Border = PdfPCell.NO_BORDER,
                };
                celLogo.AddElement(divLogo);
                tableEncabezado.AddCell(celLogo);

                PdfPCell celTextLacteosB = new PdfPCell(new Phrase("LACTEOS BETANIA S.A.",
                    FontFactory.GetFont(FontFactory.HELVETICA, 19, BaseColor.BLUE)))
                {
                    VerticalAlignment = Element.ALIGN_CENTER,
                    PaddingTop = 20,
                    Border = PdfPCell.NO_BORDER,
                };
                tableEncabezado.AddCell(celTextLacteosB);
                //---------------------------------------------------------------------------------
                PdfPCell celVacioEncabezado = new PdfPCell(new Phrase(" ")) { Border = PdfPCell.NO_BORDER, };

                PdfPTable tableInfoEmpresa1 = new PdfPTable(1);
                tableInfoEmpresa1.WidthPercentage = 100;

                Paragraph prgNit = new Paragraph($"Nit.    {(string)DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"]}", fontCustom);
                Paragraph prgDireccion = new Paragraph((string)DsInvoiceAR.Tables["Company"].Rows[0]["Address1"], fontCustom);
                Paragraph prgCity = new Paragraph((string)DsInvoiceAR.Tables["Company"].Rows[0]["City"], fontCustom);
                PdfPCell celInfoEmpresaNit = new PdfPCell()
                {
                    Border = PdfPCell.LEFT_BORDER,
                };
                celInfoEmpresaNit.AddElement(prgNit);
                celInfoEmpresaNit.AddElement(prgDireccion);
                celInfoEmpresaNit.AddElement(prgCity);
                tableInfoEmpresa1.AddCell(celInfoEmpresaNit);

                tableInfoEmpresa1.AddCell(celVacioEncabezado);

                PdfPCell celInfoEmpresa1 = new PdfPCell(tableInfoEmpresa1) { Border = PdfPCell.NO_BORDER, PaddingTop = 20 };
                tableEncabezado.AddCell(celInfoEmpresa1);
                //------------------------------------------------------------------------------------
                PdfPTable tableInfoEmpresa2 = new PdfPTable(2);
                tableInfoEmpresa2.WidthPercentage = 100;

                Paragraph prgIvaRComun = new Paragraph("IVA REGIMEN COMUN - CIIU 1040", fontCustom);
                PdfPCell celIvaRComun = new PdfPCell(prgIvaRComun) { Colspan = 2, Border = PdfPCell.LEFT_BORDER };
                tableInfoEmpresa2.AddCell(celIvaRComun);

                Paragraph prgTextFax = new Paragraph("FAX:", fontCustom);
                PdfPCell celTextFax = new PdfPCell(prgTextFax) { Border = PdfPCell.LEFT_BORDER, };
                tableInfoEmpresa2.AddCell(celTextFax);

                Paragraph prgValFax = new Paragraph($"{(string)DsInvoiceAR.Tables["Company"].Rows[0]["FaxNum"]}", fontCustom);
                prgValFax.Alignment = Element.ALIGN_RIGHT;
                PdfPCell celValFax = new PdfPCell(prgValFax) { Border = PdfPCell.NO_BORDER, HorizontalAlignment = Element.ALIGN_RIGHT, };
                tableInfoEmpresa2.AddCell(celValFax);

                Paragraph prgTextTel = new Paragraph("TELEFONO:", fontCustom);
                PdfPCell celTextTel = new PdfPCell(prgTextTel) { Border = PdfPCell.LEFT_BORDER, };
                tableInfoEmpresa2.AddCell(celTextTel);

                Paragraph prgValTel = new Paragraph($"{(string)DsInvoiceAR.Tables["Company"].Rows[0]["PhoneNum"]}", fontCustom);
                prgValTel.Alignment = Element.ALIGN_RIGHT;
                PdfPCell celValTel = new PdfPCell(prgValTel) { Border = PdfPCell.NO_BORDER, HorizontalAlignment = Element.ALIGN_RIGHT, };
                tableInfoEmpresa2.AddCell(celValTel);

                celVacioEncabezado.Colspan = 2;
                tableInfoEmpresa2.AddCell(celVacioEncabezado);
                celVacioEncabezado = null;

                PdfPCell celInfoEmpresa2 = new PdfPCell(tableInfoEmpresa2) { Border = PdfPCell.NO_BORDER, PaddingTop = 20 };
                tableEncabezado.AddCell(celInfoEmpresa2);
                //---------------------------------------------------------------------------
                Paragraph prgTextFacturaVenta = new Paragraph($"Factura de Venta\n{(string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceNum"]}",
                    fontTitleBold);
                prgTextFacturaVenta.Alignment = Element.ALIGN_RIGHT;
                Paragraph prgFechas = new Paragraph($"\nFechas: {DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).ToString("dd/mm/yyyy")}\n" +
                    $"Vence: {DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"]).ToString("dd/mm/yyyy")}", fontTitle);
                prgFechas.Alignment = Element.ALIGN_RIGHT;

                PdfPCell celDatosFacturaEncabezado = new PdfPCell()
                {
                    HorizontalAlignment = Element.ALIGN_RIGHT,
                    Border = PdfPCell.NO_BORDER,
                };
                celDatosFacturaEncabezado.AddElement(prgTextFacturaVenta);
                celDatosFacturaEncabezado.AddElement(prgFechas);
                tableEncabezado.AddCell(celDatosFacturaEncabezado);
                #endregion

                #region DATOS CUSTOMER
                PdfPTable tableDatosCliente = new PdfPTable(new float[] { 1, 2, 1, 1 });
                tableDatosCliente.WidthPercentage = 100;

                PdfPCell celTextVendidoA = new PdfPCell(new Phrase("Vendido A:", fontCustomBold)) { HorizontalAlignment = Element.ALIGN_CENTER };
                tableDatosCliente.AddCell(celTextVendidoA);

                PdfPCell celValVendidoA = new PdfPCell(new Phrase($"{(string)DsInvoiceAR.Tables["Customer"].Rows[0]["Name"]}",
                    fontTitle))
                { HorizontalAlignment = Element.ALIGN_LEFT
                };
                tableDatosCliente.AddCell(celValVendidoA);

                PdfPCell celTextNit = new PdfPCell(new Phrase("Nit./C.C.", fontCustomBold)) { HorizontalAlignment = Element.ALIGN_CENTER };
                tableDatosCliente.AddCell(celTextNit);

                PdfPCell celValNit = new PdfPCell(new Phrase($"{(string)DsInvoiceAR.Tables["Customer"].Rows[0]["ResaleID"]}",
                    fontTitle))
                {
                };
                tableDatosCliente.AddCell(celValNit);

                PdfPCell celTextAlmacen = new PdfPCell(new Phrase("Almacen", fontCustomBold)) { HorizontalAlignment = Element.ALIGN_CENTER };
                tableDatosCliente.AddCell(celTextAlmacen);

                PdfPCell celValAlmacen = new PdfPCell(new Phrase("{******}", fontTitle)) { HorizontalAlignment = Element.ALIGN_LEFT };
                tableDatosCliente.AddCell(celValAlmacen);

                PdfPCell celTextTelefono = new PdfPCell(new Phrase("Telefono", fontCustomBold)) { HorizontalAlignment = Element.ALIGN_CENTER };
                tableDatosCliente.AddCell(celTextTelefono);

                PdfPCell celValTelefono = new PdfPCell(new Phrase($"{(string)DsInvoiceAR.Tables["Customer"].Rows[0]["PhoneNum"]}", fontTitle)) { };
                tableDatosCliente.AddCell(celValTelefono);

                PdfPCell celTextDireccion = new PdfPCell(new Phrase($"Direccion", fontTitleBold)) { HorizontalAlignment = Element.ALIGN_CENTER, };
                tableDatosCliente.AddCell(celTextDireccion);

                PdfPCell celValDireccion = new PdfPCell(new Phrase($"{(string)DsInvoiceAR.Tables["Customer"].Rows[0]["Address1"]}", fontTitle)) { Colspan = 3, };
                tableDatosCliente.AddCell(celValDireccion);

                PdfPCell celTextRuta = new PdfPCell(new Phrase("Ruta", fontCustomBold)) { HorizontalAlignment = Element.ALIGN_CENTER };
                tableDatosCliente.AddCell(celTextRuta);

                PdfPCell celValRuta = new PdfPCell(new Phrase("{******}", fontTitle)) { };
                tableDatosCliente.AddCell(celValRuta);

                PdfPCell celTextZona = new PdfPCell(new Phrase("Zona", fontCustomBold)) { HorizontalAlignment = Element.ALIGN_CENTER };
                tableDatosCliente.AddCell(celTextZona);

                PdfPCell celValZona = new PdfPCell(new Phrase("{******}", fontTitle)) { };
                tableDatosCliente.AddCell(celValZona);

                PdfPCell celTextVendedor = new PdfPCell(new Phrase("Vendedor", fontCustomBold)) { HorizontalAlignment = Element.ALIGN_CENTER };
                tableDatosCliente.AddCell(celTextVendedor);

                PdfPCell celValVendedor = new PdfPCell(new Phrase("{******}", fontTitle)) { };
                tableDatosCliente.AddCell(celValVendedor);
                #endregion

                #region UNIDADES
                PdfPTable tableUnidades = new PdfPTable(new float[] { 1, 2, 0.7f, 0.5f, 1.3f, 1.4f });
                tableUnidades.WidthPercentage = 100;

                PdfPCell celTextReferencia = new PdfPCell(new Phrase("Referencia", fontCustomBold)) { Border = PdfPCell.NO_BORDER, HorizontalAlignment = Element.ALIGN_CENTER };
                tableUnidades.AddCell(celTextReferencia);

                PdfPCell celTextDesc = new PdfPCell(new Phrase("Descripcion del producto", fontCustomBold)) { Border = PdfPCell.NO_BORDER, HorizontalAlignment = Element.ALIGN_CENTER };
                tableUnidades.AddCell(celTextDesc);

                PdfPCell celTextCant = new PdfPCell(new Phrase("Cantidad", fontCustomBold)) { Border = PdfPCell.NO_BORDER, HorizontalAlignment = Element.ALIGN_CENTER };
                tableUnidades.AddCell(celTextCant);

                PdfPCell celTextIva = new PdfPCell(new Phrase("IVA(%)", fontCustomBold)) { Border = PdfPCell.NO_BORDER, HorizontalAlignment = Element.ALIGN_CENTER };
                tableUnidades.AddCell(celTextIva);

                PdfPCell celTextPrecio = new PdfPCell(new Phrase("Precio", fontCustomBold)) { Border = PdfPCell.NO_BORDER, HorizontalAlignment = Element.ALIGN_CENTER };
                tableUnidades.AddCell(celTextPrecio);

                PdfPCell celTextValor = new PdfPCell(new Phrase("Valor", fontCustomBold)) { Border = PdfPCell.NO_BORDER, HorizontalAlignment = Element.ALIGN_CENTER };
                tableUnidades.AddCell(celTextValor);

                decimal totalUnds = 0, totalSuma = 0, totalDcto = 0, totalSubTotal = 0;
                foreach (DataRow InvoiceLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
                {
                    totalUnds += decimal.Parse((string)InvoiceLine["SellingShipQty"]);
                    totalSuma += decimal.Parse((string)InvoiceLine["DocUnitPrice"]);
                    totalDcto += decimal.Parse((string)InvoiceLine["DocDiscount"]);
                    totalSubTotal += decimal.Parse((string)InvoiceLine["DspDocExtPrice"]);

                    if (!AddUnidadesBetania(InvoiceLine, ref tableUnidades, FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL), DsInvoiceAR))
                        return false;
                }
                #endregion

                #region OBSERVACIONES Y TOTALES
                PdfPTable tableTotales = new PdfPTable(9/*new float[] { 1, 2, 0.7f, 0.5f, 1.3f, 1.4f }*/);
                tableTotales.WidthPercentage = 100;

                iTextSharp.text.Image QRPdf = iTextSharp.text.Image.GetInstance(QRInvoice, BaseColor.WHITE);

                PdfDiv divQR = new PdfDiv()
                {
                    Float = iTextSharp.text.pdf.PdfDiv.FloatType.LEFT,
                    Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE,
                    BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID,
                    Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK,
                    Height = 85,
                    Width = 85,
                };
                divQR.AddElement(QRPdf);

                PdfPCell celImgQR = new PdfPCell()
                {
                    Padding = 3,
                    PaddingLeft = 15,
                    HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER,
                    VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP,
                    Colspan = 9,
                    Border = PdfPCell.NO_BORDER,
                };
                celImgQR.AddElement(divQR);
                tableTotales.AddCell(celImgQR);

                PdfPCell celTextUnds = new PdfPCell(new Phrase("UNDS", fontTitleBold)) { HorizontalAlignment = Element.ALIGN_RIGHT };
                tableTotales.AddCell(celTextUnds);

                PdfPCell celTextSuma = new PdfPCell(new Phrase("SUMA", fontTitleBold)) { HorizontalAlignment = Element.ALIGN_RIGHT };
                tableTotales.AddCell(celTextSuma);

                PdfPCell celTextDcto = new PdfPCell(new Phrase("Dcto Com.", fontTitleBold)) { HorizontalAlignment = Element.ALIGN_RIGHT };
                tableTotales.AddCell(celTextDcto);

                PdfPCell celTextSubTotal = new PdfPCell(new Phrase("Sub-total", fontTitleBold)) { HorizontalAlignment = Element.ALIGN_RIGHT };
                tableTotales.AddCell(celTextSubTotal);

                PdfPCell celTextTotalesIva = new PdfPCell(new Phrase("I.V.A", fontTitleBold)) { HorizontalAlignment = Element.ALIGN_RIGHT };
                tableTotales.AddCell(celTextTotalesIva);

                PdfPCell celTextRetIva = new PdfPCell(new Phrase("-Ret. I.V.A", fontTitleBold)) { HorizontalAlignment = Element.ALIGN_RIGHT };
                tableTotales.AddCell(celTextRetIva);

                PdfPCell celTextRetRenta = new PdfPCell(new Phrase("-Ret. Renta", fontTitleBold)) { HorizontalAlignment = Element.ALIGN_RIGHT };
                tableTotales.AddCell(celTextRetRenta);

                PdfPCell celTextFletes = new PdfPCell(new Phrase("Fletes", fontTitleBold)) { HorizontalAlignment = Element.ALIGN_RIGHT };
                tableTotales.AddCell(celTextFletes);

                PdfPCell celTextAPagar = new PdfPCell(new Phrase("A Pagar",
                    FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 9.3f, iTextSharp.text.Font.NORMAL)))
                { HorizontalAlignment = Element.ALIGN_RIGHT
                };
                tableTotales.AddCell(celTextAPagar);
                //--------------------------------------------------------------------------------------------------------------------------
                PdfPCell celValUnds = new PdfPCell(new Phrase(totalUnds.ToString(), fontTitle)) { HorizontalAlignment = Element.ALIGN_RIGHT };
                tableTotales.AddCell(celValUnds);

                PdfPCell celValSuma = new PdfPCell(new Phrase(totalSuma.ToString("N2"), fontTitle)) { HorizontalAlignment = Element.ALIGN_RIGHT };
                tableTotales.AddCell(celValSuma);

                PdfPCell celValDcto = new PdfPCell(new Phrase(totalDcto.ToString("N2"), fontTitle)) { HorizontalAlignment = Element.ALIGN_RIGHT };
                tableTotales.AddCell(celValDcto);

                PdfPCell celValSubTotal = new PdfPCell(new Phrase(totalSubTotal.ToString("N2"), fontTitle)) { HorizontalAlignment = Element.ALIGN_RIGHT };
                tableTotales.AddCell(celValSubTotal);

                PdfPCell celValTotalesIva = new PdfPCell(new Phrase(GetValImpuestoByID("01",DsInvoiceAR).ToString("N2"), 
                    fontTitle)) { HorizontalAlignment = Element.ALIGN_RIGHT };
                tableTotales.AddCell(celValTotalesIva);

                PdfPCell celValRetIva = new PdfPCell(new Phrase("{*****}", fontTitle)) { HorizontalAlignment = Element.ALIGN_RIGHT };
                tableTotales.AddCell(celValRetIva);

                PdfPCell celValRetRenta = new PdfPCell(new Phrase("{*****}", fontTitle)) { HorizontalAlignment = Element.ALIGN_RIGHT };
                tableTotales.AddCell(celValRetRenta);

                PdfPCell celValFletes = new PdfPCell(new Phrase("{*****}", fontTitle)) { HorizontalAlignment = Element.ALIGN_RIGHT };
                tableTotales.AddCell(celValFletes);

                PdfPCell celValAPagar = new PdfPCell(new Phrase(decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"]).ToString("N2"),
                    fontTitleBold))
                { HorizontalAlignment = Element.ALIGN_RIGHT };
                tableTotales.AddCell(celValAPagar);
                //-------------------------------------------------------------------------------------------------------------------------
                PdfPCell celTextValLetras = new PdfPCell(new Phrase($"Son: {Nroenletras((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"])}",
                    fontCustom)) { HorizontalAlignment = Element.ALIGN_LEFT, Colspan=7, };
                tableTotales.AddCell(celTextValLetras);

                PdfPCell celTextAceptadaPor = new PdfPCell(new Phrase("Aceptada por:",fontTitleBold))
                { HorizontalAlignment = Element.ALIGN_LEFT, Colspan = 2,BorderWidthBottom=PdfPCell.NO_BORDER };
                tableTotales.AddCell(celTextAceptadaPor);

                PdfPCell celTextOrderCompra = new PdfPCell(new Phrase($"ORDEN DE COMPRA #{(string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["PONum"]}",
                fontTitle))
                { HorizontalAlignment = Element.ALIGN_LEFT, Colspan = 7,PaddingTop=8, };
                tableTotales.AddCell(celTextOrderCompra);

                PdfPCell celTextNitCC = new PdfPCell(new Phrase("Nit / C.C", fontTitle)) {
                    HorizontalAlignment = Element.ALIGN_LEFT, Colspan=2,PaddingTop=15,BorderWidthTop=PdfPCell.NO_BORDER,
                };
                tableTotales.AddCell(celTextNitCC);

                PdfPCell celTextResolucion = new PdfPCell(new Phrase("Res. DIAN 110000689010 DE 2016-07-05 L 786678 A  L 900000", fontTitle))
                {
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    Colspan = 7,
                    PaddingTop=5,
                };
                tableTotales.AddCell(celTextResolucion);
                celVacioEncabezado = new PdfPCell();
                celVacioEncabezado.Border = PdfPCell.LEFT_BORDER | PdfPCell.RIGHT_BORDER | PdfPCell.TOP_BORDER;
                celVacioEncabezado.Colspan = 2;
                tableTotales.AddCell(celVacioEncabezado);
                celVacioEncabezado = null;
                //-----------------------------------------------------------------------------------------------------------------------------
                Paragraph prgGContrib = new Paragraph("SOMOS GRANDES CONTRIBUYENTES",fontTitleBold);
                Paragraph prgGContribDesc = new Paragraph("Esta factura de venta se asimila en todos sus efectos a un rirulo valor ley 1231  08 " +
                    "firma por tercero, en reprecentacion u otro calidad similar a nombre del comprador implica su obligación (ART.640 DE C.CIO)", fontCustom);
                prgGContribDesc.Alignment = Element.ALIGN_JUSTIFIED;

                PdfPCell celTextGContrib = new PdfPCell()
                {
                    HorizontalAlignment = Element.ALIGN_JUSTIFIED,
                    Colspan = 3,
                    PaddingRight=10,
                    //BorderWidthTop = PdfPCell.NO_BORDER,
                };
                celTextGContrib.AddElement(prgGContrib);
                celTextGContrib.AddElement(prgGContribDesc);
                tableTotales.AddCell(celTextGContrib);

                Paragraph prgR_IVA = new Paragraph("AGENTE RETENEDOR DEL I.V.A", fontTitleBold);
                Paragraph prgR_IVADesc = new Paragraph("Tambien autorizo expresamente para que en el caso de incumplimiento de la obligacion sea " +
                    "reportado a la base de datos de FENALCO (PROCREDITO) o a cualquier otra.", fontCustom);
                PdfPCell celTextR_IVA = new PdfPCell()
                {
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    Colspan = 3,
                    Border = PdfPCell.TOP_BORDER|PdfPCell.BOTTOM_BORDER,
                };
                celTextR_IVA.AddElement(prgR_IVA);
                celTextR_IVA.AddElement(prgR_IVADesc);
                tableTotales.AddCell(celTextR_IVA);

                celVacioEncabezado = new PdfPCell();
                celVacioEncabezado.Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER;
                tableTotales.AddCell(celVacioEncabezado);
                celVacioEncabezado = null;

                Paragraph prgNitCC = new Paragraph("\n\nNit / C.C", fontTitle);
                Paragraph prgAPor = new Paragraph("Aprobada por:", fontTitleBold);
                PdfPCell celTextAprobadaPor = new PdfPCell()
                { HorizontalAlignment = Element.ALIGN_LEFT, Colspan = 2, BorderWidthTop = PdfPCell.NO_BORDER,
                    Padding=0,PaddingLeft=3,
                };
                celTextAprobadaPor.AddElement(prgAPor);
                celTextAprobadaPor.AddElement(prgNitCC);
                tableTotales.AddCell(celTextAprobadaPor);
                #endregion

                document.Add(tableEncabezado);
                document.Add(divEspacio);
                document.Add(tableDatosCliente);
                document.Add(divEspacio);
                document.Add(divEspacio);
                document.Add(tableUnidades);
                var cant = Math.Abs(30-DsInvoiceAR.Tables["InvcDtl"].Rows.Count);
                for (int i = 0; i < cant; i++)
                    document.Add(divEspacio);

                document.Add(tableTotales);
                /*PIE DE PAGINA*/
                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return false;
            }
        }

        public bool FacturaNotaCreditoBetania(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType)
        {
            NomArchivo = string.Empty;
            string Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            string RutaImg = $@"{AppDomain.CurrentDomain.BaseDirectory}\Logo_800027374.png";
            NomArchivo = NombreInvoice + ".pdf";

            try
            {
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.

                Document document = new Document(iTextSharp.text.PageSize.LETTER, 20f, 15f, 30f, 30f);
                //Document document = new Document(iTextSharp.text.PageSize.LETTER);
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;

                // step 3: we open the document     
                document.Open();

                PdfDiv divEspacio = new PdfDiv();
                divEspacio.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio.Height = 10;
                divEspacio.Width = 130;

                PdfDiv divEspacio2 = new PdfDiv();
                divEspacio2.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio2.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = Bas eColor.LIGHT_GRAY;
                divEspacio2.Height = 2;
                divEspacio2.Width = 130;

                //FUENTES----------------------------------------------------------------------------------------------------------
                iTextSharp.text.Font fontTitleBold = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 8.5f, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA, 8.5f, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustom = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustomBold = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 6, iTextSharp.text.Font.NORMAL);
                //FUENTES---------------------------------------------------------------------------------------------------------

                BaseColor colorFondo = new BaseColor(245, 245, 245);

                #region DATOS CUSTOMER
                PdfPTable tableDatosCliente = new PdfPTable(new float[] { 1, 2, 1.2f, 2 });
                tableDatosCliente.WidthPercentage = 100;

                Paragraph prgNombreEmpresa = new Paragraph((string)DsInvoiceAR.Tables["Company"].Rows[0]["Name"],
                    FontFactory.GetFont(FontFactory.HELVETICA, 14f, iTextSharp.text.Font.NORMAL));
                prgNombreEmpresa.Alignment = Element.ALIGN_LEFT;
                Paragraph prgNitEmpresa = new Paragraph($"NIT: {(string)DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"]}", fontTitle);
                prgNitEmpresa.Alignment = Element.ALIGN_LEFT;

                PdfPCell celInfoEmpresa = new PdfPCell()
                {
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    Colspan = 2,
                    Padding=3,
                    PaddingBottom=8,
                    BackgroundColor = colorFondo,
                    BorderWidthRight = PdfPCell.NO_BORDER,
                };
                celInfoEmpresa.AddElement(prgNombreEmpresa);
                celInfoEmpresa.AddElement(prgNitEmpresa);
                tableDatosCliente.AddCell(celInfoEmpresa);

                Paragraph prgInfoFactura = new Paragraph(
                    $"Comprobante de Nota Credito\n{(string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceRef"]}",
                    FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 10f, iTextSharp.text.Font.NORMAL));
                prgInfoFactura.Alignment = Element.ALIGN_RIGHT;
                Paragraph prgFechaFaactura = new Paragraph(DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).ToString("dd/mm/yyyy"),
                    fontTitle);

                prgFechaFaactura.Alignment = Element.ALIGN_RIGHT;

                PdfPCell celInfoFactura = new PdfPCell()
                {
                    HorizontalAlignment = Element.ALIGN_RIGHT,
                    Colspan = 2,
                    Padding = 3,
                    PaddingBottom = 8,
                    BackgroundColor = colorFondo,
                    BorderWidthLeft = PdfPCell.NO_BORDER,
                };
                celInfoFactura.AddElement(prgInfoFactura);
                celInfoFactura.AddElement(prgFechaFaactura);
                tableDatosCliente.AddCell(celInfoFactura);
                //--------------------------------------------------------------------------------------------
                PdfPCell celTextNombreCliente = new PdfPCell(new Phrase("Nombre del Cliente",
                    fontTitle))
                {
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    BackgroundColor = colorFondo,
                    BorderWidthRight = PdfPCell.NO_BORDER,
                    Padding = 3,
                    Border = PdfPCell.LEFT_BORDER,
                };
                tableDatosCliente.AddCell(celTextNombreCliente);

                PdfPCell celValNombreCliente = new PdfPCell(new Phrase((string)DsInvoiceAR.Tables["Customer"].Rows[0]["Name"],
                    fontTitle))
                {
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    Border = PdfPCell.RIGHT_BORDER,
                    BackgroundColor = colorFondo,
                    Colspan = 3,
                    Padding = 3,
                };
                tableDatosCliente.AddCell(celValNombreCliente);

                PdfPCell celTextAlmacen = new PdfPCell(new Phrase("Almacen",
                    fontTitle))
                {
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    BackgroundColor = colorFondo,
                    //BorderWidthRight = PdfPCell.NO_BORDER,
                    Padding = 3,
                    Border = PdfPCell.LEFT_BORDER,
                };
                tableDatosCliente.AddCell(celTextAlmacen);

                PdfPCell celValAlmacen = new PdfPCell(new Phrase("{**********}",
                    fontTitle))
                {
                    HorizontalAlignment = Element.ALIGN_LEFT,/*Border=PdfPCell.NO_BORDER,*/
                    BackgroundColor = colorFondo,
                    Colspan = 3,
                    Border = PdfPCell.RIGHT_BORDER,
                    Padding = 3,
                };
                tableDatosCliente.AddCell(celValAlmacen);

                PdfPCell celTextDireccionCiudad = new PdfPCell(new Phrase("Dirección y Ciudad",
                    fontTitle))
                {
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    BackgroundColor = colorFondo,
                    Padding = 3,
                    Border = PdfPCell.LEFT_BORDER,
                };
                tableDatosCliente.AddCell(celTextDireccionCiudad);

                PdfPCell celValDireccionCiudad = new PdfPCell(new Phrase($"{(string)DsInvoiceAR.Tables["Customer"].Rows[0]["Address1"]} " +
                    $"{(string)DsInvoiceAR.Tables["Customer"].Rows[0]["City"]}",
                    fontTitle))
                {
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    BackgroundColor = colorFondo,
                    //BorderWidthRight = PdfPCell.NO_BORDER,
                    Colspan = 3,
                    Padding = 3,
                    Border = PdfPCell.RIGHT_BORDER,
                };
                tableDatosCliente.AddCell(celValDireccionCiudad);

                PdfPCell celTextNitCliente = new PdfPCell(new Phrase("Nit del Cliente",
                    fontTitle))
                {
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    Border = PdfPCell.LEFT_BORDER,
                    BackgroundColor = colorFondo,
                    Padding = 3,
                };
                tableDatosCliente.AddCell(celTextNitCliente);

                PdfPCell celValNitCliente = new PdfPCell(new Phrase((string)DsInvoiceAR.Tables["Customer"].Rows[0]["ResaleID"],
                    fontTitle))
                {
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    Border = PdfPCell.RIGHT_BORDER,
                    BackgroundColor = colorFondo,
                    Colspan = 3,
                    Padding = 3,
                };
                tableDatosCliente.AddCell(celValNitCliente);

                PdfPCell celTextVendedor = new PdfPCell(new Phrase("Vendedor",
                    fontTitle))
                {
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    BackgroundColor = colorFondo,
                    Colspan = 2,
                    Padding = 3,
                    Border = PdfPCell.LEFT_BORDER,
                };
                tableDatosCliente.AddCell(celTextVendedor);

                PdfPCell celTextRRenta = new PdfPCell(new Phrase("Ret.Renta",
                    fontTitle))
                {
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    Border = PdfPCell.RIGHT_BORDER,
                    BackgroundColor = colorFondo,
                    Colspan = 2,
                    Padding = 3,
                };
                tableDatosCliente.AddCell(celTextRRenta);

                PdfPCell celTextTel = new PdfPCell(new Phrase("Telefonos",
                    fontTitle))
                {
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    Border = PdfPCell.LEFT_BORDER,
                    BackgroundColor = colorFondo,
                    Padding = 3,
                };
                tableDatosCliente.AddCell(celTextTel);

                PdfPCell celValTel = new PdfPCell(new Phrase((string)DsInvoiceAR.Tables["Customer"].Rows[0]["PhoneNum"],
                    fontTitle))
                {
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    Border = PdfPCell.NO_BORDER,
                    BackgroundColor = colorFondo,
                    //Colspan = 3,
                    Padding = 3,
                };
                tableDatosCliente.AddCell(celValTel);

                PdfPCell celTextRIca = new PdfPCell(new Phrase($"Ret.ICA    {GetValImpuestoByID("03",DsInvoiceAR)}",
                    fontTitle))
                {
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    Border = PdfPCell.RIGHT_BORDER,
                    BackgroundColor = colorFondo,
                    Colspan = 2,
                    Padding = 3,
                };
                tableDatosCliente.AddCell(celTextRIca);

                PdfPCell celTextNoFactura = new PdfPCell(new Phrase("No. Factura",
                    fontTitle))
                {
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    Border = PdfPCell.LEFT_BORDER,
                    BackgroundColor = colorFondo,
                    Padding = 3,
                };
                tableDatosCliente.AddCell(celTextNoFactura);

                PdfPCell celValNoFactura = new PdfPCell(new Phrase((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceRef"],
                    fontTitle))
                {
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    Border = PdfPCell.RIGHT_BORDER,
                    BackgroundColor = colorFondo,
                    Colspan = 3,
                    Padding = 3,
                };
                tableDatosCliente.AddCell(celValNoFactura);

                PdfPCell celVacio = new PdfPCell(new Phrase(" "))
                {
                    Colspan = 4,
                    BorderWidthTop = PdfPCell.NO_BORDER,
                    BackgroundColor = colorFondo,
                };
                tableDatosCliente.AddCell(celVacio);

                #endregion

                #region UNIDADES
                PdfPTable tableEncabezadoUnidades = new PdfPTable(new float[] { 1, 4, 1.3f, 1.8f, 1.5f });
                tableEncabezadoUnidades.WidthPercentage = 100;

                PdfPCell celTextCodigo = new PdfPCell(new Phrase("Codigo", fontTitle))
                {
                    Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER | PdfPCell.LEFT_BORDER,
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    BackgroundColor = colorFondo,
                    Padding = 3
                };
                tableEncabezadoUnidades.AddCell(celTextCodigo);

                PdfPCell celTextDesc = new PdfPCell(new Phrase("Descripción", fontTitle))
                { Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER, HorizontalAlignment = Element.ALIGN_LEFT, BackgroundColor = colorFondo, Padding = 3 };
                tableEncabezadoUnidades.AddCell(celTextDesc);

                PdfPCell celTextCant = new PdfPCell(new Phrase("Cantidad.", fontTitle))
                { Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER, HorizontalAlignment = Element.ALIGN_LEFT, BackgroundColor = colorFondo, Padding = 3 };
                tableEncabezadoUnidades.AddCell(celTextCant);

                PdfPCell celTextVrU = new PdfPCell(new Phrase("VR/Unitario", fontTitle))
                {
                    Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER,
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    Padding = 3,
                    BackgroundColor = colorFondo
                };
                tableEncabezadoUnidades.AddCell(celTextVrU);

                PdfPCell celTextVrTotal = new PdfPCell(new Phrase("VR/Tota", fontTitle))
                {
                    Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER | PdfPCell.RIGHT_BORDER,
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    BackgroundColor = colorFondo,
                    Padding = 3
                };
                tableEncabezadoUnidades.AddCell(celTextVrTotal);


                foreach (DataRow InvoiceLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
                {
                    if (!AddUnidadesNotaCreditoBetania(InvoiceLine, ref tableEncabezadoUnidades, FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL), DsInvoiceAR))
                        return false;
                }
                #endregion

                #region CUENTA Y FIRMAS

                PdfPTable tableCuentas = new PdfPTable(new float[] { 1, 4, 1f, 1.5f, 1.5f });
                tableCuentas.WidthPercentage = 100;

                PdfPCell celTextCuenta = new PdfPCell(new Phrase("Cuenta", fontTitle))
                {
                    Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER | PdfPCell.LEFT_BORDER,
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    BackgroundColor = colorFondo,
                    Padding = 3
                };
                tableCuentas.AddCell(celTextCuenta);

                PdfPCell celTextDescripcion = new PdfPCell(new Phrase("Descripción", fontTitle))
                {
                    Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER ,
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    BackgroundColor = colorFondo,
                    Padding = 3
                };
                tableCuentas.AddCell(celTextDescripcion);

                PdfPCell celTextImp = new PdfPCell(new Phrase("Imp.", fontTitle))
                {
                    Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER ,
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    BackgroundColor = colorFondo,
                    Padding = 3
                };
                tableCuentas.AddCell(celTextImp);

                PdfPCell celTextValor = new PdfPCell(new Phrase("Valor", fontTitle))
                {
                    Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER ,
                    HorizontalAlignment = Element.ALIGN_RIGHT,
                    BackgroundColor = colorFondo,
                    Padding = 3
                };
                tableCuentas.AddCell(celTextValor);

                PdfPCell celTextNit = new PdfPCell(new Phrase("Nit", fontTitle))
                {
                    Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER | PdfPCell.RIGHT_BORDER,
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    BackgroundColor = colorFondo,
                    Padding = 3
                };
                tableCuentas.AddCell(celTextNit);

                for (int i = 0; i < 2; i++)
                {
                    AddCuentasBetania(null, ref tableCuentas, fontTitle, DsInvoiceAR);
                }
                //-----------------------------------------------------------------------------------------------
                PdfPTable tableFirmas = new PdfPTable(new float[] {1,1.5f,1 });
                tableFirmas.WidthPercentage = 100;

                Paragraph prgGuion = new Paragraph("_______________________") { Alignment=Element.ALIGN_CENTER};
                Paragraph prgElaborado = new Paragraph("Elaborado", fontCustom) { Alignment = Element.ALIGN_CENTER };

                PdfPCell celElaborado = new PdfPCell()
                {
                    Border = PdfPCell.BOTTOM_BORDER,
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    Padding = 3
                };
                celElaborado.AddElement(prgGuion);
                celElaborado.AddElement(prgElaborado);
                tableFirmas.AddCell(celElaborado);

                Paragraph prgAprobado = new Paragraph("Aprobado", fontCustom) { Alignment = Element.ALIGN_CENTER };
                //prgGuion.Alignment = Element.ALIGN_CENTER;
                PdfPCell celAprobado = new PdfPCell()
                {
                    Border = PdfPCell.BOTTOM_BORDER,
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    Padding = 3
                };
                celAprobado.AddElement(prgGuion);
                celAprobado.AddElement(prgAprobado);
                tableFirmas.AddCell(celAprobado);

                Paragraph prgFRecibido = new Paragraph("Firma de Recibido",fontCustom) { Alignment = Element.ALIGN_CENTER };
                //prgGuion.Alignment = Element.ALIGN_RIGHT;
                PdfPCell celFRecibido = new PdfPCell()
                {
                    Border = PdfPCell.BOTTOM_BORDER,
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    Padding = 3
                };
                celFRecibido.AddElement(prgGuion);
                celFRecibido.AddElement(prgFRecibido);
                tableFirmas.AddCell(celFRecibido);

                iTextSharp.text.Image QRPdf = iTextSharp.text.Image.GetInstance(QRInvoice, BaseColor.WHITE);

                PdfDiv divQR = new PdfDiv()
                {
                    Float = iTextSharp.text.pdf.PdfDiv.FloatType.RIGHT,
                    Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE,
                    BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID,
                    Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK,
                    Height = 85,
                    Width = 85,
                };
                divQR.AddElement(QRPdf);

                PdfPCell celImgQR = new PdfPCell()
                {
                    Padding = 3,
                    //PaddingLeft = 15,
                    HorizontalAlignment = iTextSharp.text.Element.ALIGN_RIGHT,
                    VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP,
                    Colspan = 3,
                    Border = PdfPCell.NO_BORDER,
                };
                celImgQR.AddElement(divQR);
                tableFirmas.AddCell(celImgQR);

                #endregion

                document.Add(tableDatosCliente);
                document.Add(tableEncabezadoUnidades);
                document.Add(divEspacio);
                document.Add(tableCuentas);
                document.Add(divEspacio);
                document.Add(divEspacio);
                document.Add(tableFirmas);

                /*PIE DE PAGINA*/
                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return false;
            }
        }

        private bool AddUnidadesBetania(DataRow dataLine, ref PdfPTable table, iTextSharp.text.Font fontCustom, DataSet dataSet)
        {
            try
            {

                PdfPCell celTextReferencia = new PdfPCell(new Phrase((string)dataLine["PartNum"], fontCustom)) { Border = PdfPCell.NO_BORDER };
                table.AddCell(celTextReferencia);

                PdfPCell celTextDesc = new PdfPCell(new Phrase((string)dataLine["LineDesc"], fontCustom)) { Border = PdfPCell.NO_BORDER };
                table.AddCell(celTextDesc);

                PdfPCell celTextCant = new PdfPCell(new Phrase((string)dataLine["SellingShipQty"], fontCustom)) { HorizontalAlignment = Element.ALIGN_RIGHT,Border=PdfPCell.NO_BORDER };
                table.AddCell(celTextCant);

                
                PdfPCell celTextIva = new PdfPCell(new Phrase(
                    GetPercentIdImpDIAN((string)dataLine["InvoiceNum"], (string)dataLine["InvoiceLine"], dataSet.Tables["InvcTax"]), 
                    fontCustom)) { HorizontalAlignment = Element.ALIGN_RIGHT, Border = PdfPCell.NO_BORDER };
                table.AddCell(celTextIva);

                PdfPCell celTextPrecio = new PdfPCell(new Phrase(decimal.Parse((string)dataLine["DocUnitPrice"]).ToString("N2"), 
                    fontCustom)) { HorizontalAlignment = Element.ALIGN_RIGHT, Border = PdfPCell.NO_BORDER };
                table.AddCell(celTextPrecio);

                PdfPCell celTextValor = new PdfPCell(new Phrase(decimal.Parse((string)dataLine["DspDocExtPrice"]).ToString("N2"), 
                    fontCustom)) { HorizontalAlignment = Element.ALIGN_RIGHT, Border = PdfPCell.NO_BORDER };
                table.AddCell(celTextValor);

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return false;
            }
        }

        private bool AddUnidadesNotaCreditoBetania(DataRow dataLine, ref PdfPTable table, iTextSharp.text.Font fontCustom, DataSet dataSet)
        {
            try
            {
                PdfPCell celTextCodigo = new PdfPCell(new Phrase((string)dataLine["PartNum"], fontCustom))
                {
                    Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER | PdfPCell.LEFT_BORDER,
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    Padding = 3
                };
                table.AddCell(celTextCodigo);

                PdfPCell celTextDesc = new PdfPCell(new Phrase((string)dataLine["LineDesc"], fontCustom))
                { Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER, HorizontalAlignment = Element.ALIGN_LEFT, Padding = 3 };
                table.AddCell(celTextDesc);

                PdfPCell celTextCant = new PdfPCell(new Phrase(decimal.Parse((string)dataLine["SellingShipQty"]).ToString("N2"), fontCustom))
                { Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER, HorizontalAlignment = Element.ALIGN_LEFT, Padding = 3 };
                table.AddCell(celTextCant);

                PdfPCell celTextVrU = new PdfPCell(new Phrase(decimal.Parse((string)dataLine["DocUnitPrice"]).ToString("N2"), fontCustom))
                { Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER, HorizontalAlignment = Element.ALIGN_LEFT, Padding = 3 };
                table.AddCell(celTextVrU);

                PdfPCell celTextVrTotal = new PdfPCell(new Phrase(decimal.Parse((string)dataLine["DspDocExtPrice"]).ToString("N2"), fontCustom))
                {
                    Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER | PdfPCell.RIGHT_BORDER,
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    Padding = 3
                };
                table.AddCell(celTextVrTotal);

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return false;
            }
        }

        private bool AddCuentasBetania(DataRow dataLine, ref PdfPTable table, iTextSharp.text.Font fontTitle, DataSet dataSet)
        {
            try
            {
                PdfPCell celTextCuenta = new PdfPCell(new Phrase("{****}", fontTitle))
                {
                    Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER | PdfPCell.LEFT_BORDER,
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    //BackgroundColor = colorFondo,
                    Padding = 3
                };
                table.AddCell(celTextCuenta);

                PdfPCell celTextDescripcion = new PdfPCell(new Phrase("{****}", fontTitle))
                {
                    Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER,
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    //BackgroundColor = colorFondo,
                    Padding = 3
                };
                table.AddCell(celTextDescripcion);

                PdfPCell celTextImp = new PdfPCell(new Phrase("{****}.", fontTitle))
                {
                    Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER,
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    //BackgroundColor = colorFondo,
                    Padding = 3
                };
                table.AddCell(celTextImp);

                PdfPCell celTextValor = new PdfPCell(new Phrase("{****}", fontTitle))
                {
                    Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER,
                    HorizontalAlignment = Element.ALIGN_RIGHT,
                    //BackgroundColor = colorFondo,
                    Padding = 3
                };
                table.AddCell(celTextValor);

                PdfPCell celTextNit = new PdfPCell(new Phrase("{****}", fontTitle))
                {
                    Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER | PdfPCell.RIGHT_BORDER,
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    //BackgroundColor = colorFondo,
                    Padding = 3
                };
                table.AddCell(celTextNit);

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return false;
            }
        }
        #endregion

        #endregion