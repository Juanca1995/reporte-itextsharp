﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
//Referenciar y usar.
using System.Data;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Web;
using System.Windows.Forms;

namespace RulesServicesDIAN2.Adquiriente
{
    public partial class pdfEstandarAR
    {

        #region FE Plasticos Union

        #region Factura de venta Plasticos uinion

        public bool FacturaNacionalInversionesDinastía(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {
            
            NomArchivo = string.Empty;

            string Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            string RutaImg = $@"{AppDomain.CurrentDomain.BaseDirectory}\Logo asteroides.png";
            //string Rutacertificado = $@"{AppDomain.CurrentDomain.BaseDirectory}\LOGO_PLASTICOS_AMBIENTALES.png";

            NomArchivo = NombreInvoice + ".pdf";

            try
            {
                #region Head
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.
                Document document = new Document(iTextSharp.text.PageSize.LETTER);
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;

                // step 3: we open the document     
                document.Open();

                PdfDiv divEspacio = new PdfDiv();
                divEspacio.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio.Height = 10;
                divEspacio.Width = 130;

                PdfDiv divEspacio2 = new PdfDiv();
                divEspacio2.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio2.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio2.Height = 2;
                divEspacio2.Width = 130;

                //FUENTES----------------------------------------------------------------------------------------------------------
                iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle2 = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustom = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);
                //FUENTES---------------------------------------------------------------------------------------------------------
                #endregion

                #region Header

                //agregamos informacion de empresa y tipo de factura 

                /// logo
                System.Drawing.Image Logo;
                System.Drawing.Image LogoBanner = null;
                var request = WebRequest.Create(RutaImg);
                using (var response = request.GetResponse())
                using (var stream = response.GetResponseStream())
                {
                    Logo = Bitmap.FromStream(stream);
                }
                var requestBanner = WebRequest.Create(RutaImg);
                using (var responseBanner = requestBanner.GetResponse())
                using (var streamBanner = responseBanner.GetResponseStream())
                {
                    LogoBanner = Bitmap.FromStream(streamBanner);
                }

                iTextSharp.text.Image LogoPdf2 = iTextSharp.text.Image.GetInstance(LogoBanner, BaseColor.WHITE);
                LogoPdf2.ScaleAbsolute(200f, 200f);
                LogoPdf2.Border = 0;

                iTextSharp.text.Image QRPdf = iTextSharp.text.Image.GetInstance(QRInvoice, BaseColor.WHITE);
                QRPdf.ScaleAbsolute(80f, 80f);
                QRPdf.Border = 0;

                //----------------------------------------------------------------------------------------------------
                iTextSharp.text.Font fontTitleFactura = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);

                // agregamos ---------------------------------------------------------------------------

                PdfPTable Header = new PdfPTable(new float[] { 3.0f, 0.1f, 0.1f, 1.5f, 2.0f, });
                Header.WidthPercentage = 100;
                //logo
                PdfPCell Cell_logo = new PdfPCell() { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, Colspan = 2, };
                //info de la empresa
                PdfPCell Cell_info = new PdfPCell(new Phrase("", fontTitle))
                {
                    VerticalAlignment = Element.ALIGN_TOP,
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    Border = 0,

                };

                //QR
                PdfPCell Cell_Qr = new PdfPCell() { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER };
                //ti po de factura 
                PdfPCell Cell_tipo = new PdfPCell() { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER };

                Cell_logo.AddElement(QRPdf);
                //Cell_Qr.AddElement(QRPdf);
                Cell_tipo.AddElement(TipoFacturaPlasticosUnion(DsInvoiceAR, InvoiceType));

                Header.AddCell(Cell_logo);
                Header.AddCell(Cell_info);
                Header.AddCell(Cell_Qr);
                Header.AddCell(Cell_tipo);

                #endregion

                #region Body
                //------------------------------------------------------------------------------------------------
                PdfPTable tableFacturar = new PdfPTable(3);
                //Dimenciones.
                float[] DimencionFacturar = new float[3];
                DimencionFacturar[0] = 1.0F;//
                DimencionFacturar[1] = 0.01F;//
                DimencionFacturar[2] = 1.0F;//

                tableFacturar.WidthPercentage = 100;
                tableFacturar.SetWidths(DimencionFacturar);
                //----------------------------------------------------------------------------------------------
                PdfPTable tableFacturarA = new PdfPTable(2);
                float[] DimencionFacturarA = new float[2];
                DimencionFacturarA[0] = 0.8F;//
                DimencionFacturarA[1] = 2.0F;//

                tableFacturarA.WidthPercentage = 100;
                tableFacturarA.SetWidths(DimencionFacturarA);

                iTextSharp.text.pdf.PdfPCell celDatosFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase("FACTURAR A:\n", fontTitleFactura));
                celDatosFacturarA.Colspan = 2;
                celDatosFacturarA.Padding = 3;
                celDatosFacturarA.Border = 0;
                celDatosFacturarA.BorderColorBottom = BaseColor.WHITE;
                celDatosFacturarA.HorizontalAlignment = Element.ALIGN_CENTER;
                celDatosFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celDatosFacturarA);

                iTextSharp.text.pdf.PdfPCell celTextClienteFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase("CLIENTE:", fontTitleFactura));
                celTextClienteFacturarA.Colspan = 1;
                celTextClienteFacturarA.Padding = 3;
                celTextClienteFacturarA.Border = 0;
                celTextClienteFacturarA.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextClienteFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celTextClienteFacturarA);

                iTextSharp.text.pdf.PdfPCell celClienteFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase(DsInvoiceAR.Tables["Customer"].Rows[0]["Name"].ToString(),
                    fontTitle2));
                celClienteFacturarA.Colspan = 1;
                celClienteFacturarA.Padding = 3;
                celClienteFacturarA.Border = 0;
                celClienteFacturarA.HorizontalAlignment = Element.ALIGN_LEFT;
                celClienteFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celClienteFacturarA);

                iTextSharp.text.pdf.PdfPCell celTextNitFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase("NIT:", fontTitleFactura));
                celTextNitFacturarA.Colspan = 1;
                celTextNitFacturarA.Padding = 3;
                celTextNitFacturarA.Border = 0;
                celTextNitFacturarA.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextNitFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celTextNitFacturarA);

                iTextSharp.text.pdf.PdfPCell celNitFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase(DsInvoiceAR.Tables["Customer"].Rows[0]["CustID"].ToString(), fontTitle2));
                celNitFacturarA.Colspan = 1;
                celNitFacturarA.Padding = 3;
                celNitFacturarA.Border = 0;
                celNitFacturarA.HorizontalAlignment = Element.ALIGN_LEFT;
                celNitFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celNitFacturarA);

                iTextSharp.text.pdf.PdfPCell celTextDireccionFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase("DIRECCION:", fontTitleFactura));
                celTextDireccionFacturarA.Colspan = 1;
                celTextDireccionFacturarA.Padding = 3;
                celTextDireccionFacturarA.Border = 0;
                celTextDireccionFacturarA.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextDireccionFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celTextDireccionFacturarA);

                iTextSharp.text.pdf.PdfPCell celDireccionFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase(DsInvoiceAR.Tables["Customer"].Rows[0]["Address1"].ToString(), fontTitle2));
                celDireccionFacturarA.Colspan = 1;
                celDireccionFacturarA.Padding = 3;
                celDireccionFacturarA.Border = 0;
                celDireccionFacturarA.HorizontalAlignment = Element.ALIGN_LEFT;
                celDireccionFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celDireccionFacturarA);

                iTextSharp.text.pdf.PdfPCell celTextTelFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase("TELEFONO:", fontTitleFactura));
                celTextTelFacturarA.Colspan = 1;
                celTextTelFacturarA.Padding = 3;
                celTextTelFacturarA.Border = 0;
                celTextTelFacturarA.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextTelFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celTextTelFacturarA);

                iTextSharp.text.pdf.PdfPCell celTelFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase(DsInvoiceAR.Tables["Customer"].Rows[0]["PhoneNum"].ToString(), fontTitle2));
                celTelFacturarA.Colspan = 1;
                celTelFacturarA.Padding = 3;
                celTelFacturarA.Border = 0;
                celTelFacturarA.HorizontalAlignment = Element.ALIGN_LEFT;
                celTelFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celTelFacturarA);

                iTextSharp.text.pdf.PdfPCell celTextCiudadFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase("CIUDAD:", fontTitleFactura));
                celTextCiudadFacturarA.Colspan = 1;
                celTextCiudadFacturarA.Padding = 3;
                celTextCiudadFacturarA.Border = 0;
                celTextCiudadFacturarA.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextCiudadFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celTextCiudadFacturarA);

                iTextSharp.text.pdf.PdfPCell celCiudadFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase(DsInvoiceAR.Tables["Customer"].Rows[0]["City"].ToString(), fontTitle2));
                celCiudadFacturarA.Colspan = 1;
                celCiudadFacturarA.Padding = 3;
                celCiudadFacturarA.Border = 0;
                celCiudadFacturarA.HorizontalAlignment = Element.ALIGN_LEFT;
                celCiudadFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celCiudadFacturarA);

                iTextSharp.text.pdf.PdfPCell celTextPaisFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase("PAIS:", fontTitleFactura));
                celTextPaisFacturarA.Colspan = 1;
                celTextPaisFacturarA.Padding = 3;
                celTextPaisFacturarA.Border = 0;
                celTextPaisFacturarA.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextPaisFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celTextPaisFacturarA);

                iTextSharp.text.pdf.PdfPCell celPaisFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase(DsInvoiceAR.Tables["Customer"].Rows[0]["Country"].ToString(), fontTitle2));
                celPaisFacturarA.Colspan = 1;
                celPaisFacturarA.Padding = 3;
                celPaisFacturarA.Border = 0;
                celPaisFacturarA.HorizontalAlignment = Element.ALIGN_LEFT;
                celPaisFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celPaisFacturarA);

                iTextSharp.text.pdf.PdfPCell celTittleFacturarA = new iTextSharp.text.pdf.PdfPCell(tableFacturarA);
                tableFacturar.AddCell(celTittleFacturarA);
                //----------------------------------------------------------------------------------------------------------------------------

                iTextSharp.text.pdf.PdfPCell celEspacio2 = new iTextSharp.text.pdf.PdfPCell(new Phrase(" ", fontTitleFactura));
                celEspacio2.Border = 0;
                tableFacturar.AddCell(celEspacio2);

                //------------------------------------------------------------------------------------------

                /// Informacion de la Empresa que realiza la FE ----------------------------------
                //---------------------------------------
                PdfPTable tableInfoTributaria = new PdfPTable(2);
                float[] DimencionDespacharA = new float[2];
                DimencionDespacharA[0] = 0.8F;
                DimencionDespacharA[1] = 2.0F;

                tableInfoTributaria.WidthPercentage = 100;
                tableInfoTributaria.SetWidths(DimencionDespacharA);

                iTextSharp.text.pdf.PdfPCell celTittleInfoTributaria = new iTextSharp.text.pdf.PdfPCell(new Phrase("INFORMACION TRIBUTARIA:\n", fontTitleFactura));
                celTittleInfoTributaria.Colspan = 2;
                celTittleInfoTributaria.Padding = 3;
                celTittleInfoTributaria.Border = 0;
                celTittleInfoTributaria.BorderColorBottom = BaseColor.WHITE;
                celTittleInfoTributaria.HorizontalAlignment = Element.ALIGN_CENTER;
                celTittleInfoTributaria.VerticalAlignment = Element.ALIGN_TOP;
                tableInfoTributaria.AddCell(celTittleInfoTributaria);

                iTextSharp.text.pdf.PdfPCell celTextNombreEmpresa = new iTextSharp.text.pdf.PdfPCell(new Phrase("Nombre:", fontTitleFactura));
                celTextNombreEmpresa.Colspan = 1;
                celTextNombreEmpresa.Padding = 3;
                celTextNombreEmpresa.Border = 0;
                celTextNombreEmpresa.HorizontalAlignment = Element.ALIGN_RIGHT;
                celTextNombreEmpresa.VerticalAlignment = Element.ALIGN_TOP;
                tableInfoTributaria.AddCell(celTextNombreEmpresa);

                iTextSharp.text.pdf.PdfPCell celNombreEmpresa = new iTextSharp.text.pdf.PdfPCell(new Phrase(DsInvoiceAR.Tables["Company"].Rows[0]["Name"].ToString(), fontTitle2));
                celNombreEmpresa.Colspan = 1;
                celNombreEmpresa.Padding = 3;
                celNombreEmpresa.Border = 0;
                celNombreEmpresa.HorizontalAlignment = Element.ALIGN_LEFT;
                celNombreEmpresa.VerticalAlignment = Element.ALIGN_TOP;
                tableInfoTributaria.AddCell(celNombreEmpresa);

                iTextSharp.text.pdf.PdfPCell celTextNitEmpresa = new iTextSharp.text.pdf.PdfPCell(new Phrase("Nit:", fontTitleFactura));
                celTextNitEmpresa.Colspan = 1;
                celTextNitEmpresa.Padding = 3;
                celTextNitEmpresa.Border = 0;
                celTextNitEmpresa.HorizontalAlignment = Element.ALIGN_RIGHT;
                celTextNitEmpresa.VerticalAlignment = Element.ALIGN_TOP;
                tableInfoTributaria.AddCell(celTextNitEmpresa);

                iTextSharp.text.pdf.PdfPCell celNitEmpresa = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    string.Concat(DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"].ToString(), "-", CalcularDigitoVerificacion(DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"].ToString())), fontTitle2));
                celNitEmpresa.Colspan = 1;
                celNitEmpresa.Padding = 3;
                celNitEmpresa.Border = 0;
                celNitEmpresa.HorizontalAlignment = Element.ALIGN_LEFT;
                celNitEmpresa.VerticalAlignment = Element.ALIGN_TOP;
                tableInfoTributaria.AddCell(celNitEmpresa);

                iTextSharp.text.pdf.PdfPCell celTextDireccionEmpresa = new iTextSharp.text.pdf.PdfPCell(new Phrase("Direccion:", fontTitleFactura));
                celTextDireccionEmpresa.Colspan = 1;
                celTextDireccionEmpresa.Padding = 3;
                celTextDireccionEmpresa.Border = 0;
                celTextDireccionEmpresa.HorizontalAlignment = Element.ALIGN_RIGHT;
                celTextDireccionEmpresa.VerticalAlignment = Element.ALIGN_TOP;
                tableInfoTributaria.AddCell(celTextDireccionEmpresa);

                iTextSharp.text.pdf.PdfPCell celDireccionEmpresa = new iTextSharp.text.pdf.PdfPCell(new Phrase(DsInvoiceAR.Tables["Company"].Rows[0]["Address1"].ToString(), fontTitle2));
                celDireccionEmpresa.Colspan = 1;
                celDireccionEmpresa.Padding = 3;
                celDireccionEmpresa.Border = 0;
                celDireccionEmpresa.HorizontalAlignment = Element.ALIGN_LEFT;
                celDireccionEmpresa.VerticalAlignment = Element.ALIGN_TOP;
                tableInfoTributaria.AddCell(celDireccionEmpresa);

                iTextSharp.text.pdf.PdfPCell celTextTelefonoEmpresa = new iTextSharp.text.pdf.PdfPCell(new Phrase("Telefono:", fontTitleFactura));
                celTextTelefonoEmpresa.Colspan = 1;
                celTextTelefonoEmpresa.Padding = 3;
                celTextTelefonoEmpresa.Border = 0;
                celTextTelefonoEmpresa.HorizontalAlignment = Element.ALIGN_RIGHT;
                celTextTelefonoEmpresa.VerticalAlignment = Element.ALIGN_TOP;
                tableInfoTributaria.AddCell(celTextTelefonoEmpresa);

                iTextSharp.text.pdf.PdfPCell celTelefonoEmpresa = new iTextSharp.text.pdf.PdfPCell(new Phrase(DsInvoiceAR.Tables["Company"].Rows[0]["PhoneNum"].ToString(), fontTitle2));
                celTelefonoEmpresa.Colspan = 1;
                celTelefonoEmpresa.Padding = 3;
                celTelefonoEmpresa.Border = 0;
                celTelefonoEmpresa.HorizontalAlignment = Element.ALIGN_LEFT;
                celTelefonoEmpresa.VerticalAlignment = Element.ALIGN_TOP;
                tableInfoTributaria.AddCell(celTelefonoEmpresa);

                iTextSharp.text.pdf.PdfPCell celTextCiudadEmpresa = new iTextSharp.text.pdf.PdfPCell(new Phrase("Ciudad:", fontTitleFactura));
                celTextCiudadEmpresa.Colspan = 1;
                celTextCiudadEmpresa.Padding = 3;
                celTextCiudadEmpresa.Border = 0;
                celTextCiudadEmpresa.HorizontalAlignment = Element.ALIGN_RIGHT;
                celTextCiudadEmpresa.VerticalAlignment = Element.ALIGN_TOP;
                tableInfoTributaria.AddCell(celTextCiudadEmpresa);

                iTextSharp.text.pdf.PdfPCell celCiudadEmpresa = new iTextSharp.text.pdf.PdfPCell(new Phrase(DsInvoiceAR.Tables["Company"].Rows[0]["City"].ToString(), fontTitle2));
                celCiudadEmpresa.Colspan = 1;
                celCiudadEmpresa.Padding = 3;
                celCiudadEmpresa.Border = 0;
                celCiudadEmpresa.HorizontalAlignment = Element.ALIGN_LEFT;
                celCiudadEmpresa.VerticalAlignment = Element.ALIGN_TOP;
                tableInfoTributaria.AddCell(celCiudadEmpresa);

                iTextSharp.text.pdf.PdfPCell celTextPaisEmpresa = new iTextSharp.text.pdf.PdfPCell(new Phrase("Pais:", fontTitleFactura));
                celTextPaisEmpresa.Colspan = 1;
                celTextPaisEmpresa.Padding = 3;
                celTextPaisEmpresa.Border = 0;
                celTextPaisEmpresa.HorizontalAlignment = Element.ALIGN_RIGHT;
                celTextPaisEmpresa.VerticalAlignment = Element.ALIGN_TOP;
                tableInfoTributaria.AddCell(celTextPaisEmpresa);

                iTextSharp.text.pdf.PdfPCell celPaisEmpresa = new iTextSharp.text.pdf.PdfPCell(new Phrase(DsInvoiceAR.Tables["Company"].Rows[0]["Country"].ToString(), fontTitle2));
                celPaisEmpresa.Colspan = 1;
                celPaisEmpresa.Padding = 3;
                celPaisEmpresa.Border = 0;
                celPaisEmpresa.HorizontalAlignment = Element.ALIGN_LEFT;
                celPaisEmpresa.VerticalAlignment = Element.ALIGN_TOP;
                tableInfoTributaria.AddCell(celPaisEmpresa);

                iTextSharp.text.pdf.PdfPCell celDatosDespacharA = new iTextSharp.text.pdf.PdfPCell(tableInfoTributaria) { Border = 0, };
                tableFacturar.AddCell(celDatosDespacharA);
                //-------------------------------------------------------------------------------------- 
                #endregion

                #region Unidades
                //------------------------------------------------------------------------------------------------
                PdfPTable tableDetalles2 = new PdfPTable(4);
                tableDetalles2.WidthPercentage = 100;

                PdfPTable tableFechaFactura = new PdfPTable(2);
                float[] dimecionesTablaFecha = new float[2];
                dimecionesTablaFecha[0] = 1.3F;
                dimecionesTablaFecha[1] = 0.9F;
                tableFechaFactura.SetWidths(dimecionesTablaFecha);

                iTextSharp.text.pdf.PdfPCell celTextFechaFactura = new iTextSharp.text.pdf.PdfPCell(new Phrase("FECHA FACTURA: ",
                    fontTitle));
                celTextFechaFactura.Colspan = 1;
                celTextFechaFactura.Padding = 7;
                celTextFechaFactura.Border = 0;
                celTextFechaFactura.BorderColorBottom = BaseColor.WHITE;
                celTextFechaFactura.HorizontalAlignment = Element.ALIGN_CENTER;
                celTextFechaFactura.VerticalAlignment = Element.ALIGN_CENTER;
                tableFechaFactura.AddCell(celTextFechaFactura);

                iTextSharp.text.pdf.PdfPCell celFechaFactura = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:MM/dd/yyyy}", DateTime.Parse(DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"].ToString())),
                fontCustom));
                celFechaFactura.Colspan = 1;
                celFechaFactura.Padding = 7;
                //celFechaFactura.PaddingTop = 4;
                celFechaFactura.Border = 0;
                celFechaFactura.BorderColorBottom = BaseColor.WHITE;
                celFechaFactura.HorizontalAlignment = Element.ALIGN_LEFT;
                celFechaFactura.VerticalAlignment = Element.ALIGN_CENTER;
                tableFechaFactura.AddCell(celFechaFactura);

                PdfPCell _celFechaFactura = new PdfPCell(tableFechaFactura);
                tableDetalles2.AddCell(_celFechaFactura);


                PdfPTable tableFechaVencimiento = new PdfPTable(2);
                float[] dmTablaFechaVencimiento = new float[2];
                dmTablaFechaVencimiento[0] = 1.4F;
                dmTablaFechaVencimiento[1] = 0.8F;
                tableFechaVencimiento.SetWidths(dmTablaFechaVencimiento);

                iTextSharp.text.pdf.PdfPCell celTextFechaVencimiento = new iTextSharp.text.pdf.PdfPCell(new Phrase("FECHA VENCIMIENTO: ",
                    fontTitle));
                celTextFechaVencimiento.Colspan = 1;
                celTextFechaVencimiento.PaddingTop = 7;
                celTextFechaVencimiento.Border = 0;
                celTextFechaVencimiento.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextFechaVencimiento.VerticalAlignment = Element.ALIGN_TOP;
                tableFechaVencimiento.AddCell(celTextFechaVencimiento);

                iTextSharp.text.pdf.PdfPCell celFechaVencimiento = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:MM/dd/yyyy}",
                   DateTime.Parse(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"].ToString())),
                fontCustom));
                celFechaVencimiento.Colspan = 1;
                celFechaVencimiento.Padding = 7;
                celFechaVencimiento.Border = 0;
                celFechaVencimiento.BorderColorBottom = BaseColor.WHITE;
                celFechaVencimiento.HorizontalAlignment = Element.ALIGN_LEFT;
                celFechaVencimiento.VerticalAlignment = Element.ALIGN_TOP;
                tableFechaVencimiento.AddCell(celFechaVencimiento);

                PdfPCell _celFechaFacturaVencimiento = new PdfPCell(tableFechaVencimiento);
                tableDetalles2.AddCell(_celFechaFacturaVencimiento);

                PdfPTable tableMoneda = new PdfPTable(2);
                float[] dimecionesTablaMoneda = new float[2];
                dimecionesTablaMoneda[0] = 0.6F;
                dimecionesTablaMoneda[1] = 1.0F;
                tableMoneda.SetWidths(dimecionesTablaMoneda);

                iTextSharp.text.pdf.PdfPCell celTextMoneda = new iTextSharp.text.pdf.PdfPCell(new Phrase("MONEDA: ",
                    fontTitle));
                celTextMoneda.Colspan = 1;
                celTextMoneda.Padding = 7;
                celTextMoneda.Border = 0;
                celTextMoneda.BorderColorBottom = BaseColor.WHITE;
                celTextMoneda.HorizontalAlignment = Element.ALIGN_RIGHT;
                celTextMoneda.VerticalAlignment = Element.ALIGN_TOP;
                tableMoneda.AddCell(celTextMoneda);

                iTextSharp.text.pdf.PdfPCell celMoneda = new iTextSharp.text.pdf.PdfPCell(new Phrase(DsInvoiceAR.Tables["InvcHead"].Rows[0]["CurrencyCode"].ToString(),
                fontCustom));
                celMoneda.Colspan = 1;
                celMoneda.Padding = 7;
                celMoneda.Border = 0;
                celMoneda.BorderColorBottom = BaseColor.WHITE;
                celMoneda.HorizontalAlignment = Element.ALIGN_LEFT;
                celMoneda.VerticalAlignment = Element.ALIGN_TOP;
                tableMoneda.AddCell(celMoneda);

                PdfPCell _celMoneda = new PdfPCell(tableMoneda);
                tableDetalles2.AddCell(_celMoneda);

                PdfPTable tableFormaPago = new PdfPTable(2);
                float[] dimecionesTablaFormaPago = new float[2];
                dimecionesTablaFormaPago[0] = 1.0F;
                dimecionesTablaFormaPago[1] = 1.0F;
                tableFormaPago.SetWidths(dimecionesTablaFormaPago);

                iTextSharp.text.pdf.PdfPCell celTextFormaPago = new iTextSharp.text.pdf.PdfPCell(new Phrase("TERMINOS DE PAGO: ", fontTitle));
                celTextFormaPago.Colspan = 1;
                celTextFormaPago.Padding = 7;
                celTextFormaPago.Border = 0;
                celTextFormaPago.BorderColorBottom = BaseColor.WHITE;
                celTextFormaPago.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextFormaPago.VerticalAlignment = Element.ALIGN_TOP;
                tableFormaPago.AddCell(celTextFormaPago);

                iTextSharp.text.pdf.PdfPCell celFormaPago = new iTextSharp.text.pdf.PdfPCell(new Phrase(DsInvoiceAR.Tables["Customer"].Rows[0]["TermsDescription"].ToString(), fontCustom));
                celFormaPago.Colspan = 1;
                celFormaPago.Padding = 7;
                celFormaPago.Border = 0;
                celFormaPago.BorderColorBottom = BaseColor.WHITE;
                celFormaPago.HorizontalAlignment = Element.ALIGN_LEFT;
                celFormaPago.VerticalAlignment = Element.ALIGN_TOP;
                tableFormaPago.AddCell(celFormaPago);


                PdfPCell _celFormaPago = new PdfPCell(tableFormaPago);
                tableDetalles2.AddCell(_celFormaPago);

                //-----------------------------------------------------------------------------------------
                #endregion

                #region Tabla Detalle Productos

                //Dimenciones Tabla Unidades
                float[] DimencionUnidades = new float[4];
                DimencionUnidades[0] = 0.8F;//OT
                DimencionUnidades[1] = 2.5F;//Articulo
                DimencionUnidades[2] = 1.0F;//O.C.
                DimencionUnidades[3] = 0.8F;//cantidad
                //DimencionUnidades[4] = 0.8F;//embalaje
                //DimencionUnidades[5] = 1.0F;//valor unitario
                //DimencionUnidades[6] = 1.0F;//Valor subtotal

                PdfPTable tableDetalles = new PdfPTable(DimencionUnidades);
                tableDetalles.WidthPercentage = 100;


                //iTextSharp.text.pdf.PdfPCell celVendedor = new iTextSharp.text.pdf.PdfPCell(new Phrase("OT", fontTitleFactura));
                //celVendedor.Colspan = 1;
                //celVendedor.Padding = 3;
                ////celVendedor.Border = 0;
                //celVendedor.BorderColorBottom = BaseColor.WHITE;
                //celVendedor.HorizontalAlignment = Element.ALIGN_CENTER;
                //celVendedor.VerticalAlignment = Element.ALIGN_TOP;
                //celVendedor.BackgroundColor = BaseColor.LIGHT_GRAY;
                //tableDetalles.AddCell(celVendedor);

                iTextSharp.text.pdf.PdfPCell celOC_Cliente = new iTextSharp.text.pdf.PdfPCell(new Phrase("ARTICULOS", fontTitleFactura));
                celOC_Cliente.Colspan = 1;
                celOC_Cliente.Padding = 3;
                //celVendedor.Border = 0;
                celOC_Cliente.BorderColorBottom = BaseColor.WHITE;
                celOC_Cliente.HorizontalAlignment = Element.ALIGN_CENTER;
                celOC_Cliente.VerticalAlignment = Element.ALIGN_TOP;
                celOC_Cliente.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableDetalles.AddCell(celOC_Cliente);

                //iTextSharp.text.pdf.PdfPCell celOC = new iTextSharp.text.pdf.PdfPCell(new Phrase("O. C.", fontTitleFactura));
                //celOC.Colspan = 1;
                //celOC.Padding = 3;
                ////celVendedor.Border = 0;
                //celOC.BorderColorBottom = BaseColor.WHITE;
                //celOC.HorizontalAlignment = Element.ALIGN_CENTER;
                //celOC.VerticalAlignment = Element.ALIGN_TOP;
                //celOC.BackgroundColor = BaseColor.LIGHT_GRAY;
                //tableDetalles.AddCell(celOC);


                iTextSharp.text.pdf.PdfPCell celOrdenVenta = new iTextSharp.text.pdf.PdfPCell(new Phrase("CANTIDAD", fontTitleFactura));
                celOrdenVenta.Colspan = 1;
                celOrdenVenta.Padding = 3;
                //celVendedor.Border = 0;
                celOrdenVenta.BorderColorBottom = BaseColor.WHITE;
                celOrdenVenta.HorizontalAlignment = Element.ALIGN_CENTER;
                celOrdenVenta.VerticalAlignment = Element.ALIGN_TOP;
                celOrdenVenta.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableDetalles.AddCell(celOrdenVenta);

                //iTextSharp.text.pdf.PdfPCell celLocalizacion = new iTextSharp.text.pdf.PdfPCell(new Phrase("EMBALAJE", fontTitleFactura));
                //celLocalizacion.Colspan = 1;
                //celLocalizacion.Padding = 3;
                ////celVendedor.Border = 0;
                //celLocalizacion.BackgroundColor = BaseColor.LIGHT_GRAY;
                //celLocalizacion.BorderColorBottom = BaseColor.WHITE;
                //celLocalizacion.HorizontalAlignment = Element.ALIGN_CENTER;
                //celLocalizacion.VerticalAlignment = Element.ALIGN_TOP;
                //tableDetalles.AddCell(celLocalizacion);

                iTextSharp.text.pdf.PdfPCell celRemision = new iTextSharp.text.pdf.PdfPCell(new Phrase("VALOR UNITARIO", fontTitleFactura));
                celRemision.Colspan = 1;
                celRemision.Padding = 3;
                celRemision.BackgroundColor = BaseColor.LIGHT_GRAY;
                //celVendedor.Border = 0;
                celRemision.BorderColorBottom = BaseColor.WHITE;
                celRemision.HorizontalAlignment = Element.ALIGN_CENTER;
                celRemision.VerticalAlignment = Element.ALIGN_TOP;
                tableDetalles.AddCell(celRemision);

                iTextSharp.text.pdf.PdfPCell celUBTOTAL = new iTextSharp.text.pdf.PdfPCell(new Phrase("SUBTOTAL", fontTitleFactura));
                celUBTOTAL.Colspan = 1;
                celUBTOTAL.Padding = 3;
                celUBTOTAL.BackgroundColor = BaseColor.LIGHT_GRAY;
                //celVendedor.Border = 0;
                celUBTOTAL.BorderColorBottom = BaseColor.WHITE;
                celUBTOTAL.HorizontalAlignment = Element.ALIGN_CENTER;
                celUBTOTAL.VerticalAlignment = Element.ALIGN_TOP;
                tableDetalles.AddCell(celUBTOTAL);

                //contador para las cendas 
                PdfPTable tableUnidades = new PdfPTable(DimencionUnidades);
                tableUnidades.WidthPercentage = 100;

                decimal totalDescuento = 0;
                foreach (DataRow InvoiceLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
                {
                    if (!AddUnidadesInversionesDinastía(InvoiceLine, ref tableUnidades, fontCustom, DsInvoiceAR))
                        return false;
                    if (Helpers.Compartido.VerificarExistenciaColumnas(DsInvoiceAR, "InvcDtl", "DspDocLessDiscount"))
                        totalDescuento += decimal.Parse((string)InvoiceLine["DspDocLessDiscount"]);
                }

                iTextSharp.text.pdf.PdfPCell LineaFinal = new iTextSharp.text.pdf.PdfPCell(new Phrase("", fontCustom));
                LineaFinal.Colspan = tableUnidades.NumberOfColumns;
                LineaFinal.PaddingTop = 0;
                LineaFinal.BorderWidthTop = 1;
                LineaFinal.BorderColorBottom = BaseColor.WHITE;
                LineaFinal.HorizontalAlignment = Element.ALIGN_CENTER;
                LineaFinal.VerticalAlignment = Element.ALIGN_TOP;
                tableUnidades.AddCell(LineaFinal);

                //-----------------------------------------------------------------------------------------
                //------------------------------------------------------------------------------------------------
                #endregion

                #region Footer

                PdfPTable info_pie = new PdfPTable(new float[] { 7.0f, 3.0f });
                info_pie.WidthPercentage = 100;

                PdfPCell info_pie_inicio = new PdfPCell() { Border = 0, };
                //valor en letras 
                PdfPTable velorletras = new PdfPTable(1);
                velorletras.WidthPercentage = 100;
                PdfPCell velorletras_cell = new PdfPCell(new Phrase("Valor en letras: " + Nroenletras(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"].ToString()), fontTitle2));
                velorletras.AddCell(velorletras_cell);
                info_pie_inicio.AddElement(velorletras);
                //obserbaciones
                PdfPTable obserbaciones = new PdfPTable(1);
                obserbaciones.WidthPercentage = 100;
                PdfPCell obserbaciones_cell = new PdfPCell(new Phrase("Garantia: " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceComment"].ToString(), fontTitle2)) { MinimumHeight = 50, };
                obserbaciones.AddCell(obserbaciones_cell);
                info_pie_inicio.AddElement(obserbaciones);

                PdfPCell info_pie_inicio2 = new PdfPCell() { Border = 0, };
                //totales finales     
                PdfPTable totales = new PdfPTable(new float[] { 1.0f, 1.0f });
                totales.WidthPercentage = 100;
                PdfPCell totales_cell_titulo = new PdfPCell();
                //agregamos todos los valores 
                //subtotal
                PdfPTable subtotal = new PdfPTable(1);
                subtotal.WidthPercentage = 100;
                PdfPCell subtutal_cell = new PdfPCell(new Phrase("SUBTOTAL: ", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, };
                subtotal.AddCell(subtutal_cell);
                //descuento
                PdfPTable descuento = new PdfPTable(1);
                descuento.WidthPercentage = 100;
                PdfPCell descuento_cell = new PdfPCell(new Phrase("DESCUENTO : ", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, };
                descuento.AddCell(descuento_cell);
                //ret_fet
                PdfPTable ret_fet = new PdfPTable(1);
                ret_fet.WidthPercentage = 100;
                PdfPCell ret_fet_cell = new PdfPCell(new Phrase("RET. FTE. : ", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, };
                ret_fet.AddCell(ret_fet_cell);
                //ret_iva
                PdfPTable ret_iva = new PdfPTable(1);
                ret_iva.WidthPercentage = 100;
                PdfPCell ret_iva_cell = new PdfPCell(new Phrase("RET. IVA. : ", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, };
                ret_iva.AddCell(ret_iva_cell);
                //iva
                PdfPTable iva = new PdfPTable(1);
                iva.WidthPercentage = 100;
                PdfPCell iva_cell = new PdfPCell(new Phrase("IVA. : ", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, };
                iva.AddCell(iva_cell);
                //total
                PdfPTable total = new PdfPTable(1);
                total.WidthPercentage = 100;
                PdfPCell total_cell = new PdfPCell(new Phrase("TOTAL. : ", fontCustom)) { HorizontalAlignment = Element.ALIGN_LEFT, };
                total.AddCell(total_cell);

                totales_cell_titulo.AddElement(subtotal);
                totales_cell_titulo.AddElement(descuento);
                totales_cell_titulo.AddElement(ret_fet);
                totales_cell_titulo.AddElement(ret_iva);
                totales_cell_titulo.AddElement(iva);
                totales_cell_titulo.AddElement(total);

                //valores de los totales 
                PdfPCell totales_cell_valores = new PdfPCell();
                //subtotal
                PdfPTable subtotal_v = new PdfPTable(1);
                subtotal_v.WidthPercentage = 100;
                PdfPCell subtutal_cell_v = new PdfPCell(new Phrase(decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubTotal"]).ToString("C2"), fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_RIGHT, };
                subtotal_v.AddCell(subtutal_cell_v);
                //descuento
                PdfPTable descuento_v = new PdfPTable(1);
                descuento_v.WidthPercentage = 100;
                PdfPCell descuento_cell_v = new PdfPCell(new Phrase(totalDescuento.ToString("C2"), fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_RIGHT, };
                descuento_v.AddCell(descuento_cell_v);
                //ret_fet
                PdfPTable ret_fet_v = new PdfPTable(1);
                ret_fet_v.WidthPercentage = 100;
                PdfPCell ret_fet_cell_v = new PdfPCell(new Phrase(decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number01"]).ToString("C2"), fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_RIGHT, };
                ret_fet_v.AddCell(ret_fet_cell_v);
                //ret_iva
                PdfPTable ret_iva_v = new PdfPTable(1);
                ret_iva_v.WidthPercentage = 100;
                PdfPCell ret_iva_cell_v = new PdfPCell(new Phrase(decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number02"]).ToString("C2"), fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_RIGHT, };
                ret_iva_v.AddCell(ret_iva_cell_v);
                //iva
                PdfPTable iva_v = new PdfPTable(1);
                iva_v.WidthPercentage = 100;
                PdfPCell iva_cell_v = new PdfPCell(new Phrase(decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DocTaxAmt"]).ToString("C2"), fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_RIGHT, };
                iva_v.AddCell(iva_cell_v);
                //total
                PdfPTable total_v = new PdfPTable(1);
                total_v.WidthPercentage = 100;
                PdfPCell total_cell_v = new PdfPCell(new Phrase(decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"]).ToString("C2"), fontCustom)) { HorizontalAlignment = Element.ALIGN_RIGHT, };
                total_v.AddCell(total_cell_v);

                totales_cell_valores.AddElement(subtotal_v);
                totales_cell_valores.AddElement(descuento_v);
                totales_cell_valores.AddElement(ret_fet_v);
                totales_cell_valores.AddElement(ret_iva_v);
                totales_cell_valores.AddElement(iva_v);
                totales_cell_valores.AddElement(total_v);

                totales.AddCell(totales_cell_titulo);
                totales.AddCell(totales_cell_valores);

                info_pie_inicio2.AddElement(totales);

                info_pie.AddCell(info_pie_inicio);
                info_pie.AddCell(info_pie_inicio2);

                //informacion legal
                PdfPTable informacion_legal = new PdfPTable(1);
                informacion_legal.WidthPercentage = 100;
                PdfPCell informacion_legal_cell = new PdfPCell(new Phrase("Informacion legal:\n " +
                    "En virtudde la ley 1581 de 2012 y del Decreto 1377 de 2013, con la firma y/o aceptacion del presente documento declaro que\n" +
                    "conozco la politica de tratamiento de datos personales a la que estara sometidos mis dato, y autorizo de manera libre\n" +
                    "expresa y espontania a INVERSIONES DINASTIA E.U. para que recolecte, almacen, use, intercambien, actualice,y procese\n" +
                    "la informacion y datos perdonales por mi suministrada, la cual sera unsada exclusivamente para fines tributarios y contables \n", fontTitle2)) { MinimumHeight = 60, };
                informacion_legal.AddCell(informacion_legal_cell);

                //----------------------------------------------------------------------------------------------  
                //------------------------------------------------------------------------
                PdfPTable tableFirmas = new PdfPTable(5);

                float[] DimencionFirmas = new float[5];
                DimencionFirmas[0] = 1.0F;//
                DimencionFirmas[1] = 0.02F;//
                DimencionFirmas[2] = 0.6F;//
                DimencionFirmas[3] = 0.02F;//
                DimencionFirmas[4] = 2.0F;//

                tableFirmas.WidthPercentage = 100;
                tableFirmas.SetWidths(DimencionFirmas);

                PdfPTable tableDespacahdoPor = new PdfPTable(1);
                //tableDespacahdoPor.WidthPercentage = 100;

                iTextSharp.text.pdf.PdfPCell celDespachadoPor = new iTextSharp.text.pdf.PdfPCell(new Phrase("\n\n\n\n\n" +
                    "\n____________________________________\n\nELABORO", fontTitleFactura));
                celDespachadoPor.Colspan = 1;
                celDespachadoPor.Padding = 3;
                celDespachadoPor.HorizontalAlignment = Element.ALIGN_CENTER;
                celDespachadoPor.VerticalAlignment = Element.ALIGN_TOP;
                tableDespacahdoPor.AddCell(celDespachadoPor);

                iTextSharp.text.pdf.PdfPCell celEspacioDespachadoPor = new iTextSharp.text.pdf.PdfPCell(new Phrase("\n\n\n", fontTitleFactura));
                celEspacioDespachadoPor.Colspan = 1;
                celEspacioDespachadoPor.Padding = 3;
                celEspacioDespachadoPor.Border = 0;
                celEspacioDespachadoPor.HorizontalAlignment = Element.ALIGN_CENTER;
                celEspacioDespachadoPor.VerticalAlignment = Element.ALIGN_TOP;
                tableDespacahdoPor.AddCell(celEspacioDespachadoPor);

                iTextSharp.text.pdf.PdfPCell _celDespachadoPor = new iTextSharp.text.pdf.PdfPCell(tableDespacahdoPor);
                _celDespachadoPor.Border = 0;
                tableFirmas.AddCell(_celDespachadoPor);
                //------------------------------------------------------------------------------------------
                tableFirmas.AddCell(celEspacio2);
                //------------------------------------------------------------------------------------------
                PdfPTable tableFirmaConductor = new PdfPTable(1);
                tableFirmaConductor.WidthPercentage = 100;
                iTextSharp.text.pdf.PdfPCell celFirmaConductor = new iTextSharp.text.pdf.PdfPCell(new Phrase("\n\n\n\n\n\n"));
                celFirmaConductor.Colspan = 1;
                celFirmaConductor.Padding = 3;
                celFirmaConductor.Border = 0;
                celFirmaConductor.HorizontalAlignment = Element.ALIGN_CENTER;
                celFirmaConductor.VerticalAlignment = Element.ALIGN_TOP;
                tableFirmaConductor.AddCell(celFirmaConductor);

                iTextSharp.text.pdf.PdfPCell celEspacioFirmaConductor = new iTextSharp.text.pdf.PdfPCell(new Phrase("\n\n\n", fontTitleFactura));
                celEspacioFirmaConductor.Colspan = 1;
                celEspacioFirmaConductor.Padding = 3;
                celEspacioFirmaConductor.Border = 0;
                celEspacioFirmaConductor.HorizontalAlignment = Element.ALIGN_CENTER;
                celEspacioFirmaConductor.VerticalAlignment = Element.ALIGN_TOP;
                tableFirmaConductor.AddCell(celEspacioFirmaConductor);

                iTextSharp.text.pdf.PdfPCell _celFirmaConductor = new iTextSharp.text.pdf.PdfPCell(tableFirmaConductor);
                _celFirmaConductor.Border = 0;
                tableFirmas.AddCell(_celFirmaConductor);
                //-------------------------------------------------------------------------------------
                tableFirmas.AddCell(celEspacio2);
                //-------------------------------------------------------------------------------------

                PdfPTable tableSelloCliente = new PdfPTable(1);
                //tableSelloCliente.WidthPercentage = 100;

                iTextSharp.text.pdf.PdfPCell cel1SelloCliente = new iTextSharp.text.pdf.PdfPCell(new Phrase("Apreciado cliente este documento " +
                    "no es endosable", fontTitleFactura));
                cel1SelloCliente.Colspan = 1;
                cel1SelloCliente.Padding = 5;
                cel1SelloCliente.Border = 1;
                cel1SelloCliente.BorderWidthBottom = 1;
                cel1SelloCliente.HorizontalAlignment = Element.ALIGN_LEFT;
                cel1SelloCliente.VerticalAlignment = Element.ALIGN_TOP;
                tableSelloCliente.AddCell(cel1SelloCliente);

                iTextSharp.text.pdf.PdfPCell cel2SelloFechaRecibido = new iTextSharp.text.pdf.PdfPCell(new Phrase("Fecha de recibido:", fontTitleFactura));

                cel2SelloFechaRecibido.Colspan = 1;
                cel2SelloFechaRecibido.Padding = 3;
                cel2SelloFechaRecibido.Border = 0;
                cel2SelloFechaRecibido.HorizontalAlignment = Element.ALIGN_LEFT;
                cel2SelloFechaRecibido.VerticalAlignment = Element.ALIGN_TOP;
                tableSelloCliente.AddCell(cel2SelloFechaRecibido);

                iTextSharp.text.pdf.PdfPCell cel2SelloNombre = new iTextSharp.text.pdf.PdfPCell(new Phrase("Nombre:", fontTitleFactura));

                cel2SelloNombre.Colspan = 1;
                cel2SelloNombre.Padding = 3;
                cel2SelloNombre.Border = 0;
                cel2SelloNombre.HorizontalAlignment = Element.ALIGN_LEFT;
                cel2SelloNombre.VerticalAlignment = Element.ALIGN_TOP;
                tableSelloCliente.AddCell(cel2SelloNombre);

                iTextSharp.text.pdf.PdfPCell cel2SelloId = new iTextSharp.text.pdf.PdfPCell(new Phrase("Identificacion:", fontTitleFactura));

                cel2SelloId.Colspan = 1;
                cel2SelloId.Padding = 3;
                cel2SelloId.Border = 0;
                cel2SelloId.HorizontalAlignment = Element.ALIGN_LEFT;
                cel2SelloId.VerticalAlignment = Element.ALIGN_TOP;
                tableSelloCliente.AddCell(cel2SelloId);

                iTextSharp.text.pdf.PdfPCell cel2SelloFirma = new iTextSharp.text.pdf.PdfPCell(new Phrase("Firma:\n", fontTitleFactura));

                cel2SelloFirma.Colspan = 1;
                cel2SelloFirma.Padding = 3;
                cel2SelloFirma.Border = 0;
                cel2SelloFirma.HorizontalAlignment = Element.ALIGN_LEFT;
                cel2SelloFirma.VerticalAlignment = Element.ALIGN_TOP;
                tableSelloCliente.AddCell(cel2SelloFirma);

                iTextSharp.text.pdf.PdfPCell cel3SelloCliente = new iTextSharp.text.pdf.PdfPCell(new Phrase("______________________________________________________\n\n" +
                    "RECIBIDO CONFORME FIRMA Y SELLO DEL CLIENTE", fontTitleFactura));

                cel3SelloCliente.Colspan = 1;
                cel3SelloCliente.Padding = 3;
                cel3SelloCliente.Border = 0;
                cel3SelloCliente.HorizontalAlignment = Element.ALIGN_CENTER;
                cel3SelloCliente.VerticalAlignment = Element.ALIGN_TOP;
                tableSelloCliente.AddCell(cel3SelloCliente);

                iTextSharp.text.pdf.PdfPCell _celSelloCliente = new iTextSharp.text.pdf.PdfPCell(tableSelloCliente);
                tableFirmas.AddCell(_celSelloCliente);
                #endregion

                #region Exti

                document.Add(Header);
                document.Add(divEspacio);
                document.Add(tableFacturar);
                document.Add(divEspacio);
                document.Add(tableDetalles2);
                document.Add(divEspacio2);
                document.Add(divEspacio2);
                document.Add(tableDetalles);
                document.Add(tableUnidades);
                document.Add(divEspacio2);
                document.Add(divEspacio2);
                document.Add(divEspacio2);
                document.Add(divEspacio2);
                document.Add(info_pie);
                document.Add(divEspacio2);
                document.Add(informacion_legal);
                document.Add(divEspacio2);
                document.Add(tableFirmas);

                /*PIE DE PAGINA*/
                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();
                return true;
                #endregion
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return false;
            }        
        }

        private PdfPTable TipoFacturaInversionesDinastía(DataSet DsInvoiceAR, string InvoiceType)
        {
            PdfPTable tableFactura = new PdfPTable(3);

            /// Fonts --------------------------------
            iTextSharp.text.Font fontCustom = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);
            iTextSharp.text.Font fontTitleFactura = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);

            //Dimenciones.
            float[] DimencionFactura = new float[3];
            DimencionFactura[0] = 1.0F;//
            DimencionFactura[1] = 4.0F;//
            DimencionFactura[2] = 0.5F;//

            tableFactura.WidthPercentage = 100;
            tableFactura.SetWidths(DimencionFactura);

            string tipoFactura = "";
            string NumLegalFactura = DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"].ToString();
            string NumInternoFactura = string.Empty;
            if (InvoiceType.Equals("InvoiceType"))
            {
                tipoFactura = "FACTURA DE VENTAS";
                NumInternoFactura = DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceNum"].ToString();

            }
            else if (InvoiceType.Equals("CreditNoteType"))
            {
                tipoFactura = "NOTA CREDITO";
                NumInternoFactura = DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceRef"].ToString();
            }
            else
            {
                tipoFactura = "NOTA DEBITO";
                NumInternoFactura = DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceRef"].ToString();
            }


            iTextSharp.text.pdf.PdfPCell celTittle = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Concat(tipoFactura + "\n\n"), fontTitleFactura));
            celTittle.Colspan = 3;
            celTittle.Padding = 3;
            celTittle.HorizontalAlignment = Element.ALIGN_CENTER;
            celTittle.VerticalAlignment = Element.ALIGN_TOP;
            celTittle.Border = 0;
            celTittle.BorderWidthTop = 1;
            celTittle.BorderWidthLeft = 1;
            celTittle.BorderWidthRight = 1;
            celTittle.BackgroundColor = BaseColor.LIGHT_GRAY;
            tableFactura.AddCell(celTittle);

            iTextSharp.text.pdf.PdfPCell celNo = new iTextSharp.text.pdf.PdfPCell(new Phrase("No. ", fontTitleFactura));
            celNo.Colspan = 1;
            celNo.Padding = 5;
            celNo.HorizontalAlignment = Element.ALIGN_CENTER;
            celNo.VerticalAlignment = Element.ALIGN_TOP;
            celNo.Border = 0;
            celNo.BorderWidthLeft = 1;
            celNo.BackgroundColor = BaseColor.LIGHT_GRAY;
            tableFactura.AddCell(celNo);

            iTextSharp.text.pdf.PdfPCell celNoFactura = new iTextSharp.text.pdf.PdfPCell(new Phrase(NumLegalFactura, fontTitleFactura));
            celNoFactura.Colspan = 1;
            celNoFactura.Padding = 5;
            celNoFactura.HorizontalAlignment = Element.ALIGN_LEFT;
            celNoFactura.VerticalAlignment = Element.ALIGN_TOP;
            //celNoFactura.Border = 1;
            celNoFactura.BackgroundColor = BaseColor.WHITE;
            tableFactura.AddCell(celNoFactura);


            iTextSharp.text.pdf.PdfPCell celEspacioNoFactura = new iTextSharp.text.pdf.PdfPCell(new Phrase(" ", fontTitleFactura));
            celEspacioNoFactura.Colspan = 1;
            celEspacioNoFactura.Border = 0;
            //celNo.Padding = 3;
            celEspacioNoFactura.BorderWidthRight = 1;
            celEspacioNoFactura.HorizontalAlignment = Element.ALIGN_CENTER;
            celEspacioNoFactura.VerticalAlignment = Element.ALIGN_TOP;
            celEspacioNoFactura.BackgroundColor = BaseColor.LIGHT_GRAY;
            tableFactura.AddCell(celEspacioNoFactura);

            iTextSharp.text.pdf.PdfPCell celRellenoNoFactura = new iTextSharp.text.pdf.PdfPCell(new Phrase(" ", fontTitleFactura));
            celRellenoNoFactura.Colspan = 3;
            celRellenoNoFactura.Padding = 3;
            celRellenoNoFactura.Border = 0;
            celRellenoNoFactura.BorderWidthLeft = 1;
            celRellenoNoFactura.BorderWidthRight = 1;
            celRellenoNoFactura.BorderWidthBottom = 1;
            celRellenoNoFactura.BackgroundColor = BaseColor.LIGHT_GRAY;
            celRellenoNoFactura.HorizontalAlignment = Element.ALIGN_RIGHT;
            celRellenoNoFactura.VerticalAlignment = Element.ALIGN_TOP;
            tableFactura.AddCell(celRellenoNoFactura);

            iTextSharp.text.pdf.PdfPCell celConsecutivoInterno = new iTextSharp.text.pdf.PdfPCell(new Phrase(NumInternoFactura, fontCustom));
            celConsecutivoInterno.Colspan = 4;
            celConsecutivoInterno.BackgroundColor = BaseColor.WHITE;
            celConsecutivoInterno.HorizontalAlignment = Element.ALIGN_RIGHT;
            celConsecutivoInterno.VerticalAlignment = Element.ALIGN_TOP;
            celConsecutivoInterno.Border = 0;
            tableFactura.AddCell(celConsecutivoInterno);

            PdfDiv divNumeroFactura = new PdfDiv();
            divNumeroFactura.Float = iTextSharp.text.pdf.PdfDiv.FloatType.LEFT;
            divNumeroFactura.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
            divNumeroFactura.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
            divNumeroFactura.PaddingLeft = 5;
            divNumeroFactura.Width = 140;
            divNumeroFactura.AddElement(tableFactura);
            return tableFactura;

            return tableFactura;
        }

        #endregion

        #region Lineas de Detalle

        private bool AddUnidadesInversionesDinastía(DataRow dataLine, ref PdfPTable table, iTextSharp.text.Font fontTitleFactura, DataSet dataSet)
        {
            try
            {
                ////ot
                //strError += "Add InvoiceLine";
                //iTextSharp.text.pdf.PdfPCell celValL = new iTextSharp.text.pdf.PdfPCell(new Phrase((string)dataLine["InvoiceLine"], fontTitleFactura));
                //celValL.Colspan = 1;
                //celValL.Padding = 2;
                //celValL.Border = PdfPCell.LEFT_BORDER | PdfPCell.RIGHT_BORDER;
                //celValL.HorizontalAlignment = Element.ALIGN_CENTER;
                //celValL.VerticalAlignment = Element.ALIGN_TOP;
                //table.AddCell(celValL);
                //strError += "InvoiceLine OK";

                //articulo
                strError += "Add PartNum";
                iTextSharp.text.pdf.PdfPCell celValCodigo = new iTextSharp.text.pdf.PdfPCell(new Phrase((string)dataLine["PartNum"], fontTitleFactura));
                celValCodigo.Colspan = 1;
                celValCodigo.Padding = 2;
                celValCodigo.Border = PdfPCell.RIGHT_BORDER | PdfPCell.LEFT_BORDER;
                celValCodigo.HorizontalAlignment = Element.ALIGN_CENTER;
                celValCodigo.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValCodigo);
                strError += "Add PartNum OK";

                ////O.C
                //strError += "Add POLine";
                //iTextSharp.text.pdf.PdfPCell celValOC = new iTextSharp.text.pdf.PdfPCell(new Phrase((string)dataLine["POLine"], fontTitleFactura));
                //celValOC.Colspan = 1;
                //celValOC.Padding = 2;
                //celValOC.Border = PdfPCell.RIGHT_BORDER;
                //celValOC.HorizontalAlignment = Element.ALIGN_CENTER;
                //celValOC.VerticalAlignment = Element.ALIGN_TOP;
                //table.AddCell(celValOC);
                //strError += "Add POLine OK";

                //cantidad
                strError += "Add SellingShipQty";
                iTextSharp.text.pdf.PdfPCell celValDescripcion = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    decimal.Parse((string)dataLine["SellingShipQty"]).ToString("N"), fontTitleFactura));
                celValDescripcion.Colspan = 1;
                celValDescripcion.Padding = 2;
                celValDescripcion.Border = PdfPCell.RIGHT_BORDER;
                //celValCantidad.BorderWidthLeft = 1;
                celValDescripcion.HorizontalAlignment = Element.ALIGN_CENTER;
                celValDescripcion.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValDescripcion);
                strError += "Add SellingShipQty OK";

                ////embalaje
                //strError += "Add SalesUM";
                //iTextSharp.text.pdf.PdfPCell celValUnidad = new iTextSharp.text.pdf.PdfPCell(new Phrase((string)dataLine["SalesUM"], fontTitleFactura));
                //celValUnidad.Colspan = 1;
                //celValUnidad.Padding = 2;
                //celValUnidad.Border = PdfPCell.RIGHT_BORDER;
                ////celValUnidad.BorderWidthLeft = 1;
                //celValUnidad.HorizontalAlignment = Element.ALIGN_CENTER;
                //celValUnidad.VerticalAlignment = Element.ALIGN_TOP;
                //table.AddCell(celValUnidad);
                //strError += "Add SalesUM OK";

                //valor unitario
                strError += "Add UnitPrice";
                iTextSharp.text.pdf.PdfPCell celValCantidad = new iTextSharp.text.pdf.PdfPCell(new Phrase(decimal.Parse((string)dataLine["UnitPrice"]).ToString("C2"), fontTitleFactura));
                celValCantidad.Colspan = 1;
                celValCantidad.Padding = 2;
                celValCantidad.Border = PdfPCell.RIGHT_BORDER;
                //celValDesc.BorderWidthLeft = 1;
                celValCantidad.HorizontalAlignment = Element.ALIGN_CENTER;
                celValCantidad.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValCantidad);
                strError += "Add UnitPrice OK";

                //subtotal
                strError += "Add DocExtPrice";
                iTextSharp.text.pdf.PdfPCell celValLote = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    decimal.Parse((string)dataLine["DocExtPrice"]).ToString("N2"), fontTitleFactura));
                celValLote.Colspan = 1;
                celValLote.Border = PdfPCell.RIGHT_BORDER;
                //celValValorUnitario.BorderWidthLeft = 1;
                celValLote.Padding = 2;
                celValLote.HorizontalAlignment = Element.ALIGN_CENTER;
                celValLote.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValLote);
                strError += "Add DocExtPrice OK";

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return false;
            }
        }

        #endregion

        #endregion
    }
}
