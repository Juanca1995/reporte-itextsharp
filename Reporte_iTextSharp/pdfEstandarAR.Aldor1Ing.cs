﻿namespace RulesServicesDIAN2.Adquiriente
{
    using iTextSharp.text;
    using iTextSharp.text.pdf;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Diagnostics;
    using System.Drawing;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.NetworkInformation;
    using System.Text;
    using System.Threading.Tasks;
    using System.Web;
    using System.Windows.Forms;

    public partial class pdfEstandarAR
    {
        #region FacturasALDOR Ecuador

        public bool FacturaEng1ALDOR(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {
            bool esLocal = Helpers.Compartido.EsLocal;
            string Ruta = string.Empty;
            string RutaImg = string.Empty;
            string RutaImgIso = string.Empty;
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            if (esLocal)
            {
                Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
                RutaImg = $@"{AppDomain.CurrentDomain.BaseDirectory}\Logo_890911327.png";
                RutaImgIso = $@"{AppDomain.CurrentDomain.BaseDirectory}\Iso_800096040.png";
            }
            else
            {
                Ruta = HttpContext.Current.Server.MapPath("~/PDF/" + NIT + "/");
                RutaImg = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";
                RutaImgIso = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Iso_" + NIT + ".png";
            }
            NomArchivo = string.Empty;
            NomArchivo = NombreInvoice + ".pdf";
            try
            {
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.
                Document document = new Document(iTextSharp.text.PageSize.LETTER);
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;

                // step 3: we open the document     
                document.Open();

                PdfDiv divEspacio = new PdfDiv();
                divEspacio.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio.Height = 10;
                divEspacio.Width = 130;

                PdfDiv divEspacio2 = new PdfDiv();
                divEspacio2.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio2.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio2.Height = 2;
                divEspacio2.Width = 130;


                //FUENTES----------------------------------------------------------------------------------------------------------
                iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle2 = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustom = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);
                //FUENTES---------------------------------------------------------------------------------------------------------



                #region "ENCABEZADO"

                System.Drawing.Image logo = null;
                var requestLogo = WebRequest.Create(RutaImg);

                using (var responseLogo = requestLogo.GetResponse())
                using (var streamLogo = responseLogo.GetResponseStream())
                {
                    logo = Bitmap.FromStream(streamLogo);
                }

                PdfDiv divLogo = new PdfDiv();
                divLogo.Float = iTextSharp.text.pdf.PdfDiv.FloatType.LEFT;
                divLogo.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divLogo.BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID;
                divLogo.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                divLogo.Height = 60;
                divLogo.Width = 90;
                iTextSharp.text.Image ImgLogo = iTextSharp.text.Image.GetInstance(logo, BaseColor.WHITE);
                divLogo.BackgroundImage = ImgLogo;
                //---------------------------------------------------------------------------------------------------------
                Paragraph prgTitle = new Paragraph(string.Format("{0}\nNIT.{1}", DsInvoiceAR.Tables["Company"].Rows[0]["Name"].ToString(), DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"].ToString() + "-9"), fontTitle);

                Paragraph prgTitle2 = new Paragraph(string.Format("{0}\nPBX:{1}\nA.A. 35155\n{2} {3}\nwww.aldoronline.com",
                    DsInvoiceAR.Tables["Company"].Rows[0]["Address1"].ToString(),
                    DsInvoiceAR.Tables["Company"].Rows[0]["PhoneNum"].ToString(),
                    DsInvoiceAR.Tables["Company"].Rows[0]["City"].ToString(),
                    DsInvoiceAR.Tables["Company"].Rows[0]["State"].ToString()),
                    fontCustom);
                prgTitle.Alignment = Element.ALIGN_CENTER;
                prgTitle2.Alignment = Element.ALIGN_CENTER;

                PdfDiv divAcercade = new PdfDiv();
                divAcercade.Float = iTextSharp.text.pdf.PdfDiv.FloatType.LEFT;
                divAcercade.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divAcercade.BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID;
                divAcercade.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divAcercade.PaddingLeft =8;
                divAcercade.Width = 115; //125;
                divAcercade.AddElement(prgTitle);
                divAcercade.AddElement(prgTitle2);

                //-----------------------------------------------------------------------------------------------------
                System.Drawing.Image logo2 = null;
                var requestLogo2 = WebRequest.Create(RutaImgIso);

                using (var responseLogo2 = requestLogo2.GetResponse())
                using (var streamLogo2 = responseLogo2.GetResponseStream())
                {
                    logo2 = Bitmap.FromStream(streamLogo2);
                }

                PdfDiv divLogo2 = new PdfDiv();
                divLogo2.Float = iTextSharp.text.pdf.PdfDiv.FloatType.LEFT;
                divLogo2.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divLogo2.BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID;
                divLogo2.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                divLogo2.Height = 68;
                divLogo2.Width = 105;//115;
                //divLogo2.BackgroundColor = BaseColor.LIGHT_GRAY;
                divLogo2.PaddingRight = 10;
                iTextSharp.text.Image ImgLogo2 = iTextSharp.text.Image.GetInstance(logo2, BaseColor.WHITE);
                divLogo2.BackgroundImage = ImgLogo2;

                //------------------------------------------QR CODE----------------------------------------------------------

                iTextSharp.text.Font fontRepGrafica = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                Paragraph prgQrTittle = new Paragraph("COD QR", fontRepGrafica);
                prgQrTittle.Alignment = Element.ALIGN_CENTER;

                //PdfDiv divQR = new PdfDiv();
                //divQR.Float = iTextSharp.text.pdf.PdfDiv.FloatType.RIGHT;
                //divQR.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                //divQR.BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID;
                //divQR.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divQR.Height = 70;
                //divQR.Width = 70;
                //divQR.BackgroundColor = BaseColor.WHITE;
                //iTextSharp.text.Image ImgQR = iTextSharp.text.Image.GetInstance(QRInvoice, BaseColor.WHITE);
                //ImgQR.Alignment = Element.ALIGN_CENTER;
                //ImgQR.ScaleAbsolute(60f, 60f);
                //divQR.AddElement(prgQrTittle);
                //divQR.AddElement(ImgQR);


                PdfDiv divQR = new PdfDiv();
                divQR.Float = iTextSharp.text.pdf.PdfDiv.FloatType.LEFT;
                divQR.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divQR.BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID;
                divQR.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                divQR.Height = 80;
                divQR.Width = 80;
                divQR.PaddingLeft = 10;
                iTextSharp.text.Image ImgQR = iTextSharp.text.Image.GetInstance(QRInvoice, BaseColor.WHITE);
                ImgQR.Alignment = Element.ALIGN_RIGHT;
                //divQR.BackgroundImage = ImgQR;
                divQR.AddElement(prgQrTittle);
                divQR.AddElement(ImgQR);

                //----------------------------------------------------------------------------------------------------
                iTextSharp.text.Font fontTitleFactura = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitleFacturaDetallePeq = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 5, iTextSharp.text.Font.NORMAL);

                PdfPTable tableFactura = new PdfPTable(3);

                //Dimenciones.
                float[] DimencionFactura = new float[3];
                DimencionFactura[0] = 1.0F;//
                DimencionFactura[1] = 4.0F;//
                DimencionFactura[2] = 0.5F;//

                tableFactura.WidthPercentage = 100;
                tableFactura.SetWidths(DimencionFactura);

                iTextSharp.text.pdf.PdfPCell celTittle = new iTextSharp.text.pdf.PdfPCell(new Phrase("COMMERCIAL INVOICE\n\n", fontTitleFactura));
                celTittle.Colspan = 3;
                celTittle.Padding = 3;
                celTittle.HorizontalAlignment = Element.ALIGN_CENTER;
                celTittle.VerticalAlignment = Element.ALIGN_TOP;
                celTittle.Border = 0;
                celTittle.BorderWidthTop = 1;
                celTittle.BorderWidthLeft = 1;
                celTittle.BorderWidthRight = 1;
                celTittle.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableFactura.AddCell(celTittle);

                iTextSharp.text.pdf.PdfPCell celNo = new iTextSharp.text.pdf.PdfPCell(new Phrase("No. ", fontTitleFactura));
                celNo.Colspan = 1;
                celNo.Padding = 5;
                celNo.HorizontalAlignment = Element.ALIGN_CENTER;
                celNo.VerticalAlignment = Element.ALIGN_TOP;
                celNo.Border = 0;
                celNo.BorderWidthLeft = 1;
                celNo.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableFactura.AddCell(celNo);

                string NumLegalFactura = DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"].ToString();
                //if (InvoiceType == "InvoiceType")
                //    NumLegalFactura = DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiseRef"].ToString();
                //else if (InvoiceType == "CreditNoteType")
                //    NumLegalFactura = DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"].ToString();
                //else
                //    NumLegalFactura = DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"].ToString();

                iTextSharp.text.pdf.PdfPCell celNoFactura = new iTextSharp.text.pdf.PdfPCell(new Phrase(NumLegalFactura, fontTitleFactura));
                celNoFactura.Colspan = 1;
                celNoFactura.Padding = 5;
                celNoFactura.HorizontalAlignment = Element.ALIGN_LEFT;
                celNoFactura.VerticalAlignment = Element.ALIGN_TOP;
                //celNoFactura.Border = 1;
                celNoFactura.BackgroundColor = BaseColor.WHITE;
                tableFactura.AddCell(celNoFactura);


                iTextSharp.text.pdf.PdfPCell celEspacioNoFactura = new iTextSharp.text.pdf.PdfPCell(new Phrase(" ", fontTitleFactura));
                celEspacioNoFactura.Colspan = 1;
                celEspacioNoFactura.Border = 0;
                //celNo.Padding = 3;
                celEspacioNoFactura.BorderWidthRight = 1;
                celEspacioNoFactura.HorizontalAlignment = Element.ALIGN_CENTER;
                celEspacioNoFactura.VerticalAlignment = Element.ALIGN_TOP;
                celEspacioNoFactura.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableFactura.AddCell(celEspacioNoFactura);

                iTextSharp.text.pdf.PdfPCell celRellenoNoFactura = new iTextSharp.text.pdf.PdfPCell(new Phrase(" ", fontTitleFactura));
                celRellenoNoFactura.Colspan = 3;
                celRellenoNoFactura.Padding = 3;
                celRellenoNoFactura.Border = 0;
                celRellenoNoFactura.BorderWidthLeft = 1;
                celRellenoNoFactura.BorderWidthRight = 1;
                celRellenoNoFactura.BorderWidthBottom = 1;
                celRellenoNoFactura.BackgroundColor = BaseColor.LIGHT_GRAY;
                celRellenoNoFactura.HorizontalAlignment = Element.ALIGN_RIGHT;
                celRellenoNoFactura.VerticalAlignment = Element.ALIGN_TOP;
                tableFactura.AddCell(celRellenoNoFactura);

                iTextSharp.text.pdf.PdfPCell celConsecutivoInterno = new iTextSharp.text.pdf.PdfPCell(new Phrase(DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceNum"].ToString(), fontCustom));
                celConsecutivoInterno.Colspan = 4;
                //celConsecutivoInterno.Padding = 3;
                celConsecutivoInterno.BackgroundColor = BaseColor.WHITE;
                celConsecutivoInterno.HorizontalAlignment = Element.ALIGN_RIGHT;
                celConsecutivoInterno.VerticalAlignment = Element.ALIGN_TOP;
                celConsecutivoInterno.Border = 0;
                tableFactura.AddCell(celConsecutivoInterno);

                PdfDiv divNumeroFactura = new PdfDiv();
                divNumeroFactura.Float = iTextSharp.text.pdf.PdfDiv.FloatType.LEFT;
                divNumeroFactura.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                //divNumeroFactura.BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID;
                divNumeroFactura.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divNumeroFactura.BackgroundColor = BaseColor.LIGHT_GRAY;
                divNumeroFactura.PaddingLeft = 5;
                divNumeroFactura.Width = 140;
                divNumeroFactura.AddElement(tableFactura);
                #endregion



                #region "FACTURAR Y DESPACHAR A"

                //------------------------------------------------------------------------------------------------
                PdfPTable tableFacturar = new PdfPTable(3);
                //Dimenciones.
                float[] DimencionFacturar = new float[3];
                DimencionFacturar[0] = 1.0F;

                tableFacturar.WidthPercentage = 100;
                tableFacturar.SetWidths(DimencionFacturar);
                //----------------------------------------------------------------------------------------------
                PdfPTable tableFacturarA = new PdfPTable(2);
                float[] DimencionFacturarA = new float[2];
                DimencionFacturarA[0] = 0.1F;//
                DimencionFacturarA[1] = 0.8F;//

                tableFacturarA.WidthPercentage = 100;
                tableFacturarA.SetWidths(DimencionFacturarA);

                iTextSharp.text.pdf.PdfPCell celDatosFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase("Invoice to:\n", fontTitleFactura));
                celDatosFacturarA.Colspan = 2;
                celDatosFacturarA.Padding = 3;
                celDatosFacturarA.Border = 0;
                celDatosFacturarA.BorderColorBottom = BaseColor.WHITE;
                celDatosFacturarA.HorizontalAlignment = Element.ALIGN_CENTER;
                celDatosFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celDatosFacturarA);

                iTextSharp.text.pdf.PdfPCell celTextClienteFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase("CUSTOMER:", fontTitleFactura));
                celTextClienteFacturarA.Colspan = 1;
                celTextClienteFacturarA.Padding = 3;
                celTextClienteFacturarA.Border = 0;
                celTextClienteFacturarA.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextClienteFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celTextClienteFacturarA);
                //ShipToName

                iTextSharp.text.pdf.PdfPCell celClienteFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase(DsInvoiceAR.Tables["Customer"].Rows[0]["ShipToName"].ToString(),
                //iTextSharp.text.pdf.PdfPCell celClienteFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase(DsInvoiceAR.Tables["Customer"].Rows[0]["Name"].ToString(),
                fontTitle2));
                celClienteFacturarA.Colspan = 1;
                celClienteFacturarA.Padding = 3;
                celClienteFacturarA.Border = 0;
                celClienteFacturarA.HorizontalAlignment = Element.ALIGN_LEFT;
                celClienteFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celClienteFacturarA);

                iTextSharp.text.pdf.PdfPCell celTextNitFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase("ID:", fontTitleFactura));
                celTextNitFacturarA.Colspan = 1;
                celTextNitFacturarA.Padding = 3;
                celTextNitFacturarA.Border = 0;
                celTextNitFacturarA.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextNitFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celTextNitFacturarA);

                iTextSharp.text.pdf.PdfPCell celNitFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase(DsInvoiceAR.Tables["Customer"].Rows[0]["ShipToNit"].ToString(), fontTitle2));
                //iTextSharp.text.pdf.PdfPCell celNitFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase(DsInvoiceAR.Tables["Customer"].Rows[0]["RelaseID"].ToString(), fontTitle2));
                celNitFacturarA.Colspan = 1;
                celNitFacturarA.Padding = 3;
                celNitFacturarA.Border = 0;
                celNitFacturarA.HorizontalAlignment = Element.ALIGN_LEFT;
                celNitFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celNitFacturarA);

                iTextSharp.text.pdf.PdfPCell celTextDireccionFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase("ADDRESS:", fontTitleFactura));
                celTextDireccionFacturarA.Colspan = 1;
                celTextDireccionFacturarA.Padding = 3;
                celTextDireccionFacturarA.Border = 0;
                celTextDireccionFacturarA.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextDireccionFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celTextDireccionFacturarA);

                iTextSharp.text.pdf.PdfPCell celDireccionFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase(DsInvoiceAR.Tables["Customer"].Rows[0]["ShipToAddress"].ToString(), fontTitle2));
                //iTextSharp.text.pdf.PdfPCell celDireccionFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase(DsInvoiceAR.Tables["Customer"].Rows[0]["Address1"].ToString(), fontTitle2));
                celDireccionFacturarA.Colspan = 1;
                celDireccionFacturarA.Padding = 3;
                celDireccionFacturarA.Border = 0;
                celDireccionFacturarA.HorizontalAlignment = Element.ALIGN_LEFT;
                celDireccionFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celDireccionFacturarA);

                iTextSharp.text.pdf.PdfPCell celTextTelFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase("PHONE:", fontTitleFactura));
                celTextTelFacturarA.Colspan = 1;
                celTextTelFacturarA.Padding = 3;
                celTextTelFacturarA.Border = 0;
                celTextTelFacturarA.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextTelFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celTextTelFacturarA);

                iTextSharp.text.pdf.PdfPCell celTelFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase(DsInvoiceAR.Tables["Customer"].Rows[0]["ShipToPhone"].ToString(), fontTitle2));
                //iTextSharp.text.pdf.PdfPCell celTelFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase(DsInvoiceAR.Tables["Customer"].Rows[0]["PhoneNum"].ToString(), fontTitle2));
                celTelFacturarA.Colspan = 1;
                celTelFacturarA.Padding = 3;
                celTelFacturarA.Border = 0;
                celTelFacturarA.HorizontalAlignment = Element.ALIGN_LEFT;
                celTelFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celTelFacturarA);

                iTextSharp.text.pdf.PdfPCell celTextCiudadFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase("CITY:", fontTitleFactura));
                celTextCiudadFacturarA.Colspan = 1;
                celTextCiudadFacturarA.Padding = 3;
                celTextCiudadFacturarA.Border = 0;
                celTextCiudadFacturarA.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextCiudadFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celTextCiudadFacturarA);

                iTextSharp.text.pdf.PdfPCell celCiudadFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase(DsInvoiceAR.Tables["Customer"].Rows[0]["ShipToCity"].ToString(), fontTitle2));
                //iTextSharp.text.pdf.PdfPCell celCiudadFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase(DsInvoiceAR.Tables["Customer"].Rows[0]["City"].ToString(), fontTitle2));
                celCiudadFacturarA.Colspan = 1;
                celCiudadFacturarA.Padding = 3;
                celCiudadFacturarA.Border = 0;
                celCiudadFacturarA.HorizontalAlignment = Element.ALIGN_LEFT;
                celCiudadFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celCiudadFacturarA);

                iTextSharp.text.pdf.PdfPCell celTextPaisFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase("COUNTRY:", fontTitleFactura));
                celTextPaisFacturarA.Colspan = 1;
                celTextPaisFacturarA.Padding = 3;
                celTextPaisFacturarA.Border = 0;
                celTextPaisFacturarA.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextPaisFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celTextPaisFacturarA);

                iTextSharp.text.pdf.PdfPCell celPaisFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase(DsInvoiceAR.Tables["Customer"].Rows[0]["ShipToCountry"].ToString(), fontTitle2));
                //iTextSharp.text.pdf.PdfPCell celPaisFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase(DsInvoiceAR.Tables["Customer"].Rows[0]["Country"].ToString(), fontTitle2));
                celPaisFacturarA.Colspan = 1;
                celPaisFacturarA.Padding = 3;
                celPaisFacturarA.Border = 0;
                celPaisFacturarA.HorizontalAlignment = Element.ALIGN_LEFT;
                celPaisFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celPaisFacturarA);

                iTextSharp.text.pdf.PdfPCell celTittleFacturarA = new iTextSharp.text.pdf.PdfPCell(tableFacturarA);
                tableFacturar.AddCell(celTittleFacturarA);
                //----------------------------------------------------------------------------------------------------------------------------
                //Espacio pequeño
                iTextSharp.text.pdf.PdfPCell celEspacio2 = new iTextSharp.text.pdf.PdfPCell(new Phrase(" ", fontTitleFactura));
                celEspacio2.Border = 0;
                tableFacturar.AddCell(celEspacio2);

                //------------------------------------------------------------------------------------------ship

                iTextSharp.text.pdf.PdfPCell celTextCufe = new iTextSharp.text.pdf.PdfPCell(new Phrase("CUFE: " + CUFE + "\nGraphic representation electronic invoice", fontTitle2));
                celTextCufe.Colspan = 3;
                celTextCufe.Padding = 0;
                celTextCufe.Border = 0;
                celTextCufe.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextCufe.VerticalAlignment = Element.ALIGN_CENTER;
                tableFacturar.AddCell(celEspacio2);
                tableFacturar.AddCell(celTextCufe);
                //-------------------------------------------------------------------------------------- 
                #endregion

                #region "Tabla de Detalles"

                //------------------------------------------------------------------------------------------------
                PdfPTable tableDetalles2 = new PdfPTable(4);
                tableDetalles2.WidthPercentage = 100;

                PdfPTable tableFechaFactura = new PdfPTable(2);
                float[] dimecionesTablaFecha = new float[2];
                dimecionesTablaFecha[0] = 1.3F;
                dimecionesTablaFecha[1] = 0.9F;
                tableFechaFactura.SetWidths(dimecionesTablaFecha);

                iTextSharp.text.pdf.PdfPCell celTextFechaFactura = new iTextSharp.text.pdf.PdfPCell(new Phrase("INVOICE DATE: ",
                    fontTitle));
                celTextFechaFactura.Colspan = 1;
                celTextFechaFactura.Padding = 7;
                celTextFechaFactura.Border = 0;
                celTextFechaFactura.BorderColorBottom = BaseColor.WHITE;
                celTextFechaFactura.HorizontalAlignment = Element.ALIGN_CENTER;
                celTextFechaFactura.VerticalAlignment = Element.ALIGN_CENTER;
                tableFechaFactura.AddCell(celTextFechaFactura);

                iTextSharp.text.pdf.PdfPCell celFechaFactura = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:MM/dd/yyyy}", DateTime.Parse(DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"].ToString())),
                fontCustom));
                celFechaFactura.Colspan = 1;
                celFechaFactura.Padding = 7;
                //celFechaFactura.PaddingTop = 4;
                celFechaFactura.Border = 0;
                celFechaFactura.BorderColorBottom = BaseColor.WHITE;
                celFechaFactura.HorizontalAlignment = Element.ALIGN_LEFT;
                celFechaFactura.VerticalAlignment = Element.ALIGN_CENTER;
                tableFechaFactura.AddCell(celFechaFactura);

                PdfPCell _celFechaFactura = new PdfPCell(tableFechaFactura);
                tableDetalles2.AddCell(_celFechaFactura);


                PdfPTable tableFechaVencimiento = new PdfPTable(2);
                float[] dmTablaFechaVencimiento = new float[2];
                dmTablaFechaVencimiento[0] = 1.4F;
                dmTablaFechaVencimiento[1] = 0.8F;
                tableFechaVencimiento.SetWidths(dmTablaFechaVencimiento);

                iTextSharp.text.pdf.PdfPCell celTextFechaVencimiento = new iTextSharp.text.pdf.PdfPCell(new Phrase("EXPIRY DATE: ",
                    fontTitle));
                celTextFechaVencimiento.Colspan = 1;
                celTextFechaVencimiento.PaddingTop = 7;
                celTextFechaVencimiento.Border = 0;
                celTextFechaVencimiento.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextFechaVencimiento.VerticalAlignment = Element.ALIGN_TOP;
                tableFechaVencimiento.AddCell(celTextFechaVencimiento);

                iTextSharp.text.pdf.PdfPCell celFechaVencimiento = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:MM/dd/yyyy}", DateTime.Parse(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"].ToString())),
                fontCustom));
                celFechaVencimiento.Colspan = 1;
                celFechaVencimiento.Padding = 7;
                celFechaVencimiento.Border = 0;
                celFechaVencimiento.BorderColorBottom = BaseColor.WHITE;
                celFechaVencimiento.HorizontalAlignment = Element.ALIGN_LEFT;
                celFechaVencimiento.VerticalAlignment = Element.ALIGN_TOP;
                tableFechaVencimiento.AddCell(celFechaVencimiento);

                PdfPCell _celFechaFacturaVencimiento = new PdfPCell(tableFechaVencimiento);
                tableDetalles2.AddCell(_celFechaFacturaVencimiento);

                PdfPTable tableMoneda = new PdfPTable(2);
                float[] dimecionesTablaMoneda = new float[2];
                dimecionesTablaMoneda[0] = 0.8F;
                dimecionesTablaMoneda[1] = 1.0F;
                tableMoneda.SetWidths(dimecionesTablaMoneda);

                iTextSharp.text.pdf.PdfPCell celTextMoneda = new iTextSharp.text.pdf.PdfPCell(new Phrase("CURRENCY: ", fontTitle));
                celTextMoneda.Colspan = 1;
                celTextMoneda.Padding = 7;
                celTextMoneda.Border = 0;
                celTextMoneda.BorderColorBottom = BaseColor.WHITE;
                celTextMoneda.HorizontalAlignment = Element.ALIGN_RIGHT;
                celTextMoneda.VerticalAlignment = Element.ALIGN_TOP;
                tableMoneda.AddCell(celTextMoneda);

                iTextSharp.text.pdf.PdfPCell celMoneda = new iTextSharp.text.pdf.PdfPCell(new Phrase(DsInvoiceAR.Tables["InvcHead"].Rows[0]["CurrencyCode"].ToString(),
                fontCustom));
                celMoneda.Colspan = 1;
                celMoneda.Padding = 7;
                celMoneda.Border = 0;
                celMoneda.BorderColorBottom = BaseColor.WHITE;
                celMoneda.HorizontalAlignment = Element.ALIGN_LEFT;
                celMoneda.VerticalAlignment = Element.ALIGN_TOP;
                tableMoneda.AddCell(celMoneda);

                PdfPCell _celMoneda = new PdfPCell(tableMoneda);
                tableDetalles2.AddCell(_celMoneda);

                PdfPTable tableFormaPago = new PdfPTable(2);
                float[] dimecionesTablaFormaPago = new float[2];
                dimecionesTablaFormaPago[0] = 0.4F;
                dimecionesTablaFormaPago[1] = 1.0F;
                tableFormaPago.SetWidths(dimecionesTablaFormaPago);

                iTextSharp.text.pdf.PdfPCell celTextFormaPago = new iTextSharp.text.pdf.PdfPCell(new Phrase("PAYMENT TERMS: " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["TermsCodeDescription"].ToString(), fontTitle));
                celTextFormaPago.Colspan = 2;
                celTextFormaPago.Padding = 7;
                celTextFormaPago.Border = 0;
                celTextFormaPago.BorderColorBottom = BaseColor.WHITE;
                celTextFormaPago.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextFormaPago.VerticalAlignment = Element.ALIGN_TOP;
                tableFormaPago.AddCell(celTextFormaPago);

                PdfPCell _celFormaPago = new PdfPCell(tableFormaPago);
                tableDetalles2.AddCell(_celFormaPago);

                //-----------------------------------------------------------------------------------------
                PdfPTable tableDetalles = new PdfPTable(5);
                tableDetalles.PaddingTop = 20;
                //Dimenciones.
                float[] DimencionDetalles = new float[5];
                DimencionDetalles[0] = 2.7F;//
                DimencionDetalles[1] = 2F;//
                DimencionDetalles[2] = 1.8F;//
                DimencionDetalles[3] = 2F;//
                DimencionDetalles[4] = 1.3F;//

                tableDetalles.WidthPercentage = 100;
                tableDetalles.SetWidths(DimencionDetalles);

                iTextSharp.text.pdf.PdfPCell celVendedor = new iTextSharp.text.pdf.PdfPCell(new Phrase("SALESPERSON", fontTitleFactura));
                celVendedor.Colspan = 1;
                celVendedor.Padding = 3;
                //celVendedor.Border = 0;
                celVendedor.BorderColorBottom = BaseColor.WHITE;
                celVendedor.HorizontalAlignment = Element.ALIGN_CENTER;
                celVendedor.VerticalAlignment = Element.ALIGN_TOP;
                celVendedor.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableDetalles.AddCell(celVendedor);

                iTextSharp.text.pdf.PdfPCell celOC_Cliente = new iTextSharp.text.pdf.PdfPCell(new Phrase("PO", fontTitleFactura));
                celOC_Cliente.Colspan = 1;
                celOC_Cliente.Padding = 3;
                //celVendedor.Border = 0;
                celOC_Cliente.BorderColorBottom = BaseColor.WHITE;
                celOC_Cliente.HorizontalAlignment = Element.ALIGN_CENTER;
                celOC_Cliente.VerticalAlignment = Element.ALIGN_TOP;
                celOC_Cliente.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableDetalles.AddCell(celOC_Cliente);

                iTextSharp.text.pdf.PdfPCell celOrdenVenta = new iTextSharp.text.pdf.PdfPCell(new Phrase("ORDER", fontTitleFactura));
                celOrdenVenta.Colspan = 1;
                celOrdenVenta.Padding = 3;
                //celVendedor.Border = 0;
                celOrdenVenta.BorderColorBottom = BaseColor.WHITE;
                celOrdenVenta.HorizontalAlignment = Element.ALIGN_CENTER;
                celOrdenVenta.VerticalAlignment = Element.ALIGN_TOP;
                celOrdenVenta.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableDetalles.AddCell(celOrdenVenta);

                iTextSharp.text.pdf.PdfPCell celLocalizacion = new iTextSharp.text.pdf.PdfPCell(new Phrase("WHSE", fontTitleFactura));
                celLocalizacion.Colspan = 1;
                celLocalizacion.Padding = 3;
                //celVendedor.Border = 0;
                celLocalizacion.BackgroundColor = BaseColor.LIGHT_GRAY;
                celLocalizacion.BorderColorBottom = BaseColor.WHITE;
                celLocalizacion.HorizontalAlignment = Element.ALIGN_CENTER;
                celLocalizacion.VerticalAlignment = Element.ALIGN_TOP;
                tableDetalles.AddCell(celLocalizacion);

                iTextSharp.text.pdf.PdfPCell celRemision = new iTextSharp.text.pdf.PdfPCell(new Phrase("SHIPMENT", fontTitleFactura));
                celRemision.Colspan = 1;
                celRemision.Padding = 3;
                celRemision.BackgroundColor = BaseColor.LIGHT_GRAY;
                //celVendedor.Border = 0;
                celRemision.BorderColorBottom = BaseColor.WHITE;
                celRemision.HorizontalAlignment = Element.ALIGN_CENTER;
                celRemision.VerticalAlignment = Element.ALIGN_TOP;
                tableDetalles.AddCell(celRemision);

                iTextSharp.text.pdf.PdfPCell celDataVendedor = new iTextSharp.text.pdf.PdfPCell(new Phrase(DsInvoiceAR.Tables["Customer"].Rows[0]["SalesPersonShipTo"].ToString(), fontCustom));
                celDataVendedor.Colspan = 1;
                celDataVendedor.Padding = 3;
                //celVendedor.Border = 0;
                celDataVendedor.BorderColorBottom = BaseColor.WHITE;
                celDataVendedor.HorizontalAlignment = Element.ALIGN_CENTER;
                celDataVendedor.VerticalAlignment = Element.ALIGN_TOP;
                tableDetalles.AddCell(celDataVendedor);

                iTextSharp.text.pdf.PdfPCell celDataOC_Cliente = new iTextSharp.text.pdf.PdfPCell(new Phrase(DsInvoiceAR.Tables["InvcHead"].Rows[0]["PONum"].ToString(), fontCustom));
                celDataOC_Cliente.Colspan = 1;
                celDataOC_Cliente.Padding = 3;
                //celVendedor.Border = 0;
                celDataOC_Cliente.BorderColorBottom = BaseColor.WHITE;
                celDataOC_Cliente.HorizontalAlignment = Element.ALIGN_CENTER;
                celDataOC_Cliente.VerticalAlignment = Element.ALIGN_TOP;
                tableDetalles.AddCell(celDataOC_Cliente);

                iTextSharp.text.pdf.PdfPCell celDataOrdenVenta = new iTextSharp.text.pdf.PdfPCell(new Phrase(DsInvoiceAR.Tables["InvcHead"].Rows[0]["OrderNum"].ToString(), fontCustom));
                celDataOrdenVenta.Colspan = 1;
                celDataOrdenVenta.Padding = 3;
                //celVendedor.Border = 0;
                celDataOrdenVenta.BorderColorBottom = BaseColor.WHITE;
                celDataOrdenVenta.HorizontalAlignment = Element.ALIGN_CENTER;
                celDataOrdenVenta.VerticalAlignment = Element.ALIGN_TOP;
                tableDetalles.AddCell(celDataOrdenVenta);


                //iTextSharp.text.pdf.PdfPCell celDataLocalizacion = new iTextSharp.text.pdf.PdfPCell(new Phrase(DsInvoiceAR.Tables["InvcHead"].Rows[0]["FOB"].ToString(), fontCustom));
                iTextSharp.text.pdf.PdfPCell celDataLocalizacion = new iTextSharp.text.pdf.PdfPCell(new Phrase(DsInvoiceAR.Tables["InvcDtl"].Rows[0]["WarehouseCode"].ToString(), fontCustom));
                celDataLocalizacion.Colspan = 1;
                celDataLocalizacion.Padding = 3;
                //celDataLocalizacion.Border = 0;
                celDataLocalizacion.BorderColorBottom = BaseColor.WHITE;
                celDataLocalizacion.HorizontalAlignment = Element.ALIGN_CENTER;
                celDataLocalizacion.VerticalAlignment = Element.ALIGN_TOP;
                tableDetalles.AddCell(celDataLocalizacion);

                iTextSharp.text.pdf.PdfPCell celDataRemision = new iTextSharp.text.pdf.PdfPCell(new Phrase(DsInvoiceAR.Tables["InvcDtl"].Rows[0]["PackNum"].ToString(), fontCustom));
                celDataRemision.Colspan = 1;
                celDataRemision.Padding = 3;
                //celVendedor.Border = 0;
                celDataRemision.BorderColorBottom = BaseColor.WHITE;
                celDataRemision.HorizontalAlignment = Element.ALIGN_CENTER;
                celDataRemision.VerticalAlignment = Element.ALIGN_TOP;
                tableDetalles.AddCell(celDataRemision);


                #endregion


                #region "Tabla Unidades"

                PdfPTable tableTituloUnidades = new PdfPTable(7);
                //Dimenciones.
                float[] DimencionUnidades = new float[7];
                DimencionUnidades[0] = 0.8F;//codigo
                DimencionUnidades[1] = 4.7F;//descripcion
                DimencionUnidades[2] = 0.8F;//cantidad
                DimencionUnidades[3] = 0.6F;//um
                DimencionUnidades[4] = 1F;//valor unitario
                DimencionUnidades[5] = 1F;//descuentos
                DimencionUnidades[6] = 1.25F;//Valor Total x Linea
                //DimencionUnidades[7] = 1.2F;//Porcentaje de Iva

                tableTituloUnidades.WidthPercentage = 100;
                tableTituloUnidades.SetWidths(DimencionUnidades);

                iTextSharp.text.pdf.PdfPCell celCodigo = new iTextSharp.text.pdf.PdfPCell(new Phrase("CODE", fontTitleFactura));
                celCodigo.Colspan = 1;
                celCodigo.PaddingTop = 6;
                //celCodigo.Border = 1;
                celCodigo.BorderWidthRight = 0;
                celCodigo.BackgroundColor = BaseColor.LIGHT_GRAY;
                celCodigo.HorizontalAlignment = Element.ALIGN_CENTER;
                celCodigo.VerticalAlignment = Element.ALIGN_TOP;

                tableTituloUnidades.AddCell(celCodigo);

                iTextSharp.text.pdf.PdfPCell celDescripcion = new iTextSharp.text.pdf.PdfPCell(new Phrase("DESCRIPTION", fontTitleFactura));
                celDescripcion.Colspan = 1;
                celDescripcion.PaddingTop = 6;
                //celDescripcion.Border = 1;
                celDescripcion.BorderWidthRight = 0;
                celDescripcion.BackgroundColor = BaseColor.LIGHT_GRAY;
                celDescripcion.HorizontalAlignment = Element.ALIGN_CENTER;
                celDescripcion.VerticalAlignment = Element.ALIGN_TOP;

                tableTituloUnidades.AddCell(celDescripcion);

                iTextSharp.text.pdf.PdfPCell celCantidad = new iTextSharp.text.pdf.PdfPCell(new Phrase("QTY", fontTitleFactura));
                celCantidad.Colspan = 1;
                celCantidad.PaddingTop = 6;
                //celVendedor.Border = 0;
                celCantidad.BorderWidthRight = 0;
                celCantidad.BackgroundColor = BaseColor.LIGHT_GRAY;
                celCantidad.HorizontalAlignment = Element.ALIGN_CENTER;
                celCantidad.VerticalAlignment = Element.ALIGN_TOP;
                tableTituloUnidades.AddCell(celCantidad);

                iTextSharp.text.pdf.PdfPCell celUM = new iTextSharp.text.pdf.PdfPCell(new Phrase("U.M.", fontTitleFactura));
                celUM.Colspan = 1;
                celUM.PaddingTop = 6;
                //celVendedor.Border = 0;
                celUM.BorderWidthRight = 0;
                celUM.BackgroundColor = BaseColor.LIGHT_GRAY;
                celUM.HorizontalAlignment = Element.ALIGN_CENTER;
                celUM.VerticalAlignment = Element.ALIGN_TOP;

                tableTituloUnidades.AddCell(celUM);

                iTextSharp.text.pdf.PdfPCell celValorUnit = new iTextSharp.text.pdf.PdfPCell(new Phrase("UNIT VALUE", fontTitleFactura));
                celValorUnit.Colspan = 1;
                celValorUnit.PaddingTop = 6;
                //celVendedor.Border = 0;
                celValorUnit.BorderWidthRight = 0;
                celValorUnit.BackgroundColor = BaseColor.LIGHT_GRAY;
                celValorUnit.HorizontalAlignment = Element.ALIGN_CENTER;
                celValorUnit.VerticalAlignment = Element.ALIGN_TOP;
                tableTituloUnidades.AddCell(celValorUnit);

                iTextSharp.text.pdf.PdfPTable tableDescuentos = new PdfPTable(2);

                iTextSharp.text.pdf.PdfPCell celDescuentos = new iTextSharp.text.pdf.PdfPCell(new Phrase("DISCOUNT", fontTitleFactura));
                celDescuentos.Colspan = 2;
                celDescuentos.HorizontalAlignment = Element.ALIGN_CENTER;
                celDescuentos.VerticalAlignment = Element.ALIGN_TOP;
                celDescuentos.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableDescuentos.AddCell(celDescuentos);

                iTextSharp.text.pdf.PdfPCell celDesc1 = new iTextSharp.text.pdf.PdfPCell(new Phrase("DCTO1", fontTitleFacturaDetallePeq));
                celDesc1.Colspan = 1;
                celDesc1.HorizontalAlignment = Element.ALIGN_CENTER;
                celDesc1.VerticalAlignment = Element.ALIGN_TOP;
                celDesc1.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableDescuentos.AddCell(celDesc1);

                iTextSharp.text.pdf.PdfPCell celDesc2 = new iTextSharp.text.pdf.PdfPCell(new Phrase("DCTO2", fontTitleFacturaDetallePeq));
                celDesc2.Colspan = 1;
                celDesc2.HorizontalAlignment = Element.ALIGN_CENTER;
                celDesc2.VerticalAlignment = Element.ALIGN_TOP;
                celDesc2.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableDescuentos.AddCell(celDesc2);

                iTextSharp.text.pdf.PdfPCell _celDescuentos12 = new iTextSharp.text.pdf.PdfPCell(tableDescuentos);
                tableTituloUnidades.AddCell(_celDescuentos12);

                iTextSharp.text.pdf.PdfPCell celValorTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase("TOTAL VALUE", fontTitleFactura));
                celValorTotal.Colspan = 1;
                celValorTotal.PaddingTop = 6;
                celValorTotal.HorizontalAlignment = Element.ALIGN_CENTER;
                celValorTotal.VerticalAlignment = Element.ALIGN_CENTER;
                celValorTotal.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableTituloUnidades.AddCell(celValorTotal);

                PdfPTable tableUnidades = new PdfPTable(7);
                tableUnidades.WidthPercentage = 100;
                tableUnidades.SetWidths(DimencionUnidades);

                //decimal TotalDescountInvc = 0;
                foreach (DataRow InvoiceLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
                {
                    //TotalDescountInvc += Convert.ToDecimal(InvoiceLine["DspDocDiscount"]);
                    if (!AddUnidadesALDOREng1(InvoiceLine, ref tableUnidades, fontCustom, DsInvoiceAR))
                        return false;
                }

                iTextSharp.text.pdf.PdfPCell LineaFinal = new iTextSharp.text.pdf.PdfPCell();
                LineaFinal.Colspan = 8;
                LineaFinal.Padding = 3;
                LineaFinal.Border = 1;
                LineaFinal.BorderColorBottom = BaseColor.WHITE;
                LineaFinal.HorizontalAlignment = Element.ALIGN_CENTER;
                LineaFinal.VerticalAlignment = Element.ALIGN_TOP;
                tableUnidades.AddCell(LineaFinal);

                //-----------------------------------------------------------------------------------------
                PdfPTable tableTotalUnidades = new PdfPTable(5);
                tableTotalUnidades.PaddingTop = 20;
                //Dimenciones.
                float[] DimencionTotalUnidades = new float[5];
                DimencionTotalUnidades[0] = 1.0F;//
                DimencionTotalUnidades[1] = 1.0F;//
                DimencionTotalUnidades[2] = 1.0F;//
                DimencionTotalUnidades[3] = 1.0F;//
                DimencionTotalUnidades[4] = 1.0F;//
                //DimencionTotalUnidades[5] = 1.0F;//

                tableTotalUnidades.WidthPercentage = 100;
                tableTotalUnidades.SetWidths(DimencionTotalUnidades);

                iTextSharp.text.pdf.PdfPCell celTotalUnidades = new iTextSharp.text.pdf.PdfPCell(new Phrase("TOTAL UNITS", fontTitleFactura));
                celTotalUnidades.Colspan = 1;
                celTotalUnidades.Padding = 3;
                //celVendedor.Border = 0;
                celTotalUnidades.BorderColorBottom = BaseColor.WHITE;
                celTotalUnidades.HorizontalAlignment = Element.ALIGN_CENTER;
                celTotalUnidades.VerticalAlignment = Element.ALIGN_TOP;
                celTotalUnidades.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableTotalUnidades.AddCell(celTotalUnidades);

                iTextSharp.text.pdf.PdfPCell celPesoNeto = new iTextSharp.text.pdf.PdfPCell(new Phrase("NET WEIGHT", fontTitleFactura));
                celPesoNeto.Colspan = 1;
                celPesoNeto.Padding = 3;
                //celVendedor.Border = 0;
                celPesoNeto.BorderColorBottom = BaseColor.WHITE;
                celPesoNeto.HorizontalAlignment = Element.ALIGN_CENTER;
                celPesoNeto.VerticalAlignment = Element.ALIGN_TOP;
                celPesoNeto.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableTotalUnidades.AddCell(celPesoNeto);

                iTextSharp.text.pdf.PdfPCell celPesoBruto = new iTextSharp.text.pdf.PdfPCell(new Phrase("GROSS WEIGHT", fontTitleFactura));
                celPesoBruto.Colspan = 1;
                celPesoBruto.Padding = 3;
                //celVendedor.Border = 0;
                celPesoBruto.BorderColorBottom = BaseColor.WHITE;
                celPesoBruto.HorizontalAlignment = Element.ALIGN_CENTER;
                celPesoBruto.VerticalAlignment = Element.ALIGN_TOP;
                celPesoBruto.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableTotalUnidades.AddCell(celPesoBruto);

                iTextSharp.text.pdf.PdfPCell celSubTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase("SUBTOTAL", fontTitleFactura));
                celSubTotal.Colspan = 1;
                celSubTotal.Padding = 3;
                //celVendedor.Border = 0;
                celSubTotal.BackgroundColor = BaseColor.LIGHT_GRAY;
                celSubTotal.BorderColorBottom = BaseColor.WHITE;
                celSubTotal.HorizontalAlignment = Element.ALIGN_CENTER;
                celSubTotal.VerticalAlignment = Element.ALIGN_TOP;
                tableTotalUnidades.AddCell(celSubTotal);

                iTextSharp.text.pdf.PdfPCell celTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase("TOTAL", fontTitleFactura));
                celTotal.Colspan = 1;
                celTotal.Padding = 3;
                celTotal.BackgroundColor = BaseColor.LIGHT_GRAY;
                //celVendedor.Border = 0;
                celTotal.BorderColorBottom = BaseColor.WHITE;
                celTotal.HorizontalAlignment = Element.ALIGN_CENTER;
                celTotal.VerticalAlignment = Element.ALIGN_TOP;
                tableTotalUnidades.AddCell(celTotal);


                decimal SellingQty = 0;
                foreach (DataRow InvcLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
                    SellingQty += Convert.ToDecimal(InvcLine["SellingShipQty"]);

                string strSellingShipQty = string.Empty;
                if (DsInvoiceAR.Tables["InvcHead"].Rows[0]["CurrencyCode"].ToString().ToUpper() == "COP")
                    strSellingShipQty = string.Format("{0:N2}", SellingQty);
                else
                    strSellingShipQty = string.Format("{0:N2}", SellingQty);


                iTextSharp.text.pdf.PdfPCell _celTotalUnidades = new iTextSharp.text.pdf.PdfPCell(new Phrase(strSellingShipQty, fontTitleFactura));
                _celTotalUnidades.Colspan = 1;
                _celTotalUnidades.Padding = 3;
                //celVendedor.Border = 0;
                _celTotalUnidades.HorizontalAlignment = Element.ALIGN_RIGHT;
                _celTotalUnidades.VerticalAlignment = Element.ALIGN_TOP;
                tableTotalUnidades.AddCell(_celTotalUnidades);


                iTextSharp.text.pdf.PdfPCell _celPesoNeto = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:N2}", Convert.ToDecimal(DsInvoiceAR.Tables["InvcHead"].Rows[0]["PartNetWeight_c"].ToString())), fontTitleFactura));
                _celPesoNeto.Colspan = 1;
                _celPesoNeto.Padding = 3;
                //celVendedor.Border = 0;
                _celPesoNeto.HorizontalAlignment = Element.ALIGN_RIGHT;
                _celPesoNeto.VerticalAlignment = Element.ALIGN_TOP;
                tableTotalUnidades.AddCell(_celPesoNeto);

                iTextSharp.text.pdf.PdfPCell _celPesoBruto = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:N2}", Convert.ToDecimal(DsInvoiceAR.Tables["InvcHead"].Rows[0]["GrossW_c"].ToString())), fontTitleFactura));
                _celPesoBruto.Colspan = 1;
                _celPesoBruto.Padding = 3;
                //celVendedor.Border = 0;
                _celPesoBruto.HorizontalAlignment = Element.ALIGN_RIGHT;
                _celPesoBruto.VerticalAlignment = Element.ALIGN_TOP;
                tableTotalUnidades.AddCell(_celPesoBruto);

                iTextSharp.text.pdf.PdfPCell _celTotalUnidadesSubTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:C2}", Convert.ToDecimal(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubTotal"].ToString())), fontTitleFactura));
                _celTotalUnidadesSubTotal.Colspan = 1;
                _celTotalUnidadesSubTotal.Padding = 3;
                //celVendedor.Border = 0;
                _celTotalUnidadesSubTotal.HorizontalAlignment = Element.ALIGN_RIGHT;
                _celTotalUnidadesSubTotal.VerticalAlignment = Element.ALIGN_TOP;
                tableTotalUnidades.AddCell(_celTotalUnidadesSubTotal);

                //Nota :  Se supone que en factura del exterior no tiene iva, entonces este seria igual al Sub total
                iTextSharp.text.pdf.PdfPCell _celTotalUnidadesTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:C2}", Convert.ToDecimal(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"].ToString())), fontTitleFactura));
                _celTotalUnidadesTotal.Colspan = 1;
                _celTotalUnidadesTotal.Padding = 3;
                //celVendedor.Border = 0;
                _celTotalUnidadesTotal.HorizontalAlignment = Element.ALIGN_RIGHT;
                _celTotalUnidadesTotal.VerticalAlignment = Element.ALIGN_TOP;
                tableTotalUnidades.AddCell(_celTotalUnidadesTotal);

                //------------------------------------------------------------------------------------------------
                PdfPTable tableValorLetras = new PdfPTable(2);

                float[] DimencionValorLetras = new float[2];
                DimencionValorLetras[0] = 1.0F;//
                DimencionValorLetras[1] = 5.0F;//

                tableValorLetras.WidthPercentage = 100;
                tableValorLetras.SetWidths(DimencionValorLetras);

                iTextSharp.text.pdf.PdfPCell celTextValorLetras = new iTextSharp.text.pdf.PdfPCell(new Phrase("VALUE: ", fontTitleFactura));
                celTextValorLetras.Colspan = 1;
                celTextValorLetras.Padding = 3;
                celTextValorLetras.BackgroundColor = BaseColor.LIGHT_GRAY;
                celTextValorLetras.BorderWidthRight = 0;
                celTextValorLetras.BorderWidthBottom = 0;
                celTextValorLetras.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextValorLetras.VerticalAlignment = Element.ALIGN_TOP;
                tableValorLetras.AddCell(celTextValorLetras);

                string NroLetrasTex = ToWords(Convert.ToDecimal(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"].ToString()));

                if (DsInvoiceAR.Tables["InvcHead"].Rows[0]["CurrencyCode"].ToString().ToUpper() != "COP")
                {
                    NroLetrasTex = NroLetrasTex.Replace("PESOS", "CENTAVOS");
                }


                iTextSharp.text.pdf.PdfPCell celValorLetras = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0}", NroLetrasTex), fontTitle2));
                celValorLetras.Colspan = 1;
                celValorLetras.Padding = 3;
                celValorLetras.BorderWidthLeft = 0;
                celValorLetras.BorderWidthBottom = 0;
                celValorLetras.HorizontalAlignment = Element.ALIGN_LEFT;
                celValorLetras.VerticalAlignment = Element.ALIGN_TOP;
                tableValorLetras.AddCell(celValorLetras);


                iTextSharp.text.pdf.PdfPCell celEspacioValorLetras = new iTextSharp.text.pdf.PdfPCell(new Phrase(" "));
                celEspacioValorLetras.Colspan = 2;
                celEspacioValorLetras.Padding = 3;
                celEspacioValorLetras.BorderWidthTop = 0;
                celEspacioValorLetras.HorizontalAlignment = Element.ALIGN_LEFT;
                celEspacioValorLetras.VerticalAlignment = Element.ALIGN_TOP;
                tableValorLetras.AddCell(celEspacioValorLetras);
                //------------------------------------------------------------------------------------------------
                PdfPTable tableObservaciones = new PdfPTable(2);

                float[] DimencionObservaciones = new float[2];
                DimencionObservaciones[0] = 1.0F;//
                DimencionObservaciones[1] = 5.0F;//

                tableObservaciones.WidthPercentage = 100;
                tableObservaciones.SetWidths(DimencionObservaciones);

                iTextSharp.text.pdf.PdfPCell celTextObservaciones = new iTextSharp.text.pdf.PdfPCell(new Phrase("NOTES: ", fontTitleFactura));
                celTextObservaciones.Colspan = 1;
                celTextObservaciones.Padding = 3;
                celTextObservaciones.BackgroundColor = BaseColor.LIGHT_GRAY;
                celTextObservaciones.BorderWidthRight = 0;
                celTextObservaciones.BorderWidthBottom = 0;
                celTextObservaciones.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextObservaciones.VerticalAlignment = Element.ALIGN_TOP;
                tableObservaciones.AddCell(celTextObservaciones);

                iTextSharp.text.pdf.PdfPCell celObservaciones = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("\n{0}", DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceComment"].ToString()), fontTitle2));
                celObservaciones.Colspan = 1;
                celObservaciones.Padding = 3;
                celObservaciones.BorderWidthLeft = 0;
                celObservaciones.BorderWidthBottom = 0;
                celObservaciones.HorizontalAlignment = Element.ALIGN_LEFT;
                celObservaciones.VerticalAlignment = Element.ALIGN_TOP;
                tableObservaciones.AddCell(celObservaciones);

                iTextSharp.text.pdf.PdfPCell celEspacioObservaciones = new iTextSharp.text.pdf.PdfPCell(new Phrase("\n\n\n\n"));
                celEspacioObservaciones.Colspan = 2;
                celEspacioObservaciones.Padding = 3;
                celEspacioObservaciones.BorderWidthTop = 0;
                celEspacioObservaciones.HorizontalAlignment = Element.ALIGN_LEFT;
                celEspacioObservaciones.VerticalAlignment = Element.ALIGN_TOP;
                tableObservaciones.AddCell(celEspacioObservaciones);
                //----------------------------------------------------------------------------------------------
                PdfPTable tableResolucion = new PdfPTable(1);
                tableResolucion.WidthPercentage = 100;

                string strResolucion = DsInvoiceAR.Tables["InvcHead"].Rows[0]["InfoResolucionInvoice"].ToString() + "\n" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["InfoResolucion"].ToString() + "\n" +
                                       "DEBE CANCELAR NUESTRA ORDEN UNICAMENTE CON SELLO CRUZADO Y SELLO RESTRICTIVO A FAVOR DEL PRIMER BENEFICIARIO, NO ACEPTAMOS DEVOLUCIONES DE MERCANCIA, NI RECLAMO ALGUNO A MENOS QUE\n" +
                                       "SEA HECHO CON JUSTA CAUSA DENTRO DE LOS 20 SIGUIENTES AL DESPACHO. FACTURA NO CANCELADA A SU VENCIMIENTO CAUSARA INTERESES DE MORA DEL 3.5% MENSUAL. LA PRESENTA FACTURA DE VENTA SE\n" +
                                       "ASIMILA EN TODOS SUS EFECTOS A LA LETRA DE CAMBIO ART. 774 DEL CODIGO DE CÓMERCIO \"LA NO DEVOLUCIÓN DE ESTA FACTURA DE VENTA EN UN PLAZO DE DIEZ(10) DÍAS APARTIR DE LA FECHA DE SU RECIBO, SE\n" +
                                       "ENTENDERA COMO ACEPTADA (ART. 773 CÓDIGO DE COMERCIO)\"\n" +
                                       "ESTA FACTURA ES REAL Y ENTREGADA DERECO AL CLIENTE SOBRE LOS BIENES AQUI DESCRITOS. EN CONTRA PRESTACIÓN EL CLIENTE ACEPTA RESPONSABILIDAD DE LOS MISMOS DESDE EL MOMENTO EN QUE LA FACTURA\n" +
                                       "ES EMITIDA ASI COMO TAMBIEN RECONOCE EL VALOR AQUI DESCRITO COMO REAL Y MANDATORIO DE PAGO.";


                iTextSharp.text.pdf.PdfPCell _celResolucion = new iTextSharp.text.pdf.PdfPCell(new Phrase(strResolucion, fontCustom));
                _celResolucion.Colspan = 1;
                _celResolucion.Padding = 3;
                //_celResolucion.Border = 0;
                _celResolucion.HorizontalAlignment = Element.ALIGN_LEFT;
                _celResolucion.VerticalAlignment = Element.ALIGN_TOP;
                tableResolucion.AddCell(_celResolucion);
                //------------------------------------------------------------------------
                PdfPTable tableTrasnportador = new PdfPTable(4);

                float[] DimencionTransportador = new float[4];
                DimencionTransportador[0] = 1.0F;//
                DimencionTransportador[1] = 1.0F;//
                DimencionTransportador[2] = 1.3F;//
                DimencionTransportador[3] = 1.0F;//


                tableTrasnportador.WidthPercentage = 100;
                tableTrasnportador.SetWidths(DimencionTransportador);

                iTextSharp.text.pdf.PdfPCell celTransportador = new iTextSharp.text.pdf.PdfPCell(new Phrase("Transporter", fontTitleFactura));
                celTransportador.Colspan = 1;
                celTransportador.Padding = 3;
                celTransportador.BorderWidthBottom = 0;
                celTransportador.BackgroundColor = BaseColor.LIGHT_GRAY;
                celTransportador.HorizontalAlignment = Element.ALIGN_CENTER;
                celTransportador.VerticalAlignment = Element.ALIGN_TOP;
                tableTrasnportador.AddCell(celTransportador);

                iTextSharp.text.pdf.PdfPCell celConductor = new iTextSharp.text.pdf.PdfPCell(new Phrase("Driver's name", fontTitleFactura));
                celConductor.Colspan = 1;
                celConductor.Padding = 3;
                celConductor.BorderWidthBottom = 0;
                celConductor.BackgroundColor = BaseColor.LIGHT_GRAY;
                celConductor.HorizontalAlignment = Element.ALIGN_CENTER;
                celConductor.VerticalAlignment = Element.ALIGN_TOP;
                tableTrasnportador.AddCell(celConductor);

                iTextSharp.text.pdf.PdfPCell celCC = new iTextSharp.text.pdf.PdfPCell(new Phrase("ID", fontTitleFactura));
                celCC.Colspan = 1;
                celCC.Padding = 3;
                celCC.BorderWidthBottom = 0;
                celCC.BackgroundColor = BaseColor.LIGHT_GRAY;
                celCC.HorizontalAlignment = Element.ALIGN_CENTER;
                celCC.VerticalAlignment = Element.ALIGN_TOP;
                tableTrasnportador.AddCell(celCC);

                iTextSharp.text.pdf.PdfPCell celPlacaVehiculo = new iTextSharp.text.pdf.PdfPCell(new Phrase("Vehicule plate", fontTitleFactura));
                celPlacaVehiculo.Colspan = 1;
                celPlacaVehiculo.Padding = 3;
                celPlacaVehiculo.BorderWidthBottom = 0;
                celPlacaVehiculo.BackgroundColor = BaseColor.LIGHT_GRAY;
                celPlacaVehiculo.HorizontalAlignment = Element.ALIGN_CENTER;
                celPlacaVehiculo.VerticalAlignment = Element.ALIGN_TOP;
                tableTrasnportador.AddCell(celPlacaVehiculo);

                //---------------------CAMPOS DE TRANSPORTADOR----------------------------
                iTextSharp.text.pdf.PdfPCell _celTransportador = new iTextSharp.text.pdf.PdfPCell(new Phrase(DsInvoiceAR.Tables["InvcHead"].Rows[0]["Transportador"].ToString(), fontTitleFactura));
                _celTransportador.Colspan = 1;
                _celTransportador.Padding = 3;
                _celTransportador.BorderWidthTop = 0;
                _celTransportador.HorizontalAlignment = Element.ALIGN_CENTER;
                _celTransportador.VerticalAlignment = Element.ALIGN_TOP;
                tableTrasnportador.AddCell(_celTransportador);

                iTextSharp.text.pdf.PdfPCell _celConductor = new iTextSharp.text.pdf.PdfPCell(new Phrase(DsInvoiceAR.Tables["InvcHead"].Rows[0]["Conductor"].ToString(), fontTitleFactura));
                _celConductor.Colspan = 1;
                _celConductor.Padding = 3;
                _celConductor.BorderWidthTop = 0;
                _celConductor.HorizontalAlignment = Element.ALIGN_CENTER;
                _celConductor.VerticalAlignment = Element.ALIGN_TOP;
                tableTrasnportador.AddCell(_celConductor);

                iTextSharp.text.pdf.PdfPCell _celCC = new iTextSharp.text.pdf.PdfPCell(new Phrase(DsInvoiceAR.Tables["InvcHead"].Rows[0]["FFID"].ToString(), fontTitleFactura));
                _celCC.Colspan = 1;
                _celCC.Padding = 3;
                _celCC.BorderWidthTop = 0;
                _celCC.HorizontalAlignment = Element.ALIGN_CENTER;
                _celCC.VerticalAlignment = Element.ALIGN_TOP;
                tableTrasnportador.AddCell(_celCC);

                iTextSharp.text.pdf.PdfPCell _celPlacaVehiculo = new iTextSharp.text.pdf.PdfPCell(new Phrase(DsInvoiceAR.Tables["InvcHead"].Rows[0]["Placa"].ToString(), fontTitleFactura));
                _celPlacaVehiculo.Colspan = 1;
                _celPlacaVehiculo.Padding = 3;
                _celPlacaVehiculo.BorderWidthTop = 0;
                _celPlacaVehiculo.HorizontalAlignment = Element.ALIGN_CENTER;
                _celPlacaVehiculo.VerticalAlignment = Element.ALIGN_TOP;
                tableTrasnportador.AddCell(_celPlacaVehiculo);

                //------------------------------------------------------------------------
                PdfPTable tableFirmas = new PdfPTable(5);

                float[] DimencionFirmas = new float[5];
                DimencionFirmas[0] = 1.0F;//
                DimencionFirmas[1] = 0.02F;//
                DimencionFirmas[2] = 1.0F;//
                DimencionFirmas[3] = 0.02F;//
                DimencionFirmas[4] = 1.4F;//

                tableFirmas.WidthPercentage = 100;
                tableFirmas.SetWidths(DimencionFirmas);

                PdfPTable tableDespacahdoPor = new PdfPTable(1);
                //tableDespacahdoPor.WidthPercentage = 100;

                iTextSharp.text.pdf.PdfPCell celDespachadoPor = new iTextSharp.text.pdf.PdfPCell(new Phrase("\n\n\n\n\n" +
                    "\n______________________________________\n\nDispatch by", fontTitleFactura));
                celDespachadoPor.Colspan = 1;
                celDespachadoPor.Padding = 3;
                celDespachadoPor.HorizontalAlignment = Element.ALIGN_CENTER;
                celDespachadoPor.VerticalAlignment = Element.ALIGN_TOP;
                tableDespacahdoPor.AddCell(celDespachadoPor);

                iTextSharp.text.pdf.PdfPCell celEspacioDespachadoPor = new iTextSharp.text.pdf.PdfPCell(new Phrase("\n\n\n", fontTitleFactura));
                celEspacioDespachadoPor.Colspan = 1;
                celEspacioDespachadoPor.Padding = 3;
                celEspacioDespachadoPor.Border = 0;
                celEspacioDespachadoPor.HorizontalAlignment = Element.ALIGN_CENTER;
                celEspacioDespachadoPor.VerticalAlignment = Element.ALIGN_TOP;
                tableDespacahdoPor.AddCell(celEspacioDespachadoPor);

                iTextSharp.text.pdf.PdfPCell _celDespachadoPor = new iTextSharp.text.pdf.PdfPCell(tableDespacahdoPor);
                _celDespachadoPor.Border = 0;
                tableFirmas.AddCell(_celDespachadoPor);
                //------------------------------------------------------------------------------------------
                tableFirmas.AddCell(celEspacio2);
                //------------------------------------------------------------------------------------------
                PdfPTable tableFirmaConductor = new PdfPTable(1);
                tableFirmaConductor.WidthPercentage = 100;
                iTextSharp.text.pdf.PdfPCell celFirmaConductor = new iTextSharp.text.pdf.PdfPCell(new Phrase("\n\n\n\n\n\n" +
                    "______________________________________" +
                    "\n\nDriver's signature", fontTitleFactura));
                celFirmaConductor.Colspan = 1;
                celFirmaConductor.Padding = 3;
                //celFirmaConductor.Border = 0;
                celFirmaConductor.HorizontalAlignment = Element.ALIGN_CENTER;
                celFirmaConductor.VerticalAlignment = Element.ALIGN_TOP;
                tableFirmaConductor.AddCell(celFirmaConductor);

                iTextSharp.text.pdf.PdfPCell celEspacioFirmaConductor = new iTextSharp.text.pdf.PdfPCell(new Phrase("\n\n\n", fontTitleFactura));
                celEspacioFirmaConductor.Colspan = 1;
                celEspacioFirmaConductor.Padding = 3;
                celEspacioFirmaConductor.Border = 0;
                celEspacioFirmaConductor.HorizontalAlignment = Element.ALIGN_CENTER;
                celEspacioFirmaConductor.VerticalAlignment = Element.ALIGN_TOP;
                tableFirmaConductor.AddCell(celEspacioFirmaConductor);

                iTextSharp.text.pdf.PdfPCell _celFirmaConductor = new iTextSharp.text.pdf.PdfPCell(tableFirmaConductor);
                _celFirmaConductor.Border = 0;
                tableFirmas.AddCell(_celFirmaConductor);
                //-------------------------------------------------------------------------------------
                tableFirmas.AddCell(celEspacio2);
                //-------------------------------------------------------------------------------------

                PdfPTable tableSelloCliente = new PdfPTable(1);
                //tableSelloCliente.WidthPercentage = 100;

                iTextSharp.text.pdf.PdfPCell cel1SelloCliente = new iTextSharp.text.pdf.PdfPCell(new Phrase("This document is not endorsable", fontTitleFactura));
                cel1SelloCliente.Colspan = 1;
                cel1SelloCliente.Padding = 5;
                cel1SelloCliente.Border = 1;
                cel1SelloCliente.BorderWidthBottom = 1;
                cel1SelloCliente.HorizontalAlignment = Element.ALIGN_LEFT;
                cel1SelloCliente.VerticalAlignment = Element.ALIGN_TOP;
                tableSelloCliente.AddCell(cel1SelloCliente);

                iTextSharp.text.pdf.PdfPCell cel2SelloFechaRecibido = new iTextSharp.text.pdf.PdfPCell(new Phrase("Date:", fontTitleFactura));

                cel2SelloFechaRecibido.Colspan = 1;
                cel2SelloFechaRecibido.Padding = 3;
                cel2SelloFechaRecibido.Border = 0;
                cel2SelloFechaRecibido.HorizontalAlignment = Element.ALIGN_LEFT;
                cel2SelloFechaRecibido.VerticalAlignment = Element.ALIGN_TOP;
                tableSelloCliente.AddCell(cel2SelloFechaRecibido);

                iTextSharp.text.pdf.PdfPCell cel2SelloNombre = new iTextSharp.text.pdf.PdfPCell(new Phrase("Name:", fontTitleFactura));

                cel2SelloNombre.Colspan = 1;
                cel2SelloNombre.Padding = 3;
                cel2SelloNombre.Border = 0;
                cel2SelloNombre.HorizontalAlignment = Element.ALIGN_LEFT;
                cel2SelloNombre.VerticalAlignment = Element.ALIGN_TOP;
                tableSelloCliente.AddCell(cel2SelloNombre);

                iTextSharp.text.pdf.PdfPCell cel2SelloId = new iTextSharp.text.pdf.PdfPCell(new Phrase("ID: ", fontTitleFactura));

                cel2SelloId.Colspan = 1;
                cel2SelloId.Padding = 3;
                cel2SelloId.Border = 0;
                cel2SelloId.HorizontalAlignment = Element.ALIGN_LEFT;
                cel2SelloId.VerticalAlignment = Element.ALIGN_TOP;
                tableSelloCliente.AddCell(cel2SelloId);

                iTextSharp.text.pdf.PdfPCell cel2SelloSignature = new iTextSharp.text.pdf.PdfPCell(new Phrase("Signature:\n", fontTitleFactura));

                cel2SelloSignature.Colspan = 1;
                cel2SelloSignature.Padding = 3;
                cel2SelloSignature.Border = 0;
                cel2SelloSignature.HorizontalAlignment = Element.ALIGN_LEFT;
                cel2SelloSignature.VerticalAlignment = Element.ALIGN_TOP;
                tableSelloCliente.AddCell(cel2SelloSignature);

                iTextSharp.text.pdf.PdfPCell cel3SelloCliente = new iTextSharp.text.pdf.PdfPCell(new Phrase("______________________________________________________\n\n" +
                    "I received according signature and seal", fontTitleFactura));

                cel3SelloCliente.Colspan = 1;
                cel3SelloCliente.Padding = 3;
                cel3SelloCliente.Border = 0;
                cel3SelloCliente.HorizontalAlignment = Element.ALIGN_CENTER;
                cel3SelloCliente.VerticalAlignment = Element.ALIGN_TOP;
                tableSelloCliente.AddCell(cel3SelloCliente);

                iTextSharp.text.pdf.PdfPCell _celSelloCliente = new iTextSharp.text.pdf.PdfPCell(tableSelloCliente);
                tableFirmas.AddCell(_celSelloCliente);
                #endregion


                document.Add(divLogo);
                document.Add(divAcercade);
                document.Add(divLogo2);
                document.Add(divQR);
                document.Add(divNumeroFactura);
                document.Add(divEspacio);
                document.Add(tableFacturar);
                document.Add(divEspacio);
                document.Add(tableDetalles2);
                document.Add(divEspacio2);
                document.Add(divEspacio2);
                document.Add(tableDetalles);
                document.Add(divEspacio2);
                document.Add(tableTituloUnidades);
                document.Add(tableUnidades);
                document.Add(divEspacio2);
                document.Add(divEspacio2);
                document.Add(tableTotalUnidades);
                document.Add(divEspacio2);
                document.Add(divEspacio2);
                document.Add(tableValorLetras);
                document.Add(divEspacio2);
                document.Add(divEspacio2);
                document.Add(tableObservaciones);
                document.Add(divEspacio2);
                document.Add(divEspacio2);
                document.Add(tableResolucion);
                document.Add(divEspacio2);
                document.Add(tableTrasnportador);
                document.Add(divEspacio2);
                document.Add(divEspacio2);
                document.Add(tableFirmas);

                /*PIE DE PAGINA*/
                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();

                AddPageNumber($"{Ruta}{NomArchivo}", 555f, 775f, fontTitle);
                //Helpers.Simex.AddPageNumberPagToSimexExp($"{Ruta}{NomArchivo}", 555f, 775f, fontTitle);

                RutaPdf = NomArchivo;

                return true;
            }
            catch (Exception ex)
            {
                strError += "Error en Factura internacional ALDOR: " + ex.Message;
                return false;
            }
        }

        private bool AddUnidadesALDOREng1(DataRow dataLine, ref PdfPTable pdfPTable, iTextSharp.text.Font font, DataSet DsInvoiceAR)
        {
            try
            {
                iTextSharp.text.pdf.PdfPCell celCodigo = new iTextSharp.text.pdf.PdfPCell(new Phrase(dataLine["PartNum"].ToString(), font));
                celCodigo.Colspan = 1;
                celCodigo.Padding = 3;
                //celCodigo.Border = 0;
                celCodigo.BorderWidthBottom = 0;
                celCodigo.BorderWidthTop = 0;
                celCodigo.BorderWidthRight = 0;
                celCodigo.BorderColorBottom = BaseColor.WHITE;
                celCodigo.HorizontalAlignment = Element.ALIGN_LEFT;
                celCodigo.VerticalAlignment = Element.ALIGN_TOP;
                pdfPTable.AddCell(celCodigo);

                iTextSharp.text.pdf.PdfPCell celDescripcion = new iTextSharp.text.pdf.PdfPCell(new Phrase(dataLine["LineDesc"].ToString(), font));
                celDescripcion.Padding = 3;
                celDescripcion.Colspan = 1;
                celDescripcion.BorderWidthBottom = 0;
                celDescripcion.BorderWidthTop = 0;
                celDescripcion.BorderWidthRight = 0;
                celDescripcion.BorderColorBottom = BaseColor.WHITE;
                celDescripcion.HorizontalAlignment = Element.ALIGN_LEFT;
                celDescripcion.VerticalAlignment = Element.ALIGN_TOP;
                pdfPTable.AddCell(celDescripcion);


                string SellingShipQty = string.Empty;
                if (DsInvoiceAR.Tables["InvcHead"].Rows[0]["CurrencyCode"].ToString().ToUpper() == "COP")
                    SellingShipQty = string.Format("{0:N2}", Convert.ToDecimal(dataLine["SellingShipQty"].ToString()));
                else
                    SellingShipQty = string.Format("{0:N3}", Convert.ToDecimal(dataLine["SellingShipQty"].ToString()));


                iTextSharp.text.pdf.PdfPCell celCantidad = new iTextSharp.text.pdf.PdfPCell(new Phrase(SellingShipQty, font));
                celCantidad.Colspan = 1;
                celCantidad.Padding = 3;
                //celCantidad.Border = 0;
                celCantidad.BorderWidthBottom = 0;
                celCantidad.BorderWidthTop = 0;
                celCantidad.BorderWidthRight = 0;
                celCantidad.BorderColorBottom = BaseColor.WHITE;
                celCantidad.HorizontalAlignment = Element.ALIGN_RIGHT;
                celCantidad.VerticalAlignment = Element.ALIGN_TOP;
                pdfPTable.AddCell(celCantidad);

                iTextSharp.text.pdf.PdfPCell celUM = new iTextSharp.text.pdf.PdfPCell(new Phrase(dataLine["SalesUM"].ToString(), font));
                celUM.Colspan = 1;
                celUM.Padding = 3;
                celUM.BorderWidthBottom = 0;
                celUM.BorderWidthTop = 0;
                celUM.BorderWidthRight = 0;
                celUM.BorderColorBottom = BaseColor.WHITE;
                celUM.HorizontalAlignment = Element.ALIGN_CENTER;
                celUM.VerticalAlignment = Element.ALIGN_TOP;
                celUM.BorderWidthRight = 0;
                pdfPTable.AddCell(celUM);

                string DocUnitPrice = string.Empty;
                if (DsInvoiceAR.Tables["InvcHead"].Rows[0]["CurrencyCode"].ToString().ToUpper() == "COP")
                    DocUnitPrice = string.Format("{0:C2}", Convert.ToDecimal(dataLine["DocUnitPrice"].ToString()));
                else
                    DocUnitPrice = string.Format("{0:C2}", Convert.ToDecimal(dataLine["DocUnitPrice"].ToString()));


                iTextSharp.text.pdf.PdfPCell celValorUnitario = new iTextSharp.text.pdf.PdfPCell(new Phrase(DocUnitPrice, font));
                celValorUnitario.Colspan = 1;
                celValorUnitario.Padding = 3;
                //celValorUnitario.Border = 0;
                celValorUnitario.BorderWidthBottom = 0;
                celValorUnitario.BorderWidthTop = 0;
                celValorUnitario.BorderWidthRight = 0;
                celValorUnitario.BorderColorBottom = BaseColor.WHITE;
                celValorUnitario.HorizontalAlignment = Element.ALIGN_RIGHT;
                celValorUnitario.VerticalAlignment = Element.ALIGN_TOP;
                pdfPTable.AddCell(celValorUnitario);

                iTextSharp.text.pdf.PdfPTable tableDescuentos = new PdfPTable(2);

                //string DocExtPrice = string.Empty;
                //if (dataSetARInvoice.Tables["InvcHead"].Rows[0]["CurrencyCode"].ToString().ToUpper() == "COP")
                //    DocExtPrice = string.Format("{0:C}", Convert.ToDecimal(dataLine["DocExtPrice"].ToString()));
                //else
                //    DocExtPrice = string.Format("{0:C4}", Convert.ToDecimal(dataLine["DocExtPrice"].ToString()));

                iTextSharp.text.pdf.PdfPCell celDesc1 = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:N2}", Convert.ToDecimal(DsInvoiceAR.Tables["InvcDtl"].Rows[0]["Descuento1"].ToString())), font));
                celDesc1.Colspan = 1;
                //celDesc1.Border = 0;
                celDesc1.BorderWidthBottom = 0;
                celDesc1.BorderWidthTop = 0;
                celDesc1.HorizontalAlignment = Element.ALIGN_RIGHT;
                celDesc1.VerticalAlignment = Element.ALIGN_TOP;
                tableDescuentos.AddCell(celDesc1);

                iTextSharp.text.pdf.PdfPCell celDesc2 = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:N2}", Convert.ToDecimal(DsInvoiceAR.Tables["InvcDtl"].Rows[0]["Descuento2"].ToString())), font));
                celDesc2.Colspan = 1;
                celDesc2.Border = 0;
                celDesc2.BorderWidthBottom = 0;
                celDesc2.BorderWidthTop = 0;
                celDesc2.HorizontalAlignment = Element.ALIGN_RIGHT;
                celDesc2.VerticalAlignment = Element.ALIGN_TOP;
                tableDescuentos.AddCell(celDesc2);

                iTextSharp.text.pdf.PdfPCell _celDescuentos12 = new iTextSharp.text.pdf.PdfPCell(tableDescuentos);
                //_celDescuentos12.BorderWidthBottom = 0;
                //_celDescuentos12.BorderWidthTop = 0;
                _celDescuentos12.Border = 0;
                pdfPTable.AddCell(_celDescuentos12);


                string DocExtPrice = string.Empty;
                if (DsInvoiceAR.Tables["InvcHead"].Rows[0]["CurrencyCode"].ToString().ToUpper() == "COP")
                    DocExtPrice = string.Format("{0:C}", Convert.ToDecimal(dataLine["DocExtPrice"].ToString()));
                else
                    DocExtPrice = string.Format("{0:C4}", Convert.ToDecimal(dataLine["DocExtPrice"].ToString()));


                iTextSharp.text.pdf.PdfPCell celValorTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase(DocExtPrice, font));
                celValorTotal.Colspan = 1;
                celValorTotal.Padding = 3;
                //celValorTotal.Border = 0;
                celValorTotal.BorderWidthBottom = 0;
                celValorTotal.BorderWidthTop = 0;
                //celValorTotal.BorderWidthRight = 0;
                celValorTotal.BorderColorBottom = BaseColor.WHITE;
                celValorTotal.HorizontalAlignment = Element.ALIGN_RIGHT;
                celValorTotal.VerticalAlignment = Element.ALIGN_TOP;
                pdfPTable.AddCell(celValorTotal);


                //decimal PercentIva = Convert.ToDecimal(GetPercentIdImpDIAN(dataLine["InvoiceNum"].ToString(), dataLine["InvoiceLine"].ToString(), DsInvoiceAR.Tables["InvcTax"])); //GetValImpuestoByID("01", DsInvoiceAR);
                ////ImpConsBolsa = GetValImpuestoByID("02", DsInvoiceAR);                
                //iTextSharp.text.pdf.PdfPCell celIva = new iTextSharp.text.pdf.PdfPCell(new Phrase(new Phrase(string.Format("{0:C2}", PercentIva), font))); //new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:C2}", Iva), font));
                ////iTextSharp.text.pdf.PdfPCell celIva = new iTextSharp.text.pdf.PdfPCell(new Phrase("xxx", font));
                //celIva.Colspan = 1;
                //celIva.Padding = 3;
                //celIva.BorderWidthBottom = 0;
                //celIva.BorderWidthTop = 0;
                //celIva.HorizontalAlignment = Element.ALIGN_LEFT;
                //celIva.VerticalAlignment = Element.ALIGN_TOP;
                //pdfPTable.AddCell(celIva);

                return true;
            }
            catch (Exception ex)
            {
                strError += "Error en AddUnidadesALDOR: " + ex.Message;
                return false;
            }
        }

        public string ToWords(decimal value)
        {
            string decimals = "";
            string input = Math.Round(value, 2).ToString();

            if (input.Contains("."))
            {
                decimals = input.Substring(input.IndexOf(".") + 1);
                // remove decimal part from input
                input = input.Remove(input.IndexOf("."));
            }

            // Convert input into words. save it into strWords
            string strWords = GetWords(input) + " Dollars";


            if (decimals.Length > 0)
            {
                // if there is any decimal part convert it to words and add it to strWords.
                strWords += " and " + GetWords(decimals) + " Cents";
            }

            return strWords;
        }

        private string GetWords(string input)
        {
            // these are seperators for each 3 digit in numbers. you can add more if you want convert beigger numbers.
            string[] seperators = { "", " Thousand ", " Million ", " Billion " };

            // Counter is indexer for seperators. each 3 digit converted this will count.
            int i = 0;

            string strWords = "";

            while (input.Length > 0)
            {
                // get the 3 last numbers from input and store it. if there is not 3 numbers just use take it.
                string _3digits = input.Length < 3 ? input : input.Substring(input.Length - 3);
                // remove the 3 last digits from input. if there is not 3 numbers just remove it.
                input = input.Length < 3 ? "" : input.Remove(input.Length - 3);

                int no = int.Parse(_3digits);
                // Convert 3 digit number into words.
                _3digits = GetWord(no);

                // apply the seperator.
                _3digits += seperators[i];
                // since we are getting numbers from right to left then we must append resault to strWords like this.
                strWords = _3digits + strWords;

                // 3 digits converted. count and go for next 3 digits
                i++;
            }
            return strWords;
        }

        private string GetWord(int no)
        {
            string[] Ones =
            {
            "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven",
            "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Ninteen"
        };

            string[] Tens = { "Ten", "Twenty", "Thirty", "Fourty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninty" };

            string word = "";

            if (no > 99 && no < 1000)
            {
                int i = no / 100;
                word = word + Ones[i - 1] + " Hundred ";
                no = no % 100;
            }

            if (no > 19 && no < 100)
            {
                int i = no / 10;
                word = word + Tens[i - 1] + " ";
                no = no % 10;
            }

            if (no > 0 && no < 20)
            {
                word = word + Ones[no - 1];
            }

            return word;
        }
        
        #endregion

    }
}
