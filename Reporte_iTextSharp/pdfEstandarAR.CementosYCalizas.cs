﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
//Referenciar y usar.
using System.Data;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Web;
using System.Windows.Forms;

namespace RulesServicesDIAN2.Adquiriente

{
    public partial class pdfEstandarAR
    {
        #region Lineas de Detalle
        private bool AddUnidadesCementos(DataRow dataLine, ref PdfPTable pdfPTable, iTextSharp.text.Font font, DataSet dataSet)
        {
            try
            {
                iTextSharp.text.pdf.PdfPCell celCodigo = new iTextSharp.text.pdf.PdfPCell(new Phrase(dataLine["PartNum"].ToString(), font));
                celCodigo.Colspan = 1;
                celCodigo.Padding = 3;
                //celCodigo.Border = 0;
                celCodigo.BorderWidthBottom = 0;
                celCodigo.BorderWidthTop = 0;
                celCodigo.BorderWidthRight = 0;
                celCodigo.BorderColorBottom = BaseColor.WHITE;
                celCodigo.HorizontalAlignment = Element.ALIGN_LEFT;
                celCodigo.VerticalAlignment = Element.ALIGN_TOP;
                pdfPTable.AddCell(celCodigo);

                iTextSharp.text.pdf.PdfPCell celDescripcion = new iTextSharp.text.pdf.PdfPCell(new Phrase(dataLine["LineDesc"].ToString(), font));
                celDescripcion.Padding = 3;
                celDescripcion.Colspan = 1;
                celDescripcion.BorderWidthBottom = 0;
                celDescripcion.BorderWidthTop = 0;
                celDescripcion.BorderWidthRight = 0;
                celDescripcion.BorderColorBottom = BaseColor.WHITE;
                celDescripcion.HorizontalAlignment = Element.ALIGN_LEFT;
                celDescripcion.VerticalAlignment = Element.ALIGN_TOP;
                pdfPTable.AddCell(celDescripcion);

                iTextSharp.text.pdf.PdfPCell celCantidad = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:N2}",
                    decimal.Parse(dataLine["SellingShipQty"].ToString())), font));
                celCantidad.Colspan = 1;
                celCantidad.Padding = 3;
                //celCantidad.Border = 0;
                celCantidad.BorderWidthBottom = 0;
                celCantidad.BorderWidthTop = 0;
                celCantidad.BorderWidthRight = 0;
                celCantidad.BorderColorBottom = BaseColor.WHITE;
                celCantidad.HorizontalAlignment = Element.ALIGN_RIGHT;
                celCantidad.VerticalAlignment = Element.ALIGN_TOP;
                pdfPTable.AddCell(celCantidad);

                iTextSharp.text.pdf.PdfPCell celUM = new iTextSharp.text.pdf.PdfPCell(new Phrase(dataLine["SalesUM"].ToString(), font));
                celUM.Colspan = 1;
                celUM.Padding = 3;
                celUM.BorderWidthBottom = 0;
                celUM.BorderWidthTop = 0;
                celUM.BorderWidthRight = 0;
                celUM.BorderColorBottom = BaseColor.WHITE;
                celUM.HorizontalAlignment = Element.ALIGN_CENTER;
                celUM.VerticalAlignment = Element.ALIGN_TOP;
                celUM.BorderWidthRight = 0;
                pdfPTable.AddCell(celUM);

                iTextSharp.text.pdf.PdfPCell celValorUnitario = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:C2}", Convert.ToDecimal(dataLine["UnitPrice"].ToString())),
                    font));
                celValorUnitario.Colspan = 1;
                celValorUnitario.Padding = 3;
                //celValorUnitario.Border = 0;
                celValorUnitario.BorderWidthBottom = 0;
                celValorUnitario.BorderWidthTop = 0;
                celValorUnitario.BorderWidthRight = 0;
                celValorUnitario.BorderColorBottom = BaseColor.WHITE;
                celValorUnitario.HorizontalAlignment = Element.ALIGN_RIGHT;
                celValorUnitario.VerticalAlignment = Element.ALIGN_TOP;
                pdfPTable.AddCell(celValorUnitario);

                iTextSharp.text.pdf.PdfPTable tableDescuentos = new PdfPTable(2);

                iTextSharp.text.pdf.PdfPCell celDesc1 = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:N2}", Convert.ToDecimal(dataLine["DiscountPercent"].ToString())), font));
                celDesc1.Colspan = 1;
                //celDesc1.Border = 0;
                celDesc1.BorderWidthBottom = 0;
                celDesc1.BorderWidthTop = 0;
                celDesc1.HorizontalAlignment = Element.ALIGN_CENTER;
                celDesc1.VerticalAlignment = Element.ALIGN_TOP;
                tableDescuentos.AddCell(celDesc1);

                iTextSharp.text.pdf.PdfPCell celDesc2 = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:N2}", Convert.ToDecimal(dataLine["Number01"].ToString())), font));
                celDesc2.Colspan = 1;
                celDesc2.Border = 0;
                celDesc2.BorderWidthBottom = 0;
                celDesc2.BorderWidthTop = 0;
                celDesc2.HorizontalAlignment = Element.ALIGN_CENTER;
                celDesc2.VerticalAlignment = Element.ALIGN_TOP;
                tableDescuentos.AddCell(celDesc2);

                iTextSharp.text.pdf.PdfPCell _celDescuentos12 = new iTextSharp.text.pdf.PdfPCell(tableDescuentos);
                //_celDescuentos12.BorderWidthBottom = 0;
                //_celDescuentos12.BorderWidthTop = 0;
                _celDescuentos12.Border = 0;
                pdfPTable.AddCell(_celDescuentos12);

                iTextSharp.text.pdf.PdfPCell celValorTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:C2}", decimal.Parse(dataLine["DocExtPrice"].ToString())), font));
                celValorTotal.Colspan = 1;
                celValorTotal.Padding = 3;
                //celValorTotal.Border = 0;
                celValorTotal.BorderWidthBottom = 0;
                celValorTotal.BorderWidthTop = 0;
                celValorTotal.BorderWidthRight = 0;
                celValorTotal.BorderColorBottom = BaseColor.WHITE;
                celValorTotal.HorizontalAlignment = Element.ALIGN_RIGHT;
                celValorTotal.VerticalAlignment = Element.ALIGN_TOP;
                pdfPTable.AddCell(celValorTotal);

                iTextSharp.text.pdf.PdfPCell celIva = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:C2}", decimal.Parse(dataLine["DocExtPrice"].ToString())), font));
                celIva.Colspan = 1;
                celIva.Padding = 3;
                celIva.BorderWidthBottom = 0;
                celIva.BorderWidthTop = 0;
                celIva.HorizontalAlignment = Element.ALIGN_CENTER;
                celIva.VerticalAlignment = Element.ALIGN_TOP;
                pdfPTable.AddCell(celIva);

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return false;
            }
        }

        private bool AddUnidadesNotaCreditoCementos(DataRow dataLine, ref PdfPTable table, iTextSharp.text.Font fontTitleFactura, DataSet dataSet)
        {
            try
            {
                //CODIGO
                strError += "Add InvoiceLine";
                iTextSharp.text.pdf.PdfPCell celValL = new iTextSharp.text.pdf.PdfPCell(new Phrase((string)dataLine["InvoiceLine"], fontTitleFactura));
                celValL.Colspan = 1;
                celValL.Padding = 2;
                celValL.Border = PdfPCell.LEFT_BORDER | PdfPCell.RIGHT_BORDER;
                celValL.HorizontalAlignment = Element.ALIGN_CENTER;
                celValL.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValL);
                strError += "InvoiceLine OK";

                //DESCRIPCIÓN ARTÍCULO
                strError += "Add PartNum";
                iTextSharp.text.pdf.PdfPCell celValCodigo = new iTextSharp.text.pdf.PdfPCell(new Phrase((string)dataLine["PartNumPartDescription"], fontTitleFactura));
                celValCodigo.Colspan = 1;
                celValCodigo.Padding = 2;
                celValCodigo.Border = PdfPCell.RIGHT_BORDER;
                //celValCodigo.BorderWidthLeft = 1;
                //celDescripcion.BorderColorBottom = BaseColor.WHITE;
                celValCodigo.HorizontalAlignment = Element.ALIGN_LEFT;
                celValCodigo.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValCodigo);
                strError += "Add PartNum OK";

                //CANTIDAD
                strError += "Add LineDesc";
                iTextSharp.text.pdf.PdfPCell celValCantidad = new iTextSharp.text.pdf.PdfPCell(new Phrase((string)dataLine["PartNum"], fontTitleFactura));
                celValCantidad.Colspan = 1;
                celValCantidad.Padding = 2;
                celValCantidad.Border = PdfPCell.RIGHT_BORDER;
                //celValDesc.BorderWidthLeft = 1;
                celValCantidad.HorizontalAlignment = Element.ALIGN_RIGHT;
                celValCantidad.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValCantidad);
                strError += "Add LineDesc OK";


                //VR. UNITARIO
                strError += "Add SellingShipQty";
                iTextSharp.text.pdf.PdfPCell celValDescripcion = new iTextSharp.text.pdf.PdfPCell(new Phrase((string)dataLine["SalesUM"], fontTitleFactura));
                celValDescripcion.Colspan = 1;
                celValDescripcion.Padding = 2;
                celValDescripcion.Border = PdfPCell.RIGHT_BORDER;
                //celValCantidad.BorderWidthLeft = 1;
                celValDescripcion.HorizontalAlignment = Element.ALIGN_RIGHT;
                celValDescripcion.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValDescripcion);
                strError += "Add SellingShipQty OK";
                return true;
            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.Message;
                return false;
            }
        }



        #endregion

        #region Factura nacional Cementos y calizas de la paz

        public bool FacturaCementosyCalizas(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {
            NomArchivo = string.Empty;
            string Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            string RutaImg = $@"{AppDomain.CurrentDomain.BaseDirectory}\logo_cementos.png";
            string Rutacertificado = $@"{AppDomain.CurrentDomain.BaseDirectory}\logo_iconteciqnet_Cemento_Vallenato.png";
            NomArchivo = NombreInvoice + ".pdf";

            //strError += "Lego";
            //NomArchivo = string.Empty;
            //string Ruta = HttpContext.Current.Server.MapPath("~/PDF/" + NIT + "/");
            //string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            //string RutaImg = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";
            //NomArchivo = NombreInvoice + ".pdf";
            //strError += "inicia Documento pdf\n";
            try
            {
                #region Head
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.
                Document document = new Document(iTextSharp.text.PageSize.LETTER);
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;

                // step 3: we open the document     
                document.Open();

                PdfDiv divEspacio = new PdfDiv();
                divEspacio.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio.Height = 10;
                divEspacio.Width = 130;

                PdfDiv divEspacio2 = new PdfDiv();
                divEspacio2.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio2.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio2.Height = 2;
                divEspacio2.Width = 130;

                //FUENTES----------------------------------------------------------------------------------------------------------
                iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle2 = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustom = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);
                //FUENTES---------------------------------------------------------------------------------------------------------
                
                #endregion

                #region ENCABEZADO

                //agregamos informacion de empresa y tipo de factura

                /// agregamos logo

                System.Drawing.Image Logo;
                System.Drawing.Image LogoBanner = null;
                var request = WebRequest.Create(RutaImg);
                using (var response = request.GetResponse())
                using (var stream = response.GetResponseStream())
                {
                    Logo = Bitmap.FromStream(stream);
                }
                var requestBanner = WebRequest.Create(RutaImg);
                using (var responseBanner = requestBanner.GetResponse())
                using (var streamBanner = responseBanner.GetResponseStream())
                {
                    LogoBanner = Bitmap.FromStream(streamBanner);
                }

                System.Drawing.Image Logo2;
                System.Drawing.Image LogoBanner2 = null;
                var request2 = WebRequest.Create(Rutacertificado);
                using (var response2 = request2.GetResponse())
                using (var stream2 = response2.GetResponseStream())
                {
                    Logo2 = Bitmap.FromStream(stream2);
                }
                var requestBanner2 = WebRequest.Create(Rutacertificado);
                using (var responseBanner2 = requestBanner2.GetResponse())
                using (var streamBanner2 = responseBanner2.GetResponseStream())
                {
                    LogoBanner2 = Bitmap.FromStream(streamBanner2);
                }


                //Logos--------------------------------
                iTextSharp.text.Image LogoPdf3 = iTextSharp.text.Image.GetInstance(LogoBanner2, BaseColor.WHITE);
                LogoPdf3.ScaleAbsolute(90f, 90f);
                LogoPdf3.Border = 0;


                iTextSharp.text.Image LogoPdf2 = iTextSharp.text.Image.GetInstance(LogoBanner, BaseColor.WHITE);
                LogoPdf2.ScaleAbsolute(80f, 80f);
                LogoPdf2.Border = 0;

                iTextSharp.text.Image QRPdf = iTextSharp.text.Image.GetInstance(QRInvoice, BaseColor.WHITE);
                QRPdf.ScaleAbsolute(60f, 60f);
                QRPdf.Border = 0;

                //----------------------------------------------------------------------------------------------------
                iTextSharp.text.Font fontTitleFactura = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);

                PdfPTable tableFactura = new PdfPTable(3);

                //Dimenciones.
                float[] DimencionFactura = new float[3];
                DimencionFactura[0] = 1.5F;//
                DimencionFactura[1] = 4.0F;//
                DimencionFactura[2] = 0.5F;//

                tableFactura.WidthPercentage = 100;
                tableFactura.SetWidths(DimencionFactura);

                iTextSharp.text.pdf.PdfPCell celTittle = new iTextSharp.text.pdf.PdfPCell(new Phrase("FACTURA DE VENTA\n\n", fontTitleFactura));
                celTittle.Colspan = 3;
                celTittle.Padding = 0;
                celTittle.HorizontalAlignment = Element.ALIGN_CENTER;
                celTittle.VerticalAlignment = Element.ALIGN_TOP;
                celTittle.Border = 0;
                celTittle.BorderWidthTop = 1;
                celTittle.BorderWidthLeft = 1;
                celTittle.BorderWidthRight = 1;
                celTittle.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableFactura.AddCell(celTittle);

                iTextSharp.text.pdf.PdfPCell celNo = new iTextSharp.text.pdf.PdfPCell(new Phrase("No. ", fontTitleFactura));
                celNo.Colspan = 1;
                celNo.Padding = 5;
                celNo.HorizontalAlignment = Element.ALIGN_CENTER;
                celNo.VerticalAlignment = Element.ALIGN_TOP;
                celNo.Border = 0;
                celNo.BorderWidthLeft = 1;
                celNo.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableFactura.AddCell(celNo);

                string NumLegalFactura = string.Empty;
                NumLegalFactura = DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"].ToString();

                iTextSharp.text.pdf.PdfPCell celNoFactura = new iTextSharp.text.pdf.PdfPCell(new Phrase(NumLegalFactura, fontTitleFactura));
                celNoFactura.Colspan = 1;
                celNoFactura.Padding = 5;
                celNoFactura.HorizontalAlignment = Element.ALIGN_LEFT;
                celNoFactura.VerticalAlignment = Element.ALIGN_TOP;
                //celNoFactura.Border = 1;
                celNoFactura.BackgroundColor = BaseColor.WHITE;
                tableFactura.AddCell(celNoFactura);


                iTextSharp.text.pdf.PdfPCell celEspacioNoFactura = new iTextSharp.text.pdf.PdfPCell(new Phrase(" ", fontTitleFactura));
                celEspacioNoFactura.Colspan = 1;
                celEspacioNoFactura.Border = 0;
                //celNo.Padding = 3;
                celEspacioNoFactura.BorderWidthRight = 1;
                celEspacioNoFactura.HorizontalAlignment = Element.ALIGN_CENTER;
                celEspacioNoFactura.VerticalAlignment = Element.ALIGN_TOP;
                celEspacioNoFactura.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableFactura.AddCell(celEspacioNoFactura);

                iTextSharp.text.pdf.PdfPCell celRellenoNoFactura = new iTextSharp.text.pdf.PdfPCell(new Phrase(" ", fontTitleFactura));
                celRellenoNoFactura.Colspan = 3;
                celRellenoNoFactura.Padding = 3;
                celRellenoNoFactura.Border = 0;
                celRellenoNoFactura.BorderWidthLeft = 1;
                celRellenoNoFactura.BorderWidthRight = 1;
                celRellenoNoFactura.BorderWidthBottom = 1;
                celRellenoNoFactura.BackgroundColor = BaseColor.LIGHT_GRAY;
                celRellenoNoFactura.HorizontalAlignment = Element.ALIGN_RIGHT;
                celRellenoNoFactura.VerticalAlignment = Element.ALIGN_TOP;
                tableFactura.AddCell(celRellenoNoFactura);

                iTextSharp.text.pdf.PdfPCell celConsecutivoInterno = new iTextSharp.text.pdf.PdfPCell(new Phrase("CUFE: " + CUFE, fontCustom));
                celConsecutivoInterno.Colspan = 4;
                //celConsecutivoInterno.Padding = 3;
                celConsecutivoInterno.BackgroundColor = BaseColor.WHITE;
                celConsecutivoInterno.HorizontalAlignment = Element.ALIGN_RIGHT;
                celConsecutivoInterno.VerticalAlignment = Element.ALIGN_TOP;
                celConsecutivoInterno.Border = 0;
                tableFactura.AddCell(celConsecutivoInterno);

                // agregamos ---------------------------------------------------------------------------

                PdfPTable Header = new PdfPTable(new float[] { 15f, 20f, 20f, 15f, 30f, });
                Header.WidthPercentage = 100;       

                PdfPCell Cell_logo = new PdfPCell() { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER };
                PdfPCell Cell_info = new PdfPCell(new Phrase(DsInvoiceAR.Tables["Company"].Rows[0]["Name"].ToString()
                    + "\nNit. " + string.Concat(DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"].ToString()), fontTitle))
                {
                    VerticalAlignment=Element.ALIGN_CENTER,
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    Border = 0,
                };
                PdfPCell Cell_certificado = new PdfPCell() { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER };
                PdfPCell Cell_Qr = new PdfPCell() { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER };
                PdfPCell Cell_tipo = new PdfPCell() { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER };

                Cell_logo.AddElement(LogoPdf2);
                Cell_certificado.AddElement(LogoPdf3);
                Cell_Qr.AddElement(QRPdf);
                Cell_tipo.AddElement(tableFactura);


                Header.AddCell(Cell_logo);
                Header.AddCell(Cell_info);
                Header.AddCell(Cell_certificado);
                Header.AddCell(Cell_Qr);
                Header.AddCell(Cell_tipo);

                #endregion

                #region FACTURAR Y DESPACHAR A

                //agregamos la informacion del cliente

                PdfPTable tableFacturar = new PdfPTable(3);
                //Dimenciones.
                float[] DimencionFacturar = new float[3];
                DimencionFacturar[0] = 1.0F;//
                DimencionFacturar[1] = 0.01F;//
                DimencionFacturar[2] = 1.0F;//

                tableFacturar.WidthPercentage = 100;
                tableFacturar.SetWidths(DimencionFacturar);
                //----------------------------------------------------------------------------------------------
                PdfPTable tableFacturarA = new PdfPTable(2);
                float[] DimencionFacturarA = new float[2];
                DimencionFacturarA[0] = 0.8F;//
                DimencionFacturarA[1] = 2.0F;//

                tableFacturarA.WidthPercentage = 100;
                tableFacturarA.SetWidths(DimencionFacturarA);

                iTextSharp.text.pdf.PdfPCell celDatosFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase("FACTURAR A:\n", fontTitleFactura));
                celDatosFacturarA.Colspan = 2;
                celDatosFacturarA.Padding = 3;
                celDatosFacturarA.Border = 0;
                celDatosFacturarA.BorderColorBottom = BaseColor.WHITE;
                celDatosFacturarA.HorizontalAlignment = Element.ALIGN_CENTER;
                celDatosFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celDatosFacturarA);

                iTextSharp.text.pdf.PdfPCell celTextClienteFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase("CLIENTE:", fontTitleFactura));
                celTextClienteFacturarA.Colspan = 1;
                celTextClienteFacturarA.Padding = 3;
                celTextClienteFacturarA.Border = 0;
                celTextClienteFacturarA.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextClienteFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celTextClienteFacturarA);

                iTextSharp.text.pdf.PdfPCell celClienteFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase(DsInvoiceAR.Tables["InvcHead"].Rows[0]["CustomerName"].ToString(),
                    fontTitle2));
                celClienteFacturarA.Colspan = 1;
                celClienteFacturarA.Padding = 3;
                celClienteFacturarA.Border = 0;
                celClienteFacturarA.HorizontalAlignment = Element.ALIGN_LEFT;
                celClienteFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celClienteFacturarA);

                iTextSharp.text.pdf.PdfPCell celTextNitFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase("NIT:", fontTitleFactura));
                celTextNitFacturarA.Colspan = 1;
                celTextNitFacturarA.Padding = 3;
                celTextNitFacturarA.Border = 0;
                celTextNitFacturarA.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextNitFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celTextNitFacturarA);

                iTextSharp.text.pdf.PdfPCell celNitFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase(DsInvoiceAR.Tables["Customer"].Rows[0]["CustID"].ToString(), fontTitle2));
                celNitFacturarA.Colspan = 1;
                celNitFacturarA.Padding = 3;
                celNitFacturarA.Border = 0;
                celNitFacturarA.HorizontalAlignment = Element.ALIGN_LEFT;
                celNitFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celNitFacturarA);

                iTextSharp.text.pdf.PdfPCell celTextDireccionFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase("DIRECCION:", fontTitleFactura));
                celTextDireccionFacturarA.Colspan = 1;
                celTextDireccionFacturarA.Padding = 3;
                celTextDireccionFacturarA.Border = 0;
                celTextDireccionFacturarA.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextDireccionFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celTextDireccionFacturarA);

                iTextSharp.text.pdf.PdfPCell celDireccionFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase(DsInvoiceAR.Tables["Customer"].Rows[0]["Address1"].ToString(), fontTitle2));
                celDireccionFacturarA.Colspan = 1;
                celDireccionFacturarA.Padding = 3;
                celDireccionFacturarA.Border = 0;
                celDireccionFacturarA.HorizontalAlignment = Element.ALIGN_LEFT;
                celDireccionFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celDireccionFacturarA);

                iTextSharp.text.pdf.PdfPCell celTextTelFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase("TELEFONO:", fontTitleFactura));
                celTextTelFacturarA.Colspan = 1;
                celTextTelFacturarA.Padding = 3;
                celTextTelFacturarA.Border = 0;
                celTextTelFacturarA.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextTelFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celTextTelFacturarA);

                iTextSharp.text.pdf.PdfPCell celTelFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase(DsInvoiceAR.Tables["Customer"].Rows[0]["PhoneNum"].ToString(), fontTitle2));
                celTelFacturarA.Colspan = 1;
                celTelFacturarA.Padding = 3;
                celTelFacturarA.Border = 0;
                celTelFacturarA.HorizontalAlignment = Element.ALIGN_LEFT;
                celTelFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celTelFacturarA);

                iTextSharp.text.pdf.PdfPCell celTextCiudadFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase("CIUDAD:", fontTitleFactura));
                celTextCiudadFacturarA.Colspan = 1;
                celTextCiudadFacturarA.Padding = 3;
                celTextCiudadFacturarA.Border = 0;
                celTextCiudadFacturarA.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextCiudadFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celTextCiudadFacturarA);

                iTextSharp.text.pdf.PdfPCell celCiudadFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase(DsInvoiceAR.Tables["Customer"].Rows[0]["City"].ToString(), fontTitle2));
                celCiudadFacturarA.Colspan = 1;
                celCiudadFacturarA.Padding = 3;
                celCiudadFacturarA.Border = 0;
                celCiudadFacturarA.HorizontalAlignment = Element.ALIGN_LEFT;
                celCiudadFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celCiudadFacturarA);

                iTextSharp.text.pdf.PdfPCell celTextPaisFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase("PAIS:", fontTitleFactura));
                celTextPaisFacturarA.Colspan = 1;
                celTextPaisFacturarA.Padding = 3;
                celTextPaisFacturarA.Border = 0;
                celTextPaisFacturarA.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextPaisFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celTextPaisFacturarA);

                iTextSharp.text.pdf.PdfPCell celPaisFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase(DsInvoiceAR.Tables["Customer"].Rows[0]["Country"].ToString(), fontTitle2));
                celPaisFacturarA.Colspan = 1;
                celPaisFacturarA.Padding = 3;
                celPaisFacturarA.Border = 0;
                celPaisFacturarA.HorizontalAlignment = Element.ALIGN_LEFT;
                celPaisFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celPaisFacturarA);

                iTextSharp.text.pdf.PdfPCell celTittleFacturarA = new iTextSharp.text.pdf.PdfPCell(tableFacturarA);
                tableFacturar.AddCell(celTittleFacturarA);
                //----------------------------------------------------------------------------------------------------------------------------

                iTextSharp.text.pdf.PdfPCell celEspacio2 = new iTextSharp.text.pdf.PdfPCell(new Phrase(" ", fontTitleFactura));
                celEspacio2.Border = 0;
                tableFacturar.AddCell(celEspacio2);

                //------------------------------------------------------------------------------------------

                PdfPTable tableDespacharA = new PdfPTable(2);
                float[] DimencionDespacharA = new float[2];
                DimencionDespacharA[0] = 0.8F;//
                DimencionDespacharA[1] = 2.0F;//

                tableDespacharA.WidthPercentage = 100;
                tableDespacharA.SetWidths(DimencionDespacharA);

                iTextSharp.text.pdf.PdfPCell celTittleDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase("DESPACHAR A:\n", fontTitleFactura));
                celTittleDespacharA.Colspan = 2;
                celTittleDespacharA.Padding = 3;
                celTittleDespacharA.Border = 0;
                celTittleDespacharA.BorderColorBottom = BaseColor.WHITE;
                celTittleDespacharA.HorizontalAlignment = Element.ALIGN_CENTER;
                celTittleDespacharA.VerticalAlignment = Element.ALIGN_TOP;
                tableDespacharA.AddCell(celTittleDespacharA);

                iTextSharp.text.pdf.PdfPCell celTextClienteDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase("CLIENTE:", fontTitleFactura));
                celTextClienteDespacharA.Colspan = 1;
                celTextClienteDespacharA.Padding = 3;
                celTextClienteDespacharA.Border = 0;
                celTextClienteDespacharA.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextClienteDespacharA.VerticalAlignment = Element.ALIGN_TOP;
                tableDespacharA.AddCell(celTextClienteDespacharA);

                iTextSharp.text.pdf.PdfPCell celClienteDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase(DsInvoiceAR.Tables["Customer"].Rows[0]["ShipToName"].ToString(), fontTitle2));
                celClienteDespacharA.Colspan = 1;
                celClienteDespacharA.Padding = 3;
                celClienteDespacharA.Border = 0;
                celClienteDespacharA.HorizontalAlignment = Element.ALIGN_LEFT;
                celClienteDespacharA.VerticalAlignment = Element.ALIGN_TOP;
                tableDespacharA.AddCell(celClienteDespacharA);

                iTextSharp.text.pdf.PdfPCell celTextNitDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase("NIT:", fontTitleFactura));
                celTextNitDespacharA.Colspan = 1;
                celTextNitDespacharA.Padding = 3;
                celTextNitDespacharA.Border = 0;
                celTextNitDespacharA.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextNitDespacharA.VerticalAlignment = Element.ALIGN_TOP;
                tableDespacharA.AddCell(celTextNitDespacharA);

                iTextSharp.text.pdf.PdfPCell celNitDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase(DsInvoiceAR.Tables["Customer"].Rows[0]["ShipToNit"].ToString(), fontTitle2));
                celNitDespacharA.Colspan = 1;
                celNitDespacharA.Padding = 3;
                celNitDespacharA.Border = 0;
                celNitDespacharA.HorizontalAlignment = Element.ALIGN_LEFT;
                celNitDespacharA.VerticalAlignment = Element.ALIGN_TOP;
                tableDespacharA.AddCell(celNitDespacharA);

                iTextSharp.text.pdf.PdfPCell celTextDireccionDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase("DIRECCION:", fontTitleFactura));
                celTextDireccionDespacharA.Colspan = 1;
                celTextDireccionDespacharA.Padding = 3;
                celTextDireccionDespacharA.Border = 0;
                celTextDireccionDespacharA.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextDireccionDespacharA.VerticalAlignment = Element.ALIGN_TOP;
                tableDespacharA.AddCell(celTextDireccionDespacharA);

                iTextSharp.text.pdf.PdfPCell celDireccionDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase(DsInvoiceAR.Tables["Customer"].Rows[0]["ShipToAddress"].ToString(), fontTitle2));
                celDireccionDespacharA.Colspan = 1;
                celDireccionDespacharA.Padding = 3;
                celDireccionDespacharA.Border = 0;
                celDireccionDespacharA.HorizontalAlignment = Element.ALIGN_LEFT;
                celDireccionDespacharA.VerticalAlignment = Element.ALIGN_TOP;
                tableDespacharA.AddCell(celDireccionDespacharA);

                iTextSharp.text.pdf.PdfPCell celTextTelDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase("TELEFONO:", fontTitleFactura));
                celTextTelDespacharA.Colspan = 1;
                celTextTelDespacharA.Padding = 3;
                celTextTelDespacharA.Border = 0;
                celTextTelDespacharA.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextTelDespacharA.VerticalAlignment = Element.ALIGN_TOP;
                tableDespacharA.AddCell(celTextTelDespacharA);

                iTextSharp.text.pdf.PdfPCell celTelDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase(DsInvoiceAR.Tables["Customer"].Rows[0]["ShipToPhone"].ToString(), fontTitle2));
                celTelDespacharA.Colspan = 1;
                celTelDespacharA.Padding = 3;
                celTelDespacharA.Border = 0;
                celTelDespacharA.HorizontalAlignment = Element.ALIGN_LEFT;
                celTelDespacharA.VerticalAlignment = Element.ALIGN_TOP;
                tableDespacharA.AddCell(celTelDespacharA);

                iTextSharp.text.pdf.PdfPCell celTextCiudadDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase("CIUDAD:", fontTitleFactura));
                celTextCiudadDespacharA.Colspan = 1;
                celTextCiudadDespacharA.Padding = 3;
                celTextCiudadDespacharA.Border = 0;
                celTextCiudadDespacharA.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextCiudadDespacharA.VerticalAlignment = Element.ALIGN_TOP;
                tableDespacharA.AddCell(celTextCiudadDespacharA);

                iTextSharp.text.pdf.PdfPCell celCiudadDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase(DsInvoiceAR.Tables["Customer"].Rows[0]["ShipToCity"].ToString(), fontTitle2));
                celCiudadDespacharA.Colspan = 1;
                celCiudadDespacharA.Padding = 3;
                celCiudadDespacharA.Border = 0;
                celCiudadDespacharA.HorizontalAlignment = Element.ALIGN_LEFT;
                celCiudadDespacharA.VerticalAlignment = Element.ALIGN_TOP;
                tableDespacharA.AddCell(celCiudadDespacharA);

                iTextSharp.text.pdf.PdfPCell celTextPaisDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase("PAIS:", fontTitleFactura));
                celTextPaisDespacharA.Colspan = 1;
                celTextPaisDespacharA.Padding = 3;
                celTextPaisDespacharA.Border = 0;
                celTextPaisDespacharA.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextPaisDespacharA.VerticalAlignment = Element.ALIGN_TOP;
                tableDespacharA.AddCell(celTextPaisDespacharA);

                iTextSharp.text.pdf.PdfPCell celPaisDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase(DsInvoiceAR.Tables["Customer"].Rows[0]["ShipToCountry"].ToString(), fontTitle2));
                celPaisDespacharA.Colspan = 1;
                celPaisDespacharA.Padding = 3;
                celPaisDespacharA.Border = 0;
                celPaisDespacharA.HorizontalAlignment = Element.ALIGN_LEFT;
                celPaisDespacharA.VerticalAlignment = Element.ALIGN_TOP;
                tableDespacharA.AddCell(celPaisDespacharA);

                iTextSharp.text.pdf.PdfPCell celDatosDespacharA = new iTextSharp.text.pdf.PdfPCell(tableDespacharA);
                tableFacturar.AddCell(celDatosDespacharA);
                //-------------------------------------------------------------------------------------- 
                #endregion

                #region Tabla de Detalles

                //agregamos los de talles de factura 
                
                PdfPTable tableDetalles2 = new PdfPTable(4);
                tableDetalles2.WidthPercentage = 100;

                PdfPTable tableFechaFactura = new PdfPTable(2);
                float[] dimecionesTablaFecha = new float[2];
                dimecionesTablaFecha[0] = 1.3F;
                dimecionesTablaFecha[1] = 0.9F;
                tableFechaFactura.SetWidths(dimecionesTablaFecha);

                iTextSharp.text.pdf.PdfPCell celTextFechaFactura = new iTextSharp.text.pdf.PdfPCell(new Phrase("FECHA FACTURA: ",
                    fontTitle));
                celTextFechaFactura.Colspan = 1;
                celTextFechaFactura.Padding = 7;
                celTextFechaFactura.Border = 0;
                celTextFechaFactura.BorderColorBottom = BaseColor.WHITE;
                celTextFechaFactura.HorizontalAlignment = Element.ALIGN_CENTER;
                celTextFechaFactura.VerticalAlignment = Element.ALIGN_CENTER;
                tableFechaFactura.AddCell(celTextFechaFactura);

                iTextSharp.text.pdf.PdfPCell celFechaFactura = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:MM/dd/yyyy}", 
                    DateTime.Parse(DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"].ToString())),
                fontCustom));
                celFechaFactura.Colspan = 1;
                celFechaFactura.Padding = 7;
                //celFechaFactura.PaddingTop = 4;
                celFechaFactura.Border = 0;
                celFechaFactura.BorderColorBottom = BaseColor.WHITE;
                celFechaFactura.HorizontalAlignment = Element.ALIGN_LEFT;
                celFechaFactura.VerticalAlignment = Element.ALIGN_CENTER;
                tableFechaFactura.AddCell(celFechaFactura);

                PdfPCell _celFechaFactura = new PdfPCell(tableFechaFactura);
                tableDetalles2.AddCell(_celFechaFactura);


                PdfPTable tableFechaVencimiento = new PdfPTable(2);
                float[] dmTablaFechaVencimiento = new float[2];
                dmTablaFechaVencimiento[0] = 1.4F;
                dmTablaFechaVencimiento[1] = 0.8F;
                tableFechaVencimiento.SetWidths(dmTablaFechaVencimiento);

                iTextSharp.text.pdf.PdfPCell celTextFechaVencimiento = new iTextSharp.text.pdf.PdfPCell(new Phrase("FECHA VENCIMIENTO: ",
                    fontTitle));
                celTextFechaVencimiento.Colspan = 1;
                celTextFechaVencimiento.PaddingTop = 7;
                celTextFechaVencimiento.Border = 0;
                celTextFechaVencimiento.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextFechaVencimiento.VerticalAlignment = Element.ALIGN_TOP;
                tableFechaVencimiento.AddCell(celTextFechaVencimiento);

                iTextSharp.text.pdf.PdfPCell celFechaVencimiento = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:MM/dd/yyyy}",
                   DateTime.Parse(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"].ToString())),
                fontCustom));
                celFechaVencimiento.Colspan = 1;
                celFechaVencimiento.Padding = 7;
                celFechaVencimiento.Border = 0;
                celFechaVencimiento.BorderColorBottom = BaseColor.WHITE;
                celFechaVencimiento.HorizontalAlignment = Element.ALIGN_LEFT;
                celFechaVencimiento.VerticalAlignment = Element.ALIGN_TOP;
                tableFechaVencimiento.AddCell(celFechaVencimiento);

                PdfPCell _celFechaFacturaVencimiento = new PdfPCell(tableFechaVencimiento);
                tableDetalles2.AddCell(_celFechaFacturaVencimiento);

                PdfPTable tableMoneda = new PdfPTable(2);
                float[] dimecionesTablaMoneda = new float[2];
                dimecionesTablaMoneda[0] = 0.6F;
                dimecionesTablaMoneda[1] = 1.0F;
                tableMoneda.SetWidths(dimecionesTablaMoneda);

                iTextSharp.text.pdf.PdfPCell celTextMoneda = new iTextSharp.text.pdf.PdfPCell(new Phrase("MONEDA: ",
                    fontTitle));
                celTextMoneda.Colspan = 1;
                celTextMoneda.Padding = 7;
                celTextMoneda.Border = 0;
                celTextMoneda.BorderColorBottom = BaseColor.WHITE;
                celTextMoneda.HorizontalAlignment = Element.ALIGN_RIGHT;
                celTextMoneda.VerticalAlignment = Element.ALIGN_TOP;
                tableMoneda.AddCell(celTextMoneda);

                iTextSharp.text.pdf.PdfPCell celMoneda = new iTextSharp.text.pdf.PdfPCell(new Phrase(DsInvoiceAR.Tables["InvcHead"].Rows[0]["CurrencyCode"].ToString(),
                fontCustom));
                celMoneda.Colspan = 1;
                celMoneda.Padding = 7;
                celMoneda.Border = 0;
                celMoneda.BorderColorBottom = BaseColor.WHITE;
                celMoneda.HorizontalAlignment = Element.ALIGN_LEFT;
                celMoneda.VerticalAlignment = Element.ALIGN_TOP;
                tableMoneda.AddCell(celMoneda);

                PdfPCell _celMoneda = new PdfPCell(tableMoneda);
                tableDetalles2.AddCell(_celMoneda);

                PdfPTable tableFormaPago = new PdfPTable(2);
                tableFormaPago.WidthPercentage = 100;

                iTextSharp.text.pdf.PdfPCell celTextFormaPago = new iTextSharp.text.pdf.PdfPCell(new Phrase("FORMA DE PAGO: ",
                    fontTitle));
                celTextFormaPago.Colspan = 1;
                celTextFormaPago.Padding = 7;
                celTextFormaPago.Border = 0;
                celTextFormaPago.BorderColorBottom = BaseColor.WHITE;
                celTextFormaPago.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextFormaPago.VerticalAlignment = Element.ALIGN_TOP;
                tableFormaPago.AddCell(celTextFormaPago);

                iTextSharp.text.pdf.PdfPCell celFormaPago = new iTextSharp.text.pdf.PdfPCell(new Phrase(DsInvoiceAR.Tables["Customer"].Rows[0]["TermsDescription"].ToString(),
                fontCustom));
                celFormaPago.Colspan = 1;
                celFormaPago.Padding = 7;
                celFormaPago.Border = 0;
                celFormaPago.BorderColorBottom = BaseColor.WHITE;
                celFormaPago.HorizontalAlignment = Element.ALIGN_LEFT;
                celFormaPago.VerticalAlignment = Element.ALIGN_TOP;
                tableFormaPago.AddCell(celFormaPago);


                PdfPCell _celFormaPago = new PdfPCell(tableFormaPago);
                tableDetalles2.AddCell(_celFormaPago);

                //-----------------------------------------------------------------------------------------
                PdfPTable tableDetalles = new PdfPTable(5);
                tableDetalles.PaddingTop = 20;
                //Dimenciones.
                float[] DimencionDetalles = new float[5];
                DimencionDetalles[0] = 2.2F;//
                DimencionDetalles[1] = 1.0F;//
                DimencionDetalles[2] = 1.0F;//
                DimencionDetalles[3] = 1.3F;//
                DimencionDetalles[4] = 1.0F;//

                tableDetalles.WidthPercentage = 100;
                tableDetalles.SetWidths(DimencionDetalles);

                iTextSharp.text.pdf.PdfPCell celVendedor = new iTextSharp.text.pdf.PdfPCell(new Phrase("VENDEDOR", fontTitleFactura));
                celVendedor.Colspan = 1;
                celVendedor.Padding = 3;
                //celVendedor.Border = 0;
                celVendedor.BorderColorBottom = BaseColor.WHITE;
                celVendedor.HorizontalAlignment = Element.ALIGN_CENTER;
                celVendedor.VerticalAlignment = Element.ALIGN_TOP;
                celVendedor.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableDetalles.AddCell(celVendedor);

                iTextSharp.text.pdf.PdfPCell celOC_Cliente = new iTextSharp.text.pdf.PdfPCell(new Phrase("O.C.", fontTitleFactura));
                celOC_Cliente.Colspan = 1;
                celOC_Cliente.Padding = 3;
                //celVendedor.Border = 0;
                celOC_Cliente.BorderColorBottom = BaseColor.WHITE;
                celOC_Cliente.HorizontalAlignment = Element.ALIGN_CENTER;
                celOC_Cliente.VerticalAlignment = Element.ALIGN_TOP;
                celOC_Cliente.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableDetalles.AddCell(celOC_Cliente);

                iTextSharp.text.pdf.PdfPCell celOrdenVenta = new iTextSharp.text.pdf.PdfPCell(new Phrase("ORDEN DE VENTA", fontTitleFactura));
                celOrdenVenta.Colspan = 1;
                celOrdenVenta.Padding = 3;
                //celVendedor.Border = 0;
                celOrdenVenta.BorderColorBottom = BaseColor.WHITE;
                celOrdenVenta.HorizontalAlignment = Element.ALIGN_CENTER;
                celOrdenVenta.VerticalAlignment = Element.ALIGN_TOP;
                celOrdenVenta.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableDetalles.AddCell(celOrdenVenta);

                iTextSharp.text.pdf.PdfPCell celLocalizacion = new iTextSharp.text.pdf.PdfPCell(new Phrase("LOCALIZACIÓN", fontTitleFactura));
                celLocalizacion.Colspan = 1;
                celLocalizacion.Padding = 3;
                //celVendedor.Border = 0;
                celLocalizacion.BackgroundColor = BaseColor.LIGHT_GRAY;
                celLocalizacion.BorderColorBottom = BaseColor.WHITE;
                celLocalizacion.HorizontalAlignment = Element.ALIGN_CENTER;
                celLocalizacion.VerticalAlignment = Element.ALIGN_TOP;
                tableDetalles.AddCell(celLocalizacion);

                iTextSharp.text.pdf.PdfPCell celRemision = new iTextSharp.text.pdf.PdfPCell(new Phrase("REMISIÓN", fontTitleFactura));
                celRemision.Colspan = 1;
                celRemision.Padding = 3;
                celRemision.BackgroundColor = BaseColor.LIGHT_GRAY;
                //celVendedor.Border = 0;
                celRemision.BorderColorBottom = BaseColor.WHITE;
                celRemision.HorizontalAlignment = Element.ALIGN_CENTER;
                celRemision.VerticalAlignment = Element.ALIGN_TOP;
                tableDetalles.AddCell(celRemision);

                iTextSharp.text.pdf.PdfPCell celDataVendedor = new iTextSharp.text.pdf.PdfPCell(new Phrase(DsInvoiceAR.Tables["InvcHead"].Rows[0]["SalesRepName1"].ToString(), fontCustom));
                celDataVendedor.Colspan = 1;
                celDataVendedor.Padding = 3;
                //celVendedor.Border = 0;
                celDataVendedor.BorderColorBottom = BaseColor.WHITE;
                celDataVendedor.HorizontalAlignment = Element.ALIGN_CENTER;
                celDataVendedor.VerticalAlignment = Element.ALIGN_TOP;
                tableDetalles.AddCell(celDataVendedor);

                iTextSharp.text.pdf.PdfPCell celDataOC_Cliente = new iTextSharp.text.pdf.PdfPCell(new Phrase(DsInvoiceAR.Tables["InvcHead"].Rows[0]["PONum"].ToString(), fontCustom));
                celDataOC_Cliente.Colspan = 1;
                celDataOC_Cliente.Padding = 3;
                //celVendedor.Border = 0;
                celDataOC_Cliente.BorderColorBottom = BaseColor.WHITE;
                celDataOC_Cliente.HorizontalAlignment = Element.ALIGN_CENTER;
                celDataOC_Cliente.VerticalAlignment = Element.ALIGN_TOP;
                tableDetalles.AddCell(celDataOC_Cliente);

                iTextSharp.text.pdf.PdfPCell celDataOrdenVenta = new iTextSharp.text.pdf.PdfPCell(new Phrase(DsInvoiceAR.Tables["InvcHead"].Rows[0]["OrderNum"].ToString(),
                    fontCustom));
                celDataOrdenVenta.Colspan = 1;
                celDataOrdenVenta.Padding = 3;
                //celVendedor.Border = 0;
                celDataOrdenVenta.BorderColorBottom = BaseColor.WHITE;
                celDataOrdenVenta.HorizontalAlignment = Element.ALIGN_CENTER;
                celDataOrdenVenta.VerticalAlignment = Element.ALIGN_TOP;
                tableDetalles.AddCell(celDataOrdenVenta);
                
                iTextSharp.text.pdf.PdfPCell celDataCondicionEntrega = new iTextSharp.text.pdf.PdfPCell(new Phrase(DsInvoiceAR.Tables["Customer"].Rows[0]["ShipViaDescription"].ToString(), fontCustom));
                celDataCondicionEntrega.Colspan = 1;
                celDataCondicionEntrega.Padding = 3;
                //celVendedor.Border = 0;
                celDataCondicionEntrega.BorderColorBottom = BaseColor.WHITE;
                celDataCondicionEntrega.HorizontalAlignment = Element.ALIGN_CENTER;
                celDataCondicionEntrega.VerticalAlignment = Element.ALIGN_TOP;
                tableDetalles.AddCell(celDataCondicionEntrega);

                iTextSharp.text.pdf.PdfPCell celDataRemision = new iTextSharp.text.pdf.PdfPCell(new Phrase(DsInvoiceAR.Tables["InvcDtl"].Rows[0]["PackNum"].ToString(), fontCustom));
                celDataRemision.Colspan = 1;
                celDataRemision.Padding = 3;
                //celVendedor.Border = 0;
                celDataRemision.BorderColorBottom = BaseColor.WHITE;
                celDataRemision.HorizontalAlignment = Element.ALIGN_CENTER;
                celDataRemision.VerticalAlignment = Element.ALIGN_TOP;
                tableDetalles.AddCell(celDataRemision);
                #endregion

                #region Tabla Unidades

                //agregamos las unidades y valores
                PdfPTable tableTituloUnidades = new PdfPTable(8);
                //Dimenciones.
                float[] DimencionUnidades = new float[8];
                DimencionUnidades[0] = 0.8F;//codigo
                DimencionUnidades[1] = 1.5F;//descripcion
                DimencionUnidades[2] = 0.8F;//cantidad
                DimencionUnidades[3] = 0.3F;//um
                DimencionUnidades[4] = 1.0F;//valor unitario
                DimencionUnidades[5] = 1.2F;//descuentos
                DimencionUnidades[6] = 1.0F;//Valor Total
                DimencionUnidades[7] = 0.7F;//IVA

                tableTituloUnidades.WidthPercentage = 100;
                tableTituloUnidades.SetWidths(DimencionUnidades);

                iTextSharp.text.pdf.PdfPCell celCodigo = new iTextSharp.text.pdf.PdfPCell(new Phrase("CÓDIGO", fontTitleFactura));
                celCodigo.Colspan = 1;
                celCodigo.PaddingTop = 6;
                //celCodigo.Border = 1;
                celCodigo.BorderWidthRight = 0;
                celCodigo.BackgroundColor = BaseColor.LIGHT_GRAY;
                celCodigo.HorizontalAlignment = Element.ALIGN_CENTER;
                celCodigo.VerticalAlignment = Element.ALIGN_TOP;

                tableTituloUnidades.AddCell(celCodigo);

                iTextSharp.text.pdf.PdfPCell celDescripcion = new iTextSharp.text.pdf.PdfPCell(new Phrase("DESCRIPCIÓN", fontTitleFactura));
                celDescripcion.Colspan = 1;
                celDescripcion.PaddingTop = 6;
                //celDescripcion.Border = 1;
                celDescripcion.BorderWidthRight = 0;
                celDescripcion.BackgroundColor = BaseColor.LIGHT_GRAY;
                celDescripcion.HorizontalAlignment = Element.ALIGN_CENTER;
                celDescripcion.VerticalAlignment = Element.ALIGN_TOP;

                tableTituloUnidades.AddCell(celDescripcion);

                iTextSharp.text.pdf.PdfPCell celCantidad = new iTextSharp.text.pdf.PdfPCell(new Phrase("CANT.", fontTitleFactura));
                celCantidad.Colspan = 1;
                celCantidad.PaddingTop = 6;
                //celVendedor.Border = 0;
                celCantidad.BorderWidthRight = 0;
                celCantidad.BackgroundColor = BaseColor.LIGHT_GRAY;
                celCantidad.HorizontalAlignment = Element.ALIGN_CENTER;
                celCantidad.VerticalAlignment = Element.ALIGN_TOP;
                tableTituloUnidades.AddCell(celCantidad);

                iTextSharp.text.pdf.PdfPCell celUM = new iTextSharp.text.pdf.PdfPCell(new Phrase("U.M.", fontTitleFactura));
                celUM.Colspan = 1;
                celUM.PaddingTop = 6;
                //celVendedor.Border = 0;
                celUM.BorderWidthRight = 0;
                celUM.BackgroundColor = BaseColor.LIGHT_GRAY;
                celUM.HorizontalAlignment = Element.ALIGN_CENTER;
                celUM.VerticalAlignment = Element.ALIGN_TOP;

                tableTituloUnidades.AddCell(celUM);

                iTextSharp.text.pdf.PdfPCell celValorUnit = new iTextSharp.text.pdf.PdfPCell(new Phrase("VR. UNIT.", fontTitleFactura));
                celValorUnit.Colspan = 1;
                celValorUnit.PaddingTop = 6;
                //celVendedor.Border = 0;
                celValorUnit.BorderWidthRight = 0;
                celValorUnit.BackgroundColor = BaseColor.LIGHT_GRAY;
                celValorUnit.HorizontalAlignment = Element.ALIGN_CENTER;
                celValorUnit.VerticalAlignment = Element.ALIGN_TOP;
                tableTituloUnidades.AddCell(celValorUnit);

                iTextSharp.text.pdf.PdfPTable tableDescuentos = new PdfPTable(2);

                iTextSharp.text.pdf.PdfPCell celDescuentos = new iTextSharp.text.pdf.PdfPCell(new Phrase("DESCUENTO", fontTitleFactura));
                celDescuentos.Colspan = 2;
                celDescuentos.HorizontalAlignment = Element.ALIGN_CENTER;
                celDescuentos.VerticalAlignment = Element.ALIGN_TOP;
                celDescuentos.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableDescuentos.AddCell(celDescuentos);

                iTextSharp.text.pdf.PdfPCell celDesc1 = new iTextSharp.text.pdf.PdfPCell(new Phrase("DCTO1", fontTitleFactura));
                celDesc1.Colspan = 1;
                celDesc1.HorizontalAlignment = Element.ALIGN_CENTER;
                celDesc1.VerticalAlignment = Element.ALIGN_TOP;
                celDesc1.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableDescuentos.AddCell(celDesc1);

                iTextSharp.text.pdf.PdfPCell celDesc2 = new iTextSharp.text.pdf.PdfPCell(new Phrase("DCTO2", fontTitleFactura));
                celDesc2.Colspan = 1;
                celDesc2.HorizontalAlignment = Element.ALIGN_CENTER;
                celDesc2.VerticalAlignment = Element.ALIGN_TOP;
                celDesc2.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableDescuentos.AddCell(celDesc2);

                iTextSharp.text.pdf.PdfPCell _celDescuentos12 = new iTextSharp.text.pdf.PdfPCell(tableDescuentos);
                tableTituloUnidades.AddCell(_celDescuentos12);

                iTextSharp.text.pdf.PdfPCell celValorTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase("VALOR TOTAL", fontTitleFactura));
                celValorTotal.Colspan = 1;
                celValorTotal.PaddingTop = 6;
                celValorTotal.HorizontalAlignment = Element.ALIGN_CENTER;
                celValorTotal.VerticalAlignment = Element.ALIGN_CENTER;
                celValorTotal.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableTituloUnidades.AddCell(celValorTotal);

                iTextSharp.text.pdf.PdfPCell celIva = new iTextSharp.text.pdf.PdfPCell(new Phrase("IVA", fontTitleFactura));
                celIva.Colspan = 1;
                celIva.PaddingTop = 6;
                celIva.HorizontalAlignment = Element.ALIGN_CENTER;
                celIva.VerticalAlignment = Element.ALIGN_TOP;
                celIva.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableTituloUnidades.AddCell(celIva);

                PdfPTable tableUnidades = new PdfPTable(8);
                tableUnidades.WidthPercentage = 100;
                tableUnidades.SetWidths(DimencionUnidades);

                decimal totalUnidades = 0;
                foreach (DataRow InvoiceLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
                {
                    if (!AddUnidadesCementos(InvoiceLine, ref tableUnidades, fontCustom, DsInvoiceAR))
                        return false;
                    //if (Helpers.Compartido.VerificarExistenciaColumnas(DsInvoiceAR, "InvcDtl", "SellingShipQty"))
                        totalUnidades += decimal.Parse((string)InvoiceLine["SellingShipQty"]);
                }

                iTextSharp.text.pdf.PdfPCell LineaFinal = new iTextSharp.text.pdf.PdfPCell();
                LineaFinal.Colspan = 8;
                LineaFinal.Padding = 3;
                LineaFinal.Border = 1;
                LineaFinal.BorderColorBottom = BaseColor.WHITE;
                LineaFinal.HorizontalAlignment = Element.ALIGN_CENTER;
                LineaFinal.VerticalAlignment = Element.ALIGN_TOP;
                tableUnidades.AddCell(LineaFinal);
                #endregion

                #region Footer
                //-----------------------------------------------------------------------------------------
                PdfPTable tableTotalUnidades = new PdfPTable(6);
                tableTotalUnidades.PaddingTop = 20;
                //Dimenciones.
                float[] DimencionTotalUnidades = new float[6];
                DimencionTotalUnidades[0] = 1.0F;//
                DimencionTotalUnidades[1] = 1.0F;//
                DimencionTotalUnidades[2] = 1.0F;//
                DimencionTotalUnidades[3] = 1.0F;//
                DimencionTotalUnidades[4] = 1.0F;//
                DimencionTotalUnidades[5] = 1.0F;//

                tableTotalUnidades.WidthPercentage = 100;
                tableTotalUnidades.SetWidths(DimencionTotalUnidades);

                iTextSharp.text.pdf.PdfPCell celTotalUnidades = new iTextSharp.text.pdf.PdfPCell(new Phrase("TOTAL UNIDADES", fontTitleFactura));
                celTotalUnidades.Colspan = 1;
                celTotalUnidades.Padding = 3;
                //celVendedor.Border = 0;
                celTotalUnidades.BorderColorBottom = BaseColor.WHITE;
                celTotalUnidades.HorizontalAlignment = Element.ALIGN_CENTER;
                celTotalUnidades.VerticalAlignment = Element.ALIGN_TOP;
                celTotalUnidades.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableTotalUnidades.AddCell(celTotalUnidades);

                iTextSharp.text.pdf.PdfPCell celPesoNeto = new iTextSharp.text.pdf.PdfPCell(new Phrase("PESO NETO", fontTitleFactura));
                celPesoNeto.Colspan = 1;
                celPesoNeto.Padding = 3;
                //celVendedor.Border = 0;
                celPesoNeto.BorderColorBottom = BaseColor.WHITE;
                celPesoNeto.HorizontalAlignment = Element.ALIGN_CENTER;
                celPesoNeto.VerticalAlignment = Element.ALIGN_TOP;
                celPesoNeto.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableTotalUnidades.AddCell(celPesoNeto);

                iTextSharp.text.pdf.PdfPCell celPesoBruto = new iTextSharp.text.pdf.PdfPCell(new Phrase("PESO BRUTO", fontTitleFactura));
                celPesoBruto.Colspan = 1;
                celPesoBruto.Padding = 3;
                //celVendedor.Border = 0;
                celPesoBruto.BorderColorBottom = BaseColor.WHITE;
                celPesoBruto.HorizontalAlignment = Element.ALIGN_CENTER;
                celPesoBruto.VerticalAlignment = Element.ALIGN_TOP;
                celPesoBruto.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableTotalUnidades.AddCell(celPesoBruto);

                iTextSharp.text.pdf.PdfPCell celSubTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase("SUBTOTAL", fontTitleFactura));
                celSubTotal.Colspan = 1;
                celSubTotal.Padding = 3;
                //celVendedor.Border = 0;
                celSubTotal.BackgroundColor = BaseColor.LIGHT_GRAY;
                celSubTotal.BorderColorBottom = BaseColor.WHITE;
                celSubTotal.HorizontalAlignment = Element.ALIGN_CENTER;
                celSubTotal.VerticalAlignment = Element.ALIGN_TOP;
                tableTotalUnidades.AddCell(celSubTotal);

                iTextSharp.text.pdf.PdfPCell celValorIva = new iTextSharp.text.pdf.PdfPCell(new Phrase("VALOR IVA", fontTitleFactura));
                celValorIva.Colspan = 1;
                celValorIva.Padding = 3;
                celValorIva.BackgroundColor = BaseColor.LIGHT_GRAY;
                //celVendedor.Border = 0;
                celValorIva.BorderColorBottom = BaseColor.WHITE;
                celValorIva.HorizontalAlignment = Element.ALIGN_CENTER;
                celValorIva.VerticalAlignment = Element.ALIGN_TOP;
                tableTotalUnidades.AddCell(celValorIva);

                iTextSharp.text.pdf.PdfPCell celTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase("TOTAL", fontTitleFactura));
                celTotal.Colspan = 1;
                celTotal.Padding = 3;
                celTotal.BackgroundColor = BaseColor.LIGHT_GRAY;
                //celVendedor.Border = 0;
                celTotal.BorderColorBottom = BaseColor.WHITE;
                celTotal.HorizontalAlignment = Element.ALIGN_CENTER;
                celTotal.VerticalAlignment = Element.ALIGN_TOP;
                tableTotalUnidades.AddCell(celTotal);

                iTextSharp.text.pdf.PdfPCell _celTotalUnidades = new iTextSharp.text.pdf.PdfPCell(new Phrase(totalUnidades.ToString("N2"), fontTitleFactura));
                _celTotalUnidades.Colspan = 1;
                _celTotalUnidades.Padding = 3;
                //celVendedor.Border = 0;
                _celTotalUnidades.HorizontalAlignment = Element.ALIGN_RIGHT;
                _celTotalUnidades.VerticalAlignment = Element.ALIGN_TOP;
                tableTotalUnidades.AddCell(_celTotalUnidades);

                iTextSharp.text.pdf.PdfPCell _celPesoNeto = new iTextSharp.text.pdf.PdfPCell(new Phrase(DsInvoiceAR.Tables["InvcHead"].Rows[0]["NetWeight"].ToString(), fontTitleFactura));
                _celPesoNeto.Colspan = 1;
                _celPesoNeto.Padding = 3;
                //celVendedor.Border = 0;
                _celPesoNeto.HorizontalAlignment = Element.ALIGN_RIGHT;
                _celPesoNeto.VerticalAlignment = Element.ALIGN_TOP;
                tableTotalUnidades.AddCell(_celPesoNeto);

                iTextSharp.text.pdf.PdfPCell _celPesoBruto = new iTextSharp.text.pdf.PdfPCell(new Phrase(DsInvoiceAR.Tables["InvcHead"].Rows[0]["GrossWeight"].ToString(), fontTitleFactura));
                _celPesoBruto.Colspan = 1;
                _celPesoBruto.Padding = 3;
                //celVendedor.Border = 0;
                _celPesoBruto.HorizontalAlignment = Element.ALIGN_RIGHT;
                _celPesoBruto.VerticalAlignment = Element.ALIGN_TOP;
                tableTotalUnidades.AddCell(_celPesoBruto);

                iTextSharp.text.pdf.PdfPCell _celTotalUnidadesSubTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase(Decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubTotal"]).ToString("C2"), fontTitleFactura));
                _celTotalUnidadesSubTotal.Colspan = 1;
                _celTotalUnidadesSubTotal.Padding = 3;
                //celVendedor.Border = 0;
                _celTotalUnidadesSubTotal.HorizontalAlignment = Element.ALIGN_RIGHT;
                _celTotalUnidadesSubTotal.VerticalAlignment = Element.ALIGN_TOP;
                tableTotalUnidades.AddCell(_celTotalUnidadesSubTotal);

                iTextSharp.text.pdf.PdfPCell _celValorIva = new iTextSharp.text.pdf.PdfPCell(new Phrase(Decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DocTaxAmt"]).ToString("C2"), fontTitleFactura));
                _celValorIva.Colspan = 1;
                _celValorIva.Padding = 3;
                //celVendedor.Border = 0;
                _celValorIva.HorizontalAlignment = Element.ALIGN_RIGHT;
                _celValorIva.VerticalAlignment = Element.ALIGN_TOP;
                tableTotalUnidades.AddCell(_celValorIva);

                iTextSharp.text.pdf.PdfPCell _celTotalUnidadesTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase(Decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"]).ToString("C2"), fontTitleFactura));
                _celTotalUnidadesTotal.Colspan = 1;
                _celTotalUnidadesTotal.Padding = 3;
                //celVendedor.Border = 0;
                _celTotalUnidadesTotal.HorizontalAlignment = Element.ALIGN_RIGHT;
                _celTotalUnidadesTotal.VerticalAlignment = Element.ALIGN_TOP;
                tableTotalUnidades.AddCell(_celTotalUnidadesTotal);

                //------------------------------------------------------------------------------------------------
                PdfPTable tableValorLetras = new PdfPTable(2);

                float[] DimencionValorLetras = new float[2];
                DimencionValorLetras[0] = 1.0F;//
                DimencionValorLetras[1] = 5.0F;//

                tableValorLetras.WidthPercentage = 100;
                tableValorLetras.SetWidths(DimencionValorLetras);

                iTextSharp.text.pdf.PdfPCell celTextValorLetras = new iTextSharp.text.pdf.PdfPCell(new Phrase("VALORES EN LETRA: ", fontTitleFactura));
                celTextValorLetras.Colspan = 1;
                celTextValorLetras.Padding = 3;
                celTextValorLetras.BackgroundColor = BaseColor.LIGHT_GRAY;
                celTextValorLetras.BorderWidthRight = 0;
                celTextValorLetras.BorderWidthBottom = 0;
                celTextValorLetras.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextValorLetras.VerticalAlignment = Element.ALIGN_TOP;
                tableValorLetras.AddCell(celTextValorLetras);

                iTextSharp.text.pdf.PdfPCell celValorLetras = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0}",
                Nroenletras(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"].ToString())), fontTitle2));
                celValorLetras.Colspan = 1;
                celValorLetras.Padding = 3;
                celValorLetras.BorderWidthLeft = 0;
                celValorLetras.BorderWidthBottom = 0;
                celValorLetras.HorizontalAlignment = Element.ALIGN_LEFT;
                celValorLetras.VerticalAlignment = Element.ALIGN_TOP;
                tableValorLetras.AddCell(celValorLetras);

                iTextSharp.text.pdf.PdfPCell celEspacioValorLetras = new iTextSharp.text.pdf.PdfPCell(new Phrase(" "));
                celEspacioValorLetras.Colspan = 2;
                celEspacioValorLetras.Padding = 3;
                celEspacioValorLetras.BorderWidthTop = 0;
                celEspacioValorLetras.HorizontalAlignment = Element.ALIGN_LEFT;
                celEspacioValorLetras.VerticalAlignment = Element.ALIGN_TOP;
                tableValorLetras.AddCell(celEspacioValorLetras);
                //------------------------------------------------------------------------------------------------
                PdfPTable tableObservaciones = new PdfPTable(2);

                float[] DimencionObservaciones = new float[2];
                DimencionObservaciones[0] = 1.0F;//
                DimencionObservaciones[1] = 5.0F;//

                tableObservaciones.WidthPercentage = 100;
                tableObservaciones.SetWidths(DimencionObservaciones);

                iTextSharp.text.pdf.PdfPCell celTextObservaciones = new iTextSharp.text.pdf.PdfPCell(new Phrase("OBSERVACIONES: ", fontTitleFactura));
                celTextObservaciones.Colspan = 1;
                celTextObservaciones.Padding = 3;
                celTextObservaciones.BackgroundColor = BaseColor.LIGHT_GRAY;
                celTextObservaciones.BorderWidthRight = 0;
                celTextObservaciones.BorderWidthBottom = 0;
                celTextObservaciones.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextObservaciones.VerticalAlignment = Element.ALIGN_TOP;
                tableObservaciones.AddCell(celTextObservaciones);

                iTextSharp.text.pdf.PdfPCell celObservaciones = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("\n{0}",
                    DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceComment"].ToString()), fontTitle2));
                celObservaciones.Colspan = 1;
                celObservaciones.Padding = 3;
                celObservaciones.BorderWidthLeft = 0;
                celObservaciones.BorderWidthBottom = 0;
                celObservaciones.HorizontalAlignment = Element.ALIGN_LEFT;
                celObservaciones.VerticalAlignment = Element.ALIGN_TOP;
                tableObservaciones.AddCell(celObservaciones);

                iTextSharp.text.pdf.PdfPCell celEspacioObservaciones = new iTextSharp.text.pdf.PdfPCell(new Phrase("\n\n\n\n"));
                celEspacioObservaciones.Colspan = 2;
                celEspacioObservaciones.Padding = 3;
                celEspacioObservaciones.BorderWidthTop = 0;
                celEspacioObservaciones.HorizontalAlignment = Element.ALIGN_LEFT;
                celEspacioObservaciones.VerticalAlignment = Element.ALIGN_TOP;
                tableObservaciones.AddCell(celEspacioObservaciones);
                //----------------------------------------------------------------------------------------------
                PdfPTable tableResolucion = new PdfPTable(1);
                tableResolucion.WidthPercentage = 100;
                string strResolucion = ".";
                iTextSharp.text.pdf.PdfPCell _celResolucion = new iTextSharp.text.pdf.PdfPCell(new Phrase(strResolucion, fontTitleFactura));
                _celResolucion.Colspan = 1;
                _celResolucion.Padding = 3;
                //_celResolucion.Border = 0;
                _celResolucion.HorizontalAlignment = Element.ALIGN_JUSTIFIED_ALL;
                _celResolucion.VerticalAlignment = Element.ALIGN_TOP;
                tableResolucion.AddCell(_celResolucion);
                //------------------------------------------------------------------------
                PdfPTable tableTrasnportador = new PdfPTable(4);

                float[] DimencionTransportador = new float[4];
                DimencionTransportador[0] = 1.0F;//
                DimencionTransportador[1] = 1.0F;//
                DimencionTransportador[2] = 1.3F;//
                DimencionTransportador[3] = 1.0F;//


                tableTrasnportador.WidthPercentage = 100;
                tableTrasnportador.SetWidths(DimencionTransportador);

                iTextSharp.text.pdf.PdfPCell celTransportador = new iTextSharp.text.pdf.PdfPCell(new Phrase("TRANSPORTADOR", fontTitleFactura));
                celTransportador.Colspan = 1;
                celTransportador.Padding = 3;
                celTransportador.BorderWidthBottom = 0;
                celTransportador.BackgroundColor = BaseColor.LIGHT_GRAY;
                celTransportador.HorizontalAlignment = Element.ALIGN_CENTER;
                celTransportador.VerticalAlignment = Element.ALIGN_TOP;
                tableTrasnportador.AddCell(celTransportador);

                iTextSharp.text.pdf.PdfPCell celConductor = new iTextSharp.text.pdf.PdfPCell(new Phrase("NOMBRE  DEL CONDUCTOR", fontTitleFactura));
                celConductor.Colspan = 1;
                celConductor.Padding = 3;
                celConductor.BorderWidthBottom = 0;
                celConductor.BackgroundColor = BaseColor.LIGHT_GRAY;
                celConductor.HorizontalAlignment = Element.ALIGN_CENTER;
                celConductor.VerticalAlignment = Element.ALIGN_TOP;
                tableTrasnportador.AddCell(celConductor);

                iTextSharp.text.pdf.PdfPCell celCC = new iTextSharp.text.pdf.PdfPCell(new Phrase("CEDULA DE CIUDADANIA", fontTitleFactura));
                celCC.Colspan = 1;
                celCC.Padding = 3;
                celCC.BorderWidthBottom = 0;
                celCC.BackgroundColor = BaseColor.LIGHT_GRAY;
                celCC.HorizontalAlignment = Element.ALIGN_CENTER;
                celCC.VerticalAlignment = Element.ALIGN_TOP;
                tableTrasnportador.AddCell(celCC);

                iTextSharp.text.pdf.PdfPCell celPlacaVehiculo = new iTextSharp.text.pdf.PdfPCell(new Phrase("PLACA VEHICULO", fontTitleFactura));
                celPlacaVehiculo.Colspan = 1;
                celPlacaVehiculo.Padding = 3;
                celPlacaVehiculo.BorderWidthBottom = 0;
                celPlacaVehiculo.BackgroundColor = BaseColor.LIGHT_GRAY;
                celPlacaVehiculo.HorizontalAlignment = Element.ALIGN_CENTER;
                celPlacaVehiculo.VerticalAlignment = Element.ALIGN_TOP;
                tableTrasnportador.AddCell(celPlacaVehiculo);

                //---------------------CAMPOS DE TRANSPORTADOR----------------------------
                iTextSharp.text.pdf.PdfPCell _celTransportador = new iTextSharp.text.pdf.PdfPCell(new Phrase(".", fontTitleFactura));
                _celTransportador.Colspan = 1;
                _celTransportador.Padding = 3;
                _celTransportador.BorderWidthTop = 0;
                _celTransportador.HorizontalAlignment = Element.ALIGN_CENTER;
                _celTransportador.VerticalAlignment = Element.ALIGN_TOP;
                tableTrasnportador.AddCell(_celTransportador);

                iTextSharp.text.pdf.PdfPCell _celConductor = new iTextSharp.text.pdf.PdfPCell(new Phrase("", fontTitleFactura));
                _celConductor.Colspan = 1;
                _celConductor.Padding = 3;
                _celConductor.BorderWidthTop = 0;
                _celConductor.HorizontalAlignment = Element.ALIGN_CENTER;
                _celConductor.VerticalAlignment = Element.ALIGN_TOP;
                tableTrasnportador.AddCell(_celConductor);

                iTextSharp.text.pdf.PdfPCell _celCC = new iTextSharp.text.pdf.PdfPCell(new Phrase("", fontTitleFactura));
                _celCC.Colspan = 1;
                _celCC.Padding = 3;
                _celCC.BorderWidthTop = 0;
                _celCC.HorizontalAlignment = Element.ALIGN_CENTER;
                _celCC.VerticalAlignment = Element.ALIGN_TOP;
                tableTrasnportador.AddCell(_celCC);

                iTextSharp.text.pdf.PdfPCell _celPlacaVehiculo = new iTextSharp.text.pdf.PdfPCell(new Phrase("", fontTitleFactura));
                _celPlacaVehiculo.Colspan = 1;
                _celPlacaVehiculo.Padding = 3;
                _celPlacaVehiculo.BorderWidthTop = 0;
                _celPlacaVehiculo.HorizontalAlignment = Element.ALIGN_CENTER;
                _celPlacaVehiculo.VerticalAlignment = Element.ALIGN_TOP;
                tableTrasnportador.AddCell(_celPlacaVehiculo);

                //------------------------------------------------------------------------
                PdfPTable tableFirmas = new PdfPTable(5);

                float[] DimencionFirmas = new float[5];
                DimencionFirmas[0] = 1.0F;//
                DimencionFirmas[1] = 0.02F;//
                DimencionFirmas[2] = 1.0F;//
                DimencionFirmas[3] = 0.02F;//
                DimencionFirmas[4] = 1.4F;//

                tableFirmas.WidthPercentage = 100;
                tableFirmas.SetWidths(DimencionFirmas);

                PdfPTable tableDespacahdoPor = new PdfPTable(1);
                //tableDespacahdoPor.WidthPercentage = 100;

                iTextSharp.text.pdf.PdfPCell celDespachadoPor = new iTextSharp.text.pdf.PdfPCell(new Phrase("\n\n\n\n\n" +
                    "\n______________________________________\n\nDESPACHADO POR", fontTitleFactura));
                celDespachadoPor.Colspan = 1;
                celDespachadoPor.Padding = 3;
                celDespachadoPor.HorizontalAlignment = Element.ALIGN_CENTER;
                celDespachadoPor.VerticalAlignment = Element.ALIGN_TOP;
                tableDespacahdoPor.AddCell(celDespachadoPor);

                iTextSharp.text.pdf.PdfPCell celEspacioDespachadoPor = new iTextSharp.text.pdf.PdfPCell(new Phrase("\n\n\n", fontTitleFactura));
                celEspacioDespachadoPor.Colspan = 1;
                celEspacioDespachadoPor.Padding = 3;
                celEspacioDespachadoPor.Border = 0;
                celEspacioDespachadoPor.HorizontalAlignment = Element.ALIGN_CENTER;
                celEspacioDespachadoPor.VerticalAlignment = Element.ALIGN_TOP;
                tableDespacahdoPor.AddCell(celEspacioDespachadoPor);

                iTextSharp.text.pdf.PdfPCell _celDespachadoPor = new iTextSharp.text.pdf.PdfPCell(tableDespacahdoPor);
                _celDespachadoPor.Border = 0;
                tableFirmas.AddCell(_celDespachadoPor);
                //------------------------------------------------------------------------------------------
                tableFirmas.AddCell(celEspacio2);
                //------------------------------------------------------------------------------------------
                PdfPTable tableFirmaConductor = new PdfPTable(1);
                tableFirmaConductor.WidthPercentage = 100;
                iTextSharp.text.pdf.PdfPCell celFirmaConductor = new iTextSharp.text.pdf.PdfPCell(new Phrase("\n\n\n\n\n\n" +
                    "______________________________________" +
                    "\n\nFIRMA DEL CONDUCTOR", fontTitleFactura));
                celFirmaConductor.Colspan = 1;
                celFirmaConductor.Padding = 3;
                //celFirmaConductor.Border = 0;
                celFirmaConductor.HorizontalAlignment = Element.ALIGN_CENTER;
                celFirmaConductor.VerticalAlignment = Element.ALIGN_TOP;
                tableFirmaConductor.AddCell(celFirmaConductor);

                iTextSharp.text.pdf.PdfPCell celEspacioFirmaConductor = new iTextSharp.text.pdf.PdfPCell(new Phrase("\n\n\n", fontTitleFactura));
                celEspacioFirmaConductor.Colspan = 1;
                celEspacioFirmaConductor.Padding = 3;
                celEspacioFirmaConductor.Border = 0;
                celEspacioFirmaConductor.HorizontalAlignment = Element.ALIGN_CENTER;
                celEspacioFirmaConductor.VerticalAlignment = Element.ALIGN_TOP;
                tableFirmaConductor.AddCell(celEspacioFirmaConductor);

                iTextSharp.text.pdf.PdfPCell _celFirmaConductor = new iTextSharp.text.pdf.PdfPCell(tableFirmaConductor);
                _celFirmaConductor.Border = 0;
                tableFirmas.AddCell(_celFirmaConductor);
                //-------------------------------------------------------------------------------------
                tableFirmas.AddCell(celEspacio2);
                //-------------------------------------------------------------------------------------

                PdfPTable tableSelloCliente = new PdfPTable(1);
                //tableSelloCliente.WidthPercentage = 100;

                iTextSharp.text.pdf.PdfPCell cel1SelloCliente = new iTextSharp.text.pdf.PdfPCell(new Phrase("Apreciado cliente este documento " +
                    "no es endolsable", fontTitleFactura));
                cel1SelloCliente.Colspan = 1;
                cel1SelloCliente.Padding = 5;
                cel1SelloCliente.Border = 1;
                cel1SelloCliente.BorderWidthBottom = 1;
                cel1SelloCliente.HorizontalAlignment = Element.ALIGN_LEFT;
                cel1SelloCliente.VerticalAlignment = Element.ALIGN_TOP;
                tableSelloCliente.AddCell(cel1SelloCliente);

                iTextSharp.text.pdf.PdfPCell cel2SelloFechaRecibido = new iTextSharp.text.pdf.PdfPCell(new Phrase("Fecha de recibido:", fontTitleFactura));

                cel2SelloFechaRecibido.Colspan = 1;
                cel2SelloFechaRecibido.Padding = 3;
                cel2SelloFechaRecibido.Border = 0;
                cel2SelloFechaRecibido.HorizontalAlignment = Element.ALIGN_LEFT;
                cel2SelloFechaRecibido.VerticalAlignment = Element.ALIGN_TOP;
                tableSelloCliente.AddCell(cel2SelloFechaRecibido);

                iTextSharp.text.pdf.PdfPCell cel2SelloNombre = new iTextSharp.text.pdf.PdfPCell(new Phrase("Nombre:", fontTitleFactura));

                cel2SelloNombre.Colspan = 1;
                cel2SelloNombre.Padding = 3;
                cel2SelloNombre.Border = 0;
                cel2SelloNombre.HorizontalAlignment = Element.ALIGN_LEFT;
                cel2SelloNombre.VerticalAlignment = Element.ALIGN_TOP;
                tableSelloCliente.AddCell(cel2SelloNombre);

                iTextSharp.text.pdf.PdfPCell cel2SelloId = new iTextSharp.text.pdf.PdfPCell(new Phrase("Identificacion:", fontTitleFactura));

                cel2SelloId.Colspan = 1;
                cel2SelloId.Padding = 3;
                cel2SelloId.Border = 0;
                cel2SelloId.HorizontalAlignment = Element.ALIGN_LEFT;
                cel2SelloId.VerticalAlignment = Element.ALIGN_TOP;
                tableSelloCliente.AddCell(cel2SelloId);

                iTextSharp.text.pdf.PdfPCell cel2SelloFirma = new iTextSharp.text.pdf.PdfPCell(new Phrase("Firma:\n", fontTitleFactura));

                cel2SelloFirma.Colspan = 1;
                cel2SelloFirma.Padding = 3;
                cel2SelloFirma.Border = 0;
                cel2SelloFirma.HorizontalAlignment = Element.ALIGN_LEFT;
                cel2SelloFirma.VerticalAlignment = Element.ALIGN_TOP;
                tableSelloCliente.AddCell(cel2SelloFirma);

                iTextSharp.text.pdf.PdfPCell cel3SelloCliente = new iTextSharp.text.pdf.PdfPCell(new Phrase("______________________________________________________\n\n" +
                    "RECIBIDO CONFORME FIRMA Y SELLO DEL CLIENTE", fontTitleFactura));

                cel3SelloCliente.Colspan = 1;
                cel3SelloCliente.Padding = 3;
                cel3SelloCliente.Border = 0;
                cel3SelloCliente.HorizontalAlignment = Element.ALIGN_CENTER;
                cel3SelloCliente.VerticalAlignment = Element.ALIGN_TOP;
                tableSelloCliente.AddCell(cel3SelloCliente);

                iTextSharp.text.pdf.PdfPCell _celSelloCliente = new iTextSharp.text.pdf.PdfPCell(tableSelloCliente);
                tableFirmas.AddCell(_celSelloCliente);
                #endregion

                #region Exit

            //agregamos todas las tablas al documento 

                document.Add(Header);
                document.Add(divEspacio);
                document.Add(tableFacturar);
                document.Add(divEspacio);
                document.Add(tableDetalles2);
                document.Add(divEspacio2);
                document.Add(divEspacio2);
                document.Add(tableDetalles);
                document.Add(divEspacio2);
                document.Add(tableTituloUnidades);
                document.Add(tableUnidades);
                document.Add(divEspacio2);
                document.Add(divEspacio2);
                document.Add(tableTotalUnidades);
                document.Add(divEspacio2);
                document.Add(divEspacio2);
                document.Add(tableValorLetras);
                document.Add(divEspacio2);
                document.Add(divEspacio2);
                document.Add(tableObservaciones);
                document.Add(divEspacio2);
                document.Add(divEspacio2);
                document.Add(tableResolucion);
                document.Add(divEspacio2);
                document.Add(tableTrasnportador);
                document.Add(divEspacio2);
                document.Add(divEspacio2);
                document.Add(tableFirmas);

                /*PIE DE PAGINA*/
                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();
                return true;
                #endregion
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return false;
            }         
        }
        #endregion

        #region Nota Credito Cementos y calizas de la paz

        public bool NotaCreditoCementos(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {
            NomArchivo = string.Empty;
            string Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
            string RutaImg = $@"{AppDomain.CurrentDomain.BaseDirectory}\logo_cementos.png";
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            NomArchivo = NombreInvoice + ".pdf";

            //strError += "Lego";
            //NomArchivo = string.Empty;
            //string Ruta = HttpContext.Current.Server.MapPath("~/PDF/" + NIT + "/");
            //string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            //string RutaImg = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";
            //NomArchivo = NombreInvoice + ".pdf";
            //strError += "inicia Documento pdf\n";
            try
            {
                #region Head
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.

                Document document = new Document(iTextSharp.text.PageSize.LETTER, 20f, 10f, 30f, 30f);

                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;

                // step 3: we open the document     
                document.Open();

                PdfDiv divEspacio = new PdfDiv();
                divEspacio.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio.Height = 10;
                divEspacio.Width = 130;

                PdfDiv divEspacio2 = new PdfDiv();
                divEspacio2.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio2.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio2.Height = 2;
                divEspacio2.Width = 130;

                //FUENTES----------------------------------------------------------------------------------------------------------
                iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle2 = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustom = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitleFecha = FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontDMA = FontFactory.GetFont(FontFactory.HELVETICA, 6, BaseColor.GRAY);
                iTextSharp.text.Font fontDetalleFecha = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 8, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitleFactura = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                //FUENTES---------------------------------------------------------------------------------------------------------


                /////saltos de trabla 
                PdfPTable salto = new PdfPTable(1);
                salto.WidthPercentage = 100;
                PdfPCell vacio = new PdfPCell() { MinimumHeight = 2, Border = 0, };
                salto.AddCell(vacio);
                ////-------------------------------------
                ///

                System.Drawing.Image logo = null;
                var requestLogo = WebRequest.Create(RutaImg);

                using (var responseLogo = requestLogo.GetResponse())
                using (var streamLogo = responseLogo.GetResponseStream())
                {
                    logo = Bitmap.FromStream(streamLogo);
                }

                PdfDiv divLogo = new PdfDiv();
                divLogo.Float = iTextSharp.text.pdf.PdfDiv.FloatType.LEFT;
                divLogo.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divLogo.BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID;
                divLogo.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                divLogo.Height = 60;
                divLogo.Width = 145;
                //divLogo.BackgroundColor = BaseColor.LIGHT_GRAY;
                iTextSharp.text.Image ImgLogo = iTextSharp.text.Image.GetInstance(logo, BaseColor.WHITE);
                divLogo.BackgroundImage = ImgLogo;
                //---------------------------------------------------------------------------------------------------------
                PdfDiv divAcercade = new PdfDiv();
                divAcercade.Float = iTextSharp.text.pdf.PdfDiv.FloatType.LEFT;
                divAcercade.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divAcercade.BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID;
                divAcercade.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divAcercade.BackgroundColor = BaseColor.LIGHT_GRAY;
                //divAcercade.PaddingLeft = 5;
                //divAcercade.PaddingRight = 10;
                divAcercade.Width = 140;


                iTextSharp.text.Image QRPdf = iTextSharp.text.Image.GetInstance(QRInvoice, BaseColor.WHITE);
                QRPdf.ScaleAbsolute(100, 100);
                QRPdf.Border = 0;

                #endregion

                #region Header

                PdfPTable header = new PdfPTable(new float[] { 0.30f, 0.50f, 0.20f, });
                header.WidthPercentage = 100;
                //agregamos logo
                PdfPCell cell_logo = new PdfPCell() { Border = 0, };
                cell_logo.AddElement(ImgLogo);

                //agregamos cufe y numero de factura
                PdfPCell cell_factura = new PdfPCell() { Border = 0, };

                ///agregamos Qr
                PdfPCell cell_Qr = new PdfPCell() { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, };
                cell_Qr.AddElement(QRPdf);

                header.AddCell(cell_logo);
                header.AddCell(cell_factura);
                header.AddCell(cell_Qr);

                PdfPTable encabezado = new PdfPTable(1);
                encabezado.WidthPercentage = 100;
                PdfPCell cabecera = new PdfPCell() { Border = 0, };

                PdfPTable Advanced = new PdfPTable(2);
                Advanced.WidthPercentage = 100;

                PdfPCell info_inicio = new PdfPCell();
                //agregamos el nimbre de la empresa
                PdfPTable nombre = new PdfPTable(1);
                nombre.WidthPercentage = 100;
                PdfPCell nombre_em = new PdfPCell(new Phrase(DsInvoiceAR.Tables["Company"].Rows[0]["Name"].ToString(), fontTitle2)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP, MinimumHeight = 50, };
                nombre.AddCell(nombre_em);
                info_inicio.AddElement(nombre);

                //agregamos el codigo postal de la empresa
                PdfPTable c_postal = new PdfPTable(1);
                c_postal.WidthPercentage = 100;
                PdfPCell postal_ = new PdfPCell(new Phrase("C. Postal: " + DsInvoiceAR.Tables["Company"].Rows[0]["Country"].ToString(), fontTitle2)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP, };
                c_postal.AddCell(postal_);
                info_inicio.AddElement(c_postal);

                //agregamos el codigo postal de la empresa
                PdfPTable pais = new PdfPTable(1);
                pais.WidthPercentage = 100;
                PdfPCell pais_ = new PdfPCell(new Phrase("Pais: " + DsInvoiceAR.Tables["Company"].Rows[0]["Country"].ToString(), fontTitle2)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP, };
                pais.AddCell(pais_);
                info_inicio.AddElement(pais);


                //agregamos el telefono postal de la empresa
                PdfPTable telefono = new PdfPTable(1);
                telefono.WidthPercentage = 100;
                PdfPCell telefono_ = new PdfPCell(new Phrase("Telefono: " + DsInvoiceAR.Tables["Company"].Rows[0]["PhoneNum"].ToString(), fontTitle2)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP, };
                telefono.AddCell(telefono_);
                info_inicio.AddElement(telefono);


                //agregamos el fical postal de la empresa
                PdfPTable fiscal = new PdfPTable(1);
                fiscal.WidthPercentage = 100;
                PdfPCell fiscal_ = new PdfPCell(new Phrase("Nr Fiscal: " + DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"].ToString(), fontTitle2)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP, };
                fiscal.AddCell(fiscal_);
                info_inicio.AddElement(fiscal);

                Advanced.AddCell(info_inicio);

                PdfPCell info_inicio2 = new PdfPCell() { Border = 0, };

                PdfPTable Adquiriente = new PdfPTable(1);
                Adquiriente.WidthPercentage = 100;
                PdfPCell div = new PdfPCell(new Phrase(DsInvoiceAR.Tables["Customer"].Rows[0]["Name"].ToString(), fontTitle));
                Adquiriente.AddCell(div);
                info_inicio2.AddElement(Adquiriente);

                string infoAdquiriente = "Dirección: " + DsInvoiceAR.Tables["Customer"].Rows[0]["Address1"].ToString() + "\nCiudad: " + DsInvoiceAR.Tables["Customer"].Rows[0]["City"].ToString() + "\nTelefono: " + DsInvoiceAR.Tables["Customer"].Rows[0]["PhoneNum"].ToString();
                PdfPTable tablaInfoAdquiriente = new PdfPTable(1);
                tablaInfoAdquiriente.WidthPercentage = 100;
                PdfPCell cell_InfoAdquiriente = new PdfPCell(new Phrase(infoAdquiriente, fontTitle2)) { MinimumHeight = 35, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP, };
                tablaInfoAdquiriente.AddCell(cell_InfoAdquiriente);
                info_inicio2.AddElement(tablaInfoAdquiriente);

                PdfPTable tablaEsp = new PdfPTable(1);
                tablaEsp.WidthPercentage = 100;
                PdfPCell cell_esp = new PdfPCell() { Border = 0, MinimumHeight = 8, };
                tablaEsp.AddCell(cell_esp);
                info_inicio2.AddElement(tablaEsp);

                PdfPTable nr = new PdfPTable(1);
                nr.WidthPercentage = 100;
                PdfPCell nr_r = new PdfPCell(new Phrase("Nŗ Fiscal: " + DsInvoiceAR.Tables["Customer"].Rows[0]["ResaleID"].ToString(), fontTitle));
                nr.AddCell(nr_r);
                info_inicio2.AddElement(nr);

                Advanced.AddCell(info_inicio2);

                cabecera.AddElement(Advanced);

                encabezado.AddCell(cabecera);

                #endregion

                #region Body

                PdfPTable cabeceta_nota = new PdfPTable(new float[] { 0.35f, 0.15f, 0.10f, 0.18f, 0.35f });
                cabeceta_nota.WidthPercentage = 100;


                PdfPCell numerof = new PdfPCell() { };
                //agregamos el campo para el numero de la factura
                PdfPTable _numerof_ = new PdfPTable(1);
                _numerof_.WidthPercentage = 100;
                PdfPCell numerof_ = new PdfPCell(new Phrase("Nota de Crédito", fontTitle2)) { HorizontalAlignment = Element.ALIGN_CENTER, Border = 0, };
                _numerof_.AddCell(numerof_);
                numerof.AddElement(_numerof_);
                //agregamos el numeo de la factura
                PdfPTable _numerof_r = new PdfPTable(1);
                _numerof_r.WidthPercentage = 100;
                PdfPCell numerof_r = new PdfPCell(new Phrase((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"], fontTitle2)) { HorizontalAlignment = Element.ALIGN_CENTER, Border = 0, };
                _numerof_r.AddCell(numerof_r);
                numerof.AddElement(_numerof_r);


                PdfPCell fecha = new PdfPCell();
                //agregamos el campo para el fehca de la factura
                PdfPTable _fecha_ = new PdfPTable(1);
                _fecha_.WidthPercentage = 100;
                PdfPCell fecha_ = new PdfPCell(new Phrase("Fecha", fontTitle2)) { HorizontalAlignment = Element.ALIGN_CENTER, Border = 0, };
                _fecha_.AddCell(fecha_);
                fecha.AddElement(_fecha_);
                //agregamos la fecha de la factura
                PdfPTable _fecha_r = new PdfPTable(1);
                _fecha_r.WidthPercentage = 100;
                PdfPCell fecha_r = new PdfPCell(new Phrase(DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).ToString("dd/MM/yyyy"), fontTitle2)) { HorizontalAlignment = Element.ALIGN_CENTER, Border = 0, };
                _fecha_r.AddCell(fecha_r);
                fecha.AddElement(_fecha_r);

                PdfPCell moneda = new PdfPCell();
                //agregamos el campo para la moneda de la factura
                PdfPTable _Moneda_ = new PdfPTable(1);
                _Moneda_.WidthPercentage = 100;
                PdfPCell Moneda_ = new PdfPCell(new Phrase("Moneda", fontTitle2)) { HorizontalAlignment = Element.ALIGN_CENTER, Border = 0, };
                _Moneda_.AddCell(Moneda_);
                moneda.AddElement(_Moneda_);
                //agregamos la moneda de la factura
                PdfPTable _Moneda_r = new PdfPTable(1);
                _Moneda_r.WidthPercentage = 100;
                PdfPCell Moneda_r = new PdfPCell(new Phrase((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["CurrencyCode"], fontTitle2)) { HorizontalAlignment = Element.ALIGN_CENTER, Border = 0, };
                _Moneda_r.AddCell(Moneda_r);
                moneda.AddElement(_Moneda_r);

                PdfPCell sobre = new PdfPCell();
                //agregamos el campo para la moneda de la factura
                PdfPTable _sobre_ = new PdfPTable(1);
                _sobre_.WidthPercentage = 100;
                PdfPCell sobre_ = new PdfPCell(new Phrase("Sobre Factura", fontTitle2)) { HorizontalAlignment = Element.ALIGN_CENTER, Border = 0, };
                _sobre_.AddCell(sobre_);
                sobre.AddElement(_sobre_);
                //agregamos la moneda de la factura
                PdfPTable _sobre_r = new PdfPTable(1);
                _sobre_r.WidthPercentage = 100;
                PdfPCell sobre_r = new PdfPCell(new Phrase((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceRef"], fontTitle2)) { HorizontalAlignment = Element.ALIGN_CENTER, Border = 0, };
                _sobre_r.AddCell(sobre_r);
                sobre.AddElement(_sobre_r);


                PdfPCell paginas = new PdfPCell() { Border = 0, };
                //agregamos el campo para la moneda de la factura
                PdfPTable _paginas_ = new PdfPTable(1);
                _paginas_.WidthPercentage = 100;
                PdfPCell paginas_ = new PdfPCell(new Phrase("pagina " + document.PageNumber, fontTitle2)) { HorizontalAlignment = Element.ALIGN_CENTER, };
                _paginas_.AddCell(paginas_);
                paginas.AddElement(_paginas_);
                //agregamos la moneda de la factura
                PdfPTable _paginas_r = new PdfPTable(1);
                _paginas_r.WidthPercentage = 100;
                PdfPCell paginas_r = new PdfPCell(new Phrase("COPIA\nCONTRAVALOR", fontTitle2)) { HorizontalAlignment = Element.ALIGN_CENTER, Border = 0, };
                _paginas_r.AddCell(paginas_r);
                paginas.AddElement(_paginas_r);

                cabeceta_nota.AddCell(numerof);
                cabeceta_nota.AddCell(fecha);
                cabeceta_nota.AddCell(moneda);
                cabeceta_nota.AddCell(sobre);
                cabeceta_nota.AddCell(paginas);


                #endregion

                #region unidades

                //Dimenciones.
                float[] DimencionUnidades = new float[4];
                DimencionUnidades[0] = 0.10f;//L
                DimencionUnidades[1] = 0.60f;//Codigo
                DimencionUnidades[2] = 0.15f;//Descripcion
                DimencionUnidades[3] = 0.15f;//Unidad

                PdfPTable tableTituloUnidades = new PdfPTable(DimencionUnidades);
                tableTituloUnidades.WidthPercentage = 100;

                PdfPCell fecha_d = new PdfPCell(new Phrase("Fecha", fontTitle2)) { HorizontalAlignment = Element.ALIGN_CENTER, };
                PdfPCell Descripción = new PdfPCell(new Phrase("Descripción", fontTitle2)) { HorizontalAlignment = Element.ALIGN_LEFT, };
                PdfPCell Total = new PdfPCell(new Phrase("Total", fontTitle2)) { HorizontalAlignment = Element.ALIGN_RIGHT };
                PdfPCell Saldo = new PdfPCell(new Phrase("Saldo", fontTitle2)) { HorizontalAlignment = Element.ALIGN_RIGHT };

                tableTituloUnidades.AddCell(fecha_d);
                tableTituloUnidades.AddCell(Descripción);
                tableTituloUnidades.AddCell(Total);
                tableTituloUnidades.AddCell(Saldo);


                PdfPTable tableUnidades = new PdfPTable(4);
                tableUnidades.WidthPercentage = 100;
                tableUnidades.SetWidths(DimencionUnidades);
                decimal totalDescuento = 0;
                foreach (DataRow InvoiceLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
                {
                    if (!AddUnidadesNotaCreditoCementos(InvoiceLine, ref tableUnidades, fontCustom, DsInvoiceAR))
                        return false;
                    totalDescuento += decimal.Parse((string)InvoiceLine["DspDocLessDiscount"]);
                }

                int numAdLineas = 10 - DsInvoiceAR.Tables["InvcDtl"].Rows.Count;
                PdfPTable tableEspacioUnidades = new PdfPTable(4);
                tableEspacioUnidades.WidthPercentage = 100;
                tableEspacioUnidades.SetWidths(DimencionUnidades);

                PdfPTable tableLineaFinal = new PdfPTable(4);
                tableLineaFinal.WidthPercentage = 100;
                tableLineaFinal.SetWidths(DimencionUnidades);
                iTextSharp.text.pdf.PdfPCell LineaFinal = new iTextSharp.text.pdf.PdfPCell();
                LineaFinal.Colspan = 4;
                LineaFinal.Padding = 3;
                LineaFinal.BorderColorBottom = BaseColor.WHITE;
                LineaFinal.HorizontalAlignment = Element.ALIGN_CENTER;
                LineaFinal.VerticalAlignment = Element.ALIGN_TOP;
                tableLineaFinal.AddCell(LineaFinal);

                #endregion

                #region Footer

                PdfPTable total = new PdfPTable(new float[] { 0.10f, 0.60f, 0.15f, 0.15f, });
                total.WidthPercentage = 100;

                PdfPCell vacio1 = new PdfPCell(new Phrase("", fontTitle2)) { HorizontalAlignment = Element.ALIGN_CENTER, };
                PdfPCell vacio2 = new PdfPCell(new Phrase("", fontTitle2)) { HorizontalAlignment = Element.ALIGN_LEFT, };
                PdfPCell vacio3 = new PdfPCell(new Phrase("Total", fontTitle2)) { HorizontalAlignment = Element.ALIGN_RIGHT, };
                PdfPCell vacio4 = new PdfPCell(new Phrase(decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"]).ToString("C2"), fontTitle2)) { HorizontalAlignment = Element.ALIGN_RIGHT, };


                total.AddCell(vacio1);
                total.AddCell(vacio2);
                total.AddCell(vacio3);
                total.AddCell(vacio4);

                PdfPTable pie = new PdfPTable(2);
                pie.WidthPercentage = 100;

                PdfPCell porsentuales = new PdfPCell() { MinimumHeight = 30, Padding = 0, };

                PdfPTable insidencia = new PdfPTable(3);
                insidencia.WidthPercentage = 100;
                //agregamos el 19%
                PdfPCell porcentaje = new PdfPCell(new Phrase("%\n19",
                    fontTitle2))
                { HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_BOTTOM, MinimumHeight = 35, };
                insidencia.AddCell(porcentaje);
                //agregamos insidencia
                PdfPCell insi = new PdfPCell(new Phrase("insidencia\n" + decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"]).ToString("C2"),
                    fontTitle2))
                { HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_BOTTOM };
                insidencia.AddCell(insi);
                //agregamos iva
                PdfPCell iva = new PdfPCell(new Phrase("iva",
                    fontTitle2))
                { HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_BOTTOM, MinimumHeight = 20, };
                insidencia.AddCell(iva);
                porsentuales.AddElement(insidencia);


                PdfPTable insidencia2 = new PdfPTable(3);
                insidencia2.WidthPercentage = 100;
                //agregamos el 19%
                PdfPCell porsentaje2 = new PdfPCell(new Phrase("",
                    fontTitle2))
                { HorizontalAlignment = Element.ALIGN_RIGHT };
                insidencia2.AddCell(porsentaje2);
                //agregamos insidencia
                PdfPCell insi2 = new PdfPCell(new Phrase(decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"]).ToString("C2"),
                    fontTitle2))
                { HorizontalAlignment = Element.ALIGN_RIGHT };
                insidencia2.AddCell(insi2);
                //agregamos iva
                PdfPCell iva2 = new PdfPCell(new Phrase(decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DocTaxAmt"]).ToString("C2"),
                    fontTitle2))
                { HorizontalAlignment = Element.ALIGN_RIGHT, MinimumHeight = 25, };
                insidencia2.AddCell(iva2);
                porsentuales.AddElement(insidencia2);

                PdfPCell funcionario = new PdfPCell() { };

                PdfPTable func = new PdfPTable(1);
                //func.WidthPercentage = 100;
                //agregamos el nombred el funcionario 
                PdfPCell funcionari = new PdfPCell(new Phrase("Funcionario",
                    fontTitle2))
                { HorizontalAlignment = Element.ALIGN_CENTER, MinimumHeight = 20, Padding = 3, };
                func.AddCell(funcionari);
                funcionario.AddElement(func);

                PdfPTable firma = new PdfPTable(1);
                //func.WidthPercentage = 100;
                //agregamos el la firma 
                PdfPCell fima_ = new PdfPCell(new Phrase("_________________________________________________________" +
                                                        "Firma/Signature",
                                                        fontTitle2))
                { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_BOTTOM, MinimumHeight = 30, Padding = 3, };
                firma.AddCell(fima_);
                funcionario.AddElement(firma);


                pie.AddCell(porsentuales);
                pie.AddCell(funcionario);

                PdfPTable procesado = new PdfPTable(1);
                procesado.WidthPercentage = 100;
                PdfPCell procesad = new PdfPCell(new Phrase("Procesado por computadora", fontTitle2)) { HorizontalAlignment = Element.ALIGN_RIGHT, Border = 0, };
                procesado.AddCell(procesad);

                #endregion

                #region Exit

                document.Add(header);
                document.Add(encabezado);
                document.Add(salto);
                document.Add(cabeceta_nota);
                document.Add(salto);
                document.Add(salto);
                document.Add(tableTituloUnidades);
                document.Add(tableUnidades);
                document.Add(tableEspacioUnidades);
                document.Add(tableLineaFinal);
                document.Add(total);
                document.Add(pie);
                document.Add(procesado);

                //document.Add(divTextBancoCheque);
                /*PIE DE PAGINA*/
                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();
                RutaPdf = NomArchivo;
                return true;
                #endregion

            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.Message;
                return false;
            }
        }
        #endregion

        #region Tipo de Factura

        private string TipoDeFactura(DataSet DsInvoiceAR)
        {
            string titulo = "";
            if (DsInvoiceAR.Tables["InvoiceHead"].Rows[0]["InvoiceType"].ToString().Equals("InvoiceType"))
                titulo = "FACTURA DE VENTA";
            if (DsInvoiceAR.Tables["InvoiceHead"].Rows[0]["InvoiceType"].ToString().Equals("CreditNoteType"))
                titulo = "NOTA CREDITO";
            if (DsInvoiceAR.Tables["InvoiceHead"].Rows[0]["InvoiceType"].ToString().Equals("DebitNoteType"))
                titulo = "NOTA DEBITO";
            return titulo;
        }
        #endregion
    }
}
