﻿namespace RulesServicesDIAN2.Adquiriente
{
    using iTextSharp.text;
    using iTextSharp.text.pdf;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Diagnostics;
    using System.Drawing;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.NetworkInformation;
    using System.Text;
    using System.Threading.Tasks;
    using System.Web;
    using System.Windows.Forms;

    public partial class pdfEstandarAR
    {
        #region Factura de ventas

        

        #region configuracion de pajina

        float dimxronelly = 540f, dimyronelly = 7f;

        protected void AddPageContinueronelly(string rutaPDF, float posicionX, float posicionY, iTextSharp.text.Font font)
        {
            byte[] bytes = File.ReadAllBytes(rutaPDF);

            //iTextSharp.text.Font blackFont = FontFactory.GetFont("Arial", 12, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
            using (MemoryStream stream = new MemoryStream())
            {
                PdfReader reader = new PdfReader(bytes);
                using (PdfStamper stamper = new PdfStamper(reader, stream))
                {
                    int pages = reader.NumberOfPages;
                    string textContinue = "CONTINUES/CONTINUA...";
                    for (int i = 1; i <= pages; i++)
                    {
                        if (i == pages)
                            textContinue = string.Empty;

                        ColumnText.ShowTextAligned(
                            stamper.GetUnderContent(i), Element.ALIGN_CENTER, new Phrase($"{textContinue}", font), posicionX, posicionY, 0);
                    }
                }
                bytes = stream.ToArray();
            }
            File.WriteAllBytes(rutaPDF, bytes);
        }

        #endregion

        #region Lineas de Detalle
        private bool AddUnidadesRonelly(DataRow dataLine, ref PdfPTable table, iTextSharp.text.Font fontTitleFactura, DataSet dataSet)
        {
            try
            {
                //CODIGO
                strError += "Add InvoiceLine";
                iTextSharp.text.pdf.PdfPCell celValL = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    (string)dataLine["PartNum"], fontTitleFactura));
                celValL.Colspan = 1;
                celValL.Padding = 2;
                //celValL.Border = 0;
                //celTextL.BorderWidthRight = 1;
                //celValL.BorderWidthLeft = 1;
                celValL.HorizontalAlignment = Element.ALIGN_CENTER;
                celValL.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValL);
                strError += "InvoiceLine OK";

                //lote
                strError += "Add PartNum";
                iTextSharp.text.pdf.PdfPCell celValCodigo = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    decimal.Parse((string)dataLine["Number01"]).ToString("N0"), fontTitleFactura));
                celValCodigo.Colspan = 1;
                celValCodigo.Padding = 2;
                //celValCodigo.Border = 0;
                //celValCodigo.BorderWidthLeft = 1;
                //celDescripcion.BorderColorBottom = BaseColor.WHITE;
                celValCodigo.HorizontalAlignment = Element.ALIGN_CENTER;
                celValCodigo.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValCodigo);
                strError += "Add PartNum OK";

                //vence
                strError += "Add SellingShipQty";
                iTextSharp.text.pdf.PdfPCell celValDescripcion = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    DateTime.Parse((string)dataLine["Date01"]).ToString("dd/mm/yyyy"), fontTitleFactura));
                celValDescripcion.Colspan = 1;
                celValDescripcion.Padding = 2;
                //celValDescripcion.Border = 0;
                //celValCantidad.BorderWidthLeft = 1;
                celValDescripcion.HorizontalAlignment = Element.ALIGN_CENTER;
                celValDescripcion.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValDescripcion);
                strError += "Add SellingShipQty OK";

                //invima
                strError += "Add SalesUM";
                iTextSharp.text.pdf.PdfPCell celValUnidad = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    (string)dataLine["ShortChar01"].ToString(), fontTitleFactura));
                celValUnidad.Colspan = 1;
                celValUnidad.Padding = 2;
                //celValUnidad.Border = 0;
                //celValUnidad.BorderWidthLeft = 1;
                celValUnidad.HorizontalAlignment = Element.ALIGN_CENTER;
                celValUnidad.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValUnidad);
                strError += "Add SalesUM OK";

                //descripcion
                strError += "Add LineDesc";
                iTextSharp.text.pdf.PdfPCell celValCantidad = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    (string)dataLine["PartNumPartDescription"].ToString(), fontTitleFactura));
                celValCantidad.Colspan = 1;
                celValCantidad.Padding = 2;
                //celValCantidad.Border = 0;
                //celValDesc.BorderWidthLeft = 1;
                celValCantidad.HorizontalAlignment = Element.ALIGN_CENTER;
                celValCantidad.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValCantidad);
                strError += "Add LineDesc OK";

                //pedido
                strError += "Add LineDesc";
                iTextSharp.text.pdf.PdfPCell celValCantidadc = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    decimal.Parse((string)dataLine["Number02"]).ToString("N0"), fontTitleFactura));
                celValCantidadc.Colspan = 1;
                celValCantidadc.Padding = 2;
                //celValCantidadc.Border = 0;
                //celValDesc.BorderWidthLeft = 1;
                celValCantidadc.HorizontalAlignment = Element.ALIGN_CENTER;
                celValCantidadc.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValCantidadc);
                strError += "Add LineDesc OK";

                //despachado
                strError += "Add LineDesc";
                iTextSharp.text.pdf.PdfPCell celValCantidadv = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    decimal.Parse((string)dataLine["Number03"]).ToString("N0"), fontTitleFactura));
                celValCantidadv.Colspan = 1;
                celValCantidadv.Padding = 2;
                //celValCantidadv.Border = 0;
                //celValDesc.BorderWidthLeft = 1;
                celValCantidadv.HorizontalAlignment = Element.ALIGN_CENTER;
                celValCantidadv.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValCantidadv);
                strError += "Add LineDesc OK";

                //pendiente despachar 
                strError += "Add LineDesc";
                iTextSharp.text.pdf.PdfPCell celValCantidadb = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    decimal.Parse((string)dataLine["Number04"]).ToString("N0"), fontTitleFactura));
                celValCantidadb.Colspan = 1;
                celValCantidadb.Padding = 2;
                //celValCantidadb.Border = 0;
                //celValDesc.BorderWidthLeft = 1;
                celValCantidadb.HorizontalAlignment = Element.ALIGN_CENTER;
                celValCantidadb.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValCantidadb);
                strError += "Add LineDesc OK";

                //vr unitario
                strError += "Add LineDesc";
                iTextSharp.text.pdf.PdfPCell celValCantidadn = new iTextSharp.text.pdf.PdfPCell(new Phrase("$ "+
                    decimal.Parse((string)dataLine["UnitPrice"]).ToString("N0"), fontTitleFactura));
                celValCantidadn.Colspan = 1;
                celValCantidadn.Padding = 2;
                //celValCantidadn.Border = 0;
                //celValDesc.BorderWidthLeft = 1;
                celValCantidadn.HorizontalAlignment = Element.ALIGN_CENTER;
                celValCantidadn.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValCantidadn);
                strError += "Add LineDesc OK";

                //iva
                strError += "Add LineDesc";
                iTextSharp.text.pdf.PdfPCell celValCantidada = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    decimal.Parse((string)dataLine["DocExtPrice"]).ToString("N0"), fontTitleFactura));
                celValCantidada.Colspan = 1;
                celValCantidada.Padding = 2;
                //celValCantidada.Border = 0;
                //celValDesc.BorderWidthLeft = 1;
                celValCantidada.HorizontalAlignment = Element.ALIGN_CENTER;
                celValCantidada.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValCantidada);
                strError += "Add LineDesc OK";

                //valor total 
                strError += "Add LineDesc";
                iTextSharp.text.pdf.PdfPCell celValCantidadw = new iTextSharp.text.pdf.PdfPCell(new Phrase("$ " +
                    decimal.Parse((string)dataLine["DocExtPrice"]).ToString("N0"), fontTitleFactura));
                celValCantidadw.Colspan = 1;
                celValCantidadw.Padding = 2;
                //celValCantidadw.Border = 0;
                //celValDesc.BorderWidthLeft = 1;
                celValCantidadw.HorizontalAlignment = Element.ALIGN_CENTER;
                celValCantidadw.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValCantidadw);
                strError += "Add LineDesc OK";
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return false;
            }
        }
        #endregion

        public bool FacturaNacionalRonelly(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {
            //NomArchivo = string.Empty;
            //string Ruta = HttpContext.Current.Server.MapPath("~/PDF/" + NIT + "/");
            //string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            //string RutaImg = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";
            //NomArchivo = NombreInvoice + ".pdf";

            NomArchivo = string.Empty;
            string Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            string RutaImg = $@"{AppDomain.CurrentDomain.BaseDirectory}\LOGO_Ronelly_Store.PNG";
            NomArchivo = NombreInvoice + ".pdf";
            try
            {
                #region Head
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.
                Document document = new Document(iTextSharp.text.PageSize.LETTER);
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;

                // step 3: we open the document     
                document.Open();

                PdfDiv divEspacio = new PdfDiv();
                divEspacio.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio.Height = 10;
                divEspacio.Width = 130;

                PdfDiv divEspacio2 = new PdfDiv();
                divEspacio2.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio2.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio2.Height = 2;
                divEspacio2.Width = 130;

                //FUENTES----------------------------------------------------------------------------------------------------------
                iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle3 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 9, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle2 = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustom = FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitleFactura = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                //FUENTES---------------------------------------------------------------------------------------------------------

                RoundRectangle CelEventBorderRound = new RoundRectangle();


                System.Drawing.Image Logo;
                System.Drawing.Image logo2 = null;
                var request = WebRequest.Create(RutaImg);
                using (var response = request.GetResponse())
                using (var streamLogo2 = response.GetResponseStream())
                {
                    Logo = Bitmap.FromStream(streamLogo2);
                }
                iTextSharp.text.Image ImgLogo2 = iTextSharp.text.Image.GetInstance(Logo, BaseColor.WHITE);
                ImgLogo2.ScaleAbsolute(130, 130);


                iTextSharp.text.Image ImgQR = iTextSharp.text.Image.GetInstance(QRInvoice, BaseColor.WHITE);
                ImgQR.ScaleAbsolute(60, 60);

                #endregion

                PdfPTable espacio = new PdfPTable(1);
                espacio.WidthPercentage = 100;
                PdfPCell cell_espacio = new PdfPCell() { MinimumHeight = 5, Border = 0, };
                espacio.AddCell(cell_espacio);

                PdfPTable espacio2 = new PdfPTable(1);
                espacio2.WidthPercentage = 100;
                PdfPCell cell_espacio2 = new PdfPCell() { MinimumHeight = 50, Border = 0, };
                espacio2.AddCell(cell_espacio2);

                #region Header

                PdfPTable Header = new PdfPTable(new float[] { 0.25f, 0.4f, 0.12f, 0.23f, });
                Header.WidthPercentage = 100;

                PdfPCell cell_logo = new PdfPCell() { Border = 0 };
                cell_logo.AddElement(ImgLogo2);
                Header.AddCell(cell_logo);
                PdfPCell cell_info = new PdfPCell() { Border = 0 };
                Paragraph prgInfoEmpresa = new Paragraph("FACTURA DE VENTAS");
                Paragraph prgInfoEmpresa1 = new Paragraph($"{(string)DsInvoiceAR.Tables["Company"].Rows[0]["Address1"]}\n" +
                $"PBX: {(string)DsInvoiceAR.Tables["Company"].Rows[0]["PhoneNum"]} -  " +
                $"FAX: {(string)DsInvoiceAR.Tables["Company"].Rows[0]["FaxNum"]}\n" +
                $"{(string)DsInvoiceAR.Tables["Company"].Rows[0]["City"]} - " +
                $"{(string)DsInvoiceAR.Tables["Company"].Rows[0]["State"]}",
                FontFactory.GetFont(FontFactory.HELVETICA, 5.79f, iTextSharp.text.Font.NORMAL));
                prgInfoEmpresa.Alignment = Element.ALIGN_CENTER;
                prgInfoEmpresa1.Alignment = Element.ALIGN_CENTER;
                PdfPCell celTextInfoEmpresa2 = new PdfPCell()
                {
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    //Colspan = 2,
                    Border = 0,
                    Padding = 3
                };
                cell_info.AddElement(prgInfoEmpresa);
                cell_info.AddElement(prgInfoEmpresa1);
                Header.AddCell(cell_info);
                PdfPCell cell_Qr = new PdfPCell() { Border = 0 };
                cell_Qr.AddElement(ImgQR);
                Header.AddCell(cell_Qr);
                PdfPCell cell_factura = new PdfPCell() { Border = 0 };

                PdfPTable f = new PdfPTable(2);
                f.WidthPercentage = 100;
                PdfPCell fno = new PdfPCell(new Phrase("No.")) { Border = 0, };
                f.AddCell(fno);
                PdfPCell fnos = new PdfPCell(new Phrase("" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"], fontCustom));
                f.AddCell(fnos);
                cell_factura.AddElement(f);

                Header.AddCell(cell_factura);


                PdfPTable header_2 = new PdfPTable(new float[] { 0.25f, 0.48f, 0.25f });
                header_2.WidthPercentage = 100;

                PdfPCell cell_vendido = new PdfPCell(new Phrase("vendido as:", fontCustom)) { HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_BOTTOM, Border = 0, };
                header_2.AddCell(cell_vendido);

                PdfPCell cell_info2 = new PdfPCell() { Border = 0, };
                Paragraph prginfo1 = new Paragraph("SOMO AUTORRETENEDORES", fontCustom);
                Paragraph prginfo2 = new Paragraph("NO SOMO GRANDES CONTRIBUYENTES Y APARTIR DEL 01 FEBRERO 2014", fontCustom);
                Paragraph prginfo3 = new Paragraph("IVA REGIMEN COMUN", fontCustom);
                prginfo1.Alignment = Element.ALIGN_LEFT;
                prginfo2.Alignment = Element.ALIGN_LEFT;
                prginfo3.Alignment = Element.ALIGN_LEFT;
                cell_info2.AddElement(prginfo1);
                cell_info2.AddElement(prginfo2);
                cell_info2.AddElement(prginfo3);
                header_2.AddCell(cell_info2);

                PdfPCell cell_info3 = new PdfPCell(new Phrase("Documento oficial de acreditacion de facturacion por computador " +
                    "18762002714520 de 27 de Marzo de 2017" +
                    "-Autoriza desde el 1 a 3000" +
                    "")) { };
                header_2.AddCell(cell_info3);



                #endregion

                #region Body

                PdfPTable Body = new PdfPTable(2);
                Body.WidthPercentage = 100;

                PdfPCell cell_body_fila1 = new PdfPCell() { MinimumHeight = 30, };
                cell_body_fila1.Border = PdfPCell.RIGHT_BORDER;

                PdfPTable nombre = new PdfPTable(1);
                nombre.WidthPercentage = 100;
                PdfPCell cell_nombre = new PdfPCell(new Phrase("" + DsInvoiceAR.Tables["Customer"].Rows[0]["Name"] + "\n\n")) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP };
                nombre.AddCell(cell_nombre);
                cell_body_fila1.AddElement(nombre);

                PdfPTable razon = new PdfPTable(1);
                razon.WidthPercentage = 100;
                PdfPCell cell_razon = new PdfPCell(new Phrase("Razón social:  ", fontCustom)) { Border = 0, VerticalAlignment = Element.ALIGN_BOTTOM, HorizontalAlignment = Element.ALIGN_LEFT };
                cell_razon.Border = PdfPCell.BOTTOM_BORDER;
                razon.AddCell(cell_razon);
                cell_body_fila1.AddElement(razon);

                PdfPTable t_info = new PdfPTable(1);
                t_info.WidthPercentage = 100;
                PdfPCell cell_t_info = new PdfPCell() { Border = 0, };
                Paragraph prgInf1 = new Paragraph(
                "Ni:         " + $" {(string)DsInvoiceAR.Tables["Customer"].Rows[0]["CustID"]}\n" +
                "Direccion:  " + $" {(string)DsInvoiceAR.Tables["Customer"].Rows[0]["Address1"]}\n" +
                "Telefono:   " + $" {(string)DsInvoiceAR.Tables["Customer"].Rows[0]["PhoneNum"]}\n" +
                "Ciudad:     " + $" {(string)DsInvoiceAR.Tables["Customer"].Rows[0]["City"]}\n" +
                "Despachada - " + "  Original", fontCustom);
                prgInf1.Alignment = Element.ALIGN_LEFT;

                cell_t_info.AddElement(prgInf1);
                t_info.AddCell(cell_t_info);
                cell_body_fila1.AddElement(t_info);

                Body.AddCell(cell_body_fila1);

                PdfPCell cell_body_fila2 = new PdfPCell() { Border = 0, };

                PdfPTable fechas = new PdfPTable(new float[] { 0.1f, 0.008f, 0.1f, 0.008f, 0.1f, 0.008f, 0.1f, 0.008f, 0.1f, });
                fechas.WidthPercentage = 100;

                PdfPCell f_factura = new PdfPCell(new Phrase("F. Factura", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, MinimumHeight = 15 };
                f_factura.Border = PdfPCell.BOTTOM_BORDER;
                fechas.AddCell(f_factura);

                PdfPCell ilio1 = new PdfPCell(new Phrase("", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, MinimumHeight = 10 };
                fechas.AddCell(ilio1);

                PdfPCell vencimiento = new PdfPCell(new Phrase("Vencimiento", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, MinimumHeight = 15 };
                vencimiento.Border = PdfPCell.BOTTOM_BORDER;
                fechas.AddCell(vencimiento);

                PdfPCell ilio2 = new PdfPCell(new Phrase("", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, MinimumHeight = 15 };
                fechas.AddCell(ilio2);

                PdfPCell ordenc = new PdfPCell(new Phrase("O. Compra", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, MinimumHeight = 15 };
                ordenc.Border = PdfPCell.BOTTOM_BORDER;
                fechas.AddCell(ordenc);

                PdfPCell ilio3 = new PdfPCell(new Phrase("", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, MinimumHeight = 10 };
                fechas.AddCell(ilio3);

                PdfPCell pedidoin = new PdfPCell(new Phrase("Nro. Pedido", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, MinimumHeight = 15 };
                pedidoin.Border = PdfPCell.BOTTOM_BORDER;
                fechas.AddCell(pedidoin);

                PdfPCell ilio4 = new PdfPCell(new Phrase("", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, MinimumHeight = 10 };
                fechas.AddCell(ilio4);

                PdfPCell pweb = new PdfPCell(new Phrase("PWEB", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, MinimumHeight = 15 };
                pweb.Border = PdfPCell.BOTTOM_BORDER;
                fechas.AddCell(pweb);

                cell_body_fila2.AddElement(fechas);

                PdfPTable f_fechasv = new PdfPTable(new float[] { 0.1f, 0.008f, 0.1f, 0.008f, 0.1f, 0.008f, 0.1f, 0.008f, 0.1f, });
                f_fechasv.WidthPercentage = 100;

                PdfPCell v_fechasv = new PdfPCell(new Phrase(DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).ToString("dd/mm/yyyy"), fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, MinimumHeight = 15 };
                f_fechasv.AddCell(v_fechasv);

                PdfPCell ilio1v = new PdfPCell(new Phrase("", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, MinimumHeight = 10 };
                f_fechasv.AddCell(ilio1v);

                PdfPCell vencimientov = new PdfPCell(new Phrase(DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"]).ToString("dd/mm/yyyy"), fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, MinimumHeight = 15 };
                f_fechasv.AddCell(vencimientov);

                PdfPCell ilio2v = new PdfPCell(new Phrase("", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, MinimumHeight = 15 };
                f_fechasv.AddCell(ilio2v);

                PdfPCell ordencv = new PdfPCell(new Phrase("" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["OrderNum"], fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, MinimumHeight = 15 };
                f_fechasv.AddCell(ordencv);

                PdfPCell ilio3v = new PdfPCell(new Phrase("", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, MinimumHeight = 10 };
                f_fechasv.AddCell(ilio3v);

                PdfPCell pedidoinv = new PdfPCell(new Phrase("" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number01"], fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, MinimumHeight = 15 };
                f_fechasv.AddCell(pedidoinv);

                PdfPCell ilio4v = new PdfPCell(new Phrase("", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, MinimumHeight = 10 };
                f_fechasv.AddCell(ilio4v);

                PdfPCell pwebv = new PdfPCell(new Phrase("" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar01"], fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, MinimumHeight = 15 };
                f_fechasv.AddCell(pwebv);

                cell_body_fila2.AddElement(f_fechasv);

                PdfPTable notas = new PdfPTable(1);
                notas.WidthPercentage = 100;
                PdfPCell cell_notas = new PdfPCell(new Phrase("\n Notas:", fontCustom)) { HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_BOTTOM, Border = 0, };
                notas.AddCell(cell_notas);
                cell_body_fila2.AddElement(notas);

                PdfPTable notasv = new PdfPTable(1);
                notasv.WidthPercentage = 100;
                PdfPCell cell_notasv = new PdfPCell(new Phrase("" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["Character01"] + "\n\n\n", fontCustom)) { HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_BOTTOM, Border = 0, };
                notasv.AddCell(cell_notasv);
                cell_body_fila2.AddElement(notasv);

                PdfPTable text = new PdfPTable(1);
                text.WidthPercentage = 100;
                PdfPCell cell_text = new PdfPCell(new Phrase($" CAMBIO DE PRECIO SINB PREVIO AVISO \n" +
                    $"lo invitamos a visitar nuestra NUEVA tienda virtual: \n" +
                    $" WWW.RONELLYSTORE.COM \n",
                    fontCustom))
                { HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_BOTTOM, Border = 0, BackgroundColor = BaseColor.LIGHT_GRAY, };
                text.AddCell(cell_text);
                cell_body_fila2.AddElement(text);

                Body.AddCell(cell_body_fila2);

                #endregion

                #region unidades

                PdfPTable t_unidad = new PdfPTable(new float[] { 0.03f, 0.04f, 0.043f, 0.03f, 0.13f, 0.09f, 0.04f, 0.04f, 0.02f, 0.04f, });
                t_unidad.WidthPercentage = 100;

                PdfPCell codigo = new PdfPCell(new Phrase("Codigo", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, };
                t_unidad.AddCell(codigo);
                PdfPCell lote = new PdfPCell(new Phrase("Nro. Lote", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, };
                t_unidad.AddCell(lote);
                PdfPCell vence = new PdfPCell(new Phrase("Vence", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, };
                t_unidad.AddCell(vence);
                PdfPCell invima = new PdfPCell(new Phrase("Invima", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, };
                t_unidad.AddCell(invima);
                PdfPCell descripcion = new PdfPCell(new Phrase("Descripción", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, };
                t_unidad.AddCell(descripcion);
                PdfPCell cantidad = new PdfPCell(new Phrase("", fontCustom)) { Padding = 0, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, };

                PdfPTable cantidad1 = new PdfPTable(1);
                cantidad1.WidthPercentage = 100;
                PdfPCell cell_catidad1 = new PdfPCell(new Phrase("Cantidad", fontCustom)) { Padding = 0, Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, MinimumHeight = 10, };
                cantidad1.AddCell(cell_catidad1);
                cantidad.AddElement(cantidad1);

                PdfPTable d = new PdfPTable(2);
                d.WidthPercentage = 100;
                PdfPCell cell_d = new PdfPCell(new Phrase("Pedida", fontCustom)) { Padding = 0, HorizontalAlignment = Element.ALIGN_CENTER, MinimumHeight = 10, };
                d.AddCell(cell_d);
                PdfPCell cell_s = new PdfPCell(new Phrase("Despachada", fontCustom)) { Padding = 0, HorizontalAlignment = Element.ALIGN_CENTER };
                d.AddCell(cell_s);
                cantidad.AddElement(d);
                t_unidad.AddCell(cantidad);

                PdfPCell p_despacho = new PdfPCell(new Phrase("Pendiente Despacho", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, };
                t_unidad.AddCell(p_despacho);
                PdfPCell vr_unitario = new PdfPCell(new Phrase("Vr. Unitario", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, };
                t_unidad.AddCell(vr_unitario);
                PdfPCell iva = new PdfPCell(new Phrase("% Iva", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, };
                t_unidad.AddCell(iva);
                PdfPCell valor = new PdfPCell(new Phrase("Valor Total", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, };
                t_unidad.AddCell(valor);


                PdfPTable tableTituloUnidades = new PdfPTable(11);
                //Dimenciones.   {0.03f, 0.04f, 0.03f, 0.03f, 0.13f, 0.09f, 0.04f, 0.04f, 0.02f, 0.04f, }
                float[] DimencionUnidades = new float[11];
                DimencionUnidades[0] = 0.03f;//CÓDIGO
                DimencionUnidades[1] = 0.04f;//DESCRIPCION
                DimencionUnidades[2] = 0.043f;//CANTIDAD
                DimencionUnidades[3] = 0.03f;//DTO
                DimencionUnidades[4] = 0.13f;//PRECIO
                DimencionUnidades[5] = 0.045f;//PRECIO
                DimencionUnidades[6] = 0.045f;//PRECIO
                DimencionUnidades[7] = 0.04f;//PRECIO
                DimencionUnidades[8] = 0.04f;//PRECIO
                DimencionUnidades[9] = 0.02f;//PRECIO
                DimencionUnidades[10] = 0.04f;//PRECIO


                PdfPTable tableUnidades = new PdfPTable(11);
                tableUnidades.WidthPercentage = 100;
                tableUnidades.SetWidths(DimencionUnidades);

                foreach (DataRow InvoiceLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
                {
                    if (!AddUnidadesRonelly(InvoiceLine, ref tableUnidades, fontCustom, DsInvoiceAR))
                        return false;
                }

                iTextSharp.text.pdf.PdfPCell LineaFinal = new iTextSharp.text.pdf.PdfPCell();
                LineaFinal.Colspan = 11;
                LineaFinal.Padding = 3;
                LineaFinal.Border = 1;
                LineaFinal.BorderColorBottom = BaseColor.WHITE;
                LineaFinal.HorizontalAlignment = Element.ALIGN_CENTER;
                LineaFinal.VerticalAlignment = Element.ALIGN_TOP;
                tableUnidades.AddCell(LineaFinal);


                #endregion

                #region Footer

                PdfPTable Footer = new PdfPTable(new float[] { 0.13F, 0.14F, 0.73F });
                Footer.WidthPercentage = 100;

                PdfPCell item = new PdfPCell(new Phrase("Total items: " +
                    decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number02"]).ToString("N0"),
                    fontCustom))
                { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_BOTTOM, };
                Footer.AddCell(item);
                PdfPCell uniodades = new PdfPCell(new Phrase("Total unidades: " +
                    decimal.Parse((string)DsInvoiceAR.Tables["InvcDtl"].Rows[0]["SellingShipQty"]).ToString("N0"),
                    fontCustom))
                { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_BOTTOM, };
                Footer.AddCell(uniodades);
                PdfPCell señore = new PdfPCell(new Phrase(
                    $"Señores:  " + DsInvoiceAR.Tables["Customer"].Rows[0]["Name"] + "\n" +
                    $"PAGUE A RONELLY S.A A LA FECHA DE VENCIMIENTO DE ESTA FACTURA LA SUMA DE:  " + "$ " +
                    decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number03"]).ToString("N0"),
                    fontCustom))
                { MinimumHeight = 20, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_BOTTOM, };
                Footer.AddCell(señore);

                PdfPTable Footer2 = new PdfPTable(new float[] { 0.3f, 0.11f, 0.0025f, 0.11f, 0.0025f, 0.11f, 0.0025f, 0.11f, 0.0025f, 0.11f });
                Footer2.WidthPercentage = 100;

                PdfPCell vendedor = new PdfPCell(new Phrase("Vendedor: " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["SalesRepName1"], fontCustom)) { HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP, Border = 0, };
                Footer2.AddCell(vendedor);

                PdfPCell subtota = new PdfPCell(new Phrase("Subtota", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, };
                Footer2.AddCell(subtota);

                PdfPCell linea1 = new PdfPCell(new Phrase("", fontCustom)) { Border = 0 };
                Footer2.AddCell(linea1);

                PdfPCell descuento = new PdfPCell(new Phrase("Descuento", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, };
                Footer2.AddCell(descuento);

                PdfPCell linea2 = new PdfPCell(new Phrase("", fontCustom)) { Border = 0 };
                Footer2.AddCell(linea2);

                PdfPCell abono = new PdfPCell(new Phrase("Abono", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, };
                Footer2.AddCell(abono);

                PdfPCell linea3 = new PdfPCell(new Phrase("", fontCustom)) { Border = 0 };
                Footer2.AddCell(linea3);

                PdfPCell ivaa = new PdfPCell(new Phrase("Iva", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, };
                Footer2.AddCell(ivaa);

                PdfPCell linea4 = new PdfPCell(new Phrase("", fontCustom)) { Border = 0 };
                Footer2.AddCell(linea4);

                PdfPCell total_p = new PdfPCell(new Phrase("Total A Pagar", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, };
                Footer2.AddCell(total_p);


                PdfPTable Footer3 = new PdfPTable(new float[] { 0.3f, 0.11f, 0.0025f, 0.11f, 0.0025f, 0.11f, 0.0025f, 0.11f, 0.0025f, 0.11f });
                Footer3.WidthPercentage = 100;

                PdfPCell vendedor3 = new PdfPCell(new Phrase("Elaboró: " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["SalesRepName1"], fontCustom)) { HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP, Border = 0, };
                Footer3.AddCell(vendedor3);

                PdfPCell subtota3 = new PdfPCell(new Phrase("$ " + decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubTotal"]).ToString("N0"), fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, };
                Footer3.AddCell(subtota3);

                Footer3.AddCell(linea1);

                PdfPCell descuento3 = new PdfPCell(new Phrase("$ " + decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number04"]).ToString("N0"), fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, };
                Footer3.AddCell(descuento3);

                Footer3.AddCell(linea2);

                PdfPCell abono3 = new PdfPCell(new Phrase("$ " + decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number05"]).ToString("N0"), fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, };
                Footer3.AddCell(abono3);

                Footer3.AddCell(linea3);

                PdfPCell ivaa3 = new PdfPCell(new Phrase("$ " + decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DocTaxAmt"]).ToString("N0"), fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, };
                Footer3.AddCell(ivaa3);

                Footer3.AddCell(linea4);

                PdfPCell total_p3 = new PdfPCell(new Phrase("$ " + decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"]).ToString("N0"), fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, };
                Footer3.AddCell(total_p3);
            
                PdfPTable Footer4 = new PdfPTable(new float[] { 0.3f, 0.001f, 0.5f, 0.001f, 0.5f, });
                Footer4.WidthPercentage = 100;

                PdfPCell firma = new PdfPCell(new Phrase(
                    $"Firma del \n Vendedor: ________________________\n\n\n\n\n\n\n" +
                    $"\n                     COPIA"
                    , fontCustom))
                { Border = 0, };

                Footer4.AddCell(firma);

                PdfPCell l3 = new PdfPCell() { Border=0,};
                Footer4.AddCell(l3);

                PdfPCell no = new PdfPCell(new Phrase("")) { Border = 0, };

                PdfPTable leyenda = new PdfPTable(new float[]{ 1 });
                PdfPCell cell_leyenda = new PdfPCell(new Phrase (""));
                leyenda.AddCell(cell_leyenda);
                no.AddElement(leyenda);

                PdfPTable leyenda2 = new PdfPTable(new float[] { 1 });
                PdfPCell cell_leyenda2 = new PdfPCell(new Phrase($"La sioguiente factura de ventas se asimila en todo sus efectos a" +
                                                                $"cambio de conformidad con los articulos 774 modificado por 2008,621,671 y 672, siguientes" +
                                                                $" y subsiguientes y con codigo de comercio y el art 617 del estatuto tributario." 
                    ,fontCustom));
                leyenda2.AddCell(cell_leyenda2);
                no.AddElement(leyenda2);

                PdfPTable leyenda2v = new PdfPTable(new float[] { 1 });
                PdfPCell cell_leyenda2v = new PdfPCell(new Phrase($"\n\n\n\n NO NEGOCIABLE, PARA EFECTOS SOLO \n" +
                                                                $"CONTABLE Y/O TRIBUTARIOS",
                 fontCustom))
                { Border=0,};
                leyenda2v.AddCell(cell_leyenda2v);
                no.AddElement(leyenda2v);

                Footer4.AddCell(no);


                Footer4.AddCell(l3);

                PdfPCell acep = new PdfPCell(new Phrase("")) { Border=0,};

                PdfPTable ac = new PdfPTable(1);
                ac.WidthPercentage = 100;
                PdfPCell cell_ac = new PdfPCell(new Phrase("Acepto esta factura sin reparay declaro que he recibido las mercancias real y ficicamente ", fontCustom)) { };
                ac.AddCell(cell_ac);
                acep.AddElement(ac);

                PdfPTable tt = new PdfPTable(1);
                tt.WidthPercentage = 100;
                PdfPCell cell_tt = new PdfPCell(new Phrase($"\nNomber de quien resibe: ________________________\n" +
                                                           $"\nCedula: ________________ Firma: ________________\n" +
                                                           $"\nEntrega Fecha: _________________Hora: __________\n" +
                                                           $"\nSello: __________________________________\n\n\n",
                                                           fontCustom)) { };
                tt.AddCell(cell_tt);
                acep.AddElement(tt);

                Footer4.AddCell(acep);

                PdfPTable fin = new PdfPTable(new float[] {0.34f, 0.45f, 0.25f });
                fin.WidthPercentage = 100;
                PdfPCell cell_fin10 = new PdfPCell(new Phrase("tipo: "+" *"+ DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar02"] +"*  "+ "   Num: "+ " *" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number06"] +"*", fontCustom)) {Border=0,};
                fin.AddCell(cell_fin10);
                PdfPCell cell_fin1 = new PdfPCell() {Colspan=0, Border=0, };
                fin.AddCell(cell_fin1);
                PdfPCell cell_fin = new PdfPCell(new Phrase($"Correos sugeridos \n" +
                                                            $"emisionfe@ronelly.com \n" +
                                                            $"recepcion@ronelly.com \n",
                                                            fontCustom)) { Border=0,HorizontalAlignment=Element.ALIGN_RIGHT};
                fin.AddCell(cell_fin);


                #endregion


                #region Exit

                document.Add(Header);
                document.Add(espacio);
                document.Add(header_2);
                document.Add(espacio);
                document.Add(Body);
                document.Add(t_unidad);
                document.Add(tableTituloUnidades);
                document.Add(tableUnidades);
                document.Add(espacio2);
                document.Add(Footer);
                document.Add(espacio);
                document.Add(Footer2);
                document.Add(espacio);
                document.Add(Footer3);
                document.Add(espacio);
                document.Add(espacio);
                document.Add(espacio);
                document.Add(Footer4);
                document.Add(espacio);
                document.Add(fin);

                //document.Add(divTextBancoCheque);
                /*PIE DE PAGINA*/
                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();
                RutaPdf = NomArchivo;
                return true;
                #endregion
            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.Message;
                return false;
            }
            #endregion
        }
    }
}
