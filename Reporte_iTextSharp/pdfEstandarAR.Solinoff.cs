﻿namespace RulesServicesDIAN2.Adquiriente
{
    using iTextSharp.text;
    using iTextSharp.text.pdf;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Drawing;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.NetworkInformation;
    using System.Text;
    using System.Threading.Tasks;
    using System.Web;
    using System.Windows.Forms;

    public partial class pdfEstandarAR
    {
        #region Solinoff
        public bool FacturaNacionalSolinoff(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {
            bool esLocal = Helpers.Compartido.EsLocal;
            string Ruta = string.Empty;
            string RutaImg = string.Empty;
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            if (esLocal)
            {
                Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
                RutaImg = $@"{AppDomain.CurrentDomain.BaseDirectory}\Logo_800134773.png";
            }
            else
            {
                Ruta = HttpContext.Current.Server.MapPath("~/PDF/" + NIT + "/");
                RutaImg = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";
            }
            NomArchivo = string.Empty;
            NomArchivo = NombreInvoice + ".pdf";
            //string RutaBanner = "https://tinyurl.com/yb3ugbb5/";
            try
            {
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.
                //Document document = new Document(iTextSharp.text.PageSize.LETTER);
                Document document = new Document(iTextSharp.text.PageSize.LETTER, 20f, 15f, 250f, 287f);
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;
                //--- Agregando los elementos estaticos
                //Celda Logo
                System.Drawing.Image Logo;
                //System.Drawing.Image LogoBanner = null;

                var request = WebRequest.Create(RutaImg);

                using (var response = request.GetResponse())
                using (var stream = response.GetResponseStream())
                {
                    Logo = Bitmap.FromStream(stream);
                }

                PdfPTable Encabezado = Solinoff.FacturaNacional.Encabezado(DsInvoiceAR, Logo, CUFE);
                PdfPTable DatosCliente = Solinoff.FacturaNacional.DatosCliente(DsInvoiceAR, QRInvoice);
                PdfPTable PiePagina = Solinoff.FacturaNacional.PiePagina(DsInvoiceAR, Logo, ref strError);
                PdfPTable DetalleHead = Solinoff.FacturaNacional.DetalleHead();

                writer.PageEvent = new Helpers.Solinoff.EventPageSolinoff(Encabezado, DatosCliente, DetalleHead, PiePagina);

                // step 3: we open the document
                document.Open();

                //---- Agregando elementos
                document.Add(Solinoff.FacturaNacional.Detalles(DsInvoiceAR, ref strError));
                document.Add(separator);
                document.Add(separator);

                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();
                Helpers.Simex.AddPageNumberPagToSimexExp($"{Ruta}{NomArchivo}", dimxSimex, dimYSimex + 10, Helpers.Fuentes.SimexLeyenda);
                RutaPdf = NomArchivo;
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("1. Error al crear PDF: " + ex);
                return false;
            }
        }
        public bool NotaCreditoSolinoff(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {
            bool esLocal = Helpers.Compartido.EsLocal;
            string Ruta = string.Empty;
            string RutaImg = string.Empty;
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            if (esLocal)
            {
                Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
                RutaImg = $@"{AppDomain.CurrentDomain.BaseDirectory}\Logo_800134773.png";
            }
            else
            {
                Ruta = HttpContext.Current.Server.MapPath("~/PDF/" + NIT + "/");
                RutaImg = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";
            }
            NomArchivo = string.Empty;
            NomArchivo = NombreInvoice + ".pdf";
            //string RutaBanner = "https://tinyurl.com/yb3ugbb5/";
            try
            {
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.
                //Document document = new Document(iTextSharp.text.PageSize.LETTER);
                Document document = new Document(iTextSharp.text.PageSize.LETTER, 20f, 15f, 250f, 287f);
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;
                //--- Agregando los elementos estaticos
                //Celda Logo
                System.Drawing.Image Logo;
                //System.Drawing.Image LogoBanner = null;

                var request = WebRequest.Create(RutaImg);

                using (var response = request.GetResponse())
                using (var stream = response.GetResponseStream())
                {
                    Logo = Bitmap.FromStream(stream);
                }

                PdfPTable Encabezado = Solinoff.NotaCredito.Encabezado(DsInvoiceAR, Logo, CUFE);
                PdfPTable DatosCliente = Solinoff.NotaCredito.DatosCliente(DsInvoiceAR, QRInvoice);
                PdfPTable PiePagina = Solinoff.NotaCredito.PiePagina(DsInvoiceAR, Logo, ref strError);
                PdfPTable DetalleHead = Solinoff.NotaCredito.DetalleHead();

                writer.PageEvent = new Helpers.Solinoff.EventPageSolinoff(Encabezado, DatosCliente, DetalleHead, PiePagina);

                // step 3: we open the document
                document.Open();

                //---- Agregando elementos
                document.Add(Solinoff.NotaCredito.Detalles(DsInvoiceAR, ref strError));
                document.Add(separator);
                document.Add(separator);

                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();
                Helpers.Simex.AddPageNumberPagToSimexExp($"{Ruta}{NomArchivo}", dimxSimex, dimYSimex + 10, Helpers.Fuentes.SimexLeyenda);
                RutaPdf = NomArchivo;
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("1. Error al crear PDF: " + ex);
                return false;
            }
        }
        public class Solinoff
        {
            public class FacturaNacional
            {
                public static PdfPTable Encabezado(DataSet ds, System.Drawing.Image Logo, string CUFE)
                {
                    return Helpers.Solinoff.Encabezado(ds, Logo, CUFE, Helpers.Compartido.TipoDocumento.FacturaNacional);
                }
                public static PdfPTable DatosCliente(DataSet ds, System.Drawing.Image QRInvoice)
                {
                    return Helpers.Plantilla_6.DatosCliente(ds, QRInvoice);
                }
                public static PdfPTable DetalleHead()
                {
                    return Helpers.Plantilla_6.DetalleHead();
                }
                public static PdfPTable Detalles(DataSet ds, ref string strError)
                {
                    return Helpers.Plantilla_6.Detalles(ds, ref strError);
                }
                public static PdfPTable PiePagina(DataSet ds, System.Drawing.Image Logo, ref string strError)
                {
                    Helpers.Plantilla_6.DatosPlantilla datos = new Helpers.Plantilla_6.DatosPlantilla();
                    datos.PiePagina_GrandesValue = "Resolución 014097 Diciembre 30 de 2010";
                    datos.PiePagina_AutoreeTittle = "NO SOMOS AUTORRETENEDORES DE RENTA";
                    datos.PiePagina_AutorreValue = "\r\n";
                    datos.PiePagina_IcaValue = "De acuerdo con el Articulo 49 de 1990 las actividades industriales solo pagaran impuesto de industria y comercio en el municipio donde se encuentre ubicada la fábrica o planta industrial, por lo tanto clientes fuera del Municipio de Funza, Cundinamarca deben abstenerse de realizar Retención de ICA  en la Actividad  CIIU 3110";
                    datos.PiePagina_Pago = @"FAVOR CONSIGNAR A NOMBRE DE Solinoff Corporation SA
en: Itau Corpbanca Colombia SA Cuenta Corriente 038019915 
y Bancolombia Cuenta Corriente 071-085827-34

ENVIAR SOPORTE A cartera@solinoff.com

De acuerdo a la ley 1231 de Julio de 2008 Art 743 del código de comercio , una vez cumplido el termino de diez (10) días calendario posteriores a la recepción de la presente factura no se acepta devolución, se entenderá que esta factura ha sido aceptada.

El pago no  oportuno  de esta factura causa intereses por mora a la tasa  máxima permitida por la ley vigente y el reporte a centrales de riesgo"; ;
                    return Helpers.Plantilla_6.PiePagina(ds, Logo, ref strError, datos);
                }
            }
            public class NotaCredito
            {
                public static PdfPTable Encabezado(DataSet ds, System.Drawing.Image Logo, string CUFE)
                {
                    return Helpers.Solinoff.Encabezado(ds, Logo, CUFE, Helpers.Compartido.TipoDocumento.NotaCredito);
                }
                public static PdfPTable DatosCliente(DataSet ds, System.Drawing.Image QRInvoice)
                {
                    return Helpers.Plantilla_6.DatosCliente(ds, QRInvoice);
                }
                public static PdfPTable DetalleHead()
                {
                    return Helpers.Plantilla_6.DetalleHead();
                }
                public static PdfPTable Detalles(DataSet ds, ref string strError)
                {
                    return Helpers.Plantilla_6.Detalles(ds, ref strError);
                }
                public static PdfPTable PiePagina(DataSet ds, System.Drawing.Image Logo, ref string strError)
                {
                    Helpers.Plantilla_6.DatosPlantilla datos = new Helpers.Plantilla_6.DatosPlantilla();
                    datos.PiePagina_GrandesValue = "Resolución 014097 Diciembre 30 de 2010";
                    datos.PiePagina_AutoreeTittle = "NO SOMOS AUTORRETENEDORES DE RENTA";
                    datos.PiePagina_AutorreValue = "\r\n";
                    datos.PiePagina_IcaValue = "De acuerdo con el Articulo 49 de 1990 las actividades industriales solo pagaran impuesto de industria y comercio en el municipio donde se encuentre ubicada la fábrica o planta industrial, por lo tanto clientes fuera del Municipio de Funza, Cundinamarca deben abstenerse de realizar Retención de ICA  en la Actividad  CIIU 3110";
                    datos.PiePagina_Pago = @"FAVOR CONSIGNAR A NOMBRE DE Solinoff Corporation SA
en: Itau Corpbanca Colombia SA Cuenta Corriente 038019915 
y Bancolombia Cuenta Corriente 071-085827-34

ENVIAR SOPORTE A cartera@solinoff.com

De acuerdo a la ley 1231 de Julio de 2008 Art 743 del código de comercio , una vez cumplido el termino de diez (10) días calendario posteriores a la recepción de la presente factura no se acepta devolución, se entenderá que esta factura ha sido aceptada.

El pago no  oportuno  de esta factura causa intereses por mora a la tasa  máxima permitida por la ley vigente y el reporte a centrales de riesgo"; ;
                    return Helpers.Plantilla_6.PiePagina(ds, Logo, ref strError, datos);
                }
            }
        }
        public partial class Helpers
        {
            public class Solinoff
            {
                public static PdfPTable Encabezado(DataSet ds, System.Drawing.Image Logo, string CUFE, Helpers.Compartido.TipoDocumento tipoDoc)
                {
                    string Sucursal1 = @"Bogotá

Showroom Bogotá
calle 113 No. 7 - 80 Torre AR Piso 1
PBX.: (571) 446 3822 - 637 1922
Línea Nacional: 01 8000 113822
www.solinoff.com

Casa Solinoff
Calle 109 No. 6 - 70
Tels: (571) 744 1291 - 744 1386
E-mail.: servicioalcliente @solinoff.com";
                    string Sucursal2 = @"Showroom Medellín

Calle 10A No. 38-30
PBX.:(574) 311 0691
Fax:(574) 314 1478
E-mail.: servicioalcliente.medellin@solinoff.com";
                    string Sucursal3 = @"Showroom Barranquilla

Carrera 53 No. 80-198 Oficina 801
Ed. Torre Atlántica
PBX.: (575) 356 8272
E-mail.: servicioalcliente@solinoff.com";
                    //return Plantilla_6.Encabezado(ds, Logo, Sucursal1, Sucursal2, Sucursal3, "");
                    //---------------------------------------------------
                    PdfPTable tbEncabezado = new PdfPTable(5);
                    tbEncabezado.HorizontalAlignment = Element.ALIGN_CENTER;
                    tbEncabezado.SetWidths(new float[] { 2.2f,1.2f,1.2f,1f, 1.3f });
                    tbEncabezado.WidthPercentage = 100;
                    //---- Titulo de la factura ----------
                    iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 10, iTextSharp.text.Font.NORMAL);
                    iTextSharp.text.Font fontTitleSubtittle = FontFactory.GetFont(FontFactory.HELVETICA, 5, iTextSharp.text.Font.NORMAL);
                    Paragraph prgTitle = new Paragraph(ds.Tables["Company"].Rows[0]["Name"].ToString(), fontTitle);
                    prgTitle.Alignment = Element.ALIGN_CENTER;
                    Paragraph prgTitle2 = new Paragraph("REALIZADA POR " + ds.Tables["Company"].Rows[0]["Name"] + " NIT: " + ds.Tables["Company"].Rows[0]["StateTaxID"], fontTitleSubtittle);
                    prgTitle2.Alignment = Element.ALIGN_CENTER;

                    iTextSharp.text.pdf.PdfPCell cellTitle = new iTextSharp.text.pdf.PdfPCell();
                    cellTitle.AddElement(prgTitle);
                    cellTitle.AddElement(prgTitle2);
                    cellTitle.Colspan = 5;
                    cellTitle.Padding = 3;
                    cellTitle.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                    cellTitle.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    cellTitle.Border = 0;
                    cellTitle.BorderColorRight = BaseColor.WHITE;
                    tbEncabezado.AddCell(cellTitle);
                    //------------------------------------
                    //---------- Logo compañia --------------
                    iTextSharp.text.Image LogoPdf = iTextSharp.text.Image.GetInstance(Logo, BaseColor.WHITE);
                    LogoPdf.ScaleAbsolute(160f, 120f);
                    iTextSharp.text.Font fontLogo = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.BOLD);

                    string InfoLogo = "NIT. " + ds.Tables["Company"].Rows[0]["StateTaxID"];
                    Paragraph prgLogo = new Paragraph(InfoLogo, fontLogo);
                    prgLogo.Alignment = Element.ALIGN_LEFT;

                    iTextSharp.text.pdf.PdfPCell celLogoImg = new iTextSharp.text.pdf.PdfPCell();
                    celLogoImg.AddElement(LogoPdf);
                    celLogoImg.AddElement(prgLogo);
                    celLogoImg.Colspan = 1;
                    celLogoImg.Padding = 3;
                    celLogoImg.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                    celLogoImg.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    celLogoImg.Border = 0;
                    celLogoImg.BorderColorRight = BaseColor.WHITE;
                    tbEncabezado.AddCell(celLogoImg);
                    //--- agregando las sucursales ----
                    Plantilla_6.EncabezadoTitulos(ref tbEncabezado, Sucursal1, 1);
                    Plantilla_6.EncabezadoTitulos(ref tbEncabezado, Sucursal2, 1);
                    Plantilla_6.EncabezadoTitulos(ref tbEncabezado, Sucursal3, 1);
                    //---- Factura de venta y Nro Legal
                    string TipoFactura = string.Empty;
                    switch (tipoDoc)
                    {
                        case Compartido.TipoDocumento.FacturaNacional:
                            TipoFactura = "FACTURA DE VENTA N°\n\n" + "" + ds.Tables["InvcHead"].Rows[0]["LegalNumber"].ToString();
                            break;
                        case Compartido.TipoDocumento.FacturaExportacion:
                            TipoFactura = "FACTURA DE VENTA N°\n\n" + "" + ds.Tables["InvcHead"].Rows[0]["LegalNumber"].ToString();
                            break;
                        case Compartido.TipoDocumento.NotaCredito:
                            TipoFactura = "NOTA CREDITO N°\n\n" + "" + ds.Tables["InvcHead"].Rows[0]["LegalNumber"].ToString();
                            break;
                        case Compartido.TipoDocumento.NotaDebito:
                            TipoFactura = "NOTA DEBITO N°\n\n" + "" + ds.Tables["InvcHead"].Rows[0]["LegalNumber"].ToString();
                            break;
                        default:
                            break;
                    }

                    iTextSharp.text.Font fontLegalNum = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                    PdfPCell celLegalNum = new PdfPCell(new Phrase(TipoFactura, fontLegalNum));
                    celLegalNum.Colspan = 1;
                    celLegalNum.Padding = 3;
                    celLegalNum.HorizontalAlignment = Element.ALIGN_LEFT;
                    celLegalNum.VerticalAlignment = Element.ALIGN_TOP;
                    celLegalNum.Border = 0;
                    tbEncabezado.AddCell(celLegalNum);
                    //------- Iva regimen comun ---------------
                    //Celda Regimen Comun
                    iTextSharp.text.Font fontRegComun = FontFactory.GetFont(FontFactory.HELVETICA, 5, iTextSharp.text.Font.NORMAL);
                    iTextSharp.text.Font fontCufe = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);

                    Paragraph prgRegComun = new Paragraph("IVA RÉGIMEN COMÚN 11220207 MARZO 15/88", fontRegComun);
                    Paragraph prgCufe = new Paragraph("CUFE: " + CUFE, fontRegComun);
                    Paragraph prgRepgrafica = new Paragraph("Representación gráfica factura electrónica", fontCufe);


                    //iTextSharp.text.pdf.PdfPCell celRegComun = new iTextSharp.text.pdf.PdfPCell(new Phrase("IVA RÉGIMEN COMÚN 11220207 MARZO 15/88\n\n", fontRegComun));
                    iTextSharp.text.pdf.PdfPCell celRegComun = new iTextSharp.text.pdf.PdfPCell();
                    celRegComun.AddElement(prgRegComun);
                    celRegComun.AddElement(prgRepgrafica);
                    celRegComun.AddElement(prgCufe);
                    celRegComun.Colspan = 7;
                    celRegComun.Padding = 3;
                    celRegComun.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                    celRegComun.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    celRegComun.BorderWidthTop = 0;
                    celRegComun.BorderWidthLeft = 0;
                    celRegComun.BorderWidthRight = 0;
                    tbEncabezado.AddCell(celRegComun);
                    //------ retorno de la tabla ------
                    return tbEncabezado;
                }
                public class EventPageSolinoff : PdfPageEventHelper
                {
                    PdfContentByte cb;

                    // we will put the final number of pages in a template
                    PdfTemplate headerTemplate, footerTemplate;

                    // this is the BaseFont we are going to use for the header / footer
                    BaseFont bf = null;

                    // This keeps track of the creation time
                    DateTime PrintTime = DateTime.Now;

                    private string _header;

                    public string Header
                    {
                        get { return _header; }
                        set { _header = value; }
                    }
                    PdfPTable _Encabezado;
                    PdfPTable _DatosCliente;
                    PdfPTable _DetalleHead;

                    PdfPTable _PiePagina;
                    public PdfPTable PiePagina
                    {
                        get
                        {
                            return _PiePagina;
                        }
                        set
                        {
                            _PiePagina = value;
                        }
                    }
                    public PdfPTable Encabezado
                    {
                        get
                        {
                            return _Encabezado;
                        }
                        set
                        {
                            _Encabezado = value;
                        }
                    }
                    public PdfPTable DatosCliente
                    {
                        get
                        {
                            return _DatosCliente;
                        }
                        set
                        {
                            _DatosCliente = value;
                        }
                    }
                    public PdfPTable DetalleHead
                    {
                        get
                        {
                            return _DetalleHead;
                        }
                        set
                        {
                            _DetalleHead = value;
                        }
                    }

                    public EventPageSolinoff(PdfPTable encabezado, PdfPTable DatosCliente, PdfPTable detalleHead, PdfPTable piepagina)
                    {
                        _Encabezado = encabezado;
                        _DatosCliente = DatosCliente;
                        _DetalleHead = detalleHead;
                        _PiePagina = piepagina;
                    }

                    public override void OnOpenDocument(PdfWriter writer, Document document)
                    {
                        try
                        {
                            PrintTime = DateTime.Now;
                            bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                            cb = writer.DirectContent;
                            headerTemplate = cb.CreateTemplate(100, 100);
                            footerTemplate = cb.CreateTemplate(50, 50);
                        }
                        catch (DocumentException de)
                        {
                        }
                        catch (System.IO.IOException ioe)
                        {
                        }
                    }

                    public override void OnEndPage(iTextSharp.text.pdf.PdfWriter writer, iTextSharp.text.Document document)
                    {
                        base.OnEndPage(writer, document);

                        //call WriteSelectedRows of PdfTable. This writes rows from PdfWriter in PdfTable
                        //first param is start row. -1 indicates there is no end row and all the rows to be included to write
                        //Third and fourth param is x and y position to start writing


                        //document.Add(_PiePagina);

                        Encabezado.TotalWidth = document.PageSize.Width - 35f;
                        Encabezado.WriteSelectedRows(0, -1, 20, document.PageSize.Height - 30, writer.DirectContentUnder);

                        _DatosCliente.TotalWidth = document.PageSize.Width - 35f;
                        _DatosCliente.WriteSelectedRows(0, -1, 20, document.PageSize.Height - 157, writer.DirectContentUnder);

                        _DetalleHead.TotalWidth = document.PageSize.Width - 35f;
                        _DetalleHead.WriteSelectedRows(0, -1, 20, document.PageSize.Height - 240, writer.DirectContentUnder);

                        _PiePagina.TotalWidth = document.PageSize.Width - 35f;
                        _PiePagina.WriteSelectedRows(0, -1, 20, document.PageSize.Height - 510, writer.DirectContentUnder);

                        //tableUnidades.TotalWidth = document.PageSize.Width - 80f;
                        //tableUnidades.WriteSelectedRows(0, -1, 40, document.PageSize.Height - 40, writer.DirectContentUnder);
                        //pdfTab.WriteSelectedRows(0, -1, 40, document.PageSize.Height - 30, writer.DirectContent);

                        //cb.MoveTo(40, document.PageSize.Height - 100);
                        //cb.LineTo(document.PageSize.Width - 40, document.PageSize.Height - 100);
                        //cb.Stroke();
                    }

                    public override void OnCloseDocument(PdfWriter writer, Document document)
                    {
                        base.OnCloseDocument(writer, document);

                        headerTemplate.BeginText();
                        headerTemplate.SetFontAndSize(bf, 12);
                        headerTemplate.SetTextMatrix(0, 0);
                        headerTemplate.ShowText((writer.PageNumber - 1).ToString());
                        headerTemplate.EndText();

                        //footerTemplate.BeginText();
                        //footerTemplate.SetFontAndSize(bf, 12);
                        //footerTemplate.SetTextMatrix(0, 0);
                        //footerTemplate.ShowText((writer.PageNumber - 1).ToString());
                        //footerTemplate.EndText();
                    }
                }
            }
            public class Plantilla_6
            {
                public class DatosPlantilla
                {
                    #region Atributos
                    private string _PiePagina_GrandesTittle = "GRANDES CONTRIBUYENTES";
                    private string _PiePagina_GrandesValue = "resolución No 000076 del 01 / 12 / 2016";
                    private string _PiePagina_AutoreeTittle = "AUTORRETENEDORES";
                    private string _PiePagina_AutorreValue = "resolución 7836 de 5 / 09 / 2001";
                    private string _PiePagina_IcaTittle = "TARIFAS DE ICA";
                    private string _PiePagina_IcaValue = "ITAGUI: Autorretenedores de ICA según acuerdo 030 de 27 / 12 / 2012\nBOGOTÁ: Comercialización 11.04 por mil -servicios 9.66 por mil\nCALI: Comercialización 7.7 por mil-servicios 10 por mil\nBUCARAMANGA: Comercialización y servicios 5 por mil\nBARRANQUILLA: Autorretenedores de Ica según decreto 0123 de 2009\nCÚCUTA: Comercialización y servicios 6 por mil.";
                    private string _PiePagina_Pago = "Favor consignar a nombre de CI TECNOLOGIA ALIMENTARIA S.A.S en:\n" +
                                  "Bancolombia cuenta de ahorros  1023 - 2467033\n" +
                                  "Banco de Bogotá cuenta corriente 250 - 04657 - 0\n" +
                                  "Davivienda cuenta de ahorro 0090 - 2230 - 8488\n" +
                                  "Enviar el soporte tesoreria@citalsa.com y/ o cartera @citalsa.com.\n\n" +
                                  "POR CANCELACIÓN DE ESTE PEDIDO SE EFECTUARÁ UNA SANCIÓN DEL 20%.\n" +
                                  "CON LA ACEPTACIÓN DE ESTA FACTURA, SE DAN POR APROBADAS LAS CONDICIONES EXPRESADAS EN LA GARANTÍA COMERCIAL EN https://citalsa.com.\n" +
                                  "PARA SER EFECTIVA LA GARANTÍA LEGAL C.I. TALSA SE DEBE PRESENTAR LA FACTURA DE COMPRA.\n";
                    private string _PiePagina_ReciboMercancia = "RECIBO DE MERCANCÍA\n\n" +
                                             "FIRMA\n\n" +
                                             "_________________________\n\n" +
                                             "NOMBRE\n\n" +
                                             "_________________________\n\n" +
                                             "FECHA dd/mm/aaaa\n\n" +
                                             "_________________________\n\n" +
                                             "CC";
                    #endregion

                    #region Propiedades
                    public string PiePagina_GrandesTittle
                    {
                        get
                        {
                            return _PiePagina_GrandesTittle;
                        }
                        set
                        {
                            _PiePagina_GrandesTittle = value;
                        }
                    }
                    public string PiePagina_GrandesValue
                    {
                        get
                        {
                            return _PiePagina_GrandesValue;
                        }
                        set
                        {
                            _PiePagina_GrandesValue = value;
                        }
                    }
                    public string PiePagina_AutoreeTittle
                    {
                        get
                        {
                            return _PiePagina_AutoreeTittle;
                        }
                        set
                        {
                            _PiePagina_AutoreeTittle = value;
                        }
                    }
                    public string PiePagina_AutorreValue
                    {
                        get
                        {
                            return _PiePagina_AutorreValue;
                        }
                        set
                        {
                            _PiePagina_AutorreValue = value;
                        }
                    }
                    public string PiePagina_IcaTittle
                    {
                        get
                        {
                            return _PiePagina_IcaTittle;
                        }
                        set
                        {
                            _PiePagina_IcaTittle = value;
                        }
                    }
                    public string PiePagina_IcaValue
                    {
                        get
                        {
                            return _PiePagina_IcaValue;
                        }
                        set
                        {
                            _PiePagina_IcaValue = value;
                        }
                    }
                    public string PiePagina_Pago
                    {
                        get
                        {
                            return _PiePagina_Pago;
                        }
                        set
                        {
                            _PiePagina_Pago = value;
                        }
                    }
                    public string PiePagina_ReciboMercancia
                    {
                        get
                        {
                            return _PiePagina_ReciboMercancia;
                        }
                        set
                        {
                            _PiePagina_ReciboMercancia = value;
                        }
                    }
                    #endregion


                }
                public class EncabezadoDatos
                {
                    public EncabezadoDatos(DataSet _ds)
                    {
                        ds = _ds;
                        _Title = ds.Tables["Company"].Rows[0]["Name"].ToString();
                        _Title2 = "REALIZADA POR " + ds.Tables["Company"].Rows[0]["Name"] + " NIT: " + ds.Tables["Company"].Rows[0]["StateTaxID"];
                        _NIT = "NIT. " + ds.Tables["Company"].Rows[0]["StateTaxID"];
                    }
                    #region Atributos
                    private DataSet ds = null;
                    private string _Sucursal1 = "Principal Itaguí\n" +
                                        "Cra 50 GG No. 12 Sur 07\n" +
                                        "Tel: (057)(4) 285 44 00\n" +
                                        "E-mail: info@citalsa.com\n\n" +
                                        "Barranquilla\n" +
                                        "Calle 93 No.46 - 168\n" +
                                        "Tel: (57)(5) 319 98 80\n" +
                                        "E-mail: superbarranquilla@citalsa.com";
                    private string _Sucursal2 = "Bogotá D.C\n" +
                                        "Avenida 68 No.8 - 05 Esquina\n" +
                                        "Tel:(57)(1) 492 60 50\n" +
                                        "E-mail: bogota@citalsa.com\n\n" +
                                        "Pereira\n" +
                                        "Cra. 16 No. 15 - 42\n" +
                                        "Dos Quebradas Risaralda\n" +
                                        "Tel:(57)(6) 330 66 90\n" +
                                        "E-mail: pereira@citalsa.com";
                    private string _Sucursal3 = "Cali\n" +
                                        "Carrera 1 No.45A - 71\n" +
                                        "Tel: (57)(2) 431 30 30\n" +
                                        "E-mail: cali@citalsa.com\n\n" +
                                        "Cúcuta\n" +
                                        "Avenida 0 No. 2N - 08\n" +
                                        "Tel: (57)(7) 577 41 11\n" +
                                        "E-mail: cucuta@citalsa.com";
                    private string _Sucursal4 = "Bucaramanga\n" +
                                        "Carrera 23 No.21 - 30 San Francisco\n" +
                                        "Tel: (57)(7) 635 02 74\n" +
                                        "E-mail: superbucaramanga@citalsa.com\n\n" +
                                        "Servicio al cliente C.I TALSA\n" +
                                        "Tel: (57)(4) 285 44 00 Ext. 158\n" +
                                        "Cel: (318) 571 35 17\n" +
                                        "E-mail: servicioalcliente@citalsa.com";
                    private float _NewWidth = 60f;
                    private float _NewHeight = 60f;
                    private string _Title = "";
                    private string _Title2 = "";
                    private string _NIT = "";
                    #endregion
                    #region Propiedades
                    public string Title
                    {
                        get
                        {
                            return _Title;
                        }
                        set
                        {
                            _Title = value;
                        }
                    }
                    public string Title2
                    {
                        get
                        {
                            return _Title2;
                        }
                        set
                        {
                            _Title2 = value;
                        }
                    }
                    public string NIT
                    {
                        get
                        {
                            return _NIT;
                        }
                        set
                        {
                            _NIT = value;
                        }
                    }
                    public string Sucursal1
                    {
                        get
                        {
                            return _Sucursal1;
                        }
                        set
                        {
                            _Sucursal1 = value;
                        }
                    }
                    public string Sucursal2
                    {
                        get
                        {
                            return _Sucursal2;
                        }
                        set
                        {
                            _Sucursal2 = value;
                        }
                    }
                    public string Sucursal3
                    {
                        get
                        {
                            return _Sucursal3;
                        }
                        set
                        {
                            _Sucursal3 = value;
                        }
                    }
                    public string Sucursal4
                    {
                        get
                        {
                            return _Sucursal4;
                        }
                        set
                        {
                            _Sucursal4 = value;
                        }
                    }
                    public float NewWidht
                    {
                        get
                        {
                            return _NewWidth;
                        }
                        set
                        {
                            _NewWidth = value;
                        }
                    }
                    public float NewHeight
                    {
                        get
                        {
                            return _NewHeight;
                        }
                        set
                        {
                            _NewHeight = value;
                        }
                    }
                    #endregion

                }
                public static PdfPTable TablaBase
                {
                    get
                    {
                        PdfPTable table = new PdfPTable(7);
                        table.HorizontalAlignment = Element.ALIGN_CENTER;
                        //Dimenciones.
                        float[] Dimesion = new float[7];
                        Dimesion[0] = 2.0F;//CODIGO
                        Dimesion[1] = 5.0F;//Descripcion
                        Dimesion[2] = 2.0F;//CANTIDAD
                        Dimesion[3] = 3.0F;//Precio Unitario               
                        Dimesion[4] = 1.0F;//Iva %
                        Dimesion[5] = 2.0F;//Dcto %
                        Dimesion[6] = 4.0F;//VR TOTAL
                        table.WidthPercentage = 100;
                        table.SetWidths(Dimesion);
                        return table;
                    }
                }
                public static void DetalleHeadTitulos(ref PdfPTable table, string titulo, iTextSharp.text.Font fuente)
                {
                    PdfPCell celda = new PdfPCell(new Phrase(titulo, fuente));
                    celda.Colspan = 1;
                    celda.Padding = 3;
                    celda.HorizontalAlignment = Element.ALIGN_CENTER;
                    celda.VerticalAlignment = Element.ALIGN_TOP;
                    table.AddCell(celda);
                }
                public static void DetalleHeadInformacion(ref PdfPTable table, float BorderWidthTop, float BorderWidthRight, float BorderWidthBottom, float BorderWidthLeft)
                {
                    PdfPCell celda = new PdfPCell();
                    celda.Colspan = 1;
                    celda.Padding = 3;
                    celda.BorderWidthBottom = BorderWidthBottom;
                    celda.BorderWidthTop = BorderWidthTop;
                    celda.BorderWidthRight = BorderWidthRight;
                    celda.BorderWidthLeft = BorderWidthLeft;
                    celda.MinimumHeight = 255;
                    celda.HorizontalAlignment = Element.ALIGN_CENTER;
                    celda.VerticalAlignment = Element.ALIGN_TOP;
                    table.AddCell(celda);
                }
                public static void DetalleValores(ref PdfPTable table, string data, iTextSharp.text.Font fuenteDatos)
                {
                    PdfPCell celda = new PdfPCell(new Phrase(data, fuenteDatos));
                    celda.Colspan = 1;
                    celda.Padding = 3;
                    celda.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                    celda.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                    //celda.BorderWidthBottom = 0;
                    //celda.BorderWidthTop = 0;
                    //celda.BorderWidthRight = 0;
                    celda.Border = 0;
                    table.AddCell(celda);
                }
                public static PdfPTable DetalleHead()
                {
                    PdfPTable table = TablaBase;
                    try
                    {
                        iTextSharp.text.Font fuenteEncabezado = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 6, iTextSharp.text.Font.NORMAL);
                        //Titulos encabezado
                        DetalleHeadTitulos(ref table, "CÓDIGO", fuenteEncabezado);
                        DetalleHeadTitulos(ref table, "DESCRIPCIÓN", fuenteEncabezado);
                        DetalleHeadTitulos(ref table, "CANTIDAD", fuenteEncabezado);
                        DetalleHeadTitulos(ref table, "PRECIO", fuenteEncabezado);
                        DetalleHeadTitulos(ref table, "% IVA", fuenteEncabezado);
                        DetalleHeadTitulos(ref table, "% DCTO", fuenteEncabezado);
                        DetalleHeadTitulos(ref table, "TOTAL NETO", fuenteEncabezado);
                        //celdas en blanco
                        DetalleHeadInformacion(ref table, 0, 0, 0.5f, 0.5f); //Codigo
                        DetalleHeadInformacion(ref table, 0, 0, 0.5f, 0.5f); //Descripcion
                        DetalleHeadInformacion(ref table, 0, 0, 0.5f, 0.5f); //cantidad
                        DetalleHeadInformacion(ref table, 0, 0, 0.5f, 0.5f); //Precio
                        DetalleHeadInformacion(ref table, 0, 0, 0.5f, 0.5f); //% iva
                        DetalleHeadInformacion(ref table, 0, 0, 0.5f, 0.5f); //% dcto
                        DetalleHeadInformacion(ref table, 0, 0.5f, 0.5f, 0.5f); //Total neto
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Error al crear columnas de tabla Items: " + ex.Message);
                    }
                    return table;
                }
                public static bool AddRowInvoiceTALSA(DataRow InvoiceLine, ref iTextSharp.text.pdf.PdfPTable table, iTextSharp.text.Font fuenteDatos, DataTable DtInvcTax, ref string strError)
                {
                    try
                    {
                        //CODIGO
                        DetalleValores(ref table, InvoiceLine["PartNum"].ToString(), fuenteDatos);

                        //DESCRIPCION
                        DetalleValores(ref table, InvoiceLine["LineDesc"].ToString(), fuenteDatos);

                        //CANTIDAD
                        decimal Qty = Convert.ToDecimal(InvoiceLine["SellingShipQty"].ToString());
                        DetalleValores(ref table, string.Format("{0:N0}", Qty), fuenteDatos);

                        //PRECIO
                        decimal UnitPrice = Convert.ToDecimal(InvoiceLine["DocUnitPrice"].ToString());
                        DetalleValores(ref table, "$" + string.Format("{0:N2}", UnitPrice), fuenteDatos);

                        //IVA %
                        decimal PercentIva = Convert.ToDecimal(Helpers.Compartido.GetPercentIdImpDIAN(InvoiceLine["InvoiceNum"].ToString(), InvoiceLine["InvoiceLine"].ToString(), DtInvcTax, ref strError));
                        DetalleValores(ref table, string.Format("{0:N1}", PercentIva), fuenteDatos);

                        //DISCOUNT %
                        DetalleValores(ref table, InvoiceLine["DiscountPercent"].ToString(), fuenteDatos);

                        //EXT PRICE
                        decimal PriceLine = Convert.ToDecimal(InvoiceLine["DocExtPrice"].ToString());
                        DetalleValores(ref table, "$" + string.Format("{0:N2}", PriceLine), fuenteDatos);

                        return true;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Error addLineInvoice: " + ex.Message);
                        return false;
                    }
                }
                public static void EncabezadoTitulos(ref PdfPTable table, string Titulo, int colspan)
                {
                    iTextSharp.text.pdf.PdfPCell celTittle = new iTextSharp.text.pdf.PdfPCell(new Phrase(Titulo, Helpers.Fuentes.Plantilla_6_fontTitle1));
                    celTittle.Colspan = colspan;
                    celTittle.Padding = 3;
                    celTittle.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                    celTittle.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    celTittle.Border = 0;
                    table.AddCell(celTittle);
                }
                //***********************-------- Datos de la factura ------------*******************************************
                public static PdfPTable Encabezado(DataSet DsInvoiceAR, System.Drawing.Image Logo, EncabezadoDatos datos)
                {
                    iTextSharp.text.pdf.PdfPTable tableInvoice = TablaBase;
                    //---- Titulo de la factura ----------
                    iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 10, iTextSharp.text.Font.NORMAL);
                    iTextSharp.text.Font fontTitleSubtittle = FontFactory.GetFont(FontFactory.HELVETICA, 5, iTextSharp.text.Font.NORMAL);
                    Paragraph prgTitle = new Paragraph(datos.Title, fontTitle);
                    prgTitle.Alignment = Element.ALIGN_CENTER;
                    Paragraph prgTitle2 = new Paragraph(datos.Title2, fontTitleSubtittle);
                    prgTitle2.Alignment = Element.ALIGN_CENTER;

                    iTextSharp.text.pdf.PdfPCell cellTitle = new iTextSharp.text.pdf.PdfPCell();
                    cellTitle.AddElement(prgTitle);
                    cellTitle.AddElement(prgTitle2);
                    cellTitle.Colspan = 7;
                    cellTitle.Padding = 3;
                    cellTitle.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                    cellTitle.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    cellTitle.Border = 0;
                    cellTitle.BorderColorRight = BaseColor.WHITE;
                    tableInvoice.AddCell(cellTitle);
                    //---------------------

                    iTextSharp.text.Image LogoPdf = iTextSharp.text.Image.GetInstance(Logo, BaseColor.WHITE);
                    LogoPdf.ScaleAbsolute(datos.NewWidht, datos.NewHeight);
                    iTextSharp.text.Font fontLogo = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.BOLD);

                    string InfoLogo = datos.NIT;
                    Paragraph prgLogo = new Paragraph(InfoLogo, fontLogo);
                    prgLogo.Alignment = Element.ALIGN_LEFT;

                    //iTextSharp.text.pdf.PdfPCell celLogoImg = new iTextSharp.text.pdf.PdfPCell(LogoPdf);
                    iTextSharp.text.pdf.PdfPCell celLogoImg = new iTextSharp.text.pdf.PdfPCell();
                    celLogoImg.AddElement(LogoPdf);
                    celLogoImg.AddElement(prgLogo);
                    celLogoImg.Colspan = 1;
                    celLogoImg.Padding = 3;
                    celLogoImg.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                    celLogoImg.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    celLogoImg.Border = 0;
                    celLogoImg.BorderColorRight = BaseColor.WHITE;
                    tableInvoice.AddCell(celLogoImg);

                    EncabezadoTitulos(ref tableInvoice, datos.Sucursal1, 1);

                    EncabezadoTitulos(ref tableInvoice, datos.Sucursal2, 1);

                    EncabezadoTitulos(ref tableInvoice, datos.Sucursal3, 1);

                    EncabezadoTitulos(ref tableInvoice, datos.Sucursal4, 2);

                    string TipoFactura = "FACTURA DE VENTA N°\n\n" +
                                         "" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"].ToString();

                    //Celda LegalNum
                    iTextSharp.text.Font fontLegalNum = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                    iTextSharp.text.pdf.PdfPCell celLegalNum = new iTextSharp.text.pdf.PdfPCell(new Phrase(TipoFactura, fontLegalNum));
                    celLegalNum.Colspan = 1;
                    celLegalNum.Padding = 3;
                    celLegalNum.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                    celLegalNum.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    celLegalNum.Border = 0;
                    tableInvoice.AddCell(celLegalNum);

                    //Celda Regimen Comun
                    iTextSharp.text.Font fontRegComun = FontFactory.GetFont(FontFactory.HELVETICA, 5, iTextSharp.text.Font.NORMAL);
                    iTextSharp.text.pdf.PdfPCell celRegComun = new iTextSharp.text.pdf.PdfPCell(new Phrase("IVA RÉGIMEN COMÚN 11220207 MARZO 15/88\n\n", fontRegComun));
                    celRegComun.Colspan = 7;
                    celRegComun.Padding = 3;
                    celRegComun.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                    celRegComun.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    celRegComun.BorderWidthTop = 0;
                    celRegComun.BorderWidthLeft = 0;
                    celRegComun.BorderWidthRight = 0;
                    tableInvoice.AddCell(celRegComun);
                    //----- retornando la factura
                    return tableInvoice;
                }
                public static PdfPTable DatosCliente(DataSet DsInvoiceAR, System.Drawing.Image QRInvoice)
                {
                    /*VENDIDO A*/
                    PdfPTable tableInvoice = TablaBase;

                    iTextSharp.text.Font fontVendidoA = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 6, iTextSharp.text.Font.NORMAL);
                    iTextSharp.text.Font fontVendidoASubTittle = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);

                    Paragraph prgVendidoA = new Paragraph("SEÑORES", fontVendidoA);
                    prgVendidoA.Alignment = Element.ALIGN_LEFT;

                    string VendidoA = DsInvoiceAR.Tables["Customer"].Rows[0]["Name"].ToString() + "\n" +
                                      DsInvoiceAR.Tables["Customer"].Rows[0]["Address1"].ToString() + "\n" +
                                      DsInvoiceAR.Tables["Customer"].Rows[0]["City"].ToString() + "\n" +
                                      DsInvoiceAR.Tables["Customer"].Rows[0]["State"].ToString() + "\n\n" +
                                      "NIT/CEDULA   " + DsInvoiceAR.Tables["Customer"].Rows[0]["ResaleID"].ToString();

                    Paragraph prgVendidoASubTittle = new Paragraph(VendidoA, fontVendidoASubTittle);
                    prgVendidoASubTittle.Alignment = Element.ALIGN_LEFT;

                    iTextSharp.text.pdf.PdfPCell cellVendidoA = new iTextSharp.text.pdf.PdfPCell();
                    cellVendidoA.AddElement(prgVendidoA);
                    cellVendidoA.AddElement(prgVendidoASubTittle);
                    cellVendidoA.Colspan = 2;
                    cellVendidoA.Padding = 3;
                    cellVendidoA.BorderWidthTop = 0;
                    cellVendidoA.BorderWidthRight = 0;
                    cellVendidoA.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                    cellVendidoA.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    tableInvoice.AddCell(cellVendidoA);

                    /*INFO PEDIDO*/
                    iTextSharp.text.Font fontInfoPedido = FontFactory.GetFont(FontFactory.HELVETICA, 5, iTextSharp.text.Font.NORMAL);
                    iTextSharp.text.pdf.PdfPTable tableInfoPedido = new iTextSharp.text.pdf.PdfPTable(5);
                    tableInfoPedido.HorizontalAlignment = Element.ALIGN_CENTER;

                    //Dimenciones.
                    float[] DimencionInfoPedido = new float[5];
                    DimencionInfoPedido[0] = 5.0F;//
                    DimencionInfoPedido[1] = 5.0F;//
                    DimencionInfoPedido[2] = 5.0F;//                
                    DimencionInfoPedido[3] = 5.0F;//
                    DimencionInfoPedido[4] = 5.0F;// 

                    tableInfoPedido.WidthPercentage = 100;
                    tableInfoPedido.SetWidths(DimencionInfoPedido);

                    //1-1 Confirma Pedido.
                    iTextSharp.text.pdf.PdfPCell cellConfirmaPedido = new iTextSharp.text.pdf.PdfPCell(new Phrase("CONFIRMACION PEDIDO\n\n\n\n", fontInfoPedido));
                    cellConfirmaPedido.Colspan = 1;
                    cellConfirmaPedido.Padding = 3;
                    cellConfirmaPedido.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                    cellConfirmaPedido.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    tableInfoPedido.AddCell(cellConfirmaPedido);

                    //1-2 No Pedido
                    iTextSharp.text.pdf.PdfPCell cellNoPedido = new iTextSharp.text.pdf.PdfPCell(new Phrase("No. PEDIDO\n" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["OrderNum"].ToString() + "\n\n", fontInfoPedido));
                    cellNoPedido.Colspan = 1;
                    cellNoPedido.Padding = 3;
                    cellNoPedido.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                    cellNoPedido.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    tableInfoPedido.AddCell(cellNoPedido);

                    //1-3 Orden de Compra                
                    iTextSharp.text.pdf.PdfPCell cellOrdenCompra = new iTextSharp.text.pdf.PdfPCell(new Phrase("ORDEN DE COMPRA\n" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["PONum"].ToString() + "\n\n", fontInfoPedido));
                    cellOrdenCompra.Colspan = 1;
                    cellOrdenCompra.Padding = 3;
                    cellOrdenCompra.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                    cellOrdenCompra.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    tableInfoPedido.AddCell(cellOrdenCompra);

                    //1-4 Fecha Vencimiento
                    iTextSharp.text.pdf.PdfPCell cellDueDate = new iTextSharp.text.pdf.PdfPCell(new Phrase("FECHA VENCIMIENTO:\n" + Convert.ToDateTime(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"]).ToString("dd/MM/yyyy") + "\n\n", fontInfoPedido));
                    cellDueDate.Colspan = 1;
                    cellDueDate.Padding = 3;
                    cellDueDate.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                    cellDueDate.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    tableInfoPedido.AddCell(cellDueDate);

                    //1-5 Fecha Factura
                    iTextSharp.text.pdf.PdfPCell cellInvoiceDate = new iTextSharp.text.pdf.PdfPCell(new Phrase("FECHA FACTURA:\n" + Convert.ToDateTime(DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).ToString("dd/MM/yyyy") + "\n\n", fontInfoPedido));
                    cellInvoiceDate.Colspan = 1;
                    cellInvoiceDate.Padding = 3;
                    cellInvoiceDate.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                    cellInvoiceDate.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    tableInfoPedido.AddCell(cellInvoiceDate);

                    //2-1 Telefono
                    iTextSharp.text.pdf.PdfPCell cellTelefono = new iTextSharp.text.pdf.PdfPCell(new Phrase("TELÉFONO\n" + DsInvoiceAR.Tables["Customer"].Rows[0]["PhoneNum"].ToString(), fontInfoPedido));
                    cellTelefono.Colspan = 1;
                    cellTelefono.Padding = 3;
                    cellTelefono.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                    cellTelefono.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    tableInfoPedido.AddCell(cellTelefono);

                    //2-2 InvoiceNum
                    iTextSharp.text.pdf.PdfPCell cellNoFacRef = new iTextSharp.text.pdf.PdfPCell(new Phrase("Nro. Fact. REF\n" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceNum"].ToString(), fontInfoPedido));
                    cellNoFacRef.Colspan = 1;
                    cellNoFacRef.Padding = 3;
                    cellNoFacRef.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                    cellNoFacRef.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    tableInfoPedido.AddCell(cellNoFacRef);

                    //2-3 PLAZO MESES
                    iTextSharp.text.pdf.PdfPCell cellTermsCode = new iTextSharp.text.pdf.PdfPCell(new Phrase("PLAZO MESES\n" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["TermsCodeDescription"].ToString(), fontInfoPedido));
                    cellTermsCode.Colspan = 1;
                    cellTermsCode.Padding = 3;
                    cellTermsCode.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                    cellTermsCode.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    tableInfoPedido.AddCell(cellTermsCode);

                    //2-4 VENDEDOR
                    iTextSharp.text.pdf.PdfPCell cellVendedor = new iTextSharp.text.pdf.PdfPCell(new Phrase("VENDEDOR\n" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["SalesRepName1"].ToString(), fontInfoPedido));
                    cellVendedor.Colspan = 1;
                    cellVendedor.Padding = 3;
                    cellVendedor.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                    cellVendedor.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    tableInfoPedido.AddCell(cellVendedor);

                    //2-4 FECHA ENTREGA
                    iTextSharp.text.pdf.PdfPCell cellFechaEntrega = new iTextSharp.text.pdf.PdfPCell(new Phrase("FECHA ENTREGA", fontInfoPedido));
                    cellFechaEntrega.Colspan = 1;
                    cellFechaEntrega.Padding = 3;
                    cellFechaEntrega.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                    cellFechaEntrega.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    tableInfoPedido.AddCell(cellFechaEntrega);

                    iTextSharp.text.pdf.PdfPCell cellInfoPedido = new iTextSharp.text.pdf.PdfPCell(tableInfoPedido);
                    cellInfoPedido.Colspan = 4;
                    cellInfoPedido.Padding = 3;
                    cellInfoPedido.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                    cellInfoPedido.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    cellInfoPedido.BorderWidthTop = 0;
                    tableInvoice.AddCell(cellInfoPedido);

                    /*CELL QR*/
                    iTextSharp.text.Font fontRepGrafica = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                    Paragraph prgQrTittle = new Paragraph("CÓDIGO QR", fontRepGrafica);
                    prgQrTittle.Alignment = Element.ALIGN_CENTER;

                    iTextSharp.text.Image QRPdf = iTextSharp.text.Image.GetInstance(QRInvoice, BaseColor.WHITE);
                    QRPdf.ScaleAbsolute(60f, 60f);
                    QRPdf.Alignment = Element.ALIGN_CENTER;
                    iTextSharp.text.pdf.PdfPCell celImgQR = new iTextSharp.text.pdf.PdfPCell();
                    celImgQR.AddElement(prgQrTittle);
                    celImgQR.AddElement(QRPdf);
                    //iTextSharp.text.Image QRPdf = iTextSharp.text.Image.GetInstance(QRInvoice, BaseColor.WHITE);
                    //iTextSharp.text.pdf.PdfPCell celImgQR = new iTextSharp.text.pdf.PdfPCell(QRPdf);
                    celImgQR.Colspan = 1;
                    celImgQR.Padding = 3;
                    celImgQR.BorderWidthTop = 0;
                    celImgQR.BorderWidthLeft = 0;
                    celImgQR.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                    celImgQR.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    tableInvoice.AddCell(celImgQR);

                    return tableInvoice;
                }
                public static PdfPTable Detalles(DataSet DsInvoiceAR, ref string strError)
                {
                    PdfPTable tableInvoice = TablaBase;
                    //Filas Factura
                    iTextSharp.text.Font fuenteDatos = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);

                    decimal TotalDescountInvc = 0;
                    decimal TotalMissCharges = 0;
                    string SalesOrder = string.Empty;
                    string Embarques = string.Empty;
                    string PartDescLoteAndSerie = string.Empty;

                    //Lineas de Factura.
                    foreach (DataRow InvoiceLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
                    {
                        //Totales de las Lineas
                        TotalDescountInvc += Convert.ToDecimal(InvoiceLine["DspDocLessDiscount"]);
                        //TotalMissCharges += Convert.ToDecimal(InvoiceLine["DspDocTotalMiscChrg"]);                                       
                        AddRowInvoiceTALSA(InvoiceLine, ref tableInvoice, fuenteDatos, DsInvoiceAR.Tables["InvcTax"], ref strError);
                        Helpers.Compartido.AddInfoByLoteAndSerie(DsInvoiceAR, Convert.ToInt32(InvoiceLine["InvoiceLine"]), ref PartDescLoteAndSerie);
                    }

                    return tableInvoice;
                }
                public static PdfPTable PiePagina(DataSet DsInvoiceAR, System.Drawing.Image Logo, ref string strError, DatosPlantilla datos)
                {
                    PdfPTable tableInvoice = TablaBase;

                    iTextSharp.text.Font fuenteDatos = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);
                    decimal TotalDescountInvc = 0;
                    foreach (DataRow InvoiceLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
                    {
                        //Totales de las Lineas
                        TotalDescountInvc += Convert.ToDecimal(InvoiceLine["DspDocLessDiscount"]);
                    }


                    //Enviar a: ShitoAddres.   
                    string ShipToAddres = string.Empty;
                    strError += "Add ShipToAddres.\n";

                    //Validando si usa OneTineOTS.
                    string OtsAddress = string.Empty;
                    foreach (DataRow Line in DsInvoiceAR.Tables["InvcDtl"].Rows)
                    {
                        if (Convert.ToBoolean(DsInvoiceAR.Tables["InvcDtl"].Rows[0]["UseOTS"].ToString()))
                        {
                            OtsAddress = Line["OTSName"].ToString() + "\n" +
                                         Line["OTSAddress1"].ToString() + "\n" +
                                         Line["OTSCity"].ToString() + "\n" +
                                         Line["OTSState"].ToString() + "\n";
                            break;
                        }
                    }


                    if (!string.IsNullOrEmpty(OtsAddress))
                        ShipToAddres = OtsAddress;
                    else
                        //ShipToAddres = DsInvoiceAR.Tables["Customer"].Rows[0]["ShipToAddres"].ToString();
                        ShipToAddres = string.Empty;

                    if(!string.IsNullOrEmpty(ShipToAddres))
                    {
                        iTextSharp.text.pdf.PdfPCell cellShipToAddres = new iTextSharp.text.pdf.PdfPCell(new Phrase(ShipToAddres + "\n", fuenteDatos));
                        cellShipToAddres.Colspan = 7;
                        cellShipToAddres.Padding = 3;
                        cellShipToAddres.BorderWidthLeft = 0;
                        cellShipToAddres.BorderWidthRight = 0;
                        cellShipToAddres.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                        cellShipToAddres.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                        tableInvoice.AddCell(cellShipToAddres);
                    }

                    strError += "Add ShipToAddres OK.\n";

                    iTextSharp.text.Font fuenteObs = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 6, iTextSharp.text.Font.NORMAL);
                    iTextSharp.text.pdf.PdfPCell cellObs = new iTextSharp.text.pdf.PdfPCell(new Phrase("OBSERVACIONES:" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceComment"].ToString(), fuenteObs));
                    cellObs.Colspan = 4;
                    cellObs.Padding = 3;
                    cellObs.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                    cellObs.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    tableInvoice.AddCell(cellObs);

                    //Info Resolucion.
                    //string Resolucion = "HAB 18762006023112 FECHA2017-12-07 VTO 2019-12-07 \n" +
                    //                    "DEL CTBQ 4.450-6.000 POR COMPUTADOR\n";

                    string Resolucion = string.Empty;
                    if (DsInvoiceAR.Tables.Contains("Encabezado"))
                    {
                        foreach (DataRow row in DsInvoiceAR.Tables["Encabezado"].Rows)
                        {
                            Resolucion = row["Prefijo1"].ToString() + "\n" +
                                         row["Prefijo2"].ToString();
                            break;
                        }
                    }

                    iTextSharp.text.pdf.PdfPCell cellResolucion = new iTextSharp.text.pdf.PdfPCell(new Phrase(Resolucion, fuenteDatos));
                    cellResolucion.Colspan = 3;
                    cellResolucion.Padding = 3;
                    cellResolucion.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                    cellResolucion.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    tableInvoice.AddCell(cellResolucion);


                    //Observacion 2 factura
                    iTextSharp.text.Font fuenteObs2Tittle = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                    iTextSharp.text.Font fuenteObs2 = FontFactory.GetFont(FontFactory.HELVETICA, 5, iTextSharp.text.Font.NORMAL);

                    //string Obs2 = "GRANDES CONTRIBUYENTES resolución No 000076 del 01 / 12 / 2016\n"+
                    //              "AUTORRETENEDORES resolución 7836 de 5 / 09 / 2001\n"+
                    //              "TARIFAS DE ICA\n"+
                    //              "Itagui:  Somos autorretenedores de ICA según acuerdo 030 de 27 / 12 / 2012\n"+
                    //              "Bogotá: Comercialización 11.04 por mil -servicios 9.66 por mil\n"+
                    //              "Cali: Comercialización 7.7 por mil-servicios 10 por mil\n"+
                    //              "Bucaramanga: Comercialización y servicios 5 por mil\n"+
                    //              "Barranquilla: Somos autorretenedores de Ica según decreto 0123 de 2009\n"+
                    //              "Cúcuta: Comercialización y servicios 6 por mil.";

                    Phrase prfGrandesTittle = new Phrase(datos.PiePagina_GrandesTittle, fuenteObs2Tittle);
                    Phrase prfGrandesValue = new Phrase(datos.PiePagina_GrandesValue, fuenteObs2);
                    Phrase prfAutoreeTittle = new Phrase(datos.PiePagina_AutoreeTittle, fuenteObs2Tittle);
                    Phrase prfAutorreValue = new Phrase(datos.PiePagina_AutorreValue, fuenteObs2);
                    Phrase prfIcaTittle = new Phrase(datos.PiePagina_IcaTittle, fuenteObs2Tittle);
                    Phrase prfIcaValue = new Phrase(datos.PiePagina_IcaValue, fuenteObs2);

                    //iTextSharp.text.pdf.PdfPCell cellObs2 = new iTextSharp.text.pdf.PdfPCell(new Phrase(Obs2, fuenteObs2));
                    iTextSharp.text.pdf.PdfPCell cellObs2 = new iTextSharp.text.pdf.PdfPCell();
                    cellObs2.AddElement(prfGrandesTittle);
                    cellObs2.AddElement(prfGrandesValue);
                    cellObs2.AddElement(prfAutoreeTittle);
                    cellObs2.AddElement(prfAutorreValue);
                    cellObs2.AddElement(prfIcaTittle);
                    cellObs2.AddElement(prfIcaValue);
                    cellObs2.Colspan = 4;
                    cellObs2.Padding = 3;
                    cellObs2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                    cellObs2.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    tableInvoice.AddCell(cellObs2);

                    /*TOTAL Factura.*/
                    iTextSharp.text.Font fontTotalesTittle = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 6, iTextSharp.text.Font.NORMAL);
                    iTextSharp.text.Font fontTotales = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);
                    iTextSharp.text.pdf.PdfPTable tableTotales = new iTextSharp.text.pdf.PdfPTable(3);
                    tableTotales.HorizontalAlignment = Element.ALIGN_CENTER;

                    //Dimenciones.
                    float[] DimencionTotales = new float[3];
                    DimencionTotales[0] = 5.0F;//
                    DimencionTotales[1] = 2.0F;//                          
                    DimencionTotales[2] = 5.0F;// 

                    tableTotales.WidthPercentage = 100;
                    tableTotales.SetWidths(DimencionTotales);

                    //1-1 TOTAL BRUTO
                    iTextSharp.text.pdf.PdfPCell cellTotalBruto = new iTextSharp.text.pdf.PdfPCell(new Phrase("TOTAL BRUTO", fontTotalesTittle));
                    cellTotalBruto.Colspan = 2;
                    cellTotalBruto.Padding = 3;
                    cellTotalBruto.BorderWidthTop = 0;
                    cellTotalBruto.BorderWidthLeft = 0;
                    cellTotalBruto.BorderWidthBottom = 0;
                    cellTotalBruto.HorizontalAlignment = iTextSharp.text.Element.ALIGN_RIGHT;
                    cellTotalBruto.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    tableTotales.AddCell(cellTotalBruto);

                    //1-2 TOTAL BRUTO VALUE
                    decimal TotalBruto = Convert.ToDecimal(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubTotal"].ToString()) + (TotalDescountInvc * -1);
                    iTextSharp.text.pdf.PdfPCell cellTotalBrutoValue = new iTextSharp.text.pdf.PdfPCell(new Phrase("$" + string.Format("{0:N2}", TotalBruto), fontTotales));
                    cellTotalBrutoValue.Colspan = 1;
                    cellTotalBrutoValue.Padding = 3;
                    cellTotalBrutoValue.BorderWidthTop = 0;
                    cellTotalBrutoValue.BorderWidthRight = 0;
                    cellTotalBrutoValue.BorderWidthBottom = 0;
                    cellTotalBrutoValue.HorizontalAlignment = iTextSharp.text.Element.ALIGN_RIGHT;
                    cellTotalBrutoValue.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    tableTotales.AddCell(cellTotalBrutoValue);

                    //2-1 DESCUENTO
                    iTextSharp.text.pdf.PdfPCell cellDescuento = new iTextSharp.text.pdf.PdfPCell(new Phrase("DESCTO.CCIAL", fontTotalesTittle));
                    cellDescuento.Colspan = 2;
                    cellDescuento.Padding = 3;
                    cellDescuento.BorderWidthTop = 0;
                    cellDescuento.BorderWidthLeft = 0;
                    cellDescuento.BorderWidthBottom = 0;
                    cellDescuento.HorizontalAlignment = iTextSharp.text.Element.ALIGN_RIGHT;
                    cellDescuento.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    tableTotales.AddCell(cellDescuento);

                    //2-2 DESCUENTO VALUE
                    iTextSharp.text.pdf.PdfPCell cellDescuentoValue = new iTextSharp.text.pdf.PdfPCell(new Phrase("$" + string.Format("{0:N2}", TotalDescountInvc), fontTotales));
                    cellDescuentoValue.Colspan = 1;
                    cellDescuentoValue.Padding = 3;
                    cellDescuentoValue.BorderWidthTop = 0;
                    cellDescuentoValue.BorderWidthRight = 0;
                    cellDescuentoValue.BorderWidthBottom = 0;
                    cellDescuentoValue.HorizontalAlignment = iTextSharp.text.Element.ALIGN_RIGHT;
                    cellDescuentoValue.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    tableTotales.AddCell(cellDescuentoValue);

                    //1-1 SUBTOTAL
                    iTextSharp.text.pdf.PdfPCell cellSubTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase("SUBTOTAL", fontTotalesTittle));
                    cellSubTotal.Colspan = 2;
                    cellSubTotal.Padding = 3;
                    cellSubTotal.BorderWidthTop = 0;
                    cellSubTotal.BorderWidthLeft = 0;
                    cellSubTotal.BorderWidthBottom = 0;
                    cellSubTotal.HorizontalAlignment = iTextSharp.text.Element.ALIGN_RIGHT;
                    cellSubTotal.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    tableTotales.AddCell(cellSubTotal);

                    //1-2 SUBTOTAL VALUE
                    decimal SubTotal = Convert.ToDecimal(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubTotal"].ToString());
                    iTextSharp.text.pdf.PdfPCell cellSubTotalValue = new iTextSharp.text.pdf.PdfPCell(new Phrase("$" + string.Format("{0:N2}", SubTotal), fontTotales));
                    cellSubTotalValue.Colspan = 1;
                    cellSubTotalValue.Padding = 3;
                    cellSubTotalValue.BorderWidthTop = 0;
                    cellSubTotalValue.BorderWidthRight = 0;
                    cellSubTotalValue.BorderWidthBottom = 0;
                    cellSubTotalValue.HorizontalAlignment = iTextSharp.text.Element.ALIGN_RIGHT;
                    cellSubTotalValue.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    tableTotales.AddCell(cellSubTotalValue);


                    //4-1 IVA
                    iTextSharp.text.pdf.PdfPCell cellIVA = new iTextSharp.text.pdf.PdfPCell(new Phrase("IVA", fontTotalesTittle));
                    cellIVA.Colspan = 2;
                    cellIVA.Padding = 3;
                    cellIVA.BorderWidthTop = 0;
                    cellIVA.BorderWidthLeft = 0;
                    cellIVA.BorderWidthBottom = 0;
                    cellIVA.HorizontalAlignment = iTextSharp.text.Element.ALIGN_RIGHT;
                    cellIVA.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    tableTotales.AddCell(cellIVA);

                    //4-2 IVA VALUE                                
                    decimal Iva = Convert.ToDecimal(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DocTaxAmt"].ToString());
                    //Iva = GetValImpuestoByID("01", DsInvoiceAR);
                    iTextSharp.text.pdf.PdfPCell cellIvaValue = new iTextSharp.text.pdf.PdfPCell(new Phrase("$" + string.Format("{0:N2}", Iva), fontTotales));
                    cellIvaValue.Colspan = 1;
                    cellIvaValue.Padding = 3;
                    cellIvaValue.BorderWidthTop = 0;
                    cellIvaValue.BorderWidthRight = 0;
                    cellIvaValue.BorderWidthBottom = 0;
                    cellIvaValue.HorizontalAlignment = iTextSharp.text.Element.ALIGN_RIGHT;
                    cellIvaValue.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    tableTotales.AddCell(cellIvaValue);

                    //4-1 RETE IVA
                    iTextSharp.text.pdf.PdfPCell cellReteIva = new iTextSharp.text.pdf.PdfPCell(new Phrase("RETE IVA", fontTotalesTittle));
                    cellReteIva.Colspan = 2;
                    cellReteIva.Padding = 3;
                    cellReteIva.BorderWidthTop = 0;
                    cellReteIva.BorderWidthLeft = 0;
                    cellReteIva.BorderWidthBottom = 0;
                    cellReteIva.HorizontalAlignment = iTextSharp.text.Element.ALIGN_RIGHT;
                    cellReteIva.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    tableTotales.AddCell(cellReteIva);

                    //4-2 RETE IVA VALUE
                    decimal ReteIva = 0; //Convert.ToDecimal(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DocTaxAmt"].ToString());
                    iTextSharp.text.pdf.PdfPCell cellReteIvaValue = new iTextSharp.text.pdf.PdfPCell(new Phrase("$" + string.Format("{0:N2}", ReteIva), fontTotales));
                    cellReteIvaValue.Colspan = 1;
                    cellReteIvaValue.Padding = 3;
                    cellReteIvaValue.BorderWidthTop = 0;
                    cellReteIvaValue.BorderWidthRight = 0;
                    cellReteIvaValue.BorderWidthBottom = 0;
                    cellReteIvaValue.HorizontalAlignment = iTextSharp.text.Element.ALIGN_RIGHT;
                    cellReteIvaValue.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    tableTotales.AddCell(cellReteIvaValue);

                    //4-1 RETE ICA
                    iTextSharp.text.pdf.PdfPCell cellReteIca = new iTextSharp.text.pdf.PdfPCell(new Phrase("RETE ICA", fontTotalesTittle));
                    cellReteIca.Colspan = 2;
                    cellReteIca.Padding = 3;
                    cellReteIca.BorderWidthTop = 0;
                    cellReteIca.BorderWidthLeft = 0;
                    cellReteIca.BorderWidthBottom = 0;
                    cellReteIca.HorizontalAlignment = iTextSharp.text.Element.ALIGN_RIGHT;
                    cellReteIca.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    tableTotales.AddCell(cellReteIca);

                    //4-2 RETE ICA VALUE
                    decimal ReteIca = 0; //Convert.ToDecimal(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DocTaxAmt"].ToString());
                    iTextSharp.text.pdf.PdfPCell cellReteIcaValue = new iTextSharp.text.pdf.PdfPCell(new Phrase("$" + string.Format("{0:N2}", ReteIca), fontTotales));
                    cellReteIcaValue.Colspan = 1;
                    cellReteIcaValue.Padding = 3;
                    cellReteIcaValue.BorderWidthTop = 0;
                    cellReteIcaValue.BorderWidthRight = 0;
                    cellReteIcaValue.BorderWidthBottom = 0;
                    cellReteIcaValue.HorizontalAlignment = iTextSharp.text.Element.ALIGN_RIGHT;
                    cellReteIcaValue.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    tableTotales.AddCell(cellReteIcaValue);

                    //4-1 INC BOLSA
                    //ImpConsBolsa = GetValImpuestoByID("02", DsInvoiceAR);
                    iTextSharp.text.pdf.PdfPCell cellIncBolsa = new iTextSharp.text.pdf.PdfPCell(new Phrase("INC BOLSA", fontTotalesTittle));
                    cellIncBolsa.Colspan = 2;
                    cellIncBolsa.Padding = 3;
                    cellIncBolsa.BorderWidthTop = 0;
                    cellIncBolsa.BorderWidthLeft = 0;
                    //cellIncBolsa.BorderWidthBottom = 0;
                    cellIncBolsa.HorizontalAlignment = iTextSharp.text.Element.ALIGN_RIGHT;
                    cellIncBolsa.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    tableTotales.AddCell(cellIncBolsa);

                    //4-2 INC BOLSA VALUE
                    decimal IncBolsa = 0; //Convert.ToDecimal(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DocTaxAmt"].ToString());
                    iTextSharp.text.pdf.PdfPCell cellIncBolsaValue = new iTextSharp.text.pdf.PdfPCell(new Phrase("$" + string.Format("{0:N2}", IncBolsa), fontTotales));
                    cellIncBolsaValue.Colspan = 1;
                    cellIncBolsaValue.Padding = 3;
                    cellIncBolsaValue.BorderWidthTop = 0;
                    cellIncBolsaValue.BorderWidthRight = 0;
                    //cellIncBolsaValue.BorderWidthBottom = 0;
                    cellIncBolsaValue.HorizontalAlignment = iTextSharp.text.Element.ALIGN_RIGHT;
                    cellIncBolsaValue.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    tableTotales.AddCell(cellIncBolsaValue);

                    //5-1 TOTAL
                    iTextSharp.text.Font fontTotal = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 8, iTextSharp.text.Font.NORMAL, BaseColor.WHITE);
                    iTextSharp.text.pdf.PdfPCell cellTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase("TOTAL", fontTotal));
                    cellTotal.Colspan = 1;
                    cellTotal.Padding = 3;
                    cellTotal.BackgroundColor = BaseColor.BLACK;
                    cellTotal.BorderWidth = 0;
                    cellTotal.HorizontalAlignment = iTextSharp.text.Element.ALIGN_RIGHT;
                    cellTotal.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    tableTotales.AddCell(cellTotal);

                    //5-1 Moneda
                    iTextSharp.text.pdf.PdfPCell cellCurrency = new iTextSharp.text.pdf.PdfPCell(new Phrase(DsInvoiceAR.Tables["InvcHead"].Rows[0]["CurrencyCodeCurrencyID"].ToString(), fontTotalesTittle));
                    cellCurrency.Colspan = 1;
                    cellCurrency.Padding = 3;
                    cellCurrency.BorderWidth = 0;
                    cellCurrency.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                    cellCurrency.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    tableTotales.AddCell(cellCurrency);

                    //5-2 TOTAL VALUE
                    decimal Total = Convert.ToDecimal(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"].ToString());
                    iTextSharp.text.pdf.PdfPCell cellTotalValue = new iTextSharp.text.pdf.PdfPCell(new Phrase("$" + string.Format("{0:N2}", Total), fontTotales));
                    cellTotalValue.Colspan = 1;
                    cellTotalValue.Padding = 3;
                    cellTotalValue.BorderWidth = 0;
                    cellTotalValue.HorizontalAlignment = iTextSharp.text.Element.ALIGN_RIGHT;
                    cellTotalValue.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    tableTotales.AddCell(cellTotalValue);

                    //Blank1
                    iTextSharp.text.pdf.PdfPCell cellBlank1 = new iTextSharp.text.pdf.PdfPCell(new Phrase("\n\n\n\n", fontTotalesTittle));
                    cellBlank1.Colspan = 3;
                    cellBlank1.Padding = 3;
                    cellBlank1.BorderWidth = 0;
                    cellBlank1.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                    cellBlank1.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    tableTotales.AddCell(cellBlank1);

                    tableTotales.WidthPercentage = 100;
                    tableTotales.SetWidths(DimencionTotales);

                    iTextSharp.text.pdf.PdfPCell cellTotales = new iTextSharp.text.pdf.PdfPCell(tableTotales);
                    cellTotales.Colspan = 3;
                    cellTotales.Rowspan = 3;
                    cellTotales.Padding = 3;
                    cellTotales.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                    cellTotales.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    tableInvoice.AddCell(cellTotales);

                    //Observacion 3 Factura.                
                    iTextSharp.text.pdf.PdfPCell cellObs3 = new iTextSharp.text.pdf.PdfPCell(new Phrase("", fuenteObs2));
                    cellObs3.Colspan = 4;
                    cellObs3.Padding = 3;
                    cellObs3.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                    cellObs3.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    tableInvoice.AddCell(cellObs3);

                    /*INFORMACION ENTREGA.*/
                    iTextSharp.text.Font fontInfoEntrega = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);
                    iTextSharp.text.pdf.PdfPTable tableInfoEntrega = new iTextSharp.text.pdf.PdfPTable(3);
                    tableInfoEntrega.HorizontalAlignment = Element.ALIGN_CENTER;

                    //Dimenciones.
                    float[] DimencionInfoEntrega = new float[3];
                    DimencionInfoEntrega[0] = 5.0F;//
                    DimencionInfoEntrega[1] = 5.0F;//
                    DimencionInfoEntrega[2] = 5.0F;//                               

                    tableInfoEntrega.WidthPercentage = 100;
                    tableInfoEntrega.SetWidths(DimencionInfoEntrega);

                    //Obs 4 Factura.
                    iTextSharp.text.Font fuenteObs4 = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);

                    //Paragraph prgEmisor = new Paragraph("EMISOR DE FACTURA", fontInfoEntrega);
                    //prgEmisor.Alignment = Element.ALIGN_LEFT;

                    //iTextSharp.text.Image LogoPdf2 = iTextSharp.text.Image.GetInstance(Logo, BaseColor.WHITE);
                    //LogoPdf2.ScaleAbsolute(60f, 60f);

                    iTextSharp.text.pdf.PdfPCell cellObs4 = new iTextSharp.text.pdf.PdfPCell(new Phrase(datos.PiePagina_Pago, fuenteObs4));
                    //cellObs4.AddElement(prgEmisor);
                    //cellObs4.AddElement(LogoPdf2);
                    cellObs4.Colspan = 2;
                    cellObs4.Padding = 3;
                    cellObs4.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                    cellObs4.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    tableInfoEntrega.AddCell(cellObs4);

                    ////Aceptacion
                    //iTextSharp.text.pdf.PdfPCell cellAceptacion = new iTextSharp.text.pdf.PdfPCell(new Phrase("ACEPTACIÓN\n",fontInfoEntrega));
                    //cellAceptacion.Colspan = 1;
                    //cellAceptacion.Padding = 3;
                    //cellAceptacion.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                    //cellAceptacion.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    //tableInfoEntrega.AddCell(cellAceptacion);               


                    iTextSharp.text.pdf.PdfPCell cellRecibido = new iTextSharp.text.pdf.PdfPCell(new Phrase(datos.PiePagina_ReciboMercancia, fontInfoEntrega));
                    cellRecibido.Colspan = 1;
                    cellRecibido.Padding = 3;
                    cellRecibido.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                    cellRecibido.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    tableInfoEntrega.AddCell(cellRecibido);

                    iTextSharp.text.pdf.PdfPCell cellInfoEntrega = new iTextSharp.text.pdf.PdfPCell(tableInfoEntrega);
                    cellInfoEntrega.Colspan = 4;
                    cellInfoEntrega.Padding = 3;
                    cellInfoEntrega.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                    cellInfoEntrega.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    tableInvoice.AddCell(cellInfoEntrega);


                    //Pie Factura.
                    //iTextSharp.text.pdf.PdfPCell cellIcaBarranquilla = new iTextSharp.text.pdf.PdfPCell(new Phrase("", fuenteObs2));
                    //cellIcaBarranquilla.Colspan = 4;
                    //cellIcaBarranquilla.Padding = 3;
                    //cellIcaBarranquilla.BorderWidth = 0;
                    //cellIcaBarranquilla.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                    //cellIcaBarranquilla.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    //tableInvoice.AddCell(cellIcaBarranquilla);

                    //iTextSharp.text.pdf.PdfPCell cellIObs3 = new iTextSharp.text.pdf.PdfPCell(new Phrase("", fuenteDatos));
                    //cellIObs3.Colspan = 3;
                    //cellIObs3.Padding = 3;
                    //cellIObs3.BorderWidth = 0;
                    //cellIObs3.HorizontalAlignment = iTextSharp.text.Element.ALIGN_RIGHT;
                    //cellIObs3.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    //tableInvoice.AddCell(cellIObs3);


                    //iTextSharp.text.Image BannerPdf = iTextSharp.text.Image.GetInstance(LogoBanner, BaseColor.WHITE);
                    //BannerPdf.ScaleAbsolute(535f, 90f);

                    //iTextSharp.text.pdf.PdfPCell cellBanner = new iTextSharp.text.pdf.PdfPCell(BannerPdf);
                    iTextSharp.text.pdf.PdfPCell cellBanner = new iTextSharp.text.pdf.PdfPCell();
                    cellBanner.Colspan = 7;
                    cellBanner.Padding = 3;
                    cellBanner.BorderWidth = 0;
                    cellBanner.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                    cellBanner.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    //tableInvoice.AddCell(cellBanner);

                    strError += "Cell Banner OK and add to table\n";


                    tableInvoice.HeadersInEvent = true;
                    tableInvoice.CompleteRow();
                    //----
                    return tableInvoice;
                }
            }
        }
        #endregion
    }
}
