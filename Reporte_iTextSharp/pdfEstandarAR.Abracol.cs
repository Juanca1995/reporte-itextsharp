﻿namespace RulesServicesDIAN2.Adquiriente
{
    using iTextSharp.text;
    using iTextSharp.text.pdf;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Diagnostics;
    using System.Drawing;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.NetworkInformation;
    using System.Text;
    using System.Threading.Tasks;
    using System.Web;
    using System.Windows.Forms;

    public partial class pdfEstandarAR
    {
        //--------------------------
        public bool FacturaNacionalABRACOL(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {
            bool esLocal = Helpers.Compartido.EsLocal;
            string Ruta = string.Empty;
            string RutaImg = string.Empty;
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            if (esLocal)
            {
                Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
                RutaImg = $@"{AppDomain.CurrentDomain.BaseDirectory}\Logo_890911327.png";
            }
            else
            {
                Ruta = HttpContext.Current.Server.MapPath("~/PDF/" + NIT + "/");
                RutaImg = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";
            }
            NomArchivo = string.Empty;
            NomArchivo = NombreInvoice + ".pdf";
            try
            {
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.

                Document document = new Document(iTextSharp.text.PageSize.LETTER, 20f, 10f, 30f, 30f);

                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;

                // step 3: we open the document     
                document.Open();

                PdfDiv divEspacio = new PdfDiv();
                divEspacio.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio.Height = 10;
                divEspacio.Width = 130;

                PdfDiv divEspacio2 = new PdfDiv();
                divEspacio2.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio2.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio2.Height = 2;
                divEspacio2.Width = 130;

                //FUENTES----------------------------------------------------------------------------------------------------------
                iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle2 = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustom = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);
                //FUENTES---------------------------------------------------------------------------------------------------------

                #region ENCABEZADO
                System.Drawing.Image logo = null;
                var requestLogo = WebRequest.Create(RutaImg);

                using (var responseLogo = requestLogo.GetResponse())
                using (var streamLogo = responseLogo.GetResponseStream())
                {
                    logo = Bitmap.FromStream(streamLogo);
                }

                iTextSharp.text.Font fontAcercade = FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL);

                //----------------------------------------------------------------------------------------------------
                iTextSharp.text.Font fontTitleFactura = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);

                #endregion

                #region DATOS CUSTOMER Y FACTURA
                //------------------------------------------------------------------------------------------------
                float[] DimencionFacturar = new float[3];
                DimencionFacturar[0] = 1.0F;//
                DimencionFacturar[1] = 1.0F;//
                DimencionFacturar[2] = 1.0F;//

                iTextSharp.text.Font fontTitleFecha = FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontDMA = FontFactory.GetFont(FontFactory.HELVETICA, 6, BaseColor.GRAY);
                iTextSharp.text.Font fontDetalleFecha = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 8, iTextSharp.text.Font.NORMAL);

                #endregion

                #region Tabla Unidades
                PdfPTable tableTituloUnidades = new PdfPTable(9);
                //Dimenciones.
                float[] DimencionUnidades = new float[9];
                DimencionUnidades[0] = 0.178F;//L
                DimencionUnidades[1] = 0.5F;//Codigo
                DimencionUnidades[2] = 0.5F;//Cantidad
                DimencionUnidades[3] = 0.328F;//Unidad
                DimencionUnidades[4] = 2.0F;//Descripcion
                DimencionUnidades[5] = 0.585F;//Valor Unitario
                DimencionUnidades[6] = 0.378F;//Descuento
                DimencionUnidades[7] = 0.5F;//Valor Total
                DimencionUnidades[8] = 0.28F;//%IVA

                tableTituloUnidades.WidthPercentage = 100;
                tableTituloUnidades.SetWidths(DimencionUnidades);

                iTextSharp.text.pdf.PdfPCell celTextL = new iTextSharp.text.pdf.PdfPCell(new Phrase("L", fontTitleFactura));
                celTextL.Colspan = 1;
                celTextL.Padding = 3;
                celTextL.Border = 0;
                //celTextL.BorderWidthRight = 1;
                celTextL.BorderWidthLeft = 1;
                celTextL.BackgroundColor = BaseColor.LIGHT_GRAY;
                celTextL.HorizontalAlignment = Element.ALIGN_CENTER;
                celTextL.VerticalAlignment = Element.ALIGN_TOP;

                tableTituloUnidades.AddCell(celTextL);

                iTextSharp.text.pdf.PdfPCell celTextCodigo = new iTextSharp.text.pdf.PdfPCell(new Phrase("Código", fontTitleFactura));
                celTextCodigo.Colspan = 1;
                celTextCodigo.Padding = 3;
                celTextCodigo.BackgroundColor = BaseColor.LIGHT_GRAY;
                celTextCodigo.Border = 0;
                celTextCodigo.BorderWidthLeft = 1;
                //celDescripcion.BorderColorBottom = BaseColor.WHITE;
                celTextCodigo.HorizontalAlignment = Element.ALIGN_CENTER;
                celTextCodigo.VerticalAlignment = Element.ALIGN_TOP;

                tableTituloUnidades.AddCell(celTextCodigo);

                iTextSharp.text.pdf.PdfPCell celTextCantidad = new iTextSharp.text.pdf.PdfPCell(new Phrase("Cantidad", fontTitleFactura));
                celTextCantidad.Colspan = 1;
                celTextCantidad.Padding = 3;
                celTextCantidad.Border = 0;
                celTextCantidad.BorderWidthLeft = 1;
                celTextCantidad.BackgroundColor = BaseColor.LIGHT_GRAY;
                celTextCantidad.HorizontalAlignment = Element.ALIGN_CENTER;
                celTextCantidad.VerticalAlignment = Element.ALIGN_TOP;

                tableTituloUnidades.AddCell(celTextCantidad);

                iTextSharp.text.pdf.PdfPCell celTextUnidad = new iTextSharp.text.pdf.PdfPCell(new Phrase("Unidad", fontTitleFactura));
                celTextUnidad.Colspan = 1;
                celTextUnidad.Padding = 3;
                celTextUnidad.BackgroundColor = BaseColor.LIGHT_GRAY;
                celTextUnidad.Border = 0;
                celTextUnidad.BorderWidthLeft = 1;
                celTextUnidad.HorizontalAlignment = Element.ALIGN_CENTER;
                celTextUnidad.VerticalAlignment = Element.ALIGN_TOP;

                tableTituloUnidades.AddCell(celTextUnidad);

                iTextSharp.text.pdf.PdfPCell celTextDesc = new iTextSharp.text.pdf.PdfPCell(new Phrase("Descripción", fontTitleFactura));
                celTextDesc.Colspan = 1;
                celTextDesc.Padding = 3;
                celTextDesc.BackgroundColor = BaseColor.LIGHT_GRAY;
                celTextDesc.Border = 0;
                celTextDesc.BorderWidthLeft = 1;
                celTextDesc.HorizontalAlignment = Element.ALIGN_CENTER;
                celTextDesc.VerticalAlignment = Element.ALIGN_TOP;

                tableTituloUnidades.AddCell(celTextDesc);

                iTextSharp.text.pdf.PdfPCell celTextValorUnitario = new iTextSharp.text.pdf.PdfPCell(new Phrase("Valor Unitario", fontTitleFactura));
                celTextValorUnitario.Colspan = 1;
                celTextValorUnitario.Border = 0;
                celTextValorUnitario.BorderWidthLeft = 1;
                celTextValorUnitario.BackgroundColor = BaseColor.LIGHT_GRAY;
                celTextValorUnitario.Padding = 3;
                celTextValorUnitario.HorizontalAlignment = Element.ALIGN_CENTER;
                celTextValorUnitario.VerticalAlignment = Element.ALIGN_TOP;

                tableTituloUnidades.AddCell(celTextValorUnitario);

                iTextSharp.text.pdf.PdfPCell celTextDescuento = new iTextSharp.text.pdf.PdfPCell(new Phrase("Descuento", fontTitleFactura));
                celTextDescuento.Colspan = 1;
                celTextDescuento.Padding = 3;
                celTextDescuento.Border = 0;
                celTextDescuento.BorderWidthLeft = 1;
                celTextDescuento.BackgroundColor = BaseColor.LIGHT_GRAY;
                celTextDescuento.HorizontalAlignment = Element.ALIGN_CENTER;
                celTextDescuento.VerticalAlignment = Element.ALIGN_TOP;

                tableTituloUnidades.AddCell(celTextDescuento);

                iTextSharp.text.pdf.PdfPCell celTextValorTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase("Valor Total", fontTitleFactura));
                celTextValorTotal.Colspan = 1;
                celTextValorTotal.Padding = 3;
                celTextValorTotal.Border = 0;
                celTextValorTotal.BorderWidthLeft = 1;
                celTextValorTotal.BackgroundColor = BaseColor.LIGHT_GRAY;
                celTextValorTotal.HorizontalAlignment = Element.ALIGN_CENTER;
                celTextValorTotal.VerticalAlignment = Element.ALIGN_TOP;

                tableTituloUnidades.AddCell(celTextValorTotal);

                iTextSharp.text.pdf.PdfPCell celTextIva = new iTextSharp.text.pdf.PdfPCell(new Phrase("% IVA", fontTitleFactura));
                celTextIva.Colspan = 1;
                celTextIva.Padding = 3;
                celTextIva.Border = 0;
                celTextIva.BorderWidthLeft = 1;
                celTextIva.BorderWidthRight = 1;
                celTextIva.BackgroundColor = BaseColor.LIGHT_GRAY;
                celTextIva.HorizontalAlignment = Element.ALIGN_CENTER;
                celTextIva.VerticalAlignment = Element.ALIGN_TOP;

                tableTituloUnidades.AddCell(celTextIva);

                PdfPTable tableUnidades = new PdfPTable(9);
                tableUnidades.WidthPercentage = 100;
                tableUnidades.SetWidths(DimencionUnidades);
                decimal totalDescuento = 0;
                foreach (DataRow InvoiceLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
                {
                    if (!AddUnidadesAbracol(InvoiceLine, ref tableUnidades, fontCustom, DsInvoiceAR))
                        return false;
                    if(Helpers.Compartido.VerificarExistenciaColumnas(DsInvoiceAR, "InvcDtl", "DspDocDiscount"))
                        totalDescuento += decimal.Parse((string)InvoiceLine["DspDocDiscount"]);
                }

                int numAdLineas = 6 - DsInvoiceAR.Tables["InvcDtl"].Rows.Count;
                PdfPTable tableEspacioUnidades = new PdfPTable(9);
                tableEspacioUnidades.WidthPercentage = 100;
                tableEspacioUnidades.SetWidths(DimencionUnidades);

                for (int i = 0; i < numAdLineas; i++)
                    AddUnidadesAbracol(ref tableEspacioUnidades);

                PdfPTable tableLineaFinal = new PdfPTable(9);
                tableLineaFinal.WidthPercentage = 100;
                tableLineaFinal.SetWidths(DimencionUnidades);
                iTextSharp.text.pdf.PdfPCell LineaFinal = new iTextSharp.text.pdf.PdfPCell();
                LineaFinal.Colspan = 9;
                LineaFinal.Padding = 3;
                LineaFinal.Border = 1;
                LineaFinal.BorderColorBottom = BaseColor.WHITE;
                LineaFinal.HorizontalAlignment = Element.ALIGN_CENTER;
                LineaFinal.VerticalAlignment = Element.ALIGN_TOP;
                tableLineaFinal.AddCell(LineaFinal);
                #endregion

                #region OBSERVACIONES Y TOTALES
                PdfPTable tableObsTotales = new PdfPTable(3);
                tableObsTotales.WidthPercentage = 100;
                float[] dimTableObsTotales = new float[3];
                dimTableObsTotales[0] = 2.0f;
                dimTableObsTotales[1] = 0.8f;
                dimTableObsTotales[2] = 0.85f;
                tableObsTotales.SetWidths(dimTableObsTotales);
                //---------------------------------------------------------------------
                PdfPTable tableObsercaciones = new PdfPTable(1);

                PdfPCell celTextObservaciones = new PdfPCell(new Phrase("OBSERVACIONES\n\n\n", fontTitleFactura));
                celTextObservaciones.Border = 0;
                tableObsercaciones.AddCell(celTextObservaciones);

                string Observaciones = string.Empty;
                if (Helpers.Compartido.VerificarExistenciaColumnas(DsInvoiceAR, "InvcHead", "InvoiceComment", 0))
                    Observaciones = DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceComment"].ToString();

                PdfPCell celTextPedirCita = new PdfPCell(new Phrase(Observaciones, fontTitle2));
                celTextPedirCita.Border = 0;
                tableObsercaciones.AddCell(celTextPedirCita);

                PdfPCell celObservaciones = new PdfPCell(tableObsercaciones);
                tableObsTotales.AddCell(celObservaciones);
                //----------------------------------------------------------------------
                PdfPCell celFimarSello = new PdfPCell(new Phrase("\n\n\n\n_______________________________\nFirma y Sello", fontTitle2));
                celFimarSello.HorizontalAlignment = Element.ALIGN_CENTER;
                celFimarSello.VerticalAlignment = Element.ALIGN_BOTTOM;
                tableObsTotales.AddCell(celFimarSello);
                //----------------------------------------------------------------------
                PdfPTable tableTotales = new PdfPTable(2);
                float[] dimTableTotales = new float[2];
                dimTableTotales[0] = 1.1f;
                dimTableTotales[1] = 1.7f;
                tableTotales.SetWidths(dimTableTotales);

                PdfPCell celTextTotalBruto = new PdfPCell(new Phrase("Total Bruto", fontTitle2));
                celTextTotalBruto.Border = 0;
                tableTotales.AddCell(celTextTotalBruto);
                //PdfPCell celValTotalBruto = new PdfPCell(new Phrase(
                //    decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubTotal"]).ToString("N2"), fontTitleFactura));
                PdfPCell celValTotalBruto = new PdfPCell(new Phrase(decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number02"]).ToString("N2"), fontTitleFactura));
                celValTotalBruto.Border = 0;
                celValTotalBruto.HorizontalAlignment = Element.ALIGN_RIGHT;
                tableTotales.AddCell(celValTotalBruto);

                PdfPCell celTotalTextDescuento = new PdfPCell(new Phrase("Descuento", fontTitle2));
                celTotalTextDescuento.Border = 0;
                tableTotales.AddCell(celTotalTextDescuento);
                PdfPCell celTotalValDescuento = new PdfPCell(new Phrase(totalDescuento.ToString("N2"), fontTitleFactura));
                celTotalValDescuento.Border = 0;
                celTotalValDescuento.HorizontalAlignment = Element.ALIGN_RIGHT;
                tableTotales.AddCell(celTotalValDescuento);

                PdfPCell celTextTotalValorFletes = new PdfPCell(new Phrase("Valor Fletes", fontTitle2));
                celTextTotalValorFletes.Border = 0;
                tableTotales.AddCell(celTextTotalValorFletes);
                PdfPCell celValTotalValorFletes = new PdfPCell(new Phrase(decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number03"]).ToString("N2"), fontTitleFactura));
                celValTotalValorFletes.Border = 0;
                celValTotalValorFletes.HorizontalAlignment = Element.ALIGN_RIGHT;
                tableTotales.AddCell(celValTotalValorFletes);

                PdfPCell celTextSubTotal = new PdfPCell(new Phrase("Subtotal", fontTitle2));
                celTextSubTotal.Border = 0;
                tableTotales.AddCell(celTextSubTotal);
                PdfPCell celValSubTotal = new PdfPCell(new Phrase(
                    decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubTotal"]).ToString("N2"), fontTitleFactura));
                celValSubTotal.Border = 0;
                celValSubTotal.HorizontalAlignment = Element.ALIGN_RIGHT;
                tableTotales.AddCell(celValSubTotal);

                PdfPCell celTextValorIva = new PdfPCell(new Phrase("Valor IVA", fontTitle2));
                celTextValorIva.Border = 0;
                tableTotales.AddCell(celTextValorIva);
                var Iva = GetValImpuestoByID("01", DsInvoiceAR);
                PdfPCell celValValorIva = new PdfPCell(new Phrase(Iva.ToString("N2"), fontTitleFactura));
                celValValorIva.Border = 0;
                celValValorIva.HorizontalAlignment = Element.ALIGN_RIGHT;
                tableTotales.AddCell(celValValorIva);

                PdfPCell celTextReteIva = new PdfPCell(new Phrase("Rete IVA 15%", fontTitle2));
                celTextReteIva.Border = 0;
                tableTotales.AddCell(celTextReteIva);
                PdfPCell celValReteIva = new PdfPCell(new Phrase(decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number04"]).ToString("N2"), fontTitleFactura));
                celValReteIva.Border = 0;
                celValReteIva.HorizontalAlignment = Element.ALIGN_RIGHT;
                tableTotales.AddCell(celValReteIva);

                PdfPCell celTotales = new PdfPCell(tableTotales);
                celTotales.Padding = 2;
                tableObsTotales.AddCell(celTotales);

                PdfPCell celTotalLetras = new PdfPCell(new Phrase(
                    $"Son: **{Nroenletras((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"])}**",
                    fontTitle2));
                celTotalLetras.Colspan = 2;
                tableObsTotales.AddCell(celTotalLetras);

                PdfPTable tableTotalFactura = new PdfPTable(2);
                tableTotalFactura.SetWidths(dimTableTotales);

                PdfPCell celTextTotal = new PdfPCell(new Phrase("Total Factura", fontTitleFactura));
                celTextTotal.BackgroundColor = BaseColor.LIGHT_GRAY;
                celTextTotal.Border = 0;
                tableTotalFactura.AddCell(celTextTotal);

                PdfPCell celValTotal = new PdfPCell(new Phrase(
                    decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"]).ToString("N2"),
                    fontTitleFactura));
                celValTotal.HorizontalAlignment = Element.ALIGN_RIGHT;
                celValTotal.BackgroundColor = BaseColor.LIGHT_GRAY;
                celValTotal.Border = 0;
                tableTotalFactura.AddCell(celValTotal);

                PdfPCell celTotal = new PdfPCell(tableTotalFactura);
                tableObsTotales.AddCell(celTotal);
                //---------------------------------------------------------------------------------------------

                PdfPTable tableResolucion = new PdfPTable(1);
                tableResolucion.WidthPercentage = 100;

                PdfPCell celResolucion = new PdfPCell(new Phrase(DsInvoiceAR.Tables["InvcHead"].Rows[0]["Character02"].ToString(),
                    fontTitle2));
                celResolucion.HorizontalAlignment = Element.ALIGN_CENTER;
                celResolucion.Border = 0;
                tableResolucion.AddCell(celResolucion);
                //------------------------------------------------------------------------------------------------------------
                PdfDiv divTextCopia = new PdfDiv();
                divTextCopia.BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.DOTTED;
                divTextCopia.Width = 582;


                PdfPTable tableTextCopia = new PdfPTable(3);
                tableTextCopia.WidthPercentage = 100;

                PdfPCell celTextNumRot = new PdfPCell(new Phrase("NumRot 2.0 Version:Factura30 / FAcx20170213.ps",
                    FontFactory.GetFont(FontFactory.HELVETICA, 5, iTextSharp.text.Font.NORMAL)));
                celTextNumRot.PaddingTop = 4;
                celTextNumRot.BorderWidthRight = PdfPCell.NO_BORDER;
                celTextNumRot.BorderWidthLeft = PdfPCell.NO_BORDER;
                tableTextCopia.AddCell(celTextNumRot);

                PdfPCell celTextCopia = new PdfPCell(new Phrase("-  -", fontTitleFactura));
                celTextCopia.PaddingTop = 4;
                celTextCopia.BorderWidthRight = PdfPCell.NO_BORDER;
                celTextCopia.BorderWidthLeft = PdfPCell.NO_BORDER;
                celTextCopia.HorizontalAlignment = Element.ALIGN_CENTER;
                tableTextCopia.AddCell(celTextCopia);

                PdfPCell celTextPagina = new PdfPCell(new Phrase(" "));
                celTextPagina.BorderWidthRight = PdfPCell.NO_BORDER;
                celTextPagina.BorderWidthLeft = PdfPCell.NO_BORDER;
                tableTextCopia.AddCell(celTextPagina);

                divTextCopia.AddElement(tableTextCopia);
                //---------------------------------------------------------------------------------------
                PdfPTable tableTextNitReferencia = new PdfPTable(2);
                float[] dimTableTextNitReferencia = new float[2];
                dimTableTextNitReferencia[0] = 0.9f;
                dimTableTextNitReferencia[1] = 1.3f;
                tableTextNitReferencia.SetWidths(dimTableTextNitReferencia);
                tableTextNitReferencia.WidthPercentage = 100;

                PdfPCell celTextNitReferenciae = new PdfPCell(new Phrase(" ", fontTitle));
                celTextNitReferenciae.HorizontalAlignment = Element.ALIGN_RIGHT;
                celTextNitReferenciae.Colspan = 1;
                celTextNitReferenciae.Border = PdfPCell.NO_BORDER;
                tableTextNitReferencia.AddCell(celTextNitReferenciae);

                //Referencia 1
                string Referencia1 = (string)DsInvoiceAR.Tables["Customer"].Rows[0]["ResaleID"];

                //Phrase phrResolBar = new Phrase(
                //    $"ABRASIVOS DE COLOMBIA S.A NIT.{(string)DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"]}" +
                //    $"-1   Referencia 1 No. {Referencia1} - Referencia 2 No. {(string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"]}", FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 6f, iTextSharp.text.Font.NORMAL));


                PdfPCell celTextNitReferencia = new PdfPCell(new Phrase(
                    $"ABRASIVOS DE COLOMBIA S.A NIT.{(string)DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"]}" +
                    $"-1   Referencia 1 No. {Referencia1} - Referencia 2 No. {(string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"]}",
                    FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 6f, iTextSharp.text.Font.NORMAL)));
                celTextNitReferencia.HorizontalAlignment = Element.ALIGN_RIGHT;
                celTextNitReferencia.Colspan = 1;
                celTextNitReferencia.Border = PdfPCell.NO_BORDER;
                celTextNitReferencia.BorderWidthTop = 0.5f;
                celTextNitReferencia.PaddingLeft = 0;
                tableTextNitReferencia.AddCell(celTextNitReferencia);
                //---------------------------------------------------------------------------------------
                PdfPTable tableBC1 = new PdfPTable(3);
                tableBC1.HorizontalAlignment = Element.ALIGN_LEFT;
                float[] dimTableBC1 = new float[3];
                dimTableBC1[0] = 1.8f;
                dimTableBC1[1] = 1.0f;
                dimTableBC1[2] = 0.95f;
                tableBC1.SetWidths(dimTableBC1);
                tableBC1.WidthPercentage = 100;

                //PdfPCell celBC1 = new PdfPCell(new Phrase("Bar Code"));
                string strNIT = DsInvoiceAR.Tables["Customer"].Rows[0]["ResaleID"].ToString();
                while (strNIT.Length < 10)
                {
                    strNIT = "0" + strNIT;
                }
                string strLegalNumber = DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"].ToString();
                Helpers.Compartido.ConvertNumeric(ref strLegalNumber);
                if (strLegalNumber.Length > 6)
                    strLegalNumber = strLegalNumber.Substring(0, 6);
                else
                {
                    while (strLegalNumber.Length < 6)
                    {
                        strLegalNumber = "0" + strLegalNumber;
                    }
                }

                string BarCodeValor = string.Format("(415)7705059777776(8020){0}(8020){1}", strNIT, strLegalNumber);

                PdfPCell celBC1 = new PdfPCell();
                Helpers.Compartido.BarCode(ref celBC1, BarCodeValor);
                celBC1.Border = PdfPCell.NO_BORDER;
                tableBC1.AddCell(celBC1);

                PdfPTable tableDatosBC1 = new PdfPTable(2);
                float[] dimTableDatosBC1 = new float[2];
                dimTableDatosBC1[0] = 1.3f;
                dimTableDatosBC1[1] = 0.7f;
                tableDatosBC1.SetWidths(dimTableDatosBC1);
                //----------------------------------------------------------------------------------
                PdfPTable tableDatosComprador = new PdfPTable(2);
                float[] dimTableDatosComprador = new float[2];
                dimTableDatosComprador[0] = 0.8f;
                dimTableDatosComprador[1] = 1.5f;
                tableDatosComprador.SetWidths(dimTableDatosComprador);

                PdfPCell celprgComprador = new PdfPCell();
                celprgComprador.AddElement(new Paragraph("COMPRADOR:", fontTitle2));
                celprgComprador.AddElement(new Paragraph((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["CustomerName"]
                    , FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 5, iTextSharp.text.Font.NORMAL)));
                celprgComprador.Border = PdfPCell.NO_BORDER;
                celprgComprador.BorderWidthLeft = 0.5f;
                celprgComprador.Colspan = 2;
                //celprgComprador.Padding = 3;
                tableDatosComprador.AddCell(celprgComprador);

                PdfPCell celprgNit = new PdfPCell();
                celprgNit.AddElement(new Paragraph("NIT./CC. ", fontTitle2));
                celprgNit.Border = PdfPCell.NO_BORDER;
                celprgNit.BorderWidthLeft = 0.5f;
                celprgNit.BorderWidthBottom = 0.5f;
                celprgNit.Colspan = 1;
                //celprgNit.Padding = 3;
                tableDatosComprador.AddCell(celprgNit);

                PdfPCell celprgValNit = new PdfPCell();
                celprgValNit.AddElement(new Phrase((string)DsInvoiceAR.Tables["Customer"].Rows[0]["ResaleID"], fontTitleFactura));
                celprgValNit.Border = PdfPCell.NO_BORDER;
                celprgValNit.BorderWidthBottom = 0.5f;
                celprgValNit.Colspan = 1;
                celprgValNit.Padding = 3;
                tableDatosComprador.AddCell(celprgValNit);

                PdfPCell celEspacionDatosComprador = new PdfPCell(new Phrase(" "));
                celEspacionDatosComprador.Colspan = 2;
                celprgValNit.BorderWidthBottom = 0.5f;
                celEspacionDatosComprador.Border = PdfPCell.NO_BORDER;
                tableDatosComprador.AddCell(celEspacionDatosComprador);

                PdfPCell celDatosComprador = new PdfPCell(tableDatosComprador);
                celDatosComprador.BorderWidthBottom = PdfPCell.NO_BORDER;
                celDatosComprador.BorderWidthRight = PdfPCell.NO_BORDER;
                celDatosComprador.BorderWidthLeft = PdfPCell.NO_BORDER;
                tableDatosBC1.AddCell(celDatosComprador);
                //----------------------------------------------------------------------------------
                PdfPTable tableTotalEfectivo = new PdfPTable(1);
                Paragraph prgTotalEfectivo = new Paragraph("Total Efectivo\n",
                    FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL));

                PdfDiv divTotalEfectivo = new iTextSharp.text.pdf.PdfDiv();
                divTotalEfectivo.Float = iTextSharp.text.pdf.PdfDiv.FloatType.LEFT;
                divTotalEfectivo.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divTotalEfectivo.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                divTotalEfectivo.Height = 21.2f;
                divTotalEfectivo.Width = 49f;
                divTotalEfectivo.PaddingTop = 5;
                divTotalEfectivo.BackgroundColor = BaseColor.LIGHT_GRAY;

                PdfPCell celTextTotalEfectivo = new PdfPCell();
                celTextTotalEfectivo.AddElement(prgTotalEfectivo);
                celTextTotalEfectivo.BorderWidthBottom = PdfPCell.NO_BORDER;
                celTextTotalEfectivo.VerticalAlignment = Element.ALIGN_LEFT;
                celTextTotalEfectivo.HorizontalAlignment = Element.ALIGN_TOP;
                tableTotalEfectivo.AddCell(celTextTotalEfectivo);

                PdfPCell celDivTotalEfectivo = new PdfPCell();
                celDivTotalEfectivo.AddElement(divTotalEfectivo);
                celDivTotalEfectivo.BorderWidthTop = PdfPCell.NO_BORDER;
                tableTotalEfectivo.AddCell(celDivTotalEfectivo);

                PdfPCell celEspacioTotalEfectivo = new PdfPCell(new Phrase(" "));
                celEspacioTotalEfectivo.Border = PdfPCell.NO_BORDER;
                tableTotalEfectivo.AddCell(celEspacioTotalEfectivo);

                PdfPCell celTotalEfectivo = new PdfPCell(tableTotalEfectivo);
                celTotalEfectivo.BorderWidthBottom = PdfPCell.NO_BORDER;
                celTotalEfectivo.BorderWidthLeft = PdfPCell.NO_BORDER;
                celTotalEfectivo.BorderWidthRight = PdfPCell.NO_BORDER;
                tableDatosBC1.AddCell(celTotalEfectivo);
                //----------------------------------------------------------------------------------
                #region DatosConsignarEn
                PdfPTable tableDatosConsignacion = new PdfPTable(2);
                tableDatosConsignacion.HorizontalAlignment = Element.ALIGN_RIGHT;
                tableDatosConsignacion.WidthPercentage = 100;
                float[] dimTableDatosConsignacion = new float[2];
                dimTableDatosConsignacion[0] = 1.0f;
                dimTableDatosConsignacion[1] = 1.5f;
                tableDatosConsignacion.SetWidths(dimTableDatosConsignacion);

                PdfPCell celConsignarEn1 = new PdfPCell(new Phrase(
                    "Favor Consignar en:", FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 6.7f, iTextSharp.text.Font.NORMAL)));
                celConsignarEn1.VerticalAlignment = Element.ALIGN_LEFT;
                celConsignarEn1.HorizontalAlignment = Element.ALIGN_TOP;
                celConsignarEn1.Border = PdfPCell.NO_BORDER;
                celConsignarEn1.Colspan = 2;
                tableDatosConsignacion.AddCell(celConsignarEn1);

                PdfPCell celTextBancolombia = new PdfPCell(new Phrase("Bancolombia", FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL)));
                celTextBancolombia.VerticalAlignment = Element.ALIGN_LEFT;
                celTextBancolombia.HorizontalAlignment = Element.ALIGN_TOP;
                celTextBancolombia.Border = PdfPCell.NO_BORDER;
                celTextBancolombia.Padding = 2;
                celTextBancolombia.Colspan = 1;
                tableDatosConsignacion.AddCell(celTextBancolombia);

                PdfPCell celCtaBancolombia = new PdfPCell(new Phrase("Cta. Cte. 00991132702", FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL)));
                celCtaBancolombia.VerticalAlignment = Element.ALIGN_LEFT;
                celCtaBancolombia.Padding = 2;
                celCtaBancolombia.HorizontalAlignment = Element.ALIGN_TOP;
                celCtaBancolombia.Border = PdfPCell.NO_BORDER;
                celCtaBancolombia.Colspan = 1;
                tableDatosConsignacion.AddCell(celCtaBancolombia);

                PdfPCell celTextBancoBogota = new PdfPCell(new Phrase("Banco de Bogota", FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL)));
                celTextBancoBogota.VerticalAlignment = Element.ALIGN_LEFT;
                celTextBancoBogota.HorizontalAlignment = Element.ALIGN_TOP;
                celTextBancoBogota.Border = PdfPCell.NO_BORDER;
                celTextBancoBogota.Padding = 2;
                celTextBancoBogota.Colspan = 1;
                tableDatosConsignacion.AddCell(celTextBancoBogota);

                PdfPCell celCtaBancoBogota = new PdfPCell(new Phrase("Cta. Cte. 349019737", FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL)));
                celCtaBancoBogota.VerticalAlignment = Element.ALIGN_LEFT;
                celCtaBancoBogota.HorizontalAlignment = Element.ALIGN_TOP;
                celCtaBancoBogota.Border = PdfPCell.NO_BORDER;
                celCtaBancoBogota.Padding = 2;
                celCtaBancoBogota.Colspan = 1;
                tableDatosConsignacion.AddCell(celCtaBancoBogota);

                PdfPCell celTextBancoBBVA = new PdfPCell(new Phrase("Banco BBVA", FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL)));
                celTextBancoBBVA.VerticalAlignment = Element.ALIGN_LEFT;
                celTextBancoBBVA.HorizontalAlignment = Element.ALIGN_TOP;
                celTextBancoBBVA.Border = PdfPCell.NO_BORDER;
                celTextBancoBBVA.Padding = 2;
                celTextBancoBBVA.Colspan = 1;
                tableDatosConsignacion.AddCell(celTextBancoBBVA);

                PdfPCell celCtaBancoBBVA = new PdfPCell(new Phrase("Cta. Cte. 498000488", FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL)));
                celCtaBancoBBVA.VerticalAlignment = Element.ALIGN_LEFT;
                celCtaBancoBBVA.HorizontalAlignment = Element.ALIGN_TOP;
                celCtaBancoBBVA.Border = PdfPCell.NO_BORDER;
                celCtaBancoBBVA.Padding = 2;
                celCtaBancoBBVA.Colspan = 1;
                tableDatosConsignacion.AddCell(celCtaBancoBBVA);

                PdfPCell celTextBancoOccidente = new PdfPCell(new Phrase("Banco de Occidente", FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL)));
                celTextBancoOccidente.VerticalAlignment = Element.ALIGN_LEFT;
                celTextBancoOccidente.HorizontalAlignment = Element.ALIGN_TOP;
                celTextBancoOccidente.Border = PdfPCell.NO_BORDER;
                celTextBancoOccidente.Padding = 2;
                celTextBancoOccidente.Colspan = 1;
                tableDatosConsignacion.AddCell(celTextBancoOccidente);

                PdfPCell celCtaBancoOccidente = new PdfPCell(new Phrase("Cta. Cte. 405059098", FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL)));
                celCtaBancoOccidente.VerticalAlignment = Element.ALIGN_LEFT;
                celCtaBancoOccidente.HorizontalAlignment = Element.ALIGN_TOP;
                celCtaBancoOccidente.Border = PdfPCell.NO_BORDER;
                celCtaBancoOccidente.Padding = 2;
                celCtaBancoOccidente.Colspan = 1;
                tableDatosConsignacion.AddCell(celCtaBancoOccidente);

                PdfPCell celTextBancoHBank = new PdfPCell(new Phrase("Itaú CorpBanca", FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL)));
                celTextBancoHBank.VerticalAlignment = Element.ALIGN_LEFT;
                celTextBancoHBank.HorizontalAlignment = Element.ALIGN_TOP;
                celTextBancoHBank.Border = PdfPCell.NO_BORDER;
                celTextBancoHBank.Padding = 2;
                celTextBancoHBank.Colspan = 1;
                tableDatosConsignacion.AddCell(celTextBancoHBank);

                PdfPCell celCtaBancoHBank = new PdfPCell(new Phrase("Cta. Cte. 101011062", FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL)));
                celCtaBancoHBank.VerticalAlignment = Element.ALIGN_LEFT;
                celCtaBancoHBank.HorizontalAlignment = Element.ALIGN_TOP;
                celCtaBancoHBank.Border = PdfPCell.NO_BORDER;
                celCtaBancoHBank.Padding = 2;
                celCtaBancoHBank.Colspan = 1;
                tableDatosConsignacion.AddCell(celCtaBancoHBank);
                #endregion
                //----------------------------------------------------------------------------------
                PdfPCell celDatosBC1 = new PdfPCell(tableDatosBC1);
                celDatosBC1.Border = PdfPCell.NO_BORDER;
                tableBC1.AddCell(celDatosBC1);

                PdfPCell celConsignarEn = new PdfPCell(tableDatosConsignacion);
                celConsignarEn.Border = PdfPCell.NO_BORDER;
                celConsignarEn.BorderWidthTop = 0.5f;
                tableBC1.AddCell(celConsignarEn);
                //------------------------SEGUNTA TABLA DE BC---------------------------------------------------------
                PdfDiv divTextBancoEfectivo = new PdfDiv();
                divTextBancoEfectivo.BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.DOTTED;
                divTextBancoEfectivo.Width = 582;

                PdfPTable tableTextBancoEfectivo = new PdfPTable(3);
                tableTextBancoEfectivo.WidthPercentage = 100;

                PdfPCell celTextBancoEfectivoEspacio1 = new PdfPCell(new Phrase(" "));
                celTextBancoEfectivoEspacio1.PaddingTop = 4;
                celTextBancoEfectivoEspacio1.BorderWidthRight = PdfPCell.NO_BORDER;
                celTextBancoEfectivoEspacio1.BorderWidthLeft = PdfPCell.NO_BORDER;
                celTextBancoEfectivoEspacio1.BorderWidthTop = PdfPCell.NO_BORDER;
                tableTextBancoEfectivo.AddCell(celTextBancoEfectivoEspacio1);

                PdfPCell celTextBancoEfectivoEspacio2 = new PdfPCell(new Phrase(" "));
                celTextBancoEfectivoEspacio2.PaddingTop = 4;
                celTextBancoEfectivoEspacio2.BorderWidthRight = PdfPCell.NO_BORDER;
                celTextBancoEfectivoEspacio2.BorderWidthLeft = PdfPCell.NO_BORDER;
                celTextBancoEfectivoEspacio2.HorizontalAlignment = Element.ALIGN_CENTER;
                celTextBancoEfectivoEspacio2.BorderWidthTop = PdfPCell.NO_BORDER;
                tableTextBancoEfectivo.AddCell(celTextBancoEfectivoEspacio2);

                PdfPCell celTextBancoEfectivoEspacio = new PdfPCell(new Phrase("- BANCO (EFECTIVO) -", fontTitleFactura));
                celTextBancoEfectivoEspacio.BorderWidthRight = PdfPCell.NO_BORDER;
                celTextBancoEfectivoEspacio.BorderWidthLeft = PdfPCell.NO_BORDER;
                celTextBancoEfectivoEspacio.BorderWidthTop = PdfPCell.NO_BORDER;
                tableTextBancoEfectivo.AddCell(celTextBancoEfectivoEspacio);

                divTextBancoEfectivo.AddElement(tableTextBancoEfectivo);
                //---------------------------------------------------------------------------------------------------
                PdfPTable tableBC2 = new PdfPTable(3);
                tableBC2.HorizontalAlignment = Element.ALIGN_LEFT;
                tableBC2.SetWidths(dimTableBC1);
                tableBC2.WidthPercentage = 100;

                //PdfPCell celBC2 = new PdfPCell(new Phrase("Bar Code 2"));
                PdfPCell celBC2 = new PdfPCell();
                Helpers.Compartido.BarCode(ref celBC2, BarCodeValor);

                celBC2.Border = PdfPCell.NO_BORDER;
                tableBC2.AddCell(celBC2);

                PdfPTable tableDatosBC2 = new PdfPTable(2);
                tableDatosBC2.SetWidths(dimTableDatosBC1);
                //----------------------------------------------------------------------------------
                PdfPTable tableDatosComprador2 = new PdfPTable(2);
                tableDatosComprador2.SetWidths(dimTableDatosComprador);

                tableDatosComprador2.AddCell(celprgComprador);

                PdfPCell celprgNit2 = new PdfPCell();
                celprgNit2.AddElement(new Paragraph("NIT./CC. ", fontTitle2));
                celprgNit2.Border = PdfPCell.NO_BORDER;
                celprgNit2.BorderWidthLeft = 0.5f;
                celprgNit2.BorderWidthBottom = 0.5f;
                celprgNit2.Colspan = 1;
                //celprgNit.Padding = 3;
                tableDatosComprador2.AddCell(celprgNit2);

                PdfPCell celprgValNit2 = new PdfPCell();
                celprgValNit2.AddElement(new Phrase((string)DsInvoiceAR.Tables["Customer"].Rows[0]["ResaleID"], fontTitleFactura));
                celprgValNit2.Border = PdfPCell.NO_BORDER;
                celprgValNit2.BorderWidthBottom = 0.5f;
                celprgValNit2.Colspan = 1;
                celprgValNit2.Padding = 3;
                tableDatosComprador2.AddCell(celprgValNit2);


                PdfPCell celDatosComprador2 = new PdfPCell(tableDatosComprador2);
                celDatosComprador2.BorderWidthBottom = PdfPCell.NO_BORDER;
                celDatosComprador2.BorderWidthRight = PdfPCell.NO_BORDER;
                celDatosComprador2.BorderWidthLeft = PdfPCell.NO_BORDER;
                tableDatosBC2.AddCell(celDatosComprador2);
                //----------------------------------------------------------------------------------
                PdfPTable tableTotalCheque = new PdfPTable(1);

                PdfPCell celTextTotalCheques = new PdfPCell();
                celTextTotalCheques.AddElement(new Paragraph("Total Cheques\n",
                    FontFactory.GetFont(FontFactory.HELVETICA, 6.7f, iTextSharp.text.Font.NORMAL)));
                celTextTotalCheques.BorderWidthBottom = PdfPCell.NO_BORDER;
                celTextTotalCheques.VerticalAlignment = Element.ALIGN_LEFT;
                celTextTotalCheques.HorizontalAlignment = Element.ALIGN_TOP;
                tableTotalCheque.AddCell(celTextTotalCheques);

                PdfPCell celDivTotalCheques = new PdfPCell();
                celDivTotalCheques.AddElement(divTotalEfectivo);
                celDivTotalCheques.BorderWidthTop = PdfPCell.NO_BORDER;
                tableTotalCheque.AddCell(celDivTotalCheques);

                PdfPCell celTotalCheques = new PdfPCell(tableTotalCheque);
                celTotalCheques.BorderWidthBottom = PdfPCell.NO_BORDER;
                celTotalCheques.BorderWidthLeft = PdfPCell.NO_BORDER;
                celTotalCheques.BorderWidthRight = PdfPCell.NO_BORDER;
                tableDatosBC2.AddCell(celTotalCheques);
                //----------------------------------------------------------------------------------
                PdfPTable tableBancoValores = new PdfPTable(3);


                iTextSharp.text.Font fontCheque = FontFactory.GetFont(FontFactory.HELVETICA, 6.8f, iTextSharp.text.Font.NORMAL);

                PdfPCell celTextBancoValoresBanco = new PdfPCell(new Phrase("Banco", fontCheque));
                celTextBancoValoresBanco.Padding = 3;
                celTextBancoValoresBanco.HorizontalAlignment = Element.ALIGN_CENTER;
                //celEspacioTotalCheques.Border = PdfPCell.NO_BORDER;
                tableBancoValores.AddCell(celTextBancoValoresBanco);

                PdfPCell celTextBancoValoresNoCheque = new PdfPCell(new Phrase("No.Cheque", fontCheque));
                celTextBancoValoresNoCheque.Padding = 3;
                celTextBancoValoresNoCheque.HorizontalAlignment = Element.ALIGN_CENTER;
                tableBancoValores.AddCell(celTextBancoValoresNoCheque);

                PdfPCell celTextBancoValoresValor = new PdfPCell(new Phrase("Valor", fontCheque));
                celTextBancoValoresValor.Padding = 3;
                celTextBancoValoresValor.HorizontalAlignment = Element.ALIGN_CENTER;
                tableBancoValores.AddCell(celTextBancoValoresValor);

                for (int i = 0; i < 2; i++)
                {
                    PdfPCell celBancoValoresBanco = new PdfPCell(new Phrase(" ", fontTitle));
                    celBancoValoresBanco.Padding = 2.3f;
                    tableBancoValores.AddCell(celBancoValoresBanco);

                    PdfPCell celBancoValoresNoCheque = new PdfPCell(new Phrase(" ", fontTitle));
                    celBancoValoresNoCheque.Padding = 2.3f;
                    tableBancoValores.AddCell(celBancoValoresNoCheque);

                    PdfPCell celBancoValoresValor = new PdfPCell(new Phrase(" ", fontTitle));
                    celBancoValoresValor.Padding = 2.3f;
                    tableBancoValores.AddCell(celBancoValoresValor);
                }

                PdfPCell celBancoValores = new PdfPCell(tableBancoValores);
                celBancoValores.Colspan = 2;
                tableDatosBC2.AddCell(celBancoValores);
                //----------------------------------------------------------------------------------

                PdfPCell celDatosBC2 = new PdfPCell(tableDatosBC2);
                celDatosBC2.Border = PdfPCell.NO_BORDER;
                tableBC2.AddCell(celDatosBC2);

                tableBC2.AddCell(celConsignarEn);
                //---------------------------------------------------------------------------------
                PdfDiv divTextBancoCheque = new PdfDiv();
                divTextBancoCheque.BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.DOTTED;
                divTextBancoCheque.Width = 582;

                PdfPTable tableTextBancoCheque = new PdfPTable(3);
                tableTextBancoCheque.WidthPercentage = 100;

                PdfPCell celTextBancoChequeEspacio1 = new PdfPCell(new Phrase(" "));
                //celTextNumRot.Border = 0;
                celTextBancoChequeEspacio1.PaddingTop = 4;
                celTextBancoChequeEspacio1.Border = PdfPCell.NO_BORDER;
                tableTextBancoCheque.AddCell(celTextBancoChequeEspacio1);

                PdfPCell celTextBancoChequeEspacio2 = new PdfPCell(new Phrase(" "));
                //celTextCopia.Border = 0;
                celTextBancoChequeEspacio2.PaddingTop = 4;
                celTextBancoChequeEspacio2.Border = PdfPCell.NO_BORDER;
                tableTextBancoCheque.AddCell(celTextBancoChequeEspacio2);

                PdfPCell celTextBancoCheque = new PdfPCell(new Phrase("- BANCO (CHEQUE) -", fontTitleFactura));
                celTextBancoCheque.Border = PdfPCell.NO_BORDER;
                tableTextBancoCheque.AddCell(celTextBancoCheque);

                divTextBancoCheque.AddElement(tableTextBancoCheque);
                #endregion

                document.Add(Abracol.FacturaNacional.Encabezado(RutaImg));
                document.Add(divEspacio);
                document.Add(Abracol.FacturaNacional.DatosCliente(DsInvoiceAR, CUFE, QRInvoice));
                document.Add(divEspacio2);
                document.Add(divEspacio2);
                document.Add(tableTituloUnidades);
                document.Add(tableUnidades);
                document.Add(tableEspacioUnidades);
                //document.Add(tableLineaFinal);
                document.Add(tableObsTotales);
                document.Add(divEspacio2);
                document.Add(tableResolucion);
                //document.Add(divEspacio2);
                document.Add(divTextCopia);
                document.Add(divEspacio2);
                document.Add(divEspacio2);
                document.Add(tableTextNitReferencia);
                document.Add(tableBC1);
                document.Add(divEspacio2);
                document.Add(divEspacio2);
                document.Add(divTextBancoEfectivo);
                document.Add(divEspacio2);
                document.Add(divEspacio2);
                document.Add(tableTextNitReferencia);
                document.Add(tableBC2);
                document.Add(divEspacio2);
                document.Add(divEspacio2);
                document.Add(divTextBancoCheque);
                /*PIE DE PAGINA*/
                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();
                Helpers.Simex.AddPageNumberPagToSimexExp($"{Ruta}{NomArchivo}", dimxSimex, dimYSimex + 10, Helpers.Fuentes.SimexLeyenda);
                RutaPdf = NomArchivo;
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return false;
            }
        }
        public bool FacturaNotaCreditoABRACOL(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {
            bool esLocal = Helpers.Compartido.EsLocal;
            string Ruta = string.Empty;
            string RutaImg = string.Empty;
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            if (esLocal)
            {
                Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
                RutaImg = $@"{AppDomain.CurrentDomain.BaseDirectory}\Logo_890911327.png";
            }
            else
            {
                Ruta = HttpContext.Current.Server.MapPath("~/PDF/" + NIT + "/");
                RutaImg = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";
            }
            NomArchivo = string.Empty;
            NomArchivo = NombreInvoice + ".pdf";

            try
            {
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.
                Document document = new Document(iTextSharp.text.PageSize.LETTER);
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;

                // step 3: we open the document     
                document.Open();

                PdfDiv divEspacio = new PdfDiv();
                divEspacio.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio.Height = 10;
                divEspacio.Width = 130;

                PdfDiv divEspacio2 = new PdfDiv();
                divEspacio2.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio2.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio2.Height = 2;
                divEspacio2.Width = 130;

                //FUENTES----------------------------------------------------------------------------------------------------------
                iTextSharp.text.Font fontTitleBold = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustom = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustomEncabezado = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 14, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustomBold = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                //FUENTES---------------------------------------------------------------------------------------------------------

                #region ENCABEZADO
                PdfPTable tableEncabezado = new PdfPTable(3);
                //Dimenciones.
                float[] DimTableEncabezado = new float[3];
                DimTableEncabezado[0] = 2.0F;//
                DimTableEncabezado[1] = 2.0F;//
                DimTableEncabezado[2] = 0.48F;//
                tableEncabezado.WidthPercentage = 100;
                tableEncabezado.SetWidths(DimTableEncabezado);

                PdfPTable tableLogo = new PdfPTable(1);
                System.Drawing.Image logo = null;
                var requestLogo = WebRequest.Create(RutaImg);

                using (var responseLogo = requestLogo.GetResponse())
                using (var streamLogo = responseLogo.GetResponseStream())
                {
                    logo = Bitmap.FromStream(streamLogo);
                }


                PdfDiv divLogo = new PdfDiv();
                divLogo.Float = iTextSharp.text.pdf.PdfDiv.FloatType.LEFT;
                divLogo.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divLogo.BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID;
                divLogo.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                divLogo.Height = 60;
                divLogo.Width = 145;
                //divLogo.BackgroundColor = BaseColor.LIGHT_GRAY;
                iTextSharp.text.Image ImgLogo = iTextSharp.text.Image.GetInstance(logo, BaseColor.WHITE);
                divLogo.BackgroundImage = ImgLogo;

                iTextSharp.text.pdf.PdfPCell celImgLogo = new iTextSharp.text.pdf.PdfPCell();
                celImgLogo.AddElement(divLogo);
                celImgLogo.Colspan = 1;
                celImgLogo.Padding = 3;
                celImgLogo.Border = 0;
                celImgLogo.HorizontalAlignment = Element.ALIGN_CENTER;
                celImgLogo.VerticalAlignment = Element.ALIGN_TOP;
                tableLogo.AddCell(celImgLogo);

                iTextSharp.text.pdf.PdfPCell celDetalleEmpresa = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    "NIT:  890911327-1  Reg. Comun  Reg.11-1021-01\n" +
                    "Autopista Norte Km.20 Girardota-Ant\nTel: (054)2890033 Planta", fontTitleBold));
                celDetalleEmpresa.Colspan = 1;
                celDetalleEmpresa.Border = 0;
                celDetalleEmpresa.HorizontalAlignment = Element.ALIGN_LEFT;
                celDetalleEmpresa.VerticalAlignment = Element.ALIGN_TOP;
                tableLogo.AddCell(celDetalleEmpresa);

                iTextSharp.text.pdf.PdfPCell celLogo = new iTextSharp.text.pdf.PdfPCell(tableLogo);
                celLogo.Border = PdfPCell.NO_BORDER;
                tableEncabezado.AddCell(celLogo);
                //----------------------------------------------------------------------------------------------------
                Paragraph prgNotaCredito = new Paragraph("             " +
                    "NOTA CREDITO", fontCustomEncabezado);
                //prgNotaCredito.Alignment = Element.ALIGN_CENTER;
                Paragraph prgNC = new Paragraph("\n**" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"].ToString() + "**", fontCustomEncabezado);
                prgNC.Alignment = Element.ALIGN_RIGHT;
                iTextSharp.text.pdf.PdfPCell celDetalles = new iTextSharp.text.pdf.PdfPCell();
                celDetalles.AddElement(prgNotaCredito);
                celDetalles.AddElement(prgNC);
                celDetalles.Colspan = 1;
                celDetalles.Padding = 3;
                //celDetalles.BackgroundColor = BaseColor.LIGHT_GRAY;
                celDetalles.Border = 0;
                celDetalles.HorizontalAlignment = Element.ALIGN_CENTER;
                celDetalles.VerticalAlignment = Element.ALIGN_TOP;
                tableEncabezado.AddCell(celDetalles);

                iTextSharp.text.pdf.PdfPCell celEspacioEncabezado = new iTextSharp.text.pdf.PdfPCell(new Phrase(" ", fontTitleBold));
                celEspacioEncabezado.Border = 0;
                tableEncabezado.AddCell(celEspacioEncabezado);
                #endregion

                #region DATOS CUSTOMER Y VENDEDOR
                //------------------------------------------------------------------------------------------------
                PdfPTable tableDatosFacturaCustomer = new PdfPTable(3);
                //Dimenciones.
                float[] DimencionDatosFacturar = new float[3];
                DimencionDatosFacturar[0] = 1.0F;//
                DimencionDatosFacturar[1] = 0.01F;//
                DimencionDatosFacturar[2] = 1.0F;//

                tableDatosFacturaCustomer.WidthPercentage = 100;
                tableDatosFacturaCustomer.SetWidths(DimencionDatosFacturar);
                //----------------------------------------------------------------------------------------------
                PdfPTable tableDatosCustomer = new PdfPTable(1);
                tableDatosCustomer.WidthPercentage = 100;

                iTextSharp.text.pdf.PdfPCell celTextSRS = new iTextSharp.text.pdf.PdfPCell();
                celTextSRS.AddElement(new Paragraph("SRS:", fontTitleBold));
                celTextSRS.AddElement(new Paragraph((string)DsInvoiceAR.Tables["Customer"].Rows[0]["Name"], fontTitle));
                celTextSRS.Colspan = 2;
                celTextSRS.Padding = 2;
                celTextSRS.Border = 0;
                celTextSRS.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextSRS.VerticalAlignment = Element.ALIGN_TOP;
                tableDatosCustomer.AddCell(celTextSRS);
                //-------------------------------------------------------------------------------
                PdfPTable tableNitCustomer = new PdfPTable(2);
                float[] DimTableNitCustomer = new float[2];
                DimTableNitCustomer[0] = 0.16F;//
                DimTableNitCustomer[1] = 2.0F;//
                tableNitCustomer.WidthPercentage = 100;
                tableNitCustomer.SetWidths(DimTableNitCustomer);

                iTextSharp.text.pdf.PdfPCell celTexNitCustomer = new iTextSharp.text.pdf.PdfPCell(new Phrase("NIT:", fontTitleBold));
                celTexNitCustomer.Colspan = 1;
                celTexNitCustomer.Padding = 3;
                celTexNitCustomer.Border = 0;
                celTexNitCustomer.HorizontalAlignment = Element.ALIGN_LEFT;
                celTexNitCustomer.VerticalAlignment = Element.ALIGN_TOP;
                tableNitCustomer.AddCell(celTexNitCustomer);

                iTextSharp.text.pdf.PdfPCell celValNitCustomer = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    (string)DsInvoiceAR.Tables["Customer"].Rows[0]["ResaleID"],
                    fontTitle));
                celValNitCustomer.Colspan = 1;
                celValNitCustomer.Padding = 3;
                celValNitCustomer.Border = 0;
                celValNitCustomer.HorizontalAlignment = Element.ALIGN_LEFT;
                celValNitCustomer.VerticalAlignment = Element.ALIGN_TOP;
                tableNitCustomer.AddCell(celValNitCustomer);

                iTextSharp.text.pdf.PdfPCell celNitCustomer = new iTextSharp.text.pdf.PdfPCell(tableNitCustomer);
                celNitCustomer.Border = PdfPCell.NO_BORDER;
                //celNitCustomer.BorderWidthTop = PdfPCell.NO_BORDER;
                tableDatosCustomer.AddCell(celNitCustomer);
                //-------------------------------------------------------------------------------
                iTextSharp.text.pdf.PdfPCell celTextDomicEmbarque = new iTextSharp.text.pdf.PdfPCell(new Phrase("DOMIC. EMBARQUE",
                    fontTitleBold));
                celTextDomicEmbarque.Colspan = 1;
                celTextDomicEmbarque.Padding = 3;
                celTextDomicEmbarque.Border = 0;
                celTextDomicEmbarque.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextDomicEmbarque.VerticalAlignment = Element.ALIGN_TOP;
                tableDatosCustomer.AddCell(celTextDomicEmbarque);

                iTextSharp.text.pdf.PdfPCell celValDomicEmbarque = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    (string)DsInvoiceAR.Tables["Customer"].Rows[0]["Address1"], fontTitle));
                celValDomicEmbarque.Colspan = 1;
                celValDomicEmbarque.Padding = 3;
                celValDomicEmbarque.PaddingBottom = 7;
                celValDomicEmbarque.Border = 0;
                celValDomicEmbarque.HorizontalAlignment = Element.ALIGN_LEFT;
                celValDomicEmbarque.VerticalAlignment = Element.ALIGN_TOP;
                tableDatosCustomer.AddCell(celValDomicEmbarque);
                //----------------------------------------------------------------------
                PdfPTable tableTelFax = new PdfPTable(4);
                float[] DimtableTelFax = new float[4];
                DimtableTelFax[0] = 0.2F;//
                DimtableTelFax[1] = 1.0F;//
                DimtableTelFax[2] = 0.2F;//
                DimtableTelFax[3] = 1.0F;//
                tableTelFax.WidthPercentage = 100;
                tableTelFax.SetWidths(DimtableTelFax);

                iTextSharp.text.pdf.PdfPCell celTextTelCustomer = new iTextSharp.text.pdf.PdfPCell(new Phrase("TEL:", fontTitleBold));
                celTextTelCustomer.Colspan = 1;
                celTextTelCustomer.Padding = 3;
                celTextTelCustomer.Border = 0;
                celTextTelCustomer.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextTelCustomer.VerticalAlignment = Element.ALIGN_TOP;
                tableTelFax.AddCell(celTextTelCustomer);

                iTextSharp.text.pdf.PdfPCell celValTelCustomer = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    (string)DsInvoiceAR.Tables["Customer"].Rows[0]["PhoneNum"], fontTitle));
                celValTelCustomer.Colspan = 1;
                celValTelCustomer.Padding = 3;
                celValTelCustomer.Border = 0;
                celValTelCustomer.HorizontalAlignment = Element.ALIGN_LEFT;
                celValTelCustomer.VerticalAlignment = Element.ALIGN_TOP;
                tableTelFax.AddCell(celValTelCustomer);

                iTextSharp.text.pdf.PdfPCell celTextFaxCustomer = new iTextSharp.text.pdf.PdfPCell(
                    new Phrase("FAX:", fontTitleBold));
                celTextFaxCustomer.Colspan = 1;
                celTextFaxCustomer.Padding = 3;
                celTextFaxCustomer.Border = 0;
                celTextFaxCustomer.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextFaxCustomer.VerticalAlignment = Element.ALIGN_TOP;
                tableTelFax.AddCell(celTextFaxCustomer);

                iTextSharp.text.pdf.PdfPCell celValFaxCustomer = new iTextSharp.text.pdf.PdfPCell(
                    new Phrase((string)DsInvoiceAR.Tables["Customer"].Rows[0]["FaxNum"], fontTitle));
                celValFaxCustomer.Colspan = 1;
                celValFaxCustomer.Padding = 3;
                celValFaxCustomer.Border = 0;
                celValFaxCustomer.HorizontalAlignment = Element.ALIGN_LEFT;
                celValFaxCustomer.VerticalAlignment = Element.ALIGN_TOP;
                tableTelFax.AddCell(celValFaxCustomer);

                iTextSharp.text.pdf.PdfPCell celTelFax = new iTextSharp.text.pdf.PdfPCell(tableTelFax);
                celTelFax.PaddingBottom = 7;
                celTelFax.Border = PdfPCell.NO_BORDER;
                tableDatosCustomer.AddCell(celTelFax);
                //-------------------------------------------------------------------------------
                iTextSharp.text.pdf.PdfPCell celDatosCustomer = new iTextSharp.text.pdf.PdfPCell(tableDatosCustomer);
                //celDatosCustomer.Border = 0;
                tableDatosFacturaCustomer.AddCell(celDatosCustomer);
                //-------------------------------------------------------------------------------
                iTextSharp.text.pdf.PdfPCell celEspacio2 = new iTextSharp.text.pdf.PdfPCell(new Phrase(" ", fontCustom));
                celEspacio2.Border = 0;
                tableDatosFacturaCustomer.AddCell(celEspacio2);
                //-------------------------------------------------------------------------------
                PdfPTable tableDatosFactura = new PdfPTable(3);
                float[] DimTableDatosFactura = new float[3];
                DimTableDatosFactura[0] = 0.8F;//
                DimTableDatosFactura[1] = 2.0F;//

                tableDatosFactura.WidthPercentage = 100;
                //tableDatosFactura.SetWidths(DimTableDatosFactura);

                iTextSharp.text.pdf.PdfPCell celTextFechaEmision = new iTextSharp.text.pdf.PdfPCell(new Phrase("FECHA EMISION",
                    fontTitleBold));
                celTextFechaEmision.Colspan = 3;
                celTextFechaEmision.Padding = 5;
                celTextFechaEmision.BorderWidthBottom = PdfPCell.NO_BORDER;
                celTextFechaEmision.HorizontalAlignment = Element.ALIGN_CENTER;
                celTextFechaEmision.VerticalAlignment = Element.ALIGN_CENTER;
                tableDatosFactura.AddCell(celTextFechaEmision);

                iTextSharp.text.pdf.PdfPCell celValFechaEmision = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).ToString("dd/MM/yyyy"),
                    fontTitle));
                celValFechaEmision.Colspan = 3;
                celValFechaEmision.Padding = 5;
                celValFechaEmision.PaddingBottom = 10;
                celValFechaEmision.BorderWidthTop = PdfPCell.NO_BORDER;
                celValFechaEmision.HorizontalAlignment = Element.ALIGN_CENTER;
                celValFechaEmision.VerticalAlignment = Element.ALIGN_CENTER;
                tableDatosFactura.AddCell(celValFechaEmision);
                //------------------------------------------------------------------------------------------------
                iTextSharp.text.pdf.PdfPCell celEspacioDatosFactura = new iTextSharp.text.pdf.PdfPCell(new Phrase(" ",
                    FontFactory.GetFont(FontFactory.HELVETICA, 0.001f)));
                celEspacioDatosFactura.Colspan = 3;
                celEspacioDatosFactura.Border = PdfPCell.NO_BORDER;
                celEspacioDatosFactura.HorizontalAlignment = Element.ALIGN_CENTER;
                celEspacioDatosFactura.VerticalAlignment = Element.ALIGN_CENTER;
                tableDatosFactura.AddCell(celEspacioDatosFactura);
                //------------------------------------------------------------------------------------------------
                Paragraph prgVendedor = new Paragraph("VENDEDOR", fontTitle);
                prgVendedor.Alignment = Element.ALIGN_CENTER;

                Paragraph prgValVendedor = new Paragraph(DsInvoiceAR.Tables["InvcHead"].Rows[0]["SalesRepCode1"].ToString(), fontTitleBold);
                prgValVendedor.Alignment = Element.ALIGN_CENTER;
                iTextSharp.text.pdf.PdfPCell celTextVendedor = new iTextSharp.text.pdf.PdfPCell();
                celTextVendedor.AddElement(prgVendedor);
                celTextVendedor.AddElement(prgValVendedor);
                celTextVendedor.Colspan = 1;
                celTextVendedor.Padding = 3;
                celTextVendedor.Border = PdfPCell.NO_BORDER;
                celTextVendedor.BorderWidthTop = 0.5f;
                celTextVendedor.BorderWidthLeft = 0.5f;
                celTextVendedor.HorizontalAlignment = Element.ALIGN_CENTER;
                celTextVendedor.VerticalAlignment = Element.ALIGN_TOP;
                tableDatosFactura.AddCell(celTextVendedor);

                Paragraph prgCodVendedor = new Paragraph("COD.VENDEDOR", fontTitle);
                prgCodVendedor.Alignment = Element.ALIGN_CENTER;

                Paragraph prgValCodVendedor = new Paragraph(DsInvoiceAR.Tables["InvcHead"].Rows[0]["SalesRepCode1"].ToString(),
                    fontTitleBold);
                prgValCodVendedor.Alignment = Element.ALIGN_CENTER;
                iTextSharp.text.pdf.PdfPCell celTextCodVendedor = new iTextSharp.text.pdf.PdfPCell();
                celTextCodVendedor.AddElement(prgCodVendedor);
                celTextCodVendedor.AddElement(prgValCodVendedor);
                celTextCodVendedor.Colspan = 1;
                celTextCodVendedor.Padding = 3;
                celTextCodVendedor.Border = PdfPCell.NO_BORDER;
                celTextCodVendedor.BorderWidthTop = 0.5f;
                celTextCodVendedor.HorizontalAlignment = Element.ALIGN_CENTER;
                celTextCodVendedor.VerticalAlignment = Element.ALIGN_TOP;
                tableDatosFactura.AddCell(celTextCodVendedor);

                Paragraph prgNoCompra = new Paragraph("O. COMPRA N°", fontTitle);
                prgNoCompra.Alignment = Element.ALIGN_CENTER;

                Paragraph prgValNoCompra = new Paragraph((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["PONum"]
                    , fontTitleBold);
                prgValNoCompra.Alignment = Element.ALIGN_CENTER;
                iTextSharp.text.pdf.PdfPCell celTextNoCompra = new iTextSharp.text.pdf.PdfPCell();
                celTextNoCompra.AddElement(prgNoCompra);
                celTextNoCompra.AddElement(prgValNoCompra);
                celTextNoCompra.Colspan = 1;
                celTextNoCompra.Padding = 3;
                celTextNoCompra.Border = PdfPCell.NO_BORDER;
                celTextNoCompra.BorderWidthTop = 0.5f;
                celTextNoCompra.BorderWidthRight = 0.5f;
                celTextNoCompra.HorizontalAlignment = Element.ALIGN_CENTER;
                celTextNoCompra.VerticalAlignment = Element.ALIGN_TOP;
                tableDatosFactura.AddCell(celTextNoCompra);

                iTextSharp.text.pdf.PdfPCell celDatosFactura = new iTextSharp.text.pdf.PdfPCell(tableDatosFactura);
                celDatosFactura.HorizontalAlignment = Element.ALIGN_CENTER;
                celDatosFactura.Border = PdfPCell.NO_BORDER;
                celDatosFactura.BorderWidthBottom = 0.1f;
                tableDatosFacturaCustomer.AddCell(celDatosFactura);
                //-------------------------------------------------------------------------------------- 
                #endregion

                #region UNIDADES
                PdfPTable tableTituloUnidades = new PdfPTable(9);
                //Dimenciones.
                float[] DimencionUnidades = new float[9];
                DimencionUnidades[0] = 0.178F;//L
                DimencionUnidades[1] = 0.6F;//Codigo
                DimencionUnidades[2] = 0.5F;//Cantidad
                DimencionUnidades[3] = 0.328F;//Unidad
                DimencionUnidades[4] = 2.0F;//Descripcion
                DimencionUnidades[5] = 0.585F;//Valor Unitario
                DimencionUnidades[6] = 0.378F;//Descuento
                DimencionUnidades[7] = 0.5F;//Valor Total
                DimencionUnidades[8] = 0.28F;//%IVA

                tableTituloUnidades.WidthPercentage = 100;
                tableTituloUnidades.SetWidths(DimencionUnidades);

                iTextSharp.text.pdf.PdfPCell celTextL = new iTextSharp.text.pdf.PdfPCell(new Phrase("L", fontTitleBold));
                celTextL.Colspan = 1;
                celTextL.Padding = 3;
                celTextL.Border = 0;
                celTextL.BorderWidthBottom = 1;
                celTextL.BackgroundColor = BaseColor.LIGHT_GRAY;
                celTextL.HorizontalAlignment = Element.ALIGN_CENTER;
                celTextL.VerticalAlignment = Element.ALIGN_TOP;

                tableTituloUnidades.AddCell(celTextL);

                iTextSharp.text.pdf.PdfPCell celTextCodigo = new iTextSharp.text.pdf.PdfPCell(new Phrase("CODIGO", fontTitleBold));
                celTextCodigo.Colspan = 1;
                celTextCodigo.Padding = 3;
                celTextCodigo.BackgroundColor = BaseColor.LIGHT_GRAY;
                celTextCodigo.Border = 0;
                celTextCodigo.BorderWidthBottom = 1;
                celTextCodigo.HorizontalAlignment = Element.ALIGN_CENTER;
                celTextCodigo.VerticalAlignment = Element.ALIGN_TOP;

                tableTituloUnidades.AddCell(celTextCodigo);

                iTextSharp.text.pdf.PdfPCell celTextCantidad = new iTextSharp.text.pdf.PdfPCell(new Phrase("CANT", fontTitleBold));
                celTextCantidad.Colspan = 1;
                celTextCantidad.Padding = 3;
                celTextCantidad.Border = 0;
                celTextCantidad.BorderWidthBottom = 1;
                celTextCantidad.BackgroundColor = BaseColor.LIGHT_GRAY;
                celTextCantidad.HorizontalAlignment = Element.ALIGN_CENTER;
                celTextCantidad.VerticalAlignment = Element.ALIGN_TOP;

                tableTituloUnidades.AddCell(celTextCantidad);

                iTextSharp.text.pdf.PdfPCell celTextUnidad = new iTextSharp.text.pdf.PdfPCell(new Phrase("UND", fontTitleBold));
                celTextUnidad.Colspan = 1;
                celTextUnidad.Padding = 3;
                celTextUnidad.BackgroundColor = BaseColor.LIGHT_GRAY;
                celTextUnidad.Border = 0;
                celTextUnidad.BorderWidthBottom = 1;
                celTextUnidad.HorizontalAlignment = Element.ALIGN_CENTER;
                celTextUnidad.VerticalAlignment = Element.ALIGN_TOP;

                tableTituloUnidades.AddCell(celTextUnidad);

                iTextSharp.text.pdf.PdfPCell celTextDesc = new iTextSharp.text.pdf.PdfPCell(new Phrase("DESCRIPCION", fontTitleBold));
                celTextDesc.Colspan = 1;
                celTextDesc.Padding = 3;
                celTextDesc.BackgroundColor = BaseColor.LIGHT_GRAY;
                celTextDesc.Border = 0;
                celTextDesc.BorderWidthBottom = 1;
                celTextDesc.HorizontalAlignment = Element.ALIGN_CENTER;
                celTextDesc.VerticalAlignment = Element.ALIGN_TOP;

                tableTituloUnidades.AddCell(celTextDesc);

                iTextSharp.text.pdf.PdfPCell celTextValorUnitario = new iTextSharp.text.pdf.PdfPCell(new Phrase("V. UNIT", fontTitleBold));
                celTextValorUnitario.Colspan = 1;
                celTextValorUnitario.Border = 0;
                celTextValorUnitario.BorderWidthBottom = 1;
                celTextValorUnitario.BackgroundColor = BaseColor.LIGHT_GRAY;
                celTextValorUnitario.Padding = 3;
                celTextValorUnitario.HorizontalAlignment = Element.ALIGN_CENTER;
                celTextValorUnitario.VerticalAlignment = Element.ALIGN_TOP;

                tableTituloUnidades.AddCell(celTextValorUnitario);

                iTextSharp.text.pdf.PdfPCell celTextPDescuento = new iTextSharp.text.pdf.PdfPCell(new Phrase("% D", fontTitleBold));
                celTextPDescuento.Colspan = 1;
                celTextPDescuento.Padding = 3;
                celTextPDescuento.Border = 0;
                celTextPDescuento.BorderWidthBottom = 1;
                celTextPDescuento.BackgroundColor = BaseColor.LIGHT_GRAY;
                celTextPDescuento.HorizontalAlignment = Element.ALIGN_CENTER;
                celTextPDescuento.VerticalAlignment = Element.ALIGN_TOP;

                tableTituloUnidades.AddCell(celTextPDescuento);

                iTextSharp.text.pdf.PdfPCell celTextValorTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase("V. TOTAL", fontTitleBold));
                celTextValorTotal.Colspan = 1;
                celTextValorTotal.Padding = 3;
                celTextValorTotal.Border = 0;
                celTextValorTotal.BorderWidthBottom = 1;
                celTextValorTotal.BackgroundColor = BaseColor.LIGHT_GRAY;
                celTextValorTotal.HorizontalAlignment = Element.ALIGN_CENTER;
                celTextValorTotal.VerticalAlignment = Element.ALIGN_TOP;

                tableTituloUnidades.AddCell(celTextValorTotal);

                iTextSharp.text.pdf.PdfPCell celTextIva = new iTextSharp.text.pdf.PdfPCell(new Phrase("% IVA", fontTitleBold));
                celTextIva.Colspan = 1;
                celTextIva.Padding = 3;
                celTextIva.Border = 0;
                celTextIva.BorderWidthBottom = 1;
                celTextIva.BackgroundColor = BaseColor.LIGHT_GRAY;
                celTextIva.HorizontalAlignment = Element.ALIGN_CENTER;
                celTextIva.VerticalAlignment = Element.ALIGN_TOP;

                tableTituloUnidades.AddCell(celTextIva);

                PdfPTable tableUnidades = new PdfPTable(9);
                tableUnidades.WidthPercentage = 100;
                tableUnidades.SetWidths(DimencionUnidades);
                decimal totalDescuento = 0;

                foreach (DataRow InvoiceLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
                {
                    if (!AddUnidadesNotaCreditoAbracol(InvoiceLine, ref tableUnidades, fontCustom, DsInvoiceAR))
                        return false;

                    totalDescuento += decimal.Parse((string)InvoiceLine["DspDocDiscount"]);
                }

                int numAdLineas = 20 - DsInvoiceAR.Tables["InvcDtl"].Rows.Count;
                //int numAdLineas = 27 - numLin;
                PdfPTable tableEspacioUnidades = new PdfPTable(9);
                tableEspacioUnidades.WidthPercentage = 100;
                tableEspacioUnidades.SetWidths(DimencionUnidades);

                for (int i = 0; i < numAdLineas; i++)
                    AddUnidadesNotaCreditoAbracol(ref tableEspacioUnidades);
                #endregion

                #region TOTALES
                PdfPTable tableValLetras = new PdfPTable(3);
                //Dimenciones.
                float[] DimTableValLetras = new float[3];
                DimTableValLetras[0] = 0.2F;//
                DimTableValLetras[1] = 3.0F;//
                DimTableValLetras[2] = 1.5F;//
                tableValLetras.WidthPercentage = 100;
                tableValLetras.SetWidths(DimTableValLetras);

                iTextSharp.text.pdf.PdfPCell celTextSon = new iTextSharp.text.pdf.PdfPCell(new Phrase("SON", fontTitleBold));
                celTextSon.Colspan = 1;
                celTextSon.Padding = 3;
                celTextSon.BorderWidthRight = 0;
                //celTextSon.BorderWidthBottom = 1;
                celTextSon.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextSon.VerticalAlignment = Element.ALIGN_TOP;
                tableValLetras.AddCell(celTextSon);

                iTextSharp.text.pdf.PdfPCell celTextValLetras = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    $"**{Nroenletras((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"])}", fontTitle));
                celTextValLetras.Colspan = 1;
                celTextValLetras.Padding = 3;
                celTextValLetras.BorderWidthLeft = 0;
                celTextValLetras.BorderWidthRight = 0;
                celTextValLetras.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextValLetras.VerticalAlignment = Element.ALIGN_TOP;
                tableValLetras.AddCell(celTextValLetras);

                PdfPTable tableTotalBrutoDescuento = new PdfPTable(3);
                float[] DimTableTotalBrutoDescuento = new float[3];
                DimTableTotalBrutoDescuento[0] = 0.5F;//
                DimTableTotalBrutoDescuento[1] = 1.0F;//
                DimTableTotalBrutoDescuento[2] = 0.2F;//
                tableTotalBrutoDescuento.WidthPercentage = 100;
                tableTotalBrutoDescuento.SetWidths(DimTableTotalBrutoDescuento);

                iTextSharp.text.pdf.PdfPCell celTextTotalBruto = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    "TOT. BRUTO", fontTitleBold));
                celTextTotalBruto.Border = PdfPCell.NO_BORDER;
                tableTotalBrutoDescuento.AddCell(celTextTotalBruto);

                //iTextSharp.text.pdf.PdfPCell celValTotalBruto = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                //    decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubTotal"]).ToString("C2"), fontTitle));
                iTextSharp.text.pdf.PdfPCell celValTotalBruto = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number02"]).ToString("C2"), fontTitle));
                celValTotalBruto.HorizontalAlignment = Element.ALIGN_RIGHT;
                celValTotalBruto.VerticalAlignment = Element.ALIGN_TOP;
                celValTotalBruto.Border = PdfPCell.NO_BORDER;
                tableTotalBrutoDescuento.AddCell(celValTotalBruto);

                iTextSharp.text.pdf.PdfPCell celEspacioTotalBruto = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    " ", fontTitleBold));
                celEspacioTotalBruto.Border = PdfPCell.NO_BORDER;
                tableTotalBrutoDescuento.AddCell(celEspacioTotalBruto);

                iTextSharp.text.pdf.PdfPCell celTextTotalDescuento = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    "DESCUENTO", fontTitleBold));
                celTextTotalDescuento.Border = PdfPCell.NO_BORDER;
                tableTotalBrutoDescuento.AddCell(celTextTotalDescuento);

                iTextSharp.text.pdf.PdfPCell celValTotalDescuento = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    totalDescuento.ToString("C2"), fontTitle));
                celValTotalDescuento.Border = PdfPCell.NO_BORDER;
                celValTotalDescuento.HorizontalAlignment = Element.ALIGN_RIGHT;
                celValTotalDescuento.VerticalAlignment = Element.ALIGN_TOP;
                tableTotalBrutoDescuento.AddCell(celValTotalDescuento);

                tableTotalBrutoDescuento.AddCell(celEspacioTotalBruto);

                iTextSharp.text.pdf.PdfPCell celTotalBrutoDescuento = new iTextSharp.text.pdf.PdfPCell(tableTotalBrutoDescuento);
                celTotalBrutoDescuento.Colspan = 1;
                celTotalBrutoDescuento.Padding = 3;
                celTotalBrutoDescuento.BorderWidthLeft = 0;
                celTotalBrutoDescuento.HorizontalAlignment = Element.ALIGN_LEFT;
                celTotalBrutoDescuento.VerticalAlignment = Element.ALIGN_TOP;
                tableValLetras.AddCell(celTotalBrutoDescuento);
                //----------------------------------------------------------------------------------------------
                PdfPTable tableObservaciones = new PdfPTable(3);
                //Dimenciones.
                float[] DimTableObservaciones = new float[3];
                DimTableObservaciones[0] = 1.0F;//
                DimTableObservaciones[1] = 1.0F;//
                DimTableObservaciones[2] = 1.3F;//
                tableObservaciones.WidthPercentage = 100;
                tableObservaciones.SetWidths(DimTableObservaciones);

                iTextSharp.text.pdf.PdfPCell celTextObservaciones = new iTextSharp.text.pdf.PdfPCell();
                celTextObservaciones.AddElement(new Phrase(
                    "OBSERVACIONES:", fontTitleBold));
                celTextObservaciones.AddElement(new Phrase(
                    (string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceComment"], fontTitle));
                //celTextTotalDescuento.Border = PdfPCell.NO_BORDER;
                tableObservaciones.AddCell(celTextObservaciones);

                iTextSharp.text.pdf.PdfPCell celTextElaboroAprobo = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    "(ELABORO-APROBO)", fontTitleBold));
                tableObservaciones.AddCell(celTextElaboroAprobo);

                PdfPTable tableTotalFletesIva = new PdfPTable(3);
                float[] DimTableTotalTotalFletesIva = new float[3];
                DimTableTotalTotalFletesIva[0] = 0.8F;//
                DimTableTotalTotalFletesIva[1] = 1.0F;//
                DimTableTotalTotalFletesIva[2] = 0.3F;//
                tableTotalFletesIva.WidthPercentage = 100;
                tableTotalFletesIva.SetWidths(DimTableTotalTotalFletesIva);

                iTextSharp.text.pdf.PdfPCell celTextVrFletes = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    "VR FLETES SUBTOTAL", fontTitleBold));
                celTextVrFletes.Border = PdfPCell.NO_BORDER;
                celTextVrFletes.BorderWidthBottom = 1;
                celTextVrFletes.HorizontalAlignment = Element.ALIGN_CENTER;
                tableTotalFletesIva.AddCell(celTextVrFletes);

                decimal ValorFlete = 0;
                Helpers.Abracol.GetFletes(DsInvoiceAR, ref ValorFlete);
                iTextSharp.text.pdf.PdfPCell celValVrFletes = new iTextSharp.text.pdf.PdfPCell(new Phrase(ValorFlete.ToString("C2"), fontTitle));
                celValVrFletes.HorizontalAlignment = Element.ALIGN_RIGHT;
                celValVrFletes.VerticalAlignment = Element.ALIGN_TOP;
                celValVrFletes.Border = PdfPCell.NO_BORDER;
                celValVrFletes.BorderWidthBottom = 1;
                tableTotalFletesIva.AddCell(celValVrFletes);

                iTextSharp.text.pdf.PdfPCell celEspacioVrFletes = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    " ", fontTitleBold));
                celEspacioVrFletes.Border = PdfPCell.NO_BORDER;
                celEspacioVrFletes.BorderWidthBottom = 1;
                tableTotalFletesIva.AddCell(celEspacioVrFletes);

                iTextSharp.text.pdf.PdfPCell celTextValorIva = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    "VALOR IVA", fontTitleBold));
                celTextValorIva.HorizontalAlignment = Element.ALIGN_CENTER;
                celTextValorIva.Border = PdfPCell.NO_BORDER;
                tableTotalFletesIva.AddCell(celTextValorIva);

                iTextSharp.text.pdf.PdfPCell celValValorIVA = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    GetValImpuestoByID("01", DsInvoiceAR).ToString("C2"), fontTitle));
                celValValorIVA.Border = PdfPCell.NO_BORDER;
                celValValorIVA.HorizontalAlignment = Element.ALIGN_RIGHT;
                celValValorIVA.VerticalAlignment = Element.ALIGN_TOP;
                tableTotalFletesIva.AddCell(celValValorIVA);

                celEspacioVrFletes.BorderWidthBottom = 0;
                tableTotalFletesIva.AddCell(celEspacioTotalBruto);

                iTextSharp.text.pdf.PdfPCell celTextReteIva = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    "RETE IVA 15%", fontTitleBold));
                celTextReteIva.HorizontalAlignment = Element.ALIGN_CENTER;
                celTextReteIva.Border = PdfPCell.NO_BORDER;
                tableTotalFletesIva.AddCell(celTextReteIva);

                iTextSharp.text.pdf.PdfPCell celValReteIVA = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number04"]).ToString("N2"), fontTitle));
                celValReteIVA.Border = PdfPCell.NO_BORDER;
                celValReteIVA.HorizontalAlignment = Element.ALIGN_RIGHT;
                celValReteIVA.VerticalAlignment = Element.ALIGN_TOP;
                tableTotalFletesIva.AddCell(celValReteIVA);

                tableTotalFletesIva.AddCell(celEspacioTotalBruto);

                iTextSharp.text.pdf.PdfPCell celTextTotalCredito = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    "T. NOTA CREDITO", fontTitleBold));
                celTextTotalCredito.HorizontalAlignment = Element.ALIGN_CENTER;
                celTextTotalCredito.Border = PdfPCell.NO_BORDER;
                tableTotalFletesIva.AddCell(celTextTotalCredito);

                iTextSharp.text.pdf.PdfPCell celValTotalCredito = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"]).ToString("C2"), fontTitle));
                celValTotalCredito.Border = PdfPCell.NO_BORDER;
                celValTotalCredito.HorizontalAlignment = Element.ALIGN_RIGHT;
                celValTotalCredito.VerticalAlignment = Element.ALIGN_TOP;
                tableTotalFletesIva.AddCell(celValTotalCredito);

                tableTotalFletesIva.AddCell(celEspacioTotalBruto);

                iTextSharp.text.pdf.PdfPCell celFletesTotal = new iTextSharp.text.pdf.PdfPCell(tableTotalFletesIva);
                tableObservaciones.AddCell(celFletesTotal);
                #endregion

                document.Add(tableEncabezado);
                document.Add(divEspacio);
                document.Add(divEspacio2);
                document.Add(tableDatosFacturaCustomer);
                document.Add(divEspacio2);
                document.Add(tableTituloUnidades);
                document.Add(tableUnidades);
                document.Add(tableEspacioUnidades);
                document.Add(tableValLetras);
                document.Add(divEspacio2);
                document.Add(divEspacio2);
                document.Add(tableObservaciones);

                /*PIE DE PAGINA*/
                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();
                RutaPdf = NomArchivo;
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return false;
            }
        }

        public bool FacturaExportacionABRACOL(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {
            //NomArchivo = string.Empty;
            //string Ruta = @"C:\Users\key94\source\repos\Reporte_iTextSharp\Reporte_iTextSharp\bin\Debug\PDF\900665411\";
            //string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            //string RutaImg = @"C:\Users\key94\source\repos\Reporte_iTextSharp\Reporte_iTextSharp\bin\Debug\Logo_800027374.png";
            //NomArchivo = NombreInvoice + ".pdf";

            NomArchivo = string.Empty;
            string Ruta = HttpContext.Current.Server.MapPath("~/PDF/" + NIT + "/");
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            string RutaImg = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";
            string RutaimgBas = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logobasc_" + NIT + ".png";


            NomArchivo = NombreInvoice + ".pdf";

            try
            {
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.
                Document document = new Document(iTextSharp.text.PageSize.LETTER);
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;

                // step 3: we open the document     
                document.Open();

                PdfDiv divEspacio = new PdfDiv();
                divEspacio.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio.Height = 10;
                divEspacio.Width = 130;

                PdfDiv divEspacio2 = new PdfDiv();
                divEspacio2.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio2.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio2.Height = 2;
                divEspacio2.Width = 130;

                //FUENTES----------------------------------------------------------------------------------------------------------
                iTextSharp.text.Font fontTitleBold = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustom = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustomEncabezado = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 14, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustomBold = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                //FUENTES---------------------------------------------------------------------------------------------------------

                #region ENCABEZADO

                PdfPTable tableEncabezado = new PdfPTable(3);
                //Dimenciones.
                float[] DimTableEncabezado = new float[3];
                DimTableEncabezado[0] = 1.2F;//
                DimTableEncabezado[1] = 1.2F;//
                DimTableEncabezado[2] = 2.0F;//
                tableEncabezado.WidthPercentage = 100;
                tableEncabezado.SetWidths(DimTableEncabezado);

                PdfPTable tableLogo = new PdfPTable(1);
                System.Drawing.Image logo = null;
                var requestLogo = WebRequest.Create(RutaImg);

                using (var responseLogo = requestLogo.GetResponse())
                using (var streamLogo = responseLogo.GetResponseStream())
                {
                    logo = Bitmap.FromStream(streamLogo);
                }

                PdfDiv divLogo = new PdfDiv();
                divLogo.Float = iTextSharp.text.pdf.PdfDiv.FloatType.LEFT;
                divLogo.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divLogo.BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID;
                divLogo.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                divLogo.Height = 55;
                divLogo.Width = 140;
                //divLogo.BackgroundColor = BaseColor.LIGHT_GRAY;
                iTextSharp.text.Image ImgLogo = iTextSharp.text.Image.GetInstance(logo, BaseColor.WHITE);
                divLogo.BackgroundImage = ImgLogo;

                iTextSharp.text.pdf.PdfPCell celImgLogo = new iTextSharp.text.pdf.PdfPCell();
                celImgLogo.AddElement(divLogo);
                celImgLogo.Colspan = 1;
                celImgLogo.Border = 0;
                celImgLogo.VerticalAlignment = Element.ALIGN_TOP;
                tableLogo.AddCell(celImgLogo);

                iTextSharp.text.pdf.PdfPCell celLogo = new iTextSharp.text.pdf.PdfPCell(tableLogo);
                celLogo.Border = PdfPCell.NO_BORDER;
                tableEncabezado.AddCell(celLogo);
                //----------------------------------------------------------------------------------------------------
                PdfPTable tableInfoEmpresa = new PdfPTable(1);

                iTextSharp.text.pdf.PdfPCell celNombre = new iTextSharp.text.pdf.PdfPCell(
                    new Phrase("FABRICA OFICINA PRINCIPAL", fontTitleBold));
                //celNombre.Padding = 3;
                celNombre.Border = 0;
                celNombre.HorizontalAlignment = Element.ALIGN_LEFT;
                celNombre.VerticalAlignment = Element.ALIGN_TOP;
                tableInfoEmpresa.AddCell(celNombre);

                iTextSharp.text.pdf.PdfPCell celTel = new iTextSharp.text.pdf.PdfPCell(
                    new Phrase($"Tel: {(string)DsInvoiceAR.Tables["Company"].Rows[0]["PhoneNum"]}", fontTitleBold));
                //celNombre.Padding = 3;
                celTel.Border = 0;
                celTel.HorizontalAlignment = Element.ALIGN_LEFT;
                celTel.VerticalAlignment = Element.ALIGN_TOP;
                tableInfoEmpresa.AddCell(celTel);

                iTextSharp.text.pdf.PdfPCell celFax = new iTextSharp.text.pdf.PdfPCell(
                    new Phrase($"FAX: {(string)DsInvoiceAR.Tables["Company"].Rows[0]["FaxNum"]}", fontTitle));
                //celNombre.Padding = 3;
                celFax.Border = 0;
                celFax.HorizontalAlignment = Element.ALIGN_LEFT;
                celFax.VerticalAlignment = Element.ALIGN_TOP;
                tableInfoEmpresa.AddCell(celFax);

                iTextSharp.text.pdf.PdfPCell celWebSite = new iTextSharp.text.pdf.PdfPCell(
                    new Phrase("WEB SITE: abracol.com.co", fontTitle));
                //celNombre.Padding = 3;
                celWebSite.Border = 0;
                celWebSite.HorizontalAlignment = Element.ALIGN_LEFT;
                celWebSite.VerticalAlignment = Element.ALIGN_TOP;
                tableInfoEmpresa.AddCell(celWebSite);

                iTextSharp.text.pdf.PdfPCell celEmail = new iTextSharp.text.pdf.PdfPCell(
                    new Phrase("E-MAIL: abracol@abracol.com.co", fontTitleBold));
                //celNombre.Padding = 3;
                celEmail.Border = 0;
                celEmail.HorizontalAlignment = Element.ALIGN_LEFT;
                celEmail.VerticalAlignment = Element.ALIGN_TOP;
                tableInfoEmpresa.AddCell(celEmail);

                iTextSharp.text.pdf.PdfPCell celDireccion = new iTextSharp.text.pdf.PdfPCell(
                    new Phrase((string)DsInvoiceAR.Tables["Company"].Rows[0]["Address1"], fontTitleBold));
                //celNombre.Padding = 3;
                celDireccion.Border = 0;
                celDireccion.HorizontalAlignment = Element.ALIGN_LEFT;
                celDireccion.VerticalAlignment = Element.ALIGN_TOP;
                tableInfoEmpresa.AddCell(celDireccion);

                iTextSharp.text.pdf.PdfPCell celDireccion2 = new iTextSharp.text.pdf.PdfPCell(
                    new Phrase($"{(string)DsInvoiceAR.Tables["Company"].Rows[0]["City"]} - " +
                    $"{(string)DsInvoiceAR.Tables["Company"].Rows[0]["State"]} - COLOMBIA", fontTitleBold));
                //celNombre.Padding = 3;
                celDireccion2.Border = 0;
                celDireccion2.HorizontalAlignment = Element.ALIGN_LEFT;
                celDireccion2.VerticalAlignment = Element.ALIGN_TOP;
                tableInfoEmpresa.AddCell(celDireccion2);

                iTextSharp.text.pdf.PdfPCell celNit = new iTextSharp.text.pdf.PdfPCell(
                    new Phrase($"NIT:  {(string)DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"]}", fontTitleBold));
                //celNombre.Padding = 3;
                celNit.Border = 0;
                celNit.HorizontalAlignment = Element.ALIGN_LEFT;
                celNit.VerticalAlignment = Element.ALIGN_TOP;
                tableInfoEmpresa.AddCell(celNit);

                iTextSharp.text.pdf.PdfPCell celInfoEmpresa = new iTextSharp.text.pdf.PdfPCell(tableInfoEmpresa);
                celInfoEmpresa.Border = 0;
                tableEncabezado.AddCell(celInfoEmpresa);
                //LOGO BASC-------------------------------------------------------
                PdfPTable tableLogoBascNoFactura = new PdfPTable(2);
                float[] dimtableLogoBascNoFactura = new float[2];
                dimtableLogoBascNoFactura[0] = 1;
                dimtableLogoBascNoFactura[1] = 2;
                tableLogoBascNoFactura.SetWidths(dimtableLogoBascNoFactura);
                System.Drawing.Image logoBasc = null;
                var requestLogoBasc = WebRequest.Create(RutaimgBas);

                using (var responseLogoBasc = requestLogoBasc.GetResponse())
                using (var streamLogoBasc = responseLogoBasc.GetResponseStream())
                {
                    logoBasc = Bitmap.FromStream(streamLogoBasc);
                }

                PdfDiv divLogoBasc = new PdfDiv();
                divLogoBasc.Float = iTextSharp.text.pdf.PdfDiv.FloatType.LEFT;
                divLogoBasc.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divLogoBasc.BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID;
                divLogoBasc.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                divLogoBasc.Height = 55;
                divLogoBasc.Width = 75;
                //divLogoBasc.BackgroundColor = BaseColor.LIGHT_GRAY;
                iTextSharp.text.Image ImgLogoBasc = iTextSharp.text.Image.GetInstance(logoBasc, BaseColor.WHITE);
                divLogoBasc.BackgroundImage = ImgLogoBasc;

                iTextSharp.text.pdf.PdfPCell celImgLogoBasc = new iTextSharp.text.pdf.PdfPCell();
                celImgLogoBasc.AddElement(divLogoBasc);
                celImgLogoBasc.Colspan = 1;
                celImgLogoBasc.Border = 0;
                celImgLogoBasc.VerticalAlignment = Element.ALIGN_TOP;
                tableLogoBascNoFactura.AddCell(celImgLogoBasc);
                //----------------------------------------------------------------
                iTextSharp.text.pdf.PdfPCell celNoFactura = new iTextSharp.text.pdf.PdfPCell(
                    new Phrase($"FACTURA / INVOICE N° EX:    {(string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"]}",
                    fontTitleBold));
                celNoFactura.Colspan = 1;
                celNoFactura.Border = 0;
                celNoFactura.HorizontalAlignment = Element.ALIGN_LEFT;
                celNoFactura.VerticalAlignment = Element.ALIGN_TOP;
                tableLogoBascNoFactura.AddCell(celNoFactura);
                //----------------------------------------------------------------
                PdfPTable tablesAcuerdoDePago = new PdfPTable(2);

                PdfPCell celTextAcuerdoDePago = new PdfPCell(new Phrase("ACUERDO DE PAGO", fontTitle));
                celTextAcuerdoDePago.BorderWidthRight = PdfPCell.NO_BORDER;
                celTextAcuerdoDePago.BorderWidthBottom = PdfPCell.NO_BORDER;
                tablesAcuerdoDePago.AddCell(celTextAcuerdoDePago);

                PdfPCell celTextAcuerdoDePagoEng = new PdfPCell(new Phrase("LATE PAYMENT TERM", fontTitle));
                celTextAcuerdoDePagoEng.BorderWidthLeft = PdfPCell.NO_BORDER;
                celTextAcuerdoDePagoEng.BorderWidthBottom = PdfPCell.NO_BORDER;
                tablesAcuerdoDePago.AddCell(celTextAcuerdoDePagoEng);

                PdfPCell celTextAcuerdoDePago2 = new PdfPCell(new Phrase("EL NO PAGO AL PLAZO ACORDADO GENERA UNA TASA DE INTERES" +
                    ", LA MAXIMA LEGAL VIGENTE", fontTitle));
                celTextAcuerdoDePago2.BorderWidthRight = PdfPCell.NO_BORDER;
                celTextAcuerdoDePago2.BorderWidthTop = PdfPCell.NO_BORDER;
                tablesAcuerdoDePago.AddCell(celTextAcuerdoDePago2);

                PdfPCell celTextAcuerdoDePagoEng2 = new PdfPCell(new Phrase("FINANC CHARGE WILL APPLYT IT FAILURE TO " +
                    "PAY THE AGREED DATE", fontTitle));
                celTextAcuerdoDePagoEng2.BorderWidthLeft = PdfPCell.NO_BORDER;
                celTextAcuerdoDePagoEng2.BorderWidthTop = PdfPCell.NO_BORDER;
                tablesAcuerdoDePago.AddCell(celTextAcuerdoDePagoEng2);

                PdfPCell celAcuerdoDePago = new PdfPCell(tablesAcuerdoDePago);
                celAcuerdoDePago.Colspan = 2;
                tableLogoBascNoFactura.AddCell(celAcuerdoDePago);
                //----------------------------------------------------------------
                PdfPCell celLogoBascNoFactura = new PdfPCell(tableLogoBascNoFactura);
                celLogoBascNoFactura.Border = 0;
                tableEncabezado.AddCell(celLogoBascNoFactura);
                #endregion

                #region DATOS CUSTOMER Y VENDEDOR

                PdfPTable tableDatosFechaMoneda = new PdfPTable(4);
                //Dimenciones.
                float[] DimTableDatosFechaMoneda = new float[4];
                DimTableDatosFechaMoneda[0] = 1.0F;//
                DimTableDatosFechaMoneda[1] = 1.0F;//
                DimTableDatosFechaMoneda[2] = 3.0F;//
                DimTableDatosFechaMoneda[3] = 1.0F;//
                tableDatosFechaMoneda.SetWidths(DimTableDatosFechaMoneda);
                tableDatosFechaMoneda.WidthPercentage = 100;

                iTextSharp.text.pdf.PdfPCell celTextFecha = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    "FECHA / DATE", fontTitleBold));
                celTextFecha.Colspan = 1;
                celTextFecha.Padding = 2;
                celTextFecha.Border = 0;
                celTextFecha.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextFecha.VerticalAlignment = Element.ALIGN_TOP;
                tableDatosFechaMoneda.AddCell(celTextFecha);

                iTextSharp.text.pdf.PdfPCell celValFecha = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).ToString("MMMM/dd/yyyy")
                    , fontTitleBold));
                celValFecha.Colspan = 1;
                celValFecha.Padding = 2;
                celValFecha.Border = 0;
                celValFecha.HorizontalAlignment = Element.ALIGN_LEFT;
                celValFecha.VerticalAlignment = Element.ALIGN_TOP;
                tableDatosFechaMoneda.AddCell(celValFecha);

                iTextSharp.text.pdf.PdfPCell celMoneda = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    $"MONEDA / CURRENCY:   $  {(string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["CurrencyCode"]}" +
                    $" {(string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["CurrencyCodeCurrDesc"]}", fontTitleBold));
                celMoneda.Colspan = 1;
                celMoneda.Padding = 2;
                celMoneda.Border = 0;
                celMoneda.HorizontalAlignment = Element.ALIGN_LEFT;
                celMoneda.VerticalAlignment = Element.ALIGN_TOP;
                tableDatosFechaMoneda.AddCell(celMoneda);

                iTextSharp.text.pdf.PdfPCell celPagina = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    " ", fontTitleBold));
                celPagina.Colspan = 1;
                celPagina.Padding = 2;
                celPagina.Border = 0;
                celPagina.HorizontalAlignment = Element.ALIGN_LEFT;
                celPagina.VerticalAlignment = Element.ALIGN_TOP;
                tableDatosFechaMoneda.AddCell(celPagina);

                //------------------------------------------------------------------------------------------------
                PdfPTable tableDatosFacturaCustomer = new PdfPTable(5);
                //Dimenciones.
                float[] DimencionDatosFacturar = new float[5];
                DimencionDatosFacturar[0] = 1.5F;//
                DimencionDatosFacturar[1] = 0.02F;//
                DimencionDatosFacturar[2] = 1.5F;//
                DimencionDatosFacturar[3] = 0.02F;//
                DimencionDatosFacturar[4] = 1.0F;//

                tableDatosFacturaCustomer.WidthPercentage = 100;
                tableDatosFacturaCustomer.SetWidths(DimencionDatosFacturar);
                //----------------------------------------------------------------------------------------------
                PdfPTable tableDatosCustomer = new PdfPTable(1);
                tableDatosCustomer.WidthPercentage = 100;

                iTextSharp.text.pdf.PdfPCell celTextSRS = new iTextSharp.text.pdf.PdfPCell();
                celTextSRS.AddElement(new Paragraph("SRS:", fontTitleBold));
                celTextSRS.AddElement(new Paragraph((string)DsInvoiceAR.Tables["Customer"].Rows[0]["Name"], fontTitle));
                celTextSRS.Colspan = 2;
                celTextSRS.Padding = 2;
                celTextSRS.Border = 0;
                celTextSRS.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextSRS.VerticalAlignment = Element.ALIGN_TOP;
                tableDatosCustomer.AddCell(celTextSRS);
                //-------------------------------------------------------------------------------
                PdfPTable tableNitCustomer = new PdfPTable(3);
                float[] DimTableNitCustomer = new float[3];
                DimTableNitCustomer[0] = 0.35F;//
                DimTableNitCustomer[1] = 1.0F;//
                DimTableNitCustomer[2] = 1.8F;//
                tableNitCustomer.WidthPercentage = 100;
                tableNitCustomer.SetWidths(DimTableNitCustomer);

                iTextSharp.text.pdf.PdfPCell celTextIdInternacional = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    "                                           ID Internacional:", fontTitleBold));
                celTextIdInternacional.Colspan = 3;
                celTextIdInternacional.Padding = 3;
                celTextIdInternacional.Border = 0;
                celTextIdInternacional.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextIdInternacional.VerticalAlignment = Element.ALIGN_TOP;
                tableNitCustomer.AddCell(celTextIdInternacional);

                iTextSharp.text.pdf.PdfPCell celTexNitCustomer = new iTextSharp.text.pdf.PdfPCell(new Phrase("NIT:", fontTitleBold));
                celTexNitCustomer.Colspan = 1;
                celTexNitCustomer.Padding = 3;
                celTexNitCustomer.Border = 0;
                celTexNitCustomer.HorizontalAlignment = Element.ALIGN_LEFT;
                celTexNitCustomer.VerticalAlignment = Element.ALIGN_TOP;
                tableNitCustomer.AddCell(celTexNitCustomer);

                iTextSharp.text.pdf.PdfPCell celValNitCustomer = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    (string)DsInvoiceAR.Tables["Customer"].Rows[0]["ResaleID"],
                    fontTitle));
                celValNitCustomer.Colspan = 1;
                celValNitCustomer.Padding = 3;
                celValNitCustomer.Border = 0;
                celValNitCustomer.HorizontalAlignment = Element.ALIGN_LEFT;
                celValNitCustomer.VerticalAlignment = Element.ALIGN_TOP;
                tableNitCustomer.AddCell(celValNitCustomer);

                iTextSharp.text.pdf.PdfPCell celValIdInternacional = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    "xxxxxxxx", fontTitle));
                celValIdInternacional.Colspan = 1;
                celValIdInternacional.Padding = 3;
                celValIdInternacional.Border = 0;
                celValIdInternacional.HorizontalAlignment = Element.ALIGN_LEFT;
                celValIdInternacional.VerticalAlignment = Element.ALIGN_TOP;
                tableNitCustomer.AddCell(celValIdInternacional);

                iTextSharp.text.pdf.PdfPCell celNitCustomer = new iTextSharp.text.pdf.PdfPCell(tableNitCustomer);
                celNitCustomer.Border = PdfPCell.NO_BORDER;
                //celNitCustomer.BorderWidthTop = PdfPCell.NO_BORDER;
                tableDatosCustomer.AddCell(celNitCustomer);
                //-------------------------------------------------------------------------------
                iTextSharp.text.pdf.PdfPCell celTextDomicEmbarque = new iTextSharp.text.pdf.PdfPCell(new Phrase("Dom. embq./Delivery Address",
                    fontTitleBold));
                celTextDomicEmbarque.Colspan = 1;
                celTextDomicEmbarque.Padding = 3;
                celTextDomicEmbarque.Border = 0;
                celTextDomicEmbarque.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextDomicEmbarque.VerticalAlignment = Element.ALIGN_TOP;
                tableDatosCustomer.AddCell(celTextDomicEmbarque);

                iTextSharp.text.pdf.PdfPCell celValDomicEmbarque = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    (string)DsInvoiceAR.Tables["Customer"].Rows[0]["Address1"], fontTitle));
                celValDomicEmbarque.Colspan = 1;
                celValDomicEmbarque.Padding = 3;
                celValDomicEmbarque.PaddingBottom = 7;
                celValDomicEmbarque.Border = 0;
                celValDomicEmbarque.HorizontalAlignment = Element.ALIGN_LEFT;
                celValDomicEmbarque.VerticalAlignment = Element.ALIGN_TOP;
                tableDatosCustomer.AddCell(celValDomicEmbarque);
                //----------------------------------------------------------------------
                iTextSharp.text.pdf.PdfPCell celTextInfoCustomer = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    "xxxxx xxxxxxxxx x\nxxxxxx\nxxxx", fontTitle));
                celTextInfoCustomer.Colspan = 1;
                celTextInfoCustomer.Padding = 3;
                celTextInfoCustomer.Border = 0;
                celTextInfoCustomer.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextInfoCustomer.VerticalAlignment = Element.ALIGN_TOP;
                tableDatosCustomer.AddCell(celTextInfoCustomer);
                //-------------------------------------------------------------------------------
                iTextSharp.text.pdf.PdfPCell celDatosCustomer = new iTextSharp.text.pdf.PdfPCell(tableDatosCustomer);
                //celDatosCustomer.Border = 0;
                tableDatosFacturaCustomer.AddCell(celDatosCustomer);
                //-------------------------------------------------------------------------------
                iTextSharp.text.pdf.PdfPCell celEspacio2 = new iTextSharp.text.pdf.PdfPCell(new Phrase(" ", fontCustom));
                celEspacio2.Border = 0;
                tableDatosFacturaCustomer.AddCell(celEspacio2);
                //-------------------------------------------------------------------------------
                PdfPTable tableDatosKG = new PdfPTable(2);
                float[] DimtableDatosKG = new float[2];
                DimtableDatosKG[0] = 2.0F;//
                DimtableDatosKG[1] = 1.0F;//

                tableDatosKG.WidthPercentage = 100;
                tableDatosKG.SetWidths(DimtableDatosKG);

                iTextSharp.text.pdf.PdfPCell celTextIncoterm = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    "INCOTERM:  FOB-Franco abordo",
                    fontTitle));
                celTextIncoterm.Colspan = 2;
                celTextIncoterm.Padding = 5;
                celTextIncoterm.BorderWidthBottom = PdfPCell.NO_BORDER;
                celTextIncoterm.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextIncoterm.VerticalAlignment = Element.ALIGN_CENTER;
                tableDatosKG.AddCell(celTextIncoterm);
                //------------------------------------------------------------------------------------------------
                Paragraph prgQuantity = new Paragraph("QUANTITY AND TYPE OF PACKAGE", fontTitleBold);
                prgQuantity.Alignment = Element.ALIGN_LEFT;

                Paragraph prgValQuantity = new Paragraph("xxxxxx", fontTitle);
                prgValQuantity.Alignment = Element.ALIGN_LEFT;
                iTextSharp.text.pdf.PdfPCell celQuantity = new iTextSharp.text.pdf.PdfPCell();
                celQuantity.AddElement(prgQuantity);
                celQuantity.AddElement(prgValQuantity);
                celQuantity.Colspan = 2;
                celQuantity.Padding = 3;
                celQuantity.BorderWidthBottom = PdfPCell.NO_BORDER;
                celQuantity.BorderWidthTop = PdfPCell.NO_BORDER;
                //celQuantity.BorderWidthLeft = 0.5f;
                celQuantity.HorizontalAlignment = Element.ALIGN_CENTER;
                celQuantity.VerticalAlignment = Element.ALIGN_TOP;
                tableDatosKG.AddCell(celQuantity);

                Paragraph prgPesoBruto = new Paragraph("PESO BRUTO/GROSS WEIGHT:", fontTitleBold);
                prgPesoBruto.Alignment = Element.ALIGN_LEFT;

                Paragraph prgValPesoBruto = new Paragraph("xxxxx", fontTitle);
                prgValPesoBruto.Alignment = Element.ALIGN_LEFT;

                iTextSharp.text.pdf.PdfPCell celTextPesoBruto = new iTextSharp.text.pdf.PdfPCell();
                celTextPesoBruto.AddElement(prgPesoBruto);
                //celTextCodVendedor.AddElement(prgValPesoBruto);
                celTextPesoBruto.Colspan = 1;
                celTextPesoBruto.Padding = 3;
                celTextPesoBruto.BorderWidthBottom = PdfPCell.NO_BORDER;
                celTextPesoBruto.BorderWidthRight = PdfPCell.NO_BORDER;
                celTextPesoBruto.BorderWidthTop = PdfPCell.NO_BORDER;
                celTextPesoBruto.HorizontalAlignment = Element.ALIGN_CENTER;
                celTextPesoBruto.VerticalAlignment = Element.ALIGN_TOP;
                tableDatosKG.AddCell(celTextPesoBruto);

                iTextSharp.text.pdf.PdfPCell celValPesoBruto = new iTextSharp.text.pdf.PdfPCell();
                celValPesoBruto.AddElement(prgValPesoBruto);
                celValPesoBruto.Colspan = 1;
                celValPesoBruto.Padding = 3;
                celValPesoBruto.BorderWidthBottom = PdfPCell.NO_BORDER;
                celValPesoBruto.BorderWidthLeft = PdfPCell.NO_BORDER;
                celValPesoBruto.BorderWidthTop = PdfPCell.NO_BORDER;
                //celTextPesoBruto.BorderWidthTop = 0.5f;
                celValPesoBruto.HorizontalAlignment = Element.ALIGN_CENTER;
                celValPesoBruto.VerticalAlignment = Element.ALIGN_TOP;
                tableDatosKG.AddCell(celValPesoBruto);

                Paragraph prgPesoNeto = new Paragraph("PESO NETO / NET WEIGHT:", fontTitleBold);
                prgPesoNeto.Alignment = Element.ALIGN_LEFT;

                Paragraph prgValPesoNeto = new Paragraph("xxxxx", fontTitle);
                prgValPesoNeto.Alignment = Element.ALIGN_LEFT;

                iTextSharp.text.pdf.PdfPCell celTextPesoNeto = new iTextSharp.text.pdf.PdfPCell();
                celTextPesoNeto.AddElement(prgPesoNeto);
                //celTextCodVendedor.AddElement(prgValPesoBruto);
                celTextPesoNeto.Colspan = 1;
                celTextPesoNeto.Padding = 3;
                celTextPesoNeto.BorderWidthBottom = PdfPCell.NO_BORDER;
                celTextPesoNeto.BorderWidthRight = PdfPCell.NO_BORDER;
                celTextPesoNeto.BorderWidthTop = PdfPCell.NO_BORDER;
                //celTextPesoBruto.BorderWidthTop = 0.5f;
                celTextPesoNeto.HorizontalAlignment = Element.ALIGN_CENTER;
                celTextPesoNeto.VerticalAlignment = Element.ALIGN_TOP;
                tableDatosKG.AddCell(celTextPesoNeto);

                iTextSharp.text.pdf.PdfPCell celValPesoNeto = new iTextSharp.text.pdf.PdfPCell();
                celValPesoNeto.AddElement(prgValPesoNeto);
                celValPesoNeto.Colspan = 1;
                celValPesoNeto.Padding = 3;
                celValPesoNeto.BorderWidthBottom = PdfPCell.NO_BORDER;
                celValPesoNeto.BorderWidthTop = PdfPCell.NO_BORDER;
                celValPesoNeto.BorderWidthLeft = PdfPCell.NO_BORDER;
                celValPesoNeto.HorizontalAlignment = Element.ALIGN_CENTER;
                celValPesoNeto.VerticalAlignment = Element.ALIGN_TOP;
                tableDatosKG.AddCell(celValPesoNeto);

                iTextSharp.text.pdf.PdfPCell celTexKG = new iTextSharp.text.pdf.PdfPCell(
                    new Phrase("KGS", FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 12, iTextSharp.text.Font.NORMAL)));
                celTexKG.Colspan = 3;
                celTexKG.Padding = 7;
                celTexKG.PaddingRight = 10;
                celTexKG.BorderWidthBottom = PdfPCell.NO_BORDER;
                celTexKG.BorderWidthTop = PdfPCell.NO_BORDER;
                celTexKG.BorderWidthLeft = PdfPCell.NO_BORDER;
                celTexKG.HorizontalAlignment = Element.ALIGN_RIGHT;
                celTexKG.VerticalAlignment = Element.ALIGN_TOP;
                tableDatosKG.AddCell(celTexKG);

                iTextSharp.text.pdf.PdfPCell celDatosKG = new iTextSharp.text.pdf.PdfPCell(tableDatosKG);
                celDatosKG.HorizontalAlignment = Element.ALIGN_CENTER;
                tableDatosFacturaCustomer.AddCell(celDatosKG);
                //--------------------------------------------------------------------------------------
                tableDatosFacturaCustomer.AddCell(celEspacio2);

                PdfPTable tableFormaPago = new PdfPTable(4);
                float[] dimTableFormaPago = new float[4];
                dimTableFormaPago[0] = 0.2f;
                dimTableFormaPago[1] = 3.5f;
                dimTableFormaPago[2] = 0.4f;
                dimTableFormaPago[3] = 0.2f;
                tableFormaPago.SetWidths(dimTableFormaPago);

                iTextSharp.text.pdf.PdfPCell celCuadro = new iTextSharp.text.pdf.PdfPCell(new Phrase(" ", fontTitle));
                celCuadro.Bottom = 5;
                iTextSharp.text.pdf.PdfPCell celEspacioFPago = new iTextSharp.text.pdf.PdfPCell(new Phrase(" ", fontTitle));
                //tableFormaPago.AddCell(celEspacioFPago);
                celEspacioFPago.Border = 0;


                iTextSharp.text.pdf.PdfPCell celTextCondPago = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    "COND PAGO / PAYMENT TERMS", fontTitle));
                celTextCondPago.Colspan = 4;
                celTextCondPago.Padding = 0;
                celTextCondPago.PaddingBottom = 5;
                celTextCondPago.Border = 0;
                tableFormaPago.AddCell(celTextCondPago);

                tableFormaPago.AddCell(celEspacioFPago);
                iTextSharp.text.pdf.PdfPCell celTextAnticipado = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    "ANTICIPADO", fontTitle));
                celTextAnticipado.Border = 0;
                celTextAnticipado.Colspan = 1;
                tableFormaPago.AddCell(celTextAnticipado);

                tableFormaPago.AddCell(celCuadro);
                tableFormaPago.AddCell(celEspacioFPago);

                tableFormaPago.AddCell(celEspacioFPago);
                iTextSharp.text.pdf.PdfPCell celTextCartaCredito = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    "CARTA DE CREDITO", fontTitle));
                celTextCartaCredito.Border = 0;
                celTextCartaCredito.Colspan = 1;
                tableFormaPago.AddCell(celTextCartaCredito);

                tableFormaPago.AddCell(celCuadro);
                tableFormaPago.AddCell(celEspacioFPago);

                tableFormaPago.AddCell(celEspacioFPago);
                iTextSharp.text.pdf.PdfPCell celTextPagare = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    "PAGARE", fontTitle));
                celTextPagare.Border = 0;
                celTextPagare.Colspan = 1;
                tableFormaPago.AddCell(celTextPagare);

                tableFormaPago.AddCell(celCuadro);
                tableFormaPago.AddCell(celEspacioFPago);

                tableFormaPago.AddCell(celEspacioFPago);
                iTextSharp.text.pdf.PdfPCell celTextCrediSeguro = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    "CREDISEGUROS", fontTitle));
                celTextCrediSeguro.Border = 0;
                celTextCrediSeguro.Colspan = 1;
                tableFormaPago.AddCell(celTextCrediSeguro);

                tableFormaPago.AddCell(celCuadro);
                tableFormaPago.AddCell(celEspacioFPago);

                tableFormaPago.AddCell(celEspacioFPago);
                iTextSharp.text.pdf.PdfPCell celTextOtro = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    "OTRO", fontTitle));
                celTextOtro.Border = 0;
                celTextOtro.Colspan = 1;
                tableFormaPago.AddCell(celTextOtro);

                tableFormaPago.AddCell(celCuadro);
                tableFormaPago.AddCell(celEspacioFPago);

                iTextSharp.text.pdf.PdfPCell celLastLine = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                   " ", fontTitle));
                celLastLine.Border = 0;
                celLastLine.Colspan = 4;
                tableFormaPago.AddCell(celLastLine);

                iTextSharp.text.pdf.PdfPCell celFormaPago = new iTextSharp.text.pdf.PdfPCell(tableFormaPago);
                tableDatosFacturaCustomer.AddCell(celFormaPago);
                //-------------------------------------------------------------------------------------- 
                #endregion

                #region UNIDADES
                PdfPTable tableTituloUnidades = new PdfPTable(7);
                //Dimenciones.
                float[] DimencionUnidades = new float[7];
                DimencionUnidades[0] = 0.178F;//L
                DimencionUnidades[1] = 0.5F;//Cantidad
                DimencionUnidades[2] = 0.328F;//Unidad
                DimencionUnidades[3] = 2.0F;//Descripcion
                DimencionUnidades[4] = 0.585F;//Valor Unitario
                DimencionUnidades[5] = 0.8F;//CodCliente
                DimencionUnidades[6] = 0.5F;//Valor Total

                tableTituloUnidades.WidthPercentage = 100;
                tableTituloUnidades.SetWidths(DimencionUnidades);

                iTextSharp.text.pdf.PdfPCell celTextL = new iTextSharp.text.pdf.PdfPCell(new Phrase("L", fontTitleBold));
                celTextL.Colspan = 1;
                celTextL.Padding = 3;
                celTextL.Border = 0;
                celTextL.BorderWidthBottom = 1;
                celTextL.BackgroundColor = BaseColor.LIGHT_GRAY;
                celTextL.HorizontalAlignment = Element.ALIGN_CENTER;
                celTextL.VerticalAlignment = Element.ALIGN_BOTTOM;

                tableTituloUnidades.AddCell(celTextL);

                iTextSharp.text.pdf.PdfPCell celTextCantidad = new iTextSharp.text.pdf.PdfPCell(new Phrase("CANT\nQTY", fontTitleBold));
                celTextCantidad.Colspan = 1;
                celTextCantidad.Padding = 3;
                celTextCantidad.Border = 0;
                celTextCantidad.BorderWidthBottom = 1;
                celTextCantidad.BackgroundColor = BaseColor.LIGHT_GRAY;
                celTextCantidad.HorizontalAlignment = Element.ALIGN_CENTER;
                celTextCantidad.VerticalAlignment = Element.ALIGN_TOP;

                tableTituloUnidades.AddCell(celTextCantidad);

                iTextSharp.text.pdf.PdfPCell celTextUnidad = new iTextSharp.text.pdf.PdfPCell(new Phrase("UND\nUNIT", fontTitleBold));
                celTextUnidad.Colspan = 1;
                celTextUnidad.Padding = 3;
                celTextUnidad.BackgroundColor = BaseColor.LIGHT_GRAY;
                celTextUnidad.Border = 0;
                celTextUnidad.BorderWidthBottom = 1;
                celTextUnidad.HorizontalAlignment = Element.ALIGN_CENTER;
                celTextUnidad.VerticalAlignment = Element.ALIGN_TOP;

                tableTituloUnidades.AddCell(celTextUnidad);

                iTextSharp.text.pdf.PdfPCell celTextDesc = new iTextSharp.text.pdf.PdfPCell(new Phrase("DESCRIPCION\nDESCRIPTION", fontTitleBold));
                celTextDesc.Colspan = 1;
                celTextDesc.Padding = 3;
                celTextDesc.BackgroundColor = BaseColor.LIGHT_GRAY;
                celTextDesc.Border = 0;
                celTextDesc.BorderWidthBottom = 1;
                celTextDesc.HorizontalAlignment = Element.ALIGN_CENTER;
                celTextDesc.VerticalAlignment = Element.ALIGN_TOP;

                tableTituloUnidades.AddCell(celTextDesc);

                iTextSharp.text.pdf.PdfPCell celTextValorUnitario = new iTextSharp.text.pdf.PdfPCell(new Phrase("V.UNIT\nU.PRICE", fontTitleBold));
                celTextValorUnitario.Colspan = 1;
                celTextValorUnitario.Border = 0;
                celTextValorUnitario.BorderWidthBottom = 1;
                celTextValorUnitario.BackgroundColor = BaseColor.LIGHT_GRAY;
                celTextValorUnitario.Padding = 3;
                celTextValorUnitario.HorizontalAlignment = Element.ALIGN_CENTER;
                celTextValorUnitario.VerticalAlignment = Element.ALIGN_TOP;

                tableTituloUnidades.AddCell(celTextValorUnitario);

                iTextSharp.text.pdf.PdfPCell celTextCodCliente = new iTextSharp.text.pdf.PdfPCell(new Phrase("COD CLIENTE\nCUSTOMER", fontTitleBold));
                celTextCodCliente.Colspan = 1;
                celTextCodCliente.Padding = 3;
                celTextCodCliente.Border = 0;
                celTextCodCliente.BorderWidthBottom = 1;
                celTextCodCliente.BackgroundColor = BaseColor.LIGHT_GRAY;
                celTextCodCliente.HorizontalAlignment = Element.ALIGN_CENTER;
                celTextCodCliente.VerticalAlignment = Element.ALIGN_TOP;

                tableTituloUnidades.AddCell(celTextCodCliente);

                iTextSharp.text.pdf.PdfPCell celTextValorTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase("V. TOTAL", fontTitleBold));
                celTextValorTotal.Colspan = 1;
                celTextValorTotal.Padding = 3;
                celTextValorTotal.Border = 0;
                celTextValorTotal.BorderWidthBottom = 1;
                celTextValorTotal.BackgroundColor = BaseColor.LIGHT_GRAY;
                celTextValorTotal.HorizontalAlignment = Element.ALIGN_CENTER;
                celTextValorTotal.VerticalAlignment = Element.ALIGN_BOTTOM;

                tableTituloUnidades.AddCell(celTextValorTotal);
                //------------------------------------------------------------------------
                PdfPTable tablePosicionArancelaria = new PdfPTable(3);
                tablePosicionArancelaria.WidthPercentage = 100;
                PdfPCell celTextPosicionArancelaria = new PdfPCell(new Phrase("Posición Arancelaria...", fontTitleBold));

                celTextPosicionArancelaria.Padding = 3;
                celTextPosicionArancelaria.Border = 0;
                celTextPosicionArancelaria.BorderWidthLeft = 1;
                celTextPosicionArancelaria.BorderWidthBottom = 1;
                celTextPosicionArancelaria.HorizontalAlignment = Element.ALIGN_RIGHT;

                tablePosicionArancelaria.AddCell(celTextPosicionArancelaria);

                PdfPCell celValPosicionArancelaria = new PdfPCell(new Phrase("xxxxx", fontTitleBold));

                celValPosicionArancelaria.Padding = 3;
                celValPosicionArancelaria.Border = 0;
                celValPosicionArancelaria.BorderWidthBottom = 1;
                celValPosicionArancelaria.HorizontalAlignment = Element.ALIGN_CENTER;

                tablePosicionArancelaria.AddCell(celValPosicionArancelaria);

                PdfPCell celFibroDiscos = new PdfPCell(new Phrase("FIBRO DISCOS", fontTitleBold));

                celFibroDiscos.Padding = 3;
                celFibroDiscos.PaddingLeft = 10;
                celFibroDiscos.Border = 0;
                celFibroDiscos.BorderWidthRight = 1;
                celFibroDiscos.BorderWidthBottom = 1;
                tablePosicionArancelaria.AddCell(celFibroDiscos);
                //------------------------------------------------------------------------
                PdfPTable tableUnidades = new PdfPTable(7);
                tableUnidades.WidthPercentage = 100;
                tableUnidades.SetWidths(DimencionUnidades);

                foreach (DataRow InvoiceLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
                    if (!AddUnidadesExportacionAbracol(InvoiceLine, ref tableUnidades, fontCustom, DsInvoiceAR))
                        return false;

                iTextSharp.text.pdf.PdfPCell celTextSubTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase("Subtotal:", fontTitleBold));
                celTextSubTotal.Colspan = 6;
                celTextSubTotal.Padding = 3;
                celTextSubTotal.Border = 0;
                celTextSubTotal.BorderWidthBottom = 1;
                celTextSubTotal.BorderWidthTop = 1;
                celTextSubTotal.BorderWidthLeft = 1;
                //celTextSubTotal.BackgroundColor = BaseColor.LIGHT_GRAY;
                celTextSubTotal.HorizontalAlignment = Element.ALIGN_RIGHT;
                //celTextSubTotal.VerticalAlignment = Element.ALIGN_BOTTOM;

                tableUnidades.AddCell(celTextSubTotal);

                iTextSharp.text.pdf.PdfPCell celValSubTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubTotal"]).ToString("N2"), fontTitleBold));
                //iTextSharp.text.pdf.PdfPCell celValSubTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                //    decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number02"]).ToString("N2"), fontTitleBold));
                celValSubTotal.Colspan = 1;
                celValSubTotal.Padding = 3;
                celValSubTotal.Border = 0;
                celValSubTotal.BorderWidthBottom = 1;
                celValSubTotal.BorderWidthTop = 1;
                celValSubTotal.BorderWidthRight = 1;
                celValSubTotal.BackgroundColor = BaseColor.LIGHT_GRAY;
                celValSubTotal.HorizontalAlignment = Element.ALIGN_CENTER;
                celValSubTotal.VerticalAlignment = Element.ALIGN_TOP;

                tableUnidades.AddCell(celValSubTotal);

                int numAdLineas = 20 - DsInvoiceAR.Tables["InvcDtl"].Rows.Count;
                //int numAdLineas = 27 - numLin;
                PdfPTable tableEspacioUnidades = new PdfPTable(3);
                float[] dimTableEspacioUnidades = new float[3];
                dimTableEspacioUnidades[0] = 4.0f;
                dimTableEspacioUnidades[1] = 1.0f;
                dimTableEspacioUnidades[2] = 1.0f;
                tableEspacioUnidades.WidthPercentage = 100;
                tableEspacioUnidades.SetWidths(dimTableEspacioUnidades);

                for (int i = 0; i < numAdLineas; i++)
                    AddUnidadesExportacionAbracol(ref tableEspacioUnidades);

                PdfPCell celTextObservaciones = new PdfPCell(new Phrase("Observaciones:", fontTitleBold));
                celTextObservaciones.Colspan = 1;
                celTextObservaciones.Padding = 3;
                celTextObservaciones.Border = 0;
                celTextObservaciones.BorderWidthLeft = 1;
                celTextObservaciones.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextObservaciones.VerticalAlignment = Element.ALIGN_TOP;

                tableEspacioUnidades.AddCell(celTextObservaciones);

                PdfPCell celTextSubTotal2 = new PdfPCell(new Phrase("Subtotal:", fontTitleBold));
                celTextSubTotal2.Colspan = 1;
                celTextSubTotal2.Padding = 3;
                celTextSubTotal2.Border = 0;
                celTextSubTotal2.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextSubTotal2.VerticalAlignment = Element.ALIGN_TOP;

                tableEspacioUnidades.AddCell(celTextSubTotal2);

                PdfPCell celValSubtotal = new PdfPCell(new Phrase(
                    decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubTotal"]).ToString("N2"), fontTitleBold));
                celValSubtotal.Colspan = 1;
                celValSubtotal.Padding = 3;
                celValSubtotal.Border = 0;
                celValSubtotal.BorderWidthRight = 1;
                celValSubtotal.HorizontalAlignment = Element.ALIGN_LEFT;
                celValSubtotal.VerticalAlignment = Element.ALIGN_TOP;

                tableEspacioUnidades.AddCell(celValSubtotal);

                AddUnidadesExportacionAbracol(ref tableEspacioUnidades);

                PdfPCell celLineaFinal = new PdfPCell(new Phrase(" ", fontTitleBold));
                celLineaFinal.Colspan = 3;
                celLineaFinal.Padding = 3;
                celLineaFinal.BorderWidthTop = PdfPCell.NO_BORDER;
                tableEspacioUnidades.AddCell(celLineaFinal);
                //----------------------------------------------------------------------------
                PdfPTable tableTotal = new PdfPTable(3);
                float[] dimTableTotal = new float[3];
                dimTableTotal[0] = 3.5f;
                dimTableTotal[1] = 1.0f;
                dimTableTotal[2] = 1.0f;
                tableTotal.SetWidths(dimTableTotal);
                tableTotal.HorizontalAlignment = Element.ALIGN_RIGHT;

                PdfPCell celTextTotal = new PdfPCell(new Phrase("TOTAL           FOB-Franco a bordo", fontTitleBold));
                celTextTotal.Colspan = 1;
                celTextTotal.Padding = 5;
                celTextTotal.BorderWidthRight = PdfPCell.NO_BORDER;
                celTextTotal.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextTotal.VerticalAlignment = Element.ALIGN_TOP;
                celTextTotal.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableTotal.AddCell(celTextTotal);

                PdfPCell celTextUsDollar = new PdfPCell(new Phrase("$ US Dollar", fontTitle));
                celTextUsDollar.Colspan = 1;
                celTextUsDollar.Padding = 5;
                celTextUsDollar.BorderWidthRight = PdfPCell.NO_BORDER;
                celTextUsDollar.BorderWidthLeft = PdfPCell.NO_BORDER;
                celTextUsDollar.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextUsDollar.VerticalAlignment = Element.ALIGN_TOP;
                celTextUsDollar.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableTotal.AddCell(celTextUsDollar);

                PdfPCell celValTotal = new PdfPCell(new Phrase(
                    decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubTotal"]).ToString("N2"), fontTitleBold));
                celValTotal.Colspan = 1;
                celValTotal.Padding = 5;
                celValTotal.BorderWidthLeft = PdfPCell.NO_BORDER;
                celValTotal.HorizontalAlignment = Element.ALIGN_LEFT;
                celValTotal.VerticalAlignment = Element.ALIGN_TOP;
                celValTotal.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableTotal.AddCell(celValTotal);
                //------------------------------------------------------------------------------
                PdfPTable tableDatosExportador = new PdfPTable(3);
                float[] dimTableDatosExportador = new float[3];
                dimTableDatosExportador[0] = 1.0f;
                dimTableDatosExportador[1] = 3.0f;
                dimTableDatosExportador[2] = 1.5f;
                tableDatosExportador.SetWidths(dimTableDatosExportador);
                tableDatosExportador.WidthPercentage = 100;

                PdfPCell celTextExportador = new PdfPCell(new Phrase("EXPORTADOR", fontTitleBold));
                celTextExportador.Colspan = 1;
                celTextExportador.Border = PdfPCell.NO_BORDER;
                celTextExportador.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextExportador.VerticalAlignment = Element.ALIGN_TOP;
                tableDatosExportador.AddCell(celTextExportador);

                PdfPCell celTextNoCuenta = new PdfPCell(new Phrase("ABRACOL S.A CUENTA N° 102001855-BANCO DE CREDITO", fontTitleBold));
                celTextNoCuenta.Border = PdfPCell.NO_BORDER;
                tableDatosExportador.AddCell(celTextNoCuenta);

                PdfPCell celLineFirma = new PdfPCell(new Phrase("_________________________________", fontTitleBold));
                celLineFirma.Colspan = 1;
                celLineFirma.Border = PdfPCell.NO_BORDER;
                tableDatosExportador.AddCell(celLineFirma);
                #endregion

                document.Add(tableEncabezado);
                document.Add(divEspacio);
                document.Add(tableDatosFechaMoneda);
                document.Add(tableDatosFacturaCustomer);
                document.Add(divEspacio2);
                document.Add(tableTituloUnidades);
                document.Add(tablePosicionArancelaria);
                document.Add(tableUnidades);
                document.Add(tableEspacioUnidades);
                document.Add(divEspacio2);
                document.Add(divEspacio2);
                document.Add(tableTotal);
                document.Add(divEspacio);
                document.Add(divEspacio);
                document.Add(tableDatosExportador);

                /*PIE DE PAGINA*/
                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();
                RutaPdf = NomArchivo;
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return false;
            }
        }

        private bool AddUnidadesAbracol(DataRow dataLine, ref PdfPTable table, iTextSharp.text.Font fontTitleFactura, DataSet dataSet)
        {
            try
            {
                strError += "Add InvoiceLine";
                iTextSharp.text.pdf.PdfPCell celValL = new iTextSharp.text.pdf.PdfPCell(new Phrase((string)dataLine["InvoiceLine"], fontTitleFactura));
                celValL.Colspan = 1;
                celValL.Padding = 2;
                celValL.Border = 0;
                //celTextL.BorderWidthRight = 1;
                celValL.BorderWidthLeft = 1;
                celValL.HorizontalAlignment = Element.ALIGN_CENTER;
                celValL.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValL);
                strError += "InvoiceLine OK";

                strError += "Add PartNum";
                iTextSharp.text.pdf.PdfPCell celValCodigo = new iTextSharp.text.pdf.PdfPCell(new Phrase((string)dataLine["PartNum"], fontTitleFactura));
                celValCodigo.Colspan = 1;
                celValCodigo.Padding = 2;
                celValCodigo.Border = 0;
                celValCodigo.BorderWidthLeft = 1;
                //celDescripcion.BorderColorBottom = BaseColor.WHITE;
                celValCodigo.HorizontalAlignment = Element.ALIGN_LEFT;
                celValCodigo.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValCodigo);
                strError += "Add PartNum OK";

                strError += "Add SellingShipQty";
                iTextSharp.text.pdf.PdfPCell celValCantidad = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    decimal.Parse((string)dataLine["SellingShipQty"]).ToString("N"), fontTitleFactura));
                celValCantidad.Colspan = 1;
                celValCantidad.Padding = 2;
                celValCantidad.Border = 0;
                celValCantidad.BorderWidthLeft = 1;
                celValCantidad.HorizontalAlignment = Element.ALIGN_RIGHT;
                celValCantidad.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValCantidad);
                strError += "Add SellingShipQty OK";

                strError += "Add SalesUM";
                iTextSharp.text.pdf.PdfPCell celValUnidad = new iTextSharp.text.pdf.PdfPCell(new Phrase((string)dataLine["SalesUM"], fontTitleFactura));
                celValUnidad.Colspan = 1;
                celValUnidad.Padding = 2;
                celValUnidad.Border = 0;
                celValUnidad.BorderWidthLeft = 1;
                celValUnidad.HorizontalAlignment = Element.ALIGN_LEFT;
                celValUnidad.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValUnidad);
                strError += "Add SalesUM OK";

                strError += "Add LineDesc";
                iTextSharp.text.pdf.PdfPCell celValDesc = new iTextSharp.text.pdf.PdfPCell(new Phrase((string)dataLine["LineDesc"], fontTitleFactura));
                celValDesc.Colspan = 1;
                celValDesc.Padding = 2;
                celValDesc.Border = 0;
                celValDesc.BorderWidthLeft = 1;
                celValDesc.HorizontalAlignment = Element.ALIGN_LEFT;
                celValDesc.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValDesc);
                strError += "Add LineDesc OK";

                strError += "Add DocUnitPrice";
                iTextSharp.text.pdf.PdfPCell celValValorUnitario = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    decimal.Parse((string)dataLine["DocUnitPrice"]).ToString("N2"), fontTitleFactura));
                celValValorUnitario.Colspan = 1;
                celValValorUnitario.Border = 0;
                celValValorUnitario.BorderWidthLeft = 1;
                celValValorUnitario.Padding = 2;
                celValValorUnitario.HorizontalAlignment = Element.ALIGN_RIGHT;
                celValValorUnitario.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValValorUnitario);
                strError += "Add DocUnitPrice OK";

                strError += "Add DspDocLessDiscount";
                //iTextSharp.text.pdf.PdfPCell celValDescuento = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                //    decimal.Parse((string)dataLine["DspDocLessDiscount"]).ToString("N2"), fontTitleFactura));
                iTextSharp.text.pdf.PdfPCell celValDescuento = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    decimal.Parse((string)dataLine["DiscountPercent"]).ToString("N2"), fontTitleFactura));
                celValDescuento.Colspan = 1;
                celValDescuento.Padding = 2;
                celValDescuento.Border = 0;
                celValDescuento.BorderWidthLeft = 1;
                celValDescuento.HorizontalAlignment = Element.ALIGN_RIGHT;
                celValDescuento.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValDescuento);
                strError += "Add DspDocLessDiscount OK";

                strError += "Add DspDocExtPrice";
                iTextSharp.text.pdf.PdfPCell celValValorTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    decimal.Parse((string)dataLine["DspDocExtPrice"]).ToString("N2"),
                    fontTitleFactura));
                celValValorTotal.Colspan = 1;
                celValValorTotal.Padding = 2;
                celValValorTotal.Border = 0;
                celValValorTotal.BorderWidthLeft = 1;
                celValValorTotal.HorizontalAlignment = Element.ALIGN_RIGHT;
                celValValorTotal.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValValorTotal);
                strError += "Add DspDocExtPrice OK";

                strError += "Add InvoiceNum";
                iTextSharp.text.pdf.PdfPCell celValIva = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    GetPercentIdImpDIAN((string)dataLine["InvoiceNum"], (string)dataLine["InvoiceLine"], dataSet.Tables["InvcTax"]),
                    fontTitleFactura));
                celValIva.Colspan = 1;
                celValIva.Padding = 2;
                celValIva.Border = 0;
                celValIva.BorderWidthLeft = 1;
                celValIva.BorderWidthRight = 1;
                celValIva.HorizontalAlignment = Element.ALIGN_RIGHT;
                celValIva.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValIva);
                strError += "Add InvoiceNum OK";
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return false;
            }
        }

        private bool AddUnidadesExportacionAbracol(DataRow dataLine, ref PdfPTable table, iTextSharp.text.Font fontTitleFactura, DataSet dataSet)
        {
            try
            {
                iTextSharp.text.pdf.PdfPCell celValL = new iTextSharp.text.pdf.PdfPCell(new Phrase((string)dataLine["InvoiceLine"], fontTitleFactura));
                celValL.Colspan = 1;
                celValL.Padding = 2;
                celValL.Border = 0;
                //celTextL.BorderWidthRight = 1;
                celValL.BorderWidthLeft = 1;
                celValL.HorizontalAlignment = Element.ALIGN_CENTER;
                celValL.VerticalAlignment = Element.ALIGN_TOP;

                table.AddCell(celValL);

                iTextSharp.text.pdf.PdfPCell celValCantidad = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    decimal.Parse((string)dataLine["SellingShipQty"]).ToString("N"), fontTitleFactura));
                celValCantidad.Colspan = 1;
                celValCantidad.Padding = 2;
                celValCantidad.Border = 0;
                celValCantidad.BorderWidthLeft = 1;
                celValCantidad.HorizontalAlignment = Element.ALIGN_CENTER;
                celValCantidad.VerticalAlignment = Element.ALIGN_TOP;

                table.AddCell(celValCantidad);

                iTextSharp.text.pdf.PdfPCell celValUnidad = new iTextSharp.text.pdf.PdfPCell(new Phrase((string)dataLine["SalesUM"], fontTitleFactura));
                celValUnidad.Colspan = 1;
                celValUnidad.Padding = 2;
                celValUnidad.Border = 0;
                celValUnidad.BorderWidthLeft = 1;
                celValUnidad.HorizontalAlignment = Element.ALIGN_CENTER;
                celValUnidad.VerticalAlignment = Element.ALIGN_TOP;

                table.AddCell(celValUnidad);

                iTextSharp.text.pdf.PdfPCell celValDesc = new iTextSharp.text.pdf.PdfPCell(new Phrase((string)dataLine["LineDesc"], fontTitleFactura));
                celValDesc.Colspan = 1;
                celValDesc.Padding = 2;
                celValDesc.Border = 0;
                celValDesc.BorderWidthLeft = 1;
                celValDesc.HorizontalAlignment = Element.ALIGN_LEFT;
                celValDesc.VerticalAlignment = Element.ALIGN_TOP;

                table.AddCell(celValDesc);

                iTextSharp.text.pdf.PdfPCell celValValorUnitario = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    decimal.Parse((string)dataLine["DocUnitPrice"]).ToString("N2"), fontTitleFactura));
                celValValorUnitario.Colspan = 1;
                celValValorUnitario.Border = 0;
                celValValorUnitario.BorderWidthLeft = 1;
                celValValorUnitario.Padding = 2;
                celValValorUnitario.HorizontalAlignment = Element.ALIGN_CENTER;
                celValValorUnitario.VerticalAlignment = Element.ALIGN_TOP;

                table.AddCell(celValValorUnitario);

                iTextSharp.text.pdf.PdfPCell celValCodigoCliente = new iTextSharp.text.pdf.PdfPCell(new Phrase("CodigoCliente", fontTitleFactura));
                celValCodigoCliente.Colspan = 1;
                celValCodigoCliente.Padding = 2;
                celValCodigoCliente.Border = 0;
                celValCodigoCliente.BorderWidthLeft = 1;
                //celDescripcion.BorderColorBottom = BaseColor.WHITE;
                celValCodigoCliente.HorizontalAlignment = Element.ALIGN_CENTER;
                celValCodigoCliente.VerticalAlignment = Element.ALIGN_TOP;

                table.AddCell(celValCodigoCliente);

                iTextSharp.text.pdf.PdfPCell celValValorTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    decimal.Parse((string)dataLine["DspDocExtPrice"]).ToString("N2"),
                    fontTitleFactura));
                celValValorTotal.Colspan = 1;
                celValValorTotal.Padding = 2;
                celValValorTotal.Border = 0;
                celValValorTotal.BorderWidthLeft = 1;
                celValValorTotal.BorderWidthRight = 1;
                celValValorTotal.HorizontalAlignment = Element.ALIGN_RIGHT;
                celValValorTotal.VerticalAlignment = Element.ALIGN_TOP;

                table.AddCell(celValValorTotal);

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return false;
            }
        }

        private bool AddUnidadesNotaCreditoAbracol(DataRow dataLine, ref PdfPTable table, iTextSharp.text.Font fontTitleFactura, DataSet dataSet)
        {
            try
            {
                iTextSharp.text.pdf.PdfPCell celValL = new iTextSharp.text.pdf.PdfPCell(new Phrase((string)dataLine["InvoiceLine"], fontTitleFactura));
                celValL.Colspan = 1;
                celValL.Padding = 2;
                celValL.Border = 0;
                celValL.PaddingBottom = 7;
                celValL.HorizontalAlignment = Element.ALIGN_CENTER;
                celValL.VerticalAlignment = Element.ALIGN_TOP;

                table.AddCell(celValL);

                iTextSharp.text.pdf.PdfPCell celValCodigo = new iTextSharp.text.pdf.PdfPCell(new Phrase((string)dataLine["PartNum"], fontTitleFactura));
                celValCodigo.Colspan = 1;
                celValCodigo.Padding = 2;
                celValCodigo.Border = 0;
                celValCodigo.PaddingBottom = 7;
                celValCodigo.HorizontalAlignment = Element.ALIGN_LEFT;
                celValCodigo.VerticalAlignment = Element.ALIGN_TOP;

                table.AddCell(celValCodigo);

                iTextSharp.text.pdf.PdfPCell celValCantidad = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    $"({decimal.Parse((string)dataLine["SellingShipQty"]).ToString("N")})", fontTitleFactura));
                celValCantidad.Colspan = 1;
                celValCantidad.Padding = 2;
                celValCantidad.Border = 0;
                celValCantidad.PaddingBottom = 7;
                celValCantidad.HorizontalAlignment = Element.ALIGN_RIGHT;
                celValCantidad.VerticalAlignment = Element.ALIGN_TOP;

                table.AddCell(celValCantidad);

                iTextSharp.text.pdf.PdfPCell celValUnidad = new iTextSharp.text.pdf.PdfPCell(new Phrase((string)dataLine["SalesUM"], fontTitleFactura));
                celValUnidad.Colspan = 1;
                celValUnidad.Padding = 2;
                celValUnidad.Border = 0;
                celValUnidad.PaddingBottom = 7;
                celValUnidad.HorizontalAlignment = Element.ALIGN_LEFT;
                celValUnidad.VerticalAlignment = Element.ALIGN_TOP;

                table.AddCell(celValUnidad);

                iTextSharp.text.pdf.PdfPCell celValDesc = new iTextSharp.text.pdf.PdfPCell(new Phrase((string)dataLine["LineDesc"], fontTitleFactura));
                celValDesc.Colspan = 1;
                celValDesc.Padding = 2;
                celValDesc.Border = 0;
                celValDesc.PaddingBottom = 7;
                celValDesc.HorizontalAlignment = Element.ALIGN_LEFT;
                celValDesc.VerticalAlignment = Element.ALIGN_TOP;

                table.AddCell(celValDesc);

                iTextSharp.text.pdf.PdfPCell celValValorUnitario = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    decimal.Parse((string)dataLine["DocUnitPrice"]).ToString("N2"), fontTitleFactura));
                celValValorUnitario.Colspan = 1;
                celValValorUnitario.Border = 0;
                celValValorUnitario.PaddingBottom = 7;
                celValValorUnitario.Padding = 2;
                celValValorUnitario.HorizontalAlignment = Element.ALIGN_RIGHT;
                celValValorUnitario.VerticalAlignment = Element.ALIGN_TOP;

                table.AddCell(celValValorUnitario);

                iTextSharp.text.pdf.PdfPCell celValDescuento = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    (string)dataLine["DiscountPercent"], fontTitleFactura));
                celValDescuento.Colspan = 1;
                celValDescuento.Padding = 2;
                celValDescuento.Border = 0;
                celValDescuento.PaddingBottom = 7;
                celValDescuento.HorizontalAlignment = Element.ALIGN_RIGHT;
                celValDescuento.VerticalAlignment = Element.ALIGN_TOP;

                table.AddCell(celValDescuento);

                iTextSharp.text.pdf.PdfPCell celValValorTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    $"({decimal.Parse((string)dataLine["DspDocExtPrice"]).ToString("N2")})",
                    fontTitleFactura));
                celValValorTotal.Colspan = 1;
                celValValorTotal.Padding = 2;
                celValValorTotal.Border = 0;
                celValValorTotal.PaddingBottom = 7;
                celValValorTotal.HorizontalAlignment = Element.ALIGN_RIGHT;
                celValValorTotal.VerticalAlignment = Element.ALIGN_TOP;

                table.AddCell(celValValorTotal);

                iTextSharp.text.pdf.PdfPCell celValIva = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    GetPercentIdImpDIAN((string)dataLine["InvoiceNum"], (string)dataLine["InvoiceLine"], dataSet.Tables["InvcTax"]),
                    fontTitleFactura));
                celValIva.Colspan = 1;
                celValIva.Padding = 2;
                celValIva.Border = 0;
                celValIva.PaddingBottom = 7;
                celValIva.HorizontalAlignment = Element.ALIGN_RIGHT;
                celValIva.VerticalAlignment = Element.ALIGN_TOP;

                table.AddCell(celValIva);
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return false;
            }
        }
        //Metodo para agregar los espacios en la lista de unidades
        private bool AddUnidadesAbracol(ref PdfPTable table)
        {
            try
            {
                iTextSharp.text.pdf.PdfPCell celValL = new iTextSharp.text.pdf.PdfPCell(new Phrase(" "));
                celValL.Colspan = 1;
                celValL.Padding = 3;
                celValL.Border = 0;
                //celTextL.BorderWidthRight = 1;
                celValL.BorderWidthLeft = 1;
                celValL.HorizontalAlignment = Element.ALIGN_CENTER;
                celValL.VerticalAlignment = Element.ALIGN_TOP;

                table.AddCell(celValL);

                iTextSharp.text.pdf.PdfPCell celValCodigo = new iTextSharp.text.pdf.PdfPCell(new Phrase(" "));
                celValCodigo.Colspan = 1;
                celValCodigo.Padding = 3;
                celValCodigo.Border = 0;
                celValCodigo.BorderWidthLeft = 1;
                //celDescripcion.BorderColorBottom = BaseColor.WHITE;
                celValCodigo.HorizontalAlignment = Element.ALIGN_CENTER;
                celValCodigo.VerticalAlignment = Element.ALIGN_TOP;

                table.AddCell(celValCodigo);

                iTextSharp.text.pdf.PdfPCell celValCantidad = new iTextSharp.text.pdf.PdfPCell(new Phrase(" "));
                celValCantidad.Colspan = 1;
                celValCantidad.Padding = 3;
                celValCantidad.Border = 0;
                celValCantidad.BorderWidthLeft = 1;
                celValCantidad.HorizontalAlignment = Element.ALIGN_CENTER;
                celValCantidad.VerticalAlignment = Element.ALIGN_TOP;

                table.AddCell(celValCantidad);

                iTextSharp.text.pdf.PdfPCell celValUnidad = new iTextSharp.text.pdf.PdfPCell(new Phrase(" "));
                celValUnidad.Colspan = 1;
                celValUnidad.Padding = 3;
                celValUnidad.Border = 0;
                celValUnidad.BorderWidthLeft = 1;
                celValUnidad.HorizontalAlignment = Element.ALIGN_CENTER;
                celValUnidad.VerticalAlignment = Element.ALIGN_TOP;

                table.AddCell(celValUnidad);

                iTextSharp.text.pdf.PdfPCell celValDesc = new iTextSharp.text.pdf.PdfPCell(new Phrase(" "));
                celValDesc.Colspan = 1;
                celValDesc.Padding = 3;
                celValDesc.Border = 0;
                celValDesc.BorderWidthLeft = 1;
                celValDesc.HorizontalAlignment = Element.ALIGN_CENTER;
                celValDesc.VerticalAlignment = Element.ALIGN_TOP;

                table.AddCell(celValDesc);

                iTextSharp.text.pdf.PdfPCell celValValorUnitario = new iTextSharp.text.pdf.PdfPCell(new Phrase(" "));
                celValValorUnitario.Colspan = 1;
                celValValorUnitario.Border = 0;
                celValValorUnitario.BorderWidthLeft = 1;
                celValValorUnitario.Padding = 3;
                celValValorUnitario.HorizontalAlignment = Element.ALIGN_CENTER;
                celValValorUnitario.VerticalAlignment = Element.ALIGN_TOP;

                table.AddCell(celValValorUnitario);

                iTextSharp.text.pdf.PdfPCell celValDescuento = new iTextSharp.text.pdf.PdfPCell(new Phrase(" "));
                celValDescuento.Colspan = 1;
                celValDescuento.Padding = 3;
                celValDescuento.Border = 0;
                celValDescuento.BorderWidthLeft = 1;
                celValDescuento.HorizontalAlignment = Element.ALIGN_CENTER;
                celValDescuento.VerticalAlignment = Element.ALIGN_TOP;

                table.AddCell(celValDescuento);

                iTextSharp.text.pdf.PdfPCell celValValorTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase(" "));
                celValValorTotal.Colspan = 1;
                celValValorTotal.Padding = 3;
                celValValorTotal.Border = 0;
                celValValorTotal.BorderWidthLeft = 1;
                celValValorTotal.HorizontalAlignment = Element.ALIGN_CENTER;
                celValValorTotal.VerticalAlignment = Element.ALIGN_TOP;

                table.AddCell(celValValorTotal);

                iTextSharp.text.pdf.PdfPCell celValIva = new iTextSharp.text.pdf.PdfPCell(new Phrase(" "));
                celValIva.Colspan = 1;
                celValIva.Padding = 3;
                celValIva.Border = 0;
                celValIva.BorderWidthLeft = 1;
                celValIva.BorderWidthRight = 1;
                celValIva.HorizontalAlignment = Element.ALIGN_CENTER;
                celValIva.VerticalAlignment = Element.ALIGN_TOP;

                table.AddCell(celValIva);
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return false;
            }
        }

        //Metodo para agregar los espacios en la lista de unidades
        private bool AddUnidadesExportacionAbracol(ref PdfPTable table)
        {
            try
            {
                iTextSharp.text.pdf.PdfPCell celEspacio = new iTextSharp.text.pdf.PdfPCell(new Phrase(" "));
                celEspacio.Border = 0;
                celEspacio.Colspan = 3;
                celEspacio.BorderWidthLeft = 1;
                celEspacio.BorderWidthRight = 1;

                table.AddCell(celEspacio);
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return false;
            }
        }

        //Metodo para agregar los espacios en la lista de unidades
        private bool AddUnidadesNotaCreditoAbracol(ref PdfPTable table)
        {
            try
            {
                iTextSharp.text.pdf.PdfPCell celValL = new iTextSharp.text.pdf.PdfPCell(new Phrase(" "));
                celValL.Colspan = 1;
                celValL.Padding = 3;
                celValL.Border = 0;
                //celTextL.BorderWidthRight = 1;
                //celValL.BorderWidthLeft = 1;
                celValL.HorizontalAlignment = Element.ALIGN_CENTER;
                celValL.VerticalAlignment = Element.ALIGN_TOP;

                table.AddCell(celValL);

                iTextSharp.text.pdf.PdfPCell celValCodigo = new iTextSharp.text.pdf.PdfPCell(new Phrase(" "));
                celValCodigo.Colspan = 1;
                celValCodigo.Padding = 3;
                celValCodigo.Border = 0;
                //celValCodigo.BorderWidthLeft = 1;
                //celDescripcion.BorderColorBottom = BaseColor.WHITE;
                celValCodigo.HorizontalAlignment = Element.ALIGN_CENTER;
                celValCodigo.VerticalAlignment = Element.ALIGN_TOP;

                table.AddCell(celValCodigo);

                iTextSharp.text.pdf.PdfPCell celValCantidad = new iTextSharp.text.pdf.PdfPCell(new Phrase(" "));
                celValCantidad.Colspan = 1;
                celValCantidad.Padding = 3;
                celValCantidad.Border = 0;
                //celValCantidad.BorderWidthLeft = 1;
                celValCantidad.HorizontalAlignment = Element.ALIGN_CENTER;
                celValCantidad.VerticalAlignment = Element.ALIGN_TOP;

                table.AddCell(celValCantidad);

                iTextSharp.text.pdf.PdfPCell celValUnidad = new iTextSharp.text.pdf.PdfPCell(new Phrase(" "));
                celValUnidad.Colspan = 1;
                celValUnidad.Padding = 3;
                celValUnidad.Border = 0;
                //celValUnidad.BorderWidthLeft = 1;
                celValUnidad.HorizontalAlignment = Element.ALIGN_CENTER;
                celValUnidad.VerticalAlignment = Element.ALIGN_TOP;

                table.AddCell(celValUnidad);

                iTextSharp.text.pdf.PdfPCell celValDesc = new iTextSharp.text.pdf.PdfPCell(new Phrase(" "));
                celValDesc.Colspan = 1;
                celValDesc.Padding = 3;
                celValDesc.Border = 0;
                //celValDesc.BorderWidthLeft = 1;
                celValDesc.HorizontalAlignment = Element.ALIGN_CENTER;
                celValDesc.VerticalAlignment = Element.ALIGN_TOP;

                table.AddCell(celValDesc);

                iTextSharp.text.pdf.PdfPCell celValValorUnitario = new iTextSharp.text.pdf.PdfPCell(new Phrase(" "));
                celValValorUnitario.Colspan = 1;
                celValValorUnitario.Border = 0;
                //celValValorUnitario.BorderWidthLeft = 1;
                celValValorUnitario.Padding = 3;
                celValValorUnitario.HorizontalAlignment = Element.ALIGN_CENTER;
                celValValorUnitario.VerticalAlignment = Element.ALIGN_TOP;

                table.AddCell(celValValorUnitario);

                iTextSharp.text.pdf.PdfPCell celValDescuento = new iTextSharp.text.pdf.PdfPCell(new Phrase(" "));
                celValDescuento.Colspan = 1;
                celValDescuento.Padding = 3;
                celValDescuento.Border = 0;
                //celValDescuento.BorderWidthLeft = 1;
                celValDescuento.HorizontalAlignment = Element.ALIGN_CENTER;
                celValDescuento.VerticalAlignment = Element.ALIGN_TOP;

                table.AddCell(celValDescuento);

                iTextSharp.text.pdf.PdfPCell celValValorTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase(" "));
                celValValorTotal.Colspan = 1;
                celValValorTotal.Padding = 3;
                celValValorTotal.Border = 0;
                //celValValorTotal.BorderWidthLeft = 1;
                celValValorTotal.HorizontalAlignment = Element.ALIGN_CENTER;
                celValValorTotal.VerticalAlignment = Element.ALIGN_TOP;

                table.AddCell(celValValorTotal);

                iTextSharp.text.pdf.PdfPCell celValIva = new iTextSharp.text.pdf.PdfPCell(new Phrase(" "));
                celValIva.Colspan = 1;
                celValIva.Padding = 3;
                celValIva.Border = 0;
                celValIva.HorizontalAlignment = Element.ALIGN_CENTER;
                celValIva.VerticalAlignment = Element.ALIGN_TOP;

                table.AddCell(celValIva);
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return false;
            }
        }

        public class Abracol
        {
            public class FacturaNacional
            {
                public static PdfPTable Encabezado(string RutaImg)
                {
                    Helpers.Plantilla_4.EncabezadoDatos datos = new Helpers.Plantilla_4.EncabezadoDatos();
                    return Helpers.Plantilla_4.Encabezado(RutaImg, datos);
                }
                public static PdfPTable DatosCliente(DataSet ds, string CUFE, System.Drawing.Image image)
                {
                    Helpers.Plantilla_4.DatosClienteDatos datos = new Helpers.Plantilla_4.DatosClienteDatos(ds);

                    PdfPTable tableFacturar = new PdfPTable(3);
                    //Dimenciones.
                    float[] DimencionFacturar = new float[3];
                    DimencionFacturar[0] = 1.0F;//
                    DimencionFacturar[1] = 1.0F;//
                    DimencionFacturar[2] = 1.0F;//

                    tableFacturar.WidthPercentage = 100;
                    tableFacturar.SetWidths(DimencionFacturar);
                    //----------------------------------------------------------------------------------------------
                    PdfPCell celNofactura = new PdfPCell(new Phrase(
                        $"FACTURA DE VENTA No. {(string)ds.Tables["InvcHead"].Rows[0]["LegalNumber"]}",
                        FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 12, iTextSharp.text.Font.NORMAL)));
                    celNofactura.HorizontalAlignment = Element.ALIGN_RIGHT;
                    celNofactura.Colspan = 3;
                    celNofactura.Padding = 3;
                    celNofactura.BorderWidthLeft = 0;
                    celNofactura.BorderWidthRight = 0;
                    tableFacturar.AddCell(celNofactura);
                    //----------------------------------------------------------------------------------------------
                    PdfPTable tableDatosCliente = new PdfPTable(1);
                    float[] DimencionFacturarA = new float[2];
                    DimencionFacturarA[0] = 1.0F;//
                    DimencionFacturarA[1] = 9.0F;//

                    tableDatosCliente.WidthPercentage = 100;
                    //-------------------------- Señores -------------------------------------------------------------------
                    PdfPCell celNombreCliente = new PdfPCell();
                    //celNombreCliente.AddElement(phrSeñores);
                    celNombreCliente.Border = 0;
                    //celNombreCliente.Padding = 0;
                    Helpers.Plantilla_4.DatosClienteTitulos(ref celNombreCliente, "Señores: ", ds.Tables["Customer"].Rows[0]["Name"].ToString());
                    tableDatosCliente.AddCell(celNombreCliente);


                    //--------------------------- NIT -------------------------------------------------------------------------

                    Chunk TituloNIT = new Chunk("NIT: ", Helpers.Fuentes.Plantilla_4_fontTitle2);
                    Chunk ValorNIT = new Chunk((string)ds.Tables["Customer"].Rows[0]["ResaleID"] + "\r\n", Helpers.Fuentes.Plantilla_4_fontTitleFactura);
                    Phrase phrNIT = new Phrase();
                    phrNIT.Add(TituloNIT);
                    phrNIT.Add(ValorNIT);
                    PdfPCell celNit = new PdfPCell();
                    celNit.AddElement(phrNIT);
                    celNit.Border = 0;
                    tableDatosCliente.AddCell(celNit);
                    //------------------- Teléfono y Fax --------------------------------------------------------------------------------------------------
                    PdfPTable tableTel = new PdfPTable(2);
                    tableTel.WidthPercentage = 100;
                    tableTel.SetWidths(new float[] { 1f, 1f });
                    tableTel.PaddingTop = 0;

                    Chunk TituloTelefono = new Chunk("Teléfono: ", Helpers.Fuentes.Plantilla_4_fontTitle2);
                    Chunk ValorTelefono = new Chunk((string)ds.Tables["Customer"].Rows[0]["PhoneNum"], Helpers.Fuentes.Plantilla_4_fontTitleFactura);
                    Phrase phrTelefono = new Phrase();
                    phrTelefono.Add(TituloTelefono);
                    phrTelefono.Add(ValorTelefono);
                    PdfPCell celValTelefonoCliente = new PdfPCell();
                    celValTelefonoCliente.AddElement(phrTelefono);
                    celValTelefonoCliente.Colspan = 1;
                    //celValTelefonoCliente.Padding = 0;
                    celValTelefonoCliente.Border = 0;
                    celValTelefonoCliente.HorizontalAlignment = Element.ALIGN_LEFT;
                    celValTelefonoCliente.VerticalAlignment = Element.ALIGN_TOP;
                    tableTel.AddCell(celValTelefonoCliente);

                    Chunk TituloFax = new Chunk("Fax: ", Helpers.Fuentes.Plantilla_4_fontTitle2);
                    Chunk ValorFax = new Chunk(ds.Tables["Customer"].Rows[0]["FaxNum"].ToString(), Helpers.Fuentes.Plantilla_4_fontTitleFactura);
                    Phrase phrFax = new Phrase();
                    phrFax.Add(TituloFax);
                    phrFax.Add(ValorFax);
                    PdfPCell celValFaxCliente = new PdfPCell();
                    celValFaxCliente.AddElement(phrFax);
                    celValFaxCliente.Colspan = 1;
                    celValFaxCliente.Padding = 0;
                    celValFaxCliente.Border = 0;
                    celValFaxCliente.HorizontalAlignment = Element.ALIGN_LEFT;
                    celValFaxCliente.VerticalAlignment = Element.ALIGN_TOP;
                    tableTel.AddCell(celValFaxCliente);

                    PdfPCell celTel = new PdfPCell(tableTel);
                    celTel.Border = 0;
                    tableDatosCliente.AddCell(celTel);
                    //--------- Evaluando forma de pago -------
                    if (datos.CelFormaPago != null)
                    {
                        tableDatosCliente.AddCell(datos.CelFormaPago);
                    }
                    //---------------- Domicilio principal -----------------------------------------------------------------------------------
                    Chunk TituloDomicilioPPal = new Chunk("Domicilio Ppal: ", Helpers.Fuentes.Plantilla_4_fontTitle2);
                    Chunk ValorDomicilioPPal = new Chunk(ds.Tables["Customer"].Rows[0]["Address1"].ToString(),Helpers.Fuentes.Plantilla_4_fontTitleFactura);
                    Phrase phrDomicilioPPal = new Phrase();
                    phrDomicilioPPal.Add(TituloDomicilioPPal);
                    phrDomicilioPPal.Add(ValorDomicilioPPal);

                    PdfPCell celDomicilioPpal = new PdfPCell(); //new PdfPCell(tableDomicilioPpal);
                    celDomicilioPpal.AddElement(phrDomicilioPPal);
                    celDomicilioPpal.Border = 0;
                    tableDatosCliente.AddCell(celDomicilioPpal);
                    //-------------------- Domicilio Embarque ------------------------------------------------------------------------------------
                    PdfPCell celDomicilioEmbarque = new PdfPCell(); //new PdfPCell(tableDomicilioEmbarque);
                    Helpers.Plantilla_4.DatosClienteTitulos(ref celDomicilioEmbarque, "Domicilio Embarque: ", ds.Tables["InvcHead"].Rows[0]["Character03"].ToString());
                    celDomicilioEmbarque.Border = 0;
                    tableDatosCliente.AddCell(celDomicilioEmbarque);
                    //------------------- Codigo Tienda ------------------------------------
                    string CodigoTienda = string.Empty;
                    if(Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead","CodigoTienda",0))
                        CodigoTienda = ds.Tables["InvcHead"].Rows[0]["CodigoTienda"].ToString();
                    PdfPCell celCodigoTienda = new PdfPCell();
                    Helpers.Plantilla_4.DatosClienteTitulos(ref celCodigoTienda, "Código Tienda: ", CodigoTienda);
                    celCodigoTienda.Border = 0;
                    tableDatosCliente.AddCell(celCodigoTienda);
                    //--------------------------------------------------------------------------------------------------------------
                    iTextSharp.text.pdf.PdfPCell celdatosCliente = new iTextSharp.text.pdf.PdfPCell(tableDatosCliente);
                    celdatosCliente.Colspan = 1;
                    tableFacturar.AddCell(celdatosCliente);
                    //--------------------------------------------------------------------------------------------------------------
                    iTextSharp.text.Font fontTitleFecha = FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL);
                    iTextSharp.text.Font fontDMA = FontFactory.GetFont(FontFactory.HELVETICA, 6, BaseColor.GRAY);
                    iTextSharp.text.Font fontDetalleFecha = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 8, iTextSharp.text.Font.NORMAL);
                    //--------------------------------------------------------------------------------------------------------------
                    PdfPTable tableToCel2 = new PdfPTable(2);
                    float[] DimencionDespacharA = new float[2];
                    DimencionDespacharA[0] = 1.0F;//
                    DimencionDespacharA[1] = 1.0F;//

                    tableToCel2.WidthPercentage = 100;
                    tableToCel2.SetWidths(DimencionDespacharA);

                    PdfPTable tableFechaEmision = new PdfPTable(1);

                    iTextSharp.text.pdf.PdfPCell celTextFechaEmision = new iTextSharp.text.pdf.PdfPCell(new Phrase("Fecha Emisión", fontTitleFecha));
                    celTextFechaEmision.Colspan = 1;
                    //celTextFechaEmision.Padding = 3;
                    celTextFechaEmision.Border = 0;
                    celTextFechaEmision.HorizontalAlignment = Element.ALIGN_CENTER;
                    celTextFechaEmision.VerticalAlignment = Element.ALIGN_TOP;
                    tableFechaEmision.AddCell(celTextFechaEmision);

                    iTextSharp.text.pdf.PdfPCell celDMA = new iTextSharp.text.pdf.PdfPCell(new Phrase("DD/MM/AAAA", fontDMA));
                    celDMA.Colspan = 1;
                    //celDMA.Padding = 3;
                    celDMA.Border = 0;
                    celDMA.HorizontalAlignment = Element.ALIGN_CENTER;
                    celDMA.VerticalAlignment = Element.ALIGN_TOP;
                    tableFechaEmision.AddCell(celDMA);

                    //------ Dato fecha y hora ------
                    //iTextSharp.text.pdf.PdfPCell celFechaEmisionVal = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    //    DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).ToString("dd/MM/yyyy"), fontDetalleFecha));
                    iTextSharp.text.pdf.PdfPCell celFechaEmisionVal = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                        datos.FechayHora, fontDetalleFecha));
                    celFechaEmisionVal.Colspan = 1;
                    //celFechaEmisionVal.Padding = 3;
                    celFechaEmisionVal.Border = 0;
                    celFechaEmisionVal.HorizontalAlignment = Element.ALIGN_CENTER;
                    celFechaEmisionVal.VerticalAlignment = Element.ALIGN_TOP;
                    tableFechaEmision.AddCell(celFechaEmisionVal);
                    PdfPCell celFechaEmision = new PdfPCell(tableFechaEmision);
                    tableToCel2.AddCell(celFechaEmision);

                    //---------------------------------------------------------------------------------------------------------------
                    PdfPTable tableFechaDetalleVencimiento = new PdfPTable(1);

                    iTextSharp.text.pdf.PdfPCell celTextDetalleFechaVencimiento = new iTextSharp.text.pdf.PdfPCell(new Phrase("Fecha Vencimiento", fontTitleFecha));
                    celTextDetalleFechaVencimiento.Colspan = 1;
                    //celTextDetalleFechaVencimiento.Padding = 3;
                    celTextDetalleFechaVencimiento.Border = 0;
                    celTextDetalleFechaVencimiento.HorizontalAlignment = Element.ALIGN_CENTER;
                    celTextDetalleFechaVencimiento.VerticalAlignment = Element.ALIGN_TOP;
                    tableFechaDetalleVencimiento.AddCell(celTextDetalleFechaVencimiento);

                    tableFechaDetalleVencimiento.AddCell(celDMA);

                    iTextSharp.text.pdf.PdfPCell celFechaVencimientoVal = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                        DateTime.Parse((string)ds.Tables["InvcHead"].Rows[0]["DueDate"]).ToString("dd/MM/yyyy"), fontDetalleFecha));
                    celFechaVencimientoVal.Colspan = 1;
                    //celFechaVencimientoVal.Padding = 3;
                    celFechaVencimientoVal.Border = 0;
                    celFechaVencimientoVal.HorizontalAlignment = Element.ALIGN_CENTER;
                    celFechaVencimientoVal.VerticalAlignment = Element.ALIGN_TOP;
                    tableFechaDetalleVencimiento.AddCell(celFechaVencimientoVal);

                    PdfPCell celFechaDetalleVencimiento = new PdfPCell(tableFechaDetalleVencimiento);
                    tableToCel2.AddCell(celFechaDetalleVencimiento);
                    //-------------------------------------------------------------------------------------------------------------

                    iTextSharp.text.pdf.PdfPCell celNoPedido = new PdfPCell();
                    Helpers.Plantilla_4.DatosClienteTitulos(ref celNoPedido, "Pedido No: ", ds.Tables["InvcHead"].Rows[0]["OrderNum"].ToString());
                    tableToCel2.AddCell(celNoPedido);
                    //-------------------------------------------------------------------------------------
                    iTextSharp.text.pdf.PdfPCell celOrderCompra = new iTextSharp.text.pdf.PdfPCell();
                    Helpers.Plantilla_4.DatosClienteTitulos(ref celOrderCompra, "Orden de Compra: ", (string)ds.Tables["InvcHead"].Rows[0]["PONum"]);
                    tableToCel2.AddCell(celOrderCompra);
                    //-------------------------------------------------------------------------------------

                    PdfPCell celVendededor = new PdfPCell();//new PdfPCell(tableVendedor);
                    Helpers.Plantilla_4.DatosClienteTitulos(ref celVendededor, "Vendedor: ", ds.Tables["InvcHead"].Rows[0]["SalesRepName1"].ToString());
                    celVendededor.Colspan = 2;
                    tableToCel2.AddCell(celVendededor);
                    //-------------------------------------------------------------------------------------

                    PdfPCell celCodigoVendededor = new PdfPCell();//new PdfPCell(tableCodigoVendedor);
                    Helpers.Plantilla_4.DatosClienteTitulos(ref celCodigoVendededor, "Código Vendedor: ", ds.Tables["InvcHead"].Rows[0]["SalesRepCode1"].ToString());
                    celCodigoVendededor.Colspan = 2;
                    tableToCel2.AddCell(celCodigoVendededor);
                    //-------------- Nro Aviso -------------------------
                    PdfPCell celnroAviso = new PdfPCell();
                    string NroAviso = string.Empty;
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "NroAviso", 0))
                        NroAviso = ds.Tables["InvcHead"].Rows[0]["NroAviso"].ToString();
                    Helpers.Plantilla_4.DatosClienteTitulos(ref celnroAviso, "Nro Aviso: ", NroAviso);
                    celnroAviso.Colspan = 2;
                    tableToCel2.AddCell(celnroAviso);
                        //-------------------------------------------------------------------------------------
                    iTextSharp.text.pdf.PdfPCell cel2 = new iTextSharp.text.pdf.PdfPCell(tableToCel2);
                    cel2.Colspan = 1;
                    tableFacturar.AddCell(cel2);


                    /*CELL QR*/
                    Paragraph prgCufe = new Paragraph("CUFE: " + CUFE, Helpers.Fuentes.Plantilla_4_fontTitle2);
                    prgCufe.Alignment = Element.ALIGN_LEFT;

                    iTextSharp.text.Image QRPdf = iTextSharp.text.Image.GetInstance(image, BaseColor.WHITE);
                    QRPdf.ScaleAbsolute(90f, 90f);
                    QRPdf.Alignment = Element.ALIGN_CENTER;
                    //iTextSharp.text.pdf.PdfPCell celImgQR = new iTextSharp.text.pdf.PdfPCell(QRPdf);
                    iTextSharp.text.pdf.PdfPCell celImgQR = new iTextSharp.text.pdf.PdfPCell();
                    celImgQR.AddElement(QRPdf);
                    celImgQR.AddElement(prgCufe);
                    celImgQR.Colspan = 2;
                    celImgQR.Padding = 3;
                    celImgQR.Border = 0;
                    celImgQR.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                    celImgQR.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    tableFacturar.AddCell(celImgQR);
                    //----- Retornando tabla
                    return tableFacturar;
                }
            }
        }
        public partial class Helpers
        {
            public class Abracol
            {
                public static void GetFletes(DataSet ds, ref decimal fletes)
                {
                    fletes = 0;
                    if (ds == null)
                        return;
                    if (!ds.Tables.Contains("InvcMisc"))
                        return;
                    if (ds.Tables["InvcMisc"].Rows.Count <= 0)
                        return;
                    if (!ds.Tables["InvcMisc"].Columns.Contains("MiscCode"))
                        return;
                    var InvcMiscLinq = from row in ds.Tables["InvcMisc"].AsEnumerable()
                                       select new InvcMisc
                                       {
                                           Company = row["Company"].ToString(),
                                           InvoiceNum = Convert.ToInt32(row["InvoiceNum"]),
                                           InvoiceLine = Convert.ToInt32(row["InvoiceLine"]),
                                           SeqNum = Convert.ToInt32(row["SeqNum"]),
                                           MiscCode = row["MiscCode"].ToString(),
                                           MiscAmt = Convert.ToDecimal(row["MiscAmt"]),
                                           DocMiscAmt = Convert.ToDecimal(row["DocMiscAmt"])
                                       };
                    if(InvcMiscLinq != null && InvcMiscLinq.Count() > 0)
                    {
                        //realiza la consulta de los fletes
                        var flete = from row in InvcMiscLinq
                                     where row.MiscCode == "Fles"
                                     && row.MiscCode == "FVN"
                                     group row by new { row.Company, row.InvoiceNum } into grp
                                     select new {
                                         grp.Key.Company,
                                         grp.Key.InvoiceNum,
                                         TotalFletes = grp.Sum(row => row.DocMiscAmt)
                                     };
                        if(flete != null && flete.Count() > 0)
                        {
                            var registro = flete.FirstOrDefault();
                            if(registro != null)
                            {
                                fletes = registro.TotalFletes;
                            }
                        }
                    }
                }
                public class InvcMisc
                {
                    public string Company { get; set; }
                    public int InvoiceNum { get; set; }
                    public int InvoiceLine { get; set; }
                    public int SeqNum { get; set; }
                    public string MiscCode { get; set; }
                    public decimal MiscAmt { get; set; }
                    public decimal DocMiscAmt { get; set; }
                }
            }
        }
    }
}
