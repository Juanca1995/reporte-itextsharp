﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//Referenciar y usar.
using System.Data;
using System.IO;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Windows.Forms;
using System.Web;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Net;
using System.Drawing;
using System.Globalization;
using static RulesServicesDIAN2.Adquiriente.pdfEstandarAR;

namespace RulesServicesDIAN2.Adquiriente
{
    public partial class pdfEstandarAR
    {

        #region Formatos Facturacion PLASTICOS TRUHER

        float dimxTruher = 550f, dimYTruher = 15f;

        public bool FacturaNacionalPlasticosTruher(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {
            NomArchivo = string.Empty;
            string Ruta = HttpContext.Current.Server.MapPath("~/PDF/" + NIT + "/");
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            string RutaImg = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";
            NomArchivo = NombreInvoice + ".pdf";

            string RutaImgLogoTS = $@"http://{LocalIP}:8081/images/EMPRESAS/Empresa_{NIT}/Logo_{NIT}_ISO-TS.png";
            string RutaLogo_ISO_9001 = $@"http://{LocalIP}:8081/images/EMPRESAS/Empresa_{NIT}/Logo_{NIT}_ISO 9001.png";
            string RutaLogo_IQNET = $@"http://{LocalIP}:8081/images/EMPRESAS/Empresa_{NIT}/Logo_{NIT}_IQNET.png";

            try
            {
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.
                //Document document = new Document(iTextSharp.text.PageSize.LETTER, 20f, 12f, 10f, 30f);
                Document document = new Document(iTextSharp.text.PageSize.LETTER, 20f, 12f, 220f, 220f);

                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));

                // step 3: we open the document     
                //document.Open();

                PdfDiv divEspacio = new PdfDiv()
                {
                    Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE,
                    Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK,
                    Height = 10,
                    Width = 130,

                };

                PdfDiv divEspacio2 = new PdfDiv()
                {
                    Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE,
                    Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK,
                    Height = 2,
                    Width = 130,
                };

                #region Fuentes
                //FUENTES----------------------------------------------------------------------------------------------------------
                iTextSharp.text.Font fontTitleBold = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7.57f, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA, 7.57f, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitleBold2 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 12f, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle2 = FontFactory.GetFont(FontFactory.HELVETICA, 8.5f, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle2Bold = FontFactory.GetFont(FontFactory.HELVETICA, 8.5f, iTextSharp.text.Font.BOLD);
                iTextSharp.text.Font fontCustom = FontFactory.GetFont(FontFactory.HELVETICA, 6.8f, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustomI = FontFactory.GetFont(FontFactory.HELVETICA_OBLIQUE, 6.8f, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustomBold = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 6.8f, iTextSharp.text.Font.NORMAL);
                //FUENTES---------------------------------------------------------------------------------------------------------

                #endregion

                #region ENCABEZADO

                PdfPTable tableEncabezadoEvent = new PdfPTable(1);

                PdfPTable tableEncabezado = new PdfPTable(new float[] { 1f, 2.0f, 2.3f })
                {
                    WidthPercentage = 100,
                };

                System.Drawing.Image logo = null;
                var requestLogo = WebRequest.Create(RutaImg);

                using (var responseLogo = requestLogo.GetResponse())
                using (var streamLogo = responseLogo.GetResponseStream())
                {
                    logo = Bitmap.FromStream(streamLogo);
                }

                PdfDiv divLogo = new PdfDiv()
                {
                    Float = iTextSharp.text.pdf.PdfDiv.FloatType.LEFT,
                    Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE,
                    BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID,
                    Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK,
                    Height = 100,
                    Width = 100,
                    //BackgroundColor = BaseColor.LIGHT_GRAY,
                };

                iTextSharp.text.Image ImgLogo = iTextSharp.text.Image.GetInstance(logo, BaseColor.WHITE);
                ImgLogo.Alignment = Element.ALIGN_CENTER;
                divLogo.BackgroundImage = ImgLogo;

                PdfPCell celLogo = new PdfPCell()
                {
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    //Colspan = 2,
                    Border = 0,
                    Padding = 0
                };

                celLogo.AddElement(divLogo);
                tableEncabezado.AddCell(celLogo);

                Paragraph prgDepartamentoCiudad = new Paragraph($"{(string)DsInvoiceAR.Tables["Company"].Rows[0]["City"]} - " +
                    $"{(string)DsInvoiceAR.Tables["Company"].Rows[0]["State"]}", fontTitle)
                {
                    Alignment = Element.ALIGN_CENTER,
                };
                //Paragraph prgEmailWeb = ;

                PdfPCell celTextInfoEmpresa = new PdfPCell()
                {
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    //Colspan = 2,
                    Border = 0,
                    Padding = 0
                };

                celTextInfoEmpresa.AddElement(new Paragraph(
                    (string)DsInvoiceAR.Tables["Company"].Rows[0]["Name"],
                    fontTitleBold2)
                {
                    Alignment = Element.ALIGN_CENTER,
                });

                celTextInfoEmpresa.AddElement(new Paragraph("TRANSFORMANDO EXPERIENCIA EN CONFIANZA", fontTitle2)
                {
                    Alignment = Element.ALIGN_CENTER,
                });

                celTextInfoEmpresa.AddElement(new Paragraph($"NIT. " +
                    $"{Decimal.Parse((string)DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"]).ToString("N0").Replace(",", ".")}-" +
                    $"{CalcularDigitoVerificacion((string)DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"])}",
                    FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 10f, iTextSharp.text.Font.NORMAL))
                {
                    Alignment = Element.ALIGN_CENTER,
                });

                celTextInfoEmpresa.AddElement(new Paragraph(
                    $"PBX:(57 4) {(string)DsInvoiceAR.Tables["Company"].Rows[0]["PhoneNum"]}", fontTitle)
                {
                    Alignment = Element.ALIGN_CENTER,
                });

                //celTextInfoEmpresa.AddElement(new Paragraph(
                //    $"FAX: {(string)DsInvoiceAR.Tables["Company"].Rows[0]["FaxNum"]}", fontCustom)
                //{
                //    Alignment = Element.ALIGN_CENTER,
                //});

                celTextInfoEmpresa.AddElement(new Paragraph(
                    $"{(string)DsInvoiceAR.Tables["Company"].Rows[0]["Address1"]} {(string)DsInvoiceAR.Tables["Company"].Rows[0]["City"]} - " +
                    $"Colombia.", fontTitle)
                {
                    Alignment = Element.ALIGN_CENTER,
                });

                celTextInfoEmpresa.AddElement(new Paragraph($"{(string)DsInvoiceAR.Tables["Company"].Rows[0]["Character01"]}", fontCustom)
                {
                    Alignment = Element.ALIGN_CENTER,
                });

                celTextInfoEmpresa.AddElement(new Paragraph("Medellín - Manizales", fontCustom)
                {
                    Alignment = Element.ALIGN_CENTER,
                });

                celTextInfoEmpresa.AddElement(new Paragraph("www.plasticostruher.com", fontCustom)
                {
                    Alignment = Element.ALIGN_CENTER,
                });

                tableEncabezado.AddCell(celTextInfoEmpresa);
                //-----------------------------------------------------------------------------------------------------------
                PdfPTable tableNoFactura = new PdfPTable(new float[] { 1, 1.3f })
                {
                    WidthPercentage = 100,
                };

                tableNoFactura.AddCell(new PdfPCell(new Phrase($"FACTURA DE VENTA No. {(string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"]}", fontTitleBold2))
                {
                    Colspan = 2,
                    Border = PdfPCell.NO_BORDER,
                });

                var requestLogoISOTS = WebRequest.Create(RutaImgLogoTS);

                using (var responseLogoISOTS = requestLogoISOTS.GetResponse())
                using (var streamLogoISOTS = responseLogoISOTS.GetResponseStream())
                {
                    logo = Bitmap.FromStream(streamLogoISOTS);
                }

                iTextSharp.text.Image ImgLogoISOTS = iTextSharp.text.Image.GetInstance(logo, BaseColor.WHITE);
                ImgLogoISOTS.Alignment = Element.ALIGN_CENTER;

                PdfDiv divLogoISOTS = new PdfDiv()
                {
                    Float = iTextSharp.text.pdf.PdfDiv.FloatType.LEFT,
                    Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE,
                    Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK,
                    Height = 70f,
                    Width = 50f,
                    //BackgroundColor = BaseColor.LIGHT_GRAY,
                    BackgroundImage = ImgLogoISOTS
                };

                //-------------------------------------------------------------------------------------
                var requestLogoISO = WebRequest.Create(RutaLogo_ISO_9001);

                using (var responseLogoISO = requestLogoISO.GetResponse())
                using (var streamLogoISO = responseLogoISO.GetResponseStream())
                {
                    logo = Bitmap.FromStream(streamLogoISO);
                }

                iTextSharp.text.Image ImgLogoISO = iTextSharp.text.Image.GetInstance(logo, BaseColor.WHITE);
                ImgLogoISO.Alignment = Element.ALIGN_CENTER;

                PdfDiv divLogoISO = new PdfDiv()
                {
                    Float = iTextSharp.text.pdf.PdfDiv.FloatType.LEFT,
                    Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE,
                    Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK,
                    Height = 67f,
                    Width = 42f,
                    //BackgroundColor = BaseColor.LIGHT_GRAY,
                    BackgroundImage = ImgLogoISO
                };

                PdfPCell celLogoISO = new PdfPCell()
                {
                    HorizontalAlignment = Element.ALIGN_BOTTOM,
                    VerticalAlignment = Element.ALIGN_BOTTOM,
                    Border = 0,
                    Padding = 0
                };

                celLogoISO.AddElement(divLogoISOTS);
                celLogoISO.AddElement(divLogoISO);
                //celLogoISO.AddElement(new Paragraph("SC-1884-1", fontCustomBold) { Alignment = Element.ALIGN_CENTER });
                tableNoFactura.AddCell(celLogoISO);


                tableNoFactura.AddCell(new PdfPCell(new Phrase($"{(string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Character02"]}\n\n" +
                    $"{(string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Character01"]}\n\n" +
                    $"AGENTE AUTORETENEDOR DE IMPUESTO A LAS VENTAS\n IVA REGIMEN COMUN", fontTitle))
                {
                    HorizontalAlignment = Element.ALIGN_JUSTIFIED,
                    VerticalAlignment = Element.ALIGN_CENTER,
                    PaddingTop = 20,
                    Border = PdfPCell.NO_BORDER,
                });
                //-------------------------------------------------------------

                tableEncabezado.AddCell(new PdfPCell(tableNoFactura) { Border = PdfPCell.NO_BORDER, PaddingBottom = 5, });

                PdfPTable tableEncabezado2 = new PdfPTable(new float[] { 1.0f, 1.75f, 2.0f })
                {
                    WidthPercentage = 100,
                };

                PdfPTable tableConInfoCliente_QR = new PdfPTable(new float[] { 2.7f, 1f }) { WidthPercentage = 100, };
                PdfPTable tableInfoCliente = new PdfPTable(2) { WidthPercentage = 100, };

                string nitCliente = string.Empty;
                decimal convertNitCliente;

                if (((string)DsInvoiceAR.Tables["COOneTime"].Rows[0]["IdentificationType"]) == "31")
                {
                    nitCliente = decimal.TryParse((string)DsInvoiceAR.Tables["Customer"].Rows[0]["ResaleID"],
                        out convertNitCliente) ? $"{convertNitCliente.ToString("N0")}-" +
                        $"{CalcularDigitoVerificacion((string)DsInvoiceAR.Tables["Customer"].Rows[0]["ResaleID"])}" :
                        (string)DsInvoiceAR.Tables["Customer"].Rows[0]["ResaleID"];
                }
                else
                {
                    nitCliente = (string)DsInvoiceAR.Tables["Customer"].Rows[0]["ResaleID"];
                }

                tableInfoCliente.AddCell(new PdfPCell(new Phrase($"Señores: {(string)DsInvoiceAR.Tables["Customer"].Rows[0]["Name"]}",
                    fontTitle))
                { Colspan = 2, Border = PdfPCell.NO_BORDER, });
                tableInfoCliente.AddCell(new PdfPCell(new Phrase($"Dirección: {(string)DsInvoiceAR.Tables["Customer"].Rows[0]["Address1"]}",
                    fontTitle))
                { Colspan = 2, Border = PdfPCell.NO_BORDER, });
                tableInfoCliente.AddCell(new PdfPCell(new Phrase($"Ciudad: {(string)DsInvoiceAR.Tables["Customer"].Rows[0]["City"]}",
                    fontTitle))
                { Colspan = 2, Border = PdfPCell.NO_BORDER, });
                tableInfoCliente.AddCell(new PdfPCell(new Phrase($"NIT: {nitCliente}", fontTitle))
                { Border = PdfPCell.NO_BORDER, });
                tableInfoCliente.AddCell(new PdfPCell(new Phrase($"Teléfono: {(string)DsInvoiceAR.Tables["Customer"].Rows[0]["PhoneNum"]}",
                    fontTitle))
                { Border = PdfPCell.NO_BORDER, });

                tableConInfoCliente_QR.AddCell(new PdfPCell(tableInfoCliente) { Border = PdfPCell.NO_BORDER, });


                iTextSharp.text.Image ImgQR = iTextSharp.text.Image.GetInstance(QRInvoice, BaseColor.WHITE);

                PdfDiv divQR = new PdfDiv()
                {
                    Float = iTextSharp.text.pdf.PdfDiv.FloatType.RIGHT,
                    Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE,
                    BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID,
                    Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK,
                    Height = 60,
                    Width = 60,
                    //BackgroundColor=BaseColor.LIGHT_GRAY,
                    BackgroundImage = ImgQR,
                };

                PdfPCell celQR = new PdfPCell()
                {
                    Border = 0,
                };
                celQR.AddElement(divQR);

                tableConInfoCliente_QR.AddCell(celQR);

                tableEncabezado2.AddCell(new PdfPCell(tableConInfoCliente_QR) { Colspan = 2, Border = PdfPCell.NO_BORDER, });

                PdfPTable tableContLogoOrden = new PdfPTable(new float[] { 1.0F, 2.5f }) { WidthPercentage = 100, };

                var requestLogoIQNet = WebRequest.Create(RutaLogo_IQNET);

                using (var responseLogoIQNet = requestLogoIQNet.GetResponse())
                using (var streamLogoIQNet = responseLogoIQNet.GetResponseStream())
                {
                    logo = Bitmap.FromStream(streamLogoIQNet);
                }

                iTextSharp.text.Image ImgLogoIQNet = iTextSharp.text.Image.GetInstance(logo, BaseColor.WHITE);
                ImgLogoIQNet.Alignment = Element.ALIGN_CENTER;

                PdfDiv divLogoIQNet = new PdfDiv()
                {
                    Float = iTextSharp.text.pdf.PdfDiv.FloatType.RIGHT,
                    Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE,
                    Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK,
                    Height = 55f,
                    Width = 35f,
                    //BackgroundColor = BaseColor.LIGHT_GRAY,
                    BackgroundImage = ImgLogoIQNet,
                };

                PdfPCell celLogoIQNet = new PdfPCell()
                {
                    HorizontalAlignment = Element.ALIGN_RIGHT,
                    Border = PdfPCell.NO_BORDER,
                };

                celLogoIQNet.AddElement(divLogoIQNet);

                tableContLogoOrden.AddCell(celLogoIQNet);

                PdfPTable tableInfoOrdenFechas = new PdfPTable(2);

                tableInfoOrdenFechas.AddCell(new PdfPCell(new Phrase($"ORDEN DE COMPRA\n{(string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["PONum"]}", fontTitle))
                {
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    PaddingBottom = 5,
                });

                tableInfoOrdenFechas.AddCell(new PdfPCell(new Phrase($"REMISIÓN\n" +
                    $"{(string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar01"]}", fontTitle))
                {
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    PaddingBottom = 5,
                });


                tableInfoOrdenFechas.AddCell(new PdfPCell(new Phrase("FECHA FACTURA", fontTitle))
                {
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    Border = PdfPCell.LEFT_BORDER,
                });

                tableInfoOrdenFechas.AddCell(new PdfPCell(new Phrase("FECHA VENCIMIENTO", fontTitle))
                {
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    Border = PdfPCell.RIGHT_BORDER | PdfPCell.LEFT_BORDER,
                });

                var tableValFechas = new PdfPTable(3) { WidthPercentage = 100, };

                tableValFechas.AddCell(new PdfPCell(new Phrase("DD", fontTitle))
                {
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    Border = PdfPCell.LEFT_BORDER,
                });

                tableValFechas.AddCell(new PdfPCell(new Phrase("MM", fontTitle))
                {
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    Border = PdfPCell.NO_BORDER,
                });

                tableValFechas.AddCell(new PdfPCell(new Phrase("AAAA", fontTitle))
                {
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    Border = PdfPCell.RIGHT_BORDER,
                });

                tableInfoOrdenFechas.AddCell(new PdfPCell(tableValFechas) { Border = PdfPCell.NO_BORDER, });
                tableInfoOrdenFechas.AddCell(new PdfPCell(tableValFechas) { Border = PdfPCell.NO_BORDER, });
                //-------------------------------------------------------------------------------------------------------------------------------------
                tableValFechas = new PdfPTable(3) { WidthPercentage = 100, };
                tableValFechas.AddCell(new PdfPCell(new Phrase($"{DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).Day}", fontTitle))
                {
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    Border = PdfPCell.BOTTOM_BORDER | PdfPCell.LEFT_BORDER,
                });

                tableValFechas.AddCell(new PdfPCell(new Phrase($"{DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).Month}", fontTitle))
                {
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    Border = PdfPCell.BOTTOM_BORDER,
                });

                tableValFechas.AddCell(new PdfPCell(new Phrase($"{DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).Year}", fontTitle))
                {
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    Border = PdfPCell.BOTTOM_BORDER | PdfPCell.RIGHT_BORDER,
                });
                tableInfoOrdenFechas.AddCell(new PdfPCell(tableValFechas) { Border = PdfPCell.NO_BORDER, });
                //--------------------------------------------------------------------------------------------------------------------------------------
                tableValFechas = new PdfPTable(3) { WidthPercentage = 100, };
                tableValFechas.AddCell(new PdfPCell(new Phrase($"{DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"]).Day}", fontTitle))
                {
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    Border = PdfPCell.BOTTOM_BORDER,
                });

                tableValFechas.AddCell(new PdfPCell(new Phrase($"{DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"]).Month}", fontTitle))
                {
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    Border = PdfPCell.BOTTOM_BORDER,
                });

                tableValFechas.AddCell(new PdfPCell(new Phrase($"{DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"]).Year}", fontTitle))
                {
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    Border = PdfPCell.BOTTOM_BORDER | PdfPCell.RIGHT_BORDER,
                });
                tableInfoOrdenFechas.AddCell(new PdfPCell(tableValFechas) { Border = PdfPCell.NO_BORDER, });
                //--------------------------------------------------------------------------------------------------------------------------------------
                tableContLogoOrden.AddCell(new PdfPCell(tableInfoOrdenFechas) { Border = PdfPCell.NO_BORDER, });

                tableEncabezado2.AddCell(new PdfPCell(tableContLogoOrden) { Border = PdfPCell.NO_BORDER, });

                tableEncabezado.AddCell(new PdfPCell(tableEncabezado2) { Colspan = 3, Border = PdfPCell.NO_BORDER, });

                #endregion

                #region UNIDADES

                PdfPTable tableUnidades = new PdfPTable(new float[] { 1, 2.5f, 1f, 1.35f, 1f })
                {
                    WidthPercentage = 100,
                };

                tableUnidades.AddCell(new PdfPCell()
                {
                    Border = PdfPCell.NO_BORDER,
                    VerticalAlignment = Element.ALIGN_BOTTOM,
                    Phrase = new Phrase($"CUFE: {CUFE}", fontTitle2),
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    Colspan = 3,
                    Padding = 3,
                });

                tableUnidades.AddCell(new PdfPCell()
                {
                    Border = PdfPCell.NO_BORDER,
                    VerticalAlignment = Element.ALIGN_BOTTOM,
                    Phrase = new Phrase("Representación gráfica factura electrónica", fontTitle2),
                    HorizontalAlignment = Element.ALIGN_RIGHT,
                    Colspan = 2,
                    Padding = 3,
                });

                tableUnidades.AddCell(new PdfPCell()
                {
                    Border = PdfPCell.TOP_BORDER,
                    VerticalAlignment = Element.ALIGN_BOTTOM,
                    Phrase = new Phrase("CÓDIGO", fontTitle2Bold),
                    HorizontalAlignment = Element.ALIGN_LEFT
                });

                tableUnidades.AddCell(new PdfPCell()
                {
                    Border = PdfPCell.TOP_BORDER,
                    VerticalAlignment = Element.ALIGN_BOTTOM,
                    Phrase = new Phrase("DESCRIPCIÓN", fontTitle2Bold),
                    HorizontalAlignment = Element.ALIGN_CENTER,
                });

                tableUnidades.AddCell(new PdfPCell()
                {
                    Border = PdfPCell.TOP_BORDER,
                    VerticalAlignment = Element.ALIGN_BOTTOM,
                    Phrase = new Phrase("CANTIDAD", fontTitle2Bold),
                    HorizontalAlignment = Element.ALIGN_CENTER
                });

                tableUnidades.AddCell(new PdfPCell()
                {
                    Border = PdfPCell.TOP_BORDER,
                    VerticalAlignment = Element.ALIGN_BOTTOM,
                    Phrase = new Phrase("PRECIO UNITARIO", fontTitle2Bold),
                    HorizontalAlignment = Element.ALIGN_CENTER
                });

                tableUnidades.AddCell(new PdfPCell()
                {
                    Border = PdfPCell.TOP_BORDER,
                    VerticalAlignment = Element.ALIGN_BOTTOM,
                    Phrase = new Phrase("VALOR TOTAL", fontTitle2Bold),
                    HorizontalAlignment = Element.ALIGN_CENTER
                });
                ////--------------------------------------------------------------------------------------
                //for (int i = 0; i < 10; i++)
                foreach (DataRow InvoiceLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
                    if (!AddUnidadesPlasticosTruher(InvoiceLine, ref tableUnidades, fontTitle2, DsInvoiceAR))
                        return false;

                #endregion

                var tablePiePag = new PdfPTable(1)
                {
                    WidthPercentage = 100,
                    ExtendLastRow = true,
                };

                tablePiePag.CompleteRow();

                #region Totales, QR

                PdfPTable tableTotales = new PdfPTable(3) { WidthPercentage = 100, };

                //tableTotales.AddCell(celQR);
                //-------------------------------------------------------------------------------------------------------
                decimal valTrm = 0;
                var trm = decimal.TryParse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number01"], out valTrm) ?
                    valTrm.ToString("C") : string.Empty;
                //tableTotales.AddCell(new PdfPCell(new Phrase($"TRM: " +
                //    $"{trm}",
                //    fontTitle2Bold))
                //{
                //    HorizontalAlignment = Element.ALIGN_RIGHT,
                //    Border = PdfPCell.NO_BORDER,
                //});

                tableTotales.AddCell(new PdfPCell(new Phrase($"TRM: {trm}            " +
                    $"SUBTOTAL: " +
                    $"{decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubTotal"]).ToString("C")}",
                    fontTitle2Bold))
                {
                    HorizontalAlignment = Element.ALIGN_RIGHT,
                    Border = PdfPCell.NO_BORDER,
                });

                tableTotales.AddCell(new PdfPCell(new Phrase($"IVA: {GetValImpuestoByID("01", DsInvoiceAR).ToString("C")}", fontTitle2Bold))
                {
                    HorizontalAlignment = Element.ALIGN_RIGHT,
                    Border = PdfPCell.NO_BORDER,
                });

                tableTotales.AddCell(new PdfPCell(new Phrase("TOTAL: " +
                    $"{ decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"]).ToString("c") }",
                    fontTitle2Bold))
                {
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    Border = PdfPCell.NO_BORDER,
                });

                Phrase prgCuentasRecaudo = new Phrase();

                prgCuentasRecaudo.Add(new Chunk("CUENTAS PARA RECAUDO: ", fontTitle2Bold));
                prgCuentasRecaudo.Add(new Chunk("BANCOLOMBIA ", fontTitle2Bold));
                prgCuentasRecaudo.Add(new Chunk("AHORROS : 100-700375-45, ", fontTitle2));
                prgCuentasRecaudo.Add(new Chunk("B.OCCIDENTE ", fontTitle2Bold));
                prgCuentasRecaudo.Add(new Chunk("CORRIENTE : 405-03271-5, ", fontTitle2));
                prgCuentasRecaudo.Add(new Chunk("B.BOGOTA ", fontTitle2Bold));
                prgCuentasRecaudo.Add(new Chunk("AHORROS: 752-02610-4", fontTitle2));

                tableTotales.AddCell(new PdfPCell(prgCuentasRecaudo)
                {
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    Border = PdfPCell.BOTTOM_BORDER,
                    Padding = 3,
                    PaddingTop = 7,
                    Colspan = 3,
                });

                //tablePiePag.AddCell(new PdfPCell(tableTotales)
                //{
                //    //Border = PdfPCell.NO_BORDER,
                //    VerticalAlignment=Element.ALIGN_BOTTOM,
                //});
                //-------------------------------------------------------------------------------------------------------------
                //var tableContFirmas = new PdfPTable(new float[] { 1.05f,4f }) { WidthPercentage = 100, };
                //var tableContFirmas = new PdfPTable(1) { WidthPercentage = 100, };
                //var tableInfoFirmas = new PdfPTable(1) { WidthPercentage = 100, };

                PdfPCell celLeyendas = new PdfPCell()
                {
                    Colspan = 3,
                    Padding = 5,
                };

                iTextSharp.text.List list = new List();
                list.SetListSymbol("\u2022");


                list.Add(new ListItem("VENCIDO EL PLAZO SE CAUSARÁN INTERESES MORATORIOS COMERCIALES.", fontCustom));
                list.Add(new ListItem("DESPUES DE 20 DIAS DE DESPACHADA LA MERCANCIA NO SE ACEPTAN DEVOLUCIONES.", fontCustom));
                list.Add(new ListItem("LA EMPRESA SE RESPONSABILIZA POR SUS PRODUCTOS DE ACUERDO A LAS NORMAS TÉCNICAS" +
                    " DE LOS MISMOS Y RESPONDERÁ SOLO POR LAS UNIDADES DEFECTUOSAS.", fontCustom));
                list.Add(new ListItem("LA EMPRESA NO SE RESPOSABILIZA POR EL MAL USO DEL PRODUCTO.", fontCustom));
                //list.Add(new ListItem("AGENTE RETENEDOR DE IMPUESTO A LAS VENTAS RÉGIMEN COMÚN.", fontCustom));
                list.Add(new ListItem("CODIGO DE ACTIVIDAD: 2229.", fontCustom));
                list.Add(new ListItem(((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Character03"]).ToUpper(),
                    fontCustom));

                celLeyendas.AddElement(list);

                tableTotales.AddCell(celLeyendas);

                //----------------------------------------------------------------------------------------------

                var celObs = new PdfPCell()
                {
                    FixedHeight = 50f,
                    Colspan = 3,
                };

                celObs.AddElement(new Paragraph($"OBSERVACIONES:" +
                    $"{((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceComment"]).ToUpper()}", fontTitle2Bold));

                tableTotales.AddCell(celObs);

                var tableFirmas = new PdfPTable(2) { WidthPercentage = 100, };

                tableFirmas.AddCell(new PdfPCell(new Phrase("ACEPTACIÓN DE LA FACTURA", fontTitle2Bold)) { Border = PdfPCell.NO_BORDER, });
                tableFirmas.AddCell(new PdfPCell(new Phrase("Nombre:________________________ ", fontTitle2Bold)) { Border = PdfPCell.NO_BORDER, });
                tableFirmas.AddCell(new PdfPCell(new Phrase("Cédula:________________________ ", fontTitle2Bold)) { Border = PdfPCell.NO_BORDER, });
                tableFirmas.AddCell(new PdfPCell(new Phrase("Firma:________________________ ", fontTitle2Bold)) { Border = PdfPCell.NO_BORDER, });

                tableTotales.AddCell(new PdfPCell(tableFirmas)
                {
                    Colspan = 3,
                    Padding = 5,
                    //Border = PdfPCell.NO_BORDER,
                });
                #endregion

                //PdfPTable tablePiePagEvent = new PdfPTable(2);

                tablePiePag.AddCell(new PdfPCell(tableTotales)
                {
                    //Border = PdfPCell.NO_BORDER,
                    //Padding=7,
                    VerticalAlignment = Element.ALIGN_BOTTOM,
                });

                //tableEncabezadoEvent.AddCell(new PdfPCell(tableEncabezado));
                //tablePiePagEvent.AddCell(new PdfPCell(tablePiePag));
                writer.PageEvent = new EventPageEncabezado(tableEncabezado);
                writer.PageEvent = new EventPagePiePag(tablePiePag, 220f);
                document.Open();

                //
                //writer.PageEvent = new EventPagePiePag(tablePiePagEvent, 30);
                //document.Open();

                //document.Add(tableEncabezado);
                //document.Add(divEspacio2);
                //document.Add(tableDatosCliente);
                document.Add(divEspacio2);
                document.Add(tableUnidades);
                //var cantidadFilasAdd = 18 - DsInvoiceAR.Tables["InvcDtl"].Rows.Count;
                //for (int i = 0; i < cantidadFilasAdd; i++)
                //    document.Add(divEspacio);

                //document.Add(tablePiePag);
                //document.Add(tableTotales);
                //document.Add(divEspacio2);
                //document.Add(tableContFirmas);

                /*PIE DE PAGINA*/
                //PdfContentByte pCb = writer.DirectContent;
                //PieDePagina(ref pCb);

                writer.Flush();
                document.Close();
                RutaPdf = NomArchivo;

                AddPageNumberPagTo($"{Ruta}{NomArchivo}", dimxTruher, dimYTruher + 10, fontTitle);

                return true;
            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.Message;
                return false;
            }
        }

        public bool FacturaNCPlasticosTruher(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {
            NomArchivo = string.Empty;
            string Ruta = HttpContext.Current.Server.MapPath("~/PDF/" + NIT + "/");
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            string RutaImg = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";
            NomArchivo = NombreInvoice + ".pdf";

            string RutaImgLogoTS = $@"http://{LocalIP}:8081/images/EMPRESAS/Empresa_{NIT}/Logo_{NIT}_ISO-TS.png";
            string RutaLogo_ISO_16949 = $@"http://{LocalIP}:8081/images/EMPRESAS/Empresa_{NIT}/Logo_{NIT}_ISO 9001.png";
            string RutaLogo_IQNET = $@"http://{LocalIP}:8081/images/EMPRESAS/Empresa_{NIT}/Logo_{NIT}_IQNET.png";

            try
            {
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.
                Document document = new Document(iTextSharp.text.PageSize.LETTER, 20f, 12f, 30f, 30f);

                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));

                // step 3: we open the document     
                document.Open();

                PdfDiv divEspacio = new PdfDiv()
                {
                    Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE,
                    Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK,
                    Height = 10,
                    Width = 130,

                };

                PdfDiv divEspacio2 = new PdfDiv()
                {
                    Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE,
                    Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK,
                    Height = 2,
                    Width = 130,
                };

                #region Fuentes
                //FUENTES----------------------------------------------------------------------------------------------------------
                iTextSharp.text.Font fontTitleBold = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7.57f, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA, 7.57f, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitleBold2 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 12f, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle2 = FontFactory.GetFont(FontFactory.HELVETICA, 8.5f, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle2Bold = FontFactory.GetFont(FontFactory.HELVETICA, 8.5f, iTextSharp.text.Font.BOLD);
                iTextSharp.text.Font fontCustom = FontFactory.GetFont(FontFactory.HELVETICA, 6f, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustomI = FontFactory.GetFont(FontFactory.HELVETICA_OBLIQUE, 6f, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustomBold = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 6, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontVals = FontFactory.GetFont(FontFactory.COURIER, 7.5f, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontValsx2 = FontFactory.GetFont(FontFactory.COURIER, 10f, iTextSharp.text.Font.NORMAL);
                //FUENTES---------------------------------------------------------------------------------------------------------
                #endregion

                #region ENCABEZADO

                PdfPTable tableEncabezado = new PdfPTable(new float[] { 1.5f, 0.8f, 1.2f })
                {
                    WidthPercentage = 100,
                };

                System.Drawing.Image logo = null;
                var requestLogo = WebRequest.Create(RutaImg);

                using (var responseLogo = requestLogo.GetResponse())
                using (var streamLogo = responseLogo.GetResponseStream())
                {
                    logo = Bitmap.FromStream(streamLogo);
                }

                PdfDiv divLogo = new PdfDiv()
                {
                    Float = iTextSharp.text.pdf.PdfDiv.FloatType.LEFT,
                    Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE,
                    BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID,
                    Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK,
                    Height = 85,
                    Width = 95,
                    //BackgroundColor = BaseColor.LIGHT_GRAY,
                };

                iTextSharp.text.Image ImgLogo = iTextSharp.text.Image.GetInstance(logo, BaseColor.WHITE);
                ImgLogo.Alignment = Element.ALIGN_CENTER;
                ImgLogo.ScaleAbsolute(80f, 80f);

                divLogo.BackgroundImage = ImgLogo;
                //----------------------------------------------------------------------------------------------
                var requestLogoISOTS = WebRequest.Create(RutaLogo_ISO_16949);

                using (var responseLogoISOTS = requestLogoISOTS.GetResponse())
                using (var streamLogoISOTS = responseLogoISOTS.GetResponseStream())
                {
                    logo = Bitmap.FromStream(streamLogoISOTS);
                }

                iTextSharp.text.Image ImgLogoISOTS = iTextSharp.text.Image.GetInstance(logo, BaseColor.WHITE);
                ImgLogoISOTS.Alignment = Element.ALIGN_CENTER;

                PdfDiv divLogoISOTS = new PdfDiv()
                {
                    Float = iTextSharp.text.pdf.PdfDiv.FloatType.LEFT,
                    Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE,
                    Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK,
                    Height = 70f,
                    Width = 50f,
                    //BackgroundColor = BaseColor.LIGHT_GRAY,
                    BackgroundImage = ImgLogoISOTS
                };

                //-------------------------------------------------------------------------------------
                var requestLogoISO = WebRequest.Create(RutaImgLogoTS);

                using (var responseLogoISO = requestLogoISO.GetResponse())
                using (var streamLogoISO = responseLogoISO.GetResponseStream())
                {
                    logo = Bitmap.FromStream(streamLogoISO);
                }

                iTextSharp.text.Image ImgLogoISO = iTextSharp.text.Image.GetInstance(logo, BaseColor.WHITE);
                ImgLogoISO.Alignment = Element.ALIGN_CENTER;

                PdfDiv divLogoISO = new PdfDiv()
                {
                    Float = iTextSharp.text.pdf.PdfDiv.FloatType.LEFT,
                    Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE,
                    Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK,
                    Height = 67f,
                    Width = 42f,
                    //BackgroundColor = BaseColor.LIGHT_GRAY,
                    BackgroundImage = ImgLogoISO
                };
                //celLogoISO.AddElement(new Paragraph("SC-1884-1", fontCustomBold) { Alignment = Element.ALIGN_CENTER });
                //----------------------------------------------------------------------------------------------
                var requestLogoIQNet = WebRequest.Create(RutaLogo_IQNET);

                using (var responseLogoIQNet = requestLogoIQNet.GetResponse())
                using (var streamLogoIQNet = responseLogoIQNet.GetResponseStream())
                {
                    logo = Bitmap.FromStream(streamLogoIQNet);
                }

                iTextSharp.text.Image ImgLogoIQNet = iTextSharp.text.Image.GetInstance(logo, BaseColor.WHITE);
                ImgLogoIQNet.Alignment = Element.ALIGN_BOTTOM;
                ImgLogoIQNet.ScaleAbsolute(55f, 55f);

                PdfDiv divLogoIQNet = new PdfDiv()
                {
                    Float = iTextSharp.text.pdf.PdfDiv.FloatType.LEFT,
                    Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE,
                    Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK,
                    Height = 55f,
                    Width = 40f,
                    //BackgroundColor = BaseColor.LIGHT_GRAY,
                    BackgroundImage = ImgLogoIQNet
                };

                //----------------------------------------------------------------------------------------------
                PdfPCell celLogo = new PdfPCell()
                {
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    VerticalAlignment = Element.ALIGN_BOTTOM,
                    //Colspan = 2,
                    Border = 0,
                    Padding = 0
                };

                celLogo.AddElement(divLogo);
                celLogo.AddElement(divLogoISOTS);
                celLogo.AddElement(divLogoISO);
                celLogo.AddElement(divLogoIQNet);
                tableEncabezado.AddCell(celLogo);
                //----------------------------------------------------------------------------------------------
                PdfPCell celTextInfoEmpresa = new PdfPCell()
                {
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    //Colspan = 2,
                    Border = 0,
                    Padding = 0
                };

                celTextInfoEmpresa.AddElement(new Paragraph($"PRODUCTORES NIT. " +
                    $"{Decimal.Parse((string)DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"]).ToString("N0")}",
                    fontTitleBold)
                {
                    Alignment = Element.ALIGN_CENTER,
                });

                celTextInfoEmpresa.AddElement(new Paragraph(
                    $"{(string)DsInvoiceAR.Tables["Company"].Rows[0]["Address1"]} " +
                    $"{(string)DsInvoiceAR.Tables["Company"].Rows[0]["City"]}", fontTitle)
                {
                    Alignment = Element.ALIGN_CENTER,
                });

                celTextInfoEmpresa.AddElement(new Paragraph(
                    $"TEL: {(string)DsInvoiceAR.Tables["Company"].Rows[0]["PhoneNum"]} " +
                    $"FAX: {(string)DsInvoiceAR.Tables["Company"].Rows[0]["FaxNum"]}", fontTitle)
                {
                    Alignment = Element.ALIGN_CENTER,
                });

                //celTextInfoEmpresa.AddElement(prgDepartamentoCiudad);
                celTextInfoEmpresa.AddElement(new Paragraph($"E-mail: {(string)DsInvoiceAR.Tables["Company"].Rows[0]["Character01"]}", fontTitle)
                {
                    Alignment = Element.ALIGN_CENTER,
                });

                tableEncabezado.AddCell(celTextInfoEmpresa);
                //-----------------------------------------------------------------------------------------------------------
                PdfPTable tableNoFactura = new PdfPTable(new float[] { 1, 3f })
                {
                    WidthPercentage = 100,
                };

                tableNoFactura.AddCell(new PdfPCell(new Phrase("NC VENTAS", fontValsx2))
                {
                    Colspan = 2,
                    Padding = 5,
                    HorizontalAlignment = Element.ALIGN_CENTER,
                });

                tableNoFactura.AddCell(new PdfPCell(new Phrase("OPERACION", fontCustom))
                {
                    BackgroundColor = BaseColor.LIGHT_GRAY,
                    HorizontalAlignment = Element.ALIGN_CENTER,
                });

                tableNoFactura.AddCell(new PdfPCell(/*new Phrase(" ",fontCustom)*/)
                {
                    BackgroundColor = BaseColor.LIGHT_GRAY,
                    Border = PdfPCell.TOP_BORDER | PdfPCell.LEFT_BORDER | PdfPCell.RIGHT_BORDER,
                });

                tableNoFactura.AddCell(new PdfPCell(/*new Phrase(" ",fontCustom)*/) { });
                tableNoFactura.AddCell(new PdfPCell(new Phrase($"No. {(string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"]}", fontTitleBold2))
                {
                    BackgroundColor = BaseColor.LIGHT_GRAY,
                    PaddingBottom = 10,
                    Border = PdfPCell.BOTTOM_BORDER | PdfPCell.LEFT_BORDER | PdfPCell.RIGHT_BORDER,
                });

                tableNoFactura.AddCell(new PdfPCell(/*new Phrase("", fontCustom)*/) { Colspan = 2, Border = PdfPCell.NO_BORDER, });

                tableEncabezado.AddCell(new PdfPCell(tableNoFactura) { Border = PdfPCell.NO_BORDER, });
                //----------------------------------------------------------------------------------------------------------------
                PdfPTable tableContInfoCliente = new PdfPTable(new float[] { 2, 1, 1, 1 });

                tableContInfoCliente.AddCell(new PdfPCell(new Phrase($"{(string)DsInvoiceAR.Tables["Customer"].Rows[0]["Name"]}", fontVals))
                {
                    Colspan = 4,
                    Border = PdfPCell.TOP_BORDER | PdfPCell.RIGHT_BORDER | PdfPCell.LEFT_BORDER,
                });

                tableContInfoCliente.AddCell(new PdfPCell(new Phrase($"{(string)DsInvoiceAR.Tables["Customer"].Rows[0]["Address1"]}", fontVals))
                {
                    Colspan = 4,
                    Border = PdfPCell.LEFT_BORDER | PdfPCell.RIGHT_BORDER,
                });

                tableContInfoCliente.AddCell(new PdfPCell(new Phrase($"{(string)DsInvoiceAR.Tables["Customer"].Rows[0]["City"]}", fontVals))
                {
                    //Colspan = 4,
                    Border = PdfPCell.LEFT_BORDER | PdfPCell.BOTTOM_BORDER,
                });

                tableContInfoCliente.AddCell(new PdfPCell(new Phrase($"TEL: {(string)DsInvoiceAR.Tables["Customer"].Rows[0]["PhoneNum"]}", fontVals))
                {
                    //Colspan = 4,
                    Border = PdfPCell.BOTTOM_BORDER,
                });

                string nitCliente = string.Empty;
                decimal convertNitCliente = 0;

                if (((string)DsInvoiceAR.Tables["COOneTime"].Rows[0]["IdentificationType"]) == "31")
                {
                    nitCliente = decimal.TryParse((string)DsInvoiceAR.Tables["Customer"].Rows[0]["ResaleID"],
                        out convertNitCliente) ? $"{convertNitCliente.ToString("N0")}-" +
                        $"{CalcularDigitoVerificacion((string)DsInvoiceAR.Tables["Customer"].Rows[0]["ResaleID"])}" :
                        (string)DsInvoiceAR.Tables["Customer"].Rows[0]["ResaleID"];
                }
                else
                {
                    nitCliente = (string)DsInvoiceAR.Tables["Customer"].Rows[0]["ResaleID"];
                }

                tableContInfoCliente.AddCell(new PdfPCell(new Phrase($"NIT: {nitCliente}", fontVals))
                {
                    //Colspan = 4,
                    Border = PdfPCell.BOTTOM_BORDER,
                });

                tableContInfoCliente.AddCell(new PdfPCell(new Phrase($"COD: {(string)DsInvoiceAR.Tables["Customer"].Rows[0]["ResaleID"]}", fontVals))
                {
                    //Colspan = 4,
                    Border = PdfPCell.BOTTOM_BORDER | PdfPCell.RIGHT_BORDER,
                });
                tableEncabezado.AddCell(new PdfPCell(tableContInfoCliente) { Padding = 0.58f, PaddingRight = 3, Colspan = 2, Border = PdfPCell.NO_BORDER, });

                PdfPTable tableContFechas = new PdfPTable(2);
                var tableContFecha1 = new PdfPTable(3);
                tableContFecha1.AddCell(new PdfPCell(new Phrase("FECHA EXPEDICION", fontCustomBold))
                {
                    Colspan = 3,
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    BackgroundColor = BaseColor.LIGHT_GRAY,
                });
                //-------TEXTOS FECHAS
                tableContFecha1.AddCell(new PdfPCell(new Phrase("DIA", fontCustomBold))
                {
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    BackgroundColor = BaseColor.LIGHT_GRAY,
                });

                tableContFecha1.AddCell(new PdfPCell(new Phrase("MES", fontCustomBold))
                {
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    BackgroundColor = BaseColor.LIGHT_GRAY,
                });

                tableContFecha1.AddCell(new PdfPCell(new Phrase("AÑO", fontCustomBold))
                {
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    BackgroundColor = BaseColor.LIGHT_GRAY,
                });

                //-------VALORES FECHAS
                tableContFecha1.AddCell(new PdfPCell(new Phrase($"{DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).Day}", fontVals))
                {
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    VerticalAlignment = Element.ALIGN_CENTER,
                    //BackgroundColor = BaseColor.LIGHT_GRAY,
                });

                tableContFecha1.AddCell(new PdfPCell(new Phrase($"{DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).Month}", fontVals))
                {
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    VerticalAlignment = Element.ALIGN_CENTER,
                    //BackgroundColor = BaseColor.LIGHT_GRAY,
                });

                tableContFecha1.AddCell(new PdfPCell(new Phrase($"{DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).Year}", fontVals))
                {
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    VerticalAlignment = Element.ALIGN_CENTER,
                    //BackgroundColor = BaseColor.LIGHT_GRAY,
                });

                tableContFechas.AddCell(new PdfPCell(tableContFecha1)
                {
                    PaddingRight = 1.5f,
                    Border = PdfPCell.NO_BORDER,
                });

                var tableContFecha2 = new PdfPTable(3);
                tableContFecha2.AddCell(new PdfPCell(new Phrase("FECHA VENCIMIENTO", fontCustomBold))
                {
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    BackgroundColor = BaseColor.LIGHT_GRAY,
                    Colspan = 3,
                });

                tableContFecha2.AddCell(new PdfPCell(new Phrase("DIA", fontCustomBold))
                {
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    BackgroundColor = BaseColor.LIGHT_GRAY,
                });

                tableContFecha2.AddCell(new PdfPCell(new Phrase("MES", fontCustomBold))
                {
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    BackgroundColor = BaseColor.LIGHT_GRAY,
                });

                tableContFecha2.AddCell(new PdfPCell(new Phrase("AÑO", fontCustomBold))
                {
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    BackgroundColor = BaseColor.LIGHT_GRAY,
                });

                //-------VALORES FECHAS VENCIMIENTO
                tableContFecha2.AddCell(new PdfPCell(new Phrase($"{DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"]).Day}", fontVals))
                {
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    VerticalAlignment = Element.ALIGN_CENTER,
                    //BackgroundColor = BaseColor.LIGHT_GRAY,
                });

                tableContFecha2.AddCell(new PdfPCell(new Phrase($"{DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"]).Month}", fontVals))
                {
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    VerticalAlignment = Element.ALIGN_CENTER,
                    //BackgroundColor = BaseColor.LIGHT_GRAY,
                });

                tableContFecha2.AddCell(new PdfPCell(new Phrase($"{DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"]).Year}", fontVals))
                {
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    VerticalAlignment = Element.ALIGN_CENTER,
                    //BackgroundColor = BaseColor.LIGHT_GRAY,
                });
                tableContFechas.AddCell(new PdfPCell(tableContFecha2)
                {
                    PaddingLeft = 1.5f,
                    Border = PdfPCell.NO_BORDER,
                });

                tableEncabezado.AddCell(new PdfPCell(tableContFechas) { Padding = 0.58f,/*PaddingLeft=5,*/ Border = PdfPCell.NO_BORDER, });

                #endregion

                #region INFO FACTURA

                PdfPTable tableInfoFactura = new PdfPTable(new float[] { 1, 1.2f, 2, 2.2f, 1, 1, 1 })
                {
                    WidthPercentage = 100,
                };

                tableInfoFactura.AddCell(new PdfPCell(new Phrase("DOCTO REF", fontVals))
                {
                    BackgroundColor = BaseColor.LIGHT_GRAY,
                    Border = PdfPCell.LEFT_BORDER | PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER,
                });

                tableInfoFactura.AddCell(new PdfPCell(new Phrase("ORDEN DE COMPRA", fontVals))
                {
                    BackgroundColor = BaseColor.LIGHT_GRAY,
                    Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER,
                });

                tableInfoFactura.AddCell(new PdfPCell(new Phrase("VENDEDOR", fontVals))
                {
                    BackgroundColor = BaseColor.LIGHT_GRAY,
                    Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER,
                    HorizontalAlignment = Element.ALIGN_CENTER,
                });

                tableInfoFactura.AddCell(new PdfPCell(new Phrase("TRANSPORTADOR", fontVals))
                {
                    BackgroundColor = BaseColor.LIGHT_GRAY,
                    Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER,
                });

                tableInfoFactura.AddCell(new PdfPCell(new Phrase("PLAZO", fontVals))
                {
                    BackgroundColor = BaseColor.LIGHT_GRAY,
                    Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER,
                });

                tableInfoFactura.AddCell(new PdfPCell(new Phrase("CCOSTO", fontVals))
                {
                    BackgroundColor = BaseColor.LIGHT_GRAY,
                    Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER,
                });

                tableInfoFactura.AddCell(new PdfPCell(new Phrase("PAG", fontVals))
                {
                    BackgroundColor = BaseColor.LIGHT_GRAY,
                    Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER | PdfPCell.RIGHT_BORDER,
                    HorizontalAlignment = Element.ALIGN_CENTER,
                });
                //----------VALORES DE INFORMACION DE LA FACTURA--------------------------------

                tableInfoFactura.AddCell(new PdfPCell(new Phrase((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceRef"], fontVals))
                {
                    //BackgroundColor = BaseColor.LIGHT_GRAY,
                    Border = PdfPCell.LEFT_BORDER | PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER,
                });

                tableInfoFactura.AddCell(new PdfPCell(new Phrase((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["PONum"], fontVals))
                {
                    //BackgroundColor = BaseColor.LIGHT_GRAY,
                    Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER,
                });

                tableInfoFactura.AddCell(new PdfPCell(new Phrase((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceRef"], fontVals))
                {
                    //BackgroundColor = BaseColor.LIGHT_GRAY,
                    Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER,
                    HorizontalAlignment = Element.ALIGN_CENTER,
                });

                tableInfoFactura.AddCell(new PdfPCell(new Phrase((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar08"], fontVals))
                {
                    //BackgroundColor = BaseColor.LIGHT_GRAY,
                    Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER,
                });

                tableInfoFactura.AddCell(new PdfPCell(new Phrase((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["TermsCodeDescription"], fontVals))
                {
                    //BackgroundColor = BaseColor.LIGHT_GRAY,
                    Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER,
                });

                tableInfoFactura.AddCell(new PdfPCell(new Phrase((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar05"], fontVals))
                {
                    //BackgroundColor = BaseColor.LIGHT_GRAY,
                    Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER,
                });

                //Espacio para el numero de pagina que se grega despues con el metodo que las agrega
                tableInfoFactura.AddCell(new PdfPCell(new Phrase(" ", fontVals))
                {
                    //BackgroundColor = BaseColor.LIGHT_GRAY,
                    Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER | PdfPCell.RIGHT_BORDER,
                    HorizontalAlignment = Element.ALIGN_CENTER,
                });

                tableInfoFactura.AddCell(new PdfPCell(new Phrase(" ", FontFactory.GetFont(FontFactory.HELVETICA, 3, iTextSharp.text.Font.NORMAL)))
                {
                    //BackgroundColor = BaseColor.LIGHT_GRAY,
                    Border = PdfPCell.NO_BORDER,
                    Padding = 1,
                    Colspan = 7,
                });

                tableInfoFactura.AddCell(new PdfPCell(new Phrase((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceComment"], fontVals))
                {
                    //BackgroundColor = BaseColor.LIGHT_GRAY,
                    Border = PdfPCell.BOX,
                    MinimumHeight = 55,
                    Colspan = 7,
                });
                #endregion

                #region UNIDADES

                PdfPTable tableUnidades = new PdfPTable(new float[] { 1, 2.4f, 0.5f, 0.8f, 0.6f, 1, 0.5f, 0.5f, 1 });
                tableUnidades.WidthPercentage = 100;

                tableUnidades.AddCell(new PdfPCell()
                {
                    Border = PdfPCell.NO_BORDER,
                    VerticalAlignment = Element.ALIGN_BOTTOM,
                    Phrase = new Phrase($"CUFE: {CUFE}", fontTitle2),
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    Colspan = 5,
                    Padding = 3,
                });

                tableUnidades.AddCell(new PdfPCell()
                {
                    Border = PdfPCell.NO_BORDER,
                    VerticalAlignment = Element.ALIGN_BOTTOM,
                    Phrase = new Phrase("Representación gráfica factura electrónica", fontTitle2),
                    HorizontalAlignment = Element.ALIGN_RIGHT,
                    Colspan = 4,
                    Padding = 3,
                });

                tableUnidades.AddCell(new PdfPCell()
                {
                    Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER | PdfPCell.LEFT_BORDER,
                    BackgroundColor = BaseColor.LIGHT_GRAY,
                    VerticalAlignment = Element.ALIGN_BOTTOM,
                    Phrase = new Phrase("REFERENCIA", fontVals),
                    HorizontalAlignment = Element.ALIGN_LEFT
                });

                tableUnidades.AddCell(new PdfPCell()
                {
                    Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER,
                    BackgroundColor = BaseColor.LIGHT_GRAY,
                    VerticalAlignment = Element.ALIGN_BOTTOM,
                    Phrase = new Phrase("DESCRIPCIÓN", fontVals),
                    HorizontalAlignment = Element.ALIGN_CENTER,
                });

                tableUnidades.AddCell(new PdfPCell()
                {
                    Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER,
                    BackgroundColor = BaseColor.LIGHT_GRAY,
                    VerticalAlignment = Element.ALIGN_BOTTOM,
                    Phrase = new Phrase("UBICA", fontVals),
                    HorizontalAlignment = Element.ALIGN_CENTER
                });

                tableUnidades.AddCell(new PdfPCell()
                {
                    Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER,
                    BackgroundColor = BaseColor.LIGHT_GRAY,
                    VerticalAlignment = Element.ALIGN_BOTTOM,
                    Phrase = new Phrase("UNIDAD", fontVals),
                    HorizontalAlignment = Element.ALIGN_CENTER
                });

                tableUnidades.AddCell(new PdfPCell()
                {
                    Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER,
                    BackgroundColor = BaseColor.LIGHT_GRAY,
                    VerticalAlignment = Element.ALIGN_BOTTOM,
                    Phrase = new Phrase("CANTIDAD", fontVals),
                    HorizontalAlignment = Element.ALIGN_CENTER
                });

                tableUnidades.AddCell(new PdfPCell()
                {
                    Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER,
                    BackgroundColor = BaseColor.LIGHT_GRAY,
                    VerticalAlignment = Element.ALIGN_BOTTOM,
                    Phrase = new Phrase("PRECIO", fontVals),
                    HorizontalAlignment = Element.ALIGN_CENTER
                });

                tableUnidades.AddCell(new PdfPCell()
                {
                    Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER,
                    BackgroundColor = BaseColor.LIGHT_GRAY,
                    VerticalAlignment = Element.ALIGN_BOTTOM,
                    Phrase = new Phrase("DSCTO", fontVals),
                    HorizontalAlignment = Element.ALIGN_CENTER
                });

                tableUnidades.AddCell(new PdfPCell()
                {
                    Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER,
                    BackgroundColor = BaseColor.LIGHT_GRAY,
                    VerticalAlignment = Element.ALIGN_BOTTOM,
                    Phrase = new Phrase("IVA", fontVals),
                    HorizontalAlignment = Element.ALIGN_CENTER
                });

                tableUnidades.AddCell(new PdfPCell()
                {
                    Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER | PdfPCell.RIGHT_BORDER,
                    BackgroundColor = BaseColor.LIGHT_GRAY,
                    VerticalAlignment = Element.ALIGN_BOTTOM,
                    Phrase = new Phrase("VALOR", fontVals),
                    HorizontalAlignment = Element.ALIGN_CENTER
                });
                //--------------------------------------------------------------------------------------
                decimal totalCantidad = 0;
                foreach (DataRow InvoiceLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
                {
                    totalCantidad += decimal.Parse((string)InvoiceLine["SellingShipQty"]);
                    if (!AddUnidadesNCPlasticosTruher(InvoiceLine, ref tableUnidades, fontVals, DsInvoiceAR))
                        return false;
                }

                var cantidadFilasAdd = 15 - DsInvoiceAR.Tables["InvcDtl"].Rows.Count;
                for (int i = 0; i <= cantidadFilasAdd; i++)
                {
                    tableUnidades.AddCell(new PdfPCell(new Phrase(" "))
                    {
                        Colspan = 9,
                        //BackgroundColor=BaseColor.LIGHT_GRAY,
                        Border = PdfPCell.LEFT_BORDER | PdfPCell.RIGHT_BORDER,
                    });
                }

                tableUnidades.AddCell(new PdfPCell(new Phrase($"Valor Gravado:   " +
                            $"{decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubTotal"]).ToString("N")}", fontVals))
                {
                    Colspan = 4,
                    Border = PdfPCell.LEFT_BORDER /*| PdfPCell.RIGHT_BORDER*/| PdfPCell.BOTTOM_BORDER,
                });

                tableUnidades.AddCell(new PdfPCell(new Phrase($"Total Cantidad:   " +
                    $"{totalCantidad.ToString("N")}", fontVals))
                {
                    Colspan = 5,
                    Border = /*PdfPCell.LEFT_BORDER |*/PdfPCell.RIGHT_BORDER | PdfPCell.BOTTOM_BORDER,
                });

                #endregion

                #region Totales, QR y Firmas

                PdfPTable tableTotales = new PdfPTable(new float[] { 3.3f, 0.8f, 1.7f }) { WidthPercentage = 100, };

                PdfPTable tableFirma_QR = new PdfPTable(new float[] { 1, 1.58f }) { WidthPercentage = 100, };

                tableFirma_QR.AddCell(new PdfPCell(new Phrase(" "))
                {
                    BackgroundColor = BaseColor.LIGHT_GRAY,
                    Colspan = 2,
                });

                var celInfoFirmas = new PdfPCell()
                {
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    VerticalAlignment = Element.ALIGN_BOTTOM,
                };

                celInfoFirmas.AddElement(new Paragraph("POR", fontVals)
                {
                    Alignment = Element.ALIGN_LEFT,
                });

                celInfoFirmas.AddElement(new Paragraph("__________________________\nFIRMA Y SELLO", fontVals)
                {
                    Alignment = Element.ALIGN_CENTER,
                });

                tableFirma_QR.AddCell(celInfoFirmas);

                PdfDiv divQR = new PdfDiv()
                {
                    Float = iTextSharp.text.pdf.PdfDiv.FloatType.RIGHT,
                    Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE,
                    BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID,
                    Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK,
                    Height = 90,
                    Width = 90,
                    //BackgroundColor=BaseColor.LIGHT_GRAY,
                };

                iTextSharp.text.Image ImgQR = iTextSharp.text.Image.GetInstance(QRInvoice, BaseColor.WHITE);
                ImgQR.Alignment = Element.ALIGN_RIGHT;
                divQR.BackgroundImage = ImgQR;

                PdfPCell celQR = new PdfPCell()
                {
                    Colspan = 3,
                    Border = 0,
                    Padding = 3
                };
                //celQR.AddElement(new Paragraph("CUFE {***}", fontTitleBold));
                celQR.AddElement(divQR);
                tableFirma_QR.AddCell(celQR);

                tableTotales.AddCell(new PdfPCell(tableFirma_QR));
                //-------------------------------------------------------------------------------------------------------

                tableTotales.AddCell(new PdfPCell(new Phrase("CUENTA", fontTitle2Bold))
                {
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    VerticalAlignment = Element.ALIGN_TOP,
                });
                //------------------------------------------------------------------------------------------------------
                PdfPTable tableToCelTotales = new PdfPTable(2);

                Paragraph prgTextSubTotal = new Paragraph("SUBTOTAL", fontVals);
                Paragraph prgValSubTotal = new Paragraph(
                    decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubTotal"]).ToString("N"),
                    fontVals);

                tableToCelTotales.AddCell(new PdfPCell(prgTextSubTotal)
                {
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    Border = PdfPCell.NO_BORDER,
                    PaddingBottom = 7,
                });

                tableToCelTotales.AddCell(new PdfPCell(prgValSubTotal)
                {
                    HorizontalAlignment = Element.ALIGN_RIGHT,
                    Border = PdfPCell.NO_BORDER,
                    PaddingBottom = 7,
                });

                tableToCelTotales.AddCell(new PdfPCell(new Phrase("IVA", fontVals))
                {
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    Border = PdfPCell.NO_BORDER,
                    PaddingBottom = 7,
                });

                tableToCelTotales.AddCell(new PdfPCell(new Phrase(GetValImpuestoByID("01", DsInvoiceAR).ToString("N"), fontVals))
                {
                    HorizontalAlignment = Element.ALIGN_RIGHT,
                    Border = PdfPCell.NO_BORDER,
                    PaddingBottom = 7,
                });

                tableToCelTotales.AddCell(new PdfPCell(new Phrase("NETO", fontVals))
                {
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    Border = PdfPCell.NO_BORDER,
                    PaddingBottom = 7,
                });

                tableToCelTotales.AddCell(new PdfPCell(new Phrase(
                    decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"]).ToString("N"),
                    fontVals))
                {
                    HorizontalAlignment = Element.ALIGN_RIGHT,
                    Border = PdfPCell.NO_BORDER,
                    PaddingBottom = 7,
                });

                tableTotales.AddCell(new PdfPCell(tableToCelTotales));

                #endregion

                document.Add(tableEncabezado);
                document.Add(divEspacio);
                document.Add(tableInfoFactura);
                document.Add(divEspacio2);
                document.Add(tableUnidades);

                document.Add(tableTotales);
                //document.Add(divEspacio2);
                //document.Add(tableContFirmas);

                /*PIE DE PAGINA*/
                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();
                AddPageNumber($"{Ruta}{NomArchivo}", 568f, 625f, fontVals);

                RutaPdf = NomArchivo;
                return true;
            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.Message;
                return false;
            }
        }

        private bool AddUnidadesNCPlasticosTruher(DataRow dataLine, ref PdfPTable table, iTextSharp.text.Font fontCustom, DataSet dataSet)
        {
            try
            {
                table.AddCell(new PdfPCell()
                {
                    Border = PdfPCell.LEFT_BORDER,
                    VerticalAlignment = Element.ALIGN_BOTTOM,
                    Phrase = new Phrase((string)dataLine["PartNum"], fontCustom),
                    HorizontalAlignment = Element.ALIGN_LEFT
                });

                table.AddCell(new PdfPCell()
                {
                    Border = PdfPCell.NO_BORDER,
                    VerticalAlignment = Element.ALIGN_BOTTOM,
                    Phrase = new Phrase((string)dataLine["LineDesc"], fontCustom),
                    HorizontalAlignment = Element.ALIGN_CENTER,
                });

                table.AddCell(new PdfPCell()
                {
                    Border = PdfPCell.NO_BORDER,
                    VerticalAlignment = Element.ALIGN_BOTTOM,
                    Phrase = new Phrase((string)dataLine["WarehouseCode"], fontCustom),
                    HorizontalAlignment = Element.ALIGN_CENTER
                });

                table.AddCell(new PdfPCell()
                {
                    Border = PdfPCell.NO_BORDER,
                    VerticalAlignment = Element.ALIGN_BOTTOM,
                    Phrase = new Phrase((string)dataLine["SalesUM"], fontCustom),
                    HorizontalAlignment = Element.ALIGN_CENTER
                });

                table.AddCell(new PdfPCell()
                {
                    Border = PdfPCell.NO_BORDER,
                    VerticalAlignment = Element.ALIGN_BOTTOM,
                    Phrase = new Phrase((string)dataLine["SellingShipQty"], fontCustom),
                    HorizontalAlignment = Element.ALIGN_CENTER
                });

                table.AddCell(new PdfPCell()
                {
                    Border = PdfPCell.NO_BORDER,
                    VerticalAlignment = Element.ALIGN_BOTTOM,
                    Phrase = new Phrase(decimal.Parse((string)dataLine["DocUnitPrice"]).ToString("N2"), fontCustom),
                    HorizontalAlignment = Element.ALIGN_CENTER
                });

                table.AddCell(new PdfPCell()
                {
                    Border = PdfPCell.NO_BORDER,
                    VerticalAlignment = Element.ALIGN_BOTTOM,
                    Phrase = new Phrase(decimal.Parse((string)dataLine["DspDocLessDiscount"]).ToString("N2"), fontCustom),
                    HorizontalAlignment = Element.ALIGN_CENTER
                });

                table.AddCell(new PdfPCell()
                {
                    Border = PdfPCell.NO_BORDER,
                    VerticalAlignment = Element.ALIGN_BOTTOM,
                    Phrase = new Phrase(GetPercentIdImpDIAN((string)dataLine["InvoiceNum"], (string)dataLine["InvoiceLine"], dataSet.Tables["InvcTax"]), fontCustom),
                    HorizontalAlignment = Element.ALIGN_CENTER
                });

                table.AddCell(new PdfPCell()
                {
                    Border = PdfPCell.RIGHT_BORDER,
                    VerticalAlignment = Element.ALIGN_BOTTOM,
                    Phrase = new Phrase(decimal.Parse((string)dataLine["DspDocExtPrice"]).ToString("N2"), fontCustom),
                    HorizontalAlignment = Element.ALIGN_RIGHT,
                });

                return true;
            }
            catch (Exception ex)
            {
                strError = "Error addLineInvoice: " + ex.Message;
                return false;
            }
        }

        private bool AddUnidadesPlasticosTruher(DataRow dataLine, ref PdfPTable table, iTextSharp.text.Font fontCustom, DataSet dataSet)
        {
            try
            {
                table.AddCell(new PdfPCell()
                {
                    Border = PdfPCell.NO_BORDER,
                    VerticalAlignment = Element.ALIGN_CENTER,
                    Phrase = new Phrase((string)dataLine["PartNum"], fontCustom),
                    HorizontalAlignment = Element.ALIGN_LEFT
                });

                table.AddCell(new PdfPCell()
                {
                    Border = PdfPCell.NO_BORDER,
                    VerticalAlignment = Element.ALIGN_CENTER,
                    Phrase = new Phrase($"{(string)dataLine["LineDesc"]}\n" +
                    $"{(string)dataLine["PartNumPartDescription"]}", fontCustom),
                    HorizontalAlignment = Element.ALIGN_CENTER,
                });

                table.AddCell(new PdfPCell()
                {
                    Border = PdfPCell.NO_BORDER,
                    VerticalAlignment = Element.ALIGN_CENTER,
                    Phrase = new Phrase((string)(string)dataLine["SellingShipQty"], fontCustom),
                    HorizontalAlignment = Element.ALIGN_CENTER
                });

                table.AddCell(new PdfPCell()
                {
                    Border = PdfPCell.NO_BORDER,
                    VerticalAlignment = Element.ALIGN_CENTER,
                    Phrase = new Phrase(decimal.Parse((string)dataLine["DocUnitPrice"]).ToString("N2"), fontCustom),
                    HorizontalAlignment = Element.ALIGN_CENTER
                });

                table.AddCell(new PdfPCell()
                {
                    Border = PdfPCell.NO_BORDER,
                    VerticalAlignment = Element.ALIGN_CENTER,
                    Phrase = new Phrase(decimal.Parse((string)dataLine["DspDocExtPrice"]).ToString("N2"), fontCustom),
                    HorizontalAlignment = Element.ALIGN_CENTER
                });

                return true;
            }
            catch (Exception ex)
            {
                strError = "Error addLineInvoice: " + ex.Message;
                return false;
            }
        }
        #endregion

    }
}
