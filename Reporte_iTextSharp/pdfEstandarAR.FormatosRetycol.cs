﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
//Referenciar y usar.
using System.Data;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Web;
using System.Windows.Forms;

namespace RulesServicesDIAN2.Adquiriente
{
    public partial class pdfEstandarAR
    {
        #region Formatos Facturacion RETYCOL
        public bool FacturaNacionalRetycol(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {
            bool esLocal = Helpers.Compartido.EsLocal;
            string Ruta = string.Empty;
            string RutaImg = string.Empty;
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            if (esLocal)
            {
                Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
                RutaImg = $@"{AppDomain.CurrentDomain.BaseDirectory}\Logo_Retycol.png";
            }
            else
            {
                Ruta = HttpContext.Current.Server.MapPath("~/PDF/" + NIT + "/");
                RutaImg = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";
            }
            NomArchivo = string.Empty;
            NomArchivo = NombreInvoice + ".pdf";

            try
            {
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.
                Document document = new Document(iTextSharp.text.PageSize.LETTER);
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;

                // step 3: we open the document     
                document.Open();

                PdfDiv divEspacio = new PdfDiv();
                divEspacio.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio.Height = 10;
                divEspacio.Width = 130;

                PdfDiv divEspacio2 = new PdfDiv();
                divEspacio2.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio2.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio2.Height = 2;
                divEspacio2.Width = 130;

                iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle2 = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustom = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);

                iTextSharp.text.pdf.PdfPCell celEspacio2 = new iTextSharp.text.pdf.PdfPCell(new Phrase(" ", Helpers.Fuentes.fontTitleFactura));
                celEspacio2.Border = 0;

                document.Add(Helpers.Retycol.Encabezado(DsInvoiceAR, QRInvoice, InvoiceType, CUFE, RutaImg));

                //document.Add(divAcercade);
                //document.Add(divQR);
                //document.Add(divNumeroFactura);
                document.Add(divEspacio);
                document.Add(Helpers.Retycol.DatosCliente(DsInvoiceAR));
                document.Add(divEspacio);
                document.Add(Helpers.Retycol.Detalles(DsInvoiceAR));
                document.Add(Helpers.Retycol.Detalles2(DsInvoiceAR));
                document.Add(divEspacio2);
                document.Add(Helpers.Retycol.TituloUnidades(DsInvoiceAR));
                document.Add(Helpers.Retycol.Unidades(DsInvoiceAR));
                document.Add(divEspacio2);
                document.Add(divEspacio2);
                document.Add(Helpers.Retycol.Observaciones(DsInvoiceAR));
                document.Add(divEspacio2);
                document.Add(divEspacio2);
                document.Add(Helpers.Retycol.Resolucion(DsInvoiceAR));
                document.Add(divEspacio2);
                document.Add(divEspacio2);
                document.Add(Helpers.Retycol.Firmas(DsInvoiceAR));

                /*PIE DE PAGINA*/
                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();
                RutaPdf = NomArchivo;
                return true;
            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.Message;
                return false;
            }
        }

        public partial class Helpers
        {
            public class Metodos
            {
                public static decimal GetValImpuestoByID(string IdImPDIAN, DataSet DsInvcHed)
                {
                    decimal ValImp = 0;

                    try
                    {
                        //Trasa += "Valor Impuesto ID" + IdImPDIAN + "\n";
                        //Recorremos los Impuestos y acumulamos por IdImpDIAN.                
                        foreach (DataRow RowTax in DsInvcHed.Tables["InvcTax"].Rows)
                        {
                            //Buscamos el IdImpDIAN por el RateCode del InvcTax.      
                            DataRow[] RowImpDIAN = DsInvcHed.Tables["SalesTRC"].Select("RateCode='" + RowTax["RateCode"].ToString() + "'");
                            if (RowImpDIAN != null && RowImpDIAN.GetLength(0) > 0 && RowImpDIAN[0]["IdImpDIAN_c"].ToString() == IdImPDIAN)
                            {
                                //Trasa += "RateCode =" + RowTax["RateCode"].ToString() + "IdImpDIAN =" + RowImpDIAN[0]["IdImpDIAN_c"].ToString() + " Valor" + Convert.ToDecimal(RowTax["DocTaxAmt"]) + "\n";                        
                                ValImp += Convert.ToDecimal(RowTax["DocTaxAmt"]);
                            }
                        }

                        return ValImp;
                    }
                    catch (Exception)
                    {
                        return 0;
                    }
                }
                public static string Nroenletras(string num)
                {
                    string res, dec = "";
                    Int64 entero;
                    int decimales;
                    double nro;

                    try
                    {
                        nro = Convert.ToDouble(num);
                    }
                    catch
                    {
                        return "";
                    }

                    entero = Convert.ToInt64(Math.Truncate(nro));
                    decimales = Convert.ToInt32(Math.Round((nro - entero) * 100, 2));
                    if (decimales > 0)
                    {
                        dec = " CON " + decimales.ToString();
                    }
                    //else
                    //{
                    //    dec = " CON 00";
                    //}

                    res = toText(Convert.ToDouble(entero)) + dec + " PESOS";
                    return res;
                }
                public static string toText(double value)
                {
                    string Num2Text = "";
                    value = Math.Truncate(value);
                    if (value == 0) Num2Text = "CERO";
                    else if (value == 1) Num2Text = "UNO";
                    else if (value == 2) Num2Text = "DOS";
                    else if (value == 3) Num2Text = "TRES";
                    else if (value == 4) Num2Text = "CUATRO";
                    else if (value == 5) Num2Text = "CINCO";
                    else if (value == 6) Num2Text = "SEIS";
                    else if (value == 7) Num2Text = "SIETE";
                    else if (value == 8) Num2Text = "OCHO";
                    else if (value == 9) Num2Text = "NUEVE";
                    else if (value == 10) Num2Text = "DIEZ";
                    else if (value == 11) Num2Text = "ONCE";
                    else if (value == 12) Num2Text = "DOCE";
                    else if (value == 13) Num2Text = "TRECE";
                    else if (value == 14) Num2Text = "CATORCE";
                    else if (value == 15) Num2Text = "QUINCE";
                    else if (value < 20) Num2Text = "DIECI" + toText(value - 10);
                    else if (value == 20) Num2Text = "VEINTE";
                    else if (value < 30) Num2Text = "VEINTI" + toText(value - 20);
                    else if (value == 30) Num2Text = "TREINTA";
                    else if (value == 40) Num2Text = "CUARENTA";
                    else if (value == 50) Num2Text = "CINCUENTA";
                    else if (value == 60) Num2Text = "SESENTA";
                    else if (value == 70) Num2Text = "SETENTA";
                    else if (value == 80) Num2Text = "OCHENTA";
                    else if (value == 90) Num2Text = "NOVENTA";
                    else if (value < 100) Num2Text = toText(Math.Truncate(value / 10) * 10) + " Y " + toText(value % 10);
                    else if (value == 100) Num2Text = "CIEN";
                    else if (value < 200) Num2Text = "CIENTO " + toText(value - 100);
                    else if ((value == 200) || (value == 300) || (value == 400) || (value == 600) || (value == 800)) Num2Text = toText(Math.Truncate(value / 100)) + "CIENTOS";
                    else if (value == 500) Num2Text = "QUINIENTOS";
                    else if (value == 700) Num2Text = "SETECIENTOS";
                    else if (value == 900) Num2Text = "NOVECIENTOS";
                    else if (value < 1000) Num2Text = toText(Math.Truncate(value / 100) * 100) + " " + toText(value % 100);
                    else if (value == 1000) Num2Text = "MIL";
                    else if (value < 2000) Num2Text = "MIL " + toText(value % 1000);
                    else if (value < 1000000)
                    {
                        Num2Text = toText(Math.Truncate(value / 1000)) + " MIL";
                        if ((value % 1000) > 0) Num2Text = Num2Text + " " + toText(value % 1000);
                    }

                    else if (value == 1000000) Num2Text = "UN MILLON";
                    else if (value < 2000000) Num2Text = "UN MILLON " + toText(value % 1000000);
                    else if (value < 1000000000000)
                    {
                        Num2Text = toText(Math.Truncate(value / 1000000)) + " MILLONES ";
                        double truncate = Math.Truncate(value / 1000000) * 1000000;
                        if ((value - truncate) > 0) Num2Text = Num2Text + " " + toText(value - truncate);
                    }

                    else if (value == 1000000000000) Num2Text = "UN BILLON";
                    else if (value < 2000000000000) Num2Text = "UN BILLON " + toText(value - Math.Truncate(value / 1000000000000) * 1000000000000);

                    else
                    {
                        Num2Text = toText(Math.Truncate(value / 1000000000000)) + " BILLONES";
                        if ((value - Math.Truncate(value / 1000000000000)) > 0) Num2Text = Num2Text + " " + toText(value - Math.Truncate(value / 1000000000000));
                    }
                    return Num2Text;
                }
            }
            public class Retycol
            {
                public static PdfPCell celEspacio2
                {
                    get
                    {
                        PdfPCell celEspacio = new PdfPCell(new Phrase(" ", Helpers.Fuentes.fontTitleFactura));
                        celEspacio.Border = 0;
                        return celEspacio;
                    }
                }
                public static PdfPCell celEspacio3
                {
                    get
                    {
                        iTextSharp.text.Font fontEspacio = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 0.3F, iTextSharp.text.Font.NORMAL);
                        PdfPCell celEspacio = new PdfPCell(new Phrase("  ", fontEspacio));
                        celEspacio.Border = 0;
                        return celEspacio;
                    }
                }
                public static float[] DimensionUnidades
                {
                    get
                    {
                        float[] _dimensionUnidades = new float[6];
                        _dimensionUnidades[0] = 1.0F;//
                        _dimensionUnidades[1] = 3.0F;//
                        _dimensionUnidades[2] = 1.0F;//
                        _dimensionUnidades[3] = 0.5F;//
                        _dimensionUnidades[4] = 1.0F;//
                        _dimensionUnidades[5] = 1.5F;//
                        return _dimensionUnidades;
                    }
                }
                public static void DetallesAddCeldas(ref PdfPTable table, int colspan, string value, int alineacion, float BorderWidthBottom, float BorderWidthLeft, float BorderWidthRight, float BorderWidthTop)
                {
                    iTextSharp.text.pdf.PdfPCell celda = new iTextSharp.text.pdf.PdfPCell(new Phrase(value, Helpers.Fuentes.fontCustom));
                    celda.Colspan = colspan;
                    celda.Padding = 3;
                    //celVendedor.Border = 0;
                    //celVendedor.BorderColorBottom = BaseColor.WHITE;
                    celda.BorderWidthBottom = BorderWidthBottom;
                    celda.BorderWidthLeft = BorderWidthLeft;
                    celda.BorderWidthRight = BorderWidthRight;
                    celda.BorderWidthTop = BorderWidthTop;
                    celda.HorizontalAlignment = alineacion;
                    celda.VerticalAlignment = Element.ALIGN_TOP;
                    celda.MinimumHeight = 10f;
                    table.AddCell(celda);
                }


                public static PdfPTable Tabla1(string RutaImg)
                {
                    PdfPTable tabla = new PdfPTable(2);
                    tabla.WidthPercentage = 100;

                    PdfPCell celda = new PdfPCell();
                    System.Drawing.Image logo = null;
                    var requestLogo = WebRequest.Create(RutaImg);

                    using (var responseLogo = requestLogo.GetResponse())
                    using (var streamLogo = responseLogo.GetResponseStream())
                    {
                        logo = Bitmap.FromStream(streamLogo);
                    }

                    iTextSharp.text.Image ImgLogo = iTextSharp.text.Image.GetInstance(logo, BaseColor.WHITE);
                    ImgLogo.ScaleAbsolute(150f, 60f);//-----
                    ImgLogo.Alignment = Element.ALIGN_TOP;


                    Paragraph prgText = new Paragraph("Hola mensaje",Helpers.Fuentes.Arial10Normal);
                    celda.AddElement(ImgLogo);
                    celda.AddElement(prgText);
                    celda.Colspan = 2;
                    celda.Border = 1;
                    celda.Padding = 22;
                    tabla.AddCell(celda);



                    return tabla;
                }
                public static PdfPTable Encabezado(DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string cufe, string RutaImg)
                {
                    //Variables internas
                    string Acercade = (string)DsInvoiceAR.Tables["Company"].Rows[0]["Character08"];
                    string CompanyName = DsInvoiceAR.Tables["Company"].Rows[0]["Name"].ToString();
                    string CompanyStateTaxID = DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"].ToString();
                    string CompanyAddress1 = DsInvoiceAR.Tables["Company"].Rows[0]["Address1"].ToString();
                    string CompanyCity = DsInvoiceAR.Tables["Company"].Rows[0]["City"].ToString();
                    string CompanyState = DsInvoiceAR.Tables["Company"].Rows[0]["State"].ToString();
                    string CompanyPhoneNum = DsInvoiceAR.Tables["Company"].Rows[0]["PhoneNum"].ToString();
                    string NumLegalFactura = DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"].ToString();
                    string InvcHeadInvoiceNum = (string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceNum"];
                    //------------ 

                    PdfPTable tableEncabezado = new PdfPTable(new float[] { 1.4f, 1.5f, 1.2f, 1.3f })
                    {
                        WidthPercentage = 100
                    };
                    System.Drawing.Image logo = null;
                    var requestLogo = WebRequest.Create(RutaImg);

                    using (var responseLogo = requestLogo.GetResponse())
                    using (var streamLogo = responseLogo.GetResponseStream())
                    {
                        logo = Bitmap.FromStream(streamLogo);
                    }

                    iTextSharp.text.Image ImgLogo = iTextSharp.text.Image.GetInstance(logo, BaseColor.WHITE);
                    ImgLogo.ScaleAbsolute(150f, 60f);//-----
                    ImgLogo.Alignment = Element.ALIGN_TOP;
                    //divLogo.BackgroundImage = ImgLogo;
                    //----- Logo por celda -------
                    PdfPCell celLogo = new PdfPCell();
                    celLogo.AddElement(ImgLogo);

                    celLogo.VerticalAlignment = Element.ALIGN_MIDDLE;
                    iTextSharp.text.Font fontAcercade = FontFactory.GetFont("Arial", 5, iTextSharp.text.Font.NORMAL);
                    Paragraph prgAcercade = new Paragraph(Acercade, fontAcercade);
                    prgAcercade.Alignment = Element.ALIGN_CENTER;
                    celLogo.AddElement(prgAcercade);
                    celLogo.Border = 0;
                    tableEncabezado.AddCell(celLogo);
                    //----------------
                    Paragraph prgTitle = new Paragraph(CompanyName, Helpers.Fuentes.fontTitle);

                    Paragraph prgTitle2 = new Paragraph(string.Format("NIT. {0}\n{1}\n{2} {3} COLOMBIA\n{4}", CompanyStateTaxID,
                        CompanyAddress1, CompanyCity, CompanyState, CompanyPhoneNum), Helpers.Fuentes.fontTitle2);
                    prgTitle.Alignment = Element.ALIGN_CENTER;
                    prgTitle2.Alignment = Element.ALIGN_CENTER;

                    var celInfoEmpresa = new PdfPCell()
                    {
                        Padding = 0,
                        Border = PdfPCell.NO_BORDER,
                    };
                    celInfoEmpresa.AddElement(prgTitle);
                    celInfoEmpresa.AddElement(prgTitle2);

                    tableEncabezado.AddCell(celInfoEmpresa);

                    //var celTextSomosAutoretenedores = new PdfPCell()
                    //{
                    //    Padding = 0,
                    //    Border = PdfPCell.NO_BORDER,
                    //};
                    //tableEncabezado.AddCell(celTextSomosAutoretenedores);

                    //----------------------------------------------------------------------------------------------------
                    iTextSharp.text.Font fontTitleFactura = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);

                    PdfPTable tableFactura = new PdfPTable(3)
                    {
                        HorizontalAlignment = Element.ALIGN_CENTER,
                    };

                    //Dimenciones.
                    float[] DimencionFactura = new float[3];
                    DimencionFactura[0] = 1.0F;//
                    DimencionFactura[1] = 4.0F;//
                    DimencionFactura[2] = 0.5F;//

                    tableFactura.WidthPercentage = 100;
                    tableFactura.SetWidths(DimencionFactura);

                    string textTipoFactura = "FACTURA DE VENTA";
                    if (InvoiceType == "InvoiceType")
                        textTipoFactura = "FACTURA DE VENTA";
                    else if (InvoiceType == "CreditNoteType")
                        textTipoFactura = "NOTA CREDITO";
                    else if (InvoiceType == "DebitNoteType")
                        textTipoFactura = "NOTA DEBITO";

                    iTextSharp.text.pdf.PdfPCell celTittle = new iTextSharp.text.pdf.PdfPCell(new Phrase($"{textTipoFactura}\n\n", fontTitleFactura));
                    celTittle.Colspan = 3;
                    celTittle.Padding = 3;
                    celTittle.HorizontalAlignment = Element.ALIGN_CENTER;
                    celTittle.VerticalAlignment = Element.ALIGN_TOP;
                    celTittle.Border = 0;
                    celTittle.BackgroundColor = BaseColor.LIGHT_GRAY;
                    tableFactura.AddCell(celTittle);

                    iTextSharp.text.pdf.PdfPCell celNo = new iTextSharp.text.pdf.PdfPCell(new Phrase("No. ", fontTitleFactura));
                    celNo.Colspan = 1;
                    celNo.Padding = 5;
                    celNo.HorizontalAlignment = Element.ALIGN_CENTER;
                    celNo.VerticalAlignment = Element.ALIGN_TOP;
                    celNo.Border = 0;
                    celNo.BackgroundColor = BaseColor.LIGHT_GRAY;
                    tableFactura.AddCell(celNo);

                    
                    //if (InvoiceType == "InvoiceType")
                    //    NumLegalFactura = DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiseRef"].ToString();
                    //else if (InvoiceType == "CreditNoteType")
                    //    NumLegalFactura = DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"].ToString();
                    //else
                    //    NumLegalFactura = DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"].ToString();

                    iTextSharp.text.pdf.PdfPCell celNoFactura = new iTextSharp.text.pdf.PdfPCell(new Phrase(NumLegalFactura, fontTitleFactura));
                    celNoFactura.Colspan = 1;
                    celNoFactura.Padding = 5;
                    celNoFactura.HorizontalAlignment = Element.ALIGN_LEFT;
                    celNoFactura.VerticalAlignment = Element.ALIGN_TOP;
                    celNoFactura.Border = 0;
                    celNoFactura.BackgroundColor = BaseColor.WHITE;
                    tableFactura.AddCell(celNoFactura);


                    iTextSharp.text.pdf.PdfPCell celEspacioNoFactura = new iTextSharp.text.pdf.PdfPCell(new Phrase(" ", fontTitleFactura));
                    celEspacioNoFactura.Colspan = 1;
                    //celNo.Padding = 3;
                    celEspacioNoFactura.HorizontalAlignment = Element.ALIGN_CENTER;
                    celEspacioNoFactura.VerticalAlignment = Element.ALIGN_TOP;
                    celEspacioNoFactura.Border = 0;
                    celEspacioNoFactura.BackgroundColor = BaseColor.LIGHT_GRAY;
                    tableFactura.AddCell(celEspacioNoFactura);

                    iTextSharp.text.pdf.PdfPCell celRellenoNoFactura = new iTextSharp.text.pdf.PdfPCell(new Phrase(" ", fontTitleFactura));
                    celRellenoNoFactura.Colspan = 3;
                    celRellenoNoFactura.Padding = 3;
                    celRellenoNoFactura.BackgroundColor = BaseColor.LIGHT_GRAY;
                    celRellenoNoFactura.HorizontalAlignment = Element.ALIGN_RIGHT;
                    celRellenoNoFactura.VerticalAlignment = Element.ALIGN_TOP;
                    celRellenoNoFactura.Border = 0;
                    tableFactura.AddCell(celRellenoNoFactura);

                    iTextSharp.text.pdf.PdfPCell celConsecutivoInterno = new iTextSharp.text.pdf.PdfPCell(
                        new Phrase(InvcHeadInvoiceNum, Helpers.Fuentes.fontCustom));
                    celConsecutivoInterno.Colspan = 4;
                    //celConsecutivoInterno.Padding = 3;
                    celConsecutivoInterno.BackgroundColor = BaseColor.WHITE;
                    celConsecutivoInterno.HorizontalAlignment = Element.ALIGN_RIGHT;
                    celConsecutivoInterno.VerticalAlignment = Element.ALIGN_TOP;
                    celConsecutivoInterno.Border = 0;
                    tableFactura.AddCell(celConsecutivoInterno);

                    tableEncabezado.AddCell(new PdfPCell(tableFactura)
                    {
                        Padding = 0,
                        Border = PdfPCell.NO_BORDER,
                        //Colspan = 4
                    });
                    //---------
                    //-----------------------------------------------------------------------------------------------------
                    iTextSharp.text.Image ImgQR = iTextSharp.text.Image.GetInstance(QRInvoice, BaseColor.WHITE);
                    ImgQR.ScaleAbsolute(70f, 70f);
                    ImgQR.Alignment = Element.ALIGN_CENTER;

                    PdfPCell celQR = new PdfPCell()
                    {
                        Padding = 5,
                        Border = PdfPCell.NO_BORDER,
                    };
                    celQR.AddElement(ImgQR);
                    Paragraph prgCUFE = new Paragraph($"CUFE: {cufe}", fontAcercade);
                    Paragraph prgMuestra = new Paragraph("Representacion gráfica factura electrónica", fontAcercade);
                    prgCUFE.Alignment = Element.ALIGN_CENTER;
                    prgMuestra.Alignment = Element.ALIGN_CENTER;
                    celQR.AddElement(prgCUFE);
                    celQR.AddElement(prgMuestra);
                    tableEncabezado.AddCell(celQR);

                    return tableEncabezado;
                }
                public static PdfPTable DatosCliente(DataSet DsInvoiceAR)
                {


                    #region FACTURAR Y DESPACHAR A
                    //------------------------------------------------------------------------------------------------
                    PdfPTable tableFacturar = new PdfPTable(3);
                    //Dimenciones.
                    float[] DimencionFacturar = new float[3];
                    DimencionFacturar[0] = 1.0F;//
                    DimencionFacturar[1] = 0.01F;//
                    DimencionFacturar[2] = 1.0F;//

                    tableFacturar.WidthPercentage = 100;
                    tableFacturar.SetWidths(DimencionFacturar);
                    //----------------------------------------------------------------------------------------------
                    PdfPTable tableFacturarA = new PdfPTable(2);
                    float[] DimencionFacturarA = new float[2];
                    DimencionFacturarA[0] = 0.8F;//
                    DimencionFacturarA[1] = 2.0F;//

                    tableFacturarA.WidthPercentage = 100;
                    tableFacturarA.SetWidths(DimencionFacturarA);

                    iTextSharp.text.pdf.PdfPCell celDatosFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase("FACTURAR A:\n", Helpers.Fuentes.fontTitleFactura));
                    celDatosFacturarA.Colspan = 2;
                    celDatosFacturarA.Padding = 3;
                    celDatosFacturarA.Border = 0;
                    celDatosFacturarA.BorderColorBottom = BaseColor.WHITE;
                    celDatosFacturarA.HorizontalAlignment = Element.ALIGN_CENTER;
                    celDatosFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                    tableFacturarA.AddCell(celDatosFacturarA);

                    iTextSharp.text.pdf.PdfPCell celTextClienteFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase("CLIENTE:", Helpers.Fuentes.fontTitleFactura));
                    celTextClienteFacturarA.Colspan = 1;
                    celTextClienteFacturarA.Padding = 3;
                    celTextClienteFacturarA.Border = 0;
                    celTextClienteFacturarA.HorizontalAlignment = Element.ALIGN_LEFT;
                    celTextClienteFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                    tableFacturarA.AddCell(celTextClienteFacturarA);

                    PdfPCell celClienteFacturarA = new PdfPCell(new Phrase((string)DsInvoiceAR.Tables["Customer"].Rows[0]["Name"], Helpers.Fuentes.fontTitle2));
                    celClienteFacturarA.Colspan = 1;
                    celClienteFacturarA.Padding = 3;
                    celClienteFacturarA.Border = 0;
                    celClienteFacturarA.HorizontalAlignment = Element.ALIGN_LEFT;
                    celClienteFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                    tableFacturarA.AddCell(celClienteFacturarA);

                    iTextSharp.text.pdf.PdfPCell celTextNitFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase("NIT:", Helpers.Fuentes.fontTitleFactura));
                    celTextNitFacturarA.Colspan = 1;
                    celTextNitFacturarA.Padding = 3;
                    celTextNitFacturarA.Border = 0;
                    celTextNitFacturarA.HorizontalAlignment = Element.ALIGN_LEFT;
                    celTextNitFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                    tableFacturarA.AddCell(celTextNitFacturarA);

                    PdfPCell celNitFacturarA = new PdfPCell(new Phrase((string)DsInvoiceAR.Tables["Customer"].Rows[0]["ResaleID"], Helpers.Fuentes.fontTitle2));
                    celNitFacturarA.Colspan = 1;
                    celNitFacturarA.Padding = 3;
                    celNitFacturarA.Border = 0;
                    celNitFacturarA.HorizontalAlignment = Element.ALIGN_LEFT;
                    celNitFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                    tableFacturarA.AddCell(celNitFacturarA);

                    iTextSharp.text.pdf.PdfPCell celTextDireccionFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase("DIRECCION:", Helpers.Fuentes.fontTitleFactura));
                    celTextDireccionFacturarA.Colspan = 1;
                    celTextDireccionFacturarA.Padding = 3;
                    celTextDireccionFacturarA.Border = 0;
                    celTextDireccionFacturarA.HorizontalAlignment = Element.ALIGN_LEFT;
                    celTextDireccionFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                    tableFacturarA.AddCell(celTextDireccionFacturarA);

                    PdfPCell celDireccionFacturarA = new PdfPCell(new Phrase((string)DsInvoiceAR.Tables["Customer"].Rows[0]["Address1"], Helpers.Fuentes.fontTitle2));
                    celDireccionFacturarA.Colspan = 1;
                    celDireccionFacturarA.Padding = 3;
                    celDireccionFacturarA.Border = 0;
                    celDireccionFacturarA.HorizontalAlignment = Element.ALIGN_LEFT;
                    celDireccionFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                    tableFacturarA.AddCell(celDireccionFacturarA);

                    iTextSharp.text.pdf.PdfPCell celTextTelFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase("TELEFONO:", Helpers.Fuentes.fontTitleFactura));
                    celTextTelFacturarA.Colspan = 1;
                    celTextTelFacturarA.Padding = 3;
                    celTextTelFacturarA.Border = 0;
                    celTextTelFacturarA.HorizontalAlignment = Element.ALIGN_LEFT;
                    celTextTelFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                    tableFacturarA.AddCell(celTextTelFacturarA);

                    PdfPCell celTelFacturarA = new PdfPCell(new Phrase((string)DsInvoiceAR.Tables["Customer"].Rows[0]["PhoneNum"], Helpers.Fuentes.fontTitle2));
                    celTelFacturarA.Colspan = 1;
                    celTelFacturarA.Padding = 3;
                    celTelFacturarA.Border = 0;
                    celTelFacturarA.HorizontalAlignment = Element.ALIGN_LEFT;
                    celTelFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                    tableFacturarA.AddCell(celTelFacturarA);

                    iTextSharp.text.pdf.PdfPCell celTextCiudadFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase("CIUDAD:", Helpers.Fuentes.fontTitleFactura));
                    celTextCiudadFacturarA.Colspan = 1;
                    celTextCiudadFacturarA.Padding = 3;
                    celTextCiudadFacturarA.Border = 0;
                    celTextCiudadFacturarA.HorizontalAlignment = Element.ALIGN_LEFT;
                    celTextCiudadFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                    tableFacturarA.AddCell(celTextCiudadFacturarA);

                    PdfPCell celCiudadFacturarA = new PdfPCell(new Phrase((string)DsInvoiceAR.Tables["Customer"].Rows[0]["City"], Helpers.Fuentes.fontTitle2));
                    celCiudadFacturarA.Colspan = 1;
                    celCiudadFacturarA.Padding = 3;
                    celCiudadFacturarA.Border = 0;
                    celCiudadFacturarA.HorizontalAlignment = Element.ALIGN_LEFT;
                    celCiudadFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                    tableFacturarA.AddCell(celCiudadFacturarA);

                    iTextSharp.text.pdf.PdfPCell celTextPaisFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase("PAIS:", Helpers.Fuentes.fontTitleFactura));
                    celTextPaisFacturarA.Colspan = 1;
                    celTextPaisFacturarA.Padding = 3;
                    celTextPaisFacturarA.Border = 0;
                    celTextPaisFacturarA.HorizontalAlignment = Element.ALIGN_LEFT;
                    celTextPaisFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                    tableFacturarA.AddCell(celTextPaisFacturarA);

                    PdfPCell celPaisFacturarA = new PdfPCell(new Phrase((string)DsInvoiceAR.Tables["Customer"].Rows[0]["Country"], Helpers.Fuentes.fontTitle2));
                    celPaisFacturarA.Colspan = 1;
                    celPaisFacturarA.Padding = 3;
                    celPaisFacturarA.Border = 0;
                    celPaisFacturarA.HorizontalAlignment = Element.ALIGN_LEFT;
                    celPaisFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                    tableFacturarA.AddCell(celPaisFacturarA);

                    iTextSharp.text.pdf.PdfPCell celTittleFacturarA = new iTextSharp.text.pdf.PdfPCell(tableFacturarA);
                    tableFacturar.AddCell(celTittleFacturarA);
                    //----------------------------------------------------------------------------------------------------------------------------

                    iTextSharp.text.pdf.PdfPCell celEspacio2 = new iTextSharp.text.pdf.PdfPCell(new Phrase(" ", Helpers.Fuentes.fontTitleFactura));
                    celEspacio2.Border = 0;
                    tableFacturar.AddCell(celEspacio2);

                    //------------------------------------------------------------------------------------------

                    PdfPTable tableDespacharA = new PdfPTable(2);
                    float[] DimencionDespacharA = new float[2];
                    DimencionDespacharA[0] = 0.8F;//
                    DimencionDespacharA[1] = 2.0F;//

                    tableDespacharA.WidthPercentage = 100;
                    tableDespacharA.SetWidths(DimencionDespacharA);

                    iTextSharp.text.pdf.PdfPCell celTittleDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase("DESPACHAR A:\n", Helpers.Fuentes.fontTitleFactura));
                    celTittleDespacharA.Colspan = 2;
                    celTittleDespacharA.Padding = 3;
                    celTittleDespacharA.Border = 0;
                    celTittleDespacharA.BorderColorBottom = BaseColor.WHITE;
                    celTittleDespacharA.HorizontalAlignment = Element.ALIGN_CENTER;
                    celTittleDespacharA.VerticalAlignment = Element.ALIGN_TOP;
                    tableDespacharA.AddCell(celTittleDespacharA);

                    iTextSharp.text.pdf.PdfPCell celTextClienteDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase("CLIENTE:", Helpers.Fuentes.fontTitleFactura));
                    celTextClienteDespacharA.Colspan = 1;
                    celTextClienteDespacharA.Padding = 3;
                    celTextClienteDespacharA.Border = 0;
                    celTextClienteDespacharA.HorizontalAlignment = Element.ALIGN_LEFT;
                    celTextClienteDespacharA.VerticalAlignment = Element.ALIGN_TOP;
                    tableDespacharA.AddCell(celTextClienteDespacharA);

                    PdfPCell celClienteDespacharA = new PdfPCell(new Phrase((string)DsInvoiceAR.Tables["InvcDtl"].Rows[0]["ShipToCustName"], Helpers.Fuentes.fontTitle2));
                    celClienteDespacharA.Colspan = 1;
                    celClienteDespacharA.Padding = 3;
                    celClienteDespacharA.Border = 0;
                    celClienteDespacharA.HorizontalAlignment = Element.ALIGN_LEFT;
                    celClienteDespacharA.VerticalAlignment = Element.ALIGN_TOP;
                    tableDespacharA.AddCell(celClienteDespacharA);

                    iTextSharp.text.pdf.PdfPCell celTextNitDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase("NIT:", Helpers.Fuentes.fontTitleFactura));
                    celTextNitDespacharA.Colspan = 1;
                    celTextNitDespacharA.Padding = 3;
                    celTextNitDespacharA.Border = 0;
                    celTextNitDespacharA.HorizontalAlignment = Element.ALIGN_LEFT;
                    celTextNitDespacharA.VerticalAlignment = Element.ALIGN_TOP;
                    tableDespacharA.AddCell(celTextNitDespacharA);

                    PdfPCell celNitDespacharA = new PdfPCell(new Phrase((string)DsInvoiceAR.Tables["Customer"].Rows[0]["ResaleID"], Helpers.Fuentes.fontTitle2));
                    celNitDespacharA.Colspan = 1;
                    celNitDespacharA.Padding = 3;
                    celNitDespacharA.Border = 0;
                    celNitDespacharA.HorizontalAlignment = Element.ALIGN_LEFT;
                    celNitDespacharA.VerticalAlignment = Element.ALIGN_TOP;
                    tableDespacharA.AddCell(celNitDespacharA);

                    iTextSharp.text.pdf.PdfPCell celTextDireccionDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase("DIRECCION:", Helpers.Fuentes.fontTitleFactura));
                    celTextDireccionDespacharA.Colspan = 1;
                    celTextDireccionDespacharA.Padding = 3;
                    celTextDireccionDespacharA.Border = 0;
                    celTextDireccionDespacharA.HorizontalAlignment = Element.ALIGN_LEFT;
                    celTextDireccionDespacharA.VerticalAlignment = Element.ALIGN_TOP;
                    tableDespacharA.AddCell(celTextDireccionDespacharA);

                    //----- Direccion del despacho
                    string Address1 = string.Empty;
                    string Address2 = string.Empty;
                    string Address3 = string.Empty;
                    if (Helpers.Compartido.VerificarExistenciaColumnas(DsInvoiceAR, "Customer", "ShipToAddress1",0))
                        Address1 = DsInvoiceAR.Tables["Customer"].Rows[0]["ShipToAddress1"].ToString();
                    if(Helpers.Compartido.VerificarExistenciaColumnas(DsInvoiceAR, "Customer", "ShipToAddress2",0))
                        Address2 = DsInvoiceAR.Tables["Customer"].Rows[0]["ShipToAddress2"].ToString();
                    if(Helpers.Compartido.VerificarExistenciaColumnas(DsInvoiceAR, "Customer", "ShipToAddress3",0))
                        Address3 = DsInvoiceAR.Tables["Customer"].Rows[0]["ShipToAddress3"].ToString();

                    string Direccion = Address1 + "\r\n" + Address2 + "\r\n" + Address3;
                    //PdfPCell celDireccionDespacharA = new PdfPCell(new Phrase((string)DsInvoiceAR.Tables["InvcDtl"].Rows[0]["DispShipToAddr"], Helpers.Fuentes.fontTitle2));
                    PdfPCell celDireccionDespacharA = new PdfPCell(new Phrase(Direccion, Helpers.Fuentes.fontTitle2));
                    celDireccionDespacharA.Colspan = 1;
                    celDireccionDespacharA.Padding = 3;
                    celDireccionDespacharA.Border = 0;
                    celDireccionDespacharA.HorizontalAlignment = Element.ALIGN_LEFT;
                    celDireccionDespacharA.VerticalAlignment = Element.ALIGN_TOP;
                    tableDespacharA.AddCell(celDireccionDespacharA);

                    iTextSharp.text.pdf.PdfPCell celTextTelDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase("TELEFONO:", Helpers.Fuentes.fontTitleFactura));
                    celTextTelDespacharA.Colspan = 1;
                    celTextTelDespacharA.Padding = 3;
                    celTextTelDespacharA.Border = 0;
                    celTextTelDespacharA.HorizontalAlignment = Element.ALIGN_LEFT;
                    celTextTelDespacharA.VerticalAlignment = Element.ALIGN_TOP;
                    tableDespacharA.AddCell(celTextTelDespacharA);

                    PdfPCell celTelDespacharA = new PdfPCell(new Phrase((string)DsInvoiceAR.Tables["InvcDtl"].Rows[0]["ShipToNum"], Helpers.Fuentes.fontTitle2));
                    celTelDespacharA.Colspan = 1;
                    celTelDespacharA.Padding = 3;
                    celTelDespacharA.Border = 0;
                    celTelDespacharA.HorizontalAlignment = Element.ALIGN_LEFT;
                    celTelDespacharA.VerticalAlignment = Element.ALIGN_TOP;
                    tableDespacharA.AddCell(celTelDespacharA);

                    iTextSharp.text.pdf.PdfPCell celTextCiudadDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase("CIUDAD:", Helpers.Fuentes.fontTitleFactura));
                    celTextCiudadDespacharA.Colspan = 1;
                    celTextCiudadDespacharA.Padding = 3;
                    celTextCiudadDespacharA.Border = 0;
                    celTextCiudadDespacharA.HorizontalAlignment = Element.ALIGN_LEFT;
                    celTextCiudadDespacharA.VerticalAlignment = Element.ALIGN_TOP;
                    tableDespacharA.AddCell(celTextCiudadDespacharA);

                    //----- Ciudad del shipto
                    string ShipToCity = string.Empty;
                    string ShipToState = string.Empty;
                    if (Helpers.Compartido.VerificarExistenciaColumnas(DsInvoiceAR, "Customer", "ShipToCity",0))
                        ShipToCity = DsInvoiceAR.Tables["Customer"].Rows[0]["ShipToCity"].ToString();
                    if(Helpers.Compartido.VerificarExistenciaColumnas(DsInvoiceAR, "Customer", "ShipToState",0))
                        ShipToState = DsInvoiceAR.Tables["Customer"].Rows[0]["ShipToState"].ToString();
                    string Ubicacion = ShipToCity;
                    if (!string.IsNullOrEmpty(ShipToState))
                        Ubicacion += " - " + ShipToState;

                    PdfPCell celCiudadDespacharA = new PdfPCell(new Phrase(Ubicacion, Helpers.Fuentes.fontTitle2));
                    celCiudadDespacharA.Colspan = 1;
                    celCiudadDespacharA.Padding = 3;
                    celCiudadDespacharA.Border = 0;
                    celCiudadDespacharA.HorizontalAlignment = Element.ALIGN_LEFT;
                    celCiudadDespacharA.VerticalAlignment = Element.ALIGN_TOP;
                    tableDespacharA.AddCell(celCiudadDespacharA);

                    //----- Pais del Shipto
                    iTextSharp.text.pdf.PdfPCell celTextPaisDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase("PAIS:", Helpers.Fuentes.fontTitleFactura));
                    celTextPaisDespacharA.Colspan = 1;
                    celTextPaisDespacharA.Padding = 3;
                    celTextPaisDespacharA.Border = 0;
                    celTextPaisDespacharA.HorizontalAlignment = Element.ALIGN_LEFT;
                    celTextPaisDespacharA.VerticalAlignment = Element.ALIGN_TOP;
                    tableDespacharA.AddCell(celTextPaisDespacharA);

                    string ShipToCountry = string.Empty;
                    if(Helpers.Compartido.VerificarExistenciaColumnas(DsInvoiceAR, "Customer", "ShipToCountry",0))
                        ShipToCountry = DsInvoiceAR.Tables["Customer"].Rows[0]["ShipToCountry"].ToString();
                    PdfPCell celPaisDespacharA = new PdfPCell(new Phrase(ShipToCountry, Helpers.Fuentes.fontTitle2));
                    celPaisDespacharA.Colspan = 1;
                    celPaisDespacharA.Padding = 3;
                    celPaisDespacharA.Border = 0;
                    celPaisDespacharA.HorizontalAlignment = Element.ALIGN_LEFT;
                    celPaisDespacharA.VerticalAlignment = Element.ALIGN_TOP;
                    tableDespacharA.AddCell(celPaisDespacharA);

                    iTextSharp.text.pdf.PdfPCell celDatosDespacharA = new iTextSharp.text.pdf.PdfPCell(tableDespacharA);
                    tableFacturar.AddCell(celDatosDespacharA);
                    //-------------------------------------------------------------------------------------- 
                    #endregion
                    return tableFacturar;
                }
                #region Detalles
                public static PdfPTable Detalles(DataSet DsInvoiceAR)
                {
                    //---- Variables ---------
                    string InvcHeadSalesRep1 = (string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["SalesRepName1"];
                    //------------------------

                    PdfPTable tableDetalles = new PdfPTable(5);
                    tableDetalles.PaddingTop = 20;
                    //Dimenciones.
                    float[] DimencionDetalles = new float[5];
                    DimencionDetalles[0] = 2.5F;//
                    DimencionDetalles[1] = 1.0F;//
                    DimencionDetalles[2] = 1.0F;//
                    DimencionDetalles[3] = 1.0F;//
                    DimencionDetalles[4] = 0.8F;//

                    tableDetalles.WidthPercentage = 100;
                    tableDetalles.SetWidths(DimencionDetalles);

                    iTextSharp.text.pdf.PdfPCell celVendedor = new iTextSharp.text.pdf.PdfPCell(new Phrase("VENDEDOR", Helpers.Fuentes.fontTitleFactura));
                    celVendedor.Colspan = 1;
                    celVendedor.Padding = 3;
                    //celVendedor.Border = 0;
                    celVendedor.BorderColorBottom = BaseColor.WHITE;
                    celVendedor.HorizontalAlignment = Element.ALIGN_CENTER;
                    celVendedor.VerticalAlignment = Element.ALIGN_TOP;
                    tableDetalles.AddCell(celVendedor);

                    iTextSharp.text.pdf.PdfPCell celOC_Cliente = new iTextSharp.text.pdf.PdfPCell(new Phrase("O.C. CLIENTE", Helpers.Fuentes.fontTitleFactura));
                    celOC_Cliente.Colspan = 1;
                    celOC_Cliente.Padding = 3;
                    //celVendedor.Border = 0;
                    celOC_Cliente.BorderColorBottom = BaseColor.WHITE;
                    celOC_Cliente.HorizontalAlignment = Element.ALIGN_CENTER;
                    celOC_Cliente.VerticalAlignment = Element.ALIGN_TOP;
                    tableDetalles.AddCell(celOC_Cliente);

                    iTextSharp.text.pdf.PdfPCell celOrdenVenta = new iTextSharp.text.pdf.PdfPCell(new Phrase("ORDEN DE VENTA", Helpers.Fuentes.fontTitleFactura));
                    celOrdenVenta.Colspan = 1;
                    celOrdenVenta.Padding = 3;
                    //celVendedor.Border = 0;
                    celOrdenVenta.BorderColorBottom = BaseColor.WHITE;
                    celOrdenVenta.HorizontalAlignment = Element.ALIGN_CENTER;
                    celOrdenVenta.VerticalAlignment = Element.ALIGN_TOP;
                    tableDetalles.AddCell(celOrdenVenta);

                    iTextSharp.text.pdf.PdfPCell celTerminoPago = new iTextSharp.text.pdf.PdfPCell(new Phrase("TÉRMINO DE PAGO", Helpers.Fuentes.fontTitleFactura));
                    celTerminoPago.Colspan = 1;
                    celTerminoPago.Padding = 3;
                    //celVendedor.Border = 0;
                    celTerminoPago.BorderColorBottom = BaseColor.WHITE;
                    celTerminoPago.HorizontalAlignment = Element.ALIGN_CENTER;
                    celTerminoPago.VerticalAlignment = Element.ALIGN_TOP;
                    tableDetalles.AddCell(celTerminoPago);

                    iTextSharp.text.pdf.PdfPCell celRemision = new iTextSharp.text.pdf.PdfPCell(new Phrase("REMISIÓN", Helpers.Fuentes.fontTitleFactura));
                    celRemision.Colspan = 1;
                    celRemision.Padding = 3;
                    //celVendedor.Border = 0;
                    celRemision.BorderColorBottom = BaseColor.WHITE;
                    celRemision.HorizontalAlignment = Element.ALIGN_CENTER;
                    celRemision.VerticalAlignment = Element.ALIGN_TOP;
                    tableDetalles.AddCell(celRemision);

                    PdfPCell celDataVendedor = new PdfPCell(new Phrase(InvcHeadSalesRep1, Helpers.Fuentes.fontCustom));
                    celDataVendedor.Colspan = 1;
                    celDataVendedor.Padding = 3;
                    //celVendedor.Border = 0;
                    celDataVendedor.BorderColorBottom = BaseColor.WHITE;
                    celDataVendedor.HorizontalAlignment = Element.ALIGN_CENTER;
                    celDataVendedor.VerticalAlignment = Element.ALIGN_TOP;
                    tableDetalles.AddCell(celDataVendedor);

                    PdfPCell celDataOC_Cliente = new PdfPCell(new Phrase((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["PONum"], Helpers.Fuentes.fontCustom));
                    celDataOC_Cliente.Colspan = 1;
                    celDataOC_Cliente.Padding = 3;
                    //celVendedor.Border = 0;
                    celDataOC_Cliente.BorderColorBottom = BaseColor.WHITE;
                    celDataOC_Cliente.HorizontalAlignment = Element.ALIGN_CENTER;
                    celDataOC_Cliente.VerticalAlignment = Element.ALIGN_TOP;
                    tableDetalles.AddCell(celDataOC_Cliente);

                    PdfPCell celDataOrdenVenta = new PdfPCell(new Phrase((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["OrderNum"], Helpers.Fuentes.fontCustom));
                    celDataOrdenVenta.Colspan = 1;
                    celDataOrdenVenta.Padding = 3;
                    //celVendedor.Border = 0;
                    celDataOrdenVenta.BorderColorBottom = BaseColor.WHITE;
                    celDataOrdenVenta.HorizontalAlignment = Element.ALIGN_CENTER;
                    celDataOrdenVenta.VerticalAlignment = Element.ALIGN_TOP;
                    tableDetalles.AddCell(celDataOrdenVenta);

                    PdfPCell celDataTerminoPago = new PdfPCell(new Phrase((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["TermsCodeDescription"], Helpers.Fuentes.fontCustom));
                    celDataTerminoPago.Colspan = 1;
                    celDataTerminoPago.Padding = 3;
                    //celVendedor.Border = 0;
                    celDataTerminoPago.BorderColorBottom = BaseColor.WHITE;
                    celDataTerminoPago.HorizontalAlignment = Element.ALIGN_CENTER;
                    celDataTerminoPago.VerticalAlignment = Element.ALIGN_TOP;
                    tableDetalles.AddCell(celDataTerminoPago);

                    string remision = DsInvoiceAR.Tables["InvcDtl"].Rows[0]["PackNum"].ToString();
                    PdfPCell celDataRemision = new PdfPCell(new Phrase(remision, Helpers.Fuentes.fontCustom));
                    celDataRemision.Colspan = 1;
                    celDataRemision.Padding = 3;
                    //celVendedor.Border = 0;
                    celDataRemision.BorderColorBottom = BaseColor.WHITE;
                    celDataRemision.HorizontalAlignment = Element.ALIGN_CENTER;
                    celDataRemision.VerticalAlignment = Element.ALIGN_TOP;
                    tableDetalles.AddCell(celDataRemision);
                    tableDetalles.AddCell(celVendedor);


                    return tableDetalles;
                }
                public static PdfPTable Detalles2(DataSet DsInvoiceAR)
                {
                    PdfPTable tableDetalles2 = new PdfPTable(3);
                    tableDetalles2.WidthPercentage = 100;
                    tableDetalles2.PaddingTop = 1f;

                    PdfPTable tableFechaFactura = new PdfPTable(2);
                    float[] dimecionesTablaFecha = new float[2];
                    dimecionesTablaFecha[0] = 0.7F;
                    dimecionesTablaFecha[1] = 1.0F;
                    tableFechaFactura.SetWidths(dimecionesTablaFecha);

                    PdfPCell celTextFechaFactura = new PdfPCell(new Phrase("FECHA FACTURA: ", Helpers.Fuentes.fontTitle));
                    celTextFechaFactura.Colspan = 1;
                    celTextFechaFactura.Padding = 3;
                    celTextFechaFactura.Border = 0;
                    celTextFechaFactura.BorderColorBottom = BaseColor.WHITE;
                    celTextFechaFactura.HorizontalAlignment = Element.ALIGN_CENTER;
                    celTextFechaFactura.VerticalAlignment = Element.ALIGN_CENTER;
                    tableFechaFactura.AddCell(celTextFechaFactura);

                    //PdfPCell celFechaFactura = new PdfPCell(new Phrase(string.Format("{0:MM/dd/yyyy}", DateTime.Parse(DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"].ToString())),
                    //fontCustom));
                    PdfPCell celFechaFactura = new PdfPCell(new Phrase(string.Format("{0:MM/dd/yyyy}", DateTime.Parse(DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"].ToString())),
                    Helpers.Fuentes.fontCustom));
                    celFechaFactura.Colspan = 1;
                    celFechaFactura.Padding = 3;
                    celFechaFactura.PaddingTop = 4;
                    celFechaFactura.Border = 0;
                    celFechaFactura.BorderColorBottom = BaseColor.WHITE;
                    celFechaFactura.HorizontalAlignment = Element.ALIGN_LEFT;
                    celFechaFactura.VerticalAlignment = Element.ALIGN_CENTER;
                    tableFechaFactura.AddCell(celFechaFactura);

                    PdfPCell _celFechaFactura = new PdfPCell(tableFechaFactura);
                    tableDetalles2.AddCell(_celFechaFactura);


                    PdfPTable tableFechaVencimiento = new PdfPTable(2);
                    float[] dmTablaFechaVencimiento = new float[2];
                    dmTablaFechaVencimiento[0] = 1.0F;
                    dmTablaFechaVencimiento[1] = 1.0F;
                    tableFechaVencimiento.SetWidths(dmTablaFechaVencimiento);

                    //PdfPCell celTextFechaVencimiento = new PdfPCell(new Phrase("FECHA VENCIMIENTO: ",fontTitle));
                    PdfPCell celTextFechaVencimiento = new PdfPCell(new Phrase("FECHA VENCIMIENTO: ", Helpers.Fuentes.fontTitle));
                    celTextFechaVencimiento.Colspan = 1;
                    celTextFechaVencimiento.Padding = 3;
                    celTextFechaVencimiento.Border = 0;
                    celTextFechaVencimiento.HorizontalAlignment = Element.ALIGN_LEFT;
                    celTextFechaVencimiento.VerticalAlignment = Element.ALIGN_TOP;
                    tableFechaVencimiento.AddCell(celTextFechaVencimiento);

                    //PdfPCell celFechaVencimiento = new PdfPCell(new Phrase(string.Format("{0:MM/dd/yyyy}", DateTime.Parse(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"].ToString())),
                    //fontCustom));
                    PdfPCell celFechaVencimiento = new PdfPCell(new Phrase(string.Format("{0:MM/dd/yyyy}", DateTime.Parse(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"].ToString())),
                    Helpers.Fuentes.fontCustom));
                    celFechaVencimiento.Colspan = 1;
                    celFechaVencimiento.Padding = 4;
                    celFechaVencimiento.Border = 0;
                    celFechaVencimiento.BorderColorBottom = BaseColor.WHITE;
                    celFechaVencimiento.HorizontalAlignment = Element.ALIGN_LEFT;
                    celFechaVencimiento.VerticalAlignment = Element.ALIGN_TOP;
                    tableFechaVencimiento.AddCell(celFechaVencimiento);

                    PdfPCell _celFechaFacturaVencimiento = new PdfPCell(tableFechaVencimiento);
                    tableDetalles2.AddCell(_celFechaFacturaVencimiento);

                    PdfPTable tableMoneda = new PdfPTable(2);
                    float[] dimecionesTablaMoneda = new float[2];
                    dimecionesTablaMoneda[0] = 0.4F;
                    dimecionesTablaMoneda[1] = 1.0F;
                    tableMoneda.SetWidths(dimecionesTablaMoneda);

                    //PdfPCell celTextMoneda = new PdfPCell(new Phrase("MONEDA: ", fontTitle));
                    PdfPCell celTextMoneda = new PdfPCell(new Phrase("MONEDA: ", Helpers.Fuentes.fontTitle));
                    celTextMoneda.Colspan = 1;
                    celTextMoneda.Padding = 3;
                    celTextMoneda.Border = 0;
                    celTextMoneda.BorderColorBottom = BaseColor.WHITE;
                    celTextMoneda.HorizontalAlignment = Element.ALIGN_LEFT;
                    celTextMoneda.VerticalAlignment = Element.ALIGN_TOP;
                    tableMoneda.AddCell(celTextMoneda);

                    //PdfPCell celMoneda = new PdfPCell(new Phrase(DsInvoiceAR.Tables["InvcHead"].Rows[0]["CurrencyCode"].ToString(), fontCustom));
                    PdfPCell celMoneda = new PdfPCell(new Phrase(DsInvoiceAR.Tables["InvcHead"].Rows[0]["CurrencyCode"].ToString(), Helpers.Fuentes.fontCustom));
                    celMoneda.Colspan = 1;
                    celMoneda.Padding = 4;
                    celMoneda.Border = 0;
                    celMoneda.BorderColorBottom = BaseColor.WHITE;
                    celMoneda.HorizontalAlignment = Element.ALIGN_LEFT;
                    celMoneda.VerticalAlignment = Element.ALIGN_TOP;
                    tableMoneda.AddCell(celMoneda);

                    PdfPCell _celMoneda = new PdfPCell(tableMoneda);
                    tableDetalles2.AddCell(_celMoneda);
                    //---Segunda fila de datos
                    DetallesAddCeldas(ref tableDetalles2, 2, "SPECIAL INSTRUCTIONS / INSTRUCCIONES ESPECIALES", Element.ALIGN_LEFT, 0f, 0.5f, 0f, 0f);
                    DetallesAddCeldas(ref tableDetalles2, 1, $"TRM: {string.Format("{0:C2}", Convert.ToDecimal(DsInvoiceAR.Tables["InvcHead"].Rows[0]["ExchangeRate"]))}", Element.ALIGN_LEFT, 0f, 0f, 0.5f, 0f);
                    string InvoiceComment = string.Empty;
                    if (Helpers.Compartido.VerificarExistenciaColumnas(DsInvoiceAR, "InvcHead", "InvoiceComment", 0))
                        InvoiceComment = DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceComment"].ToString();
                    string PrecioUnitario = string.Empty;
                    if (Helpers.Compartido.VerificarExistenciaColumnas(DsInvoiceAR, "InvcDtl", "DocUnitPrice"))
                        PrecioUnitario = DsInvoiceAR.Tables["InvcDtl"].Rows[0]["DocUnitPrice"].ToString();
                    string mensaje = string.Empty;
                    mensaje += "Precio: " + string.Format("{0:C2}",Convert.ToDecimal(PrecioUnitario));
                    if(!string.IsNullOrEmpty(InvoiceComment))
                        mensaje += ", " + InvoiceComment;
                    DetallesAddCeldas(ref tableDetalles2, 3, mensaje, Element.ALIGN_LEFT, 0.5f, 0.5f, 0.5f, 0f);
                    return tableDetalles2;
                }
                #endregion
                #region Tablas Unidades
                public static PdfPTable TituloUnidades(DataSet DsInvoiceAR)
                {
                    PdfPTable tableTituloUnidades = new PdfPTable(6);
                    ////Dimenciones.
                    //float[] DimencionUnidades = new float[6];
                    //DimencionUnidades[0] = 1.0F;//
                    //DimencionUnidades[1] = 3.0F;//
                    //DimencionUnidades[2] = 1.0F;//
                    //DimencionUnidades[3] = 0.5F;//
                    //DimencionUnidades[4] = 1.0F;//
                    //DimencionUnidades[5] = 1.5F;//

                    tableTituloUnidades.WidthPercentage = 100;
                    tableTituloUnidades.SetWidths(DimensionUnidades);

                    iTextSharp.text.pdf.PdfPCell celCodigo = new iTextSharp.text.pdf.PdfPCell(new Phrase("CÓDIGO", Helpers.Fuentes.fontTitleFactura));
                    celCodigo.Colspan = 1;
                    celCodigo.Padding = 3;
                    //celCodigo.Border = 1;
                    celCodigo.BorderWidthRight = 0;
                    //celCodigo.BorderColorBottom = BaseColor.WHITE;
                    celCodigo.HorizontalAlignment = Element.ALIGN_CENTER;
                    celCodigo.VerticalAlignment = Element.ALIGN_TOP;
                    celCodigo.BackgroundColor = BaseColor.LIGHT_GRAY;
                    tableTituloUnidades.AddCell(celCodigo);

                    iTextSharp.text.pdf.PdfPCell celDescripcion = new iTextSharp.text.pdf.PdfPCell(new Phrase("DESCRIPCIÓN", Helpers.Fuentes.fontTitleFactura));
                    celDescripcion.Colspan = 1;
                    celDescripcion.Padding = 3;
                    //celDescripcion.Border = 1;
                    celDescripcion.BorderWidthRight = 0;
                    //celDescripcion.BorderColorBottom = BaseColor.WHITE;
                    celDescripcion.HorizontalAlignment = Element.ALIGN_CENTER;
                    celDescripcion.VerticalAlignment = Element.ALIGN_TOP;
                    celDescripcion.BackgroundColor = BaseColor.LIGHT_GRAY;
                    tableTituloUnidades.AddCell(celDescripcion);

                    iTextSharp.text.pdf.PdfPCell celCantidad = new iTextSharp.text.pdf.PdfPCell(new Phrase("CANTIDAD", Helpers.Fuentes.fontTitleFactura));
                    celCantidad.Colspan = 1;
                    celCantidad.Padding = 3;
                    //celVendedor.Border = 0;
                    celCantidad.BorderWidthRight = 0;
                    //celCantidad.BorderColorBottom = BaseColor.WHITE;
                    celCantidad.HorizontalAlignment = Element.ALIGN_CENTER;
                    celCantidad.VerticalAlignment = Element.ALIGN_TOP;
                    celCantidad.BackgroundColor = BaseColor.LIGHT_GRAY;
                    tableTituloUnidades.AddCell(celCantidad);

                    iTextSharp.text.pdf.PdfPCell celUnidad = new iTextSharp.text.pdf.PdfPCell(new Phrase("UNIDAD", Helpers.Fuentes.fontTitleFactura));
                    celUnidad.Colspan = 1;
                    celUnidad.Padding = 3;
                    //celVendedor.Border = 0;
                    celUnidad.BorderWidthRight = 0;
                    //celUnidad.BorderColorBottom = BaseColor.WHITE;
                    celUnidad.HorizontalAlignment = Element.ALIGN_CENTER;
                    celUnidad.VerticalAlignment = Element.ALIGN_TOP;
                    celUnidad.BackgroundColor = BaseColor.LIGHT_GRAY;
                    tableTituloUnidades.AddCell(celUnidad);

                    iTextSharp.text.pdf.PdfPCell celValorUnitario = new iTextSharp.text.pdf.PdfPCell(new Phrase("VALOR UNITARIO", Helpers.Fuentes.fontTitleFactura));
                    celValorUnitario.Colspan = 1;
                    celValorUnitario.Padding = 3;
                    //celVendedor.Border = 0;
                    celValorUnitario.BorderWidthRight = 0;
                    //celValorUnitario.BorderColorBottom = BaseColor.WHITE;
                    celValorUnitario.HorizontalAlignment = Element.ALIGN_CENTER;
                    celValorUnitario.VerticalAlignment = Element.ALIGN_TOP;
                    celValorUnitario.BackgroundColor = BaseColor.LIGHT_GRAY;
                    tableTituloUnidades.AddCell(celValorUnitario);

                    iTextSharp.text.pdf.PdfPCell celValorTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase("VALOR TOTAL", Helpers.Fuentes.fontTitleFactura));
                    celValorTotal.Colspan = 1;
                    celValorTotal.Padding = 3;
                    //celVendedor.Border = 0;
                    //celValorTotal.BorderColorBottom = BaseColor.WHITE;
                    celValorTotal.HorizontalAlignment = Element.ALIGN_CENTER;
                    celValorTotal.VerticalAlignment = Element.ALIGN_TOP;
                    celValorTotal.BackgroundColor = BaseColor.LIGHT_GRAY;
                    tableTituloUnidades.AddCell(celValorTotal);
                    return tableTituloUnidades;
                }
                public static PdfPTable Unidades(DataSet DsInvoiceAR)
                {
                    PdfPTable tableUnidades = new PdfPTable(6);
                    tableUnidades.WidthPercentage = 100;
                    tableUnidades.SetWidths(DimensionUnidades);
                    string CustomerCheckBox06 = "false";
                    if (Helpers.Compartido.VerificarExistenciaColumnas(DsInvoiceAR, "Customer", "CheckBox06", 0))
                        CustomerCheckBox06 = DsInvoiceAR.Tables["Customer"].Rows[0]["CheckBox06"].ToString();

                    foreach (DataRow InvoiceLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
                    {
                        //if (!AddRowInvoiceRimax(InvoiceLine, ref tableItems, fuenteDatos, DsInvoiceAR.Tables["InvcTax"]))
                        //    return false;
                        AddUnidadesRetycol(InvoiceLine, ref tableUnidades, Helpers.Fuentes.fontCustom, CustomerCheckBox06);
                    }

                    iTextSharp.text.pdf.PdfPCell LineaFinal = new iTextSharp.text.pdf.PdfPCell();
                    LineaFinal.Colspan = 6;
                    LineaFinal.Padding = 3;
                    LineaFinal.Border = 1;
                    LineaFinal.BorderColorBottom = BaseColor.WHITE;
                    LineaFinal.HorizontalAlignment = Element.ALIGN_CENTER;
                    LineaFinal.VerticalAlignment = Element.ALIGN_TOP;
                    tableUnidades.AddCell(LineaFinal);
                    return tableUnidades;
                }
                public static PdfPTable Observaciones(DataSet DsInvoiceAR)
                {
                    //Variables
                    string Valorletras = string.Empty;
                    string DspDocInvoiceAmt = "0";
                    string DspInvoiceAmt = "0";
                    string CustomerCheckBox06 = "false";
                    string DspDocSubTotal = "0";
                    string DspSubTotal = "0";
                    string Subtotal = "";

                    //---- Asigancion de vaiables
                    if (Helpers.Compartido.VerificarExistenciaColumnas(DsInvoiceAR, "InvcHead", "DspDocInvoiceAmt", 0))
                        DspDocInvoiceAmt = DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(DsInvoiceAR, "InvcHead", "DspInvoiceAmt", 0))
                        DspInvoiceAmt = DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspInvoiceAmt"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(DsInvoiceAR, "InvcHead", "DspDocSubTotal", 0))
                        DspDocSubTotal = DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubTotal"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(DsInvoiceAR, "InvcHead", "DspSubTotal", 0))
                        DspSubTotal = DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspSubTotal"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(DsInvoiceAR, "Customer", "CheckBox06", 0))
                        CustomerCheckBox06 = DsInvoiceAR.Tables["Customer"].Rows[0]["CheckBox06"].ToString();

                    if (CustomerCheckBox06 == "false")
                        Valorletras = DspInvoiceAmt;
                    else
                        Valorletras = DspDocInvoiceAmt;

                    //---
                    PdfPTable tableTotalesObs = new PdfPTable(3);

                    float[] DimencionTotalesObs = new float[3];
                    DimencionTotalesObs[0] = 2.5F;//
                    DimencionTotalesObs[1] = 0.01F;//
                    DimencionTotalesObs[2] = 1.0F;//

                    tableTotalesObs.WidthPercentage = 100;
                    tableTotalesObs.SetWidths(DimencionTotalesObs);

                    PdfPTable _tableObs = new PdfPTable(1);
                    _tableObs.WidthPercentage = 100;

                    //iTextSharp.text.pdf.PdfPCell celValorLetras = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("VALORES EN LETRA: {0}",
                    //    Nroenletras(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"].ToString())), Helpers.Fuentes.fontTitleFactura));
                    iTextSharp.text.pdf.PdfPCell celValorLetras = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("VALORES EN LETRA: {0}",
                        Metodos.Nroenletras(Valorletras)), Helpers.Fuentes.fontTitleFactura));
                    celValorLetras.Colspan = 1;
                    celValorLetras.Padding = 3;
                    //celValorLetras.Border = 0;
                    celValorLetras.BorderColorBottom = BaseColor.WHITE;
                    celValorLetras.HorizontalAlignment = Element.ALIGN_LEFT;
                    celValorLetras.VerticalAlignment = Element.ALIGN_TOP;

                    //iTextSharp.text.pdf.PdfPCell celValorAsegurado = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("VALOR ASEGURADO: {0:C2}",
                    //   decimal.Parse(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubTotal"].ToString())), fontTitle2));
                    PdfPCell celValorAsegurado = new PdfPCell(new Phrase(string.Format("VALOR ASEGURADO: {0:C2}",
                       decimal.Parse(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubTotal"].ToString())), Helpers.Fuentes.fontTitle2));
                    celValorAsegurado.Colspan = 1;
                    celValorAsegurado.Padding = 5;
                    //celValorLetras.Border = 0;
                    celValorAsegurado.BorderColorBottom = BaseColor.WHITE;
                    celValorAsegurado.HorizontalAlignment = Element.ALIGN_LEFT;
                    celValorAsegurado.VerticalAlignment = Element.ALIGN_TOP;

                    iTextSharp.text.pdf.PdfPCell celObservaciones = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("OBSERVACIONES:\n{0}",
                        DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceComment"].ToString()), Helpers.Fuentes.fontTitleFactura));
                    celObservaciones.Colspan = 1;
                    celObservaciones.Padding = 3;
                    //celValorLetras.Border = 0;
                    celObservaciones.BorderColorBottom = BaseColor.WHITE;
                    celObservaciones.HorizontalAlignment = Element.ALIGN_LEFT;
                    celObservaciones.VerticalAlignment = Element.ALIGN_TOP;

                    _tableObs.AddCell(celValorLetras);
                    _tableObs.AddCell(celEspacio3);
                    _tableObs.AddCell(celValorAsegurado);
                    _tableObs.AddCell(celEspacio3);
                    _tableObs.AddCell(celObservaciones);

                    iTextSharp.text.pdf.PdfPCell _celObs = new iTextSharp.text.pdf.PdfPCell(_tableObs);
                    _celObs.Border = 0;
                    tableTotalesObs.AddCell(_celObs);
                    //----------------------------------------------------------------------------------------------------------------------------

                    iTextSharp.text.pdf.PdfPCell _celEspacio2 = new iTextSharp.text.pdf.PdfPCell(new Phrase(" ", Helpers.Fuentes.fontTitleFactura));
                    _celEspacio2.Border = 0;
                    tableTotalesObs.AddCell(_celEspacio2);

                    //------------------------------------------------------------------------------------------

                    PdfPTable _tableTotales = new PdfPTable(2);
                    float[] _DimencionTotales = new float[2];
                    _DimencionTotales[0] = 1.5F;//
                    _DimencionTotales[1] = 2.5F;//

                    _tableTotales.WidthPercentage = 100;
                    _tableTotales.SetWidths(_DimencionTotales);

                    iTextSharp.text.pdf.PdfPCell _celTextSubTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase("SUBTOTAL:", Helpers.Fuentes.fontTitleFactura));
                    _celTextSubTotal.Colspan = 1;
                    _celTextSubTotal.Padding = 3;
                    _celTextSubTotal.PaddingBottom = 5;
                    _celTextSubTotal.Border = 0;
                    _celTextSubTotal.BorderWidthRight = 1;
                    _celTextSubTotal.BorderColorBottom = BaseColor.WHITE;
                    _celTextSubTotal.HorizontalAlignment = Element.ALIGN_LEFT;
                    _celTextSubTotal.VerticalAlignment = Element.ALIGN_TOP;
                    _tableTotales.AddCell(_celTextSubTotal);

                    //PdfPCell _celSubTotal = new PdfPCell(new Phrase(string.Format("{0:C2}", decimal.Parse(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubTotal"].ToString())), fontTitle2));
                    PdfPCell _celSubTotal = new PdfPCell(new Phrase(string.Format("{0:C2}", decimal.Parse(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubTotal"].ToString())), Helpers.Fuentes.fontTitle2));
                    _celSubTotal.Colspan = 1;
                    _celSubTotal.Padding = 3;
                    _celSubTotal.PaddingBottom = 5;
                    _celSubTotal.Border = 0;
                    _celSubTotal.BorderColorBottom = BaseColor.WHITE;
                    _celSubTotal.HorizontalAlignment = Element.ALIGN_RIGHT;
                    _celSubTotal.VerticalAlignment = Element.ALIGN_TOP;
                    _tableTotales.AddCell(_celSubTotal);

                    //iTextSharp.text.pdf.PdfPCell _celTextDescuentos = new iTextSharp.text.pdf.PdfPCell(new Phrase("DESCUENTOS:", Helpers.Fuentes.fontTitleFactura));
                    //_celTextDescuentos.Colspan = 1;
                    //_celTextDescuentos.Padding = 3;
                    //_celTextDescuentos.PaddingBottom = 5;
                    //_celTextDescuentos.Border = 0;
                    //_celTextDescuentos.BorderWidthRight = 1;
                    //_celTextDescuentos.BorderColorBottom = BaseColor.WHITE;
                    //_celTextDescuentos.HorizontalAlignment = Element.ALIGN_LEFT;
                    //_celTextDescuentos.VerticalAlignment = Element.ALIGN_TOP;
                    //_tableTotales.AddCell(_celTextDescuentos);

                    //PdfPCell _celDescuentos = new PdfPCell(new Phrase((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubTotal"], Helpers.Fuentes.fontTitle2));
                    //_celDescuentos.Colspan = 1;
                    //_celDescuentos.Padding = 3;
                    //_celDescuentos.PaddingBottom = 5;
                    //_celDescuentos.Border = 0;
                    //_celDescuentos.BorderColorBottom = BaseColor.WHITE;
                    //_celDescuentos.HorizontalAlignment = Element.ALIGN_RIGHT;
                    //_celDescuentos.VerticalAlignment = Element.ALIGN_TOP;
                    //_tableTotales.AddCell(_celDescuentos);

                    iTextSharp.text.pdf.PdfPCell _celTextSubTotal2 = new iTextSharp.text.pdf.PdfPCell(new Phrase("  ", Helpers.Fuentes.fontTitleFactura));
                    _celTextSubTotal2.Colspan = 1;
                    _celTextSubTotal2.Padding = 3;
                    _celTextSubTotal2.PaddingBottom = 5;
                    _celTextSubTotal2.Border = 0;
                    _celTextSubTotal2.BorderWidthRight = 1;
                    _celTextSubTotal2.BorderColorBottom = BaseColor.WHITE;
                    _celTextSubTotal2.HorizontalAlignment = Element.ALIGN_LEFT;
                    _celTextSubTotal2.VerticalAlignment = Element.ALIGN_TOP;
                    _tableTotales.AddCell(_celTextSubTotal2);

                    PdfPCell _celSubTotal2 = new PdfPCell(new Phrase(" ", Helpers.Fuentes.fontTitle2));
                    _celSubTotal2.Colspan = 1;
                    _celSubTotal2.Padding = 3;
                    _celSubTotal2.PaddingBottom = 5;
                    _celSubTotal2.Border = 0;
                    _celSubTotal2.BorderColorBottom = BaseColor.WHITE;
                    _celSubTotal2.HorizontalAlignment = Element.ALIGN_RIGHT;
                    _celSubTotal2.VerticalAlignment = Element.ALIGN_TOP;
                    _tableTotales.AddCell(_celSubTotal2);

                    iTextSharp.text.pdf.PdfPCell _celTextIva = new iTextSharp.text.pdf.PdfPCell(new Phrase("IVA:", Helpers.Fuentes.fontTitleFactura));
                    _celTextIva.Colspan = 1;
                    _celTextIva.Padding = 3;
                    _celTextIva.PaddingBottom = 5;
                    _celTextIva.Border = 0;
                    _celTextIva.BorderWidthRight = 1;
                    //_celTextIva.BorderColorBottom = BaseColor.BLACK;
                    _celTextIva.HorizontalAlignment = Element.ALIGN_LEFT;
                    _celTextIva.VerticalAlignment = Element.ALIGN_TOP;
                    _tableTotales.AddCell(_celTextIva);

                    //PdfPCell _celIva = new PdfPCell(new Phrase(GetValImpuestoByID("01", DsInvoiceAR).ToString("C"), Helpers.Fuentes.fontTitle2));
                    PdfPCell _celIva = new PdfPCell(new Phrase(Metodos.GetValImpuestoByID("01", DsInvoiceAR).ToString("C"), Helpers.Fuentes.fontTitle2));
                    _celIva.Colspan = 1;
                    _celIva.Padding = 3;
                    _celIva.PaddingBottom = 5;
                    _celIva.Border = 0;
                    //_celIva.BorderColorBottom = BaseColor.BLACK;
                    _celIva.HorizontalAlignment = Element.ALIGN_RIGHT;
                    _celIva.VerticalAlignment = Element.ALIGN_TOP;
                    _tableTotales.AddCell(_celIva);

                    iTextSharp.text.pdf.PdfPCell _celTextTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase("TOTAL:", Helpers.Fuentes.fontTitleFactura));
                    _celTextTotal.Colspan = 1;
                    _celTextTotal.Padding = 3;
                    _celTextTotal.PaddingBottom = 5;
                    _celTextTotal.Border = 0;
                    _celTextTotal.BorderWidthRight = 1;
                    _celTextTotal.BorderWidthTop = 1;
                    _celTextTotal.HorizontalAlignment = Element.ALIGN_LEFT;
                    _celTextTotal.VerticalAlignment = Element.ALIGN_TOP;
                    _tableTotales.AddCell(_celTextTotal);

                    iTextSharp.text.pdf.PdfPCell _celTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:C2}",
                        decimal.Parse(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"].ToString())), Helpers.Fuentes.fontTitleFactura));
                    _celTotal.Colspan = 1;
                    _celTotal.Padding = 3;
                    _celTotal.PaddingBottom = 5;
                    _celTotal.Border = 0;
                    _celTotal.BorderWidthTop = 1;
                    _celTotal.HorizontalAlignment = Element.ALIGN_RIGHT;
                    _celTotal.VerticalAlignment = Element.ALIGN_TOP;
                    _tableTotales.AddCell(_celTotal);

                    iTextSharp.text.pdf.PdfPCell _celTotales = new iTextSharp.text.pdf.PdfPCell(_tableTotales);
                    tableTotalesObs.AddCell(_celTotales);
                    return tableTotalesObs;
                }
                public static PdfPTable Resolucion(DataSet DsInvoiceAR)
                {
                    PdfPTable tableResolucion = new PdfPTable(1);
                    tableResolucion.WidthPercentage = 100;
                    string strResolucion = (string)DsInvoiceAR.Tables["Company"].Rows[0]["Character02"];
                    iTextSharp.text.pdf.PdfPCell _celResolucion = new iTextSharp.text.pdf.PdfPCell(new Phrase(strResolucion, Helpers.Fuentes.fontTitleFactura));
                    _celResolucion.Colspan = 1;
                    _celResolucion.Padding = 3;
                    //_celResolucion.Border = 0;
                    _celResolucion.HorizontalAlignment = Element.ALIGN_JUSTIFIED;
                    _celResolucion.VerticalAlignment = Element.ALIGN_TOP;
                    tableResolucion.AddCell(_celResolucion);
                    return tableResolucion;
                }
                public static PdfPTable Firmas(DataSet DsInvoiceAR)
                {
                    PdfPTable tableFirmas = new PdfPTable(5);

                    float[] DimencionFirmas = new float[5];
                    DimencionFirmas[0] = 1.0F;//
                    DimencionFirmas[1] = 0.02F;//
                    DimencionFirmas[2] = 1.0F;//
                    DimencionFirmas[3] = 0.02F;//
                    DimencionFirmas[4] = 1.4F;//

                    tableFirmas.WidthPercentage = 100;
                    tableFirmas.SetWidths(DimencionFirmas);

                    PdfPTable tableDespacahdoPor = new PdfPTable(1);
                    tableDespacahdoPor.WidthPercentage = 100;
                    iTextSharp.text.pdf.PdfPCell celDespachadoPor = new iTextSharp.text.pdf.PdfPCell(new Phrase("\n\n\n\n\n\n\n\n usuario que genera la factura:" +
                        "\n______________________________________\n\nDESPACHADO POR", Helpers.Fuentes.fontTitleFactura));
                    celDespachadoPor.Colspan = 1;
                    celDespachadoPor.Padding = 3;
                    celDespachadoPor.HorizontalAlignment = Element.ALIGN_CENTER;
                    celDespachadoPor.VerticalAlignment = Element.ALIGN_TOP;
                    tableDespacahdoPor.AddCell(celDespachadoPor);

                    iTextSharp.text.pdf.PdfPCell _celDespachadoPor = new iTextSharp.text.pdf.PdfPCell(tableDespacahdoPor);
                    tableFirmas.AddCell(_celDespachadoPor);
                    //----------------------------------------------------------------------------------------------------------------------------

                    tableFirmas.AddCell(celEspacio2);

                    //------------------------------------------------------------------------------------------

                    PdfPTable tableFirmaConductor = new PdfPTable(1);
                    tableFirmaConductor.WidthPercentage = 100;

                    string Lote = DsInvoiceAR.Tables["InvcDtl"].Rows[0]["LotNum"].ToString();
                    decimal ExchangeRate = Convert.ToDecimal(DsInvoiceAR.Tables["InvcHead"].Rows[0]["ExchangeRate"]);
                    tableFirmaConductor.AddCell(new PdfPCell(new Phrase($"Lote: {Lote}", Helpers.Fuentes.fontTitle2)));

                    tableFirmaConductor.AddCell(new PdfPCell(new Phrase($"TRM: {string.Format("{0:N2}", ExchangeRate)}", Helpers.Fuentes.fontTitle2)));

                    //iTextSharp.text.pdf.PdfPCell celFirmaConductor = new iTextSharp.text.pdf.PdfPCell(new Phrase("\n\n\n\n\n\n\n\n\n " +
                    //    "______________________________________" +
                    //    "\n\nFIRMA DEL CONDUCTOR", fontTitleFactura));
                    //celFirmaConductor.Colspan = 1;
                    //celFirmaConductor.Padding = 3;
                    ////celFirmaConductor.Border = 0;
                    //celFirmaConductor.HorizontalAlignment = Element.ALIGN_CENTER;
                    //celFirmaConductor.VerticalAlignment = Element.ALIGN_TOP;
                    //tableFirmaConductor.AddCell(celFirmaConductor);

                    iTextSharp.text.pdf.PdfPCell _celFirmaConductor = new iTextSharp.text.pdf.PdfPCell(tableFirmaConductor);
                    tableFirmas.AddCell(_celFirmaConductor);

                    //-------------------------------------------------------------------------------------
                    tableFirmas.AddCell(celEspacio2);
                    //-------------------------------------------------------------------------------------

                    PdfPTable tableSelloCliente = new PdfPTable(1);
                    //tableSelloCliente.WidthPercentage = 100;

                    iTextSharp.text.pdf.PdfPCell cel1SelloCliente = new iTextSharp.text.pdf.PdfPCell(new Phrase("Apreciado cliente este documento " +
                        "no es endolsable", Helpers.Fuentes.fontTitleFactura));
                    cel1SelloCliente.Colspan = 1;
                    cel1SelloCliente.Padding = 5;
                    cel1SelloCliente.Border = 1;
                    cel1SelloCliente.BorderWidthBottom = 1;
                    cel1SelloCliente.HorizontalAlignment = Element.ALIGN_LEFT;
                    cel1SelloCliente.VerticalAlignment = Element.ALIGN_TOP;
                    tableSelloCliente.AddCell(cel1SelloCliente);

                    iTextSharp.text.pdf.PdfPCell cel2SelloFechaRecibido = new iTextSharp.text.pdf.PdfPCell(new Phrase("Fecha de recibido:", Helpers.Fuentes.fontTitleFactura));

                    cel2SelloFechaRecibido.Colspan = 1;
                    cel2SelloFechaRecibido.Padding = 3;
                    cel2SelloFechaRecibido.Border = 0;
                    cel2SelloFechaRecibido.HorizontalAlignment = Element.ALIGN_LEFT;
                    cel2SelloFechaRecibido.VerticalAlignment = Element.ALIGN_TOP;
                    tableSelloCliente.AddCell(cel2SelloFechaRecibido);

                    iTextSharp.text.pdf.PdfPCell cel2SelloNombre = new iTextSharp.text.pdf.PdfPCell(new Phrase("Nombre:", Helpers.Fuentes.fontTitleFactura));

                    cel2SelloNombre.Colspan = 1;
                    cel2SelloNombre.Padding = 3;
                    cel2SelloNombre.Border = 0;
                    cel2SelloNombre.HorizontalAlignment = Element.ALIGN_LEFT;
                    cel2SelloNombre.VerticalAlignment = Element.ALIGN_TOP;
                    tableSelloCliente.AddCell(cel2SelloNombre);

                    iTextSharp.text.pdf.PdfPCell cel2SelloId = new iTextSharp.text.pdf.PdfPCell(new Phrase("Identificacion:", Helpers.Fuentes.fontTitleFactura));

                    cel2SelloId.Colspan = 1;
                    cel2SelloId.Padding = 3;
                    cel2SelloId.Border = 0;
                    cel2SelloId.HorizontalAlignment = Element.ALIGN_LEFT;
                    cel2SelloId.VerticalAlignment = Element.ALIGN_TOP;
                    tableSelloCliente.AddCell(cel2SelloId);

                    iTextSharp.text.pdf.PdfPCell cel2SelloFirma = new iTextSharp.text.pdf.PdfPCell(new Phrase("Firma:\n", Helpers.Fuentes.fontTitleFactura));

                    cel2SelloFirma.Colspan = 1;
                    cel2SelloFirma.Padding = 3;
                    cel2SelloFirma.Border = 0;
                    cel2SelloFirma.HorizontalAlignment = Element.ALIGN_LEFT;
                    cel2SelloFirma.VerticalAlignment = Element.ALIGN_TOP;
                    tableSelloCliente.AddCell(cel2SelloFirma);

                    iTextSharp.text.pdf.PdfPCell cel3SelloCliente = new iTextSharp.text.pdf.PdfPCell(new Phrase("______________________________________________________\n\n" +
                        "RECIBIDO CONFORME FIRMA Y SELLO DEL CLIENTE", Helpers.Fuentes.fontTitleFactura));

                    cel3SelloCliente.Colspan = 1;
                    cel3SelloCliente.Padding = 3;
                    cel3SelloCliente.Border = 0;
                    cel3SelloCliente.HorizontalAlignment = Element.ALIGN_CENTER;
                    cel3SelloCliente.VerticalAlignment = Element.ALIGN_TOP;
                    tableSelloCliente.AddCell(cel3SelloCliente);

                    iTextSharp.text.pdf.PdfPCell _celSelloCliente = new iTextSharp.text.pdf.PdfPCell(tableSelloCliente);
                    //_celSelloCliente.Border = 0;
                    //_celSelloCliente.BorderColor = BaseColor.WHITE;
                    tableFirmas.AddCell(_celSelloCliente);
                    return tableFirmas;
                }
                #endregion
                private static void AddUnidadesRetycol(DataRow dataLine, ref PdfPTable pdfPTable, iTextSharp.text.Font font, string CustomerCheckBox06)
                {

                    iTextSharp.text.pdf.PdfPCell celCodigo = new iTextSharp.text.pdf.PdfPCell(new Phrase((string)dataLine["PartNum"], font));
                    celCodigo.Colspan = 1;
                    celCodigo.Padding = 3;
                    //celCodigo.Border = 0;
                    celCodigo.BorderWidthBottom = 0;
                    celCodigo.BorderWidthTop = 0;
                    celCodigo.BorderWidthRight = 0;
                    celCodigo.BorderColorBottom = BaseColor.WHITE;
                    celCodigo.HorizontalAlignment = Element.ALIGN_CENTER;
                    celCodigo.VerticalAlignment = Element.ALIGN_TOP;
                    pdfPTable.AddCell(celCodigo);

                    iTextSharp.text.pdf.PdfPCell celDescripcion = new iTextSharp.text.pdf.PdfPCell(new Phrase(dataLine["LineDesc"].ToString(), font));
                    celDescripcion.Colspan = 1;
                    celDescripcion.Padding = 3;
                    //celDescripcion.Border = 0;
                    celDescripcion.BorderWidthBottom = 0;
                    celDescripcion.BorderWidthTop = 0;
                    celDescripcion.BorderWidthRight = 0;
                    celDescripcion.BorderColorBottom = BaseColor.WHITE;
                    celDescripcion.HorizontalAlignment = Element.ALIGN_CENTER;
                    celDescripcion.VerticalAlignment = Element.ALIGN_TOP;
                    pdfPTable.AddCell(celDescripcion);

                    iTextSharp.text.pdf.PdfPCell celCantidad = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:N0}", dataLine["SellingShipQty"].ToString()), font));
                    celCantidad.Colspan = 1;
                    celCantidad.Padding = 3;
                    //celCantidad.Border = 0;
                    celCantidad.BorderWidthBottom = 0;
                    celCantidad.BorderWidthTop = 0;
                    celCantidad.BorderWidthRight = 0;
                    celCantidad.BorderColorBottom = BaseColor.WHITE;
                    celCantidad.HorizontalAlignment = Element.ALIGN_CENTER;
                    celCantidad.VerticalAlignment = Element.ALIGN_TOP;
                    pdfPTable.AddCell(celCantidad);

                    iTextSharp.text.pdf.PdfPCell celUnidad = new iTextSharp.text.pdf.PdfPCell(new Phrase((string)dataLine["SalesUM"], font));
                    celUnidad.Colspan = 1;
                    celUnidad.Padding = 3;
                    //celUnidad.Border = 0;
                    celUnidad.BorderWidthBottom = 0;
                    celUnidad.BorderWidthTop = 0;
                    celUnidad.BorderWidthRight = 0;
                    celUnidad.BorderColorBottom = BaseColor.WHITE;
                    celUnidad.HorizontalAlignment = Element.ALIGN_CENTER;
                    celUnidad.VerticalAlignment = Element.ALIGN_TOP;
                    pdfPTable.AddCell(celUnidad);

                    iTextSharp.text.pdf.PdfPCell celValorUnitario = new PdfPCell();
                    if(CustomerCheckBox06 == "false")
                        celValorUnitario = new PdfPCell(new Phrase(string.Format("{0:C2}",Convert.ToDecimal(dataLine["UnitPrice"].ToString())), font));
                    else
                        celValorUnitario = new PdfPCell(new Phrase(string.Format("{0:C2}", Convert.ToDecimal(dataLine["DocUnitPrice"].ToString())), font));

                    celValorUnitario.Colspan = 1;
                    celValorUnitario.Padding = 3;
                    //celValorUnitario.Border = 0;
                    celValorUnitario.BorderWidthBottom = 0;
                    celValorUnitario.BorderWidthTop = 0;
                    celValorUnitario.BorderWidthRight = 0;
                    celValorUnitario.BorderColorBottom = BaseColor.WHITE;
                    celValorUnitario.HorizontalAlignment = Element.ALIGN_CENTER;
                    celValorUnitario.VerticalAlignment = Element.ALIGN_TOP;
                    pdfPTable.AddCell(celValorUnitario);

                    //decimal cantidad = 0, unitPrice = 0, Total = 0;
                    //cantidad = decimal.Parse(dataLine["SellingShipQty"].ToString());
                    //unitPrice = decimal.Parse(dataLine["DocExtPrice"].ToString());
                    //Total = cantidad * unitPrice;
                    iTextSharp.text.pdf.PdfPCell celValorTotal = new PdfPCell();
                    if (CustomerCheckBox06 == "false")
                        celValorTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:C2}",decimal.Parse(dataLine["ExtPrice"].ToString())), font));
                    else
                        celValorTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:C2}", decimal.Parse(dataLine["DocExtPrice"].ToString())), font));
                    celValorTotal.Colspan = 1;
                    celValorTotal.Padding = 3;
                    //celValorTotal.Border = 0;
                    celValorTotal.BorderWidthBottom = 0;
                    celValorTotal.BorderWidthTop = 0;
                    celValorTotal.BorderColorBottom = BaseColor.WHITE;
                    celValorTotal.HorizontalAlignment = Element.ALIGN_CENTER;
                    celValorTotal.VerticalAlignment = Element.ALIGN_TOP;
                    pdfPTable.AddCell(celValorTotal);
                    //-------------Comentario final, en caso de tenerlo
                    string Comentario = dataLine["InvoiceComment"].ToString();
                    if (!string.IsNullOrEmpty(Comentario))
                    {
                        PdfPCell celComentario = new PdfPCell(new Phrase(Comentario, font));
                        celComentario.Border = 1;
                        celComentario.HorizontalAlignment = Element.ALIGN_LEFT;
                        celComentario.Colspan = 6;
                        pdfPTable.AddCell(celComentario);
                    }
                }
            }
        }
        #endregion
    }
}
