﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
//Referenciar y usar.
using System.Data;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Web;
using System.Windows.Forms;

namespace RulesServicesDIAN2.Adquiriente
{
    public partial class pdfEstandarAR
    {
        private bool AddUnidadesInterbel(DataRow dataLine, ref PdfPTable table, iTextSharp.text.Font fontTitleFactura, DataSet dataSet)
        {
            try
            {

                strError += "Add PartNum";
                PdfPCell celValL = new PdfPCell(new Phrase((string)dataLine["PartNum"], fontTitleFactura));
                celValL.Colspan = 1;
                celValL.Padding = 2;
                celValL.Border = PdfPCell.LEFT_BORDER | PdfPCell.RIGHT_BORDER;
                celValL.HorizontalAlignment = Element.ALIGN_CENTER;
                celValL.VerticalAlignment = Element.ALIGN_CENTER;
                table.AddCell(celValL);

                strError += "Add LineDesc";
                PdfPCell celValreferencia = new PdfPCell(new Phrase((string)dataLine["LineDesc"], fontTitleFactura));
                celValreferencia.Colspan = 1;
                celValreferencia.Padding = 2;
                celValreferencia.MinimumHeight = 20;
                celValreferencia.Border = PdfPCell.RIGHT_BORDER;
                celValreferencia.HorizontalAlignment = Element.ALIGN_CENTER;
                celValreferencia.VerticalAlignment = Element.ALIGN_CENTER;
                table.AddCell(celValreferencia);

                strError += "Add SellingShipQty";
                PdfPCell celValCodigo = new PdfPCell(new Phrase((string)dataLine["SellingShipQty"], fontTitleFactura));
                celValCodigo.Colspan = 1;
                celValCodigo.Padding = 2;
                celValCodigo.Border = PdfPCell.RIGHT_BORDER;
                celValCodigo.HorizontalAlignment = Element.ALIGN_CENTER;
                celValCodigo.VerticalAlignment = Element.ALIGN_CENTER;
                table.AddCell(celValCodigo);

                strError += "Add IVA";
                PdfPCell celValOC = new PdfPCell(new Phrase(GetPercentIdImpDIAN((string)dataLine["InvoiceNum"], (string)dataLine["InvoiceLine"], dataSet.Tables["InvcTax"]), fontTitleFactura));
                celValOC.Colspan = 1;
                celValOC.Padding = 2;
                celValOC.Border = PdfPCell.RIGHT_BORDER;
                celValOC.HorizontalAlignment = Element.ALIGN_CENTER;
                celValOC.VerticalAlignment = Element.ALIGN_CENTER;
                table.AddCell(celValOC);

                strError += "Add DocUnitPrice";
                PdfPCell celValDescripcion = new PdfPCell(new Phrase(
                    decimal.Parse((string)dataLine["DocUnitPrice"]).ToString("N2"), fontTitleFactura));
                celValDescripcion.Colspan = 1;
                celValDescripcion.Padding = 2;
                celValDescripcion.Border = PdfPCell.RIGHT_BORDER;
                celValDescripcion.HorizontalAlignment = Element.ALIGN_CENTER;
                celValDescripcion.VerticalAlignment = Element.ALIGN_CENTER;
                table.AddCell(celValDescripcion);

                strError += "Add DspDocExtPrice";
                PdfPCell celValUnidad = new PdfPCell(new Phrase(decimal.Parse((string)dataLine["DspDocExtPrice"]).ToString("N2"), fontTitleFactura));
                celValUnidad.Colspan = 1;
                celValUnidad.Padding = 2;
                celValUnidad.MinimumHeight = 20;
                celValUnidad.Border = PdfPCell.RIGHT_BORDER;
                celValUnidad.HorizontalAlignment = Element.ALIGN_CENTER;
                celValUnidad.VerticalAlignment = Element.ALIGN_CENTER;
                table.AddCell(celValUnidad);

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return false;
            }
        }

        #region Factura de venta Inter Bel

        public bool FacturaVentaInterbel(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {
           
            NomArchivo = string.Empty;

            string Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            string RutaImg = $@"{AppDomain.CurrentDomain.BaseDirectory}\logoInterBel.png";
            string Rutacertificado = $@"{AppDomain.CurrentDomain.BaseDirectory}\logoInterBel.png";

            NomArchivo = NombreInvoice + ".pdf";

            try
            {
                #region Head
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.
                Document document = new Document(iTextSharp.text.PageSize.LETTER);
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;

                // step 3: we open the document     
                document.Open();

                PdfDiv divEspacio = new PdfDiv();
                divEspacio.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio.Height = 10;
                divEspacio.Width = 130;

                PdfDiv divEspacio2 = new PdfDiv();
                divEspacio2.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio2.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio2.Height = 2;
                divEspacio2.Width = 130;

                //FUENTES----------------------------------------------------------------------------------------------------------
                iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle2 = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustom = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustom2 = FontFactory.GetFont(FontFactory.HELVETICA, 5, iTextSharp.text.Font.NORMAL);
                //FUENTES---------------------------------------------------------------------------------------------------------

                ///objeto para el borde 
                RoundRectangle CelEventBorderRound = new RoundRectangle();

                System.Drawing.Image Logo;
                System.Drawing.Image LogoBanner = null;
                var request = WebRequest.Create(RutaImg);
                using (var response = request.GetResponse())
                using (var stream = response.GetResponseStream())
                {
                    Logo = Bitmap.FromStream(stream);
                }
                var requestBanner = WebRequest.Create(RutaImg);
                using (var responseBanner = requestBanner.GetResponse())
                using (var streamBanner = responseBanner.GetResponseStream())
                {
                    LogoBanner = Bitmap.FromStream(streamBanner);
                }


                System.Drawing.Image Logo2;
                System.Drawing.Image LogoBanner2 = null;
                var request2 = WebRequest.Create(Rutacertificado);
                using (var response2 = request2.GetResponse())
                using (var stream2 = response2.GetResponseStream())
                {
                    Logo2 = Bitmap.FromStream(stream2);
                }
                var requestBanner2 = WebRequest.Create(Rutacertificado);
                using (var responseBanner2 = requestBanner2.GetResponse())
                using (var streamBanner2 = responseBanner2.GetResponseStream())
                {
                    LogoBanner2 = Bitmap.FromStream(streamBanner2);
                }


                //Logos--------------------------------
                iTextSharp.text.Image LogoPdf3 = iTextSharp.text.Image.GetInstance(LogoBanner2, BaseColor.WHITE);
                LogoPdf3.ScaleAbsolute(90f, 90f);
                LogoPdf3.Border = 0;


                iTextSharp.text.Image LogoPdf2 = iTextSharp.text.Image.GetInstance(LogoBanner, BaseColor.WHITE);
                LogoPdf2.ScaleAbsolute(150, 150);
                LogoPdf2.Border = 0;

                /*CELL QR*/
                iTextSharp.text.Image QRPdf = iTextSharp.text.Image.GetInstance(QRInvoice, BaseColor.WHITE);
                QRPdf.ScaleAbsolute(80f, 80f);
                iTextSharp.text.pdf.PdfPCell celImgQR = new iTextSharp.text.pdf.PdfPCell(QRPdf);
                celImgQR.HorizontalAlignment = Element.ALIGN_CENTER;
                celImgQR.Border = 0;
                celImgQR.PaddingTop = 2;


                PdfPTable espacio = new PdfPTable(1);
                espacio.WidthPercentage = 100;
                PdfPCell salto = new PdfPCell() { Border = 0, MinimumHeight = 10, };
                espacio.AddCell(salto);

                #endregion

                #region Header 


                PdfPTable tabla_borde = new PdfPTable(1);
                tabla_borde.WidthPercentage = 100;
                PdfPCell tabla1 = new PdfPCell()
                {
                    CellEvent = CelEventBorderRound,
                    Border = 0,
                    MinimumHeight = 15,
                };


                //encabezado de la factura 
                PdfPTable Header = new PdfPTable(3);
                Header.WidthPercentage = 100;

                PdfPCell logo = new PdfPCell() { };
                logo.Border = PdfPCell.RIGHT_BORDER;
                PdfPCell info = new PdfPCell(new Phrase((DsInvoiceAR.Tables["Company"].Rows[0]["Name"].ToString() + "\n\nNIT: " + DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"].ToString()))) { HorizontalAlignment = Element.ALIGN_CENTER, Border = 0, };
                info.Border = PdfPCell.RIGHT_BORDER;
                PdfPCell numero_factura = new PdfPCell(new Phrase("FACTURA DE VENTA\n\n" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"].ToString())) { HorizontalAlignment = Element.ALIGN_CENTER, Border = 0, };

                logo.AddElement(LogoPdf2);

                Header.AddCell(logo);
                Header.AddCell(info);
                Header.AddCell(numero_factura);


                tabla1.AddElement(Header);
                tabla_borde.AddCell(tabla1);


                PdfPTable tabla_borde2 = new PdfPTable(1);
                tabla_borde2.WidthPercentage = 100;
                PdfPCell tabla2 = new PdfPCell()
                {
                    CellEvent = CelEventBorderRound,
                    Border = 0,
                    MinimumHeight = 15,
                };



                //cliente y informacion de la factura 
                PdfPTable cliente = new PdfPTable(new float[] { 3.5f, 1.5f, 3.0f });
                cliente.WidthPercentage = 100;

                PdfPCell señores = new PdfPCell(new Phrase("")) { MinimumHeight = 60, Border = 0, };

                PdfPTable SEÑORES_ = new PdfPTable(1);
                SEÑORES_.WidthPercentage = 100;
                PdfPCell señores_f = new PdfPCell(new Phrase("SEÑORES: " + DsInvoiceAR.Tables["Customer"].Rows[0]["Name"].ToString(), fontTitle2)) { Border = 0, };//nombre clioente 
                SEÑORES_.AddCell(señores_f);
                señores.AddElement(SEÑORES_);

                PdfPTable NIT_ = new PdfPTable(1);
                NIT_.WidthPercentage = 100;
                PdfPCell NIT_f = new PdfPCell(new Phrase("NIT: " + DsInvoiceAR.Tables["Customer"].Rows[0]["CustID"].ToString(), fontTitle2)) { Border = 0, };//NIT
                NIT_.AddCell(NIT_f);
                señores.AddElement(NIT_);

                PdfPTable DIRECCION_ = new PdfPTable(1);
                DIRECCION_.WidthPercentage = 100;
                PdfPCell DIRECCION_f = new PdfPCell(new Phrase("DIRECCION: " + DsInvoiceAR.Tables["Customer"].Rows[0]["Address1"].ToString(), fontTitle2)) { Border = 0, };//DIRECCION
                DIRECCION_.AddCell(DIRECCION_f);
                señores.AddElement(DIRECCION_);

                PdfPTable CIUDAD_ = new PdfPTable(1);
                CIUDAD_.WidthPercentage = 100;
                PdfPCell CIUDAD_f = new PdfPCell(new Phrase("CIUDAD: " + DsInvoiceAR.Tables["Customer"].Rows[0]["City"].ToString(), fontTitle2)) { Border = 0, };//VIUDAD
                CIUDAD_.AddCell(CIUDAD_f);
                señores.AddElement(CIUDAD_);

                PdfPTable TELEFONO_ = new PdfPTable(1);
                TELEFONO_.WidthPercentage = 100;
                PdfPCell TELEFONO_f = new PdfPCell(new Phrase("TELEFONO: " + DsInvoiceAR.Tables["Customer"].Rows[0]["PhoneNum"].ToString(), fontTitle2)) { Border = 0, };//TELEFONO 
                TELEFONO_.AddCell(TELEFONO_f);
                señores.AddElement(TELEFONO_);

                PdfPTable Cufe = new PdfPTable(1);
                Cufe.WidthPercentage = 100;
                PdfPCell cufeVal = new PdfPCell(new Phrase("CUFE: " + CUFE, fontTitle2)) { Border = 0, };
                Cufe.AddCell(cufeVal);
                señores.AddElement(Cufe);

                PdfPCell info_factura = new PdfPCell();

                PdfPTable FECHA_ = new PdfPTable(2);
                FECHA_.WidthPercentage = 100;
                PdfPCell FECHA_f = new PdfPCell(new Phrase("FECHA: " + DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).ToString("dd/MM/yyyy"), fontTitle2)) { MinimumHeight = 15, };//FECHA 
                PdfPCell VENCE_f = new PdfPCell(new Phrase("VENCE: " + DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"]).ToString("dd/MM/yyyy"), fontTitle2)) { MinimumHeight = 15, };//VENCE
                FECHA_.AddCell(FECHA_f);
                FECHA_.AddCell(VENCE_f);
                info_factura.AddElement(FECHA_);



                PdfPTable VENDEDOR_ = new PdfPTable(1);
                VENDEDOR_.WidthPercentage = 100;
                PdfPCell VENDEDOR_f = new PdfPCell(new Phrase("VENDEDOR: " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["SalesRepName1"].ToString(), fontTitle2)) { MinimumHeight = 15, };//VENDEDOR
                VENDEDOR_.AddCell(VENDEDOR_f);
                info_factura.AddElement(VENDEDOR_);

                PdfPTable FORMA_ = new PdfPTable(1);
                FORMA_.WidthPercentage = 100;
                PdfPCell FORMA_f = new PdfPCell(new Phrase("FORMA DE PAGO: " + DsInvoiceAR.Tables["Customer"].Rows[0]["TermsDescription"].ToString(), fontTitle2)) { MinimumHeight = 15, };//FORMA DE PAGO
                FORMA_.AddCell(FORMA_f);
                info_factura.AddElement(FORMA_);

                PdfPTable ff_ = new PdfPTable(1);
                ff_.WidthPercentage = 100;
                PdfPCell f_f = new PdfPCell(new Phrase(DsInvoiceAR.Tables["Company"].Rows[0]["Address1"].ToString(), fontTitle2)) { MinimumHeight = 15, };
                ff_.AddCell(f_f);
                info_factura.AddElement(ff_);

                PdfPTable leyenda1 = new PdfPTable(1);
                leyenda1.WidthPercentage = 100;
                PdfPCell leyendaval = new PdfPCell(new Phrase("Representación gráfica factura electrónica", fontTitle2)) { MinimumHeight = 15, };
                leyenda1.AddCell(leyendaval);
                info_factura.AddElement(leyenda1);

                cliente.AddCell(señores);
                cliente.AddCell(celImgQR);
                cliente.AddCell(info_factura);

                tabla2.AddElement(cliente);
                tabla_borde2.AddCell(tabla2);

                #endregion

                #region Body
                PdfPTable tabla_borde3 = new PdfPTable(1);
                tabla_borde3.WidthPercentage = 100;
                PdfPCell tabla3 = new PdfPCell()
                {
                    CellEvent = CelEventBorderRound,
                    Border = 0,
                    MinimumHeight = 15,
                };

                PdfPTable Body = new PdfPTable(new float[] { 0.8f, 3.2f, 0.7f, 0.5f, 1.0f, 1.0f });
                Body.WidthPercentage = 100;
                PdfPCell referencia = new PdfPCell(new Phrase("REFERENCIA", fontTitle)) { HorizontalAlignment = Element.ALIGN_CENTER, Border = 0, };
                PdfPCell DESCRIPCION = new PdfPCell(new Phrase("DESCRIPCION", fontTitle)) { HorizontalAlignment = Element.ALIGN_CENTER, Border = 0, };
                PdfPCell CANT = new PdfPCell(new Phrase("CANT", fontTitle)) { HorizontalAlignment = Element.ALIGN_CENTER, Border = 0, };
                PdfPCell IVA = new PdfPCell(new Phrase("IVA", fontTitle)) { HorizontalAlignment = Element.ALIGN_CENTER, Border = 0, };
                PdfPCell V_UNIDAD = new PdfPCell(new Phrase("V.UNIDAD", fontTitle)) { HorizontalAlignment = Element.ALIGN_CENTER, Border = 0, };
                PdfPCell V_TOTAL = new PdfPCell(new Phrase("V.TOTAL", fontTitle)) { HorizontalAlignment = Element.ALIGN_CENTER, Border = 0, };


                Body.AddCell(referencia);
                Body.AddCell(DESCRIPCION);
                Body.AddCell(CANT);
                Body.AddCell(IVA);
                Body.AddCell(V_UNIDAD);
                Body.AddCell(V_TOTAL);

                tabla3.AddElement(Body);
                tabla_borde3.AddCell(tabla3);

                //contador para las cendas 
                PdfPTable tableUnidades = new PdfPTable(new float[] { 0.8f, 3.2f, 0.7f, 0.5f, 1.0f, 1.0f });
                tableUnidades.WidthPercentage = 100;

                decimal totalDescuento = 0;
                foreach (DataRow InvoiceLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
                {
                    totalDescuento += decimal.Parse((string)InvoiceLine["DspDocLessDiscount"]);
                    if (!AddUnidadesInterbel(InvoiceLine, ref tableUnidades, fontCustom, DsInvoiceAR))
                        return false;
                }

                PdfPCell LineaFinal = new PdfPCell();
                LineaFinal.Colspan = 6;
                LineaFinal.Padding = 3;
                LineaFinal.Border = 1;
                LineaFinal.BorderColorBottom = BaseColor.WHITE;
                LineaFinal.HorizontalAlignment = Element.ALIGN_CENTER;
                LineaFinal.VerticalAlignment = Element.ALIGN_TOP;
                tableUnidades.AddCell(LineaFinal);


                #endregion

                #region Footer

                PdfPTable tabla_borde4 = new PdfPTable(1);
                tabla_borde4.WidthPercentage = 100;
                PdfPCell tabla4 = new PdfPCell()
                {
                    CellEvent = CelEventBorderRound,
                    Border = 0,
                    MinimumHeight = 15,
                };


                PdfPTable Footer = new PdfPTable(new float[] { 7.5f, 2.5f });
                Footer.WidthPercentage = 100;

                PdfPCell info_pie = new PdfPCell() { Border = 0, };
                //-----------------------------------
                PdfPTable l1 = new PdfPTable(1);
                l1.WidthPercentage = 100;
                PdfPCell l1_2 = new PdfPCell(new Phrase("SON: " + Helpers.Compartido.Nroenletras(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"].ToString()), fontTitle)) { MinimumHeight = 20 };
                l1.AddCell(l1_2);
                info_pie.AddElement(l1);
                //----------------------------------
                PdfPTable l2 = new PdfPTable(1);
                l2.WidthPercentage = 100;
                PdfPCell l2_2 = new PdfPCell(new Phrase("Esta factura cambiaria de compraventa se asimila en todos sus efectos a una letra de cambio (art. 774 del código de comercio)." +
                    "Después el vencimiento se cobráran intereses por mora segun lo fijado por la Superfinanciera.", fontTitle))
                { MinimumHeight = 20 };
                l2.AddCell(l2_2);
                info_pie.AddElement(l2);
                ///7------------------------------
                PdfPTable l3 = new PdfPTable(1);
                l3.WidthPercentage = 100;
                PdfPCell l3_2 = new PdfPCell(new Phrase(DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceComment"].ToString(), fontTitle)) { MinimumHeight = 20 };
                l3.AddCell(l3_2);
                info_pie.AddElement(l3);
                ///----------------------
                PdfPTable l4 = new PdfPTable(1);
                l4.WidthPercentage = 100;
                PdfPCell l4_2 = new PdfPCell(new Phrase("FAVOR CONSIGNAR: CUENTA CORRIENTE DE BANCOLOMBIA 0 1 2 6 1 7 6 1 7 0 7", fontTitle)) { MinimumHeight = 20 };
                l4.AddCell(l4_2);
                info_pie.AddElement(l4);


                ///agregamos los totales 
                PdfPCell totales = new PdfPCell() { };

                PdfPTable valores = new PdfPTable(2);
                valores.WidthPercentage = 100;

                PdfPCell totales_t = new PdfPCell() { Border = 0 };
                //subtotal titulo
                PdfPTable subtotal = new PdfPTable(1);
                subtotal.WidthPercentage = 100;
                PdfPCell subtotal_t = new PdfPCell(new Phrase("SUBTOTAL ", fontTitle)) { MinimumHeight = 17, HorizontalAlignment = Element.ALIGN_LEFT, Border = 0 };
                subtotal.AddCell(subtotal_t);
                totales_t.AddElement(subtotal);
                //descuento titulo
                PdfPTable descuento = new PdfPTable(1);
                descuento.WidthPercentage = 100;
                PdfPCell descuento_t = new PdfPCell(new Phrase("DESCUENTO ", fontTitle)) { MinimumHeight = 17, HorizontalAlignment = Element.ALIGN_LEFT, Border = 0 };
                descuento.AddCell(descuento_t);
                totales_t.AddElement(descuento);
                //iva titulo
                PdfPTable iva = new PdfPTable(1);
                iva.WidthPercentage = 100;
                PdfPCell iva_t = new PdfPCell(new Phrase("IVA ", fontTitle)) { MinimumHeight = 17, HorizontalAlignment = Element.ALIGN_LEFT, Border = 0 };
                iva.AddCell(iva_t);
                totales_t.AddElement(iva);
                //total titulo
                PdfPTable total = new PdfPTable(1);
                total.WidthPercentage = 100;
                PdfPCell total_t = new PdfPCell(new Phrase("TOTAL ", fontTitle)) { MinimumHeight = 17, HorizontalAlignment = Element.ALIGN_LEFT, };
                total.AddCell(total_t);
                totales_t.AddElement(total);

                PdfPCell totales_v = new PdfPCell() { Border = 0 };
                //subtotal valores
                PdfPTable subtotal_v = new PdfPTable(1);
                subtotal_v.WidthPercentage = 100;
                PdfPCell subtotal_v2 = new PdfPCell(new Phrase(decimal.Parse(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubTotal"].ToString()).ToString("N2"), fontTitle)) { MinimumHeight = 17, HorizontalAlignment = Element.ALIGN_RIGHT, Border = 0 };
                subtotal_v.AddCell(subtotal_v2);
                totales_v.AddElement(subtotal_v);
                //descuento valores
                PdfPTable descuento_v = new PdfPTable(1);
                descuento_v.WidthPercentage = 100;
                PdfPCell descuento_v2 = new PdfPCell(new Phrase(totalDescuento.ToString("N2"), fontTitle)) { MinimumHeight = 17, HorizontalAlignment = Element.ALIGN_RIGHT, Border = 0 };
                descuento_v.AddCell(descuento_v2);
                totales_v.AddElement(descuento_v);
                //iva valores
                PdfPTable iva_v = new PdfPTable(1);
                iva_v.WidthPercentage = 100;
                PdfPCell iva_v2 = new PdfPCell(new Phrase(GetValImpuestoByID("01", DsInvoiceAR).ToString("N2"), fontTitle)) { MinimumHeight = 17, HorizontalAlignment = Element.ALIGN_RIGHT, Border = 0 };
                iva_v.AddCell(iva_v2);
                totales_v.AddElement(iva_v);
                //total valores
                PdfPTable total_v = new PdfPTable(1);
                total_v.WidthPercentage = 100;
                PdfPCell total_v2 = new PdfPCell(new Phrase(decimal.Parse(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"].ToString()).ToString("N2"), fontTitle)) { MinimumHeight = 17, HorizontalAlignment = Element.ALIGN_RIGHT, Border = 0 };
                total_v.AddCell(total_v2);
                totales_v.AddElement(total_v);


                valores.AddCell(totales_t);
                valores.AddCell(totales_v);

                totales.AddElement(valores);

                Footer.AddCell(info_pie);
                Footer.AddCell(totales);

                //agregamos las ultimas tablas de pie de pagina 
                PdfPTable info_salida = new PdfPTable(4);
                info_salida.WidthPercentage = 100;
                PdfPCell emisor = new PdfPCell(new Phrase("Emisor", fontTitle)) { HorizontalAlignment = Element.ALIGN_CENTER };
                PdfPCell Recibo = new PdfPCell(new Phrase("Recibo de bien o servico", fontTitle)) { HorizontalAlignment = Element.ALIGN_CENTER };
                PdfPCell recibo_f = new PdfPCell(new Phrase("Recibo de la factura", fontTitle)) { HorizontalAlignment = Element.ALIGN_CENTER };
                PdfPCell Asectamos = new PdfPCell(new Phrase("Aceptamos expresamente el contenido  de la de la presente factura", fontTitle)) { HorizontalAlignment = Element.ALIGN_CENTER };

                info_salida.AddCell(emisor);
                info_salida.AddCell(Recibo);
                info_salida.AddCell(recibo_f);
                info_salida.AddCell(Asectamos);

                PdfPTable fin = new PdfPTable(new float[] { 7.5f, 2.5f });
                fin.WidthPercentage = 100;

                PdfPCell fin1 = new PdfPCell() { Border = 0, };

                PdfPTable infosalida_b = new PdfPTable(3);
                infosalida_b.WidthPercentage = 100;
                PdfPCell emisor_cel = new PdfPCell(new Phrase("_________________________________(firma) elaborado por", fontTitle)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_BOTTOM, };
                PdfPCell recibo_cel = new PdfPCell() { MinimumHeight = 50, };

                PdfPTable fima = new PdfPTable(1);
                fima.WidthPercentage = 100;
                PdfPCell firma_1 = new PdfPCell(new Phrase("Firma:_________________________", fontTitle)) { HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_BOTTOM, Border = 0 };
                fima.AddCell(firma_1);
                recibo_cel.AddElement(fima);

                PdfPTable nombre = new PdfPTable(1);
                nombre.WidthPercentage = 100;
                PdfPCell nombre_1 = new PdfPCell(new Phrase("Nombre:________________________", fontTitle)) { HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_BOTTOM, Border = 0 };
                nombre.AddCell(nombre_1);
                recibo_cel.AddElement(nombre);

                PdfPTable Identificación = new PdfPTable(1);
                Identificación.WidthPercentage = 100;
                PdfPCell Identificación_1 = new PdfPCell(new Phrase("Identificación:___________________", fontTitle)) { HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_BOTTOM, Border = 0 };
                Identificación.AddCell(Identificación_1);
                recibo_cel.AddElement(Identificación);

                PdfPTable Fecha = new PdfPTable(1);
                Fecha.WidthPercentage = 100;
                PdfPCell Fecha_1 = new PdfPCell(new Phrase("Fecha de Recibo:___________", fontTitle)) { VerticalAlignment = Element.ALIGN_BOTTOM, HorizontalAlignment = Element.ALIGN_LEFT, Border = 0 };
                Fecha.AddCell(Fecha_1);
                recibo_cel.AddElement(Fecha);


                PdfPCell recibo_f_cel = new PdfPCell();

                PdfPTable fima_2 = new PdfPTable(1);
                fima_2.WidthPercentage = 100;
                PdfPCell fima_2l = new PdfPCell(new Phrase("Firma:___________", fontTitle)) { VerticalAlignment = Element.ALIGN_BOTTOM, HorizontalAlignment = Element.ALIGN_LEFT, Border = 0 };
                fima_2.AddCell(fima_2l);
                recibo_f_cel.AddElement(fima_2);

                PdfPTable nombre_2 = new PdfPTable(1);
                nombre_2.WidthPercentage = 100;
                PdfPCell nombre_2l = new PdfPCell(new Phrase("Nombre:___________", fontTitle)) { VerticalAlignment = Element.ALIGN_BOTTOM, HorizontalAlignment = Element.ALIGN_LEFT, Border = 0 };
                nombre_2.AddCell(nombre_2l);
                recibo_f_cel.AddElement(nombre_2);

                PdfPTable Identificación_2 = new PdfPTable(1);
                Identificación_2.WidthPercentage = 100;
                PdfPCell Identificación_2l = new PdfPCell(new Phrase("Identificación:___________", fontTitle)) { VerticalAlignment = Element.ALIGN_BOTTOM, HorizontalAlignment = Element.ALIGN_LEFT, Border = 0 };
                Identificación_2.AddCell(Identificación_2l);
                recibo_f_cel.AddElement(Identificación_2);

                PdfPTable Fecha_2 = new PdfPTable(1);
                Fecha_2.WidthPercentage = 100;
                PdfPCell Fecha_2l = new PdfPCell(new Phrase("Fecha de Recibo:___________", fontTitle)) { VerticalAlignment = Element.ALIGN_BOTTOM, HorizontalAlignment = Element.ALIGN_LEFT, Border = 0 };
                Fecha_2.AddCell(Fecha_2l);
                recibo_f_cel.AddElement(Fecha_2);

                infosalida_b.AddCell(emisor_cel);
                infosalida_b.AddCell(recibo_cel);
                infosalida_b.AddCell(recibo_f_cel);
                fin1.AddElement(infosalida_b);

                PdfPTable leyenda = new PdfPTable(1);
                leyenda.WidthPercentage = 100;
                PdfPCell ley = new PdfPCell(new Phrase("NO SOMOS AUTORETENEDORES NI GRANDES CONTRIBUYENTES. NO APLICAR RETENCION ICA,SOMOS FABRICANTES.ART.77 LEY 49 DE 1990", fontCustom)) { Border = 0, };//agregamos la yeyenda 
                leyenda.AddCell(ley);
                fin1.AddElement(leyenda);



                PdfPCell fin2 = new PdfPCell(new Phrase("___________________________________" + "La aceptación de esta factura por unapersona " +
                                                        " distinta al comprador implicaque dicha " +
                                                        "persona ha sido autorizadapor éste" +
                                                        " para tales efectos.", fontCustom))
                { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_BOTTOM, };

                fin.AddCell(fin1);
                fin.AddCell(fin2);

                tabla4.AddElement(Footer);
                tabla4.AddElement(info_salida);
                tabla4.AddElement(fin);
                tabla_borde4.AddCell(tabla4);


                PdfPTable letras = new PdfPTable(new float[] { 4.0f, 2.0f, 4.0f });
                letras.WidthPercentage = 100;
                PdfPCell letras1 = new PdfPCell(new Phrase("Teléfonos de servicio al cliente : 318 359 8801 - (4) 379 46 48 ext 104", fontCustom2)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT };
                PdfPCell letras2 = new PdfPCell(new Phrase("", fontTitle)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT };
                PdfPCell letras3 = new PdfPCell(new Phrase("Factura impresa programa FOXSCI. Eric D. Porras A. Tel 3108480624 Nit.70563174-3", fontCustom2)) { Border = 0, HorizontalAlignment = Element.ALIGN_RIGHT };

                letras.AddCell(letras1);
                letras.AddCell(letras2);
                letras.AddCell(letras3);

                #endregion

                #region Exti
                document.Add(tabla_borde);
                document.Add(tabla_borde2);
                document.Add(espacio);
                document.Add(tabla_borde3);
                document.Add(tableUnidades);
                document.Add(tabla_borde4);
                document.Add(letras);

                /*PIE DE PAGINA*/
                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();
                return true;
                #endregion
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return false;
            }
            

        }
        #endregion


        #region Nota Credito Inter Bel

        public bool NotaCreditoInterbel(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {
            #region Head
            NomArchivo = string.Empty;

            string Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            string RutaImg = $@"{AppDomain.CurrentDomain.BaseDirectory}\LOGO_INTERBEL.png";
            string Rutacertificado = $@"{AppDomain.CurrentDomain.BaseDirectory}\LOGO_PLASTICOS_AMBIENTALES.png";

            NomArchivo = NombreInvoice + ".pdf";

            try
            {
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.
                //Document document = new Document(iTextSharp.text.PageSize.LETTER);
                Document document = new Document(iTextSharp.text.PageSize.A4.Rotate(), 10, 10, 10, 10);
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;



                // step 3: we open the document     
                document.Open();

                PdfDiv divEspacio = new PdfDiv();
                divEspacio.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio.Height = 10;
                divEspacio.Width = 130;

                PdfDiv divEspacio2 = new PdfDiv();
                divEspacio2.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio2.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio2.Height = 2;
                divEspacio2.Width = 130;

                //FUENTES----------------------------------------------------------------------------------------------------------
                iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle3 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 24, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle2 = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustom = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustom2 = FontFactory.GetFont(FontFactory.HELVETICA, 5, iTextSharp.text.Font.NORMAL);
                //FUENTES---------------------------------------------------------------------------------------------------------

                ///objeto para el borde 
                RoundRectangle CelEventBorderRound = new RoundRectangle();

                System.Drawing.Image Logo;
                System.Drawing.Image LogoBanner = null;
                var request = WebRequest.Create(RutaImg);
                using (var response = request.GetResponse())
                using (var stream = response.GetResponseStream())
                {
                    Logo = Bitmap.FromStream(stream);
                }
                var requestBanner = WebRequest.Create(RutaImg);
                using (var responseBanner = requestBanner.GetResponse())
                using (var streamBanner = responseBanner.GetResponseStream())
                {
                    LogoBanner = Bitmap.FromStream(streamBanner);
                }


                System.Drawing.Image Logo2;
                System.Drawing.Image LogoBanner2 = null;
                var request2 = WebRequest.Create(Rutacertificado);
                using (var response2 = request2.GetResponse())
                using (var stream2 = response2.GetResponseStream())
                {
                    Logo2 = Bitmap.FromStream(stream2);
                }
                var requestBanner2 = WebRequest.Create(Rutacertificado);
                using (var responseBanner2 = requestBanner2.GetResponse())
                using (var streamBanner2 = responseBanner2.GetResponseStream())
                {
                    LogoBanner2 = Bitmap.FromStream(streamBanner2);
                }


                //Logos--------------------------------
                iTextSharp.text.Image LogoPdf3 = iTextSharp.text.Image.GetInstance(LogoBanner2, BaseColor.WHITE);
                LogoPdf3.ScaleAbsolute(90f, 90f);
                LogoPdf3.Border = 0;


                iTextSharp.text.Image LogoPdf2 = iTextSharp.text.Image.GetInstance(LogoBanner, BaseColor.WHITE);
                LogoPdf2.ScaleAbsolute(250, 250);
                LogoPdf2.Border = 0;

                iTextSharp.text.Image QRPdf = iTextSharp.text.Image.GetInstance(QRInvoice, BaseColor.WHITE);
                QRPdf.ScaleAbsolute(100, 100);
                QRPdf.Border = 0;


                PdfPTable espacio = new PdfPTable(1);
                espacio.WidthPercentage = 100;
                PdfPCell salto = new PdfPCell(new Phrase(" 0 " + " LOTE: ")) { Border = 0, MinimumHeight = 20, HorizontalAlignment = Element.ALIGN_CENTER };//se agrega el lote
                espacio.AddCell(salto);

                #endregion

                #region Header
                PdfPTable Header = new PdfPTable(1);
                Header.WidthPercentage = 100;
                PdfPCell header_master = new PdfPCell()
                {
                    CellEvent = CelEventBorderRound,
                };

                PdfPTable encabezado = new PdfPTable(3);
                encabezado.WidthPercentage = 100;
                PdfPCell logo = new PdfPCell() { Border = 0, MinimumHeight = 80, };
                logo.Border = PdfPCell.RIGHT_BORDER;
                PdfPCell empresa = new PdfPCell(new Phrase("INTERBEL")) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER };
                empresa.Border = PdfPCell.RIGHT_BORDER;
                PdfPCell tip_factura = new PdfPCell(new Phrase("NOTA Debito No.", fontTitle3)) { Border = 0, MinimumHeight = 50, HorizontalAlignment = Element.ALIGN_CENTER };

                PdfPTable info_factura = new PdfPTable(new float[] { 1.5f, 4.5f, 2.0f });
                info_factura.WidthPercentage = 100;

                PdfPCell C1 = new PdfPCell(new Phrase("")) { MinimumHeight = 50, Border = 0, };
                PdfPTable Ciudad = new PdfPTable(1);
                Ciudad.WidthPercentage = 100;

                PdfPCell Ciudad_l = new PdfPCell(new Phrase("Ciudad")) { MinimumHeight = 20, };
                Ciudad.AddCell(Ciudad_l);
                C1.AddElement(Ciudad);

                PdfPTable Recibido = new PdfPTable(1);
                Recibido.WidthPercentage = 100;
                PdfPCell Recibido_l = new PdfPCell(new Phrase("Recibido por")) { MinimumHeight = 20, };
                Recibido.AddCell(Recibido_l);
                C1.AddElement(Recibido);

                PdfPTable suma = new PdfPTable(1);
                suma.WidthPercentage = 100;
                PdfPCell suma_l = new PdfPCell(new Phrase("La suma de")) { MinimumHeight = 20, };
                Recibido.AddCell(suma_l);
                C1.AddElement(suma);

                PdfPTable concepto = new PdfPTable(1);
                concepto.WidthPercentage = 100;
                PdfPCell concepto_l = new PdfPCell(new Phrase("Por concepto de")) { MinimumHeight = 20, };
                concepto.AddCell(concepto_l);
                C1.AddElement(concepto);
                ///celta del medio del los datos del cliente 
                PdfPCell C2 = new PdfPCell(new Phrase("")) { Border = 0, };

                PdfPTable Ciudadr = new PdfPTable(1);
                Ciudadr.WidthPercentage = 100;
                PdfPCell Ciudad_lr = new PdfPCell(new Phrase("xxxxxx")) { MinimumHeight = 20, };
                Ciudadr.AddCell(Ciudad_lr);
                C2.AddElement(Ciudadr);

                PdfPTable Recibidor = new PdfPTable(new float[] { 7.5f, 0.9f, 2.0f });
                Recibidor.WidthPercentage = 100;
                PdfPCell Recibido_lr = new PdfPCell(new Phrase("xxxxxx")) { MinimumHeight = 20, };
                PdfPCell Recibido_lr_n = new PdfPCell(new Phrase("Nit")) { MinimumHeight = 20, HorizontalAlignment = Element.ALIGN_CENTER };
                PdfPCell Recibido_lr_v = new PdfPCell(new Phrase("xxxxxxxx")) { MinimumHeight = 20, HorizontalAlignment = Element.ALIGN_CENTER };
                Recibidor.AddCell(Recibido_lr);
                Recibidor.AddCell(Recibido_lr_n);
                Recibidor.AddCell(Recibido_lr_v);
                C2.AddElement(Recibidor);

                PdfPTable sumar = new PdfPTable(1);
                sumar.WidthPercentage = 100;
                PdfPCell suma_lr = new PdfPCell(new Phrase("xxxxxx")) { MinimumHeight = 20, };
                sumar.AddCell(suma_lr);
                C2.AddElement(sumar);

                PdfPTable conceptor = new PdfPTable(1);
                conceptor.WidthPercentage = 100;
                PdfPCell concepto_lr = new PdfPCell(new Phrase("xxxxxx")) { MinimumHeight = 20, };
                conceptor.AddCell(concepto_lr);
                C2.AddElement(conceptor);
                //tabla final de tados del clñiente 
                PdfPCell C3 = new PdfPCell(new Phrase("")) { Border = 0, };

                PdfPTable Ciudadrf = new PdfPTable(new float[] { 0.2f, 0.7f });
                Ciudadrf.WidthPercentage = 100;
                PdfPCell Ciudad_lrf = new PdfPCell(new Phrase("Fecha")) { MinimumHeight = 20, };
                PdfPCell Ciudad_lrf_v = new PdfPCell(new Phrase("xxxxxxx")) { MinimumHeight = 20, };
                Ciudadrf.AddCell(Ciudad_lrf);
                Ciudadrf.AddCell(Ciudad_lrf_v);
                C3.AddElement(Ciudadrf);

                PdfPTable Recibidorf = new PdfPTable(new float[] { 0.2f, 0.7f });
                Recibidorf.WidthPercentage = 100;
                PdfPCell Recibido_lrf = new PdfPCell(new Phrase("Código ")) { MinimumHeight = 20, };
                PdfPCell Recibido_lrf_v = new PdfPCell(new Phrase("xxxxxxxx ")) { MinimumHeight = 20, };//codigo
                Recibidorf.AddCell(Recibido_lrf);
                Recibidorf.AddCell(Recibido_lrf_v);
                C3.AddElement(Recibidorf);

                PdfPTable sumarf = new PdfPTable(new float[] { 0.2f, 0.7f });
                sumarf.WidthPercentage = 100;
                PdfPCell suma_lrf = new PdfPCell(new Phrase("  ")) { MinimumHeight = 20, };
                PdfPCell suma_lrf_v = new PdfPCell(new Phrase("  ")) { MinimumHeight = 20, };//espacio en blanco no se defino un uso 
                sumarf.AddCell(suma_lrf);
                sumarf.AddCell(suma_lrf_v);
                C3.AddElement(sumarf);

                PdfPTable conceptorf = new PdfPTable(new float[] { 0.2f, 0.7f });
                conceptorf.WidthPercentage = 100;
                PdfPCell concepto_lrf = new PdfPCell(new Phrase(" No: ")) { MinimumHeight = 20, };
                PdfPCell concepto_lrf_v = new PdfPCell(new Phrase(" xxxxxxxx ")) { MinimumHeight = 20, };
                conceptorf.AddCell(concepto_lrf);
                conceptorf.AddCell(concepto_lrf_v);
                C3.AddElement(conceptorf);


                info_factura.AddCell(C1);
                info_factura.AddCell(C2);
                info_factura.AddCell(C3);

                PdfPTable pieencabezado = new PdfPTable(new float[] { 1.2f, 1.2f, 1.2f, 1.2f, 2.0f, 3.0f });
                pieencabezado.WidthPercentage = 100;
                PdfPCell enpie0 = new PdfPCell(new Phrase("FACTURAS")) { Border = 0, };
                PdfPCell enpie1 = new PdfPCell(new Phrase("REFERENCIA")) { Border = 0, };
                PdfPCell enpie2 = new PdfPCell(new Phrase("CANTIDAD")) { Border = 0, };
                PdfPCell enpie3 = new PdfPCell(new Phrase("VALOR")) { Border = 0, };
                PdfPCell enpie4 = new PdfPCell(new Phrase("I.V.A  DESCRIPCION")) { Border = 0, };
                PdfPCell enpie5 = new PdfPCell(new Phrase(" ")) { Border = 0, };
                pieencabezado.AddCell(enpie0);
                pieencabezado.AddCell(enpie1);
                pieencabezado.AddCell(enpie2);
                pieencabezado.AddCell(enpie3);
                pieencabezado.AddCell(enpie4);
                pieencabezado.AddCell(enpie5);

                logo.AddElement(LogoPdf2);

                encabezado.AddCell(logo);
                encabezado.AddCell(empresa);
                encabezado.AddCell(tip_factura);
                header_master.AddElement(encabezado);
                header_master.AddElement(info_factura);
                header_master.AddElement(pieencabezado);

                Header.AddCell(header_master);
                #endregion

                #region Body
                //agregamos el cuerpo de el documento no qagregaremos los detalles de el formato 
                PdfPTable Body = new PdfPTable(1);
                Body.WidthPercentage = 100;
                PdfPCell cuerpo = new PdfPCell()
                {
                    CellEvent = CelEventBorderRound,
                    MinimumHeight = 100,
                };

                PdfPTable obs = new PdfPTable(1);
                obs.WidthPercentage = 100;
                PdfPCell observaciones = new PdfPCell(new Phrase("OBSERVACIONES")) { HorizontalAlignment = Element.ALIGN_CENTER };
                obs.AddCell(observaciones);
                cuerpo.AddElement(obs);

                PdfPTable reser = new PdfPTable(1);
                reser.WidthPercentage = 100;
                PdfPCell reservas = new PdfPCell(new Phrase("")) { HorizontalAlignment = Element.ALIGN_CENTER, MinimumHeight = 50, };
                reser.AddCell(reservas);
                cuerpo.AddElement(reser);

                PdfPTable valores = new PdfPTable(3);
                valores.WidthPercentage = 100;
                PdfPCell valoresf = new PdfPCell(new Phrase("")) { HorizontalAlignment = Element.ALIGN_CENTER, MinimumHeight = 60, };

                PdfPTable contable = new PdfPTable(1);
                contable.WidthPercentage = 100;
                PdfPCell valorUnidad = new PdfPCell(new Phrase("valor por Unidad: " + " xxxxxxxxx")) { Border = 0, };///valor de las unidades 
                contable.AddCell(valorUnidad);
                valoresf.AddElement(contable);

                PdfPTable contable2 = new PdfPTable(1);
                contable2.WidthPercentage = 100;
                PdfPCell unidades = new PdfPCell(new Phrase("Unidades: " + "  xxxxxxxxx")) { Border = 0, };//las unidades 
                contable2.AddCell(unidades);
                valoresf.AddElement(contable2);

                PdfPTable contable3 = new PdfPTable(1);
                contable3.WidthPercentage = 100;
                PdfPCell valor = new PdfPCell(new Phrase("valor Unidades: " + " xxxxxxxxx")) { Border = 0, };//valor de la unidad indfividuial
                contable3.AddCell(valor);
                valoresf.AddElement(contable3);

                PdfPCell valoress = new PdfPCell(new Phrase("")) { HorizontalAlignment = Element.ALIGN_CENTER, MinimumHeight = 60, };


                PdfPTable contable4 = new PdfPTable(1);
                contable4.WidthPercentage = 100;
                PdfPCell descuento = new PdfPCell(new Phrase("valor Descuento: " + " xxxxxxxxx")) { Border = 0, };///valor de las unidades 
                contable4.AddCell(descuento);
                valoress.AddElement(contable4);

                PdfPTable contable5 = new PdfPTable(1);
                contable5.WidthPercentage = 100;
                PdfPCell iva = new PdfPCell(new Phrase("iva: " + " xxxxxxxxx")) { Border = 0, };//las unidades 
                contable5.AddCell(iva);
                valoress.AddElement(contable5);

                PdfPTable contable6 = new PdfPTable(1);
                contable6.WidthPercentage = 100;
                PdfPCell neto = new PdfPCell(new Phrase("Neto : " + "  xxxxxxxxx")) { Border = 0, };//valor de la unidad indfividuial
                contable6.AddCell(neto);
                valoress.AddElement(contable6);

                PdfPCell valoresa = new PdfPCell(new Phrase("")) { HorizontalAlignment = Element.ALIGN_CENTER, MinimumHeight = 60, };
                PdfPTable contable7 = new PdfPTable(1);
                contable7.WidthPercentage = 100;
                PdfPCell Rfuente = new PdfPCell(new Phrase("Rete Fuente : " + " xxxxxxxxx")) { Border = 0, };///valor de las unidades 
                contable7.AddCell(Rfuente);
                valoresa.AddElement(contable7);

                PdfPTable contable8 = new PdfPTable(1);
                contable8.WidthPercentage = 100;
                PdfPCell Riva = new PdfPCell(new Phrase("Rete iva: " + " xxxxxxxxx")) { Border = 0, };//las unidades 
                contable8.AddCell(iva);
                valoresa.AddElement(contable8);

                PdfPTable contable9 = new PdfPTable(1);
                contable9.WidthPercentage = 100;
                PdfPCell Total = new PdfPCell(new Phrase("Total : " + " xxxxxxxxx")) { MinimumHeight = 25, };//valor de la unidad indfividuial
                contable9.AddCell(Total);
                valoresa.AddElement(contable9);



                valores.AddCell(valoresf);
                valores.AddCell(valoress);
                valores.AddCell(valoresa);
                cuerpo.AddElement(valores);


                PdfPTable final = new PdfPTable(1);
                final.WidthPercentage = 100;
                PdfPCell finalf = new PdfPCell(new Phrase("FIRMA Y SELLO C.C. O NIT")) { VerticalAlignment = Element.ALIGN_BOTTOM, HorizontalAlignment = Element.ALIGN_RIGHT, MinimumHeight = 80, };
                final.AddCell(finalf);
                cuerpo.AddElement(final);

                Body.AddCell(cuerpo);
                #endregion

                #region Footer

                #endregion

                #region Exti
                document.Add(Header);
                document.Add(espacio);
                document.Add(Body);

                /*PIE DE PAGINA*/
                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return false;
            }
            #endregion
        }
        #endregion


        #region Nota Debito Inter Bel

        public bool NotaDebitoInterbel(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {
            #region Head
            NomArchivo = string.Empty;

            string Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            string RutaImg = $@"{AppDomain.CurrentDomain.BaseDirectory}\LOGO_INTERBEL.png";
            string Rutacertificado = $@"{AppDomain.CurrentDomain.BaseDirectory}\LOGO_PLASTICOS_AMBIENTALES.png";

            NomArchivo = NombreInvoice + ".pdf";

            try
            {
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.
                //Document document = new Document(iTextSharp.text.PageSize.LETTER);
                Document document = new Document(iTextSharp.text.PageSize.A4.Rotate(), 10, 10, 10, 10);
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;



                // step 3: we open the document     
                document.Open();

                PdfDiv divEspacio = new PdfDiv();
                divEspacio.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio.Height = 10;
                divEspacio.Width = 130;

                PdfDiv divEspacio2 = new PdfDiv();
                divEspacio2.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio2.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio2.Height = 2;
                divEspacio2.Width = 130;

                //FUENTES----------------------------------------------------------------------------------------------------------
                iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle3 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 24, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle2 = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustom = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustom2 = FontFactory.GetFont(FontFactory.HELVETICA, 5, iTextSharp.text.Font.NORMAL);
                //FUENTES---------------------------------------------------------------------------------------------------------

                ///objeto para el borde 
                RoundRectangle CelEventBorderRound = new RoundRectangle();

                System.Drawing.Image Logo;
                System.Drawing.Image LogoBanner = null;
                var request = WebRequest.Create(RutaImg);
                using (var response = request.GetResponse())
                using (var stream = response.GetResponseStream())
                {
                    Logo = Bitmap.FromStream(stream);
                }
                var requestBanner = WebRequest.Create(RutaImg);
                using (var responseBanner = requestBanner.GetResponse())
                using (var streamBanner = responseBanner.GetResponseStream())
                {
                    LogoBanner = Bitmap.FromStream(streamBanner);
                }


                System.Drawing.Image Logo2;
                System.Drawing.Image LogoBanner2 = null;
                var request2 = WebRequest.Create(Rutacertificado);
                using (var response2 = request2.GetResponse())
                using (var stream2 = response2.GetResponseStream())
                {
                    Logo2 = Bitmap.FromStream(stream2);
                }
                var requestBanner2 = WebRequest.Create(Rutacertificado);
                using (var responseBanner2 = requestBanner2.GetResponse())
                using (var streamBanner2 = responseBanner2.GetResponseStream())
                {
                    LogoBanner2 = Bitmap.FromStream(streamBanner2);
                }


                //Logos--------------------------------
                iTextSharp.text.Image LogoPdf3 = iTextSharp.text.Image.GetInstance(LogoBanner2, BaseColor.WHITE);
                LogoPdf3.ScaleAbsolute(90f, 90f);
                LogoPdf3.Border = 0;


                iTextSharp.text.Image LogoPdf2 = iTextSharp.text.Image.GetInstance(LogoBanner, BaseColor.WHITE);
                LogoPdf2.ScaleAbsolute(250, 250);
                LogoPdf2.Border = 0;

                iTextSharp.text.Image QRPdf = iTextSharp.text.Image.GetInstance(QRInvoice, BaseColor.WHITE);
                QRPdf.ScaleAbsolute(100, 100);
                QRPdf.Border = 0;


                PdfPTable espacio = new PdfPTable(1);
                espacio.WidthPercentage = 100;
                PdfPCell salto = new PdfPCell(new Phrase(" 0 " + " LOTE: ")) { Border = 0, MinimumHeight = 20, HorizontalAlignment = Element.ALIGN_CENTER };//se agrega el lote
                espacio.AddCell(salto);

                #endregion

                #region Header
                PdfPTable Header = new PdfPTable(1);
                Header.WidthPercentage = 100;
                PdfPCell header_master = new PdfPCell()
                {
                    CellEvent = CelEventBorderRound,
                };

                PdfPTable encabezado = new PdfPTable(3);
                encabezado.WidthPercentage = 100;
                PdfPCell logo = new PdfPCell() { Border = 0, MinimumHeight = 80, };
                logo.Border = PdfPCell.RIGHT_BORDER;
                PdfPCell empresa = new PdfPCell(new Phrase("INTERBEL")) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER };
                empresa.Border = PdfPCell.RIGHT_BORDER;
                PdfPCell tip_factura = new PdfPCell(new Phrase("NOTA DEBITO No.", fontTitle3)) { Border = 0, MinimumHeight = 50, HorizontalAlignment = Element.ALIGN_CENTER };

                PdfPTable info_factura = new PdfPTable(new float[] { 1.5f, 4.5f, 2.0f });
                info_factura.WidthPercentage = 100;

                PdfPCell C1 = new PdfPCell(new Phrase("")) { MinimumHeight = 50, Border = 0, };
                PdfPTable Ciudad = new PdfPTable(1);
                Ciudad.WidthPercentage = 100;

                PdfPCell Ciudad_l = new PdfPCell(new Phrase("Ciudad")) { MinimumHeight = 20, };
                Ciudad.AddCell(Ciudad_l);
                C1.AddElement(Ciudad);

                PdfPTable Recibido = new PdfPTable(1);
                Recibido.WidthPercentage = 100;
                PdfPCell Recibido_l = new PdfPCell(new Phrase("Recibido por")) { MinimumHeight = 20, };
                Recibido.AddCell(Recibido_l);
                C1.AddElement(Recibido);

                PdfPTable suma = new PdfPTable(1);
                suma.WidthPercentage = 100;
                PdfPCell suma_l = new PdfPCell(new Phrase("La suma de")) { MinimumHeight = 20, };
                Recibido.AddCell(suma_l);
                C1.AddElement(suma);

                PdfPTable concepto = new PdfPTable(1);
                concepto.WidthPercentage = 100;
                PdfPCell concepto_l = new PdfPCell(new Phrase("Por concepto de")) { MinimumHeight = 20, };
                concepto.AddCell(concepto_l);
                C1.AddElement(concepto);
                ///celta del medio del los datos del cliente 
                PdfPCell C2 = new PdfPCell(new Phrase("")) { Border = 0, };

                PdfPTable Ciudadr = new PdfPTable(1);
                Ciudadr.WidthPercentage = 100;
                PdfPCell Ciudad_lr = new PdfPCell(new Phrase("xxxxxx")) { MinimumHeight = 20, };
                Ciudadr.AddCell(Ciudad_lr);
                C2.AddElement(Ciudadr);

                PdfPTable Recibidor = new PdfPTable(new float[] { 7.5f, 0.9f, 2.0f });
                Recibidor.WidthPercentage = 100;
                PdfPCell Recibido_lr = new PdfPCell(new Phrase("xxxxxx")) { MinimumHeight = 20, };
                PdfPCell Recibido_lr_n = new PdfPCell(new Phrase("Nit")) { MinimumHeight = 20, HorizontalAlignment = Element.ALIGN_CENTER };
                PdfPCell Recibido_lr_v = new PdfPCell(new Phrase("xxxxxxxx")) { MinimumHeight = 20, HorizontalAlignment = Element.ALIGN_CENTER };
                Recibidor.AddCell(Recibido_lr);
                Recibidor.AddCell(Recibido_lr_n);
                Recibidor.AddCell(Recibido_lr_v);
                C2.AddElement(Recibidor);

                PdfPTable sumar = new PdfPTable(1);
                sumar.WidthPercentage = 100;
                PdfPCell suma_lr = new PdfPCell(new Phrase("xxxxxx")) { MinimumHeight = 20, };
                sumar.AddCell(suma_lr);
                C2.AddElement(sumar);

                PdfPTable conceptor = new PdfPTable(1);
                conceptor.WidthPercentage = 100;
                PdfPCell concepto_lr = new PdfPCell(new Phrase("xxxxxx")) { MinimumHeight = 20, };
                conceptor.AddCell(concepto_lr);
                C2.AddElement(conceptor);
                //tabla final de tados del clñiente 
                PdfPCell C3 = new PdfPCell(new Phrase("")) { Border = 0, };

                PdfPTable Ciudadrf = new PdfPTable(new float[] { 0.2f, 0.7f });
                Ciudadrf.WidthPercentage = 100;
                PdfPCell Ciudad_lrf = new PdfPCell(new Phrase("Fecha")) { MinimumHeight = 20, };
                PdfPCell Ciudad_lrf_v = new PdfPCell(new Phrase("xxxxxxx")) { MinimumHeight = 20, };
                Ciudadrf.AddCell(Ciudad_lrf);
                Ciudadrf.AddCell(Ciudad_lrf_v);
                C3.AddElement(Ciudadrf);

                PdfPTable Recibidorf = new PdfPTable(new float[] { 0.2f, 0.7f });
                Recibidorf.WidthPercentage = 100;
                PdfPCell Recibido_lrf = new PdfPCell(new Phrase("Código ")) { MinimumHeight = 20, };
                PdfPCell Recibido_lrf_v = new PdfPCell(new Phrase("xxxxxxxx ")) { MinimumHeight = 20, };//codigo
                Recibidorf.AddCell(Recibido_lrf);
                Recibidorf.AddCell(Recibido_lrf_v);
                C3.AddElement(Recibidorf);

                PdfPTable sumarf = new PdfPTable(new float[] { 0.2f, 0.7f });
                sumarf.WidthPercentage = 100;
                PdfPCell suma_lrf = new PdfPCell(new Phrase("  ")) { MinimumHeight = 20, };
                PdfPCell suma_lrf_v = new PdfPCell(new Phrase("  ")) { MinimumHeight = 20, };//espacio en blanco no se defino un uso 
                sumarf.AddCell(suma_lrf);
                sumarf.AddCell(suma_lrf_v);
                C3.AddElement(sumarf);

                PdfPTable conceptorf = new PdfPTable(new float[] { 0.2f, 0.7f });
                conceptorf.WidthPercentage = 100;
                PdfPCell concepto_lrf = new PdfPCell(new Phrase(" No: ")) { MinimumHeight = 20, };
                PdfPCell concepto_lrf_v = new PdfPCell(new Phrase(" xxxxxxxx ")) { MinimumHeight = 20, };
                conceptorf.AddCell(concepto_lrf);
                conceptorf.AddCell(concepto_lrf_v);
                C3.AddElement(conceptorf);


                info_factura.AddCell(C1);
                info_factura.AddCell(C2);
                info_factura.AddCell(C3);

                PdfPTable pieencabezado = new PdfPTable(new float[] { 1.2f, 1.2f, 1.2f, 1.2f, 2.0f, 3.0f });
                pieencabezado.WidthPercentage = 100;
                PdfPCell enpie0 = new PdfPCell(new Phrase("FACTURAS")) { Border = 0, };
                PdfPCell enpie1 = new PdfPCell(new Phrase("REFERENCIA")) { Border = 0, };
                PdfPCell enpie2 = new PdfPCell(new Phrase("CANTIDAD")) { Border = 0, };
                PdfPCell enpie3 = new PdfPCell(new Phrase("VALOR")) { Border = 0, };
                PdfPCell enpie4 = new PdfPCell(new Phrase("I.V.A  DESCRIPCION")) { Border = 0, };
                PdfPCell enpie5 = new PdfPCell(new Phrase(" ")) { Border = 0, };
                pieencabezado.AddCell(enpie0);
                pieencabezado.AddCell(enpie1);
                pieencabezado.AddCell(enpie2);
                pieencabezado.AddCell(enpie3);
                pieencabezado.AddCell(enpie4);
                pieencabezado.AddCell(enpie5);

                logo.AddElement(LogoPdf2);

                encabezado.AddCell(logo);
                encabezado.AddCell(empresa);
                encabezado.AddCell(tip_factura);
                header_master.AddElement(encabezado);
                header_master.AddElement(info_factura);
                header_master.AddElement(pieencabezado);

                Header.AddCell(header_master);
                #endregion

                #region Body
                //agregamos el cuerpo de el documento no qagregaremos los detalles de el formato 
                PdfPTable Body = new PdfPTable(1);
                Body.WidthPercentage = 100;
                PdfPCell cuerpo = new PdfPCell()
                {
                    CellEvent = CelEventBorderRound,
                    MinimumHeight = 100,
                };

                PdfPTable obs = new PdfPTable(1);
                obs.WidthPercentage = 100;
                PdfPCell observaciones = new PdfPCell(new Phrase("OBSERVACIONES")) { HorizontalAlignment = Element.ALIGN_CENTER };
                obs.AddCell(observaciones);
                cuerpo.AddElement(obs);

                PdfPTable reser = new PdfPTable(1);
                reser.WidthPercentage = 100;
                PdfPCell reservas = new PdfPCell(new Phrase("")) { HorizontalAlignment = Element.ALIGN_CENTER, MinimumHeight = 50, };
                reser.AddCell(reservas);
                cuerpo.AddElement(reser);

                PdfPTable valores = new PdfPTable(3);
                valores.WidthPercentage = 100;
                PdfPCell valoresf = new PdfPCell(new Phrase("")) { HorizontalAlignment = Element.ALIGN_CENTER, MinimumHeight = 60, };

                PdfPTable contable = new PdfPTable(1);
                contable.WidthPercentage = 100;
                PdfPCell valorUnidad = new PdfPCell(new Phrase("valor por Unidad: " + " xxxxxxxxx")) { Border = 0, };///valor de las unidades 
                contable.AddCell(valorUnidad);
                valoresf.AddElement(contable);

                PdfPTable contable2 = new PdfPTable(1);
                contable2.WidthPercentage = 100;
                PdfPCell unidades = new PdfPCell(new Phrase("Unidades: " + "  xxxxxxxxx")) { Border = 0, };//las unidades 
                contable2.AddCell(unidades);
                valoresf.AddElement(contable2);

                PdfPTable contable3 = new PdfPTable(1);
                contable3.WidthPercentage = 100;
                PdfPCell valor = new PdfPCell(new Phrase("valor Unidades: " + " xxxxxxxxx")) { Border = 0, };//valor de la unidad indfividuial
                contable3.AddCell(valor);
                valoresf.AddElement(contable3);

                PdfPCell valoress = new PdfPCell(new Phrase("")) { HorizontalAlignment = Element.ALIGN_CENTER, MinimumHeight = 60, };


                PdfPTable contable4 = new PdfPTable(1);
                contable4.WidthPercentage = 100;
                PdfPCell descuento = new PdfPCell(new Phrase("valor Descuento: " + " xxxxxxxxx")) { Border = 0, };///valor de las unidades 
                contable4.AddCell(descuento);
                valoress.AddElement(contable4);

                PdfPTable contable5 = new PdfPTable(1);
                contable5.WidthPercentage = 100;
                PdfPCell iva = new PdfPCell(new Phrase("iva: " + " xxxxxxxxx")) { Border = 0, };//las unidades 
                contable5.AddCell(iva);
                valoress.AddElement(contable5);

                PdfPTable contable6 = new PdfPTable(1);
                contable6.WidthPercentage = 100;
                PdfPCell neto = new PdfPCell(new Phrase("Neto : " + "  xxxxxxxxx")) { Border = 0, };//valor de la unidad indfividuial
                contable6.AddCell(neto);
                valoress.AddElement(contable6);

                PdfPCell valoresa = new PdfPCell(new Phrase("")) { HorizontalAlignment = Element.ALIGN_CENTER, MinimumHeight = 60, };
                PdfPTable contable7 = new PdfPTable(1);
                contable7.WidthPercentage = 100;
                PdfPCell Rfuente = new PdfPCell(new Phrase("Rete Fuente : " + " xxxxxxxxx")) { Border = 0, };///valor de las unidades 
                contable7.AddCell(Rfuente);
                valoresa.AddElement(contable7);

                PdfPTable contable8 = new PdfPTable(1);
                contable8.WidthPercentage = 100;
                PdfPCell Riva = new PdfPCell(new Phrase("Rete iva: " + " xxxxxxxxx")) { Border = 0, };//las unidades 
                contable8.AddCell(iva);
                valoresa.AddElement(contable8);

                PdfPTable contable9 = new PdfPTable(1);
                contable9.WidthPercentage = 100;
                PdfPCell Total = new PdfPCell(new Phrase("Total : " + " xxxxxxxxx")) { MinimumHeight = 25, };//valor de la unidad indfividuial
                contable9.AddCell(Total);
                valoresa.AddElement(contable9);



                valores.AddCell(valoresf);
                valores.AddCell(valoress);
                valores.AddCell(valoresa);
                cuerpo.AddElement(valores);


                PdfPTable final = new PdfPTable(1);
                final.WidthPercentage = 100;
                PdfPCell finalf = new PdfPCell(new Phrase("FIRMA Y SELLO C.C. O NIT")) { VerticalAlignment = Element.ALIGN_BOTTOM, HorizontalAlignment = Element.ALIGN_RIGHT, MinimumHeight = 80, };
                final.AddCell(finalf);
                cuerpo.AddElement(final);

                Body.AddCell(cuerpo);
                #endregion

                #region Footer

                #endregion

                #region Exti
                document.Add(Header);
                document.Add(espacio);
                document.Add(Body);

                /*PIE DE PAGINA*/
                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return false;
            }
            #endregion
        }
        #endregion
    }
}