﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
//Referenciar y usar.
using System.Data;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Web;
using System.Windows.Forms;

namespace RulesServicesDIAN2.Adquiriente

{
    public partial class pdfEstandarAR
    {

        #region Formatos Facturacion PRIMORDIAL

        public bool FacturaNacionalc(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {
            //NomArchivo = string.Empty;
            //string Ruta = HttpContext.Current.Server.MapPath("~/PDF/" + NIT + "/");
            //string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            //string RutaImg = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";
            //NomArchivo = NombreInvoice + ".pdf";

            NomArchivo = string.Empty;
            string Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            string RutaImg = $@"{AppDomain.CurrentDomain.BaseDirectory}\calidad3.png";
            NomArchivo = NombreInvoice + ".pdf";
            try
            {
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.
                Document document = new Document(/*iTextSharp.text.PageSize.LETTER, 20f, 15f, 30f, 30f*/);
                document.SetPageSize(PageSize.A4);
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                document.Open();
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;

                PdfPCell celEspacio = new PdfPCell()
                {
                    Border = PdfPCell.NO_BORDER
                };

                PdfDiv divEspacio = new PdfDiv();
                divEspacio.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio.Height = 10;
                divEspacio.Width = 130;

                PdfDiv divEspacio2 = new PdfDiv();
                divEspacio2.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio2.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio2.Height = 2;
                divEspacio2.Width = 130;

                //FUENTES----------------------------------------------------------------------------------------------------------
                iTextSharp.text.Font fontTitleBold = FontFactory.GetFont(FontFactory.COURIER_BOLD, 8.5f, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.COURIER, 8.5f, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustom = FontFactory.GetFont(FontFactory.COURIER, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustomBold = FontFactory.GetFont(FontFactory.COURIER_BOLD, 7, iTextSharp.text.Font.NORMAL);
                //FUENTES---------------------------------------------------------------------------------------------------------

                #region ENCABEZADO
                PdfPTable tableEncabezado = new PdfPTable(new float[] { 2, 1, 2f });
                tableEncabezado.WidthPercentage = 100;

                //TODO: informacion empresa
                PdfPTable tableInfoEmpresa = new PdfPTable(1)
                {
                    WidthPercentage = 100,
                };

                tableInfoEmpresa.AddCell(new PdfPCell(new Phrase((string)DsInvoiceAR.Tables["Company"].Rows[0]["Name"], fontTitle))
                { Border = PdfPCell.NO_BORDER });

                tableInfoEmpresa.AddCell(new PdfPCell(new Phrase(
                    $"NIT:      " +
                    $"{(string)DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"]}", fontTitle))
                { Border = PdfPCell.NO_BORDER });

                tableInfoEmpresa.AddCell(new PdfPCell(new Phrase(
                    $"Dirección  :  " +
                    $"{(string)DsInvoiceAR.Tables["Company"].Rows[0]["Address1"]}", fontTitle))
                { Border = PdfPCell.NO_BORDER });

                tableInfoEmpresa.AddCell(new PdfPCell(new Phrase($"Teléfono   :     " +
                    $"{(string)DsInvoiceAR.Tables["Company"].Rows[0]["PhoneNum"]}  " +
                    $"{(string)DsInvoiceAR.Tables["Company"].Rows[0]["City"]}",
                    fontTitle))
                { Border = PdfPCell.NO_BORDER });

                tableEncabezado.AddCell(new PdfPCell(tableInfoEmpresa) { Border = PdfPCell.NO_BORDER, });

                //TODO: Celda de CUFE y QR
                PdfDiv divQR = new PdfDiv()
                {
                    Float = iTextSharp.text.pdf.PdfDiv.FloatType.LEFT,
                    Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE,
                    BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID,
                    Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK,
                    Height = 90,
                    Width = 90,
                    //BackgroundColor=BaseColor.LIGHT_GRAY,
                };

                iTextSharp.text.Image ImgQR = iTextSharp.text.Image.GetInstance(QRInvoice, BaseColor.WHITE);
                ImgQR.Alignment = Element.ALIGN_RIGHT;
                divQR.BackgroundImage = ImgQR;

                PdfPCell celQR = new PdfPCell()
                {
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    VerticalAlignment = Element.ALIGN_TOP,
                    //Colspan = 3,
                    Border = 0,
                    Padding = 3
                };
                //celQR.AddElement(new Paragraph("CUFE {***}", fontTitleBold));
                celQR.AddElement(divQR);
                tableEncabezado.AddCell(celQR);

                //TODO: Informacion factura
                PdfPTable tableInfoFactura = new PdfPTable(1);

                tableInfoFactura.AddCell(new PdfPCell(new Phrase($"FACTURA DE VENTA <R>     " +
                    $"{(string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"]}", fontTitle))
                {
                    Border = PdfPCell.NO_BORDER,
                    HorizontalAlignment = Element.ALIGN_RIGHT,
                });

                tableInfoFactura.AddCell(new PdfPCell(new Phrase("PAG:  ", fontTitle))
                {
                    HorizontalAlignment = Element.ALIGN_RIGHT,
                    Border = PdfPCell.NO_BORDER,
                    PaddingTop = 10,
                    PaddingRight = 9,
                });
                tableEncabezado.AddCell(new PdfPCell(tableInfoFactura) { Border = PdfPCell.NO_BORDER, });

                //CELDA DE CUFE
                tableEncabezado.AddCell(new PdfPCell(new Phrase($"CUFE: {CUFE}", fontTitleBold))
                {
                    Border = PdfPCell.NO_BORDER,
                    Colspan = 3,
                    Padding = 5,
                    HorizontalAlignment = Element.ALIGN_CENTER,
                });

                tableEncabezado.AddCell(new PdfPCell(new Phrase($"Representación gráfica factura electrónica", fontTitleBold))
                {
                    Border = PdfPCell.NO_BORDER,
                    Colspan = 3,
                    Padding = 5,
                    HorizontalAlignment = Element.ALIGN_CENTER,
                });
                #endregion

                #region DATOS CLIENTE

                var tableInfoCliente = new PdfPTable(2)
                {
                    WidthPercentage = 100,
                };

                var tableSenores = new PdfPTable(2)
                {
                    WidthPercentage = 100,
                };

                tableSenores.AddCell(new PdfPCell(new Phrase("SEÑORES", fontTitle))
                {
                    Border = PdfPCell.NO_BORDER,
                    Colspan = 2,
                });

                tableSenores.AddCell(new PdfPCell(new Phrase(
                    (string)DsInvoiceAR.Tables["Customer"].Rows[0]["Name"], fontTitle))
                {
                    Border = PdfPCell.NO_BORDER,
                    Colspan = 2,
                });

                tableSenores.AddCell(new PdfPCell(new Phrase(
                    (string)DsInvoiceAR.Tables["Customer"].Rows[0]["Address1"], fontTitle))
                {
                    Border = PdfPCell.NO_BORDER,
                    Colspan = 2,
                });

                tableSenores.AddCell(new PdfPCell(new Phrase(
                    (string)DsInvoiceAR.Tables["Customer"].Rows[0]["City"], fontTitle))
                {
                    Border = PdfPCell.NO_BORDER,
                });

                tableSenores.AddCell(new PdfPCell(new Phrase(
                    $"TEL:  {(string)DsInvoiceAR.Tables["Customer"].Rows[0]["PhoneNum"]}", fontTitle))
                {
                    Border = PdfPCell.NO_BORDER,
                });

                tableSenores.AddCell(new PdfPCell(new Phrase($"NIT. " +
                    $"{(string)DsInvoiceAR.Tables["Customer"].Rows[0]["ResaleID"]}-" /*+*/
                    /*$"{CalcularDigitoVerificacion((string)DsInvoiceAR.Tables["Customer"].Rows[0]["ResaleID"])}"*/, fontTitle))
                {
                    Border = PdfPCell.NO_BORDER,

                });

                tableSenores.AddCell(new PdfPCell(new Phrase(
                    $"COD:  {(string)DsInvoiceAR.Tables["Customer"].Rows[0]["ResaleID"]}", fontTitle))
                {
                    Border = PdfPCell.NO_BORDER,
                });

                tableInfoCliente.AddCell(new PdfPCell(tableSenores)
                {
                    Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER,
                    PaddingBottom = 10,
                });

                tableSenores = new PdfPTable(new float[] { 1, 2f }) { WidthPercentage = 100, };

                tableSenores.AddCell(new PdfPCell(new Phrase("FECHA DOCTO   :", fontTitle))
                {
                    Border = PdfPCell.NO_BORDER,
                    HorizontalAlignment = Element.ALIGN_LEFT,
                });

                tableSenores.AddCell(new PdfPCell(new Phrase(
                    $"{DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).ToString("dd-MMM-yyyy")}",
                    fontTitle))
                {
                    Border = PdfPCell.NO_BORDER,
                    HorizontalAlignment = Element.ALIGN_LEFT,
                });

                tableSenores.AddCell(new PdfPCell(new Phrase("VENCIMIENTO   :", fontTitle))
                {
                    Border = PdfPCell.NO_BORDER,
                    HorizontalAlignment = Element.ALIGN_LEFT,
                });

                tableSenores.AddCell(new PdfPCell(new Phrase(
                    $"{DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"]).ToString("dd-MMM-yyyy")}",
                    fontTitle))
                {
                    Border = PdfPCell.NO_BORDER,
                    HorizontalAlignment = Element.ALIGN_LEFT,
                });

                tableSenores.AddCell(new PdfPCell(new Phrase("PEDIDO   :", fontTitle))
                {
                    Border = PdfPCell.NO_BORDER,
                    HorizontalAlignment = Element.ALIGN_LEFT,
                });

                tableSenores.AddCell(new PdfPCell(new Phrase(
                    $"{(string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["PONum"]}",
                    fontTitle))
                {
                    Border = PdfPCell.NO_BORDER,
                    HorizontalAlignment = Element.ALIGN_LEFT,
                });

                tableSenores.AddCell(new PdfPCell(new Phrase("COND. DE PAGO :", fontTitle))
                {
                    Border = PdfPCell.NO_BORDER,
                    HorizontalAlignment = Element.ALIGN_LEFT,
                });

                tableSenores.AddCell(new PdfPCell(new Phrase(
                    $"{(string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["TermsCodeDescription"]}",
                    fontTitle))
                {
                    Border = PdfPCell.NO_BORDER,
                    HorizontalAlignment = Element.ALIGN_LEFT,
                });


                tableSenores.AddCell(new PdfPCell(new Phrase("VENDEDOR      :", fontTitle))
                {
                    Border = PdfPCell.NO_BORDER,
                    HorizontalAlignment = Element.ALIGN_LEFT,
                });

                tableSenores.AddCell(new PdfPCell(new Phrase(
                    $"{(string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["SalesRepName1"]}",
                    fontTitle))
                {
                    Border = PdfPCell.NO_BORDER,
                    HorizontalAlignment = Element.ALIGN_LEFT,
                });

                tableSenores.AddCell(new PdfPCell(new Phrase("TRANSPORTADOR :", fontTitle))
                {
                    Border = PdfPCell.NO_BORDER,
                    HorizontalAlignment = Element.ALIGN_LEFT,
                });

                tableSenores.AddCell(new PdfPCell(new Phrase(
                    $"{(string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar01"]}",
                    fontTitle))
                {
                    Border = PdfPCell.NO_BORDER,
                    HorizontalAlignment = Element.ALIGN_LEFT,
                });

                tableInfoCliente.AddCell(new PdfPCell(tableSenores)
                {
                    Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER,
                    PaddingBottom = 10,
                });

                #endregion

                #region UNIDADES
                PdfPTable tableUnidades = new PdfPTable(new float[] { 1, 3, 0.8F, 0.8f, 0.8f, 1.2f })
                {
                    WidthPercentage = 100,
                };

                tableUnidades.AddCell(new PdfPCell(new Phrase("REFERENCIA", fontTitle))
                {
                    Border = PdfPCell.BOTTOM_BORDER,
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    PaddingBottom = 10,
                });

                tableUnidades.AddCell(new PdfPCell(new Phrase("DESCRIPCIÓN", fontTitle))
                {
                    Border = PdfPCell.BOTTOM_BORDER,
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    PaddingBottom = 10,
                });

                tableUnidades.AddCell(new PdfPCell(new Phrase("CANTIDAD", fontTitle))
                {
                    Border = PdfPCell.BOTTOM_BORDER,
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    PaddingBottom = 10,
                });

                tableUnidades.AddCell(new PdfPCell(new Phrase("PRECIO", fontTitle))
                {
                    Border = PdfPCell.BOTTOM_BORDER,
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    PaddingBottom = 10,
                });

                tableUnidades.AddCell(new PdfPCell(new Phrase("IVA RET.", fontTitle))
                {
                    Border = PdfPCell.BOTTOM_BORDER,
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    PaddingBottom = 10,
                });

                tableUnidades.AddCell(new PdfPCell(new Phrase("VALOR", fontTitle))
                {
                    Border = PdfPCell.BOTTOM_BORDER,
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    PaddingBottom = 10,
                });

                decimal totalCantidad = 0;
                foreach (DataRow InvoiceLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
                {
                    totalCantidad += decimal.Parse((string)InvoiceLine["SellingShipQty"]);
                    if (!AddUnidadesc(InvoiceLine, ref tableUnidades, fontCustom, DsInvoiceAR))
                        return false;
                }

                var cantEspacios = 20 - DsInvoiceAR.Tables["InvcDtl"].Rows.Count;

                for (int i = 0; i < cantEspacios; i++)
                    tableUnidades.AddCell(new PdfPCell(new Phrase(" ")) { Colspan = 6, Border = PdfPCell.NO_BORDER, });

                tableUnidades.AddCell(new PdfPCell(new Phrase("Valor Gravado", fontCustom))
                {
                    Border = PdfPCell.NO_BORDER,
                    PaddingBottom = 10,
                });

                var subtotalGravado = decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubTotal"]) -
                   decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number01"]);

                tableUnidades.AddCell(new PdfPCell(new Phrase(
                    /*decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubTotal"])*/subtotalGravado.ToString("N0"),
                    fontCustom))
                {
                    Border = PdfPCell.NO_BORDER,
                    PaddingBottom = 10,
                });

                tableUnidades.AddCell(new PdfPCell(new Phrase(
                    $"TOTAL CANT. {totalCantidad.ToString("N0")}", fontTitle))
                {
                    Border = PdfPCell.NO_BORDER,
                    Colspan = 4,
                    PaddingBottom = 10,
                });

                #endregion

                #region RESOLUCIONES Y TOTALES
                PdfPTable tableResTotales = new PdfPTable(new float[] { 2, 1 }) { WidthPercentage = 100, };

                PdfPTable tableResolucion = new PdfPTable(1);
                //Text grandes contribuyentes
                tableResolucion.AddCell(new PdfPCell(new Phrase(
                    (string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar02"],
                    fontTitle))
                {
                    PaddingBottom = 7,
                    Border = PdfPCell.NO_BORDER,
                });

                //Rangos
                tableResolucion.AddCell(new PdfPCell(new Phrase(
                    (string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar03"],
                    fontCustom))
                {
                    PaddingBottom = 7,
                    Border = PdfPCell.NO_BORDER,
                });
                //Text consignar en cta
                tableResolucion.AddCell(new PdfPCell(new Phrase(
                    (string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar04"], fontTitle))
                {
                    PaddingBottom = 7,
                    Border = PdfPCell.NO_BORDER,
                });

                tableResolucion.AddCell(new PdfPCell(new Phrase("Esta factura se asimila en todos sus efectos a una " +
                    "letra de cambio. Art. 774", fontCustom))
                {
                    PaddingBottom = 7,
                    Border = PdfPCell.NO_BORDER,
                });

                tableResTotales.AddCell(new PdfPCell(tableResolucion));

                PdfPTable tableTotales = new PdfPTable(2);

                tableTotales.AddCell(new PdfPCell(new Phrase("SUBTOTAL", fontTitle))
                {
                    Border = PdfPCell.NO_BORDER,
                    PaddingBottom = 10,
                });

                tableTotales.AddCell(new PdfPCell(new Phrase(
                    decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubTotal"]).ToString("N0"),
                    fontTitle))
                {
                    Border = PdfPCell.NO_BORDER,
                    PaddingBottom = 10,
                });

                tableTotales.AddCell(new PdfPCell(new Phrase("DESCTO", fontTitle))
                {
                    Border = PdfPCell.NO_BORDER,
                    PaddingBottom = 10,
                });

                var descuentoVario = decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number01"]);
                tableTotales.AddCell(new PdfPCell(new Phrase(
                    descuentoVario.ToString("N0"),
                    fontTitle))
                {
                    Border = PdfPCell.NO_BORDER,
                    PaddingBottom = 10,
                });

                tableTotales.AddCell(new PdfPCell(new Phrase("I.V.A", fontTitle))
                {
                    Border = PdfPCell.NO_BORDER,
                    PaddingBottom = 10,
                });

                var totalIva = GetValImpuestoByID("01", DsInvoiceAR);
                tableTotales.AddCell(new PdfPCell(new Phrase(
                    totalIva.ToString("N0"),
                    fontTitle))
                {
                    Border = PdfPCell.NO_BORDER,
                    PaddingBottom = 10,
                });

                tableTotales.AddCell(new PdfPCell(new Phrase("RET. IVA", fontTitle))
                {
                    Border = PdfPCell.NO_BORDER,
                    PaddingBottom = 10,
                });
                var reteIva = GetValImpuestoByID("0B", DsInvoiceAR);
                tableTotales.AddCell(new PdfPCell(new Phrase(
                    reteIva.ToString("N0"),
                    fontTitle))
                {
                    Border = PdfPCell.NO_BORDER,
                    PaddingBottom = 10,
                });

                tableTotales.AddCell(new PdfPCell(new Phrase("NETO", fontTitle))
                {
                    Border = PdfPCell.NO_BORDER,
                    PaddingBottom = 10,
                });

                var totalNeto = (subtotalGravado + totalIva) - reteIva;
                tableTotales.AddCell(new PdfPCell(new Phrase(
                    /*decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"])*/totalNeto.ToString("N0"),
                    fontTitle))
                {
                    Border = PdfPCell.NO_BORDER,
                    PaddingBottom = 10,
                });

                tableTotales.AddCell(new PdfPCell(new Phrase("_________________________\nFIRMA DEL CLIENTE:", fontTitle))
                {
                    Border = PdfPCell.NO_BORDER,
                    PaddingBottom = 10,
                    PaddingTop = 5,
                    Colspan = 2,
                });

                tableResTotales.AddCell(new PdfPCell(tableTotales) { PaddingLeft = 7 });
                #endregion

                document.Add(tableEncabezado);
                document.Add(divEspacio);
                document.Add(tableInfoCliente);
                document.Add(tableUnidades);
                document.Add(tableResTotales);

                /*PIE DE PAGINA*/
                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();

                AddPageNumber($"{Ruta}{NomArchivo}", 555f, 775f, fontTitle);
                RutaPdf = NomArchivo;
                return true;
            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.Message;
                return false;
            }
        }

        public bool FacturaNotaCreditoc(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {
            NomArchivo = string.Empty;
            string Ruta = HttpContext.Current.Server.MapPath("~/PDF/" + NIT + "/");
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            string RutaImg = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";

            NomArchivo = NombreInvoice + ".pdf";

            try
            {
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.
                Document document = new Document(/*iTextSharp.text.PageSize.LETTER, 20f, 15f, 30f, 30f*/);
                document.SetPageSize(PageSize.A4);
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                document.Open();
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;

                PdfPCell celEspacio = new PdfPCell()
                {
                    Border = PdfPCell.NO_BORDER
                };

                PdfDiv divEspacio = new PdfDiv();
                divEspacio.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio.Height = 10;
                divEspacio.Width = 130;

                PdfDiv divEspacio2 = new PdfDiv();
                divEspacio2.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio2.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio2.Height = 2;
                divEspacio2.Width = 130;

                //FUENTES----------------------------------------------------------------------------------------------------------
                iTextSharp.text.Font fontTitleBold = FontFactory.GetFont(FontFactory.COURIER_BOLD, 8.5f, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.COURIER, 8.5f, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustom = FontFactory.GetFont(FontFactory.COURIER, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustomBold = FontFactory.GetFont(FontFactory.COURIER_BOLD, 7, iTextSharp.text.Font.NORMAL);
                //FUENTES---------------------------------------------------------------------------------------------------------

                #region ENCABEZADO
                PdfPTable tableEncabezado = new PdfPTable(new float[] { 2, 1, 2f });
                tableEncabezado.WidthPercentage = 100;

                //TODO: informacion empresa
                PdfPTable tableInfoEmpresa = new PdfPTable(1)
                {
                    WidthPercentage = 100,
                };

                tableInfoEmpresa.AddCell(new PdfPCell(new Phrase((string)DsInvoiceAR.Tables["Company"].Rows[0]["Name"], fontTitle))
                { Border = PdfPCell.NO_BORDER });

                tableInfoEmpresa.AddCell(new PdfPCell(new Phrase(
                    $"NIT:      " +
                    $"{(string)DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"]}", fontTitle))
                { Border = PdfPCell.NO_BORDER });

                tableInfoEmpresa.AddCell(new PdfPCell(new Phrase(
                    $"Dirección  :  " +
                    $"{(string)DsInvoiceAR.Tables["Company"].Rows[0]["Address1"]}", fontTitle))
                { Border = PdfPCell.NO_BORDER });

                tableInfoEmpresa.AddCell(new PdfPCell(new Phrase($"Teléfono   :     " +
                    $"{(string)DsInvoiceAR.Tables["Company"].Rows[0]["PhoneNum"]}  " +
                    $"{(string)DsInvoiceAR.Tables["Company"].Rows[0]["City"]}",
                    fontTitle))
                { Border = PdfPCell.NO_BORDER });

                tableEncabezado.AddCell(new PdfPCell(tableInfoEmpresa) { Border = PdfPCell.NO_BORDER, });

                //TODO: Celda de CUFE y QR
                PdfDiv divQR = new PdfDiv()
                {
                    Float = iTextSharp.text.pdf.PdfDiv.FloatType.LEFT,
                    Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE,
                    BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID,
                    Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK,
                    Height = 90,
                    Width = 90,
                    //BackgroundColor=BaseColor.LIGHT_GRAY,
                };

                iTextSharp.text.Image ImgQR = iTextSharp.text.Image.GetInstance(QRInvoice, BaseColor.WHITE);
                ImgQR.Alignment = Element.ALIGN_RIGHT;
                divQR.BackgroundImage = ImgQR;

                PdfPCell celQR = new PdfPCell()
                {
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    VerticalAlignment = Element.ALIGN_TOP,
                    //Colspan = 3,
                    Border = 0,
                    Padding = 3
                };
                //celQR.AddElement(new Paragraph("CUFE {***}", fontTitleBold));
                celQR.AddElement(divQR);
                tableEncabezado.AddCell(celQR);

                //TODO: Informacion factura
                PdfPTable tableInfoFactura = new PdfPTable(1);

                tableInfoFactura.AddCell(new PdfPCell(new Phrase($"NC VENTA         <R>     " +
                    $"{(string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"]}", fontTitle))
                {
                    Border = PdfPCell.NO_BORDER,
                    HorizontalAlignment = Element.ALIGN_RIGHT,
                });

                tableInfoFactura.AddCell(new PdfPCell(new Phrase("PAG:  ", fontTitle))
                {
                    HorizontalAlignment = Element.ALIGN_RIGHT,
                    Border = PdfPCell.NO_BORDER,
                    PaddingTop = 10,
                    PaddingRight = 9,
                });
                tableEncabezado.AddCell(new PdfPCell(tableInfoFactura) { Border = PdfPCell.NO_BORDER, });

                //CELDA DE CUFE
                tableEncabezado.AddCell(new PdfPCell(new Phrase($"CUFE: {CUFE}", fontTitleBold))
                {
                    Border = PdfPCell.NO_BORDER,
                    Colspan = 3,
                    Padding = 5,
                    HorizontalAlignment = Element.ALIGN_CENTER,
                });

                tableEncabezado.AddCell(new PdfPCell(new Phrase($"Representación gráfica factura electrónica", fontTitleBold))
                {
                    Border = PdfPCell.NO_BORDER,
                    Colspan = 3,
                    Padding = 5,
                    HorizontalAlignment = Element.ALIGN_CENTER,
                });
                #endregion

                #region DATOS CLIENTE

                var tableInfoCliente = new PdfPTable(2)
                {
                    WidthPercentage = 100,
                };

                var tableSenores = new PdfPTable(2)
                {
                    WidthPercentage = 100,
                };

                tableSenores.AddCell(new PdfPCell(new Phrase("SEÑORES", fontTitle))
                {
                    Border = PdfPCell.NO_BORDER,
                    Colspan = 2,
                });

                tableSenores.AddCell(new PdfPCell(new Phrase(
                    (string)DsInvoiceAR.Tables["Customer"].Rows[0]["Name"], fontTitle))
                {
                    Border = PdfPCell.NO_BORDER,
                    Colspan = 2,
                });

                tableSenores.AddCell(new PdfPCell(new Phrase(
                    (string)DsInvoiceAR.Tables["Customer"].Rows[0]["Address1"], fontTitle))
                {
                    Border = PdfPCell.NO_BORDER,
                    Colspan = 2,
                });

                tableSenores.AddCell(new PdfPCell(new Phrase(
                    (string)DsInvoiceAR.Tables["Customer"].Rows[0]["City"], fontTitle))
                {
                    Border = PdfPCell.NO_BORDER,
                });

                tableSenores.AddCell(new PdfPCell(new Phrase(
                    $"TEL:  {(string)DsInvoiceAR.Tables["Customer"].Rows[0]["PhoneNum"]}", fontTitle))
                {
                    Border = PdfPCell.NO_BORDER,
                });

                tableSenores.AddCell(new PdfPCell(new Phrase($"NIT. " +
                    $"{(string)DsInvoiceAR.Tables["Customer"].Rows[0]["ResaleID"]}-" +
                    $"{CalcularDigitoVerificacion((string)DsInvoiceAR.Tables["Customer"].Rows[0]["ResaleID"])}", fontTitle))
                {
                    Border = PdfPCell.NO_BORDER,

                });

                tableSenores.AddCell(new PdfPCell(new Phrase(
                    $"COD:  {(string)DsInvoiceAR.Tables["Customer"].Rows[0]["ResaleID"]}", fontTitle))
                {
                    Border = PdfPCell.NO_BORDER,
                });

                tableInfoCliente.AddCell(new PdfPCell(tableSenores)
                {
                    Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER,
                    PaddingBottom = 10,
                });

                tableSenores = new PdfPTable(new float[] { 1, 2f }) { WidthPercentage = 100, };

                tableSenores.AddCell(new PdfPCell(new Phrase("FECHA DOCTO   :", fontTitle))
                {
                    Border = PdfPCell.NO_BORDER,
                    HorizontalAlignment = Element.ALIGN_LEFT,
                });

                tableSenores.AddCell(new PdfPCell(new Phrase(
                    $"{DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).ToString("dd-MMM-yyyy")}",
                    fontTitle))
                {
                    Border = PdfPCell.NO_BORDER,
                    HorizontalAlignment = Element.ALIGN_LEFT,
                });

                tableSenores.AddCell(new PdfPCell(new Phrase("VENCIMIENTO   :", fontTitle))
                {
                    Border = PdfPCell.NO_BORDER,
                    HorizontalAlignment = Element.ALIGN_LEFT,
                });

                tableSenores.AddCell(new PdfPCell(new Phrase(
                    $"{DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"]).ToString("dd-MMM-yyyy")}",
                    fontTitle))
                {
                    Border = PdfPCell.NO_BORDER,
                    HorizontalAlignment = Element.ALIGN_LEFT,
                });

                tableSenores.AddCell(new PdfPCell(new Phrase("PEDIDO   :", fontTitle))
                {
                    Border = PdfPCell.NO_BORDER,
                    HorizontalAlignment = Element.ALIGN_LEFT,
                });

                tableSenores.AddCell(new PdfPCell(new Phrase(
                    $"{(string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["PONum"]}",
                    fontTitle))
                {
                    Border = PdfPCell.NO_BORDER,
                    HorizontalAlignment = Element.ALIGN_LEFT,
                });

                tableSenores.AddCell(new PdfPCell(new Phrase("COND. DE PAGO :", fontTitle))
                {
                    Border = PdfPCell.NO_BORDER,
                    HorizontalAlignment = Element.ALIGN_LEFT,
                });

                tableSenores.AddCell(new PdfPCell(new Phrase(
                    $"{(string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["TermsCodeDescription"]}",
                    fontTitle))
                {
                    Border = PdfPCell.NO_BORDER,
                    HorizontalAlignment = Element.ALIGN_LEFT,
                });


                tableSenores.AddCell(new PdfPCell(new Phrase("VENDEDOR      :", fontTitle))
                {
                    Border = PdfPCell.NO_BORDER,
                    HorizontalAlignment = Element.ALIGN_LEFT,
                });

                tableSenores.AddCell(new PdfPCell(new Phrase(
                    $"{(string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["SalesRepName1"]}",
                    fontTitle))
                {
                    Border = PdfPCell.NO_BORDER,
                    HorizontalAlignment = Element.ALIGN_LEFT,
                });

                tableSenores.AddCell(new PdfPCell(new Phrase("TRANSPORTADOR :", fontTitle))
                {
                    Border = PdfPCell.NO_BORDER,
                    HorizontalAlignment = Element.ALIGN_LEFT,
                });

                tableSenores.AddCell(new PdfPCell(new Phrase(
                    $"{(string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar01"]}",
                    fontTitle))
                {
                    Border = PdfPCell.NO_BORDER,
                    HorizontalAlignment = Element.ALIGN_LEFT,
                });

                tableInfoCliente.AddCell(new PdfPCell(tableSenores)
                {
                    Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER,
                    PaddingBottom = 10,
                });

                #endregion

                #region UNIDADES
                PdfPTable tableUnidades = new PdfPTable(new float[] { 1, 3, 0.8F, 0.8f, 0.8f, 1.2f })
                {
                    WidthPercentage = 100,
                };

                tableUnidades.AddCell(new PdfPCell(new Phrase("REFERENCIA", fontTitle))
                {
                    Border = PdfPCell.BOTTOM_BORDER,
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    PaddingBottom = 10,
                });

                tableUnidades.AddCell(new PdfPCell(new Phrase("DESCRIPCIÓN", fontTitle))
                {
                    Border = PdfPCell.BOTTOM_BORDER,
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    PaddingBottom = 10,
                });

                tableUnidades.AddCell(new PdfPCell(new Phrase("CANTIDAD", fontTitle))
                {
                    Border = PdfPCell.BOTTOM_BORDER,
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    PaddingBottom = 10,
                });

                tableUnidades.AddCell(new PdfPCell(new Phrase("PRECIO", fontTitle))
                {
                    Border = PdfPCell.BOTTOM_BORDER,
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    PaddingBottom = 10,
                });

                tableUnidades.AddCell(new PdfPCell(new Phrase("IVA RET.", fontTitle))
                {
                    Border = PdfPCell.BOTTOM_BORDER,
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    PaddingBottom = 10,
                });

                tableUnidades.AddCell(new PdfPCell(new Phrase("VALOR", fontTitle))
                {
                    Border = PdfPCell.BOTTOM_BORDER,
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    PaddingBottom = 10,
                });

                decimal totalCantidad = 0;
                foreach (DataRow InvoiceLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
                {
                    totalCantidad += decimal.Parse((string)InvoiceLine["SellingShipQty"]);
                    if (!AddUnidadesPrimordial(InvoiceLine, ref tableUnidades, fontCustom, DsInvoiceAR))
                        return false;
                }

                var cantEspacios = 20 - DsInvoiceAR.Tables["InvcDtl"].Rows.Count;

                for (int i = 0; i < cantEspacios; i++)
                    tableUnidades.AddCell(new PdfPCell(new Phrase(" ")) { Colspan = 6, Border = PdfPCell.NO_BORDER, });

                tableUnidades.AddCell(new PdfPCell(new Phrase("Valor Gravado", fontCustom))
                {
                    Border = PdfPCell.NO_BORDER,
                    PaddingBottom = 10,
                });

                var subtotalGravado = decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubTotal"]) -
                decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number01"]);
                tableUnidades.AddCell(new PdfPCell(new Phrase(
                    /*decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubTotal"])*/subtotalGravado.ToString("N0"),
                    fontCustom))
                {
                    Border = PdfPCell.NO_BORDER,
                    PaddingBottom = 10,
                });

                tableUnidades.AddCell(new PdfPCell(new Phrase(
                    $"TOTAL CANT. {totalCantidad.ToString("N0")}", fontTitle))
                {
                    Border = PdfPCell.NO_BORDER,
                    Colspan = 4,
                    PaddingBottom = 10,
                });

                #endregion

                #region RESOLUCIONES Y TOTALES
                PdfPTable tableResTotales = new PdfPTable(new float[] { 2, 1 }) { WidthPercentage = 100, };

                PdfPTable tableResolucion = new PdfPTable(1);

                //Text grandes contribuyentes
                tableResolucion.AddCell(new PdfPCell(new Phrase(
                    (string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar02"],
                    fontTitle))
                {
                    PaddingBottom = 7,
                    Border = PdfPCell.NO_BORDER,
                });

                //Rangos
                tableResolucion.AddCell(new PdfPCell(new Phrase(
                    (string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar03"],
                    fontCustom))
                {
                    PaddingBottom = 7,
                    Border = PdfPCell.NO_BORDER,
                });
                //Text consignar en cta
                tableResolucion.AddCell(new PdfPCell(new Phrase(
                    (string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar04"], fontTitle))
                {
                    PaddingBottom = 7,
                    Border = PdfPCell.NO_BORDER,
                });

                tableResolucion.AddCell(new PdfPCell(new Phrase("Esta factura se asimila en todos sus efectos a una " +
                    "letra de cambio. Art. 774", fontCustom))
                {
                    PaddingBottom = 7,
                    Border = PdfPCell.NO_BORDER,
                });

                tableResTotales.AddCell(new PdfPCell(tableResolucion));

                PdfPTable tableTotales = new PdfPTable(2);

                tableTotales.AddCell(new PdfPCell(new Phrase("SUBTOTAL", fontTitle))
                {
                    Border = PdfPCell.NO_BORDER,
                    PaddingBottom = 10,
                });

                tableTotales.AddCell(new PdfPCell(new Phrase(
                    decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubTotal"]).ToString("N0"),
                    fontTitle))
                {
                    Border = PdfPCell.NO_BORDER,
                    PaddingBottom = 10,
                });

                tableTotales.AddCell(new PdfPCell(new Phrase("DESCTO", fontTitle))
                {
                    Border = PdfPCell.NO_BORDER,
                    PaddingBottom = 10,
                });

                var descuentoVario = decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number01"]);
                tableTotales.AddCell(new PdfPCell(new Phrase(
                    descuentoVario.ToString("N0"),
                    fontTitle))
                {
                    Border = PdfPCell.NO_BORDER,
                    PaddingBottom = 10,
                });

                tableTotales.AddCell(new PdfPCell(new Phrase("I.V.A", fontTitle))
                {
                    Border = PdfPCell.NO_BORDER,
                    PaddingBottom = 10,
                });

                var totalIva = GetValImpuestoByID("01", DsInvoiceAR);
                tableTotales.AddCell(new PdfPCell(new Phrase(
                    totalIva.ToString("N0"),
                    fontTitle))
                {
                    Border = PdfPCell.NO_BORDER,
                    PaddingBottom = 10,
                });

                tableTotales.AddCell(new PdfPCell(new Phrase("RET. IVA", fontTitle))
                {
                    Border = PdfPCell.NO_BORDER,
                    PaddingBottom = 10,
                });

                var reteIva = GetValImpuestoByID("0B", DsInvoiceAR);
                tableTotales.AddCell(new PdfPCell(new Phrase(
                    reteIva.ToString("N0"),
                    fontTitle))
                {
                    Border = PdfPCell.NO_BORDER,
                    PaddingBottom = 10,
                });

                tableTotales.AddCell(new PdfPCell(new Phrase("NETO", fontTitle))
                {
                    Border = PdfPCell.NO_BORDER,
                    PaddingBottom = 10,
                });

                var totalNeto = (subtotalGravado + totalIva) - reteIva;
                tableTotales.AddCell(new PdfPCell(new Phrase(
                    /*decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"])*/totalNeto.ToString("N0"),
                    fontTitle))
                {
                    Border = PdfPCell.NO_BORDER,
                    PaddingBottom = 10,
                });

                tableTotales.AddCell(new PdfPCell(new Phrase("_________________________\nFIRMA DEL CLIENTE:", fontTitle))
                {
                    Border = PdfPCell.NO_BORDER,
                    PaddingBottom = 10,
                    PaddingTop = 5,
                    Colspan = 2,
                });

                tableResTotales.AddCell(new PdfPCell(tableTotales) { PaddingLeft = 7 });
                #endregion

                document.Add(tableEncabezado);
                document.Add(divEspacio);
                document.Add(tableInfoCliente);
                document.Add(tableUnidades);
                document.Add(tableResTotales);

                /*PIE DE PAGINA*/
                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();
                AddPageNumber($"{Ruta}{NomArchivo}", 555f, 775f, fontTitle);
                RutaPdf = NomArchivo;
                return true;
            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.Message;
                return false;
            }
        }

        private bool AddUnidadesc(DataRow dataLine, ref PdfPTable table, iTextSharp.text.Font font, DataSet dataSet)
        {
            try
            {
                table.AddCell(new PdfPCell(new Phrase((string)dataLine["PartNum"], font))
                {
                    Border = PdfPCell.NO_BORDER,
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    VerticalAlignment = Element.ALIGN_CENTER,
                });

                table.AddCell(new PdfPCell(new Phrase((string)dataLine["LineDesc"], font))
                {
                    Border = PdfPCell.NO_BORDER,
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    VerticalAlignment = Element.ALIGN_CENTER,
                });

                table.AddCell(new PdfPCell(new Phrase((string)dataLine["SellingShipQty"], font))
                {
                    Border = PdfPCell.NO_BORDER,
                    HorizontalAlignment = Element.ALIGN_RIGHT,
                    VerticalAlignment = Element.ALIGN_CENTER,
                });

                table.AddCell(new PdfPCell(new Phrase(decimal.Parse((string)dataLine["DocUnitPrice"]).ToString("N0"), font))
                {
                    Border = PdfPCell.NO_BORDER,
                    HorizontalAlignment = Element.ALIGN_RIGHT,
                    VerticalAlignment = Element.ALIGN_CENTER,
                });

                table.AddCell(new PdfPCell(new Phrase(
                    GetPercentIdImpDIAN((string)dataLine["InvoiceNum"], (string)dataLine["InvoiceLine"],
                    dataSet.Tables["InvcTax"]), font))
                {
                    Border = PdfPCell.NO_BORDER,
                    HorizontalAlignment = Element.ALIGN_RIGHT,
                    VerticalAlignment = Element.ALIGN_CENTER,
                });

                table.AddCell(new PdfPCell(new Phrase(decimal.Parse((string)dataLine["DspDocExtPrice"]).ToString("N0"), font))
                {
                    Border = PdfPCell.NO_BORDER,
                    HorizontalAlignment = Element.ALIGN_RIGHT,
                    VerticalAlignment = Element.ALIGN_CENTER,
                });
                return true;
            }
            catch (Exception ex)
            {
                strError += "Error al crear columnas de tabla Items: " + ex.Message;
                return false;
            }
        }

        #endregion
    }
}
