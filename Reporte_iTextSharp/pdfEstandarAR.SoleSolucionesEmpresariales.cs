﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
//Referenciar y usar.
using System.Data;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Web;
using System.Windows.Forms;

namespace RulesServicesDIAN2.Adquiriente
{
    public partial class pdfEstandarAR
    {

        #region  Unidades

        private bool AddUnidadeSoleSolucionesEmpresariales(DataRow dataLine, ref PdfPTable table, iTextSharp.text.Font fontTitleFactura, DataSet dataSet)
        {
            try
            {
                //CODIGO
                iTextSharp.text.pdf.PdfPCell celValL = new iTextSharp.text.pdf.PdfPCell(new Phrase((string)dataLine["PartNum"], fontTitleFactura));
                celValL.Colspan = 1;
                celValL.Padding = 2;
                celValL.Border = PdfPCell.LEFT_BORDER | PdfPCell.RIGHT_BORDER;
                celValL.HorizontalAlignment = Element.ALIGN_CENTER;
                celValL.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValL);
                strError += "InvoiceLine OK";

                //DESCRIPCIÓN ARTÍCULO
                iTextSharp.text.pdf.PdfPCell celValCodigo = new iTextSharp.text.pdf.PdfPCell(new Phrase((string)dataLine["LineDesc"], fontTitleFactura));
                celValCodigo.Colspan = 1;
                celValCodigo.Padding = 2;
                celValCodigo.Border = PdfPCell.RIGHT_BORDER;
                //celValCodigo.BorderWidthLeft = 1;
                //celDescripcion.BorderColorBottom = BaseColor.WHITE;
                celValCodigo.HorizontalAlignment = Element.ALIGN_LEFT;
                celValCodigo.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValCodigo);
                strError += "Add PartNum OK";

                //CANTIDAD
                iTextSharp.text.pdf.PdfPCell celValCantidad = new iTextSharp.text.pdf.PdfPCell(new Phrase((string)dataLine["PartNum"], fontTitleFactura));
                celValCantidad.Colspan = 1;
                celValCantidad.Padding = 2;
                celValCantidad.Border = PdfPCell.RIGHT_BORDER;
                //celValDesc.BorderWidthLeft = 1;
                celValCantidad.HorizontalAlignment = Element.ALIGN_RIGHT;
                celValCantidad.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValCantidad);
                strError += "Add LineDesc OK";

                //%iva
                iTextSharp.text.pdf.PdfPCell celVaIva = new iTextSharp.text.pdf.PdfPCell(new Phrase((string)dataLine["PartNum"], fontTitleFactura));
                celVaIva.Colspan = 1;
                celVaIva.Padding = 2;
                celVaIva.Border = PdfPCell.RIGHT_BORDER;
                //celValDesc.BorderWidthLeft = 1;
                celVaIva.HorizontalAlignment = Element.ALIGN_RIGHT;
                celVaIva.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celVaIva);
                strError += "Add LineDesc OK";


                //VR. total
                iTextSharp.text.pdf.PdfPCell celVatotal = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    decimal.Parse((string)dataLine["DocExtPrice"]).ToString("N0"), fontTitleFactura));
                celVatotal.Colspan = 1;
                celVatotal.Padding = 2;
                celVatotal.Border = PdfPCell.RIGHT_BORDER;
                //celValCantidad.BorderWidthLeft = 1;
                celVatotal.HorizontalAlignment = Element.ALIGN_RIGHT;
                celVatotal.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celVatotal);
                strError += "Add SellingShipQty OK";

                return true;
            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.Message;
                return false;
            }
        }

        #endregion

        #region Factura de ventas 
        public bool FacturadeVentasSoleSolucionesEmpresariales(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {
            NomArchivo = string.Empty;
            string Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
            string RutaImg = $@"{AppDomain.CurrentDomain.BaseDirectory}\LOGO_Sole_SolucionesE.png";
            string RutaMarcaAgua = $@"{AppDomain.CurrentDomain.BaseDirectory}\LOGO_Sole_SolucionesE.png";
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            NomArchivo = NombreInvoice + ".pdf";
            NomArchivo = NombreInvoice + ".pdf";

            //NomArchivo = string.Empty;
            //string Ruta = HttpContext.Current.Server.MapPath("~/PDF/" + NIT + "/");
            //string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            //string RutaImg = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";
            //string RutaMarcaAgua = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";
            //NomArchivo = NombreInvoice + ".pdf";
            try
            {
                #region Head
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.
                Document document = new Document(iTextSharp.text.PageSize.LETTER);
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;

                // step 3: we open the document     
                document.Open();

                PdfDiv divEspacio = new PdfDiv();
                divEspacio.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio.Height = 10;
                divEspacio.Width = 130;

                PdfDiv divEspacio2 = new PdfDiv();
                divEspacio2.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio2.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio2.Height = 2;
                divEspacio2.Width = 130;

                //FUENTES----------------------------------------------------------------------------------------------------------
                iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle3 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 9, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle2 = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustom = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustom2 = FontFactory.GetFont(FontFactory.HELVETICA, 4, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitleFactura = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                //FUENTES---------------------------------------------------------------------------------------------------------

                System.Drawing.Image logo = null;
                var requestLogo = WebRequest.Create(RutaMarcaAgua);

                using (var responseLogo = requestLogo.GetResponse())
                using (var streamLogo = responseLogo.GetResponseStream())
                {
                    logo = Bitmap.FromStream(streamLogo);
                }

                System.Drawing.Image logo2 = null;
                var requestLogo2 = WebRequest.Create(RutaImg);

                using (var responseLogo2 = requestLogo2.GetResponse())
                using (var streamLogo2 = responseLogo2.GetResponseStream())
                {
                    logo2 = Bitmap.FromStream(streamLogo2);
                }

                iTextSharp.text.Image ImgLogo = iTextSharp.text.Image.GetInstance(logo2, BaseColor.WHITE);
                ImgLogo.ScaleAbsolute(90, 90);
                ImgLogo.Border = 0;

                iTextSharp.text.Image QRPdf = iTextSharp.text.Image.GetInstance(QRInvoice, BaseColor.WHITE);
                QRPdf.ScaleAbsolute(80, 80);
                QRPdf.Border = 0;

                #endregion

                /////saltos de trabla 
                PdfPTable salto = new PdfPTable(1);
                salto.WidthPercentage = 100;
                PdfPCell vacio = new PdfPCell() { MinimumHeight = 2, Border = 0, };
                salto.AddCell(vacio);
                ////-------------------------------------

                #region Header

                PdfPTable header = new PdfPTable(new float[] { 0.15f, 0.20f, 0.13f, 0.13f, 0.19f, });
                header.WidthPercentage = 100;
                //agregamos logo

                PdfPCell cell_logo = new PdfPCell() { Border = 0, };
                cell_logo.AddElement(ImgLogo);
                header.AddCell(cell_logo);

                //info de factura 
                PdfPCell cell_info = new PdfPCell() { Border = 0, };
                Paragraph prgTitle = new Paragraph(string.Format($""+DsInvoiceAR.Tables["Company"].Rows[0]["Name"],
                    $"NIT."+ DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"]), fontTitle);
                Paragraph prgTitle2 = new Paragraph(string.Format("{0}\n{1}\n{2} - {3}\nwww.SoleSolucionesEmpresariales.com",
                    DsInvoiceAR.Tables["Company"].Rows[0]["Address1"].ToString(),
                    DsInvoiceAR.Tables["Company"].Rows[0]["PhoneNum"].ToString(),
                   $"Tel: "+ DsInvoiceAR.Tables["Company"].Rows[0]["City"].ToString(),
                    DsInvoiceAR.Tables["Company"].Rows[0]["State"].ToString()),
                    fontCustom);
                prgTitle.Alignment = Element.ALIGN_CENTER;
                prgTitle2.Alignment = Element.ALIGN_CENTER;
                cell_info.AddElement(prgTitle);
                cell_info.AddElement(prgTitle2);
                header.AddCell(cell_info);

                //info sucursal 1
                PdfPCell celll_suc_1 = new PdfPCell() { Border=0};
                Paragraph prgTitle3 = new Paragraph(string.Format($"Medellín", fontTitle));
                Paragraph prgTitle4 = new Paragraph(string.Format(
                DsInvoiceAR.Tables["Company"].Rows[0]["PhoneNum"].ToString(), fontCustom2));
                prgTitle3.Alignment = Element.ALIGN_CENTER;
                prgTitle4.Alignment = Element.ALIGN_CENTER;
                celll_suc_1.AddElement(prgTitle3);
                celll_suc_1.AddElement(prgTitle4);
                header.AddCell(celll_suc_1);

                //info sucursal 2
                PdfPCell celll_suc_2 = new PdfPCell() { Border=0};
                Paragraph prgTitle5 = new Paragraph(string.Format("Bogotá", fontTitle));
                Paragraph prgTitle6 = new Paragraph(string.Format(
                DsInvoiceAR.Tables["Company"].Rows[0]["PhoneNum"].ToString(), fontCustom2));
                prgTitle5.Alignment = Element.ALIGN_CENTER;
                prgTitle6.Alignment = Element.ALIGN_CENTER;
                celll_suc_2.AddElement(prgTitle5);
                celll_suc_2.AddElement(prgTitle6);
                header.AddCell(celll_suc_2);

                ///info de factura como numero 
                PdfPCell cell_factura = new PdfPCell() { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, };
                Paragraph prgTitle7 = new Paragraph(string.Format("FACTURA DE VENTA", fontTitle));
                Paragraph prgTitle8 = new Paragraph(string.Format(
                DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"].ToString(), fontTitle));
                prgTitle7.Alignment = Element.ALIGN_CENTER;
                prgTitle8.Alignment = Element.ALIGN_CENTER;
                cell_factura.AddElement(prgTitle7);
                cell_factura.AddElement(prgTitle8);
                header.AddCell(cell_factura);

                PdfPTable encabezado = new PdfPTable(1);
                encabezado.WidthPercentage = 100;
                PdfPCell cabecera = new PdfPCell() { Border = 0, };

                PdfPTable Advanced = new PdfPTable(2);
                Advanced.WidthPercentage = 100;

                PdfPCell info_inicio = new PdfPCell();
                //agregamos el nimbre de la empresa
                PdfPTable nombre = new PdfPTable(1);
                nombre.WidthPercentage = 100;
                PdfPCell nombre_em = new PdfPCell(new Phrase(DsInvoiceAR.Tables["Customer"].Rows[0]["Name"].ToString(), fontTitle2)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP, MinimumHeight = 50, };
                nombre.AddCell(nombre_em);
                info_inicio.AddElement(nombre);

                //agregamos el codigo postal de la empresa
                PdfPTable c_postal = new PdfPTable(1);
                c_postal.WidthPercentage = 100;
                PdfPCell postal_ = new PdfPCell(new Phrase("Direccon: " + DsInvoiceAR.Tables["Company"].Rows[0]["Address1"].ToString(), fontTitle2)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP, };
                c_postal.AddCell(postal_);
                info_inicio.AddElement(c_postal);

                //agregamos el codigo postal de la empresa
                PdfPTable pais = new PdfPTable(1);
                pais.WidthPercentage = 100;
                PdfPCell pais_ = new PdfPCell(new Phrase("Pais: " + DsInvoiceAR.Tables["Customer"].Rows[0]["Country"].ToString(), fontTitle2)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP, };
                pais.AddCell(pais_);
                info_inicio.AddElement(pais);


                //agregamos el telefono postal de la empresa
                PdfPTable telefono = new PdfPTable(1);
                telefono.WidthPercentage = 100;
                PdfPCell telefono_ = new PdfPCell(new Phrase("Telefono: " + DsInvoiceAR.Tables["Customer"].Rows[0]["PhoneNum"].ToString(), fontTitle2)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP, };
                telefono.AddCell(telefono_);
                info_inicio.AddElement(telefono);


                //agregamos el fical postal de la empresa
                PdfPTable fiscal = new PdfPTable(1);
                fiscal.WidthPercentage = 100;
                PdfPCell fiscal_ = new PdfPCell(new Phrase("Nit: " + DsInvoiceAR.Tables["Customer"].Rows[0]["CustID"].ToString() + "-" + CalcularDigitoVerificacion(DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"].ToString()), fontTitle2)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP, };
                fiscal.AddCell(fiscal_);
                info_inicio.AddElement(fiscal);

                Advanced.AddCell(info_inicio);

                PdfPCell info_inicio2 = new PdfPCell() { Border = 0, };

                PdfPTable Adquiriente = new PdfPTable(1);
                Adquiriente.WidthPercentage = 100;
                PdfPCell div = new PdfPCell(new Phrase("", fontTitle));
                Adquiriente.AddCell(div);
                info_inicio2.AddElement(Adquiriente);

                string infoAdquiriente = "cufe:"+CUFE;
                PdfPTable tablaInfoAdquiriente = new PdfPTable(1);
                tablaInfoAdquiriente.WidthPercentage = 100;
                PdfPCell cell_InfoAdquiriente = new PdfPCell(new Phrase(infoAdquiriente, fontCustom)) { MinimumHeight = 35, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP, };
                tablaInfoAdquiriente.AddCell(cell_InfoAdquiriente);
                info_inicio2.AddElement(tablaInfoAdquiriente);

                PdfPTable tablaEsp = new PdfPTable(1);
                tablaEsp.WidthPercentage = 100;
                PdfPCell cell_esp = new PdfPCell() { Border = 0, MinimumHeight = 8, };
                tablaEsp.AddCell(cell_esp);
                info_inicio2.AddElement(tablaEsp);

                PdfPTable nr = new PdfPTable(1);
                nr.WidthPercentage = 100;
                PdfPCell nr_r = new PdfPCell(new Phrase());
                nr.AddCell(nr_r);
                info_inicio2.AddElement(nr);

                Advanced.AddCell(info_inicio2);

                cabecera.AddElement(Advanced);

                encabezado.AddCell(cabecera);

                #endregion

                #region Body

                PdfPTable cabeceta_nota = new PdfPTable(new float[] { 0.35f, 0.15f, 0.10f, 0.18f, 0.35f });
                cabeceta_nota.WidthPercentage = 100;


                PdfPCell numerof = new PdfPCell() { };
                //agregamos el campo para el numero de la factura
                PdfPTable _numerof_ = new PdfPTable(1);
                _numerof_.WidthPercentage = 100;
                PdfPCell numerof_ = new PdfPCell(new Phrase("Centro de costo", fontTitle2)) { HorizontalAlignment = Element.ALIGN_CENTER, Border = 0, };
                _numerof_.AddCell(numerof_);
                numerof.AddElement(_numerof_);
                //agregamos el numeo de la factura
                PdfPTable _numerof_r = new PdfPTable(1);
                _numerof_r.WidthPercentage = 100;
                PdfPCell numerof_r = new PdfPCell(new Phrase(""+ DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar02"].ToString(), fontTitle2)) { HorizontalAlignment = Element.ALIGN_CENTER, Border = 0, };
                _numerof_r.AddCell(numerof_r);
                numerof.AddElement(_numerof_r);


                PdfPCell fecha = new PdfPCell();
                //agregamos el campo para el fehca de la factura
                PdfPTable _fecha_ = new PdfPTable(1);
                _fecha_.WidthPercentage = 100;
                PdfPCell fecha_ = new PdfPCell(new Phrase("Fecha", fontTitle2)) { HorizontalAlignment = Element.ALIGN_CENTER, Border = 0, };
                _fecha_.AddCell(fecha_);
                fecha.AddElement(_fecha_);
                //agregamos la fecha de la factura
                PdfPTable _fecha_r = new PdfPTable(1);
                _fecha_r.WidthPercentage = 100;
                PdfPCell fecha_r = new PdfPCell(new Phrase(""+ DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).ToString("dd/mm/yyyy"),fontTitle2)) { HorizontalAlignment = Element.ALIGN_CENTER, Border = 0, };
                _fecha_r.AddCell(fecha_r);
                fecha.AddElement(_fecha_r);

                PdfPCell moneda = new PdfPCell();
                //agregamos el campo para la moneda de la factura
                PdfPTable _Moneda_ = new PdfPTable(1);
                _Moneda_.WidthPercentage = 100;
                PdfPCell Moneda_ = new PdfPCell(new Phrase("Moneda", fontTitle2)) { HorizontalAlignment = Element.ALIGN_CENTER, Border = 0, };
                _Moneda_.AddCell(Moneda_);
                moneda.AddElement(_Moneda_);
                //agregamos la moneda de la factura
                PdfPTable _Moneda_r = new PdfPTable(1);
                _Moneda_r.WidthPercentage = 100;
                PdfPCell Moneda_r = new PdfPCell(new Phrase((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["CurrencyCode"], fontTitle2)) { HorizontalAlignment = Element.ALIGN_CENTER, Border = 0, };
                _Moneda_r.AddCell(Moneda_r);
                moneda.AddElement(_Moneda_r);

                PdfPCell sobre = new PdfPCell();
                //agregamos el campo para la moneda de la factura
                PdfPTable _sobre_ = new PdfPTable(1);
                _sobre_.WidthPercentage = 100;
                PdfPCell sobre_ = new PdfPCell(new Phrase("Subcentro ", fontTitle2)) { HorizontalAlignment = Element.ALIGN_CENTER, Border = 0, };
                _sobre_.AddCell(sobre_);
                sobre.AddElement(_sobre_);
                //agregamos la moneda de la factura
                PdfPTable _sobre_r = new PdfPTable(1);
                _sobre_r.WidthPercentage = 100;
                PdfPCell sobre_r = new PdfPCell(new Phrase((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar01"], fontTitle2)) { HorizontalAlignment = Element.ALIGN_CENTER, Border = 0, };
                _sobre_r.AddCell(sobre_r);
                sobre.AddElement(_sobre_r);


                PdfPCell paginas = new PdfPCell() { Border = 0, };
                //agregamos el campo para la moneda de la factura
                PdfPTable _paginas_ = new PdfPTable(1);
                _paginas_.WidthPercentage = 100;
                PdfPCell paginas_ = new PdfPCell(new Phrase("pagina " + document.PageNumber, fontTitle2)) { HorizontalAlignment = Element.ALIGN_CENTER, };
                _paginas_.AddCell(paginas_);
                paginas.AddElement(_paginas_);
                //agregamos la moneda de la factura
                PdfPTable _paginas_r = new PdfPTable(1);
                _paginas_r.WidthPercentage = 100;
                PdfPCell paginas_r = new PdfPCell(new Phrase("COPIA\nCONTRAVALOR", fontTitle2)) { HorizontalAlignment = Element.ALIGN_CENTER, Border = 0, };
                _paginas_r.AddCell(paginas_r);
                paginas.AddElement(_paginas_r);

                cabeceta_nota.AddCell(numerof);
                cabeceta_nota.AddCell(fecha);
                cabeceta_nota.AddCell(moneda);
                cabeceta_nota.AddCell(sobre);
                cabeceta_nota.AddCell(paginas);


                #endregion

                #region unidades

                //Dimenciones.
                float[] DimencionUnidades = new float[5];
                DimencionUnidades[0] = 0.15f;//Producto 
                DimencionUnidades[1] = 0.60f;//Descripcion
                DimencionUnidades[2] = 0.15f;//cantidad 
                DimencionUnidades[3] = 0.15f;//%IVA
                DimencionUnidades[4] = 0.15f;//Vr Total

                PdfPTable tableTituloUnidades = new PdfPTable(DimencionUnidades);
                tableTituloUnidades.WidthPercentage = 100;

                PdfPCell fecha_d = new PdfPCell(new Phrase("Producto ", fontTitle2)) { HorizontalAlignment = Element.ALIGN_CENTER, };
                PdfPCell Descripción = new PdfPCell(new Phrase("Descripción", fontTitle2)) { HorizontalAlignment = Element.ALIGN_LEFT, };
                PdfPCell Total = new PdfPCell(new Phrase("Cantidad", fontTitle2)) { HorizontalAlignment = Element.ALIGN_RIGHT };
                PdfPCell Saldo = new PdfPCell(new Phrase("%Iva", fontTitle2)) { HorizontalAlignment = Element.ALIGN_RIGHT };
                PdfPCell vtotal = new PdfPCell(new Phrase("Vr Total", fontTitle2)) { HorizontalAlignment = Element.ALIGN_RIGHT };

                tableTituloUnidades.AddCell(fecha_d);
                tableTituloUnidades.AddCell(Descripción);
                tableTituloUnidades.AddCell(Total);
                tableTituloUnidades.AddCell(Saldo);
                tableTituloUnidades.AddCell(vtotal);

                PdfPTable tableUnidades = new PdfPTable(5);
                tableUnidades.WidthPercentage = 100;
                tableUnidades.SetWidths(DimencionUnidades);
                decimal totalDescuento = 0;
                foreach (DataRow InvoiceLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
                {
                    if (!AddUnidadeSoleSolucionesEmpresariales(InvoiceLine, ref tableUnidades, fontCustom, DsInvoiceAR))
                        return false;
                    totalDescuento += decimal.Parse((string)InvoiceLine["InvoiceLine"]);
                }

                int numAdLineas = 10 - DsInvoiceAR.Tables["InvcDtl"].Rows.Count;
                PdfPTable tableEspacioUnidades = new PdfPTable(5);
                tableEspacioUnidades.WidthPercentage = 100;
                tableEspacioUnidades.SetWidths(DimencionUnidades);

                PdfPTable tableLineaFinal = new PdfPTable(5);
                tableLineaFinal.WidthPercentage = 100;
                tableLineaFinal.SetWidths(DimencionUnidades);
                iTextSharp.text.pdf.PdfPCell LineaFinal = new iTextSharp.text.pdf.PdfPCell();
                LineaFinal.Colspan = 4;
                LineaFinal.Padding = 3;
                LineaFinal.BorderColorBottom = BaseColor.WHITE;
                LineaFinal.HorizontalAlignment = Element.ALIGN_CENTER;
                LineaFinal.VerticalAlignment = Element.ALIGN_TOP;
                tableLineaFinal.AddCell(LineaFinal);

                #endregion

                #region Footer

                PdfPTable total = new PdfPTable(new float[] { 0.15f, 0.60f, 0.15f, 0.15f, 0.15f, });
                total.WidthPercentage = 100;

                PdfPCell vacio1 = new PdfPCell(new Phrase("", fontTitle2)) { HorizontalAlignment = Element.ALIGN_CENTER, };
                PdfPCell vacio2 = new PdfPCell(new Phrase("", fontTitle2)) { HorizontalAlignment = Element.ALIGN_LEFT, Colspan=2};
                PdfPCell vacio3 = new PdfPCell(new Phrase("Total", fontTitle2)) { HorizontalAlignment = Element.ALIGN_RIGHT, };
                PdfPCell vacio4 = new PdfPCell(new Phrase(decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"]).ToString("C2"), fontTitle2)) { HorizontalAlignment = Element.ALIGN_RIGHT, };


                total.AddCell(vacio1);
                total.AddCell(vacio2);
                total.AddCell(vacio3);
                total.AddCell(vacio4);

                PdfPTable pie = new PdfPTable(2);
                pie.WidthPercentage = 100;

                PdfPCell porsentuales = new PdfPCell() { MinimumHeight = 30, Padding = 0, };

                PdfPTable insidencia = new PdfPTable(3);
                insidencia.WidthPercentage = 100;
                //agregamos el 19%
                PdfPCell porcentaje = new PdfPCell(new Phrase("%\n19",
                    fontTitle2))
                { HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_BOTTOM, MinimumHeight = 35, };
                insidencia.AddCell(porcentaje);
                //agregamos insidencia
                PdfPCell insi = new PdfPCell(new Phrase("insidencia\n" + decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number08"]).ToString("C2"),
                    fontTitle2))
                { HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_BOTTOM };
                insidencia.AddCell(insi);
                //agregamos iva
                PdfPCell iva = new PdfPCell(new Phrase("iva",
                    fontTitle2))
                { HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_BOTTOM, MinimumHeight = 20, };
                insidencia.AddCell(iva);
                porsentuales.AddElement(insidencia);


                PdfPTable insidencia2 = new PdfPTable(3);
                insidencia2.WidthPercentage = 100;
                //agregamos el 19%
                PdfPCell porsentaje2 = new PdfPCell(new Phrase("",
                    fontTitle2))
                { HorizontalAlignment = Element.ALIGN_RIGHT };
                insidencia2.AddCell(porsentaje2);
                //agregamos insidencia
                PdfPCell insi2 = new PdfPCell(new Phrase(decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number08"]).ToString("C2"),
                    fontTitle2))
                { HorizontalAlignment = Element.ALIGN_RIGHT };
                insidencia2.AddCell(insi2);
                //agregamos iva
                PdfPCell iva2 = new PdfPCell(new Phrase(decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DocTaxAmt"]).ToString("C2"),
                    fontTitle2))
                { HorizontalAlignment = Element.ALIGN_RIGHT, MinimumHeight = 25, };
                insidencia2.AddCell(iva2);
                porsentuales.AddElement(insidencia2);

                PdfPCell funcionario = new PdfPCell() { };

                PdfPTable func = new PdfPTable(1);
                //func.WidthPercentage = 100;
                //agregamos el nombred el funcionario 
                PdfPCell funcionari = new PdfPCell(new Phrase("Funcionario",
                    fontTitle2))
                { HorizontalAlignment = Element.ALIGN_CENTER, MinimumHeight = 20, Padding = 3, };
                func.AddCell(funcionari);
                funcionario.AddElement(func);

                PdfPTable firma = new PdfPTable(1);
                //func.WidthPercentage = 100;
                //agregamos el la firma 
                PdfPCell fima_ = new PdfPCell(new Phrase("_________________________________________________________" +
                                                        "Firma/Signature",
                                                        fontTitle2))
                { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_BOTTOM, MinimumHeight = 30, Padding = 3, };
                firma.AddCell(fima_);
                funcionario.AddElement(firma);


                pie.AddCell(porsentuales);
                pie.AddCell(funcionario);

                PdfPTable procesado = new PdfPTable(2);
                procesado.WidthPercentage = 100;
                PdfPCell cell_q = new PdfPCell() { Border=0};
                cell_q.AddElement(QRPdf);
                procesado.AddCell(cell_q);
                PdfPCell procesad = new PdfPCell(new Phrase("Procesado por computadora", fontTitle2)) { HorizontalAlignment = Element.ALIGN_RIGHT, Border = 0, };
                procesado.AddCell(procesad);

                #endregion

                #region Exit

                document.Add(header);
                document.Add(encabezado);
                document.Add(salto);
                document.Add(cabeceta_nota);
                document.Add(salto);
                document.Add(salto);
                document.Add(tableTituloUnidades);
                document.Add(tableUnidades);
                document.Add(tableEspacioUnidades);
                document.Add(tableLineaFinal);
                document.Add(total);
                document.Add(pie);
                document.Add(procesado);

                //document.Add(divTextBancoCheque);
                /*PIE DE PAGINA*/
                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();
                RutaPdf = NomArchivo;
                return true;
                #endregion

            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.Message;
                return false;
            }
        }
        #endregion

        #region Nota Credito
        public bool NotaCreditoSoleSolucionesEmpresariales(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {
            NomArchivo = string.Empty;
            string Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
            string RutaImg = $@"{AppDomain.CurrentDomain.BaseDirectory}\LOGO_Sole_SolucionesE.png";
            string RutaMarcaAgua = $@"{AppDomain.CurrentDomain.BaseDirectory}\LOGO_Sole_SolucionesE.png";
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            NomArchivo = NombreInvoice + ".pdf";
            NomArchivo = NombreInvoice + ".pdf";

            //NomArchivo = string.Empty;
            //string Ruta = HttpContext.Current.Server.MapPath("~/PDF/" + NIT + "/");
            //string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            //string RutaImg = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";
            //string RutaMarcaAgua = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";
            //NomArchivo = NombreInvoice + ".pdf";
            try
            {
                #region Head
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.
                Document document = new Document(iTextSharp.text.PageSize.LETTER);
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;

                // step 3: we open the document     
                document.Open();

                PdfDiv divEspacio = new PdfDiv();
                divEspacio.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio.Height = 10;
                divEspacio.Width = 130;

                PdfDiv divEspacio2 = new PdfDiv();
                divEspacio2.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio2.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio2.Height = 2;
                divEspacio2.Width = 130;

                //FUENTES----------------------------------------------------------------------------------------------------------
                iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle3 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 9, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle2 = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustom = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustom2 = FontFactory.GetFont(FontFactory.HELVETICA, 4, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitleFactura = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                //FUENTES---------------------------------------------------------------------------------------------------------

                System.Drawing.Image logo = null;
                var requestLogo = WebRequest.Create(RutaMarcaAgua);

                using (var responseLogo = requestLogo.GetResponse())
                using (var streamLogo = responseLogo.GetResponseStream())
                {
                    logo = Bitmap.FromStream(streamLogo);
                }

                System.Drawing.Image logo2 = null;
                var requestLogo2 = WebRequest.Create(RutaImg);

                using (var responseLogo2 = requestLogo2.GetResponse())
                using (var streamLogo2 = responseLogo2.GetResponseStream())
                {
                    logo2 = Bitmap.FromStream(streamLogo2);
                }

                iTextSharp.text.Image ImgLogo = iTextSharp.text.Image.GetInstance(logo2, BaseColor.WHITE);
                ImgLogo.ScaleAbsolute(90, 90);
                ImgLogo.Border = 0;

                iTextSharp.text.Image QRPdf = iTextSharp.text.Image.GetInstance(QRInvoice, BaseColor.WHITE);
                QRPdf.ScaleAbsolute(80, 80);
                QRPdf.Border = 0;

                #endregion

                /////saltos de trabla 
                PdfPTable salto = new PdfPTable(1);
                salto.WidthPercentage = 100;
                PdfPCell vacio = new PdfPCell() { MinimumHeight = 2, Border = 0, };
                salto.AddCell(vacio);
                ////-------------------------------------

                #region Header

                PdfPTable header = new PdfPTable(new float[] { 0.15f, 0.20f, 0.13f, 0.13f, 0.19f, });
                header.WidthPercentage = 100;
                //agregamos logo

                PdfPCell cell_logo = new PdfPCell() { Border = 0, };
                cell_logo.AddElement(ImgLogo);
                header.AddCell(cell_logo);

                //info de factura 
                PdfPCell cell_info = new PdfPCell() { Border = 0, };
                Paragraph prgTitle = new Paragraph(string.Format($"" + DsInvoiceAR.Tables["Company"].Rows[0]["Name"],
                    $"NIT." + DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"]), fontTitle);
                Paragraph prgTitle2 = new Paragraph(string.Format("{0}\n{1}\n{2} - {3}\nwww.SoleSolucionesEmpresariales.com",
                    DsInvoiceAR.Tables["Company"].Rows[0]["Address1"].ToString(),
                    DsInvoiceAR.Tables["Company"].Rows[0]["PhoneNum"].ToString(),
                   $"Tel: " + DsInvoiceAR.Tables["Company"].Rows[0]["City"].ToString(),
                    DsInvoiceAR.Tables["Company"].Rows[0]["State"].ToString()),
                    fontCustom);
                prgTitle.Alignment = Element.ALIGN_CENTER;
                prgTitle2.Alignment = Element.ALIGN_CENTER;
                cell_info.AddElement(prgTitle);
                cell_info.AddElement(prgTitle2);
                header.AddCell(cell_info);

                //info sucursal 1
                PdfPCell celll_suc_1 = new PdfPCell() { Border = 0 };
                Paragraph prgTitle3 = new Paragraph(string.Format($"Medellín", fontTitle));
                Paragraph prgTitle4 = new Paragraph(string.Format(
                DsInvoiceAR.Tables["Company"].Rows[0]["PhoneNum"].ToString(), fontCustom2));
                prgTitle3.Alignment = Element.ALIGN_CENTER;
                prgTitle4.Alignment = Element.ALIGN_CENTER;
                celll_suc_1.AddElement(prgTitle3);
                celll_suc_1.AddElement(prgTitle4);
                header.AddCell(celll_suc_1);

                //info sucursal 2
                PdfPCell celll_suc_2 = new PdfPCell() { Border = 0 };
                Paragraph prgTitle5 = new Paragraph(string.Format("Bogotá", fontTitle));
                Paragraph prgTitle6 = new Paragraph(string.Format(
                DsInvoiceAR.Tables["Company"].Rows[0]["PhoneNum"].ToString(), fontCustom2));
                prgTitle5.Alignment = Element.ALIGN_CENTER;
                prgTitle6.Alignment = Element.ALIGN_CENTER;
                celll_suc_2.AddElement(prgTitle5);
                celll_suc_2.AddElement(prgTitle6);
                header.AddCell(celll_suc_2);

                ///info de factura como numero 
                PdfPCell cell_factura = new PdfPCell() { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, };
                Paragraph prgTitle7 = new Paragraph(string.Format("NOTA CREDITO", fontTitle));
                Paragraph prgTitle8 = new Paragraph(string.Format(
                DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"].ToString(), fontTitle));
                prgTitle7.Alignment = Element.ALIGN_CENTER;
                prgTitle8.Alignment = Element.ALIGN_CENTER;
                cell_factura.AddElement(prgTitle7);
                cell_factura.AddElement(prgTitle8);
                header.AddCell(cell_factura);

                PdfPTable encabezado = new PdfPTable(1);
                encabezado.WidthPercentage = 100;
                PdfPCell cabecera = new PdfPCell() { Border = 0, };

                PdfPTable Advanced = new PdfPTable(2);
                Advanced.WidthPercentage = 100;

                PdfPCell info_inicio = new PdfPCell();
                //agregamos el nimbre de la empresa
                PdfPTable nombre = new PdfPTable(1);
                nombre.WidthPercentage = 100;
                PdfPCell nombre_em = new PdfPCell(new Phrase(DsInvoiceAR.Tables["Customer"].Rows[0]["Name"].ToString(), fontTitle2)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP, MinimumHeight = 50, };
                nombre.AddCell(nombre_em);
                info_inicio.AddElement(nombre);

                //agregamos el codigo postal de la empresa
                PdfPTable c_postal = new PdfPTable(1);
                c_postal.WidthPercentage = 100;
                PdfPCell postal_ = new PdfPCell(new Phrase("Direccon: " + DsInvoiceAR.Tables["Company"].Rows[0]["Address1"].ToString(), fontTitle2)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP, };
                c_postal.AddCell(postal_);
                info_inicio.AddElement(c_postal);

                //agregamos el codigo postal de la empresa
                PdfPTable pais = new PdfPTable(1);
                pais.WidthPercentage = 100;
                PdfPCell pais_ = new PdfPCell(new Phrase("Pais: " + DsInvoiceAR.Tables["Customer"].Rows[0]["Country"].ToString(), fontTitle2)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP, };
                pais.AddCell(pais_);
                info_inicio.AddElement(pais);


                //agregamos el telefono postal de la empresa
                PdfPTable telefono = new PdfPTable(1);
                telefono.WidthPercentage = 100;
                PdfPCell telefono_ = new PdfPCell(new Phrase("Telefono: " + DsInvoiceAR.Tables["Customer"].Rows[0]["PhoneNum"].ToString(), fontTitle2)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP, };
                telefono.AddCell(telefono_);
                info_inicio.AddElement(telefono);


                //agregamos el fical postal de la empresa
                PdfPTable fiscal = new PdfPTable(1);
                fiscal.WidthPercentage = 100;
                PdfPCell fiscal_ = new PdfPCell(new Phrase("Nit: " + DsInvoiceAR.Tables["Customer"].Rows[0]["CustID"].ToString() + "-" + CalcularDigitoVerificacion(DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"].ToString()), fontTitle2)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP, };
                fiscal.AddCell(fiscal_);
                info_inicio.AddElement(fiscal);

                Advanced.AddCell(info_inicio);

                PdfPCell info_inicio2 = new PdfPCell() { Border = 0, };

                PdfPTable Adquiriente = new PdfPTable(1);
                Adquiriente.WidthPercentage = 100;
                PdfPCell div = new PdfPCell(new Phrase("", fontTitle));
                Adquiriente.AddCell(div);
                info_inicio2.AddElement(Adquiriente);

                string infoAdquiriente = "cufe:" + CUFE;
                PdfPTable tablaInfoAdquiriente = new PdfPTable(1);
                tablaInfoAdquiriente.WidthPercentage = 100;
                PdfPCell cell_InfoAdquiriente = new PdfPCell(new Phrase(infoAdquiriente, fontCustom)) { MinimumHeight = 35, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP, };
                tablaInfoAdquiriente.AddCell(cell_InfoAdquiriente);
                info_inicio2.AddElement(tablaInfoAdquiriente);

                PdfPTable tablaEsp = new PdfPTable(1);
                tablaEsp.WidthPercentage = 100;
                PdfPCell cell_esp = new PdfPCell() { Border = 0, MinimumHeight = 8, };
                tablaEsp.AddCell(cell_esp);
                info_inicio2.AddElement(tablaEsp);

                PdfPTable nr = new PdfPTable(1);
                nr.WidthPercentage = 100;
                PdfPCell nr_r = new PdfPCell(new Phrase());
                nr.AddCell(nr_r);
                info_inicio2.AddElement(nr);

                Advanced.AddCell(info_inicio2);

                cabecera.AddElement(Advanced);

                encabezado.AddCell(cabecera);

                #endregion

                #region Body

                PdfPTable cabeceta_nota = new PdfPTable(new float[] { 0.35f, 0.15f, 0.10f, 0.18f, 0.35f });
                cabeceta_nota.WidthPercentage = 100;


                PdfPCell numerof = new PdfPCell() { };
                //agregamos el campo para el numero de la factura
                PdfPTable _numerof_ = new PdfPTable(1);
                _numerof_.WidthPercentage = 100;
                PdfPCell numerof_ = new PdfPCell(new Phrase("Centro de costo", fontTitle2)) { HorizontalAlignment = Element.ALIGN_CENTER, Border = 0, };
                _numerof_.AddCell(numerof_);
                numerof.AddElement(_numerof_);
                //agregamos el numeo de la factura
                PdfPTable _numerof_r = new PdfPTable(1);
                _numerof_r.WidthPercentage = 100;
                PdfPCell numerof_r = new PdfPCell(new Phrase("" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar02"].ToString(), fontTitle2)) { HorizontalAlignment = Element.ALIGN_CENTER, Border = 0, };
                _numerof_r.AddCell(numerof_r);
                numerof.AddElement(_numerof_r);


                PdfPCell fecha = new PdfPCell();
                //agregamos el campo para el fehca de la factura
                PdfPTable _fecha_ = new PdfPTable(1);
                _fecha_.WidthPercentage = 100;
                PdfPCell fecha_ = new PdfPCell(new Phrase("Fecha", fontTitle2)) { HorizontalAlignment = Element.ALIGN_CENTER, Border = 0, };
                _fecha_.AddCell(fecha_);
                fecha.AddElement(_fecha_);
                //agregamos la fecha de la factura
                PdfPTable _fecha_r = new PdfPTable(1);
                _fecha_r.WidthPercentage = 100;
                PdfPCell fecha_r = new PdfPCell(new Phrase("" + DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).ToString("dd/mm/yyyy"), fontTitle2)) { HorizontalAlignment = Element.ALIGN_CENTER, Border = 0, };
                _fecha_r.AddCell(fecha_r);
                fecha.AddElement(_fecha_r);

                PdfPCell moneda = new PdfPCell();
                //agregamos el campo para la moneda de la factura
                PdfPTable _Moneda_ = new PdfPTable(1);
                _Moneda_.WidthPercentage = 100;
                PdfPCell Moneda_ = new PdfPCell(new Phrase("Moneda", fontTitle2)) { HorizontalAlignment = Element.ALIGN_CENTER, Border = 0, };
                _Moneda_.AddCell(Moneda_);
                moneda.AddElement(_Moneda_);
                //agregamos la moneda de la factura
                PdfPTable _Moneda_r = new PdfPTable(1);
                _Moneda_r.WidthPercentage = 100;
                PdfPCell Moneda_r = new PdfPCell(new Phrase((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["CurrencyCode"], fontTitle2)) { HorizontalAlignment = Element.ALIGN_CENTER, Border = 0, };
                _Moneda_r.AddCell(Moneda_r);
                moneda.AddElement(_Moneda_r);

                PdfPCell sobre = new PdfPCell();
                //agregamos el campo para la moneda de la factura
                PdfPTable _sobre_ = new PdfPTable(1);
                _sobre_.WidthPercentage = 100;
                PdfPCell sobre_ = new PdfPCell(new Phrase("Subcentro ", fontTitle2)) { HorizontalAlignment = Element.ALIGN_CENTER, Border = 0, };
                _sobre_.AddCell(sobre_);
                sobre.AddElement(_sobre_);
                //agregamos la moneda de la factura
                PdfPTable _sobre_r = new PdfPTable(1);
                _sobre_r.WidthPercentage = 100;
                PdfPCell sobre_r = new PdfPCell(new Phrase((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar01"], fontTitle2)) { HorizontalAlignment = Element.ALIGN_CENTER, Border = 0, };
                _sobre_r.AddCell(sobre_r);
                sobre.AddElement(_sobre_r);


                PdfPCell paginas = new PdfPCell() { Border = 0, };
                //agregamos el campo para la moneda de la factura
                PdfPTable _paginas_ = new PdfPTable(1);
                _paginas_.WidthPercentage = 100;
                PdfPCell paginas_ = new PdfPCell(new Phrase("pagina " + document.PageNumber, fontTitle2)) { HorizontalAlignment = Element.ALIGN_CENTER, };
                _paginas_.AddCell(paginas_);
                paginas.AddElement(_paginas_);
                //agregamos la moneda de la factura
                PdfPTable _paginas_r = new PdfPTable(1);
                _paginas_r.WidthPercentage = 100;
                PdfPCell paginas_r = new PdfPCell(new Phrase("COPIA\nCONTRAVALOR", fontTitle2)) { HorizontalAlignment = Element.ALIGN_CENTER, Border = 0, };
                _paginas_r.AddCell(paginas_r);
                paginas.AddElement(_paginas_r);

                cabeceta_nota.AddCell(numerof);
                cabeceta_nota.AddCell(fecha);
                cabeceta_nota.AddCell(moneda);
                cabeceta_nota.AddCell(sobre);
                cabeceta_nota.AddCell(paginas);


                #endregion

                #region unidades

                //Dimenciones.
                float[] DimencionUnidades = new float[5];
                DimencionUnidades[0] = 0.15f;//Producto 
                DimencionUnidades[1] = 0.60f;//Descripcion
                DimencionUnidades[2] = 0.15f;//cantidad 
                DimencionUnidades[3] = 0.15f;//%IVA
                DimencionUnidades[4] = 0.15f;//Vr Total

                PdfPTable tableTituloUnidades = new PdfPTable(DimencionUnidades);
                tableTituloUnidades.WidthPercentage = 100;

                PdfPCell fecha_d = new PdfPCell(new Phrase("Producto ", fontTitle2)) { HorizontalAlignment = Element.ALIGN_CENTER, };
                PdfPCell Descripción = new PdfPCell(new Phrase("Descripción", fontTitle2)) { HorizontalAlignment = Element.ALIGN_LEFT, };
                PdfPCell Total = new PdfPCell(new Phrase("Cantidad", fontTitle2)) { HorizontalAlignment = Element.ALIGN_RIGHT };
                PdfPCell Saldo = new PdfPCell(new Phrase("%Iva", fontTitle2)) { HorizontalAlignment = Element.ALIGN_RIGHT };
                PdfPCell vtotal = new PdfPCell(new Phrase("Vr Total", fontTitle2)) { HorizontalAlignment = Element.ALIGN_RIGHT };

                tableTituloUnidades.AddCell(fecha_d);
                tableTituloUnidades.AddCell(Descripción);
                tableTituloUnidades.AddCell(Total);
                tableTituloUnidades.AddCell(Saldo);
                tableTituloUnidades.AddCell(vtotal);

                PdfPTable tableUnidades = new PdfPTable(5);
                tableUnidades.WidthPercentage = 100;
                tableUnidades.SetWidths(DimencionUnidades);
                decimal totalDescuento = 0;
                foreach (DataRow InvoiceLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
                {
                    if (!AddUnidadeSoleSolucionesEmpresariales(InvoiceLine, ref tableUnidades, fontCustom, DsInvoiceAR))
                        return false;
                    totalDescuento += decimal.Parse((string)InvoiceLine["InvoiceLine"]);
                }

                int numAdLineas = 10 - DsInvoiceAR.Tables["InvcDtl"].Rows.Count;
                PdfPTable tableEspacioUnidades = new PdfPTable(5);
                tableEspacioUnidades.WidthPercentage = 100;
                tableEspacioUnidades.SetWidths(DimencionUnidades);

                PdfPTable tableLineaFinal = new PdfPTable(5);
                tableLineaFinal.WidthPercentage = 100;
                tableLineaFinal.SetWidths(DimencionUnidades);
                iTextSharp.text.pdf.PdfPCell LineaFinal = new iTextSharp.text.pdf.PdfPCell();
                LineaFinal.Colspan = 4;
                LineaFinal.Padding = 3;
                LineaFinal.BorderColorBottom = BaseColor.WHITE;
                LineaFinal.HorizontalAlignment = Element.ALIGN_CENTER;
                LineaFinal.VerticalAlignment = Element.ALIGN_TOP;
                tableLineaFinal.AddCell(LineaFinal);

                #endregion

                #region Footer

                PdfPTable total = new PdfPTable(new float[] { 0.15f, 0.60f, 0.15f, 0.15f, 0.15f, });
                total.WidthPercentage = 100;

                PdfPCell vacio1 = new PdfPCell(new Phrase("", fontTitle2)) { HorizontalAlignment = Element.ALIGN_CENTER, };
                PdfPCell vacio2 = new PdfPCell(new Phrase("", fontTitle2)) { HorizontalAlignment = Element.ALIGN_LEFT, Colspan = 2 };
                PdfPCell vacio3 = new PdfPCell(new Phrase("Total", fontTitle2)) { HorizontalAlignment = Element.ALIGN_RIGHT, };
                PdfPCell vacio4 = new PdfPCell(new Phrase(decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"]).ToString("C2"), fontTitle2)) { HorizontalAlignment = Element.ALIGN_RIGHT, };


                total.AddCell(vacio1);
                total.AddCell(vacio2);
                total.AddCell(vacio3);
                total.AddCell(vacio4);

                PdfPTable pie = new PdfPTable(2);
                pie.WidthPercentage = 100;

                PdfPCell porsentuales = new PdfPCell() { MinimumHeight = 30, Padding = 0, };

                PdfPTable insidencia = new PdfPTable(3);
                insidencia.WidthPercentage = 100;
                //agregamos el 19%
                PdfPCell porcentaje = new PdfPCell(new Phrase("%\n19",
                    fontTitle2))
                { HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_BOTTOM, MinimumHeight = 35, };
                insidencia.AddCell(porcentaje);
                //agregamos insidencia
                PdfPCell insi = new PdfPCell(new Phrase("insidencia\n" + decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number08"]).ToString("C2"),
                    fontTitle2))
                { HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_BOTTOM };
                insidencia.AddCell(insi);
                //agregamos iva
                PdfPCell iva = new PdfPCell(new Phrase("iva",
                    fontTitle2))
                { HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_BOTTOM, MinimumHeight = 20, };
                insidencia.AddCell(iva);
                porsentuales.AddElement(insidencia);


                PdfPTable insidencia2 = new PdfPTable(3);
                insidencia2.WidthPercentage = 100;
                //agregamos el 19%
                PdfPCell porsentaje2 = new PdfPCell(new Phrase("",
                    fontTitle2))
                { HorizontalAlignment = Element.ALIGN_RIGHT };
                insidencia2.AddCell(porsentaje2);
                //agregamos insidencia
                PdfPCell insi2 = new PdfPCell(new Phrase(decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number08"]).ToString("C2"),
                    fontTitle2))
                { HorizontalAlignment = Element.ALIGN_RIGHT };
                insidencia2.AddCell(insi2);
                //agregamos iva
                PdfPCell iva2 = new PdfPCell(new Phrase(decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DocTaxAmt"]).ToString("C2"),
                    fontTitle2))
                { HorizontalAlignment = Element.ALIGN_RIGHT, MinimumHeight = 25, };
                insidencia2.AddCell(iva2);
                porsentuales.AddElement(insidencia2);

                PdfPCell funcionario = new PdfPCell() { };

                PdfPTable func = new PdfPTable(1);
                //func.WidthPercentage = 100;
                //agregamos el nombred el funcionario 
                PdfPCell funcionari = new PdfPCell(new Phrase("Funcionario",
                    fontTitle2))
                { HorizontalAlignment = Element.ALIGN_CENTER, MinimumHeight = 20, Padding = 3, };
                func.AddCell(funcionari);
                funcionario.AddElement(func);

                PdfPTable firma = new PdfPTable(1);
                //func.WidthPercentage = 100;
                //agregamos el la firma 
                PdfPCell fima_ = new PdfPCell(new Phrase("_________________________________________________________" +
                                                        "Firma/Signature",
                                                        fontTitle2))
                { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_BOTTOM, MinimumHeight = 30, Padding = 3, };
                firma.AddCell(fima_);
                funcionario.AddElement(firma);


                pie.AddCell(porsentuales);
                pie.AddCell(funcionario);

                PdfPTable procesado = new PdfPTable(2);
                procesado.WidthPercentage = 100;
                PdfPCell cell_q = new PdfPCell() { Border = 0 };
                cell_q.AddElement(QRPdf);
                procesado.AddCell(cell_q);
                PdfPCell procesad = new PdfPCell(new Phrase("Procesado por computadora", fontTitle2)) { HorizontalAlignment = Element.ALIGN_RIGHT, Border = 0, };
                procesado.AddCell(procesad);

                #endregion

                #region Exit

                document.Add(header);
                document.Add(encabezado);
                document.Add(salto);
                document.Add(cabeceta_nota);
                document.Add(salto);
                document.Add(salto);
                document.Add(tableTituloUnidades);
                document.Add(tableUnidades);
                document.Add(tableEspacioUnidades);
                document.Add(tableLineaFinal);
                document.Add(total);
                document.Add(pie);
                document.Add(procesado);

                //document.Add(divTextBancoCheque);
                /*PIE DE PAGINA*/
                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();
                RutaPdf = NomArchivo;
                return true;
                #endregion

            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.Message;
                return false;
            }
        }
        #endregion

    }
}
