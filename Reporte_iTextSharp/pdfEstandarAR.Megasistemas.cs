﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
//Referenciar y usar.
using System.Data;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Web;
using System.Windows.Forms;

namespace RulesServicesDIAN2.Adquiriente
{
    public partial class pdfEstandarAR
    {
        #region Formato de Factura Megasistemas

        private bool AddUnidadesMegasistemas(DataRow dataLine, ref PdfPTable table, iTextSharp.text.Font fontTitleFactura, DataSet dataSet)
        {
            try
            {
                //Linea
                strError += "Add InvoiceLine";
                iTextSharp.text.pdf.PdfPCell celValL = new iTextSharp.text.pdf.PdfPCell(new Phrase((string)dataLine["InvoiceLine"], fontTitleFactura));
                celValL.Colspan = 1;
                celValL.Padding = 2;
                celValL.HorizontalAlignment = Element.ALIGN_CENTER;
                celValL.VerticalAlignment = Element.ALIGN_CENTER;
                table.AddCell(celValL);
                strError += "InvoiceLine OK";

                //Codigo Articulo
                strError += "Add PartNum";
                iTextSharp.text.pdf.PdfPCell celValCodigo = new iTextSharp.text.pdf.PdfPCell(new Phrase((string)dataLine["PartNum"], fontTitleFactura));
                celValCodigo.Colspan = 1;
                celValCodigo.Padding = 2;
                celValCodigo.HorizontalAlignment = Element.ALIGN_CENTER;
                celValCodigo.VerticalAlignment = Element.ALIGN_CENTER;
                table.AddCell(celValCodigo);
                strError += "Add PartNum OK";

                //descripcion
                strError += "Add LineDesc";
                iTextSharp.text.pdf.PdfPCell celValDescripcion = new iTextSharp.text.pdf.PdfPCell(new Phrase((string)dataLine["LineDesc"], fontTitleFactura));
                celValDescripcion.Colspan = 1;
                celValDescripcion.Padding = 2;
                celValDescripcion.HorizontalAlignment = Element.ALIGN_CENTER;
                celValDescripcion.VerticalAlignment = Element.ALIGN_CENTER;
                table.AddCell(celValDescripcion);
                strError += "Add LineDesc OK";

                //Cantidad
                strError += "Add SellingShipQty";
                iTextSharp.text.pdf.PdfPCell celValCantidad = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    decimal.Parse((string)dataLine["SellingShipQty"]).ToString("N"), fontTitleFactura));
                celValCantidad.Colspan = 1;
                celValCantidad.Padding = 2;
                celValCantidad.HorizontalAlignment = Element.ALIGN_CENTER;
                celValCantidad.VerticalAlignment = Element.ALIGN_CENTER;
                table.AddCell(celValCantidad);
                strError += "Add SellingShipQty OK";

                //% Iva
                strError += "Add IVA";
                iTextSharp.text.pdf.PdfPCell celValIva = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    decimal.Parse((string)dataLine["SellingShipQty"]).ToString("N"), fontTitleFactura));
                celValIva.Colspan = 1;
                celValIva.Padding = 2;
                celValIva.MinimumHeight = 20;
                celValIva.HorizontalAlignment = Element.ALIGN_CENTER;
                celValIva.VerticalAlignment = Element.ALIGN_CENTER;
                table.AddCell(celValIva);
                strError += "Add IVA OK";

                //% Descuento
                strError += "Add DiscountPercent";
                iTextSharp.text.pdf.PdfPCell celValDcto = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    Convert.ToDecimal((string)dataLine["DiscountPercent"]).ToString("N"), fontTitleFactura));
                celValDcto.Colspan = 1;
                celValDcto.Padding = 2;
                celValDcto.MinimumHeight = 20;
                celValDcto.HorizontalAlignment = Element.ALIGN_CENTER;
                celValDcto.VerticalAlignment = Element.ALIGN_CENTER;
                table.AddCell(celValDcto);
                strError += "Add DiscountPercent OK";

                //Valor Unidad
                strError += "Add DocUnitPrice";
                iTextSharp.text.pdf.PdfPCell celValUnitario = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:C2}", Convert.ToDecimal(dataLine["DocUnitPrice"])), fontTitleFactura));
                celValUnitario.Colspan = 1;
                celValUnitario.Padding = 2;
                celValUnitario.MinimumHeight = 20;
                celValUnitario.HorizontalAlignment = Element.ALIGN_CENTER;
                celValUnitario.VerticalAlignment = Element.ALIGN_CENTER;
                table.AddCell(celValUnitario);
                strError += "Add DocUnitPrice OK";

                //Valor Total
                strError += "Add DspDocExtPrice";
                iTextSharp.text.pdf.PdfPCell celValTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:C2}", Convert.ToDecimal(dataLine["DspDocExtPrice"])), fontTitleFactura));
                celValTotal.Colspan = 1;
                celValTotal.Padding = 2;
                celValTotal.MinimumHeight = 20;
                celValTotal.HorizontalAlignment = Element.ALIGN_CENTER;
                celValTotal.VerticalAlignment = Element.ALIGN_CENTER;
                table.AddCell(celValTotal);
                strError += "Add DspDocExtPrice OK";

                return true;
            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.Message;
                return false;
            }
        }

        public bool FacturaMegasistemas(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {

            string Ruta = string.Empty;
            string RutaImg = string.Empty;
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);

            //Ruta = HttpContext.Current.Server.MapPath("~/PDF/" + NIT + "/");
            //RutaImg = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";

            Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
            RutaImg = $@"{AppDomain.CurrentDomain.BaseDirectory}\megactivo-azul-fondotransparent.png";

            NomArchivo = string.Empty;
            NomArchivo = NombreInvoice + ".pdf";

            try
            {
                #region head
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.
                Document document = new Document(iTextSharp.text.PageSize.LETTER);
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;

                // step 3: we open the document     
                //document.Open();

                PdfDiv divEspacio = new PdfDiv();
                divEspacio.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio.Height = 10;
                divEspacio.Width = 130;

                PdfDiv divEspacio2 = new PdfDiv();
                divEspacio2.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio2.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio2.Height = 2;
                divEspacio2.Width = 130;

                //FUENTES----------------------------------------------------------------------------------------------------------
                iTextSharp.text.Font fontTitle1 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 9, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle2 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustom = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustom2 = FontFactory.GetFont(FontFactory.HELVETICA, 9, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fonPaginacion = FontFactory.GetFont(FontFactory.COURIER, 6, iTextSharp.text.Font.NORMAL);
                //FUENTES---------------------------------------------------------------------------------------------------------

                System.Drawing.Image logo = null;
                var requestLogo = WebRequest.Create(RutaImg);

                using (var responseLogo = requestLogo.GetResponse())
                using (var streamLogo = responseLogo.GetResponseStream())
                {
                    logo = Bitmap.FromStream(streamLogo);
                }
                iTextSharp.text.Image QRPdf = iTextSharp.text.Image.GetInstance(QRInvoice, BaseColor.WHITE);
                QRPdf.ScaleAbsolute(100, 100);
                QRPdf.Border = 0;

                System.Drawing.Image logo2 = null;
                var requestLogo2 = WebRequest.Create(RutaImg);

                using (var responseLogo2 = requestLogo2.GetResponse())
                using (var streamLogo2 = responseLogo2.GetResponseStream())
                {
                    logo2 = Bitmap.FromStream(streamLogo2);
                }
                iTextSharp.text.Image ImgLogo2 = iTextSharp.text.Image.GetInstance(logo2, BaseColor.WHITE);
                ImgLogo2.ScaleAbsolute(180, 150);

              
                PdfPTable espacio = new PdfPTable(1);
                espacio.WidthPercentage = 100;
                PdfPCell salto = new PdfPCell() { Border = 0, MinimumHeight = 15, };
                espacio.AddCell(salto);

                PdfPCell celEspacio = new PdfPCell(new Phrase("", fontTitle1)) { Border = 0 };

                #endregion

                #region Header

                //Logo
                PdfPTable Header = new PdfPTable(new float[] { 2.5f, 3.5f, 1.2f });
                Header.WidthPercentage = 100;
                PdfPCell cell_logo = new PdfPCell()
                {
                    Border = 0,
                    Padding = 0,
                    MinimumHeight = 80,
                };
                cell_logo.AddElement(ImgLogo2);
                //Info

                string companyName = DsInvoiceAR.Tables["Company"].Rows[0]["Name"].ToString();
                string companyCompany = DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"].ToString();
                string CheckDigit = CalcularDigitoVerificacion(DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"].ToString());
                string companyAddress = DsInvoiceAR.Tables["Company"].Rows[0]["Address1"].ToString();
                string companyPhoneNum = DsInvoiceAR.Tables["Company"].Rows[0]["PhoneNum"].ToString();
                PdfPCell info = new PdfPCell(new Phrase(string.Format("{0}\nNIT: {1} - {2}\n{3}\nPBX: {4}\n \n \nCUFE: {5}\nRepresentación gráfica factura electrónica",
                    companyName, companyCompany, CheckDigit, companyAddress, companyPhoneNum, CUFE), fontTitle1))
                {
                    Border = 0,
                    Padding = 0,
                    HorizontalAlignment = Element.ALIGN_CENTER
                };
                PdfPCell t_factura = new PdfPCell(new Phrase(string.Format("FACTURA VENTAS\n\n{0}"
                    , DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"].ToString()), fontTitle1))
                {
                    Border = 0,
                    Padding = 0,
                    HorizontalAlignment = Element.ALIGN_CENTER,
                };

                Header.AddCell(cell_logo);
                Header.AddCell(info);
                Header.AddCell(t_factura);

                PdfPTable Detalles = new PdfPTable(new float[] { 3.0F, 3.0F, 1.5F });
                Detalles.WidthPercentage = 100;

                //Cliente------------
                PdfPTable Cliente = new PdfPTable(new float[] { 0.8F, 1.5F });

                PdfPCell celTextcliente = new PdfPCell(new Phrase("SEÑORES", fontTitle1)) { Border = 0 };
                Cliente.AddCell(celTextcliente);
                Cliente.AddCell(celEspacio);

                PdfPCell celTextNombre = new PdfPCell(new Phrase("Nombre:", fontCustom2)) { Border = 0 };
                PdfPCell celNombre = new PdfPCell(new Phrase(DsInvoiceAR.Tables["Customer"].Rows[0]["Name"].ToString(), fontCustom2)) { Border = 0 };
                Cliente.AddCell(celTextNombre);
                Cliente.AddCell(celNombre);

                PdfPCell celTextDireccion = new PdfPCell(new Phrase("Dirección:", fontCustom2)) { Border = 0 };
                PdfPCell celDireccion = new PdfPCell(new Phrase(DsInvoiceAR.Tables["Customer"].Rows[0]["Address1"].ToString(), fontCustom2)) { Border = 0 };
                Cliente.AddCell(celTextDireccion);
                Cliente.AddCell(celDireccion);

                PdfPCell celTextCiudad = new PdfPCell(new Phrase("Ciudad:", fontCustom2)) { Border = 0 };
                PdfPCell celCiudad = new PdfPCell(new Phrase(DsInvoiceAR.Tables["Customer"].Rows[0]["City"].ToString(), fontCustom2)) { Border = 0 };
                Cliente.AddCell(celTextCiudad);
                Cliente.AddCell(celCiudad);

                PdfPCell celTextDepartamento = new PdfPCell(new Phrase("Departamento:", fontCustom2)) { Border = 0 };
                PdfPCell celDepartamento = new PdfPCell(new Phrase(DsInvoiceAR.Tables["Customer"].Rows[0]["State"].ToString(), fontCustom2)) { Border = 0 };
                Cliente.AddCell(celTextDepartamento);
                Cliente.AddCell(celDepartamento);

                PdfPCell celTextNit = new PdfPCell(new Phrase("Nit:", fontCustom2)) { Border = 0 };
                PdfPCell celNit = new PdfPCell(new Phrase(DsInvoiceAR.Tables["Customer"].Rows[0]["CustID"].ToString(), fontCustom2)) { Border = 0 };
                Cliente.AddCell(celTextNit);
                Cliente.AddCell(celNit);

                PdfPCell celCliente = new PdfPCell(Cliente) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, };

                //info factura------------
                PdfPTable InfoFactura = new PdfPTable(new float[] { 1.2F, 0.8F });

                PdfPCell celTextFactura = new PdfPCell(new Phrase("INFORMACION FACTURA", fontTitle1)) { Border = 0 };
                InfoFactura.AddCell(celTextFactura);
                InfoFactura.AddCell(celEspacio);

                PdfPCell celTextNumeroPedido = new PdfPCell(new Phrase("No. pedido:", fontCustom2)) { Border = 0 };
                PdfPCell celNumeroPedido = new PdfPCell(new Phrase(DsInvoiceAR.Tables["InvcHead"].Rows[0]["OrderNum"].ToString(), fontCustom2)) { Border = 0 };
                InfoFactura.AddCell(celTextNumeroPedido);
                InfoFactura.AddCell(celNumeroPedido);

                PdfPCell celTextNumeroFactura = new PdfPCell(new Phrase("No. Fac:", fontCustom2)) { Border = 0 };
                PdfPCell celNumeroFactura = new PdfPCell(new Phrase(DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceNum"].ToString(), fontCustom2)) { Border = 0 };
                InfoFactura.AddCell(celTextNumeroFactura);
                InfoFactura.AddCell(celNumeroFactura);

                PdfPCell celTextFechaFactura = new PdfPCell(new Phrase("Fecha Factura:", fontCustom2)) { Border = 0 };
                PdfPCell celFechaFactura = new PdfPCell(new Phrase(string.Format("{0:MM/dd/yyyy}", DateTime.Parse(DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"].ToString())), fontCustom2)) { Border = 0 };
                InfoFactura.AddCell(celTextFechaFactura);
                InfoFactura.AddCell(celFechaFactura);

                PdfPCell celTextFechaVencimiento = new PdfPCell(new Phrase("Fecha De Vencimiento:", fontCustom2)) { Border = 0 };
                PdfPCell celFechaVencimiento = new PdfPCell(new Phrase(string.Format("{0:MM/dd/yyyy}", DateTime.Parse(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"].ToString())), fontCustom2)) { Border = 0 };
                InfoFactura.AddCell(celTextFechaVencimiento);
                InfoFactura.AddCell(celFechaVencimiento);

                PdfPCell celTextFormaPago = new PdfPCell(new Phrase("Forma de Pago:", fontCustom2)) { Border = 0 };
                PdfPCell celFormaPago = new PdfPCell(new Phrase(DsInvoiceAR.Tables["Customer"].Rows[0]["TermsDescription"].ToString(), fontCustom2)) { Border = 0 };
                InfoFactura.AddCell(celTextFormaPago);
                InfoFactura.AddCell(celFormaPago);

                PdfPCell celInfoFactura = new PdfPCell(InfoFactura) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, };

                PdfPCell Qr = new PdfPCell() { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER };

                Qr.AddElement(QRPdf);

                Detalles.AddCell(celCliente);
                Detalles.AddCell(celInfoFactura);
                Detalles.AddCell(Qr);

                #endregion

                #region Lineas de Detalle

                float[] Dimensiones = new float[8];
                Dimensiones[0] = 0.5f; // Linea
                Dimensiones[1] = 1.0f; // Codigo
                Dimensiones[2] = 2.5f; // Descripcion
                Dimensiones[3] = 0.6f; // Cantidad
                Dimensiones[4] = 0.5f; // % IVA
                Dimensiones[5] = 0.6f; // % Descuento
                Dimensiones[6] = 0.8f; // Valor Unitario
                Dimensiones[7] = 1.0f; // Total

                PdfPTable Body = new PdfPTable(Dimensiones);
                //Body.WidthPercentage = 100;

                PdfPCell linea = new PdfPCell(new Phrase("Linea", fontTitle1)) { HorizontalAlignment = Element.ALIGN_CENTER, BackgroundColor = BaseColor.LIGHT_GRAY, };
                PdfPCell codigo = new PdfPCell(new Phrase("Código", fontTitle1)) { HorizontalAlignment = Element.ALIGN_CENTER, MinimumHeight = 17, BackgroundColor = BaseColor.LIGHT_GRAY, };
                PdfPCell Descripcion = new PdfPCell(new Phrase("Descripcion", fontTitle1)) { HorizontalAlignment = Element.ALIGN_CENTER, BackgroundColor = BaseColor.LIGHT_GRAY, };
                PdfPCell Cantidad = new PdfPCell(new Phrase("Cant.", fontTitle1)) { HorizontalAlignment = Element.ALIGN_CENTER, BackgroundColor = BaseColor.LIGHT_GRAY, };
                PdfPCell Iva = new PdfPCell(new Phrase("% IVA", fontTitle1)) { HorizontalAlignment = Element.ALIGN_CENTER, BackgroundColor = BaseColor.LIGHT_GRAY, };
                PdfPCell Descuento = new PdfPCell(new Phrase("% Dcto", fontTitle1)) { HorizontalAlignment = Element.ALIGN_CENTER, BackgroundColor = BaseColor.LIGHT_GRAY, };
                PdfPCell ValUnitario = new PdfPCell(new Phrase("Valor Unit.", fontTitle1)) { HorizontalAlignment = Element.ALIGN_CENTER, BackgroundColor = BaseColor.LIGHT_GRAY, };
                PdfPCell Total = new PdfPCell(new Phrase("Total", fontTitle1)) { HorizontalAlignment = Element.ALIGN_CENTER, BackgroundColor = BaseColor.LIGHT_GRAY, };

                //contador para las cendas 
                PdfPTable tableUnidades = new PdfPTable(Dimensiones);
                //tableUnidades.WidthPercentage = 100;

                decimal totalDescuento = 0;
                foreach (DataRow InvoiceLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
                {
                    if (!AddUnidadesMegasistemas(InvoiceLine, ref tableUnidades, fontCustom, DsInvoiceAR))
                        return false;
                    totalDescuento += decimal.Parse((string)InvoiceLine["DspDocLessDiscount"]);
                }

                iTextSharp.text.pdf.PdfPCell LineaFinal = new iTextSharp.text.pdf.PdfPCell();
                LineaFinal.Colspan = Dimensiones.Length;
                LineaFinal.Padding = 3;
                LineaFinal.Border = 1;
                LineaFinal.BorderColorBottom = BaseColor.WHITE;
                LineaFinal.HorizontalAlignment = Element.ALIGN_CENTER;
                LineaFinal.VerticalAlignment = Element.ALIGN_TOP;
                tableUnidades.AddCell(LineaFinal);

                Body.AddCell(linea);
                Body.AddCell(codigo);
                Body.AddCell(Descripcion);
                Body.AddCell(Cantidad);
                Body.AddCell(Iva);
                Body.AddCell(Descuento);
                Body.AddCell(ValUnitario);
                Body.AddCell(Total);

                #endregion

                #region Footer

                PdfPTable Footer = new PdfPTable(new float[] { 6.0f, 3.0f, 1.0f });
                Footer.WidthPercentage = 100;

                PdfPTable Observaciones = new PdfPTable(1);

                PdfPCell celTextObservaciones = new PdfPCell(new Phrase("OBSERVACIONES:", fontTitle1)) { Border = 0, MinimumHeight = 50, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP, };
                PdfPCell celObservaciones = new PdfPCell(new Phrase(DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceComment"].ToString()
                    , fontCustom2))
                { Border = 0, MinimumHeight = 50, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP, };
                Observaciones.AddCell(celTextObservaciones);
                Observaciones.AddCell(celObservaciones);

                PdfPCell totales = new PdfPCell() { Border = 0, };

                PdfPTable Totales_fin = new PdfPTable(new float[] { 2.5f, 1.5f });
                Totales_fin.WidthPercentage = 100;

                PdfPCell SUBTOTAL_cell = new PdfPCell(new Phrase("SUBTOTAL", fontTitle2)) { HorizontalAlignment = Element.ALIGN_LEFT };
                PdfPCell IVA_cell16 = new PdfPCell(new Phrase("IVA 16%", fontTitle2)) { HorizontalAlignment = Element.ALIGN_LEFT };
                PdfPCell IVA_cell = new PdfPCell(new Phrase("IVA", fontTitle2)) { HorizontalAlignment = Element.ALIGN_LEFT };
                PdfPCell RETENCIONES_FUENTE_cell = new PdfPCell(new Phrase("RETE FUENTE", fontTitle2)) { HorizontalAlignment = Element.ALIGN_LEFT };
                PdfPCell RETENCIONES_IVA_cell = new PdfPCell(new Phrase("RETE IVA", fontTitle2)) { HorizontalAlignment = Element.ALIGN_LEFT };
                PdfPCell RETENCIONES_ICA_cell = new PdfPCell(new Phrase("RETE ICA", fontTitle2)) { HorizontalAlignment = Element.ALIGN_LEFT };
                PdfPCell DCTO_cell = new PdfPCell(new Phrase("DESCUENTO", fontTitle2)) { HorizontalAlignment = Element.ALIGN_LEFT };
                PdfPTable TOTAL = new PdfPTable(new float[] { 0.6f, 0.4f });
                TOTAL.WidthPercentage = 100;
                PdfPCell TOTAL_text = new PdfPCell(new Phrase("TOTAL", fontTitle2)) { HorizontalAlignment = Element.ALIGN_LEFT };
                PdfPCell COPL_cell = new PdfPCell(new Phrase("COP", fontTitle2)) { HorizontalAlignment = Element.ALIGN_CENTER };
                TOTAL.AddCell(TOTAL_text);
                TOTAL.AddCell(COPL_cell);
                PdfPCell TOTAL_cell = new PdfPCell(TOTAL);

                PdfPCell SUBTOTAL_cell_v = new PdfPCell(new Phrase(string.Format("{0:C2}", Convert.ToDecimal(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubTotal"].ToString())), fontTitle2)) { HorizontalAlignment = Element.ALIGN_RIGHT, };
                PdfPCell IVA_cell16_v = new PdfPCell(new Phrase(string.Format("{0:C2}", Convert.ToDecimal(GetValImpuestoByRateCode("IVA16", "01", DsInvoiceAR).ToString())), fontTitle2)) { HorizontalAlignment = Element.ALIGN_RIGHT, };
                PdfPCell IVA_cell_v = new PdfPCell(new Phrase(string.Format("{0:C2}", decimal.Parse(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DocTaxAmt"].ToString())), fontTitle2)) { HorizontalAlignment = Element.ALIGN_RIGHT, };
                PdfPCell RETENCIONES_FUENTE_cell_v = new PdfPCell(new Phrase(string.Format("{0:C2}", Convert.ToDecimal(GetValImpuestoByRateCode("RETEFUENTE", "0B", DsInvoiceAR).ToString())), fontTitle2)) { HorizontalAlignment = Element.ALIGN_RIGHT };
                PdfPCell RETENCIONES_IVA_cell_V = new PdfPCell(new Phrase(string.Format("{0:C2}", Convert.ToDecimal(GetValImpuestoByRateCode("RETEIVA", "0A", DsInvoiceAR).ToString())), fontTitle2)) { HorizontalAlignment = Element.ALIGN_RIGHT };
                PdfPCell RETENCIONES_ICA_cell_V = new PdfPCell(new Phrase(string.Format("{0:C2}", Convert.ToDecimal(GetValImpuestoByRateCode("RETEICA", "0C", DsInvoiceAR).ToString())), fontTitle2)) { HorizontalAlignment = Element.ALIGN_RIGHT };
                PdfPCell DCTO_cell_v = new PdfPCell(new Phrase(totalDescuento.ToString("C2"), fontTitle2)) { HorizontalAlignment = Element.ALIGN_RIGHT, };
                PdfPCell TOTAL_cell_v = new PdfPCell(new Phrase(string.Format("{0:C2}", Convert.ToDecimal(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"].ToString())), fontTitle2)) { HorizontalAlignment = Element.ALIGN_RIGHT, };

                Totales_fin.AddCell(SUBTOTAL_cell);
                Totales_fin.AddCell(SUBTOTAL_cell_v);
                Totales_fin.AddCell(IVA_cell16);
                Totales_fin.AddCell(IVA_cell16_v);
                Totales_fin.AddCell(IVA_cell);
                Totales_fin.AddCell(IVA_cell_v);
                Totales_fin.AddCell(RETENCIONES_FUENTE_cell);
                Totales_fin.AddCell(RETENCIONES_FUENTE_cell_v);
                Totales_fin.AddCell(RETENCIONES_IVA_cell);
                Totales_fin.AddCell(RETENCIONES_IVA_cell_V);
                Totales_fin.AddCell(RETENCIONES_ICA_cell);
                Totales_fin.AddCell(RETENCIONES_ICA_cell_V);
                Totales_fin.AddCell(DCTO_cell);
                Totales_fin.AddCell(DCTO_cell_v);
                Totales_fin.AddCell(TOTAL_cell);
                Totales_fin.AddCell(TOTAL_cell_v);

                totales.AddElement(Totales_fin);

                // Tabla de Consignaciones ---------------------------------------------------------------------------------------
                PdfPTable tableBC1 = new PdfPTable(3);
                tableBC1.HorizontalAlignment = Element.ALIGN_LEFT;
                float[] dimTableBC1 = new float[3];
                dimTableBC1[0] = 1.8f;
                dimTableBC1[1] = 1.0f;
                dimTableBC1[2] = 0.95f;
                tableBC1.SetWidths(dimTableBC1);
                tableBC1.WidthPercentage = 100;

                // Datos de Comprador ----------------------------------------------------------------------------------
                PdfPTable tableDatosLegales = new PdfPTable(1);
                tableDatosLegales.WidthPercentage = 100;

                PdfPCell celprgLegal = new PdfPCell();
                celprgLegal.AddElement(new Paragraph("PASADOS OCHO DIAS NO SE ACEPTAN DEVOLUCIONES.\nCOMPONENTES ELÉCTRICOS NO TIENEN GARANTÍA NI CAMBIO.", fontTitle2));
                celprgLegal.Border = PdfPCell.NO_BORDER;
                celprgLegal.VerticalAlignment = Element.ALIGN_CENTER;
                tableDatosLegales.AddCell(celprgLegal);

                // Datos de Comprador ----------------------------------------------------------------------------------
                PdfPTable tableDatosComprador = new PdfPTable(1);
                tableDatosComprador.WidthPercentage = 100;

                PdfPCell celprgComprador = new PdfPCell();
                celprgComprador.AddElement(new Paragraph("COMPRADOR:", fontTitle2));
                celprgComprador.AddElement(new Paragraph((string)DsInvoiceAR.Tables["Customer"].Rows[0]["Name"], fontCustom));
                celprgComprador.Border = PdfPCell.NO_BORDER;
                tableDatosComprador.AddCell(celprgComprador);

                PdfPCell celprgNit = new PdfPCell();
                celprgNit.AddElement(new Paragraph("NIT./CC. ", fontTitle2));
                celprgNit.AddElement(new Phrase((string)DsInvoiceAR.Tables["Customer"].Rows[0]["ResaleID"], fontCustom));
                celprgNit.Border = PdfPCell.NO_BORDER;
                tableDatosComprador.AddCell(celprgNit);

                // Datos de Consignacion ----------------------------------------------------------------------------------
                //string txtBanco1 = "Banco BBVA";
                //string valBanco1 = "569021793";
                //string txtBanco2 = "Banco Bogota";
                //string valBanco2 = "584264517";

                //PdfPTable tableDatosConsignacion = new PdfPTable(2);
                //tableDatosConsignacion.HorizontalAlignment = Element.ALIGN_RIGHT;
                //tableDatosConsignacion.WidthPercentage = 100;
                //float[] dimTableDatosConsignacion = new float[2];
                //dimTableDatosConsignacion[0] = 1.0f;
                //dimTableDatosConsignacion[1] = 1.5f;
                //tableDatosConsignacion.SetWidths(dimTableDatosConsignacion);

                //PdfPCell celConsignarEn1 = new PdfPCell(new Phrase("Favor Consignar en:", fontTitle2));
                //celConsignarEn1.VerticalAlignment = Element.ALIGN_LEFT;
                //celConsignarEn1.HorizontalAlignment = Element.ALIGN_TOP;
                //celConsignarEn1.Border = PdfPCell.NO_BORDER;
                //celConsignarEn1.Colspan = 2;
                //tableDatosConsignacion.AddCell(celConsignarEn1);

                //PdfPCell celTextBanco1 = new PdfPCell(new Phrase(txtBanco1, fontCustom));
                //celTextBanco1.VerticalAlignment = Element.ALIGN_LEFT;
                //celTextBanco1.HorizontalAlignment = Element.ALIGN_TOP;
                //celTextBanco1.Border = PdfPCell.NO_BORDER;
                //celTextBanco1.Padding = 2;
                //celTextBanco1.Colspan = 1;
                //tableDatosConsignacion.AddCell(celTextBanco1);

                //PdfPCell celCtaBanco1 = new PdfPCell(new Phrase(string.Concat("Cta. Cte. # ", valBanco1), fontCustom));
                //celCtaBanco1.VerticalAlignment = Element.ALIGN_LEFT;
                //celCtaBanco1.Padding = 2;
                //celCtaBanco1.HorizontalAlignment = Element.ALIGN_TOP;
                //celCtaBanco1.Border = PdfPCell.NO_BORDER;
                //celCtaBanco1.Colspan = 1;
                //tableDatosConsignacion.AddCell(celCtaBanco1);

                //PdfPCell celTextBanco2 = new PdfPCell(new Phrase(txtBanco2, fontCustom));
                //celTextBanco2.VerticalAlignment = Element.ALIGN_LEFT;
                //celTextBanco2.HorizontalAlignment = Element.ALIGN_TOP;
                //celTextBanco2.Border = PdfPCell.NO_BORDER;
                //celTextBanco2.Padding = 2;
                //celTextBanco2.Colspan = 1;
                //tableDatosConsignacion.AddCell(celTextBanco2);

                //PdfPCell celCtaBanco2 = new PdfPCell(new Phrase(string.Concat("Cta. Cte. # ", valBanco2), fontCustom));
                //celCtaBanco2.VerticalAlignment = Element.ALIGN_LEFT;
                //celCtaBanco2.HorizontalAlignment = Element.ALIGN_TOP;
                //celCtaBanco2.Border = PdfPCell.NO_BORDER;
                //celCtaBanco2.Padding = 2;
                //celCtaBanco2.Colspan = 1;
                //tableDatosConsignacion.AddCell(celCtaBanco2);

                //PdfPCell celDatosLegales = new PdfPCell(tableDatosLegales);
                //PdfPCell celDatosComprador = new PdfPCell(tableDatosComprador);
                //PdfPCell celConsignarEn = new PdfPCell(tableDatosConsignacion);
                //tableBC1.AddCell(celDatosLegales);
                //tableBC1.AddCell(celDatosComprador);
                //tableBC1.AddCell(celConsignarEn);

                //----------------------------------------------------------------------------------
                PdfPCell _celObservaciones = new PdfPCell(Observaciones) { Border = 0 };
                PdfPCell cell_BC1 = new PdfPCell(tableBC1) { Border = 0, Colspan = 3 };

                Footer.AddCell(_celObservaciones);
                Footer.AddCell(totales);
                Footer.AddCell(celEspacio);
                Footer.AddCell(cell_BC1);


                #endregion

                #region Exit

                document.SetMargins(30f, 30f, 60f, 30f);

                PdfPTable tablaEncabezado = new PdfPTable(1)
                {
                    TotalWidth = document.PageSize.Width - 80f,
                    WidthPercentage = 100,
                };

                //tablaEncabezado.AddCell(new PdfPCell(Header) { Border = 0, Padding = 0 });
                //tablaEncabezado.AddCell(new PdfPCell(Detalles) { Border = 0, Padding = 0 });
                //writer.PageEvent = new EventPageEncabezado(tablaEncabezado, 40f);

                document.Open();
                document.Add(Header);
                document.Add(Detalles);
                document.Add(Body);
                document.Add(tableUnidades);
                document.Add(espacio);
                document.Add(Footer);

                //PIE DE PAGINA//
                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();
                RutaPdf = NomArchivo;

                //Paginacion
                float dimxLeonisa = 540f, dimYLeonisa = 17f;

                AddPageNumberPagTo($"{Ruta}{NomArchivo}", dimxLeonisa, dimYLeonisa + 10, fonPaginacion);
                AddPageContinues($"{Ruta}{NomArchivo}", dimxLeonisa, dimYLeonisa, fonPaginacion);
                return true;
                #endregion
            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.Message;
                return false;
            } 
        }
        #endregion

    }
}