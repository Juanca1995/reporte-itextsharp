﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
//Referenciar y usar.
using System.Data;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Web;
using System.Windows.Forms;

namespace RulesServicesDIAN2.Adquiriente
{
    public partial class pdfEstandarAR
    {
        float dimxFloresa = 540f, dimyFlores = 7f;
        //private PdfPTable DatosFactura;
        public bool FacturaFloresDeOriente(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {
            bool esLocal = Helpers.Compartido.EsLocal;
            string Ruta = string.Empty;
            string RutaImg = string.Empty;
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            if (esLocal)
            {
                Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
                RutaImg = $@"{AppDomain.CurrentDomain.BaseDirectory}\Logo_Fedco.png";
            }
            else
            {
                Ruta = HttpContext.Current.Server.MapPath("~/PDF/" + NIT + "/");
                RutaImg = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";
            }
            NomArchivo = string.Empty;
            NomArchivo = NombreInvoice + ".pdf";

            try
            {
                #region Head

                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.

                Document document = new Document(iTextSharp.text.PageSize.LETTER);

                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;

                // step 3: we open the document     
                document.Open();

                PdfDiv divEspacio = new PdfDiv();
                divEspacio.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio.Height = 10;
                divEspacio.Width = 130;

                PdfDiv divEspacio2 = new PdfDiv();
                divEspacio2.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio2.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                divEspacio2.Height = 2;
                divEspacio2.Width = 130;


                //FUENTES----------------------------------------------------------------------------------------------------------
                iTextSharp.text.Font fontTitleBold = FontFactory.GetFont(FontFactory.HELVETICA, 8.5f, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitleCustomer = FontFactory.GetFont(FontFactory.HELVETICA, 6f, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA, 8f, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitleTotal = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 8f, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustom = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustomBold = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle3 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle4 = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font grande = FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font mediana = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font pequeña = FontFactory.GetFont(FontFactory.HELVETICA, 5, iTextSharp.text.Font.NORMAL);

                //FUENTES---------------------------------------------------------------------------------------------------------

                PdfPCell vacio = new PdfPCell()
                {
                    MinimumHeight = 10,
                    Border = 0
                };
#endregion

                #region Header


                PdfPTable TableHeader = new PdfPTable(3);
                TableHeader.WidthPercentage = 100;
                TableHeader.SetWidths(new float[] { 7.0f, 0.8f, 3.0f });

                //celda de mla imagen logo-------------------------------------------------------------- 
                System.Drawing.Image Logo;
                System.Drawing.Image LogoBanner = null;


                var request = WebRequest.Create(RutaImg);

                using (var response = request.GetResponse())
                using (var stream = response.GetResponseStream())
                {
                    Logo = Bitmap.FromStream(stream);
                }

                //Banner-------------
                try
                {
                    var requestBanner = WebRequest.Create(RutaImg);

                    using (var responseBanner = requestBanner.GetResponse())
                    using (var streamBanner = responseBanner.GetResponseStream())
                    {
                        LogoBanner = Bitmap.FromStream(streamBanner);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error al cargar Logo Banner, not foud\n" + ex.Message);
                }

                //Logo--------------------------------
                iTextSharp.text.Image LogoPdf2 = iTextSharp.text.Image.GetInstance(LogoBanner, BaseColor.WHITE);
                LogoPdf2.ScaleAbsolute(60f, 60f);
                LogoPdf2.Border = 0;

                //QR-----------------------------------
                iTextSharp.text.Image QRPdf = iTextSharp.text.Image.GetInstance(QRInvoice, BaseColor.WHITE);
                QRPdf.ScaleAbsolute(90f, 90f);
                QRPdf.Border = 0;
                QRPdf.PaddingTop = 0;
                //QRPdf.ScaleAbsoluteHeight(10);
                //QRPdf.ScaleAbsoluteHeight(10);


                //PdfPCell CelHeader2 = new PdfPCell() { Border = 0 };

                string tipoFactura = string.Empty;

                tipoFactura = (InvoiceType.ToUpper().Trim(' ') == "INVOICETYPE") ? "FACTURA DE VENTA" :
                    (InvoiceType.ToUpper().Trim(' ') == "CREDITNOTETYPE") ? "NOTA CREDITO" :
                    (InvoiceType.ToUpper().Trim(' ') == "DEBITNOTETYPE") ? "NOTA DEBITO" : string.Empty;

                PdfPCell CelHeader3 = new PdfPCell(QRPdf) { Border = 0, };
                CelHeader3.AddElement(new Phrase(tipoFactura, fontCustomBold));
                CelHeader3.AddElement(QRPdf);
                PdfPCell CelHeader4 = new PdfPCell() { Border = 0 };


                TableHeader.AddCell(LogoPdf2);
                TableHeader.AddCell(CelHeader4);
                TableHeader.AddCell(CelHeader3);

                //info de encabezado----------------------
                PdfPTable TableHeader2 = new PdfPTable(3);
                TableHeader2.WidthPercentage = 100;
                TableHeader2.SetWidths(new float[] { 4.4f, 0.1f, 4.4f });

                PdfPTable TableHeaderCustomer = new PdfPTable(2);
                TableHeaderCustomer.WidthPercentage = 100;
                TableHeaderCustomer.SetWidths(new float[] { 0.8f, 1.2f });
                //TableHeader3.SetWidths(new float[] { 4.4f, 0.1f, 4.4f });

                PdfPCell cuerpo3 = new PdfPCell();

                Paragraph prgTextcustomer = new Paragraph(string.Format($"CUSTOMER: " + DsInvoiceAR.Tables["Customer"].Rows[0]["Name"].ToString(), fontTitleCustomer));
                Paragraph prgTexxtnit = new Paragraph(string.Format($"NIT: " + DsInvoiceAR.Tables["Customer"].Rows[0]["ResaleID"].ToString(), fontTitleCustomer));
                Paragraph prgTextadddress = new Paragraph(string.Format($"ADDRESS: " + DsInvoiceAR.Tables["Customer"].Rows[0]["Address1"].ToString(), fontTitleCustomer));
                Paragraph prgTextcity = new Paragraph(string.Format($"CITY: " + DsInvoiceAR.Tables["Customer"].Rows[0]["City"].ToString(), fontTitleCustomer));
                Paragraph prgTextcounttry = new Paragraph(string.Format($"COUNTTRY: " + DsInvoiceAR.Tables["Customer"].Rows[0]["State"].ToString(), fontTitleCustomer));
                Paragraph prgTextphone = new Paragraph(string.Format($"PHONE NUMBER: " + DsInvoiceAR.Tables["Customer"].Rows[0]["PhoneNum"].ToString(), fontTitleCustomer));

                cuerpo3.AddElement(prgTextcustomer);
                cuerpo3.AddElement(prgTexxtnit);
                cuerpo3.AddElement(prgTextadddress);
                cuerpo3.AddElement(prgTextcity);
                cuerpo3.AddElement(prgTextcounttry);
                cuerpo3.AddElement(prgTextphone);

                PdfPCell Celbody2_0 = new PdfPCell() { Border = 0, };

                PdfPCell Celbody2 = new PdfPCell();
                Paragraph prgFarm = new Paragraph(string.Format("FARM CODE: " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar09"].ToString(), fontTitleCustomer));
                Paragraph prgDate = new Paragraph(string.Format($"FECHA: " + Convert.ToDateTime(DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).ToString("dd-MMM-yyyy"), fontTitleCustomer));
                Paragraph prgCountry = new Paragraph(string.Format($"COUNTRY CODE: " + DsInvoiceAR.Tables["COOneTime"].Rows[0]["CountryCode"].ToString(), fontTitleCustomer));
                Paragraph prgInvoice = new Paragraph(string.Format($"FACTURE No.: " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"].ToString(), fontTitleCustomer));
                Paragraph prgAWB = new Paragraph(string.Format($"AWB: " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar02"].ToString(), fontTitleCustomer));
                Paragraph prgpHAWB = new Paragraph(string.Format($"HAWB: " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar05"].ToString(), fontTitleCustomer));
                Celbody2.AddElement(prgFarm);
                Celbody2.AddElement(prgDate);
                Celbody2.AddElement(prgCountry);
                Celbody2.AddElement(prgInvoice);
                Celbody2.AddElement(prgAWB);
                Celbody2.AddElement(prgpHAWB);

                TableHeader2.AddCell(cuerpo3);
                TableHeader2.AddCell(Celbody2_0);
                TableHeader2.AddCell(Celbody2);

                PdfPTable TableHeader3 = new PdfPTable(3);
                TableHeader3.WidthPercentage = 100;
                TableHeader3.SetWidths(new float[] { 4.4f, 0.1f, 4.4f });

                PdfPCell _celEspacio2 = new PdfPCell(new Phrase($"NOTIFY TO: " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar10"].ToString(), fontTitleCustomer));
                _celEspacio2.HorizontalAlignment = Element.ALIGN_LEFT;
                _celEspacio2.VerticalAlignment = Element.ALIGN_TOP;

                PdfPTable TableHeader3_1 = new PdfPTable(1);
                TableHeader3_1.WidthPercentage = 100;

                PdfPCell _celEspacio1 = new PdfPCell(new Phrase(string.Format($"Airline: " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar11"].ToString(), fontTitleCustomer)))
                { Border = 0 };
                PdfPCell _celEspacio1_1 = new PdfPCell(new Phrase(string.Format($"Port Of Entry: " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar12"].ToString(), fontTitleCustomer)))
                { Border = 0 };
                _celEspacio1.VerticalAlignment = Element.ALIGN_TOP;
                //Paragraph prgAirline = new Paragraph();
                //Paragraph prgPortOfEntry = new Paragraph();
                //_celEspacio1.AddElement(_celEspacio2);
                //_celEspacio1.AddElement(prgAirline);
                //_celEspacio1.AddElement(prgPortOfEntry);
                TableHeader3_1.AddCell(_celEspacio1);
                TableHeader3_1.AddCell(_celEspacio1_1);

                TableHeader3.AddCell(_celEspacio2);
                TableHeader3.AddCell(Celbody2_0);
                TableHeader3.AddCell(TableHeader3_1);


                PdfPTable vacio1 = new PdfPTable(1);
                PdfPCell salto = new PdfPCell() { Border = 0, MinimumHeight = 10 };
                vacio1.AddCell(salto);

                PdfPTable vacio2 = new PdfPTable(1);
                PdfPCell salto2 = new PdfPCell() { Border = 0, MinimumHeight = 80, };
                vacio2.AddCell(salto2);

                PdfPTable vacio3 = new PdfPTable(1);
                PdfPCell salto3 = new PdfPCell() { Border = 0, MinimumHeight = 5, };
                vacio3.AddCell(salto3);

                #endregion

                #region Body

                PdfPTable cheqbottons = new PdfPTable(5);
                cheqbottons.WidthPercentage = 100;
                cheqbottons.SetWidths(new float[] { 3.0f, 3.0f, 2.0f, 0.5f, 0.2f });

                PdfPCell solo = new PdfPCell()
                {
                    Border = 0,
                };
                PdfPCell text = new PdfPCell(new Phrase("FOB Farm (AJMC): " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar03"].ToString(), grande))
                {
                    Border = 0,
                };
                PdfPCell textt = new PdfPCell(new Phrase("Consignment / Consignación: ", grande))
                {
                    Border = 0,
                };
                PdfPCell botones = new PdfPCell(new Phrase((Convert.ToBoolean(DsInvoiceAR.Tables["InvcHead"].Rows[0]["CheckBox01"])) ? "X" : "", grande));
                botones.HorizontalAlignment = Element.ALIGN_CENTER;

                PdfPCell botonesN = new PdfPCell(new Phrase("")) { Border = 0 };

                cheqbottons.AddCell(solo);
                cheqbottons.AddCell(text);
                cheqbottons.AddCell(textt);
                cheqbottons.AddCell(botones);
                cheqbottons.AddCell(botonesN);

                PdfPTable cheqbottons1 = new PdfPTable(5);
                cheqbottons1.WidthPercentage = 100;
                cheqbottons1.SetWidths(new float[] { 3.0f, 3.0f, 2.0f, 0.5f, 0.2f });

                PdfPCell solo1 = new PdfPCell()
                {
                    Border = 0,
                };
                PdfPCell text1 = new PdfPCell(new Phrase("PO#: " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar04"].ToString(), grande))
                {
                    Border = 0,
                };
                PdfPCell textt1 = new PdfPCell(new Phrase("Fixed Price / Venta Directa: ", grande))
                {
                    Border = 0,
                };
                PdfPCell botones1 = new PdfPCell(new Phrase((Convert.ToBoolean(DsInvoiceAR.Tables["InvcHead"].Rows[0]["CheckBox02"])) ? "x" : "", grande));
                botones1.HorizontalAlignment = Element.ALIGN_CENTER;
                PdfPCell botones1N = new PdfPCell(new Phrase("")) { Border = 0 };


                cheqbottons.AddCell(solo1);
                cheqbottons.AddCell(text1);
                cheqbottons.AddCell(textt1);
                cheqbottons.AddCell(botones1);
                cheqbottons.AddCell(botones1N);

                PdfPTable TableHeader4 = new PdfPTable(11);
                TableHeader4.WidthPercentage = 100;
                TableHeader4.SetWidths(new float[] { 0.5f, 2.6f, 0.3f, 1f, 0.5f, 0.5f, 0.5f, 0.5f, 0.5f, 0.5f, 1f, });

                //body es el cuerpo del formato------------------------------------------------------
                //Nota en todas la inmporesiones uso numero -----------------------------------------
                PdfPCell Body1 = new PdfPCell(new Phrase("BOX", mediana)) { HorizontalAlignment = Element.ALIGN_CENTER };
                PdfPCell Body2 = new PdfPCell(new Phrase("DESCRIPCIÓN", mediana)) { HorizontalAlignment = Element.ALIGN_CENTER };
                PdfPCell Body3 = new PdfPCell(new Phrase("AT\n" + "PA", mediana)) { HorizontalAlignment = Element.ALIGN_CENTER };
                PdfPCell Body4 = new PdfPCell(new Phrase("HTS #", mediana)) { HorizontalAlignment = Element.ALIGN_CENTER };
                PdfPCell Body5 = new PdfPCell(new Phrase("UNIT\n/" + "BOX", mediana)) { HorizontalAlignment = Element.ALIGN_CENTER };
                PdfPCell Body6 = new PdfPCell(new Phrase("STEM\n" + "/BUNCH", mediana)) { HorizontalAlignment = Element.ALIGN_CENTER };
                PdfPCell Body7 = new PdfPCell(new Phrase("TOTAL\n" + "/BUNCH", mediana)) { HorizontalAlignment = Element.ALIGN_CENTER };
                PdfPCell Body8 = new PdfPCell(new Phrase("TOTAL\n" + "STEM", mediana)) { HorizontalAlignment = Element.ALIGN_CENTER };
                PdfPCell Body9 = new PdfPCell(new Phrase("PRINCE\n" + "BUNCH", mediana)) { HorizontalAlignment = Element.ALIGN_CENTER };
                PdfPCell Body10 = new PdfPCell(new Phrase("PRINCE\n" + "STEM", mediana)) { HorizontalAlignment = Element.ALIGN_CENTER };
                PdfPCell Body11 = new PdfPCell(new Phrase("TOTAL", mediana)) { HorizontalAlignment = Element.ALIGN_CENTER };

                TableHeader4.AddCell(Body1);
                TableHeader4.AddCell(Body2);
                TableHeader4.AddCell(Body3);
                TableHeader4.AddCell(Body4);
                TableHeader4.AddCell(Body5);
                TableHeader4.AddCell(Body6);
                TableHeader4.AddCell(Body7);
                TableHeader4.AddCell(Body8);
                TableHeader4.AddCell(Body9);
                TableHeader4.AddCell(Body10);
                TableHeader4.AddCell(Body11);

                PdfPTable TableHeader5 = new PdfPTable(11);
                TableHeader5.WidthPercentage = 100;
                TableHeader5.SetWidths(new float[] { 0.5f, 2.6f, 0.3f, 1f, 0.5f, 0.5f, 0.5f, 0.5f, 0.5f, 0.5f, 1f, });

                PdfPTable tableUnidades = new PdfPTable(11);
                tableUnidades.WidthPercentage = 100;
                tableUnidades.SetWidths(new float[] { 0.5f, 2.6f, 0.3f, 1f, 0.5f, 0.5f, 0.5f, 0.5f, 0.5f, 0.5f, 1f, });

                foreach (DataRow InvoiceLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
                {
                    //Helpers.Fedco.AddUnidades_Flores(InvoiceLine, ref tableUnidades, Helpers.Fuentes.Plantilla_2_fontCustom);
                }

                PdfPTable BodyTotal = new PdfPTable(6);
                BodyTotal.WidthPercentage = 100;
                BodyTotal.SetWidths(new float[] { 5.5f, 1.0f, 1.0f, 1.5f, 1.5f, 1.5f });

                PdfPCell bodySubtotal = new PdfPCell(new Phrase("SUBTOTAL" + "", fontTitleTotal));
                PdfPCell body15_1 = new PdfPCell(new Phrase("IVA" + "", fontTitleTotal));
                PdfPCell body14_1 = new PdfPCell(new Phrase("RETEFUENTE" + "", fontTitleTotal));
                PdfPCell body13_1 = new PdfPCell(new Phrase("TOTAL", fontTitleTotal));

                PdfPTable totales = new PdfPTable(6);
                totales.WidthPercentage = 100;
                totales.SetWidths(new float[] { 5.3f, 1.0f, 1.0f, 1.5f, 1.5f, 1.7f });

                PdfPCell totales1 = new PdfPCell() { Border = 0, Colspan = 4 };

                PdfPCell celDatoTotalIva = new PdfPCell();
                celDatoTotalIva.HorizontalAlignment = Element.ALIGN_RIGHT;
                Paragraph prgiva = new Paragraph(new Phrase(string.Format("{0:C2}",
                        decimal.Parse(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DocTaxAmt"].ToString())), Helpers.Fuentes.Plantilla_2_fontTitle2));

                PdfPCell celDatoTotalRte = new PdfPCell();
                celDatoTotalRte.HorizontalAlignment = Element.ALIGN_RIGHT;
                //var Iva = GetValImpuestoByID("01", DsInvoiceAR);
                Paragraph prgRETEFUENTE = new Paragraph(new Phrase(string.Format("{0:C2}",
                        decimal.Parse(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DocWHTaxAmt"].ToString())), Helpers.Fuentes.Plantilla_2_fontTitle2));


                PdfPCell celDatoTotal = new PdfPCell();
                celDatoTotal.HorizontalAlignment = Element.ALIGN_RIGHT;
                Paragraph prgTOTAL = new Paragraph(new Phrase(string.Format("{0:C2}",
                        decimal.Parse(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"].ToString())), Helpers.Fuentes.Plantilla_2_fontTitle2));

                celDatoTotalIva.AddElement(prgiva);
                celDatoTotalRte.AddElement(prgRETEFUENTE);
                celDatoTotal.AddElement(prgTOTAL);

                totales.AddCell(totales1);
                totales.AddCell(bodySubtotal);
                totales.AddCell(new PdfPCell(new Phrase(
                    decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubTotal"]).ToString("C2"),
                    Helpers.Fuentes.Plantilla_2_fontTitle2))
                {
                    //Border=PdfPCell.NO_BORDER,
                });

                totales.AddCell(totales1);
                totales.AddCell(body15_1);
                totales.AddCell(celDatoTotalIva);

                totales.AddCell(totales1);
                totales.AddCell(body14_1);
                totales.AddCell(celDatoTotalRte);

                totales.AddCell(totales1);
                totales.AddCell(body13_1);
                totales.AddCell(celDatoTotal);

                #endregion

                #region Footer
                //pie final del formato----------------------

                PdfPTable pieinicio = new PdfPTable(new float[] { 5.0f, 5.0f, });
                pieinicio.WidthPercentage = 100;

                PdfPCell nombre0 = new PdfPCell() { Border = 0, MinimumHeight = 5, };
                PdfPCell nombre = new PdfPCell(new Phrase("Name: " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar13"].ToString(), grande)) { Border = 0 };

                pieinicio.AddCell(nombre0);
                pieinicio.AddCell(nombre);

                PdfPTable pieinicio1 = new PdfPTable(new float[] { 5.0f, 5.0f, });
                pieinicio1.WidthPercentage = 100;

                PdfPCell nombre0_2 = new PdfPCell(new Phrase("Name and Title Person Preparing Invoice", fontTitle))
                { Border = 0, MinimumHeight = 5, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_BOTTOM };
                PdfPCell nombre2 = new PdfPCell(new Phrase("Number of Identifica:   " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar14"].ToString(), grande)) { Border = 0 };

                pieinicio1.AddCell(nombre0_2);
                pieinicio1.AddCell(nombre2);



                PdfPTable footer = new PdfPTable(new float[] { 4.5f, 0.1f, 4.5f });
                footer.WidthPercentage = 100;
                PdfPCell footer1 = new PdfPCell(new Phrase(DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar13"].ToString(), fontTitle))
                { MinimumHeight = 15, HorizontalAlignment = Element.ALIGN_CENTER };
                PdfPCell footer2 = new PdfPCell() { Border = 0, };
                PdfPCell footer3 = new PdfPCell(new Phrase("FREIGHT FORWARDER", fontTitle));

                footer.AddCell(footer1);
                footer.AddCell(footer2);
                footer.AddCell(footer3);

                PdfPTable footer_2 = new PdfPTable(new float[] { 4.5f, 0.1f, 4.5f });
                footer_2.WidthPercentage = 100;
                PdfPCell footer_21 = new PdfPCell() { MinimumHeight = 15 };
                PdfPCell footer_22 = new PdfPCell() { Border = 0, };
                PdfPCell footer_23 = new PdfPCell();

                footer_2.AddCell(footer_21);
                footer_2.AddCell(footer_22);
                footer_2.AddCell(footer_23);

                PdfPTable footer_ = new PdfPTable(new float[] { 4.5f, 0.1f, 4.5f });
                footer_.WidthPercentage = 100;
                PdfPCell _footer = new PdfPCell(new Phrase("Customs Use Only", fontTitle)) { MinimumHeight = 15, HorizontalAlignment = Element.ALIGN_CENTER };
                PdfPCell _footer1 = new PdfPCell() { Border = 0, };
                PdfPCell _footer2 = new PdfPCell(new Phrase("USDA Aphis P.P.Q Use Only", fontTitle)) { HorizontalAlignment = Element.ALIGN_CENTER };

                footer_.AddCell(_footer);
                footer_.AddCell(_footer1);
                footer_.AddCell(_footer2);


                PdfPTable footerfin = new PdfPTable(1);
                footerfin.WidthPercentage = 100;

                PdfPCell piefin1 = new PdfPCell();

                PdfPCell footerfin2 = new PdfPCell(new Phrase(
                    $"{(string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Character01"]}", fontTitle))
                { MinimumHeight = 50, VerticalAlignment = Element.ALIGN_BOTTOM, HorizontalAlignment = Element.ALIGN_CENTER };

                PdfPCell piefin3 = new PdfPCell();

                footerfin.AddCell(piefin1);
                footerfin.AddCell(footerfin2);
                footerfin.AddCell(piefin3);

                PdfPTable tablePie = new PdfPTable(1);
                tablePie.WidthPercentage = 100;

                PdfPCell celTextCol = new PdfPCell(new Phrase("The flowers and plants on this Invoice where wholly grown in Colombia:" + CUFE, fontTitle))
                { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP };
                tablePie.AddCell(celTextCol);
                PdfPCell cufee = new PdfPCell(new Phrase("CUFE:" + CUFE, fontTitle)) { Border = 0 };
                tablePie.AddCell(cufee);


                #endregion

                #region Exit
                //document.Add(tipodeformato);

                document.Add(TableHeader);
                document.Add(vacio1);
                document.Add(TableHeader2);
                document.Add(vacio1);
                document.Add(TableHeader3);
                document.Add(vacio1);
                document.Add(cheqbottons);
                document.Add(cheqbottons1);
                document.Add(vacio1);
                document.Add(TableHeader4);
                document.Add(vacio1);
                document.Add(TableHeader5);
                document.Add(tableUnidades);
                document.Add(BodyTotal);
                document.Add(totales);
                document.Add(vacio2);
                document.Add(pieinicio);
                document.Add(pieinicio1);
                document.Add(vacio3);
                document.Add(footer);
                document.Add(footer_2);
                document.Add(footer_);
                document.Add(vacio3);
                document.Add(footerfin);
                document.Add(tablePie);


                strError += "inicia se agrega el content byte del pie de pagina pdf\n";
                PdfContentByte pCb = writer.DirectContent; PdfContentByte pCb2 = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();
                strError += "inicia Documento pdf\n";
                AddPageNumberPagTo($"{Ruta}{NomArchivo}", dimxFloresa + 10, 780, fontTitle);
                //AddPageContinueLeonisa($"{Ruta}{NomArchivo}", dimyFlores, 745, fontTitle);
                RutaPdf = NomArchivo;
                strError += "inicia se agrego la numeracion satisfactoriamente OK\n";
                return true;
                #endregion
            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.Message;
                return false;
            }

           
        }

        public bool FacturaFloresDeOrienteFormato2(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {
            bool esLocal = Helpers.Compartido.EsLocal;
            string Ruta = string.Empty;
            string RutaImg = string.Empty;
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            if (esLocal)
            {
                Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
                RutaImg = $@"{AppDomain.CurrentDomain.BaseDirectory}\Logo_890926122.PNG";
            }
            else
            {
                Ruta = HttpContext.Current.Server.MapPath("~/PDF/" + NIT + "/");
                RutaImg = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";
            }
            NomArchivo = string.Empty;
            NomArchivo = NombreInvoice + ".pdf";

            try
            {
                #region Head
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.

                Document document = new Document(iTextSharp.text.PageSize.LETTER);

                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;

                // step 3: we open the document     
                document.Open();

                PdfDiv divEspacio = new PdfDiv();
                divEspacio.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio.Height = 10;
                divEspacio.Width = 130;

                PdfDiv divEspacio2 = new PdfDiv();
                divEspacio2.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio2.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                divEspacio2.Height = 2;
                divEspacio2.Width = 130;


                //FUENTES----------------------------------------------------------------------------------------------------------
                iTextSharp.text.Font fontTitleBold = FontFactory.GetFont(FontFactory.HELVETICA, 8.5f, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitleCustomer = FontFactory.GetFont(FontFactory.HELVETICA, 6f, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA, 8f, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitleTotal = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 8f, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustom = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustomBold = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle3 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle4 = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font grande = FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font mediana = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font pequeña = FontFactory.GetFont(FontFactory.HELVETICA, 5, iTextSharp.text.Font.NORMAL);

                //FUENTES---------------------------------------------------------------------------------------------------------

                PdfPCell vacio = new PdfPCell()
                {
                    MinimumHeight = 10,
                    Border = 0
                };
#endregion

                #region Header


                PdfPTable TableHeader = new PdfPTable(3);
                TableHeader.WidthPercentage = 100;
                TableHeader.SetWidths(new float[] { 7.0f, 0.8f, 3.0f });

                //celda de mla imagen logo-------------------------------------------------------------- 
                System.Drawing.Image Logo;
                System.Drawing.Image LogoBanner = null;


                var request = WebRequest.Create(RutaImg);

                using (var response = request.GetResponse())
                using (var stream = response.GetResponseStream())
                {
                    Logo = Bitmap.FromStream(stream);
                }

                //Banner-------------
                try
                {
                    var requestBanner = WebRequest.Create(RutaImg);

                    using (var responseBanner = requestBanner.GetResponse())
                    using (var streamBanner = responseBanner.GetResponseStream())
                    {
                        LogoBanner = Bitmap.FromStream(streamBanner);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error al cargar Logo Banner, not foud\n" + ex.Message);
                }

                //Logo--------------------------------
                iTextSharp.text.Image LogoPdf2 = iTextSharp.text.Image.GetInstance(LogoBanner, BaseColor.WHITE);
                LogoPdf2.ScaleAbsolute(60f, 60f);
                LogoPdf2.Border = 0;

                //QR-----------------------------------
                iTextSharp.text.Image QRPdf = iTextSharp.text.Image.GetInstance(QRInvoice, BaseColor.WHITE);
                QRPdf.ScaleAbsolute(90f, 90f);
                QRPdf.Border = 0;
                QRPdf.PaddingTop = 0;
                //QRPdf.ScaleAbsoluteHeight(10);
                //QRPdf.ScaleAbsoluteHeight(10);


                //PdfPCell CelHeader2 = new PdfPCell() { Border = 0 };

                string tipoFactura = string.Empty;

                tipoFactura = (InvoiceType.ToUpper().Trim(' ') == "INVOICETYPE") ? "FACTURA DE VENTA" :
                    (InvoiceType.ToUpper().Trim(' ') == "CREDITNOTETYPE") ? "NOTA CREDITO" :
                    (InvoiceType.ToUpper().Trim(' ') == "DEBITNOTETYPE") ? "NOTA DEBITO" : string.Empty;

                PdfPCell CelHeader3 = new PdfPCell(QRPdf) { Border = 0, };
                CelHeader3.AddElement(new Phrase(tipoFactura, fontCustomBold));
                CelHeader3.AddElement(QRPdf);
                PdfPCell CelHeader4 = new PdfPCell() { Border = 0 };


                TableHeader.AddCell(LogoPdf2);
                TableHeader.AddCell(CelHeader4);
                TableHeader.AddCell(CelHeader3);

                //info de encabezado----------------------
                PdfPTable TableHeader2 = new PdfPTable(3);
                TableHeader2.WidthPercentage = 100;
                TableHeader2.SetWidths(new float[] { 4.4f, 0.1f, 4.4f });

                PdfPTable TableHeaderCustomer = new PdfPTable(2);
                TableHeaderCustomer.WidthPercentage = 100;
                TableHeaderCustomer.SetWidths(new float[] { 0.8f, 1.2f });
                //TableHeader3.SetWidths(new float[] { 4.4f, 0.1f, 4.4f });

                PdfPCell cuerpo3 = new PdfPCell();

                Paragraph prgTextcustomer = new Paragraph(string.Format($"CUSTOMER: " + DsInvoiceAR.Tables["Customer"].Rows[0]["Name"].ToString(), fontTitleCustomer));
                Paragraph prgTexxtnit = new Paragraph(string.Format($"NIT: " + DsInvoiceAR.Tables["Customer"].Rows[0]["ResaleID"].ToString(), fontTitleCustomer));
                Paragraph prgTextadddress = new Paragraph(string.Format($"ADDRESS: " + DsInvoiceAR.Tables["Customer"].Rows[0]["Address1"].ToString(), fontTitleCustomer));
                Paragraph prgTextcity = new Paragraph(string.Format($"CITY: " + DsInvoiceAR.Tables["Customer"].Rows[0]["City"].ToString(), fontTitleCustomer));
                Paragraph prgTextcounttry = new Paragraph(string.Format($"COUNTTRY: " + DsInvoiceAR.Tables["Customer"].Rows[0]["State"].ToString(), fontTitleCustomer));
                Paragraph prgTextphone = new Paragraph(string.Format($"PHONE NUMBER: " + DsInvoiceAR.Tables["Customer"].Rows[0]["PhoneNum"].ToString(), fontTitleCustomer));

                cuerpo3.AddElement(prgTextcustomer);
                cuerpo3.AddElement(prgTexxtnit);
                cuerpo3.AddElement(prgTextadddress);
                cuerpo3.AddElement(prgTextcity);
                cuerpo3.AddElement(prgTextcounttry);
                cuerpo3.AddElement(prgTextphone);

                PdfPCell Celbody2_0 = new PdfPCell() { Border = 0, };

                PdfPCell Celbody2 = new PdfPCell();
                Paragraph prgFarm = new Paragraph(string.Format("FARM CODE: " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar09"].ToString(), fontTitleCustomer));
                Paragraph prgDate = new Paragraph(string.Format($"FECHA: " + Convert.ToDateTime(DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).ToString("dd-MMM-yyyy"), fontTitleCustomer));
                Paragraph prgCountry = new Paragraph(string.Format($"COUNTRY CODE: " + DsInvoiceAR.Tables["COOneTime"].Rows[0]["CountryCode"].ToString(), fontTitleCustomer));
                Paragraph prgInvoice = new Paragraph(string.Format($"FACTURE No.: " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"].ToString(), fontTitleCustomer));
                Paragraph prgAWB = new Paragraph(string.Format($"AWB: " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar02"].ToString(), fontTitleCustomer));
                Paragraph prgpHAWB = new Paragraph(string.Format($"HAWB: " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar05"].ToString(), fontTitleCustomer));
                Celbody2.AddElement(prgFarm);
                Celbody2.AddElement(prgDate);
                Celbody2.AddElement(prgCountry);
                Celbody2.AddElement(prgInvoice);
                Celbody2.AddElement(prgAWB);
                Celbody2.AddElement(prgpHAWB);

                TableHeader2.AddCell(cuerpo3);
                TableHeader2.AddCell(Celbody2_0);
                TableHeader2.AddCell(Celbody2);

                PdfPTable TableHeader3 = new PdfPTable(3);
                TableHeader3.WidthPercentage = 100;
                TableHeader3.SetWidths(new float[] { 4.4f, 0.1f, 4.4f });

                PdfPCell _celEspacio2 = new PdfPCell(new Phrase($"NOTIFY TO: " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar10"].ToString(), fontTitleCustomer));
                _celEspacio2.HorizontalAlignment = Element.ALIGN_LEFT;
                _celEspacio2.VerticalAlignment = Element.ALIGN_TOP;

                PdfPTable TableHeader3_1 = new PdfPTable(1);
                TableHeader3_1.WidthPercentage = 100;

                PdfPCell _celEspacio1 = new PdfPCell(new Phrase(string.Format($"Airline: " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar11"].ToString(), fontTitleCustomer)))
                { Border = 0 };
                PdfPCell _celEspacio1_1 = new PdfPCell(new Phrase(string.Format($"Port Of Entry: " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar12"].ToString(), fontTitleCustomer)))
                { Border = 0 };
                _celEspacio1.VerticalAlignment = Element.ALIGN_TOP;
                //Paragraph prgAirline = new Paragraph();
                //Paragraph prgPortOfEntry = new Paragraph();
                //_celEspacio1.AddElement(_celEspacio2);
                //_celEspacio1.AddElement(prgAirline);
                //_celEspacio1.AddElement(prgPortOfEntry);
                TableHeader3_1.AddCell(_celEspacio1);
                TableHeader3_1.AddCell(_celEspacio1_1);

                TableHeader3.AddCell(_celEspacio2);
                TableHeader3.AddCell(Celbody2_0);
                TableHeader3.AddCell(TableHeader3_1);


                PdfPTable vacio1 = new PdfPTable(1);
                PdfPCell salto = new PdfPCell() { Border = 0, MinimumHeight = 10 };
                vacio1.AddCell(salto);

                PdfPTable vacio2 = new PdfPTable(1);
                PdfPCell salto2 = new PdfPCell() { Border = 0, MinimumHeight = 80, };
                vacio2.AddCell(salto2);

                PdfPTable vacio3 = new PdfPTable(1);
                PdfPCell salto3 = new PdfPCell() { Border = 0, MinimumHeight = 5, };
                vacio3.AddCell(salto3);

                #endregion

                #region Body

                PdfPTable cheqbottons = new PdfPTable(5);
                cheqbottons.WidthPercentage = 100;
                cheqbottons.SetWidths(new float[] { 3.0f, 3.0f, 2.0f, 0.5f, 0.2f });

                PdfPCell solo = new PdfPCell()
                {
                    Border = 0,
                };
                PdfPCell text = new PdfPCell(new Phrase("FOB Farm (AJMC): " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar03"].ToString(), grande))
                {
                    Border = 0,
                };
                PdfPCell textt = new PdfPCell(new Phrase("Consignment / Consignación: ", grande))
                {
                    Border = 0,
                };
                PdfPCell botones = new PdfPCell(new Phrase((Convert.ToBoolean(DsInvoiceAR.Tables["InvcHead"].Rows[0]["CheckBox01"])) ? "X" : "", grande));
                botones.HorizontalAlignment = Element.ALIGN_CENTER;

                PdfPCell botonesN = new PdfPCell(new Phrase("")) { Border = 0 };

                cheqbottons.AddCell(solo);
                cheqbottons.AddCell(text);
                cheqbottons.AddCell(textt);
                cheqbottons.AddCell(botones);
                cheqbottons.AddCell(botonesN);

                PdfPTable cheqbottons1 = new PdfPTable(5);
                cheqbottons1.WidthPercentage = 100;
                cheqbottons1.SetWidths(new float[] { 3.0f, 3.0f, 2.0f, 0.5f, 0.2f });

                PdfPCell solo1 = new PdfPCell()
                {
                    Border = 0,
                };
                PdfPCell text1 = new PdfPCell(new Phrase("PO#: " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar04"].ToString(), grande))
                {
                    Border = 0,
                };
                PdfPCell textt1 = new PdfPCell(new Phrase("Fixed Price / Venta Directa: ", grande))
                {
                    Border = 0,
                };
                PdfPCell botones1 = new PdfPCell(new Phrase((Convert.ToBoolean(DsInvoiceAR.Tables["InvcHead"].Rows[0]["CheckBox02"])) ? "x" : "", grande));
                botones1.HorizontalAlignment = Element.ALIGN_CENTER;
                PdfPCell botones1N = new PdfPCell(new Phrase("")) { Border = 0 };


                cheqbottons.AddCell(solo1);
                cheqbottons.AddCell(text1);
                cheqbottons.AddCell(textt1);
                cheqbottons.AddCell(botones1);
                cheqbottons.AddCell(botones1N);

                PdfPTable TableHeader4 = new PdfPTable(7);
                TableHeader4.WidthPercentage = 100;
                TableHeader4.SetWidths(new float[] { 0.5f, 2.6f, 0.5f, 0.5f, 0.5f, 1f, 1f, });

                //body es el cuerpo del formato------------------------------------------------------
                //Nota en todas la inmporesiones uso numero -----------------------------------------
                PdfPCell Body1 = new PdfPCell(new Phrase("BOX", mediana)) { HorizontalAlignment = Element.ALIGN_CENTER };
                PdfPCell Body2 = new PdfPCell(new Phrase("DESCRIPCIÓN", mediana)) { HorizontalAlignment = Element.ALIGN_CENTER };
                //PdfPCell Body3 = new PdfPCell(new Phrase("AT\n" + "PA", mediana)) { HorizontalAlignment = Element.ALIGN_CENTER };
                PdfPCell Body4 = new PdfPCell(new Phrase("HTS #", mediana)) { HorizontalAlignment = Element.ALIGN_CENTER };
                //PdfPCell Body5 = new PdfPCell(new Phrase("UNIT\n/" + "BOX", mediana)) { HorizontalAlignment = Element.ALIGN_CENTER };
                //PdfPCell Body6 = new PdfPCell(new Phrase("STEM\n" + "/BUNCH", mediana)) { HorizontalAlignment = Element.ALIGN_CENTER };
                PdfPCell Body7 = new PdfPCell(new Phrase("UNIDADES", mediana)) { HorizontalAlignment = Element.ALIGN_CENTER };
                PdfPCell Body8 = new PdfPCell(new Phrase("TOTAL\n" + "STEM", mediana)) { HorizontalAlignment = Element.ALIGN_CENTER };
                //PdfPCell Body9 = new PdfPCell(new Phrase("PRINCE\n" + "BUNCH", mediana)) { HorizontalAlignment = Element.ALIGN_CENTER };
                PdfPCell Body10 = new PdfPCell(new Phrase("VALOR UNITARIO", mediana)) { HorizontalAlignment = Element.ALIGN_CENTER };
                PdfPCell Body11 = new PdfPCell(new Phrase("TOTAL", mediana)) { HorizontalAlignment = Element.ALIGN_CENTER };

                TableHeader4.AddCell(Body1);
                TableHeader4.AddCell(Body2);
                //TableHeader4.AddCell(Body3);
                TableHeader4.AddCell(Body4);
                //TableHeader4.AddCell(Body5);
                //TableHeader4.AddCell(Body6);
                TableHeader4.AddCell(Body7);
                TableHeader4.AddCell(Body8);
                //TableHeader4.AddCell(Body9);
                TableHeader4.AddCell(Body10);
                TableHeader4.AddCell(Body11);

                PdfPTable TableHeader5 = new PdfPTable(7);
                TableHeader5.WidthPercentage = 100;
                TableHeader5.SetWidths(new float[] { 0.5f, 2.6f, 0.5f, 0.5f, 0.5f, 1f, 1f, });

                PdfPTable tableUnidades = new PdfPTable(7);
                tableUnidades.WidthPercentage = 100;
                tableUnidades.SetWidths(new float[] { 0.5f, 2.6f, 0.5f, 0.5f, 0.5f, 1f, 1f, });

                foreach (DataRow InvoiceLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
                {
                    AddUnidades_FloresFormato2(InvoiceLine, ref tableUnidades, Helpers.Fuentes.Plantilla_2_fontCustom);
                }

                PdfPTable BodyTotal = new PdfPTable(6);
                BodyTotal.WidthPercentage = 100;
                BodyTotal.SetWidths(new float[] { 5.5f, 1.0f, 1.0f, 1.5f, 1.5f, 1.5f });

                PdfPCell bodySubtotal = new PdfPCell(new Phrase("SUBTOTAL" + "", fontTitleTotal));
                PdfPCell body15_1 = new PdfPCell(new Phrase("IVA" + "", fontTitleTotal));
                PdfPCell body14_1 = new PdfPCell(new Phrase("RETEFUENTE" + "", fontTitleTotal));
                PdfPCell body13_1 = new PdfPCell(new Phrase("TOTAL", fontTitleTotal));

                PdfPTable totales = new PdfPTable(6);
                totales.WidthPercentage = 100;
                totales.SetWidths(new float[] { 5.3f, 1.0f, 1.0f, 1.5f, 1.5f, 1.7f });

                PdfPCell totales1 = new PdfPCell() { Border = 0, Colspan = 4 };

                PdfPCell celDatoTotalIva = new PdfPCell();
                celDatoTotalIva.HorizontalAlignment = Element.ALIGN_RIGHT;
                Paragraph prgiva = new Paragraph(new Phrase(string.Format("{0:C2}",
                        decimal.Parse(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DocTaxAmt"].ToString())), Helpers.Fuentes.Plantilla_2_fontTitle2));

                PdfPCell celDatoTotalRte = new PdfPCell();
                celDatoTotalRte.HorizontalAlignment = Element.ALIGN_RIGHT;
                //var Iva = GetValImpuestoByID("01", DsInvoiceAR);
                Paragraph prgRETEFUENTE = new Paragraph(new Phrase(string.Format("{0:C2}",
                        decimal.Parse(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DocWHTaxAmt"].ToString())), Helpers.Fuentes.Plantilla_2_fontTitle2));


                PdfPCell celDatoTotal = new PdfPCell();
                celDatoTotal.HorizontalAlignment = Element.ALIGN_RIGHT;
                Paragraph prgTOTAL = new Paragraph(new Phrase(string.Format("{0:C2}",
                        decimal.Parse(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"].ToString())), Helpers.Fuentes.Plantilla_2_fontTitle2));

                celDatoTotalIva.AddElement(prgiva);
                celDatoTotalRte.AddElement(prgRETEFUENTE);
                celDatoTotal.AddElement(prgTOTAL);

                totales.AddCell(totales1);
                totales.AddCell(bodySubtotal);
                totales.AddCell(new PdfPCell(new Phrase(
                    decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubTotal"]).ToString("C2"),
                    Helpers.Fuentes.Plantilla_2_fontTitle2))
                {
                    //Border=PdfPCell.NO_BORDER,
                });

                totales.AddCell(totales1);
                totales.AddCell(body15_1);
                totales.AddCell(celDatoTotalIva);

                totales.AddCell(totales1);
                totales.AddCell(body14_1);
                totales.AddCell(celDatoTotalRte);

                totales.AddCell(totales1);
                totales.AddCell(body13_1);
                totales.AddCell(celDatoTotal);

                #endregion

                #region Footer
                //pie final del formato----------------------

                PdfPTable pieinicio = new PdfPTable(new float[] { 5.0f, 5.0f, });
                pieinicio.WidthPercentage = 100;

                PdfPCell nombre0 = new PdfPCell() { Border = 0, MinimumHeight = 5, };
                PdfPCell nombre = new PdfPCell(new Phrase("Name: " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar13"].ToString(), grande)) { Border = 0 };

                pieinicio.AddCell(nombre0);
                pieinicio.AddCell(nombre);

                PdfPTable pieinicio1 = new PdfPTable(new float[] { 5.0f, 5.0f, });
                pieinicio1.WidthPercentage = 100;

                PdfPCell nombre0_2 = new PdfPCell(new Phrase("Name and Title Person Preparing Invoice", fontTitle))
                { Border = 0, MinimumHeight = 5, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_BOTTOM };
                PdfPCell nombre2 = new PdfPCell(new Phrase("Number of Identifica:   " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar14"].ToString(), grande)) { Border = 0 };

                pieinicio1.AddCell(nombre0_2);
                pieinicio1.AddCell(nombre2);



                PdfPTable footer = new PdfPTable(new float[] { 4.5f, 0.1f, 4.5f });
                footer.WidthPercentage = 100;
                PdfPCell footer1 = new PdfPCell(new Phrase(DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar13"].ToString(), fontTitle))
                { MinimumHeight = 15, HorizontalAlignment = Element.ALIGN_CENTER };
                PdfPCell footer2 = new PdfPCell() { Border = 0, };
                PdfPCell footer3 = new PdfPCell(new Phrase("FREIGHT FORWARDER", fontTitle));

                footer.AddCell(footer1);
                footer.AddCell(footer2);
                footer.AddCell(footer3);

                PdfPTable footer_2 = new PdfPTable(new float[] { 4.5f, 0.1f, 4.5f });
                footer_2.WidthPercentage = 100;
                PdfPCell footer_21 = new PdfPCell() { MinimumHeight = 15 };
                PdfPCell footer_22 = new PdfPCell() { Border = 0, };
                PdfPCell footer_23 = new PdfPCell();

                footer_2.AddCell(footer_21);
                footer_2.AddCell(footer_22);
                footer_2.AddCell(footer_23);

                PdfPTable footer_ = new PdfPTable(new float[] { 4.5f, 0.1f, 4.5f });
                footer_.WidthPercentage = 100;
                PdfPCell _footer = new PdfPCell(new Phrase("Customs Use Only", fontTitle)) { MinimumHeight = 15, HorizontalAlignment = Element.ALIGN_CENTER };
                PdfPCell _footer1 = new PdfPCell() { Border = 0, };
                PdfPCell _footer2 = new PdfPCell(new Phrase("USDA Aphis P.P.Q Use Only", fontTitle)) { HorizontalAlignment = Element.ALIGN_CENTER };

                footer_.AddCell(_footer);
                footer_.AddCell(_footer1);
                footer_.AddCell(_footer2);


                PdfPTable footerfin = new PdfPTable(1);
                footerfin.WidthPercentage = 100;

                PdfPCell piefin1 = new PdfPCell(new Phrase("Observaciones: " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceComment"].ToString(), fontTitle))
                { MinimumHeight = 10, PaddingLeft = 20, PaddingBottom = 10 };

                PdfPCell footerfin2 = new PdfPCell(new Phrase(
                    $"{(string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Character01"]}", fontTitle))
                { MinimumHeight = 20, VerticalAlignment = Element.ALIGN_BOTTOM, HorizontalAlignment = Element.ALIGN_CENTER };

                PdfPCell piefin3 = new PdfPCell();

                footerfin.AddCell(piefin1);
                footerfin.AddCell(footerfin2);
                footerfin.AddCell(piefin3);

                PdfPTable tablePie = new PdfPTable(1);
                tablePie.WidthPercentage = 100;

                PdfPCell celTextCol = new PdfPCell(new Phrase("The flowers and plants on this Invoice where wholly grown in Colombia:" + CUFE, fontTitle))
                { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP };
                tablePie.AddCell(celTextCol);
                PdfPCell cufee = new PdfPCell(new Phrase("CUFE:" + CUFE, fontTitle)) { Border = 0 };
                tablePie.AddCell(cufee);


                #endregion

                #region Exit
                //document.Add(tipodeformato);

                document.Add(TableHeader);
                document.Add(vacio1);
                document.Add(TableHeader2);
                document.Add(vacio1);
                document.Add(TableHeader3);
                document.Add(vacio1);
                document.Add(cheqbottons);
                document.Add(cheqbottons1);
                document.Add(vacio1);
                document.Add(TableHeader4);
                document.Add(vacio1);
                document.Add(TableHeader5);
                document.Add(tableUnidades);
                document.Add(BodyTotal);
                document.Add(totales);
                document.Add(vacio2);
                document.Add(pieinicio);
                document.Add(pieinicio1);
                document.Add(vacio3);
                document.Add(footer);
                document.Add(footer_2);
                document.Add(footer_);
                document.Add(vacio3);
                document.Add(footerfin);
                document.Add(tablePie);


                strError += "inicia se agrega el content byte del pie de pagina pdf\n";
                PdfContentByte pCb = writer.DirectContent; PdfContentByte pCb2 = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();
                strError += "inicia Documento pdf\n";
                AddPageNumberPagTo($"{Ruta}{NomArchivo}", dimxFloresa + 10, 780, fontTitle);
                //AddPageContinueLeonisa($"{Ruta}{NomArchivo}", dimyFlores, 745, fontTitle);
                RutaPdf = NomArchivo;
                strError += "inicia se agrego la numeracion satisfactoriamente OK\n";
                return true;
                #endregion
            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.Message;
                return false;
            }

            
        }

        float dimxNoFloresa = 540f, dimyNoFlores = 7f;
        //private PdfPTable DatosFactura;

        public bool NotaCreditoFlores(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {
            bool esLocal = Helpers.Compartido.EsLocal;
            string Ruta = string.Empty;
            string RutaImg = string.Empty;
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            if (esLocal)
            {
                Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
                RutaImg = $@"{AppDomain.CurrentDomain.BaseDirectory}\Logo_Fedco.png";
            }
            else
            {
                Ruta = HttpContext.Current.Server.MapPath("~/PDF/" + NIT + "/");
                RutaImg = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";
            }
            NomArchivo = string.Empty;
            NomArchivo = NombreInvoice + ".pdf";


            try
            {
                #region Head
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.

                Document document = new Document(iTextSharp.text.PageSize.LETTER);

                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;

                // step 3: we open the document     
                document.Open();

                PdfDiv divEspacio = new PdfDiv();
                divEspacio.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio.Height = 10;
                divEspacio.Width = 130;

                PdfDiv divEspacio2 = new PdfDiv();
                divEspacio2.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio2.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                divEspacio2.Height = 2;
                divEspacio2.Width = 130;


                //FUENTES----------------------------------------------------------------------------------------------------------
                iTextSharp.text.Font fontTitleBold = FontFactory.GetFont(FontFactory.HELVETICA, 8.5f, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA, 8.5f, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustom = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustomBold = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle3 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle4 = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustom2 = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);
                //FUENTES---------------------------------------------------------------------------------------------------------

                PdfPCell vacio = new PdfPCell()
                {
                    MinimumHeight = 10,
                    Border = 0
                };

                #endregion

                #region header

                PdfPTable header = new PdfPTable(new float[] { 4.0f, 2.0f, 3.0f });
                header.WidthPercentage = 100;

                System.Drawing.Image Logo;
                System.Drawing.Image LogoBanner = null;

                var request = WebRequest.Create(RutaImg);

                using (var response = request.GetResponse())
                using (var stream = response.GetResponseStream())
                {
                    Logo = Bitmap.FromStream(stream);
                }
                //Banner-------------
                try
                {
                    var requestBanner = WebRequest.Create(RutaImg);

                    using (var responseBanner = requestBanner.GetResponse())
                    using (var streamBanner = responseBanner.GetResponseStream())
                    {
                        LogoBanner = Bitmap.FromStream(streamBanner);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error al cargar Logo Banner, not foud\n" + ex.Message);
                }

                iTextSharp.text.Image LogoPdf2 = iTextSharp.text.Image.GetInstance(LogoBanner, BaseColor.WHITE);
                LogoPdf2.ScaleAbsolute(60f, 60f);

                iTextSharp.text.Image QRPdf = iTextSharp.text.Image.GetInstance(QRInvoice, BaseColor.WHITE);
                QRPdf.ScaleAbsolute(60f, 60f);

                PdfPCell CelHeader2 = new PdfPCell(new Phrase("", fontCustom2));

                //PdfPCell logo = new PdfPCell() ;

                PdfPCell info = new PdfPCell();
                Paragraph prgTitle = new Paragraph(string.Format("NOTE NOTE No. :" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"].ToString(), fontCustom2));
                Paragraph prgTitle2 = new Paragraph(string.Format("DATE :" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"].ToString(), fontCustom2));
                info.AddElement(prgTitle);
                info.AddElement(prgTitle2);

                header.AddCell(LogoPdf2);
                header.AddCell(QRPdf);
                header.AddCell(info);

                PdfPTable salto = new PdfPTable(1);
                salto.WidthPercentage = 100;
                PdfPCell salto1 = new PdfPCell() { Border = 0, MinimumHeight = 10 };
                salto.AddCell(salto1);

                #endregion 

                #region Body

                PdfPTable Body = new PdfPTable(new float[] { 6.0f, 0.1f, 4.0f });
                Body.WidthPercentage = 100;
                PdfPCell cuerpo = new PdfPCell(new Phrase("CUSTOMER'S INFORMATION")) { MinimumHeight = 35, };

                PdfPCell cuerpo2 = new PdfPCell() { Border = 0 };

                PdfPCell cuerpo0 = new PdfPCell(new Phrase("TRANSPORT INFORMATION")) { };
                Body.AddCell(cuerpo);
                Body.AddCell(cuerpo2);
                Body.AddCell(cuerpo0);

                PdfPTable Body2 = new PdfPTable(new float[] { 6.0f, 0.1f, 4.0f });
                Body.WidthPercentage = 100;

                PdfPCell cuerpo3 = new PdfPCell() { MinimumHeight = 80, };

                Paragraph prgcustomer = new Paragraph(string.Format("Customer:  " + DsInvoiceAR.Tables["Customer"].Rows[0]["Name"].ToString(), fontCustom2));
                Paragraph prgnit = new Paragraph(string.Format($"Nit: " + DsInvoiceAR.Tables["Customer"].Rows[0]["CustID"].ToString(), fontCustom2));
                Paragraph prgadddress = new Paragraph(string.Format($"Adddress:  " + DsInvoiceAR.Tables["Customer"].Rows[0]["Address1"].ToString(), fontCustom2));
                Paragraph prgcity = new Paragraph(string.Format($"City:  " + DsInvoiceAR.Tables["Customer"].Rows[0]["City"].ToString(), fontCustom2));
                Paragraph prgcounttry = new Paragraph(string.Format($"Country:  " + DsInvoiceAR.Tables["Customer"].Rows[0]["Country"].ToString(), fontCustom2));
                Paragraph prgphone = new Paragraph(string.Format($"Phone Number:  " + DsInvoiceAR.Tables["Customer"].Rows[0]["PhoneNum"].ToString(), fontCustom2));
                cuerpo3.AddElement(prgcustomer);
                cuerpo3.AddElement(prgnit);
                cuerpo3.AddElement(prgadddress);
                cuerpo3.AddElement(prgcity);
                cuerpo3.AddElement(prgcounttry);
                cuerpo3.AddElement(prgphone);

                PdfPCell cuerpo00 = new PdfPCell() { Border = 0 };

                PdfPCell cuerpo4 = new PdfPCell();
                Paragraph prgshippvia = new Paragraph(string.Format("Shippvia:  " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar06"].ToString(), fontCustom2));
                Paragraph prgawb = new Paragraph(string.Format("A. W. B.:  " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar02"].ToString(), fontCustom2));
                Paragraph prgcustoms = new Paragraph(string.Format("Customs Agent:  " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar07"].ToString(), fontCustom2));
                Paragraph prgFlight = new Paragraph(string.Format("Flight No.:  " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar08"].ToString(), fontCustom2));
                cuerpo4.AddElement(prgshippvia);
                cuerpo4.AddElement(prgawb);
                cuerpo4.AddElement(prgcustoms);
                cuerpo4.AddElement(prgFlight);



                Body.AddCell(cuerpo3);
                Body.AddCell(cuerpo00);
                Body.AddCell(cuerpo4);

                PdfPTable bodyfin = new PdfPTable(1);
                bodyfin.WidthPercentage = 100;

                PdfPCell bodyfin1 = new PdfPCell() { MinimumHeight = 30 };

                Paragraph prgref = new Paragraph(string.Format("REF:" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceRef"].ToString(), fontCustom2));
                bodyfin1.AddElement(prgref);

                Paragraph comm = new Paragraph(string.Format("Observaciones: " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceComment"].ToString(), fontCustom2));
                PdfPCell celComm = new PdfPCell() { MinimumHeight = 30 };
                celComm.AddElement(comm);

                bodyfin.AddCell(new PdfPCell(new Phrase($"CUFE: {CUFE}", fontCustom)) { Border = PdfPCell.NO_BORDER, });
                bodyfin.AddCell(bodyfin1);
                bodyfin.AddCell(celComm);

                #endregion

                #region Footer

                PdfPTable footer = new PdfPTable(new float[] { 2.0f, 2.0f, 4.0f, 2.0f });
                footer.WidthPercentage = 100;

                PdfPCell pie = new PdfPCell(new Phrase("Invoice No:"))
                { Border = 2, };

                PdfPCell pie2 = new PdfPCell(new Phrase("Date:"))
                { Border = 2, };

                PdfPCell pie3 = new PdfPCell(new Phrase(""))
                { Border = 2, };

                PdfPCell pie4 = new PdfPCell(new Phrase(""))
                { Border = 2, };

                footer.AddCell(pie);
                footer.AddCell(pie2);
                footer.AddCell(pie3);
                footer.AddCell(pie4);

                PdfPTable footer2 = new PdfPTable(new float[] { 2.0f, 2.0f, 4.0f, 2.0f });
                footer.WidthPercentage = 100;

                PdfPCell pies = new PdfPCell() { Border = 2, };
                Paragraph prgInvoice = new Paragraph(string.Format(DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"].ToString(), fontCustom2));
                pies.AddElement(prgInvoice);

                PdfPCell pies1 = new PdfPCell() { Border = 2, };
                Paragraph prgdate = new Paragraph(string.Format(DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"]).ToString("dd/MM/yyyy"),
                    fontCustom2));
                pies1.AddElement(prgdate);

                PdfPCell pies2 = new PdfPCell() { Border = 2, };

                PdfPCell pies3 = new PdfPCell() { Border = 2, };
                Paragraph prgvalor = new Paragraph(string.Format(Decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubTotal"]).ToString("N0"), fontCustom2));
                pies2.AddElement(prgvalor);

                footer.AddCell(pies);
                footer.AddCell(pies1);
                footer.AddCell(pies2);
                footer.AddCell(pies3);

                PdfPTable footerfin = new PdfPTable(new float[] { 3.0f, 2.0f, 4.0f, 2.0f });
                footer.WidthPercentage = 100;

                PdfPCell pief = new PdfPCell() { Border = 0, };
                Paragraph prgtotaln = new Paragraph(string.Format("TOTAL CREDIT NOTE:" + Decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceRef"]).ToString("N0"), fontCustom2));
                Paragraph prgobserv = new Paragraph(string.Format("Observaciones:" +
                    DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceComment"].ToString(), fontCustom2));
                pief.AddElement(prgtotaln);
                //pief.AddElement(prgobserv);

                PdfPCell piesf1 = new PdfPCell() { Border = 0, };
                PdfPCell piesf2 = new PdfPCell() { Border = 0, };
                PdfPCell piesf3 = new PdfPCell() { Border = 0, };
                PdfPCell piesf4 = new PdfPCell() { Border = 0, };

                Paragraph prgtotal = new Paragraph(string.Format("TOTAL: " + "  $ " + decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"]).ToString("N0"), fontCustom2));
                Paragraph prgiva = new Paragraph(string.Format("IVA:" + " $ " + decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DocTaxAmt"]).ToString("N0"), fontCustom2));
                piesf4.AddElement(prgtotal);
                piesf4.AddElement(prgiva);

                footer.AddCell(pief);
                footer.AddCell(piesf1);
                footer.AddCell(piesf2);
                footer.AddCell(piesf4);

                #endregion

                #region Exit

                document.Add(header);
                document.Add(salto);
                document.Add(Body);
                document.Add(Body2);
                document.Add(salto);
                document.Add(bodyfin);
                document.Add(salto);
                document.Add(salto);
                document.Add(footer);
                document.Add(footer2);
                document.Add(footerfin);

                strError += "inicia se agrega el content byte del pie de pagina pdf\n";
                PdfContentByte pCb = writer.DirectContent; PdfContentByte pCb2 = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();
                strError += "inicia Documento pdf\n";
                AddPageNumberPagTo($"{Ruta}{NomArchivo}", dimxNoFloresa, 755, fontTitle);
                AddPageContinueLeonisa($"{Ruta}{NomArchivo}", dimyNoFlores, 745, fontTitle);
                RutaPdf = NomArchivo;
                strError += "inicia se agrego la numeracion satisfactoriamente OK\n";
                return true;
                #endregion
            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.Message;
                return false;
            }
        }

        public bool NotaDebitoFlores(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {
            bool esLocal = Helpers.Compartido.EsLocal;
            string Ruta = string.Empty;
            string RutaImg = string.Empty;
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            if (esLocal)
            {
                Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
                RutaImg = $@"{AppDomain.CurrentDomain.BaseDirectory}\Logo_Fedco.png";
            }
            else
            {
                Ruta = HttpContext.Current.Server.MapPath("~/PDF/" + NIT + "/");
                RutaImg = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";
            }
            NomArchivo = string.Empty;
            NomArchivo = NombreInvoice + ".pdf";


            try
            {
                #region Head
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.

                Document document = new Document(iTextSharp.text.PageSize.LETTER);

                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;

                // step 3: we open the document     
                document.Open();

                PdfDiv divEspacio = new PdfDiv();
                divEspacio.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio.Height = 10;
                divEspacio.Width = 130;

                PdfDiv divEspacio2 = new PdfDiv();
                divEspacio2.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio2.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                divEspacio2.Height = 2;
                divEspacio2.Width = 130;


                //FUENTES----------------------------------------------------------------------------------------------------------
                iTextSharp.text.Font fontTitleBold = FontFactory.GetFont(FontFactory.HELVETICA, 8.5f, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA, 8.5f, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustom = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustomBold = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle3 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle4 = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustom2 = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);
                //FUENTES---------------------------------------------------------------------------------------------------------

                PdfPCell vacio = new PdfPCell()
                {
                    MinimumHeight = 10,
                    Border = 0
                };

                #endregion

                #region header

                PdfPTable header = new PdfPTable(new float[] { 4.0f, 2.0f, 3.0f });
                header.WidthPercentage = 100;

                System.Drawing.Image Logo;
                System.Drawing.Image LogoBanner = null;

                var request = WebRequest.Create(RutaImg);

                using (var response = request.GetResponse())
                using (var stream = response.GetResponseStream())
                {
                    Logo = Bitmap.FromStream(stream);
                }
                //Banner-------------
                try
                {
                    var requestBanner = WebRequest.Create(RutaImg);

                    using (var responseBanner = requestBanner.GetResponse())
                    using (var streamBanner = responseBanner.GetResponseStream())
                    {
                        LogoBanner = Bitmap.FromStream(streamBanner);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error al cargar Logo Banner, not foud\n" + ex.Message);
                }

                iTextSharp.text.Image LogoPdf2 = iTextSharp.text.Image.GetInstance(LogoBanner, BaseColor.WHITE);
                LogoPdf2.ScaleAbsolute(60f, 60f);

                iTextSharp.text.Image QRPdf = iTextSharp.text.Image.GetInstance(QRInvoice, BaseColor.WHITE);
                QRPdf.ScaleAbsolute(60f, 60f);

                PdfPCell CelHeader2 = new PdfPCell(new Phrase("", fontCustom2));

                //PdfPCell logo = new PdfPCell() ;

                PdfPCell info = new PdfPCell();
                Paragraph prgTitle = new Paragraph(string.Format("DEBIT NOTE No. :" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"].ToString(), fontCustom2));
                Paragraph prgTitle2 = new Paragraph(string.Format("DATE :" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"].ToString(), fontCustom2));
                info.AddElement(prgTitle);
                info.AddElement(prgTitle2);

                header.AddCell(LogoPdf2);
                header.AddCell(QRPdf);
                header.AddCell(info);

                PdfPTable salto = new PdfPTable(1);
                salto.WidthPercentage = 100;
                PdfPCell salto1 = new PdfPCell() { Border = 0, MinimumHeight = 10 };
                salto.AddCell(salto1);

                #endregion 

                #region Body

                PdfPTable Body = new PdfPTable(new float[] { 6.0f, 0.1f, 4.0f });
                Body.WidthPercentage = 100;
                PdfPCell cuerpo = new PdfPCell(new Phrase("CUSTOMER'S INFORMATION")) { MinimumHeight = 35, };

                PdfPCell cuerpo2 = new PdfPCell() { Border = 0 };

                PdfPCell cuerpo0 = new PdfPCell(new Phrase("TRANSPORT INFORMATION")) { };
                Body.AddCell(cuerpo);
                Body.AddCell(cuerpo2);
                Body.AddCell(cuerpo0);

                PdfPTable Body2 = new PdfPTable(new float[] { 6.0f, 0.1f, 4.0f });
                Body.WidthPercentage = 100;

                PdfPCell cuerpo3 = new PdfPCell() { MinimumHeight = 80, };

                Paragraph prgcustomer = new Paragraph(string.Format("Customer:  " + DsInvoiceAR.Tables["Customer"].Rows[0]["Name"].ToString(), fontCustom2));
                Paragraph prgnit = new Paragraph(string.Format($"Nit: " + DsInvoiceAR.Tables["Customer"].Rows[0]["CustID"].ToString(), fontCustom2));
                Paragraph prgadddress = new Paragraph(string.Format($"Adddress:  " + DsInvoiceAR.Tables["Customer"].Rows[0]["Address1"].ToString(), fontCustom2));
                Paragraph prgcity = new Paragraph(string.Format($"City:  " + DsInvoiceAR.Tables["Customer"].Rows[0]["City"].ToString(), fontCustom2));
                Paragraph prgcounttry = new Paragraph(string.Format($"Country:  " + DsInvoiceAR.Tables["Customer"].Rows[0]["Country"].ToString(), fontCustom2));
                Paragraph prgphone = new Paragraph(string.Format($"Phone Number:  " + DsInvoiceAR.Tables["Customer"].Rows[0]["PhoneNum"].ToString(), fontCustom2));
                cuerpo3.AddElement(prgcustomer);
                cuerpo3.AddElement(prgnit);
                cuerpo3.AddElement(prgadddress);
                cuerpo3.AddElement(prgcity);
                cuerpo3.AddElement(prgcounttry);
                cuerpo3.AddElement(prgphone);

                PdfPCell cuerpo00 = new PdfPCell() { Border = 0 };

                PdfPCell cuerpo4 = new PdfPCell();
                Paragraph prgshippvia = new Paragraph(string.Format("Shippvia:  " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar06"].ToString(), fontCustom2));
                Paragraph prgawb = new Paragraph(string.Format("A. W. B.:  " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar02"].ToString(), fontCustom2));
                Paragraph prgcustoms = new Paragraph(string.Format("Customs Agent:  " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar07"].ToString(), fontCustom2));
                Paragraph prgFlight = new Paragraph(string.Format("Flight No.:  " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar08"].ToString(), fontCustom2));
                cuerpo4.AddElement(prgshippvia);
                cuerpo4.AddElement(prgawb);
                cuerpo4.AddElement(prgcustoms);
                cuerpo4.AddElement(prgFlight);



                Body.AddCell(cuerpo3);
                Body.AddCell(cuerpo00);
                Body.AddCell(cuerpo4);

                PdfPTable bodyfin = new PdfPTable(1);
                bodyfin.WidthPercentage = 100;

                PdfPCell bodyfin1 = new PdfPCell() { MinimumHeight = 30 };

                Paragraph prgref = new Paragraph(string.Format("REF:" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceRef"].ToString(), fontCustom2));
                bodyfin1.AddElement(prgref);

                Paragraph comm = new Paragraph(string.Format("Observaciones: " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceComment"].ToString(), fontCustom2));
                PdfPCell celComm = new PdfPCell() { MinimumHeight = 30 };
                celComm.AddElement(comm);

                bodyfin.AddCell(new PdfPCell(new Phrase($"CUFE: {CUFE}", fontCustom)) { Border = PdfPCell.NO_BORDER, });
                bodyfin.AddCell(bodyfin1);
                bodyfin.AddCell(celComm);

                #endregion

                #region Footer

                PdfPTable footer = new PdfPTable(new float[] { 2.0f, 2.0f, 4.0f, 2.0f });
                footer.WidthPercentage = 100;

                PdfPCell pie = new PdfPCell(new Phrase("Invoice No:"))
                { Border = 2, };

                PdfPCell pie2 = new PdfPCell(new Phrase("Date:"))
                { Border = 2, };

                PdfPCell pie3 = new PdfPCell(new Phrase(""))
                { Border = 2, };

                PdfPCell pie4 = new PdfPCell(new Phrase(""))
                { Border = 2, };

                footer.AddCell(pie);
                footer.AddCell(pie2);
                footer.AddCell(pie3);
                footer.AddCell(pie4);

                PdfPTable footer2 = new PdfPTable(new float[] { 2.0f, 2.0f, 4.0f, 2.0f });
                footer.WidthPercentage = 100;

                PdfPCell pies = new PdfPCell() { Border = 2, };
                Paragraph prgInvoice = new Paragraph(string.Format(DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"].ToString(), fontCustom2));
                pies.AddElement(prgInvoice);

                PdfPCell pies1 = new PdfPCell() { Border = 2, };
                Paragraph prgdate = new Paragraph(string.Format(DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"]).ToString("dd/MM/yyyy"),
                    fontCustom2));
                pies1.AddElement(prgdate);

                PdfPCell pies2 = new PdfPCell() { Border = 2, };

                PdfPCell pies3 = new PdfPCell() { Border = 2, };
                Paragraph prgvalor = new Paragraph(string.Format(Decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubTotal"]).ToString("N0"), fontCustom2));
                pies2.AddElement(prgvalor);

                footer.AddCell(pies);
                footer.AddCell(pies1);
                footer.AddCell(pies2);
                footer.AddCell(pies3);

                PdfPTable footerfin = new PdfPTable(new float[] { 3.0f, 2.0f, 4.0f, 2.0f });
                footer.WidthPercentage = 100;

                PdfPCell pief = new PdfPCell() { Border = 0, };
                Paragraph prgtotaln = new Paragraph(string.Format("TOTAL CREDIT NOTE:" + Decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceRef"]).ToString("N0"), fontCustom2));
                Paragraph prgobserv = new Paragraph(string.Format("Observaciones:" +
                    DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceComment"].ToString(), fontCustom2));
                pief.AddElement(prgtotaln);
                //pief.AddElement(prgobserv);

                PdfPCell piesf1 = new PdfPCell() { Border = 0, };
                PdfPCell piesf2 = new PdfPCell() { Border = 0, };
                PdfPCell piesf3 = new PdfPCell() { Border = 0, };
                PdfPCell piesf4 = new PdfPCell() { Border = 0, };

                Paragraph prgtotal = new Paragraph(string.Format("TOTAL: " + "  $ " + decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"]).ToString("N0"), fontCustom2));
                Paragraph prgiva = new Paragraph(string.Format("IVA:" + " $ " + decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DocTaxAmt"]).ToString("N0"), fontCustom2));
                piesf4.AddElement(prgtotal);
                piesf4.AddElement(prgiva);

                footer.AddCell(pief);
                footer.AddCell(piesf1);
                footer.AddCell(piesf2);
                footer.AddCell(piesf4);

                #endregion

                #region Exit

                document.Add(header);
                document.Add(salto);
                document.Add(Body);
                document.Add(Body2);
                document.Add(salto);
                document.Add(bodyfin);
                document.Add(salto);
                document.Add(salto);
                document.Add(footer);
                document.Add(footer2);
                document.Add(footerfin);

                strError += "inicia se agrega el content byte del pie de pagina pdf\n";
                PdfContentByte pCb = writer.DirectContent; PdfContentByte pCb2 = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();
                strError += "inicia Documento pdf\n";
                AddPageNumberPagTo($"{Ruta}{NomArchivo}", dimxNoFloresa, 755, fontTitle);
                AddPageContinueLeonisa($"{Ruta}{NomArchivo}", dimyNoFlores, 745, fontTitle);
                RutaPdf = NomArchivo;
                strError += "inicia se agrego la numeracion satisfactoriamente OK\n";
                return true;
                #endregion
            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.Message;
                return false;
            }
        }

        public static void AddUnidades_FloresFormato2(DataRow dataLine, ref PdfPTable pdfPTable, iTextSharp.text.Font font)
        {

            Helpers.Plantilla_2.DetallesCeldas(ref pdfPTable, dataLine["NumberBoxes"].ToString() + "  " + dataLine["SalesUM"].ToString(), font); //partnum
            Helpers.Plantilla_2.DetallesCeldas(ref pdfPTable, dataLine["LineDesc"].ToString(), font); //partnum
            //Helpers.Plantilla_2.DetallesCeldas(ref pdfPTable, (Convert.ToBoolean(dataLine["CheckBox01"])) ? "X" : "", font); //partnum
            Helpers.Plantilla_2.DetallesCeldas(ref pdfPTable, dataLine["ShortChar01"].ToString(), font); //partnum    
            //Helpers.Plantilla_2.DetallesCeldas(ref pdfPTable, string.Format("{0:N2}", dataLine["Number06"]), font); //Unit/Box
            //Helpers.Plantilla_2.DetallesCeldas(ref pdfPTable, string.Format("{0:N2}", dataLine["Number01"]), font); //partnum
            Helpers.Plantilla_2.DetallesCeldas(ref pdfPTable, decimal.Parse(dataLine["SellingShipQty"].ToString()).ToString("N2"), font); //descripcion
            Helpers.Plantilla_2.DetallesCeldas(ref pdfPTable, string.Format("{0:N2}", dataLine["Number03"]), font); //descripcion
            //Helpers.Plantilla_2.DetallesCeldas(ref pdfPTable, string.Format("{0:N2}", dataLine["Number04"]), font); //descripcion
            Helpers.Plantilla_2.DetallesCeldas(ref pdfPTable, decimal.Parse(dataLine["DocUnitPrice"].ToString()).ToString("N2"), font); //descripcion
            Helpers.Plantilla_2.DetallesCeldas(ref pdfPTable, string.Format("{0:C2}", decimal.Parse(dataLine["DspDocExtPrice"].ToString())), font); //Totallinea
        }

        private bool AddUnidadesFloresOriente(DataRow dataLine, ref PdfPTable table, iTextSharp.text.Font fontTitleFactura, DataSet dataSet)
        {
            try
            {
                //descriccion
                strError += "Add InvoiceLine";
                iTextSharp.text.pdf.PdfPCell celValL = new iTextSharp.text.pdf.PdfPCell(new Phrase((string)dataLine["LineDesc"], fontTitleFactura));
                celValL.Colspan = 1;
                celValL.Padding = 2;
                celValL.Border = PdfPCell.RIGHT_BORDER | PdfPCell.LEFT_BORDER;
                //celTextL.BorderWidthRight = 1;
                //celValL.BorderWidthLeft = 1;
                celValL.HorizontalAlignment = Element.ALIGN_LEFT;
                celValL.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValL);
                strError += "InvoiceLine OK";

                //pioezas
                strError += "Add PartNum";
                iTextSharp.text.pdf.PdfPCell celValCodigo = new iTextSharp.text.pdf.PdfPCell(new Phrase((string)dataLine["Number10"], fontTitleFactura));
                celValCodigo.Colspan = 1;
                celValCodigo.Padding = 2;
                celValCodigo.Border = PdfPCell.RIGHT_BORDER;
                //celValCodigo.BorderWidthLeft = 1;
                //celDescripcion.BorderColorBottom = BaseColor.WHITE;
                celValCodigo.HorizontalAlignment = Element.ALIGN_CENTER;
                celValCodigo.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValCodigo);
                strError += "Add PartNum OK";

                //ramos
                strError += "Add SellingShipQty";
                iTextSharp.text.pdf.PdfPCell celValDescripcion = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    decimal.Parse((string)dataLine["Number07"]).ToString("N0"), fontTitleFactura));
                celValDescripcion.Colspan = 1;
                celValDescripcion.Padding = 2;
                celValDescripcion.Border = PdfPCell.RIGHT_BORDER;
                //celValCantidad.BorderWidthLeft = 1;
                celValDescripcion.HorizontalAlignment = Element.ALIGN_CENTER;
                celValDescripcion.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValDescripcion);
                strError += "Add SellingShipQty OK";

                //tallos
                strError += "Add SalesUM";
                iTextSharp.text.pdf.PdfPCell celValUnidad = new iTextSharp.text.pdf.PdfPCell(new Phrase("  " +
                    decimal.Parse((string)dataLine["Number08"]).ToString("N0"), fontTitleFactura));
                celValUnidad.Colspan = 1;
                celValUnidad.Padding = 2;
                celValUnidad.Border = PdfPCell.RIGHT_BORDER;
                //celValUnidad.BorderWidthLeft = 1;
                celValUnidad.HorizontalAlignment = Element.ALIGN_CENTER;
                celValUnidad.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValUnidad);
                strError += "Add SalesUM OK";

                //precio de tallo
                strError += "Add LineDesc";
                iTextSharp.text.pdf.PdfPCell celValCantidad = new iTextSharp.text.pdf.PdfPCell(new Phrase("$ " +
                    decimal.Parse((string)dataLine["Number09"]).ToString("N2"), fontTitleFactura));
                celValCantidad.Colspan = 1;
                celValCantidad.Padding = 2;
                celValCantidad.Border = PdfPCell.RIGHT_BORDER;
                //celValDesc.BorderWidthLeft = 1;
                celValCantidad.HorizontalAlignment = Element.ALIGN_CENTER;
                celValCantidad.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValCantidad);
                strError += "Add LineDesc OK";

                //total uss
                strError += "Add LineDesc";
                iTextSharp.text.pdf.PdfPCell celValCantidad1 = new iTextSharp.text.pdf.PdfPCell(new Phrase("$ " +
                    decimal.Parse((string)dataLine["DspDocExtPrice"]).ToString("N2"), fontTitleFactura));
                celValCantidad1.Colspan = 1;
                celValCantidad1.Padding = 2;
                //celValCantidad1.Border = 0;
                celValCantidad1.Border = PdfPCell.RIGHT_BORDER;
                //celValDesc.BorderWidthLeft = 1;
                celValCantidad1.HorizontalAlignment = Element.ALIGN_CENTER;
                celValCantidad1.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValCantidad1);
                strError += "Add LineDesc OK";

                return true;
            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.Message;
                return false;
            }
        }

        public bool FacturadeExportacionFroresdeOriente(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {

            NomArchivo = string.Empty;
            string Ruta = HttpContext.Current.Server.MapPath("~/PDF/" + NIT + "/");
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            string RutaImg = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";

            NomArchivo = NombreInvoice + ".pdf";

            try
            {
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.
                Document document = new Document(iTextSharp.text.PageSize.LETTER);
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;

                // step 3: we open the document     
                document.Open();

                PdfDiv divEspacio = new PdfDiv();
                divEspacio.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio.Height = 10;
                divEspacio.Width = 130;

                PdfDiv divEspacio2 = new PdfDiv();
                divEspacio2.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio2.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio2.Height = 2;
                divEspacio2.Width = 130;

                #region Head
                //FUENTES----------------------------------------------------------------------------------------------------------
                iTextSharp.text.Font titulo = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 10, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font titulofactura = FontFactory.GetFont(FontFactory.TIMES_BOLD, 10, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle1 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 10, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font titulosdetablas = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 12, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle2 = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustom = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustom2 = FontFactory.GetFont(FontFactory.HELVETICA, 9, iTextSharp.text.Font.NORMAL);
                //FUENTES---------------------------------------------------------------------------------------------------------
                System.Drawing.Image Logo;
                System.Drawing.Image LogoBanner = null;
                var request = WebRequest.Create(RutaImg);
                using (var response = request.GetResponse())
                using (var stream = response.GetResponseStream())
                {
                    Logo = Bitmap.FromStream(stream);
                }
                var requestBanner = WebRequest.Create(RutaImg);
                using (var responseBanner = requestBanner.GetResponse())
                using (var streamBanner = responseBanner.GetResponseStream())
                {
                    LogoBanner = Bitmap.FromStream(streamBanner);
                }

                iTextSharp.text.Image LogoPdf2 = iTextSharp.text.Image.GetInstance(LogoBanner, BaseColor.WHITE);
                LogoPdf2.ScaleAbsolute(100, 100);
                LogoPdf2.Border = 0;

                iTextSharp.text.Image QRPdf = iTextSharp.text.Image.GetInstance(QRInvoice, BaseColor.WHITE);
                QRPdf.ScaleAbsolute(80, 80);
                //QRPdf.Border = 0;


                PdfPTable espacio = new PdfPTable(1);
                espacio.WidthPercentage = 100;
                PdfPCell salto = new PdfPCell() { Border = 0, MinimumHeight = 5, };
                espacio.AddCell(salto);

                #endregion

                #region  Header

                PdfPTable header = new PdfPTable(new float[] { 0.2f, 0.34f, 0.16f, 0.3f });
                header.WidthPercentage = 100;

                PdfPCell cell_logo = new PdfPCell() { /*MinimumHeight = 80*/};
                cell_logo.AddElement(LogoPdf2);

                PdfPCell cell_info = new PdfPCell() { };

                Paragraph prgTitle = new Paragraph(string.Format("{0}\n.{1}", DsInvoiceAR.Tables["Company"].Rows[0]["Name"],
                "Nit" + DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"]), fontTitle);
                Paragraph prgTitle2 = new Paragraph(string.Format("{0}\n{1}\n\n{2}",
                    DsInvoiceAR.Tables["Company"].Rows[0]["Address1"].ToString(),
                "Grandes contribuyentes según resolucion DIAN 000076 de Diciembre 1 de 2016",
                    DsInvoiceAR.Tables["Company"].Rows[0]["City"].ToString(),
                    DsInvoiceAR.Tables["Company"].Rows[0]["State"].ToString()),
                    fontCustom);
                prgTitle.Alignment = Element.ALIGN_CENTER;
                prgTitle2.Alignment = Element.ALIGN_CENTER;
                cell_info.AddElement(prgTitle);
                cell_info.AddElement(prgTitle2);

                PdfPCell cell_Qr = new PdfPCell() { VerticalAlignment = Element.ALIGN_CENTER, HorizontalAlignment = Element.ALIGN_CENTER, };
                cell_Qr.AddElement(QRPdf);


                PdfPCell cellfactura = new PdfPCell();
                Paragraph cellfacturaT_ = new Paragraph(string.Format($"FACTURA DE EXPORTACION No. {(string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"]}",
                    FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 10f, iTextSharp.text.Font.NORMAL)));
                cellfacturaT_.Alignment = Element.ALIGN_CENTER;
                Paragraph fecha_factura = new Paragraph(string.Format($"\nFecha Factura: " +
                    $"{DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).ToString("dd/MM/yyyy")}", fontCustom));
                fecha_factura.Alignment = Element.ALIGN_RIGHT;

                cellfactura.AddElement(cellfacturaT_);
                cellfactura.AddElement(fecha_factura);

                header.AddCell(cell_logo);
                header.AddCell(cell_info);
                header.AddCell(cell_Qr);
                header.AddCell(cellfactura);

                #endregion

                #region  Body 

                PdfPTable Body = new PdfPTable(2);
                Body.WidthPercentage = 100;


                PdfPCell info_cliente = new PdfPCell(new Phrase("INFORMACION CLIENTES", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_BOTTOM, };

                PdfPCell info_trasporte = new PdfPCell(new Phrase("INFORMACION TRANSPORTE", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_BOTTOM, };


                Body.AddCell(info_cliente);
                Body.AddCell(info_trasporte);


                PdfPTable Body_2 = new PdfPTable(2);
                Body_2.WidthPercentage = 100;
                PdfPCell info_cloente_2 = new PdfPCell(new Phrase());
                Paragraph info_cliente3 = new Paragraph(string.Format("" + "{0}\n{1}\n\n{2}\n{3} ",
                  "Cliente  : " + DsInvoiceAR.Tables["Customer"].Rows[0]["Company"].ToString(),
                  "Direccion :  " + DsInvoiceAR.Tables["Customer"].Rows[0]["Address1"].ToString(),
                   "Ciudad-Pais : " + DsInvoiceAR.Tables["Customer"].Rows[0]["City"].ToString() + " - " + DsInvoiceAR.Tables["Customer"].Rows[0]["Country"].ToString(),
                   "Telefono : " + DsInvoiceAR.Tables["Customer"].Rows[0]["PhoneNum"].ToString()),
                fontCustom);
                info_cliente3.Alignment = Element.ALIGN_LEFT;


                info_cloente_2.AddElement(info_cliente3);
                PdfPCell info_trnasporte_2 = new PdfPCell();
                Paragraph info_traspor = new Paragraph(string.Format("{0}\n{1}\n{2} ",
                                                                      " Agente de la Duena:  " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar15"],
                                                                      " Agente de Carga: " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar16"].ToString(),
                                                                      " Aseptacion Global:  " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number04"].ToString()),
                   fontCustom);
                info_traspor.Alignment = Element.ALIGN_LEFT;
                info_trnasporte_2.AddElement(info_traspor);

                Body_2.AddCell(info_cloente_2);
                Body_2.AddCell(info_trnasporte_2);

                #endregion

                #region  Unidades

                PdfPTable unidades_titulo = new PdfPTable(new float[] { 0.5f, 0.1f, 0.1f, 0.1f, 0.1f, 0.1f, });
                unidades_titulo.WidthPercentage = 100;

                PdfPCell descripcion = new PdfPCell(new Phrase("DESCRIPCIÓN", fontCustom)) { HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_CENTER, MinimumHeight = 15, };
                PdfPCell piezas = new PdfPCell(new Phrase("PIEZAS", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_CENTER, MinimumHeight = 15, };
                PdfPCell ramos = new PdfPCell(new Phrase("RAMOS", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_CENTER, MinimumHeight = 15, };
                PdfPCell tallos = new PdfPCell(new Phrase("TALLOS", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_CENTER, MinimumHeight = 15, };
                PdfPCell precio_tallo = new PdfPCell(new Phrase("PRECION TALLO", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_CENTER, MinimumHeight = 15, };
                PdfPCell total = new PdfPCell(new Phrase("TOTAL USS", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_CENTER, MinimumHeight = 15, };

                unidades_titulo.AddCell(descripcion);
                unidades_titulo.AddCell(piezas);
                unidades_titulo.AddCell(ramos);
                unidades_titulo.AddCell(tallos);
                unidades_titulo.AddCell(precio_tallo);
                unidades_titulo.AddCell(total);

                PdfPTable tableTituloUnidades = new PdfPTable(6);
                //Dimenciones.
                float[] DimencionUnidades = new float[6];
                DimencionUnidades[0] = 0.5f;//CÓDIGO
                DimencionUnidades[1] = 0.1f;//DESCRIPCION
                DimencionUnidades[2] = 0.1f;//CANTIDAD
                DimencionUnidades[3] = 0.1f;//DTO
                DimencionUnidades[4] = 0.1f;//PRECIO
                DimencionUnidades[5] = 0.1f;//SUBTOTAL


                PdfPTable tableUnidades = new PdfPTable(6);
                tableUnidades.WidthPercentage = 100;
                tableUnidades.SetWidths(DimencionUnidades);

                foreach (DataRow InvoiceLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
                {
                    if (!AddUnidadesFloresOriente(InvoiceLine, ref tableUnidades, fontCustom, DsInvoiceAR))
                        return false;
                }

                iTextSharp.text.pdf.PdfPCell LineaFinal = new iTextSharp.text.pdf.PdfPCell();
                LineaFinal.Colspan = 6;
                LineaFinal.Padding = 3;
                LineaFinal.Border = 1;
                LineaFinal.BorderColorBottom = BaseColor.WHITE;
                LineaFinal.HorizontalAlignment = Element.ALIGN_CENTER;
                LineaFinal.VerticalAlignment = Element.ALIGN_TOP;
                tableUnidades.AddCell(LineaFinal);


                PdfPTable unidades_titulo_bajo = new PdfPTable(new float[] { 0.5f, 0.1f, 0.1f, 0.1f, 0.1f, 0.1f, });
                unidades_titulo_bajo.WidthPercentage = 100;

                PdfPCell descripcion_bajo = new PdfPCell(new Phrase("$ " + decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubtotal"]).ToString("N2"), fontCustom)) { HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_CENTER, MinimumHeight = 15, };
                PdfPCell piezas_bajo = new PdfPCell(new Phrase("" + decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number05"]).ToString("N0"), fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_CENTER, MinimumHeight = 15, };
                PdfPCell ramos_bajo = new PdfPCell(new Phrase("" + decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number07"]).ToString("N0"), fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_CENTER, MinimumHeight = 15, };
                PdfPCell tallos_bajo = new PdfPCell(new Phrase("" + decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number06"]).ToString("N0"), fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_CENTER, MinimumHeight = 15, };
                PdfPCell precio_tallo_bajo = new PdfPCell(new Phrase("", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_CENTER, MinimumHeight = 15, };
                PdfPCell total_bajo = new PdfPCell(new Phrase("$ " + decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"]).ToString("N2"), fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_CENTER, MinimumHeight = 15, };

                unidades_titulo_bajo.AddCell(descripcion_bajo);
                unidades_titulo_bajo.AddCell(piezas_bajo);
                unidades_titulo_bajo.AddCell(ramos_bajo);
                unidades_titulo_bajo.AddCell(tallos_bajo);
                unidades_titulo_bajo.AddCell(precio_tallo_bajo);
                unidades_titulo_bajo.AddCell(total_bajo);

                #endregion

                #region  Footeer

                PdfPTable espacio1 = new PdfPTable(1);
                espacio1.WidthPercentage = 100;
                PdfPCell salto1 = new PdfPCell(new Phrase("CUFE:" + CUFE, fontCustom)) { Border = 0, MinimumHeight = 100, HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_TOP };
                espacio1.AddCell(salto1);


                PdfPTable Footer = new PdfPTable(1);
                Footer.WidthPercentage = 100;

                PdfPCell leyenda1 = new PdfPCell(new Phrase("Any uqality  must be reported within(3) working days of receipt of the flowewrs. failure yo provisde such notice within this requisite time consultes an inrrevocable  acceplance of said goods.particular detais of the claim shoud be duty documnted and faxed or emailed to farm whih digital picture ir this invoice is not paid on time daid conduct will be considerecd delinquent. A delinquet  Adelinquecy charge of 1.5%  per month (18% per anum ) or the maximum allowed is greater, will added on the amount due, plus attomey fees incorred if debt collection are necessary. this invoice should be pad in null and will expire 30 days affer invoice original date.    ", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, MinimumHeight = 15, };
                Footer.AddCell(leyenda1);


                PdfPTable Footer_2 = new PdfPTable(1);
                Footer_2.WidthPercentage = 100;

                PdfPCell leyenda12 = new PdfPCell(new Phrase((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Character01"], fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, MinimumHeight = 15, };
                Footer_2.AddCell(leyenda12);

                PdfPTable Footer_3 = new PdfPTable(1);
                Footer_3.WidthPercentage = 100;

                PdfPCell leyenda13 = new PdfPCell(new Phrase("Observaciones: " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceComment"], fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, MinimumHeight = 15, };
                Footer_3.AddCell(leyenda13);

                #endregion

                #region Exit

                document.Add(header);
                document.Add(espacio);
                document.Add(Body);
                document.Add(Body_2);
                document.Add(espacio);
                document.Add(unidades_titulo);
                document.Add(tableTituloUnidades);
                document.Add(tableUnidades);
                document.Add(unidades_titulo_bajo);
                document.Add(espacio1);
                document.Add(espacio);
                document.Add(Footer);
                document.Add(Footer_2);
                document.Add(Footer_3);

                //document.Open();

                #endregion

                //document.Add(divTextBancoCheque);
                /*PIE DE PAGINA*/
                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();
                RutaPdf = NomArchivo;
                return true;
            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.Message;
                return false;
            }
        }

    }
}
 