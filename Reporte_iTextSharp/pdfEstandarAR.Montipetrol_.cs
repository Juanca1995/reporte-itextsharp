﻿namespace RulesServicesDIAN2.Adquiriente
{
    using iTextSharp.text;
    using iTextSharp.text.pdf;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Diagnostics;
    using System.Drawing;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.NetworkInformation;
    using System.Text;
    using System.Threading.Tasks;
    using System.Web;
    using System.Windows.Forms;

    public partial class pdfEstandarAR
    {
        #region Formatos Montinpetrol

        public bool FacturaMontinpetrolMontinpetrol2(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {
            NomArchivo = string.Empty;
            string Ruta = HttpContext.Current.Server.MapPath("~/PDF/" + NIT + "/");
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            string RutaImg = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";

            NomArchivo = NombreInvoice + ".pdf";

            try
            {
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.
                Document document = new Document(iTextSharp.text.PageSize.LETTER);
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;

                // step 3: we open the document
                document.Open();

                /*TABLA FACTURA*/
                iTextSharp.text.pdf.PdfPTable tableInvoice = new iTextSharp.text.pdf.PdfPTable(5);
                tableInvoice.HorizontalAlignment = Element.ALIGN_CENTER;

                //Dimenciones.
                float[] DimencionInvoice = new float[5];
                DimencionInvoice[0] = 2.0F;//CODIGO
                DimencionInvoice[1] = 5.0F;//Descripcion
                DimencionInvoice[2] = 2.0F;//CANTIDAD
                DimencionInvoice[3] = 3.0F;//Precio Unitario               
                //DimencionInvoice[4] = 1.0F;//Iva %
                //DimencionInvoice[5] = 2.0F;//Dcto %
                DimencionInvoice[4] = 4.0F;//VR TOTAL                                

                tableInvoice.WidthPercentage = 100;
                tableInvoice.SetWidths(DimencionInvoice);

                #region Encabezado
                PdfPTable tableEncabezado = new PdfPTable(new float[] { 1.6f, 0.1f, 2.5f, 0.1f, 1.6f })
                {
                    WidthPercentage = 100,
                };

                //Celda Logo
                System.Drawing.Image Logo;
                //System.Drawing.Image LogoBanner = null;

                var request = WebRequest.Create(RutaImg);

                using (var response = request.GetResponse())
                using (var stream = response.GetResponseStream())
                {
                    Logo = Bitmap.FromStream(stream);
                }

                iTextSharp.text.Image LogoPdf = iTextSharp.text.Image.GetInstance(Logo, BaseColor.WHITE);
                LogoPdf.ScaleAbsolute(150f, 100f);

                iTextSharp.text.pdf.PdfPCell celLogoImg = new iTextSharp.text.pdf.PdfPCell();
                celLogoImg.AddElement(LogoPdf);
                //celLogoImg.AddElement(prgLogo);
                celLogoImg.Colspan = 1;
                celLogoImg.Padding = 3;
                celLogoImg.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celLogoImg.VerticalAlignment = iTextSharp.text.Element.ALIGN_BOTTOM;
                celLogoImg.Border = 0;
                //celLogoImg.BorderColorRight = BaseColor.LIGHT_GRAY;
                tableEncabezado.AddCell(celLogoImg);

                string strTitulo1 = string.Empty;

                iTextSharp.text.Font fontTitle1 = FontFactory.GetFont(FontFactory.HELVETICA, 5, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.pdf.PdfPCell celTittle1 = new iTextSharp.text.pdf.PdfPCell(new Phrase(strTitulo1, fontTitle1));
                celTittle1.Colspan = 1;
                celTittle1.Padding = 3;
                celTittle1.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celTittle1.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                celTittle1.Border = 0;
                tableEncabezado.AddCell(celTittle1);

                iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 10, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitleSubtittle = FontFactory.GetFont(FontFactory.HELVETICA, 5, iTextSharp.text.Font.NORMAL);
                string companyInfo = " Direccion: "+ DsInvoiceAR.Tables["Company"].Rows[0]["Address1"] + "\n"+ "Telefono: " + DsInvoiceAR.Tables["Company"].Rows[0]["PhoneNum"] + "\n"+ "Ciudad: " + DsInvoiceAR.Tables["Company"].Rows[0]["City"];
                string companyResolution = " Resolución"+ (string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Character09"];
                Paragraph prgTitle = new Paragraph(DsInvoiceAR.Tables["Company"].Rows[0]["Name"].ToString(), fontTitle);
                prgTitle.Alignment = Element.ALIGN_CENTER;
                Paragraph prgTitle2 = new Paragraph("REALIZADA POR " +
                    DsInvoiceAR.Tables["Company"].Rows[0]["Name"] +
                    " NIT: " + DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"] + "-" +
                    CalcularDigitoVerificacion((string)DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"]), fontTitleSubtittle);
                prgTitle2.Alignment = Element.ALIGN_CENTER;
                Paragraph prgTitle3 = new Paragraph(companyInfo, fontTitleSubtittle) { Alignment = Element.ALIGN_CENTER };
                Paragraph prgTitle4 = new Paragraph(companyResolution, fontTitleSubtittle) { Alignment = Element.ALIGN_CENTER };


                iTextSharp.text.pdf.PdfPCell cellTitle = new iTextSharp.text.pdf.PdfPCell();
                cellTitle.AddElement(prgTitle);
                cellTitle.AddElement(prgTitle2);
                cellTitle.AddElement(prgTitle3);
                cellTitle.AddElement(prgTitle4);
                cellTitle.Padding = 3;
                cellTitle.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                cellTitle.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                cellTitle.Border = 0;
                cellTitle.BorderColorRight = BaseColor.WHITE;
                tableEncabezado.AddCell(cellTitle);

                string strTitulo4 = string.Empty;


                string TipoFactura = string.Empty;
                if (InvoiceType == "InvoiceType")
                {
                    TipoFactura = $"FACTURA DE VENTA N°\n\n" +
                                         "" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"].ToString();
                }
                else if (InvoiceType == "CreditNoteType" || InvoiceType == "DebitNoteType")
                {
                    TipoFactura = $"NOTA CREDITO N°\n\n" +
                                         "" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"].ToString();
                }

                iTextSharp.text.pdf.PdfPCell celTittle4 = new iTextSharp.text.pdf.PdfPCell(new Phrase(strTitulo4, fontTitle1));
                celTittle4.Colspan = 1;
                celTittle4.Padding = 3;
                celTittle4.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celTittle4.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                celTittle4.Border = 0;
                tableEncabezado.AddCell(celTittle4);

                iTextSharp.text.Font fontLegalNum = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.pdf.PdfPCell celTittle2 = new iTextSharp.text.pdf.PdfPCell(new Phrase(TipoFactura, fontLegalNum));
                celTittle2.Colspan = 1;
                celTittle2.Padding = 3;
                celTittle2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celTittle2.VerticalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celTittle2.Border = 0;
                tableEncabezado.AddCell(celTittle2);

                iTextSharp.text.Font fontLogo = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.BOLD);

                string Nit = $"NIT: {(string)DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"]}-" + $"{CalcularDigitoVerificacion((string)DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"])}";
                iTextSharp.text.pdf.PdfPCell celLegalNum = new iTextSharp.text.pdf.PdfPCell(new Phrase(Nit, fontLegalNum));
                celLegalNum.Colspan = 5;
                celLegalNum.Padding = 3;
                celLegalNum.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                celLegalNum.VerticalAlignment = iTextSharp.text.Element.ALIGN_BOTTOM;
                celLegalNum.Border = 0;
                tableEncabezado.AddCell(celLegalNum);

                //Celda Regimen Comun
                string Reg = "IVA RÉGIMEN COMÚN - NO SOMOS AUTORETENEDORES - SOMOS RETENEDORES DE ICA - ACTIVIDAD ECONOMICA ICA 7110 TARIFA 6,9 x 1000";
                iTextSharp.text.Font fontRegComun = FontFactory.GetFont(FontFactory.HELVETICA, 5, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.pdf.PdfPCell celRegComun = new iTextSharp.text.pdf.PdfPCell(new Phrase(Reg, fontRegComun));
                celRegComun.Colspan = 5;
                celRegComun.Padding = 3;
                celRegComun.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                celRegComun.VerticalAlignment = iTextSharp.text.Element.ALIGN_BOTTOM;
                celRegComun.Border = PdfPCell.NO_BORDER;
                tableEncabezado.AddCell(celRegComun);
                #endregion

                #region InfoCliente_InfoFactura

                PdfPTable tableContInfoCliente_Factura = new PdfPTable(new float[] { 1.8f, 2, 0.8f })
                { WidthPercentage = 100, };

                /*VENDIDO A*/
                iTextSharp.text.Font fontVendidoA = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 6, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontVendidoASubTittle = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);

                Paragraph prgVendidoA = new Paragraph("SEÑORES", fontVendidoA);
                prgVendidoA.Alignment = Element.ALIGN_LEFT;

                string VendidoA = "CLiente"+ DsInvoiceAR.Tables["Customer"].Rows[0]["Name"].ToString() + "\n" +
                                  "Direccion" + DsInvoiceAR.Tables["Customer"].Rows[0]["Address1"].ToString() + "\n" +
                                  "Ciudad" + DsInvoiceAR.Tables["Customer"].Rows[0]["City"].ToString() + "\n" +
                                  "Pais" + DsInvoiceAR.Tables["Customer"].Rows[0]["State"].ToString() + "\n\n" +
                                  "NIT/CEDULA   " + DsInvoiceAR.Tables["Customer"].Rows[0]["ResaleID"].ToString();

                Paragraph prgVendidoASubTittle = new Paragraph(VendidoA, fontVendidoASubTittle);
                prgVendidoASubTittle.Alignment = Element.ALIGN_LEFT;

                iTextSharp.text.pdf.PdfPCell cellVendidoA = new iTextSharp.text.pdf.PdfPCell();
                cellVendidoA.AddElement(prgVendidoA);
                cellVendidoA.AddElement(prgVendidoASubTittle);
                cellVendidoA.Padding = 3;
                cellVendidoA.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellVendidoA.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableContInfoCliente_Factura.AddCell(cellVendidoA);

                /*INFO PEDIDO*/
                iTextSharp.text.Font fontInfoPedido = FontFactory.GetFont(FontFactory.HELVETICA, 5, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontDatosPedido = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                {
                    iTextSharp.text.pdf.PdfPTable tableInfoPedido = new iTextSharp.text.pdf.PdfPTable(5);
                    tableInfoPedido.HorizontalAlignment = Element.ALIGN_CENTER;
                    tableInfoPedido.WidthPercentage = 100;

                    //1-1 Confirma Pedido.
                    iTextSharp.text.pdf.PdfPCell cellConfirmaPedido = new iTextSharp.text.pdf.PdfPCell(new Phrase("CONFIRMACION PEDIDO\n\n\n\n", fontInfoPedido));
                    cellConfirmaPedido.Colspan = 1;
                    cellConfirmaPedido.Padding = 3;
                    cellConfirmaPedido.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                    cellConfirmaPedido.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    tableInfoPedido.AddCell(cellConfirmaPedido);

                    //1-2 No Pedido
                    Phrase text_Pedido = new Phrase("No. PEDIDO\n", fontInfoPedido);
                    Phrase val_Pedido = new Phrase(DsInvoiceAR.Tables["InvcHead"].Rows[0]["OrderNum"].ToString(), fontDatosPedido);
                    PdfPCell cell_NoPedido = new PdfPCell();
                    PdfPCell cellNoPedido = new PdfPCell();
                    cellNoPedido.Colspan = 1;
                    cellNoPedido.Padding = 3;
                    cellNoPedido.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                    cellNoPedido.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    cellNoPedido.AddElement(text_Pedido);
                    cellNoPedido.AddElement(val_Pedido);
                    tableInfoPedido.AddCell(cellNoPedido);

                    //1-3 Orden de Compra                
                    Phrase text_OrdenCompra = new Phrase("ORDEN DE COMPRA\n", fontInfoPedido);
                    Phrase val_OrdenCompra = new Phrase("", fontDatosPedido);
                    PdfPCell cellOrdenCompra = new PdfPCell();
                    cellOrdenCompra.Colspan = 1;
                    cellOrdenCompra.Padding = 3;
                    cellOrdenCompra.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                    cellOrdenCompra.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    cellOrdenCompra.AddElement(text_OrdenCompra);
                    cellOrdenCompra.AddElement(val_OrdenCompra);
                    tableInfoPedido.AddCell(cellOrdenCompra);

                    //1-4 Fecha Vencimiento
                    Phrase text_DueDate = new Phrase("FECHA VENCIMIENTO:\n", fontInfoPedido);
                    Phrase val_DueDate = new Phrase(Convert.ToDateTime(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"]).ToString("dd/MM/yyyy"), fontDatosPedido);
                    PdfPCell cellDueDate = new PdfPCell();
                    cellDueDate.Colspan = 1;
                    cellDueDate.Padding = 3;
                    cellDueDate.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                    cellDueDate.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    cellDueDate.AddElement(text_DueDate);
                    cellDueDate.AddElement(val_DueDate);
                    tableInfoPedido.AddCell(cellDueDate);

                    //1-5 Fecha Factura
                    Phrase text_InvoiceDate = new Phrase("FECHA FACTURA:\n", fontInfoPedido);
                    Phrase val_InvoiceDate = new Phrase(Convert.ToDateTime(DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).ToString("dd/MM/yyyy"), fontDatosPedido);
                    PdfPCell cellInvoiceDate = new PdfPCell();
                    cellInvoiceDate.Colspan = 1;
                    cellInvoiceDate.Padding = 3;
                    cellInvoiceDate.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                    cellInvoiceDate.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    cellInvoiceDate.AddElement(text_InvoiceDate);
                    cellInvoiceDate.AddElement(val_InvoiceDate);
                    tableInfoPedido.AddCell(cellInvoiceDate);

                    //2-1 Telefono
                    Phrase text_Telefono = new Phrase("TELÉFONO\n", fontInfoPedido);
                    Phrase val_Telefono = new Phrase(DsInvoiceAR.Tables["Customer"].Rows[0]["PhoneNum"].ToString(), fontDatosPedido);
                    PdfPCell cellTelefono = new PdfPCell();
                    cellTelefono.Colspan = 1;
                    cellTelefono.Padding = 3; cellTelefono.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                    cellTelefono.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    cellTelefono.AddElement(text_Telefono);
                    cellTelefono.AddElement(val_Telefono);
                    tableInfoPedido.AddCell(cellTelefono);

                    //2-2 InvoiceNum
                    Phrase text_NoFac = new Phrase("Nro. Fact. REF\n", fontInfoPedido);
                    Phrase val_NoFac = new Phrase(DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceNum"].ToString(), fontDatosPedido);
                    PdfPCell cellNoFacRef = new PdfPCell();
                    cellNoFacRef.Colspan = 1;
                    cellNoFacRef.Padding = 3;
                    cellNoFacRef.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                    cellNoFacRef.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    cellNoFacRef.AddElement(text_NoFac);
                    cellNoFacRef.AddElement(val_NoFac);
                    tableInfoPedido.AddCell(cellNoFacRef);

                    //2-3 PLAZO MESES
                    Phrase text_TermsCode = new Phrase("PLAZO MESES\n", fontInfoPedido);
                    Phrase val_TermsCode = new Phrase(DsInvoiceAR.Tables["InvcHead"].Rows[0]["TermsCodeDescription"].ToString(), fontDatosPedido);
                    PdfPCell cellTermsCode = new PdfPCell(
                        new Phrase("PLAZO MESES\n"
                        /*+ DsInvoiceAR.Tables["InvcHead"].Rows[0]["TermsCodeDescription"].ToString()*/, fontInfoPedido));
                    cellTermsCode.Colspan = 1;
                    cellTermsCode.Padding = 3;
                    cellTermsCode.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                    cellTermsCode.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    cellTermsCode.AddElement(text_TermsCode);
                    cellTermsCode.AddElement(val_TermsCode);
                    tableInfoPedido.AddCell(cellTermsCode);

                    //2-4 VENDEDOR
                    Phrase text_Vendedor = new Phrase("VENDEDOR\n", fontInfoPedido);
                    Phrase val_Vendedor = new Phrase(DsInvoiceAR.Tables["InvcHead"].Rows[0]["SalesRepName1"].ToString(), fontDatosPedido);
                    PdfPCell cellVendedor = new PdfPCell();
                    cellVendedor.Colspan = 1;
                    cellVendedor.Padding = 3;
                    cellVendedor.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                    cellVendedor.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    cellVendedor.AddElement(text_Vendedor);
                    cellVendedor.AddElement(val_Vendedor);
                    tableInfoPedido.AddCell(cellVendedor);

                    //2-4 FECHA ENTREGA
                    iTextSharp.text.pdf.PdfPCell cellFechaEntrega = new iTextSharp.text.pdf.PdfPCell(new Phrase(" ", fontInfoPedido));
                    cellFechaEntrega.Colspan = 1;
                    cellFechaEntrega.Padding = 3;
                    cellFechaEntrega.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                    cellFechaEntrega.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    tableInfoPedido.AddCell(cellFechaEntrega);

                    tableContInfoCliente_Factura.AddCell(new PdfPCell(tableInfoPedido) { Padding = 3, });
                }

                /*CELL QR*/
                iTextSharp.text.Image QRPdf = iTextSharp.text.Image.GetInstance(QRInvoice, BaseColor.WHITE);
                QRPdf.ScaleAbsolute(80f, 80f);//Se cambia el tamaño de la imagen del QR
                iTextSharp.text.pdf.PdfPCell celImgQR = new iTextSharp.text.pdf.PdfPCell(QRPdf);
                celImgQR.Colspan = 1;
                celImgQR.Padding = 3;
                celImgQR.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celImgQR.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableContInfoCliente_Factura.AddCell(celImgQR);
                #endregion

                #region Unidades

                PdfPTable tableUnidades = new PdfPTable(new float[] { 1, 2.5f, 1, 1.3f, 2 })
                {
                    WidthPercentage = 100,
                };

                tableUnidades.AddCell(new PdfPCell(new Phrase($"CUFE: {CUFE}", fontVendidoASubTittle))
                { Border = PdfPCell.NO_BORDER, Colspan = 2, });

                tableUnidades.AddCell(new PdfPCell(new Phrase($"Representación gráfica factura electrónica", fontVendidoASubTittle))
                { Border = PdfPCell.NO_BORDER, Colspan = 3, HorizontalAlignment = Element.ALIGN_RIGHT });

                //Configuramos las columnas.
                if (!AddColumnsInvoiceLineMontinpetrol(ref tableUnidades))
                    return false;

                /*Tabla - ITEMS FACTURA.*/
                //Filas Factura
                iTextSharp.text.Font fuenteDatos = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);

                decimal TotalDescountInvc = 0;
                decimal TotalMissCharges = 0;
                string SalesOrder = string.Empty;
                string Embarques = string.Empty;
                string PartDescLoteAndSerie = string.Empty;

                //Lineas de Factura.
                foreach (DataRow InvoiceLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
                {
                    //Totales de las Lineas
                    TotalDescountInvc += Convert.ToDecimal(InvoiceLine["DspDocLessDiscount"]);
                    //TotalMissCharges += Convert.ToDecimal(InvoiceLine["DspDocTotalMiscChrg"]);                                       

                    if (!AddRowInvoiceMontinpetrol(InvoiceLine, ref tableUnidades, fuenteDatos, DsInvoiceAR.Tables["InvcTax"]))
                        return false;

                    //if (!AddInfoByLoteAndSerie(DsInvoiceAR, Convert.ToInt32(InvoiceLine["InvoiceLine"]), ref PartDescLoteAndSerie))
                    //    return false;

                    //Enviar a: ShitoAddres.                
                }

                iTextSharp.text.pdf.PdfPCell cellShipToAddres = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    " ", fuenteDatos));
                cellShipToAddres.Colspan = 7;
                cellShipToAddres.Padding = 3;
                cellShipToAddres.BorderWidthLeft = 0;
                cellShipToAddres.BorderWidthRight = 0;
                cellShipToAddres.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellShipToAddres.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableInvoice.AddCell(cellShipToAddres);

                PdfPTable valor = new PdfPTable(new float[] { 2,2,2,2,2 });
                valor.WidthPercentage = 100;
                PdfPCell cell_cubre = new PdfPCell() { Border=0,Colspan=4};
                valor.AddCell(cell_cubre);
                PdfPCell cell_valor = new PdfPCell(new Phrase("" + decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"]).ToString("N0"), fuenteDatos)) { HorizontalAlignment=Element.ALIGN_CENTER,VerticalAlignment=Element.ALIGN_BOTTOM,};
                valor.AddCell(cell_valor);

                #endregion

                #region PiePag  
                PdfPTable tablePiePag = new PdfPTable(new float[] { 4, 2.5f }) { WidthPercentage = 100, };

                tablePiePag.AddCell(new PdfPCell()
                {
                    Border = PdfPCell.NO_BORDER,
                    Colspan = 2,
                    FixedHeight = 10,
                });

                iTextSharp.text.Font fuenteObs = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 6, iTextSharp.text.Font.NORMAL);
                Phrase text_Obs = new Phrase("OBSERVACIONES: ", fuenteObs);
                Phrase val_Obs = new Phrase(DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceComment"].ToString(), fuenteObs);
                Phrase text_Espacio = new Phrase("\n ", fuenteObs);
                iTextSharp.text.pdf.PdfPCell cellObs = new iTextSharp.text.pdf.PdfPCell();
                cellObs.Padding = 3;
                cellObs.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellObs.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                cellObs.AddElement(text_Obs);
                cellObs.AddElement(val_Obs);
                cellObs.AddElement(text_Espacio);
                tablePiePag.AddCell(cellObs);

                //Info Resolucion.
                //string Resolucion = "HAB 18762006023112 FECHA2017-12-07 VTO 2019-12-07 \n" +
                //                    "DEL CTBQ 4.450-6.000 POR COMPUTADOR\n";

                //string Resolucion = DsInvoiceAR.Tables["Encabezado"].Rows[0]["Prefijo1"].ToString() + "\n" +
                //                    DsInvoiceAR.Tables["Encabezado"].Rows[0]["Prefijo2"].ToString();
                string Resolucion = string.Empty;

                iTextSharp.text.pdf.PdfPCell cellResolucion = new iTextSharp.text.pdf.PdfPCell();
                cellResolucion.Colspan = 3;
                cellResolucion.Padding = 3;
                cellResolucion.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellResolucion.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tablePiePag.AddCell(cellResolucion);

                iTextSharp.text.Font fuenteObs2 = FontFactory.GetFont(FontFactory.HELVETICA, 5, iTextSharp.text.Font.NORMAL);
                //Observacion 3 Factura.                
                iTextSharp.text.pdf.PdfPCell cellObs3 = new iTextSharp.text.pdf.PdfPCell(
                    new Phrase("Somos grandes contribuyentes según la resolución Nº 000076 del 01/12/2016.", fuenteObs2));
                cellObs3.Padding = 3;
                cellObs3.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellObs3.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tablePiePag.AddCell(cellObs3);

                PdfPTable tableSubtotal = new PdfPTable(2) { WidthPercentage = 100, };

                /*TOTAL Factura.*/
                iTextSharp.text.Font fontTotalesTittle = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 6, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTotales = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);

                tableSubtotal.AddCell(new PdfPCell(new Phrase("SUBTOTAL", fontTotalesTittle))
                {
                    Border = PdfPCell.RIGHT_BORDER | PdfPCell.LEFT_BORDER,
                    HorizontalAlignment = iTextSharp.text.Element.ALIGN_RIGHT,
                    VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP,
                });

                //1-2 SUBTOTAL VALUE
                decimal SubTotal = Convert.ToDecimal(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubTotal"].ToString());

                tableSubtotal.AddCell(new PdfPCell(new Phrase(SubTotal.ToString("C2"), fontTotales))
                {
                    Border = PdfPCell.LEFT_BORDER | PdfPCell.RIGHT_BORDER,
                    HorizontalAlignment = iTextSharp.text.Element.ALIGN_RIGHT,
                    VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP,
                });

                tablePiePag.AddCell(new PdfPCell(tableSubtotal) { Border = PdfPCell.NO_BORDER, });

                PdfPTable tableEmisor_Firmas = new PdfPTable(3);

                /*INFORMACION ENTREGA.*/
                iTextSharp.text.Font fontInfoEntrega = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);

                //Emisor Factura.
                Paragraph prgEmisor = new Paragraph("EMISOR DE FACTURA", fontInfoEntrega);
                prgEmisor.Alignment = Element.ALIGN_LEFT;

                iTextSharp.text.Image LogoPdf2 = iTextSharp.text.Image.GetInstance(Logo, BaseColor.WHITE);
                LogoPdf2.ScaleAbsolute(60f, 60f);

                iTextSharp.text.pdf.PdfPCell cellEmisor = new iTextSharp.text.pdf.PdfPCell();
                cellEmisor.AddElement(prgEmisor);
                cellEmisor.AddElement(LogoPdf2);
                cellEmisor.AddElement(new Phrase((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Character09"],
                    fontInfoEntrega));
                cellEmisor.Padding = 3;
                cellEmisor.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellEmisor.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableEmisor_Firmas.AddCell(cellEmisor);

                //Leyenda de resoluciones
                tableEmisor_Firmas.AddCell(new PdfPCell(new Phrase((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Character03"],
                    fontInfoEntrega))
                { });


                //RECIBO DE MERCANCIA.
                string ReciboMercancia = "RECIBO DE MERCANCÍA\n\n" +
                                         "FIRMA\n\n" +
                                         "_________________________\n\n" +
                                         "NOMBRE\n\n" +
                                         "_________________________\n\n" +
                                         "FECHA dd/mm/aaaa\n\n" +
                                         "_________________________\n\n" +
                                         "CC";
                tableEmisor_Firmas.AddCell(new PdfPCell(new Phrase(ReciboMercancia, fontInfoEntrega)) { });

                tablePiePag.AddCell(new PdfPCell(tableEmisor_Firmas)
                {
                    //Border =PdfPCell.NO_BORDER,
                    Padding = 2,
                });

                //----------------------Totales-------------------------------------
                PdfPTable tableTotales = new PdfPTable(2) { WidthPercentage = 100, };

                tableTotales.AddCell(new PdfPCell(new Phrase("DESCUENTO", fontTotalesTittle))
                {
                    Border = PdfPCell.RIGHT_BORDER,
                    HorizontalAlignment = Element.ALIGN_RIGHT,
                    VerticalAlignment = Element.ALIGN_TOP,
                });

                tableTotales.AddCell(new PdfPCell(new Phrase(TotalDescountInvc.ToString("C2"), fontTotales))
                {
                    Border = PdfPCell.LEFT_BORDER,
                    HorizontalAlignment = Element.ALIGN_RIGHT,
                    VerticalAlignment = Element.ALIGN_TOP,
                });

                tableTotales.AddCell(new PdfPCell(new Phrase("IVA", fontTotalesTittle))
                {
                    Border = PdfPCell.RIGHT_BORDER,
                    HorizontalAlignment = Element.ALIGN_RIGHT,
                    VerticalAlignment = Element.ALIGN_TOP,
                });

                //4-2 IVA VALUE                                
                decimal Iva = Convert.ToDecimal(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DocTaxAmt"].ToString());
                tableTotales.AddCell(new PdfPCell(new Phrase(Iva.ToString("C2"), fontTotales))
                {
                    Border = PdfPCell.LEFT_BORDER,
                    HorizontalAlignment = Element.ALIGN_RIGHT,
                    VerticalAlignment = Element.ALIGN_TOP,
                });

                tableTotales.AddCell(new PdfPCell(new Phrase("RETE FUENTE", fontTotalesTittle))
                {
                    Border = PdfPCell.RIGHT_BORDER,
                    HorizontalAlignment = Element.ALIGN_RIGHT,
                    VerticalAlignment = Element.ALIGN_TOP,
                });

                //4-2 IVA VALUE                                
                decimal reteFuente = GetValImpuestoByID("0A", DsInvoiceAR);
                tableTotales.AddCell(new PdfPCell(new Phrase(reteFuente.ToString("C2"), fontTotales))
                {
                    Border = PdfPCell.LEFT_BORDER,
                    HorizontalAlignment = Element.ALIGN_RIGHT,
                    VerticalAlignment = Element.ALIGN_TOP,
                });

                PdfPTable tableTotalesCop = new PdfPTable(new float[] { 0.7f, 0.3f, 1 })
                {
                    WidthPercentage = 100,
                };

                tableTotalesCop.AddCell(new PdfPCell(new Phrase("TOTAL",
                    FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 8, iTextSharp.text.Font.NORMAL, BaseColor.WHITE)))
                { BackgroundColor = BaseColor.BLACK, Border = PdfPCell.TOP_BORDER, });

                tableTotalesCop.AddCell(new PdfPCell(new Phrase(
                    (String)DsInvoiceAR.Tables["InvcHead"].Rows[0]["CurrencyCode"], fontTotalesTittle))
                {
                    Border = PdfPCell.TOP_BORDER,
                    HorizontalAlignment = Element.ALIGN_RIGHT,
                    VerticalAlignment = Element.ALIGN_TOP,
                });

                decimal Total = Convert.ToDecimal(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"].ToString());
                tableTotalesCop.AddCell(new PdfPCell(new Phrase(Total.ToString("C2"), fontTotales))
                {
                    Border = PdfPCell.TOP_BORDER,
                    HorizontalAlignment = Element.ALIGN_RIGHT,
                    VerticalAlignment = Element.ALIGN_TOP,
                });


                tableTotales.AddCell(new PdfPCell(tableTotalesCop)
                { Border = PdfPCell.NO_BORDER, Colspan = 2, });

                tableTotales.AddCell(new PdfPCell()
                {
                    Border = PdfPCell.NO_BORDER,
                    Colspan = 2,
                });

                tablePiePag.AddCell(new PdfPCell(tableTotales)
                {
                    Border = PdfPCell.RIGHT_BORDER | PdfPCell.LEFT_BORDER | PdfPCell.BOTTOM_BORDER,
                    HorizontalAlignment = Element.ALIGN_RIGHT,
                    VerticalAlignment = Element.ALIGN_TOP,
                    PaddingLeft = 2,
                    PaddingRight = 2,
                });


                tablePiePag.AddCell(new PdfPCell(new Phrase("ESTA FACTURA DE VENTA ES UN TITULO " +
                    "VALOR SEGUN LEY 1231 DE JULIO 17 DE 2008. DECLARAMOS HABER RECIBIDO DE CONFORMIDAD" +
                    " REAL Y MATERIALMENTE LAS MERCANCIAS OBJETO DE LA PRESENTE.", fuenteObs2))
                {
                    Colspan = 2,
                    Border = PdfPCell.NO_BORDER,
                });

                #endregion

                document.Add(tableEncabezado);
                document.Add(tableContInfoCliente_Factura);
                document.Add(tableUnidades);
                document.Add(valor);
                document.Add(tablePiePag);
                //document.Add(tableSucursales);

                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();
                RutaPdf = NomArchivo;
                return true;
            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.Message;
                return false;
            }
        }

        private bool AddColumnsInvoiceLineMontinpetrol(ref iTextSharp.text.pdf.PdfPTable table)
        {
            try
            {
                iTextSharp.text.Font fuenteEncabezado = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 6, iTextSharp.text.Font.NORMAL);

                iTextSharp.text.pdf.PdfPCell celda1 = new iTextSharp.text.pdf.PdfPCell(new Phrase("CÓDIGO", fuenteEncabezado));
                celda1.Padding = 3;
                celda1.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celda1.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                table.AddCell(celda1);

                iTextSharp.text.pdf.PdfPCell celda2 = new iTextSharp.text.pdf.PdfPCell(new Phrase("DESCRIPCIÓN", fuenteEncabezado));
                celda2.Padding = 3;
                celda2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celda2.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                table.AddCell(celda2);

                iTextSharp.text.pdf.PdfPCell celda3 = new iTextSharp.text.pdf.PdfPCell(new Phrase("CANTIDAD", fuenteEncabezado));
                celda3.Padding = 3;
                celda3.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celda3.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                table.AddCell(celda3);

                iTextSharp.text.pdf.PdfPCell celda5 = new iTextSharp.text.pdf.PdfPCell(new Phrase("VR. UNITARIO", fuenteEncabezado));
                celda5.Padding = 3;
                celda5.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celda5.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                table.AddCell(celda5);

                iTextSharp.text.pdf.PdfPCell celda6 = new iTextSharp.text.pdf.PdfPCell(new Phrase("VALOR TOTAL", fuenteEncabezado));
                celda6.Padding = 3;
                celda6.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celda6.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                table.AddCell(celda6);

                return true;
            }
            catch (Exception ex)
            {
                strError = "Error addLineInvoice: " + ex.Message;
                return false;
            }
        }

        private bool AddRowInvoiceMontinpetrol(DataRow InvoiceLine, ref iTextSharp.text.pdf.PdfPTable table, iTextSharp.text.Font fuenteDatos, DataTable DtInvcTax)
        {
            try
            {
                //CODIGO
                iTextSharp.text.pdf.PdfPCell celCodigo = new iTextSharp.text.pdf.PdfPCell(new Phrase(InvoiceLine["PartNum"].ToString(), fuenteDatos));
                celCodigo.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celCodigo.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                table.AddCell(celCodigo);

                //DESCRIPCION
                iTextSharp.text.pdf.PdfPCell celDesc = new iTextSharp.text.pdf.PdfPCell(new Phrase(InvoiceLine["LineDesc"].ToString(), fuenteDatos));
                celDesc.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                celDesc.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                table.AddCell(celDesc);

                //CANTIDAD
                decimal Qty = Convert.ToDecimal(InvoiceLine["SellingShipQty"].ToString());
                iTextSharp.text.pdf.PdfPCell celCant = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:N0}", Qty), fuenteDatos));
                celCant.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celCant.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                table.AddCell(celCant);

                //PRECIO
                decimal UnitPrice = Convert.ToDecimal(InvoiceLine["DocUnitPrice"].ToString());
                iTextSharp.text.pdf.PdfPCell cellUnitPrice = new iTextSharp.text.pdf.PdfPCell(new Phrase("$" + string.Format("{0:N2}", UnitPrice), fuenteDatos));
                cellUnitPrice.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                cellUnitPrice.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                table.AddCell(cellUnitPrice);

                //EXT PRICE
                decimal PriceLine = Convert.ToDecimal(InvoiceLine["DocExtPrice"].ToString());
                iTextSharp.text.pdf.PdfPCell celExtPrise = new iTextSharp.text.pdf.PdfPCell(new Phrase("$" + string.Format("{0:N2}", PriceLine), fuenteDatos));
                celExtPrise.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celExtPrise.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                table.AddCell(celExtPrise);

                return true;
            }
            catch (Exception ex)
            {
                strError = "Error addLineInvoice: " + ex.Message;
                return false;
            }
        }

        #endregion
    }
}
