﻿namespace RulesServicesDIAN2.Adquiriente
{
    using iTextSharp.text;
    using iTextSharp.text.pdf;
    using System;
    //Referenciar y usar.
    using System.Data;
    using System.Drawing;
    using System.IO;
    using System.Net;
    using System.Net.NetworkInformation;
    using System.Net.Sockets;
    using System.Web;
    using System.Windows.Forms;
    using BarcodeLib;
    using System.Linq;

    public partial class pdfEstandarAR
    {
        public partial class Helpers
        {
            public class Compartido
            {
                public enum TipoDocumento
                {
                    FacturaNacional,
                    FacturaExportacion,
                    NotaCredito,
                    NotaDebito
                }
                public static bool EsLocal = true;
                public static string GetPercentIdImpDIAN(string InvoiceNum, string InvoiceLine, DataTable DtInvcTax, ref string strError)
                {
                    string Percent = string.Empty;

                    try
                    {
                        var rowImpuestos = from row in DtInvcTax.AsEnumerable()
                                           where row["InvoiceNum"].ToString() == InvoiceNum
                                           && row["InvoiceLine"].ToString() == InvoiceLine
                                           select row;
                        //DataRow[] RowsImpuestos = DtInvcTax.Select("InvoiceNum='" + InvoiceNum + "' and InvoiceLine='" + InvoiceLine + "'");

                        //if (RowsImpuestos != null && RowsImpuestos.GetLength(0) > 0)
                        if (rowImpuestos != null && rowImpuestos.Count() > 0)
                        {
                            //foreach (var Tax in RowsImpuestos)
                            foreach (var Tax in rowImpuestos)
                            {
                                Percent = string.Format("{0:N}", Tax["Percent"]);
                                //if (Tax["TaxDescription"].ToString().ToUpper().Contains("IVA"))
                                //{
                                //    Percent = string.Format("{0:N}", Tax["Percent"]);
                                //    break;
                                //}
                            }

                        }

                        return Percent;
                    }
                    catch (Exception ex)
                    {
                        strError = "GetPercentIdImpDIAN Error: " + ex.Message;
                        return string.Empty;
                    }
                }
                public static bool AddInfoByLoteAndSerie(DataSet DsInvoiceAR, int LineNum, ref string PartDescLoteAndSerie)
                {
                    try
                    {
                        //Buscamos Lotes por Linea de factura
                        if (DsInvoiceAR.Tables.Contains("Detalle"))
                        {
                            foreach (DataRow fila in DsInvoiceAR.Tables["Detalle"].Rows)
                            {
                                if (Convert.ToInt32(fila["Linea"]) == LineNum)
                                {
                                    PartDescLoteAndSerie += fila["N_x00BA__x0020_Lote"].ToString() + "\n";
                                }
                            }
                        }

                        //Buscamos Series por Linea de factura
                        if (DsInvoiceAR.Tables.Contains("Seriales_x0020_Facturados"))
                        {
                            foreach (DataRow fila in DsInvoiceAR.Tables["Seriales_x0020_Facturados"].Rows)
                            {
                                if (Convert.ToInt32(fila["Linea"]) == LineNum)
                                {
                                    PartDescLoteAndSerie += "   Nº Serie: " + fila["Serie"].ToString() + "\n";
                                }
                            }
                        }

                        return true;
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                }
                public static string Nroenletras(string num)
                {
                    string res, dec = "";
                    Int64 entero;
                    int decimales;
                    double nro;

                    try
                    {
                        nro = Convert.ToDouble(num);
                    }
                    catch
                    {
                        return "";
                    }

                    entero = Convert.ToInt64(Math.Truncate(nro));
                    decimales = Convert.ToInt32(Math.Round((nro - entero) * 100, 2));
                    if (decimales > 0)
                    {
                        dec = " CON " + decimales.ToString();
                    }
                    //else
                    //{
                    //    dec = " CON 00";
                    //}

                    res = toText(Convert.ToDouble(entero)) + dec + " PESOS";
                    return res;
                }
                public static string toText(double value)
                {
                    string Num2Text = "";
                    value = Math.Truncate(value);
                    if (value == 0) Num2Text = "CERO";
                    else if (value == 1) Num2Text = "UNO";
                    else if (value == 2) Num2Text = "DOS";
                    else if (value == 3) Num2Text = "TRES";
                    else if (value == 4) Num2Text = "CUATRO";
                    else if (value == 5) Num2Text = "CINCO";
                    else if (value == 6) Num2Text = "SEIS";
                    else if (value == 7) Num2Text = "SIETE";
                    else if (value == 8) Num2Text = "OCHO";
                    else if (value == 9) Num2Text = "NUEVE";
                    else if (value == 10) Num2Text = "DIEZ";
                    else if (value == 11) Num2Text = "ONCE";
                    else if (value == 12) Num2Text = "DOCE";
                    else if (value == 13) Num2Text = "TRECE";
                    else if (value == 14) Num2Text = "CATORCE";
                    else if (value == 15) Num2Text = "QUINCE";
                    else if (value < 20) Num2Text = "DIECI" + toText(value - 10);
                    else if (value == 20) Num2Text = "VEINTE";
                    else if (value < 30) Num2Text = "VEINTI" + toText(value - 20);
                    else if (value == 30) Num2Text = "TREINTA";
                    else if (value == 40) Num2Text = "CUARENTA";
                    else if (value == 50) Num2Text = "CINCUENTA";
                    else if (value == 60) Num2Text = "SESENTA";
                    else if (value == 70) Num2Text = "SETENTA";
                    else if (value == 80) Num2Text = "OCHENTA";
                    else if (value == 90) Num2Text = "NOVENTA";
                    else if (value < 100) Num2Text = toText(Math.Truncate(value / 10) * 10) + " Y " + toText(value % 10);
                    else if (value == 100) Num2Text = "CIEN";
                    else if (value < 200) Num2Text = "CIENTO " + toText(value - 100);
                    else if ((value == 200) || (value == 300) || (value == 400) || (value == 600) || (value == 800)) Num2Text = toText(Math.Truncate(value / 100)) + "CIENTOS";
                    else if (value == 500) Num2Text = "QUINIENTOS";
                    else if (value == 700) Num2Text = "SETECIENTOS";
                    else if (value == 900) Num2Text = "NOVECIENTOS";
                    else if (value < 1000) Num2Text = toText(Math.Truncate(value / 100) * 100) + " " + toText(value % 100);
                    else if (value == 1000) Num2Text = "MIL";
                    else if (value < 2000) Num2Text = "MIL " + toText(value % 1000);
                    else if (value < 1000000)
                    {
                        Num2Text = toText(Math.Truncate(value / 1000)) + " MIL";
                        if ((value % 1000) > 0) Num2Text = Num2Text + " " + toText(value % 1000);
                    }

                    else if (value == 1000000) Num2Text = "UN MILLON";
                    else if (value < 2000000) Num2Text = "UN MILLON " + toText(value % 1000000);
                    else if (value < 1000000000000)
                    {
                        Num2Text = toText(Math.Truncate(value / 1000000)) + " MILLONES ";
                        double truncate = Math.Truncate(value / 1000000) * 1000000;
                        if ((value - truncate) > 0) Num2Text = Num2Text + " " + toText(value - truncate);
                    }

                    else if (value == 1000000000000) Num2Text = "UN BILLON";
                    else if (value < 2000000000000) Num2Text = "UN BILLON " + toText(value - Math.Truncate(value / 1000000000000) * 1000000000000);

                    else
                    {
                        Num2Text = toText(Math.Truncate(value / 1000000000000)) + " BILLONES";
                        if ((value - Math.Truncate(value / 1000000000000)) > 0) Num2Text = Num2Text + " " + toText(value - Math.Truncate(value / 1000000000000));
                    }
                    return Num2Text;
                }
                public static decimal GetValImpuestoByID(string IdImPDIAN, DataSet DsInvcHed)
                {
                    decimal ValImp = 0;

                    try
                    {
                        //Trasa += "Valor Impuesto ID" + IdImPDIAN + "\n";
                        //Recorremos los Impuestos y acumulamos por IdImpDIAN.                
                        foreach (DataRow RowTax in DsInvcHed.Tables["InvcTax"].Rows)
                        {
                            //Buscamos el IdImpDIAN por el RateCode del InvcTax.      
                            DataRow[] RowImpDIAN = DsInvcHed.Tables["SalesTRC"].Select("RateCode='" + RowTax["RateCode"].ToString() + "'");
                            if (RowImpDIAN != null && RowImpDIAN.GetLength(0) > 0 && RowImpDIAN[0]["IdImpDIAN_c"].ToString() == IdImPDIAN)
                            {
                                //Trasa += "RateCode =" + RowTax["RateCode"].ToString() + "IdImpDIAN =" + RowImpDIAN[0]["IdImpDIAN_c"].ToString() + " Valor" + Convert.ToDecimal(RowTax["DocTaxAmt"]) + "\n";                        
                                ValImp += Convert.ToDecimal(RowTax["DocTaxAmt"]);
                            }
                        }

                        return ValImp;
                    }
                    catch (Exception)
                    {
                        return 0;
                    }
                }
                public static decimal GetValImpuestoByID(string InvoiceNum, string InvoiceLine, string IdImPDIAN, DataSet DsInvcHed)
                {
                    decimal ValImp = 0;

                    try
                    {
                        //Trasa += "Valor Impuesto ID" + IdImPDIAN + "\n";
                        //Recorremos los Impuestos y acumulamos por IdImpDIAN.                
                        foreach (DataRow RowTax in DsInvcHed.Tables["InvcTax"].Rows)
                        {
                            //Buscamos el IdImpDIAN por el RateCode del InvcTax.      
                            DataRow[] RowImpDIAN = DsInvcHed.Tables["SalesTRC"].Select("RateCode='" + RowTax["RateCode"].ToString() + "'");
                            if (RowImpDIAN != null && RowImpDIAN.GetLength(0) > 0 &&
                                RowImpDIAN[0]["IdImpDIAN_c"].ToString() == IdImPDIAN &&
                                (string)RowTax["InvoiceNum"] == InvoiceNum &&
                                (string)RowTax["InvoiceLine"] == InvoiceLine)
                            {
                                //Trasa += "RateCode =" + RowTax["RateCode"].ToString() + "IdImpDIAN =" + RowImpDIAN[0]["IdImpDIAN_c"].ToString() + " Valor" + Convert.ToDecimal(RowTax["DocTaxAmt"]) + "\n";                        
                                ValImp += Convert.ToDecimal(RowTax["DocTaxAmt"]);
                            }
                        }

                        return ValImp;
                    }
                    catch (Exception)
                    {
                        return 0;
                    }
                }
                public static void BarCode(ref PdfPCell celda, string valor)
                {
                    BarcodeLib.Barcode Codigo = new BarcodeLib.Barcode();
                    Codigo.IncludeLabel = true;
                    var ImgBar = Codigo.Encode(TYPE.CODE128, valor, Color.Black, Color.White,650,150);
                    iTextSharp.text.Image LogoCodBarras = iTextSharp.text.Image.GetInstance(ImgBar, BaseColor.WHITE);
                    celda.AddElement(LogoCodBarras);
                }
                public static void ConvertNumeric(ref string data)
                {
                    int entero = -1;
                    while (!int.TryParse(data, out entero))
                    {
                        data = data.Substring(1);
                        ConvertNumeric(ref data);
                    }
                }
                public static bool VerificarExistenciaColumnas(DataSet ds, string tableName, string ColumnName, int fila)
                {
                    bool result = false;
                    if (ds.Tables.Contains(tableName) 
                        && ds.Tables[tableName].Columns.Contains(ColumnName)
                        && !string.IsNullOrEmpty(ds.Tables[tableName].Rows[fila][ColumnName].ToString()))
                        result = true;
                    return result;
                }
                public static bool VerificarExistenciaColumnas(DataSet ds, string tableName, string ColumnName)
                {
                    bool result = false;
                    if (ds.Tables.Contains(tableName)
                        && ds.Tables[tableName].Columns.Contains(ColumnName))
                        result = true;
                    return result;
                }
                public static bool VerificarExistenciaColumnas(DataRow dr, string ColumnName)
                {
                    bool result = false;
                    if (dr != null && dr.Table.Columns.Contains(ColumnName))
                        result = true;
                    return result;
                }
                public static iTextSharp.text.Image GetLogo(string RutaImg, float Width, float Height)
                {
                    System.Drawing.Image logo = null;
                    var requestLogo = WebRequest.Create(RutaImg);

                    using (var responseLogo = requestLogo.GetResponse())
                    using (var streamLogo = responseLogo.GetResponseStream())
                    {
                        logo = Bitmap.FromStream(streamLogo);
                    }

                    iTextSharp.text.Image ImgLogo = iTextSharp.text.Image.GetInstance(logo, BaseColor.WHITE);
                    ImgLogo.ScaleAbsolute(Width, Height);
                    return ImgLogo;
                }
                public static string ValidarListaString(string[] lista, int valor)
                {
                    string Result = string.Empty;
                    if (lista != null && lista.Count() > 0 && lista.Count() > valor && lista[valor] != null)
                        Result = lista[valor];
                    return Result;
                }
            }
            public class Aldor
            {
                public static bool AddUnidadesALDOR(DataRow dataLine, ref PdfPTable pdfPTable, iTextSharp.text.Font font, DataSet dataSet)
                {
                    string PartNum = string.Empty;
                    string LineDesc = string.Empty;
                    string SellingShipQty = "0";
                    string SalesUM = string.Empty;
                    string UnitPrice = "0";
                    string Number01 = "0";
                    string Number02 = "0";
                    string DocExtPrice = "0";
                    string DspDocLineTax = "0";

                    if (Helpers.Compartido.VerificarExistenciaColumnas(dataLine, "PartNum"))
                        PartNum = dataLine["PartNum"].ToString();
                    if(Helpers.Compartido.VerificarExistenciaColumnas(dataLine, "LineDesc"))
                        LineDesc = dataLine["LineDesc"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(dataLine, "SellingShipQty"))
                        SellingShipQty = dataLine["SellingShipQty"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(dataLine, "SalesUM"))
                        SalesUM = dataLine["SalesUM"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(dataLine, "UnitPrice"))
                        UnitPrice = dataLine["UnitPrice"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(dataLine, "Number01"))
                        Number01 = dataLine["Number01"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(dataLine, "Number02"))
                        Number02 = dataLine["Number02"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(dataLine, "DocExtPrice"))
                        DocExtPrice = dataLine["DocExtPrice"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(dataLine, "DspDocLineTax"))
                        DspDocLineTax = dataLine["DspDocLineTax"].ToString();

                    try
                    {
                        iTextSharp.text.pdf.PdfPCell celCodigo = new iTextSharp.text.pdf.PdfPCell(new Phrase(PartNum, font));
                        celCodigo.Colspan = 1;
                        celCodigo.Padding = 3;
                        //celCodigo.Border = 0;
                        celCodigo.BorderWidthBottom = 0;
                        celCodigo.BorderWidthTop = 0;
                        celCodigo.BorderWidthRight = 0;
                        celCodigo.BorderColorBottom = BaseColor.WHITE;
                        celCodigo.HorizontalAlignment = Element.ALIGN_LEFT;
                        celCodigo.VerticalAlignment = Element.ALIGN_TOP;
                        pdfPTable.AddCell(celCodigo);

                        iTextSharp.text.pdf.PdfPCell celDescripcion = new iTextSharp.text.pdf.PdfPCell(new Phrase(LineDesc, font));
                        celDescripcion.Padding = 3;
                        celDescripcion.Colspan = 1;
                        celDescripcion.BorderWidthBottom = 0;
                        celDescripcion.BorderWidthTop = 0;
                        celDescripcion.BorderWidthRight = 0;
                        celDescripcion.BorderColorBottom = BaseColor.WHITE;
                        celDescripcion.HorizontalAlignment = Element.ALIGN_LEFT;
                        celDescripcion.VerticalAlignment = Element.ALIGN_TOP;
                        pdfPTable.AddCell(celDescripcion);

                        iTextSharp.text.pdf.PdfPCell celCantidad = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:N2}",
                            decimal.Parse(SellingShipQty)), font));
                        celCantidad.Colspan = 1;
                        celCantidad.Padding = 3;
                        //celCantidad.Border = 0;
                        celCantidad.BorderWidthBottom = 0;
                        celCantidad.BorderWidthTop = 0;
                        celCantidad.BorderWidthRight = 0;
                        celCantidad.BorderColorBottom = BaseColor.WHITE;
                        celCantidad.HorizontalAlignment = Element.ALIGN_RIGHT;
                        celCantidad.VerticalAlignment = Element.ALIGN_TOP;
                        pdfPTable.AddCell(celCantidad);

                        iTextSharp.text.pdf.PdfPCell celUM = new iTextSharp.text.pdf.PdfPCell(new Phrase(SalesUM, font));
                        celUM.Colspan = 1;
                        celUM.Padding = 3;
                        celUM.BorderWidthBottom = 0;
                        celUM.BorderWidthTop = 0;
                        celUM.BorderWidthRight = 0;
                        celUM.BorderColorBottom = BaseColor.WHITE;
                        celUM.HorizontalAlignment = Element.ALIGN_CENTER;
                        celUM.VerticalAlignment = Element.ALIGN_TOP;
                        celUM.BorderWidthRight = 0;
                        pdfPTable.AddCell(celUM);

                        iTextSharp.text.pdf.PdfPCell celValorUnitario = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:C2}", Convert.ToDecimal(UnitPrice)),
                            font));
                        celValorUnitario.Colspan = 1;
                        celValorUnitario.Padding = 3;
                        //celValorUnitario.Border = 0;
                        celValorUnitario.BorderWidthBottom = 0;
                        celValorUnitario.BorderWidthTop = 0;
                        celValorUnitario.BorderWidthRight = 0;
                        celValorUnitario.BorderColorBottom = BaseColor.WHITE;
                        celValorUnitario.HorizontalAlignment = Element.ALIGN_RIGHT;
                        celValorUnitario.VerticalAlignment = Element.ALIGN_TOP;
                        pdfPTable.AddCell(celValorUnitario);

                        iTextSharp.text.pdf.PdfPTable tableDescuentos = new PdfPTable(2);

                        iTextSharp.text.pdf.PdfPCell celDesc1 = new iTextSharp.text.pdf.PdfPCell(new Phrase(decimal.Parse(Number01).ToString("N2"), font));
                        celDesc1.Colspan = 1;
                        //celDesc1.Border = 0;
                        celDesc1.BorderWidthBottom = 0;
                        celDesc1.BorderWidthTop = 0;
                        celDesc1.HorizontalAlignment = Element.ALIGN_CENTER;
                        celDesc1.VerticalAlignment = Element.ALIGN_TOP;
                        tableDescuentos.AddCell(celDesc1);

                        iTextSharp.text.pdf.PdfPCell celDesc2 = new iTextSharp.text.pdf.PdfPCell(new Phrase(decimal.Parse(Number02).ToString("N2"), font));
                        celDesc2.Colspan = 1;
                        celDesc2.Border = 0;
                        celDesc2.BorderWidthBottom = 0;
                        celDesc2.BorderWidthTop = 0;
                        celDesc2.HorizontalAlignment = Element.ALIGN_CENTER;
                        celDesc2.VerticalAlignment = Element.ALIGN_TOP;
                        tableDescuentos.AddCell(celDesc2);

                        iTextSharp.text.pdf.PdfPCell _celDescuentos12 = new iTextSharp.text.pdf.PdfPCell(tableDescuentos);
                        //_celDescuentos12.BorderWidthBottom = 0;
                        //_celDescuentos12.BorderWidthTop = 0;
                        _celDescuentos12.Border = 0;
                        pdfPTable.AddCell(_celDescuentos12);

                        iTextSharp.text.pdf.PdfPCell celValorTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:C2}", decimal.Parse(DocExtPrice)), font));
                        celValorTotal.Colspan = 1;
                        celValorTotal.Padding = 3;
                        //celValorTotal.Border = 0;
                        celValorTotal.BorderWidthBottom = 0;
                        celValorTotal.BorderWidthTop = 0;
                        celValorTotal.BorderWidthRight = 0;
                        celValorTotal.BorderColorBottom = BaseColor.WHITE;
                        celValorTotal.HorizontalAlignment = Element.ALIGN_RIGHT;
                        celValorTotal.VerticalAlignment = Element.ALIGN_TOP;
                        pdfPTable.AddCell(celValorTotal);

                        iTextSharp.text.pdf.PdfPCell celIva = new iTextSharp.text.pdf.PdfPCell(new Phrase(decimal.Parse(DspDocLineTax).ToString("C2"), font));
                        celIva.Colspan = 1;
                        celIva.Padding = 3;
                        celIva.BorderWidthBottom = 0;
                        celIva.BorderWidthTop = 0;
                        celIva.HorizontalAlignment = Element.ALIGN_CENTER;
                        celIva.VerticalAlignment = Element.ALIGN_TOP;
                        pdfPTable.AddCell(celIva);

                        return true;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                        return false;
                    }
                }
            }
        }
    }
}
