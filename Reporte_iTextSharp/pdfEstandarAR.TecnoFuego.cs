namespace RulesServicesDIAN2.Adquiriente
{
    using iTextSharp.text;
    using iTextSharp.text.pdf;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Diagnostics;
    using System.Drawing;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.NetworkInformation;
    using System.Text;
    using System.Threading.Tasks;
    using System.Web;
    using System.Windows.Forms;

//    public partial class pdfEstandarAR
//    {

//        #region TECNOFUEGO

//        public bool FacturaNacionalTecnoFuego(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
//        {
//            //NomArchivo = string.Empty;
//            //string Ruta = @"C:\Users\INGJORGEC\source\repos\Reporte_iTextSharp\Reporte_iTextSharp\bin\Debug\PDF\900665411\";
//            //string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
//            //NomArchivo = NombreInvoice + ".pdf";

//            NomArchivo = string.Empty;
//            string Ruta = HttpContext.Current.Server.MapPath("~/PDF/" + NIT + "/");
//            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
//            string RutaImg = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";
//            NomArchivo = NombreInvoice + ".pdf";

//            try
//            {
//                //Validamos Existencia del Directorio.
//                if (!System.IO.Directory.Exists(Ruta))
//                    System.IO.Directory.CreateDirectory(Ruta);

//                //Valido la existencia previa de este archivo.
//                if (System.IO.File.Exists(Ruta + NomArchivo))
//                    System.IO.File.Delete(Ruta + NomArchivo);

//                //Dimenciones del documento.
//                Document document = new Document(iTextSharp.text.PageSize.LETTER, 0, 0, 0, 10);

//                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));

//                Paragraph separator = new Paragraph("\n");
//                separator.Alignment = Element.ALIGN_CENTER;

//                writer.PageEvent = new EventPageTecnoFuego(ref strError, DsInvoiceAR, CUFE, NIT, QRInvoice);

//                // step 3: we open the document     
//                document.Open();

//                #region FUENTES
//                //Arial
//                iTextSharp.text.Font Arial4 = FontFactory.GetFont("Arial", 4, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font Arial5 = FontFactory.GetFont("Arial", 5, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font Arial6 = FontFactory.GetFont("Arial", 6, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font Arial7 = FontFactory.GetFont("Arial", 7, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font Arial8 = FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font Arial9 = FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font Arial10 = FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font Arial11 = FontFactory.GetFont("Arial", 11, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font Arial12 = FontFactory.GetFont("Arial", 12, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font Arial14 = FontFactory.GetFont("Arial", 14, iTextSharp.text.Font.NORMAL);


//                //ArialBold14
//                iTextSharp.text.Font ArialBold4 = FontFactory.GetFont("Arial", 4, iTextSharp.text.Font.BOLD);
//                iTextSharp.text.Font ArialBold5 = FontFactory.GetFont("Arial", 5, iTextSharp.text.Font.BOLD);
//                iTextSharp.text.Font ArialBold6 = FontFactory.GetFont("Arial", 6, iTextSharp.text.Font.BOLD);
//                iTextSharp.text.Font ArialBold7 = FontFactory.GetFont("Arial", 7, iTextSharp.text.Font.BOLD);
//                iTextSharp.text.Font ArialBold8 = FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD);
//                iTextSharp.text.Font ArialBold9 = FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD);
//                iTextSharp.text.Font ArialBold10 = FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD);
//                iTextSharp.text.Font ArialBold11 = FontFactory.GetFont("Arial", 11, iTextSharp.text.Font.BOLD);
//                iTextSharp.text.Font ArialBold12 = FontFactory.GetFont("Arial", 12, iTextSharp.text.Font.BOLD);
//                iTextSharp.text.Font ArialBold14 = FontFactory.GetFont("Arial", 14, iTextSharp.text.Font.BOLD);
//                iTextSharp.text.Font ArialBold18 = FontFactory.GetFont("Arial", 18, iTextSharp.text.Font.BOLD);


//                //HELVETICA
//                iTextSharp.text.Font Helvetica4 = FontFactory.GetFont(FontFactory.HELVETICA, 4, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font Helvetica5 = FontFactory.GetFont(FontFactory.HELVETICA, 5, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font Helvetica6 = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font Helvetica7 = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font Helvetica8 = FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font Helvetica9 = FontFactory.GetFont(FontFactory.HELVETICA, 9, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font Helvetica10 = FontFactory.GetFont(FontFactory.HELVETICA, 10, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font Helvetica11 = FontFactory.GetFont(FontFactory.HELVETICA, 11, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font Helvetica12 = FontFactory.GetFont(FontFactory.HELVETICA, 12, iTextSharp.text.Font.NORMAL);


//                //HELVETICA BOLD
//                iTextSharp.text.Font HelveticBOLD4 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 4, iTextSharp.text.Font.BOLD);
//                iTextSharp.text.Font HelveticaBOLD5 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 5, iTextSharp.text.Font.BOLD);
//                iTextSharp.text.Font HelveticaBOLD6 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 6, iTextSharp.text.Font.BOLD);
//                iTextSharp.text.Font HelveticaBOLD7 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.BOLD);
//                iTextSharp.text.Font HelveticaBOLD8 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 8, iTextSharp.text.Font.BOLD);
//                iTextSharp.text.Font HelveticaBOLD9 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 9, iTextSharp.text.Font.BOLD);
//                iTextSharp.text.Font HelveticaBOLD10 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 10, iTextSharp.text.Font.BOLD);
//                iTextSharp.text.Font HelveticaBOLD11 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 11, iTextSharp.text.Font.BOLD);
//                iTextSharp.text.Font HelveticaBOLD12 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 12, iTextSharp.text.Font.BOLD);


//                //COURIER
//                iTextSharp.text.Font Courier4 = FontFactory.GetFont(FontFactory.COURIER, 4, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font Courier6 = FontFactory.GetFont(FontFactory.COURIER, 6, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font Courier5 = FontFactory.GetFont(FontFactory.COURIER, 5, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font Courier7 = FontFactory.GetFont(FontFactory.COURIER, 7, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font Courier8 = FontFactory.GetFont(FontFactory.COURIER, 8, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font Courier9 = FontFactory.GetFont(FontFactory.COURIER, 9, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font Courier10 = FontFactory.GetFont(FontFactory.COURIER, 10, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font Courier11 = FontFactory.GetFont(FontFactory.COURIER, 11, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font Courier12 = FontFactory.GetFont(FontFactory.COURIER, 12, iTextSharp.text.Font.NORMAL);

//                #endregion

//                // ESPACIO 1
//                PdfDiv divEspacio = new PdfDiv();
//                divEspacio.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
//                divEspacio.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
//                divEspacio.Height = 10;
//                divEspacio.Width = 130;

//                //ESPACIO2
//                PdfDiv divEspacio2 = new PdfDiv();
//                divEspacio2.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
//                divEspacio2.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
//                divEspacio2.Height = 2;
//                divEspacio2.Width = 130;


//                //ZONA ENCABEZADO
//                #region DimensionamientoTablaEncabezado
//                PdfPTable TblEncabezadoFactura = new PdfPTable(5);

//                //Dimensiones de tabla.
//                float[] DimensionEncabezado = new float[5];
//                DimensionEncabezado[0] = 1.3F;//
//                DimensionEncabezado[1] = 1.8F;//
//                DimensionEncabezado[2] = 1.0F;//
//                DimensionEncabezado[3] = 0.8F;//
//                DimensionEncabezado[4] = 2.2F;//

//                TblEncabezadoFactura.WidthPercentage = 95;
//                TblEncabezadoFactura.HorizontalAlignment = Element.ALIGN_CENTER;

//                TblEncabezadoFactura.SetWidths(DimensionEncabezado);
//                #endregion

//                //CELDA ENCABEZADO FACTURA
//                #region EncabezadoFactura
//                PdfPCell CelLogoFactura = new PdfPCell()
//                {
//                    Border = 0,
//                    HorizontalAlignment = Element.ALIGN_LEFT,
//                };


//                string HeaderDoc = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";
//                // var HeaderDoc = $@"{AppDomain.CurrentDomain.BaseDirectory}\Logo_" + NIT + ".png";

//                iTextSharp.text.Image ITHeaderDoc = iTextSharp.text.Image.GetInstance(HeaderDoc);
//                ITHeaderDoc.ScaleAbsoluteWidth(100);
//                ITHeaderDoc.ScaleAbsoluteHeight(110);
//                ITHeaderDoc.Alignment = Element.ALIGN_LEFT;

//                CelLogoFactura.AddElement(ITHeaderDoc);

//                TblEncabezadoFactura.AddCell(CelLogoFactura);

//                PdfPCell CelInfoCia = new PdfPCell()
//                {
//                    BorderColor = BaseColor.WHITE,
//                    HorizontalAlignment = Element.ALIGN_RIGHT,
//                    Colspan = 3
//                };


//                Paragraph phCompanyName = new Paragraph(DsInvoiceAR.Tables["Company"].Rows[0]["Name"].ToString(), ArialBold18);
//                Paragraph phNIT = new Paragraph("NIT: " + DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"].ToString(), Helvetica12);
//                Paragraph phRegimenComun = new Paragraph("IVA REGIMEN COMÚN", Helvetica12);


//                phCompanyName.Alignment = Element.ALIGN_LEFT;
//                phNIT.Alignment = Element.ALIGN_LEFT;
//                phRegimenComun.Alignment = Element.ALIGN_LEFT;

//                CelInfoCia.AddElement(phCompanyName);
//                CelInfoCia.AddElement(phNIT);
//                CelInfoCia.AddElement(phRegimenComun);


//                TblEncabezadoFactura.AddCell(CelInfoCia);



//                Paragraph phAddress = new Paragraph(DsInvoiceAR.Tables["Company"].Rows[0]["Address1"].ToString(), Helvetica8);
//                Paragraph phPBX = new Paragraph("PBX: " + DsInvoiceAR.Tables["Company"].Rows[0]["PhoneNum"].ToString(), Helvetica8);
//                //  Paragraph phEmailAddress = new Paragraph("e-mail: "+DsInvoiceAR.Tables["Company"].Rows[0]["EmailAddress"].ToString(), Helvetica8);
//                Paragraph phCityState = new Paragraph(DsInvoiceAR.Tables["Company"].Rows[0]["City"].ToString() + " - Colombia", Helvetica8);

//                phAddress.Alignment = Element.ALIGN_CENTER;
//                phPBX.Alignment = Element.ALIGN_CENTER;
//                phCityState.Alignment = Element.ALIGN_CENTER;



//                PdfPCell CelInfoCIA2 = new PdfPCell()
//                {
//                    Border = 0,
//                    HorizontalAlignment = Element.ALIGN_RIGHT,
//                };


//                CelInfoCIA2.AddElement(phAddress);
//                CelInfoCIA2.AddElement(phPBX);
//                //CelInfoCIA2.AddElement(phEmailAddress);
//                CelInfoCIA2.AddElement(phCityState);


//                //Celda que Contiene TITULO FACTURA DE VENTA
//                PdfPTable tblFacturaVentaTitle = new PdfPTable(1);
//                Paragraph phFActuraVenta = new Paragraph("FACTURA DE VENTA", ArialBold12);
//                phFActuraVenta.Alignment = Element.ALIGN_CENTER;
//                tblFacturaVentaTitle.AddCell(new PdfPCell() { Border = 0, BackgroundColor = BaseColor.LIGHT_GRAY, Phrase = phFActuraVenta });

//                //TITULO NUMERO DE FACTURA
//                CelInfoCIA2.AddElement(tblFacturaVentaTitle);


//                //Numero Legal Value
//                Paragraph PHNumeroLegal = new Paragraph("No. " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"].ToString(), ArialBold12);
//                PHNumeroLegal.Alignment = Element.ALIGN_CENTER;
//                CelInfoCIA2.AddElement(PHNumeroLegal);

//                //CELDA INFO CIA Y NUMERO DE FACTURA
//                TblEncabezadoFactura.AddCell(CelInfoCIA2);

//                TblEncabezadoFactura.AddCell(new PdfPCell() { Colspan = 5, Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, Phrase = new Phrase("Representación grafica factura electrónica.        -        CUFE: " + CUFE, Helvetica5) });


//                //RESOLUCION
//                TblEncabezadoFactura.AddCell(new PdfPCell() { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, Colspan = 5, Phrase = new Phrase(DsInvoiceAR.Tables["Company"].Rows[0]["ResolucionDian_c"].ToString(), Helvetica5) });

//                #endregion

//                #region INFO CLIENTE


//                PdfPCell CeldaTitlesCliente = new PdfPCell() { Colspan = 2, HorizontalAlignment = Element.ALIGN_LEFT };

//                Phrase phCliente = new Phrase();
//                Chunk chCliente = new Chunk("CLIENTE:          ", ArialBold10);
//                Chunk chClienteValue = new Chunk(DsInvoiceAR.Tables["Customer"].Rows[0]["Name"].ToString(), Helvetica10);
//                phCliente.Add(chCliente);
//                phCliente.Add(chClienteValue);


//                Phrase phNITCliente = new Phrase();
//                Chunk chNIT = new Chunk("NIT:                   ", ArialBold10);
//                Chunk chNITValue = new Chunk(DsInvoiceAR.Tables["Customer"].Rows[0]["ResaleID"].ToString(), Helvetica10);
//                phNITCliente.Add(chNIT);
//                phNITCliente.Add(chNITValue);

//                Phrase phAddressCliente = new Phrase();
//                Chunk chAddress = new Chunk("DIRECCIÓN:     ", ArialBold10);
//                Chunk chAddressValue = new Chunk(DsInvoiceAR.Tables["Customer"].Rows[0]["Address1"].ToString(), Helvetica10);
//                phAddressCliente.Add(chAddress);
//                phAddressCliente.Add(chAddressValue);


//                Phrase phPhone = new Phrase();
//                Chunk chPhone = new Chunk("TELEFÓNO:      ", ArialBold10);
//                Chunk chPhoneValue = new Chunk(DsInvoiceAR.Tables["Customer"].Rows[0]["PhoneNum"].ToString(), Helvetica10);
//                phPhone.Add(chPhone);
//                phPhone.Add(chPhoneValue);


//                Phrase phCity = new Phrase();
//                Chunk chCity = new Chunk("CIUDAD:           ", ArialBold10);
//                Chunk chCityValue = new Chunk(DsInvoiceAR.Tables["Customer"].Rows[0]["City"].ToString() + " " + DsInvoiceAR.Tables["Customer"].Rows[0]["State"].ToString() + " " + DsInvoiceAR.Tables["Customer"].Rows[0]["Country"].ToString(), Helvetica10);
//                phCity.Add(chCity);
//                phCity.Add(chCityValue);

//                CeldaTitlesCliente.AddElement(phCliente);
//                CeldaTitlesCliente.AddElement(phNITCliente);
//                CeldaTitlesCliente.AddElement(phAddressCliente);
//                CeldaTitlesCliente.AddElement(phPhone);
//                CeldaTitlesCliente.AddElement(phCity);

//                #endregion

//                #region InfoFactura2 (Fechas)

//                TblEncabezadoFactura.AddCell(CeldaTitlesCliente);
//                PdfPTable tblInfoFactura2 = new PdfPTable(3);
//                float[] DimensionInfoFactura2 = new float[3];
//                DimensionInfoFactura2[0] = 1.5F;//
//                DimensionInfoFactura2[1] = 1.0F;//
//                DimensionInfoFactura2[2] = 1.0F;//
//                tblInfoFactura2.WidthPercentage = 100;
//                tblInfoFactura2.HorizontalAlignment = Element.ALIGN_CENTER;


//                tblInfoFactura2.AddCell(new PdfPCell() { Phrase = new Phrase("FECHA FACTURA", ArialBold9) });
//                tblInfoFactura2.AddCell(new PdfPCell() { Phrase = new Phrase("PLAZO", ArialBold9) });
//                tblInfoFactura2.AddCell(new PdfPCell() { Phrase = new Phrase("VENCIMIENTO", ArialBold9) });

//                var FechaFact = Convert.ToDateTime(DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).Day.ToString() + "-" + CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(Convert.ToInt32(Convert.ToDateTime(DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).Month.ToString())) + "-" + Convert.ToDateTime(DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).Year.ToString();
//                var DueDateFact = Convert.ToDateTime(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"]).Day.ToString() + "-" + CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(Convert.ToInt32(Convert.ToDateTime(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"]).Month.ToString())) + "-" + Convert.ToDateTime(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"]).Year.ToString();

//                tblInfoFactura2.AddCell(new PdfPCell() { Phrase = new Phrase(FechaFact, Helvetica9) });
//                tblInfoFactura2.AddCell(new PdfPCell() { Phrase = new Phrase(DsInvoiceAR.Tables["InvcHead"].Rows[0]["TermsCodeDescription"].ToString(), Helvetica9) });
//                tblInfoFactura2.AddCell(new PdfPCell() { Phrase = new Phrase(DueDateFact, Helvetica9) });


//                tblInfoFactura2.AddCell(new PdfPCell() { HorizontalAlignment = Element.ALIGN_CENTER, Colspan = 3, Phrase = new Phrase("ORDEN DE COMPRA", ArialBold9) });

//                tblInfoFactura2.AddCell(new PdfPCell() { HorizontalAlignment = Element.ALIGN_CENTER, Colspan = 3, Phrase = new Phrase(DsInvoiceAR.Tables["InvcHead"].Rows[0]["PONum"].ToString(), Helvetica9) });

//                tblInfoFactura2.AddCell(new PdfPCell() { HorizontalAlignment = Element.ALIGN_LEFT, Phrase = new Phrase("VENDEDOR", ArialBold9) });

//                tblInfoFactura2.AddCell(new PdfPCell() { Colspan = 2, HorizontalAlignment = Element.ALIGN_LEFT, Phrase = new Phrase(DsInvoiceAR.Tables["InvcHead"].Rows[0]["SalesRepName1"].ToString(), Helvetica9) });


//                TblEncabezadoFactura.AddCell(new PdfPCell(tblInfoFactura2) { Colspan = 3, Border = 0 });

//                #endregion

//                TblEncabezadoFactura.AddCell(new PdfPCell() { HorizontalAlignment = Element.ALIGN_RIGHT, Colspan = 5, Phrase = new Phrase("Moneda: " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["CurrencyCode"].ToString(), Helvetica6) });

//                PdfPTable tblLineasFactura = new PdfPTable(6);
//                float[] DimensionesLineasFactura = new float[6];
//                DimensionesLineasFactura[0] = 1.5F;//
//                DimensionesLineasFactura[1] = 1.5F;//
//                DimensionesLineasFactura[2] = 0.8F;//
//                DimensionesLineasFactura[3] = 1.0F;//
//                DimensionesLineasFactura[4] = 1.0F;//
//                DimensionesLineasFactura[5] = 1.0F;//
//                tblLineasFactura.WidthPercentage = 95;
//                tblLineasFactura.HorizontalAlignment = Element.ALIGN_CENTER;
//                tblLineasFactura.SetWidths(DimensionesLineasFactura);

//                PdfPCell CelTituloDesc = new PdfPCell(new Phrase(
//                    "DESCRIPCIÓN", ArialBold8))
//                {
//                    Colspan = 2,
//                    BorderColor = BaseColor.BLACK,
//                    Border = PdfPCell.LEFT_BORDER | PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER,
//                    HorizontalAlignment = Element.ALIGN_LEFT
//                };

//                tblLineasFactura.AddCell(CelTituloDesc);

//                PdfPCell CelTituloCantidad = new PdfPCell(new Phrase(
//                    "CANTIDAD", ArialBold8))
//                {
//                    Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER,
//                    BorderColor = BaseColor.BLACK,
//                    HorizontalAlignment = Element.ALIGN_RIGHT
//                };

//                tblLineasFactura.AddCell(CelTituloCantidad);

//                PdfPCell CelTituloPrecio = new PdfPCell(new Phrase(
//                    "PRECIO", ArialBold8))
//                {
//                    BorderColor = BaseColor.BLACK,
//                    Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER,
//                    HorizontalAlignment = Element.ALIGN_RIGHT
//                };

//                tblLineasFactura.AddCell(CelTituloPrecio);

//                PdfPCell CelTitulosubtotal = new PdfPCell(new Phrase(
//                    "SUBTOTAL", ArialBold8))
//                {
//                    BorderColor = BaseColor.BLACK,
//                    Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER,
//                    HorizontalAlignment = Element.ALIGN_RIGHT
//                };

//                tblLineasFactura.AddCell(CelTitulosubtotal);

//                PdfPCell CelTituloDcto = new PdfPCell(new Phrase(
//                    "DESCUENTO", ArialBold8))
//                {
//                    BorderColor = BaseColor.BLACK,
//                    Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER | PdfPCell.RIGHT_BORDER,
//                    HorizontalAlignment = Element.ALIGN_RIGHT
//                };
//                tblLineasFactura.AddCell(CelTituloDcto);

//                //-------------------------------------------------------------------------------

//                PdfPTable tableValUnidades = new PdfPTable(DimensionesLineasFactura);
//                tableValUnidades.WidthPercentage = 95;
//                tableValUnidades.HorizontalAlignment = Element.ALIGN_CENTER;

//                decimal totalDescuento = 0, totalSubtotal = 0;
//                double totalCantidad = 0;
//                foreach (DataRow InvoiceLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
//                {
//                    totalCantidad += Convert.ToDouble(InvoiceLine["SellingShipQty"]);
//                    AddUnidadesFacturaNacionalTecnoFuego(InvoiceLine, ref tableValUnidades, Arial7);
//                    totalDescuento += decimal.Parse((string)InvoiceLine["DocDiscount"]);
//                    totalSubtotal += decimal.Parse((string)InvoiceLine["DspDocExtPrice"]);
//                }

//                var cantidadFilasAdd = 20 - DsInvoiceAR.Tables["InvcDtl"].Rows.Count;
//                for (int i = 0; i <= cantidadFilasAdd; i++)
//                {
//                    if (i == cantidadFilasAdd)
//                    {
//                        tableValUnidades.AddCell(new PdfPCell()
//                        {
//                            Colspan = 6,
//                            Border = PdfPCell.LEFT_BORDER | PdfPCell.RIGHT_BORDER
//                        });
//                    }
//                    else
//                    {
//                        tableValUnidades.AddCell(new PdfPCell(new Phrase(" "))
//                        {
//                            Colspan = 6,
//                            //BackgroundColor=BaseColor.LIGHT_GRAY,
//                            Border = PdfPCell.LEFT_BORDER | PdfPCell.RIGHT_BORDER,
//                        });
//                    }
//                }

//                document.Add(divEspacio2);

//                PdfPTable tblFindDcto = new PdfPTable(4);
//                float[] DimensionesFinDctoFactura = new float[4];
//                DimensionesFinDctoFactura[0] = 1.5F;//
//                DimensionesFinDctoFactura[1] = 1.5F;//
//                DimensionesFinDctoFactura[2] = 0.8F;//
//                DimensionesFinDctoFactura[3] = 0.8F;//
//                tblFindDcto.WidthPercentage = 95;
//                tblFindDcto.HorizontalAlignment = Element.ALIGN_CENTER;
//                tblFindDcto.SetWidths(DimensionesFinDctoFactura);


//                #region OBSERVACIONES
//                Chunk chObservaciones = new Chunk("OBSERVACIONES:   ", ArialBold9);
//                Chunk chObservacionesVal = new Chunk(DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceComment"].ToString(), Helvetica9);
//                Phrase phObservaciones = new Phrase();
//                phObservaciones.Add(chObservaciones);
//                phObservaciones.Add(chObservacionesVal);

//                tblFindDcto.AddCell(new PdfPCell() { Colspan = 2, Phrase = phObservaciones });

//                #endregion

//                #region TOTALESFACTURA

//                PdfPTable tblTotalesFactura = new PdfPTable(2);
//                float[] DimensionestotalesFactura = new float[2];
//                DimensionestotalesFactura[0] = 1.0F;//
//                DimensionestotalesFactura[1] = 1.0F;//
//                tblTotalesFactura.WidthPercentage = 95;
//                tblTotalesFactura.HorizontalAlignment = Element.ALIGN_CENTER;
//                tblTotalesFactura.SetWidths(DimensionestotalesFactura);

//                tblTotalesFactura.AddCell(new PdfPCell() { Phrase = new Phrase("VALOR", ArialBold9) });
//                tblTotalesFactura.AddCell(new PdfPCell() { HorizontalAlignment = Element.ALIGN_RIGHT, Phrase = new Phrase(Convert.ToDecimal(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubTotal"]).ToString("C0"), Helvetica9) });

//                tblTotalesFactura.AddCell(new PdfPCell() { Phrase = new Phrase("DESCUENTO", ArialBold9) });
//                tblTotalesFactura.AddCell(new PdfPCell() { HorizontalAlignment = Element.ALIGN_RIGHT, Phrase = new Phrase(totalDescuento.ToString("C0"), Helvetica9) });

//                tblTotalesFactura.AddCell(new PdfPCell() { Phrase = new Phrase("IVA", ArialBold9) });
//                tblTotalesFactura.AddCell(new PdfPCell() { HorizontalAlignment = Element.ALIGN_RIGHT, Phrase = new Phrase(Convert.ToDecimal(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DocTaxAmt"]).ToString("C0"), Helvetica9) });

//                tblFindDcto.AddCell(new PdfPCell(tblTotalesFactura) { Colspan = 2 });
//                #endregion

//                #region REMISION
//                Chunk chRemision = new Chunk("REMISIÓN:   ", ArialBold9);
//                Chunk chRemisionVal = new Chunk(DsInvoiceAR.Tables["InvcDtl"].Rows[0]["PackNum"].ToString(), Helvetica9);
//                Phrase phremosion = new Phrase();
//                phremosion.Add(chRemision);
//                phremosion.Add(chRemisionVal);

//                tblFindDcto.AddCell(new PdfPCell() { Colspan = 2, Phrase = phremosion });
//                #endregion

//                #region SUBTOTAL TF

//                PdfPTable tblsubTotalTF = new PdfPTable(2);
//                float[] DimensionessubTotalTF = new float[2];
//                DimensionessubTotalTF[0] = 1.0F;//
//                DimensionessubTotalTF[1] = 1.0F;//
//                tblsubTotalTF.WidthPercentage = 95;
//                tblsubTotalTF.HorizontalAlignment = Element.ALIGN_CENTER;
//                tblsubTotalTF.SetWidths(DimensionestotalesFactura);

//                tblsubTotalTF.AddCell(new PdfPCell() { Phrase = new Phrase("SUBTOTAL", ArialBold9) });
//                tblsubTotalTF.AddCell(new PdfPCell() { HorizontalAlignment = Element.ALIGN_RIGHT, Phrase = new Phrase(Convert.ToDecimal(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DocSubTotal"]).ToString("C0"), Helvetica9) });

//                tblsubTotalTF.AddCell(new PdfPCell() { Phrase = new Phrase("(-) TF", ArialBold9) });
//                tblsubTotalTF.AddCell(new PdfPCell() { HorizontalAlignment = Element.ALIGN_RIGHT, Phrase = new Phrase(totalDescuento.ToString("C0"), Helvetica9) });

//                tblFindDcto.AddCell(new PdfPCell(tblsubTotalTF) { Colspan = 2 });
//                #endregion

//                #region VALORENLETRAS


//                string NroLetrasTex = Nroenletras(Math.Abs(Convert.ToDecimal(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"])).ToString());


//                if (DsInvoiceAR.Tables["InvcHead"].Rows[0]["CurrencyCode"].ToString().ToUpper() != "COP")
//                {
//                    NroLetrasTex = NroLetrasTex.Replace("PESOS", "DOLARES");
//                }
//                else
//                {
//                    NroLetrasTex = Nroenletras(Math.Abs(Convert.ToDecimal(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DocInvoiceAmt"])).ToString());
//                }


//                Chunk chValorLetras = new Chunk("VALOR EN LETRAS:   ", ArialBold9);
//                Chunk chValorLetrasVal = new Chunk(NroLetrasTex, ArialBold9);

//                Phrase phValorLetras = new Phrase();
//                phValorLetras.Add(chValorLetras);
//                phValorLetras.Add(chValorLetrasVal);
//                tblFindDcto.AddCell(new PdfPCell() { Colspan = 2, Phrase = phValorLetras });


//                Phrase phTotal = new Phrase("TOTAL:                       " + Convert.ToDecimal(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DocInvoiceAmt"]).ToString("C0"), ArialBold9);
//                tblFindDcto.AddCell(new PdfPCell() { Colspan = 2, Phrase = phTotal });

//                #endregion

//                #region FIRMAS

//                Paragraph phGerente = new Paragraph("\n \n \n \n \n \n ING. ARTURO CASTILLO PEREZ \n GERENTE", Helvetica9);
//                phGerente.Alignment = Element.ALIGN_CENTER;



//                tblFindDcto.AddCell(new PdfPCell() { PaddingTop = 4, Phrase = new Phrase("Sirvase Realizar consignacion así: BANCOLOMBIA CTA CTE No. 4753116104 - 5 Suc.Murillo, BARRANQUILLA.", Helvetica6) });
//                tblFindDcto.AddCell(new PdfPCell() { HorizontalAlignment = Element.ALIGN_CENTER, Phrase = phGerente });
//                PdfPCell CellFirma = new PdfPCell() { Colspan = 2 };
//                Paragraph phAceptacion = new Paragraph("Acepto el contenido de la presente factura y hago constar el recibo conforme en ella discriminada", Helvetica5);
//                phAceptacion.Alignment = Element.ALIGN_CENTER;

//                Paragraph phFimaYSello = new Paragraph("\n \n\n \n ______________________________________________________\n Firma y Sello del Cliente", Helvetica6);
//                phFimaYSello.Alignment = Element.ALIGN_CENTER;

//                CellFirma.AddElement(phAceptacion);
//                CellFirma.AddElement(phFimaYSello);
//                tblFindDcto.AddCell(CellFirma);


//                tblFindDcto.AddCell(new PdfPCell()
//                {
//                    Border = 0,
//                    Phrase = new Phrase($"Epicor.ERP " +
//                    $"{(string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceNum"]}", ArialBold6)
//                });
//                tblFindDcto.AddCell(new PdfPCell() { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, Phrase = new Phrase("\n www.tecno-fuego.com.co", ArialBold6) }); 
//                tblFindDcto.AddCell(new PdfPCell() { HorizontalAlignment = Element.ALIGN_CENTER, Border = 0, Colspan = 2, Phrase = new Phrase("O.V: 0"+ DsInvoiceAR.Tables["InvcDtl"].Rows[0]["OrderNum"].ToString(), Helvetica7) });

//                tblFindDcto.AddCell(new PdfPCell() { HorizontalAlignment = Element.ALIGN_CENTER, Border = 0, Colspan = 4, Phrase = new Phrase("ESTA FACTURA DE VENTA ES UN TITULO VALOR EN CUANTO CUMPLE CON LOS REQUERIMIENTOS EXIGIDOS POR LA LEY 1231 / 08.", Helvetica7) });


//                #endregion  

//                document.Add(TblEncabezadoFactura);
//                document.Add(tblLineasFactura);
//                document.Add(tableValUnidades);
//                document.Add(tblFindDcto);

//                /*PIE DE PAGINA*/
//                PdfContentByte pCb = writer.DirectContent;
//                // PieDePagina(ref pCb);

//                writer.Flush();
//                document.Close();

//                //AddFooterDocMercaLlantas(Ruta + NomArchivo, 180, 180);
//                RutaPdf = NomArchivo;
//                return true;
//            }
//            catch (Exception ex)
//            {
//                strError = ex.ToString();
//                //MessageBox.Show(ex.ToString());
//                return false;
//            }
//        }

//        private bool AddUnidadesFacturaNacionalTecnoFuego(DataRow dataLine, ref PdfPTable pdfPTable, iTextSharp.text.Font font)
//        {
//            try
//            {
//                Phrase prgLineDesc = new Phrase();
//                prgLineDesc.Add(new Chunk($"{(string)dataLine["LineDesc"]}\n", font));
//                prgLineDesc.Add(new Chunk($"{(string)dataLine["InvoiceComment"]}",
//                    FontFactory.GetFont("Arial", 7, iTextSharp.text.Font.BOLD)));

//                PdfPCell celDescripcion = new PdfPCell(prgLineDesc);
//                celDescripcion.Colspan = 2;
//                celDescripcion.HorizontalAlignment = Element.ALIGN_LEFT;
//                celDescripcion.VerticalAlignment = Element.ALIGN_TOP;
//                pdfPTable.AddCell(celDescripcion);

//                iTextSharp.text.pdf.PdfPCell celCantidad = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:N0}", dataLine["SellingShipQty"].ToString()), font));
//                celCantidad.HorizontalAlignment = Element.ALIGN_RIGHT;
//                celCantidad.VerticalAlignment = Element.ALIGN_TOP;
//                pdfPTable.AddCell(celCantidad);



//                iTextSharp.text.pdf.PdfPCell celValorUnitario = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:C2}",
//                Convert.ToDecimal(dataLine["DocUnitPrice"].ToString())), font));
//                celValorUnitario.HorizontalAlignment = Element.ALIGN_RIGHT;
//                celValorUnitario.VerticalAlignment = Element.ALIGN_TOP;
//                pdfPTable.AddCell(celValorUnitario);


//                iTextSharp.text.pdf.PdfPCell celValorTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:C2}",
//                decimal.Parse(dataLine["DocExtPrice"].ToString())), font));
//                celValorTotal.PaddingTop = 0;
//                celValorTotal.HorizontalAlignment = Element.ALIGN_RIGHT;
//                celValorTotal.VerticalAlignment = Element.ALIGN_TOP;
//                pdfPTable.AddCell(celValorTotal);



//                iTextSharp.text.pdf.PdfPCell CelDcto = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:C2}",
//                decimal.Parse(dataLine["DocDiscount"].ToString())), font));
//                CelDcto.HorizontalAlignment = Element.ALIGN_RIGHT;
//                CelDcto.VerticalAlignment = Element.ALIGN_TOP;
//                pdfPTable.AddCell(CelDcto);


//                return true;
//            }
//            catch (Exception ex)
//            {
//                strError += ex.Message;
//                //MessageBox.Show(ex.Message);
//                return false;
//            }
//        }

//        public bool NotaCreditoTecnoFuego(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
//        {
//            //NomArchivo = string.Empty;
//            //string Ruta = @"C:\Users\INGJORGEC\source\repos\Reporte_iTextSharp\Reporte_iTextSharp\bin\Debug\PDF\900665411\";
//            //string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
//            //NomArchivo = NombreInvoice + ".pdf";

//            NomArchivo = string.Empty;
//            string Ruta = HttpContext.Current.Server.MapPath("~/PDF/" + NIT + "/");
//            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
//            string RutaImg = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";
//            NomArchivo = NombreInvoice + ".pdf";

//            try
//            {
//                //Validamos Existencia del Directorio.
//                if (!System.IO.Directory.Exists(Ruta))
//                    System.IO.Directory.CreateDirectory(Ruta);

//                //Valido la existencia previa de este archivo.
//                if (System.IO.File.Exists(Ruta + NomArchivo))
//                    System.IO.File.Delete(Ruta + NomArchivo);

//                //Dimenciones del documento.
//                Document document = new Document(iTextSharp.text.PageSize.LETTER, 0, 0, 0, 10);

//                //if (DsInvoiceAR.Tables["InvcDtl"].Rows.Count < 30)
//                //{
//                //    document = new Document(iTextSharp.text.PageSize.LETTER, 0, 0, 0, 125);
//                //}

//                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));

//                Paragraph separator = new Paragraph("\n");
//                separator.Alignment = Element.ALIGN_CENTER;

//                writer.PageEvent = new EventPageTecnoFuego(ref strError, DsInvoiceAR, CUFE, NIT, QRInvoice);

//                // step 3: we open the document     
//                document.Open();



//                #region FUENTES
//                //Arial
//                iTextSharp.text.Font Arial4 = FontFactory.GetFont("Arial", 4, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font Arial5 = FontFactory.GetFont("Arial", 5, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font Arial6 = FontFactory.GetFont("Arial", 6, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font Arial7 = FontFactory.GetFont("Arial", 7, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font Arial8 = FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font Arial9 = FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font Arial10 = FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font Arial11 = FontFactory.GetFont("Arial", 11, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font Arial12 = FontFactory.GetFont("Arial", 12, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font Arial14 = FontFactory.GetFont("Arial", 14, iTextSharp.text.Font.NORMAL);


//                //Arialbold14
//                iTextSharp.text.Font ArialBold4 = FontFactory.GetFont("Arial", 4, iTextSharp.text.Font.BOLD);
//                iTextSharp.text.Font ArialBold5 = FontFactory.GetFont("Arial", 5, iTextSharp.text.Font.BOLD);
//                iTextSharp.text.Font ArialBold6 = FontFactory.GetFont("Arial", 6, iTextSharp.text.Font.BOLD);
//                iTextSharp.text.Font ArialBold7 = FontFactory.GetFont("Arial", 7, iTextSharp.text.Font.BOLD);
//                iTextSharp.text.Font ArialBold8 = FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD);
//                iTextSharp.text.Font ArialBold9 = FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD);
//                iTextSharp.text.Font ArialBold10 = FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD);
//                iTextSharp.text.Font ArialBold11 = FontFactory.GetFont("Arial", 11, iTextSharp.text.Font.BOLD);
//                iTextSharp.text.Font ArialBold12 = FontFactory.GetFont("Arial", 12, iTextSharp.text.Font.BOLD);
//                iTextSharp.text.Font ArialBold14 = FontFactory.GetFont("Arial", 14, iTextSharp.text.Font.BOLD);
//                iTextSharp.text.Font ArialBold18 = FontFactory.GetFont("Arial", 18, iTextSharp.text.Font.BOLD);

//                //HELVETICA
//                iTextSharp.text.Font Helvetica4 = FontFactory.GetFont(FontFactory.HELVETICA, 4, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font Helvetica5 = FontFactory.GetFont(FontFactory.HELVETICA, 5, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font Helvetica6 = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font Helvetica7 = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font Helvetica8 = FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font Helvetica9 = FontFactory.GetFont(FontFactory.HELVETICA, 9, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font Helvetica10 = FontFactory.GetFont(FontFactory.HELVETICA, 10, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font Helvetica11 = FontFactory.GetFont(FontFactory.HELVETICA, 11, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font Helvetica12 = FontFactory.GetFont(FontFactory.HELVETICA, 12, iTextSharp.text.Font.NORMAL);


//                //HELVETICA BOLD
//                iTextSharp.text.Font HelveticBOLD4 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 4, iTextSharp.text.Font.BOLD);
//                iTextSharp.text.Font HelveticaBOLD5 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 5, iTextSharp.text.Font.BOLD);
//                iTextSharp.text.Font HelveticaBOLD6 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 6, iTextSharp.text.Font.BOLD);
//                iTextSharp.text.Font HelveticaBOLD7 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.BOLD);
//                iTextSharp.text.Font HelveticaBOLD8 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 8, iTextSharp.text.Font.BOLD);
//                iTextSharp.text.Font HelveticaBOLD9 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 9, iTextSharp.text.Font.BOLD);
//                iTextSharp.text.Font HelveticaBOLD10 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 10, iTextSharp.text.Font.BOLD);
//                iTextSharp.text.Font HelveticaBOLD11 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 11, iTextSharp.text.Font.BOLD);
//                iTextSharp.text.Font HelveticaBOLD12 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 12, iTextSharp.text.Font.BOLD);


//                //COURIER
//                iTextSharp.text.Font Courier4 = FontFactory.GetFont(FontFactory.COURIER, 4, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font Courier6 = FontFactory.GetFont(FontFactory.COURIER, 6, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font Courier5 = FontFactory.GetFont(FontFactory.COURIER, 5, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font Courier7 = FontFactory.GetFont(FontFactory.COURIER, 7, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font Courier8 = FontFactory.GetFont(FontFactory.COURIER, 8, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font Courier9 = FontFactory.GetFont(FontFactory.COURIER, 9, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font Courier10 = FontFactory.GetFont(FontFactory.COURIER, 10, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font Courier11 = FontFactory.GetFont(FontFactory.COURIER, 11, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font Courier12 = FontFactory.GetFont(FontFactory.COURIER, 12, iTextSharp.text.Font.NORMAL);

//                #endregion

//                // ESPACIO 1
//                PdfDiv divEspacio = new PdfDiv();
//                divEspacio.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
//                divEspacio.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
//                divEspacio.Height = 10;
//                divEspacio.Width = 130;



//                //ESPACIO2
//                PdfDiv divEspacio2 = new PdfDiv();
//                divEspacio2.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
//                divEspacio2.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
//                divEspacio2.Height = 2;
//                divEspacio2.Width = 130;


//                //ZONA ENCABEZADO
//                #region DimensionamientoTablaEncabezado
//                PdfPTable TblEncabezadoFactura = new PdfPTable(5);

//                //Dimensiones de tabla.
//                float[] DimensionEncabezado = new float[5];
//                DimensionEncabezado[0] = 1.3F;//
//                DimensionEncabezado[1] = 1.8F;//
//                DimensionEncabezado[2] = 1.0F;//
//                DimensionEncabezado[3] = 0.8F;//
//                DimensionEncabezado[4] = 2.2F;//

//                TblEncabezadoFactura.WidthPercentage = 95;
//                TblEncabezadoFactura.HorizontalAlignment = Element.ALIGN_CENTER;

//                TblEncabezadoFactura.SetWidths(DimensionEncabezado);
//                #endregion

//                //CELDA ENCABEZADO FACTURA
//                #region EncabezadoFactura
//                PdfPCell CelLogoFactura = new PdfPCell()
//                {
//                    Border = 0,
//                    HorizontalAlignment = Element.ALIGN_LEFT,
//                };


//                string HeaderDoc = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";
//                // var HeaderDoc = $@"{AppDomain.CurrentDomain.BaseDirectory}\Logo_" + NIT + ".png";

//                iTextSharp.text.Image ITHeaderDoc = iTextSharp.text.Image.GetInstance(HeaderDoc);
//                ITHeaderDoc.ScaleAbsoluteWidth(100);
//                ITHeaderDoc.ScaleAbsoluteHeight(110);
//                ITHeaderDoc.Alignment = Element.ALIGN_LEFT;

//                CelLogoFactura.AddElement(ITHeaderDoc);

//                TblEncabezadoFactura.AddCell(CelLogoFactura);

//                PdfPCell CelInfoCia = new PdfPCell()
//                {
//                    BorderColor = BaseColor.WHITE,
//                    HorizontalAlignment = Element.ALIGN_RIGHT,
//                    Colspan = 3
//                };


//                Paragraph phCompanyName = new Paragraph(DsInvoiceAR.Tables["Company"].Rows[0]["Name"].ToString(), ArialBold18);
//                Paragraph phNIT = new Paragraph("NIT: " + DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"].ToString(), Helvetica12);
//                Paragraph phRegimenComun = new Paragraph("IVA REGIMEN COMÚN", Helvetica12);


//                phCompanyName.Alignment = Element.ALIGN_LEFT;
//                phNIT.Alignment = Element.ALIGN_LEFT;
//                phRegimenComun.Alignment = Element.ALIGN_LEFT;

//                CelInfoCia.AddElement(phCompanyName);
//                CelInfoCia.AddElement(phNIT);
//                CelInfoCia.AddElement(phRegimenComun);


//                TblEncabezadoFactura.AddCell(CelInfoCia);



//                Paragraph phAddress = new Paragraph(DsInvoiceAR.Tables["Company"].Rows[0]["Address1"].ToString(), Helvetica8);
//                Paragraph phPBX = new Paragraph("PBX: " + DsInvoiceAR.Tables["Company"].Rows[0]["PhoneNum"].ToString(), Helvetica8);
//                //  Paragraph phEmailAddress = new Paragraph("e-mail: "+DsInvoiceAR.Tables["Company"].Rows[0]["EmailAddress"].ToString(), Helvetica8);
//                Paragraph phCityState = new Paragraph(DsInvoiceAR.Tables["Company"].Rows[0]["City"].ToString() + " - Colombia", Helvetica8);

//                phAddress.Alignment = Element.ALIGN_CENTER;
//                phPBX.Alignment = Element.ALIGN_CENTER;
//                phCityState.Alignment = Element.ALIGN_CENTER;



//                PdfPCell CelInfoCIA2 = new PdfPCell()
//                {
//                    Border = 0,
//                    HorizontalAlignment = Element.ALIGN_RIGHT,
//                };


//                CelInfoCIA2.AddElement(phAddress);
//                CelInfoCIA2.AddElement(phPBX);
//                //CelInfoCIA2.AddElement(phEmailAddress);
//                CelInfoCIA2.AddElement(phCityState);


//                //Celda que Contiene TITULO FACTURA DE VENTA
//                PdfPTable tblFacturaVentaTitle = new PdfPTable(1);
//                Paragraph phFActuraVenta = new Paragraph("NOTA CREDITO", ArialBold12);
//                phFActuraVenta.Alignment = Element.ALIGN_CENTER;
//                tblFacturaVentaTitle.AddCell(new PdfPCell() { Border = 0, BackgroundColor = BaseColor.LIGHT_GRAY, Phrase = phFActuraVenta });

//                //TITULO NUMERO DE FACTURA
//                CelInfoCIA2.AddElement(tblFacturaVentaTitle);


//                //Numero Legal Value
//                Paragraph PHNumeroLegal = new Paragraph("No. " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"].ToString(), ArialBold12);
//                PHNumeroLegal.Alignment = Element.ALIGN_CENTER;
//                CelInfoCIA2.AddElement(PHNumeroLegal);

//                //CELDA INFO CIA Y NUMERO DE FACTURA
//                TblEncabezadoFactura.AddCell(CelInfoCIA2);

//                TblEncabezadoFactura.AddCell(new PdfPCell() { Colspan = 5, Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, Phrase = new Phrase("Representación grafica factura electrónica.        -        CUFE: " + CUFE, Helvetica5) });


//                //RESOLUCION
//                TblEncabezadoFactura.AddCell(new PdfPCell() { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, Colspan = 5, Phrase = new Phrase(DsInvoiceAR.Tables["Company"].Rows[0]["ResolucionDian_c"].ToString(), Helvetica5) });

//                #endregion

//                #region INFO CLIENTE


//                PdfPCell CeldaTitlesCliente = new PdfPCell() { Colspan = 2, HorizontalAlignment = Element.ALIGN_LEFT };

//                Phrase phCliente = new Phrase();
//                Chunk chCliente = new Chunk("CLIENTE:          ", ArialBold10);
//                Chunk chClienteValue = new Chunk(DsInvoiceAR.Tables["Customer"].Rows[0]["Name"].ToString(), Helvetica10);
//                phCliente.Add(chCliente);
//                phCliente.Add(chClienteValue);


//                Phrase phNITCliente = new Phrase();
//                Chunk chNIT = new Chunk("NIT:                   ", ArialBold10);
//                Chunk chNITValue = new Chunk(DsInvoiceAR.Tables["Customer"].Rows[0]["ResaleID"].ToString(), Helvetica10);
//                phNITCliente.Add(chNIT);
//                phNITCliente.Add(chNITValue);

//                Phrase phAddressCliente = new Phrase();
//                Chunk chAddress = new Chunk("DIRECCIÓN:     ", ArialBold10);
//                Chunk chAddressValue = new Chunk(DsInvoiceAR.Tables["Customer"].Rows[0]["Address1"].ToString(), Helvetica10);
//                phAddressCliente.Add(chAddress);
//                phAddressCliente.Add(chAddressValue);


//                Phrase phPhone = new Phrase();
//                Chunk chPhone = new Chunk("TELEFÓNO:      ", ArialBold10);
//                Chunk chPhoneValue = new Chunk(DsInvoiceAR.Tables["Customer"].Rows[0]["PhoneNum"].ToString(), Helvetica10);
//                phPhone.Add(chPhone);
//                phPhone.Add(chPhoneValue);


//                Phrase phCity = new Phrase();
//                Chunk chCity = new Chunk("CIUDAD:           ", ArialBold10);
//                Chunk chCityValue = new Chunk(DsInvoiceAR.Tables["Customer"].Rows[0]["City"].ToString() + " " + DsInvoiceAR.Tables["Customer"].Rows[0]["State"].ToString() + " " + DsInvoiceAR.Tables["Customer"].Rows[0]["Country"].ToString(), Helvetica10);
//                phCity.Add(chCity);
//                phCity.Add(chCityValue);

//                CeldaTitlesCliente.AddElement(phCliente);
//                CeldaTitlesCliente.AddElement(phNITCliente);
//                CeldaTitlesCliente.AddElement(phAddressCliente);
//                CeldaTitlesCliente.AddElement(phPhone);
//                CeldaTitlesCliente.AddElement(phCity);

//                #endregion

//                #region InfoFactura2 (Fechas)

//                TblEncabezadoFactura.AddCell(CeldaTitlesCliente);
//                PdfPTable tblInfoFactura2 = new PdfPTable(3);
//                float[] DimensionInfoFactura2 = new float[3];
//                DimensionInfoFactura2[0] = 1.5F;//
//                DimensionInfoFactura2[1] = 1.0F;//
//                DimensionInfoFactura2[2] = 1.0F;//
//                tblInfoFactura2.WidthPercentage = 100;
//                tblInfoFactura2.HorizontalAlignment = Element.ALIGN_CENTER;


//                tblInfoFactura2.AddCell(new PdfPCell() { Phrase = new Phrase("FECHA NOTA CREDITO", ArialBold9) });
//                tblInfoFactura2.AddCell(new PdfPCell() { Phrase = new Phrase("PLAZO", ArialBold9) });
//                tblInfoFactura2.AddCell(new PdfPCell() { Phrase = new Phrase("VENCIMIENTO", ArialBold9) });

//                var FechaFact = Convert.ToDateTime(DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).Day.ToString() + "-" + CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(Convert.ToInt32(Convert.ToDateTime(DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).Month.ToString())) + "-" + Convert.ToDateTime(DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).Year.ToString();
//                var DueDateFact = Convert.ToDateTime(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"]).Day.ToString() + "-" + CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(Convert.ToInt32(Convert.ToDateTime(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"]).Month.ToString())) + "-" + Convert.ToDateTime(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"]).Year.ToString();

//                tblInfoFactura2.AddCell(new PdfPCell() { Phrase = new Phrase(FechaFact, Helvetica9) });
//                tblInfoFactura2.AddCell(new PdfPCell() { Phrase = new Phrase(DsInvoiceAR.Tables["InvcHead"].Rows[0]["TermsCodeDescription"].ToString(), Helvetica9) });
//                tblInfoFactura2.AddCell(new PdfPCell() { Phrase = new Phrase(DueDateFact, Helvetica9) });


//                tblInfoFactura2.AddCell(new PdfPCell() { HorizontalAlignment = Element.ALIGN_CENTER, Colspan = 3, Phrase = new Phrase("ORDEN DE COMPRA", ArialBold9) });

//                tblInfoFactura2.AddCell(new PdfPCell() { HorizontalAlignment = Element.ALIGN_CENTER, Colspan = 3, Phrase = new Phrase(DsInvoiceAR.Tables["InvcHead"].Rows[0]["PONum"].ToString(), Helvetica9) });

//                tblInfoFactura2.AddCell(new PdfPCell() { HorizontalAlignment = Element.ALIGN_LEFT, Phrase = new Phrase("VENDEDOR", ArialBold9) });

//                tblInfoFactura2.AddCell(new PdfPCell() { Colspan = 2, HorizontalAlignment = Element.ALIGN_LEFT, Phrase = new Phrase(DsInvoiceAR.Tables["InvcHead"].Rows[0]["SalesRepName1"].ToString(), Helvetica9) });


//                TblEncabezadoFactura.AddCell(new PdfPCell(tblInfoFactura2) { Colspan = 3, Border = 0 });

//                #endregion

//                TblEncabezadoFactura.AddCell(new PdfPCell() { HorizontalAlignment = Element.ALIGN_RIGHT, Colspan = 5, Phrase = new Phrase("Moneda: " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["CurrencyCode"].ToString(), Helvetica6) });



//                PdfPTable tblLineasFactura = new PdfPTable(6);
//                float[] DimensionesLineasFactura = new float[6];
//                DimensionesLineasFactura[0] = 1.5F;//
//                DimensionesLineasFactura[1] = 1.5F;//
//                DimensionesLineasFactura[2] = 0.8F;//
//                DimensionesLineasFactura[3] = 1.0F;//
//                DimensionesLineasFactura[4] = 1.0F;//
//                DimensionesLineasFactura[5] = 1.0F;//
//                tblLineasFactura.WidthPercentage = 95;
//                tblLineasFactura.HorizontalAlignment = Element.ALIGN_CENTER;
//                tblLineasFactura.SetWidths(DimensionesLineasFactura);


//                PdfPCell CelTituloDesc = new PdfPCell(new Phrase(
//                    "DESCRIPCIÓN", ArialBold8))
//                {
//                    Colspan = 2,
//                    BorderColor = BaseColor.BLACK,
//                    Border = PdfPCell.LEFT_BORDER | PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER,
//                    HorizontalAlignment = Element.ALIGN_LEFT
//                };

//                tblLineasFactura.AddCell(CelTituloDesc);

//                PdfPCell CelTituloCantidad = new PdfPCell(new Phrase(
//                    "CANTIDAD", ArialBold8))
//                {
//                    Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER,
//                    BorderColor = BaseColor.BLACK,
//                    HorizontalAlignment = Element.ALIGN_RIGHT
//                };

//                tblLineasFactura.AddCell(CelTituloCantidad);

//                PdfPCell CelTituloPrecio = new PdfPCell(new Phrase(
//                    "PRECIO", ArialBold8))
//                {
//                    BorderColor = BaseColor.BLACK,
//                    Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER,
//                    HorizontalAlignment = Element.ALIGN_RIGHT
//                };

//                tblLineasFactura.AddCell(CelTituloPrecio);

//                PdfPCell CelTitulosubtotal = new PdfPCell(new Phrase(
//                    "SUBTOTAL", ArialBold8))
//                {
//                    BorderColor = BaseColor.BLACK,
//                    Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER,
//                    HorizontalAlignment = Element.ALIGN_RIGHT
//                };

//                tblLineasFactura.AddCell(CelTitulosubtotal);

//                PdfPCell CelTituloDcto = new PdfPCell(new Phrase(
//                    "DESCUENTO", ArialBold8))
//                {
//                    BorderColor = BaseColor.BLACK,
//                    Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER | PdfPCell.RIGHT_BORDER,
//                    HorizontalAlignment = Element.ALIGN_RIGHT
//                };
//                tblLineasFactura.AddCell(CelTituloDcto);




//                //-------------------------------------------------------------------------------

//                PdfPTable tableValUnidades = new PdfPTable(DimensionesLineasFactura);
//                tableValUnidades.WidthPercentage = 95;
//                tableValUnidades.HorizontalAlignment = Element.ALIGN_CENTER;

//                decimal totalDescuento = 0, totalSubtotal = 0;
//                double totalCantidad = 0;
//                foreach (DataRow InvoiceLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
//                {
//                    totalCantidad += Convert.ToDouble(InvoiceLine["SellingShipQty"]);
//                    AddUnidadesFacturaNacionalTecnoFuego(InvoiceLine, ref tableValUnidades, Arial7);
//                    totalDescuento += decimal.Parse((string)InvoiceLine["DocDiscount"]);
//                    totalSubtotal += decimal.Parse((string)InvoiceLine["DspDocExtPrice"]);
//                }

//                var cantidadFilasAdd = 20 - DsInvoiceAR.Tables["InvcDtl"].Rows.Count;
//                for (int i = 0; i <= cantidadFilasAdd; i++)
//                {
//                    if (i == cantidadFilasAdd)
//                    {
//                        tableValUnidades.AddCell(new PdfPCell()
//                        {
//                            Colspan = 6,
//                            Border = PdfPCell.LEFT_BORDER | PdfPCell.RIGHT_BORDER
//                        });
//                    }
//                    else
//                    {
//                        tableValUnidades.AddCell(new PdfPCell(new Phrase(" "))
//                        {
//                            Colspan = 6,
//                            //BackgroundColor=BaseColor.LIGHT_GRAY,
//                            Border = PdfPCell.LEFT_BORDER | PdfPCell.RIGHT_BORDER,
//                        });
//                    }
//                }

//                document.Add(divEspacio2);



//                PdfPTable tblFindDcto = new PdfPTable(4);
//                float[] DimensionesFinDctoFactura = new float[4];
//                DimensionesFinDctoFactura[0] = 1.5F;//
//                DimensionesFinDctoFactura[1] = 1.5F;//
//                DimensionesFinDctoFactura[2] = 0.8F;//
//                DimensionesFinDctoFactura[3] = 0.8F;//
//                tblFindDcto.WidthPercentage = 95;
//                tblFindDcto.HorizontalAlignment = Element.ALIGN_CENTER;
//                tblFindDcto.SetWidths(DimensionesFinDctoFactura);


//                #region OBSERVACIONES
//                Chunk chObservaciones = new Chunk("OBSERVACIONES:   ", ArialBold9);
//                Chunk chObservacionesVal = new Chunk(DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceComment"].ToString(), Helvetica9);
//                Phrase phObservaciones = new Phrase();
//                phObservaciones.Add(chObservaciones);
//                phObservaciones.Add(chObservacionesVal);

//                tblFindDcto.AddCell(new PdfPCell() { Colspan = 2, Phrase = phObservaciones });

//                #endregion

//                #region TOTALESFACTURA

//                PdfPTable tblTotalesFactura = new PdfPTable(2);
//                float[] DimensionestotalesFactura = new float[2];
//                DimensionestotalesFactura[0] = 1.0F;//
//                DimensionestotalesFactura[1] = 1.0F;//
//                tblTotalesFactura.WidthPercentage = 95;
//                tblTotalesFactura.HorizontalAlignment = Element.ALIGN_CENTER;
//                tblTotalesFactura.SetWidths(DimensionestotalesFactura);

//                tblTotalesFactura.AddCell(new PdfPCell() { Phrase = new Phrase("VALOR", ArialBold9) });
//                tblTotalesFactura.AddCell(new PdfPCell() { HorizontalAlignment = Element.ALIGN_RIGHT, Phrase = new Phrase(Convert.ToDecimal(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubTotal"]).ToString("C0"), Helvetica9) });

//                tblTotalesFactura.AddCell(new PdfPCell() { Phrase = new Phrase("DESCUENTO", ArialBold9) });
//                tblTotalesFactura.AddCell(new PdfPCell() { HorizontalAlignment = Element.ALIGN_RIGHT, Phrase = new Phrase(totalDescuento.ToString("C0"), Helvetica9) });

//                tblTotalesFactura.AddCell(new PdfPCell() { Phrase = new Phrase("IVA", ArialBold9) });
//                tblTotalesFactura.AddCell(new PdfPCell() { HorizontalAlignment = Element.ALIGN_RIGHT, Phrase = new Phrase(Convert.ToDecimal(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DocTaxAmt"]).ToString("C0"), Helvetica9) });

//                tblFindDcto.AddCell(new PdfPCell(tblTotalesFactura) { Colspan = 2 });
//                #endregion

//                #region REMISION
//                Chunk chRemision = new Chunk("REMISIÓN:   ", ArialBold9);
//                Chunk chRemisionVal = new Chunk(DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceComment"].ToString(), Helvetica9);
//                Phrase phremosion = new Phrase();
//                phremosion.Add(chRemision);
//                phremosion.Add(chRemisionVal);

//                tblFindDcto.AddCell(new PdfPCell() { Colspan = 2, Phrase = phremosion });
//                #endregion

//                #region SUBTOTAL TF

//                PdfPTable tblsubTotalTF = new PdfPTable(2);
//                float[] DimensionessubTotalTF = new float[2];
//                DimensionessubTotalTF[0] = 1.0F;//
//                DimensionessubTotalTF[1] = 1.0F;//
//                tblsubTotalTF.WidthPercentage = 95;
//                tblsubTotalTF.HorizontalAlignment = Element.ALIGN_CENTER;
//                tblsubTotalTF.SetWidths(DimensionestotalesFactura);

//                tblsubTotalTF.AddCell(new PdfPCell() { Phrase = new Phrase("SUBTOTAL", ArialBold9) });
//                tblsubTotalTF.AddCell(new PdfPCell() { HorizontalAlignment = Element.ALIGN_RIGHT, Phrase = new Phrase(Convert.ToDecimal(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DocSubTotal"]).ToString("C0"), Helvetica9) });

//                tblsubTotalTF.AddCell(new PdfPCell() { Phrase = new Phrase("(-) TF", ArialBold9) });
//                tblsubTotalTF.AddCell(new PdfPCell() { HorizontalAlignment = Element.ALIGN_RIGHT, Phrase = new Phrase(totalDescuento.ToString("C0"), Helvetica9) });

//                tblFindDcto.AddCell(new PdfPCell(tblsubTotalTF) { Colspan = 2 });
//                #endregion

//                #region VALORENLETRAS
//                Chunk chValorLetras = new Chunk("VALOR EN LETRAS:   ", ArialBold9);
//                Chunk chValorLetrasVal = new Chunk(Nroenletras(Math.Abs(Convert.ToDecimal(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DocInvoiceAmt"])).ToString()), ArialBold9);
//                Phrase phValorLetras = new Phrase();
//                phValorLetras.Add(chValorLetras);
//                phValorLetras.Add(chValorLetrasVal);
//                tblFindDcto.AddCell(new PdfPCell() { Colspan = 2, Phrase = phValorLetras });


//                Phrase phTotal = new Phrase("TOTAL:                       " + Convert.ToDecimal(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DocInvoiceAmt"]).ToString("C0"), ArialBold9);
//                tblFindDcto.AddCell(new PdfPCell() { Colspan = 2, Phrase = phTotal });

//                #endregion


//                #region FIRMAS

//                Paragraph phGerente = new Paragraph("\n \n \n \n \n \n ING. ARTURO CASTILLO PEREZ \n GERENTE", Helvetica9);
//                phGerente.Alignment = Element.ALIGN_CENTER;



//                tblFindDcto.AddCell(new PdfPCell() { PaddingTop = 4, Phrase = new Phrase("Sirvase Realizar consignacion así: BANCOLOMBIA CTA CTE No. 4753116104 - 5 Suc.Murillo, BARRANQUILLA.", Helvetica6) });
//                tblFindDcto.AddCell(new PdfPCell() { HorizontalAlignment = Element.ALIGN_CENTER, Phrase = phGerente });
//                PdfPCell CellFirma = new PdfPCell() { Colspan = 2 };
//                Paragraph phAceptacion = new Paragraph("Acepto el contenido de la presente factura y hago constar el recibo conforme en ella discriminada", Helvetica5);
//                phAceptacion.Alignment = Element.ALIGN_CENTER;

//                Paragraph phFimaYSello = new Paragraph("\n \n\n \n ______________________________________________________\n Firma y Sello del Cliente", Helvetica6);
//                phFimaYSello.Alignment = Element.ALIGN_CENTER;

//                CellFirma.AddElement(phAceptacion);
//                CellFirma.AddElement(phFimaYSello);
//                tblFindDcto.AddCell(CellFirma);


//                tblFindDcto.AddCell(new PdfPCell() { Border = 0, Phrase = new Phrase("Epicor.ERP 10267", ArialBold6) });
//                tblFindDcto.AddCell(new PdfPCell() { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, Phrase = new Phrase("\n www.tecno-fuego.com.co", ArialBold6) });
//                tblFindDcto.AddCell(new PdfPCell() { HorizontalAlignment = Element.ALIGN_CENTER, Border = 0, Colspan = 2, Phrase = new Phrase("O.V: 0", Helvetica7) });

//                // tblFindDcto.AddCell(new PdfPCell() { HorizontalAlignment = Element.ALIGN_CENTER, Border = 0, Colspan = 4, Phrase = new Phrase("ESTA FACTURA DE VENTA ES UN TITULO VALOR EN CUANTO CUMPLE CON LOS REQUERIMIENTOS EXIGIDOS POR LA LEY 1231 / 08.", Helvetica7) });


//                #endregion


//                document.Add(TblEncabezadoFactura);
//                document.Add(tblLineasFactura);
//                document.Add(tableValUnidades);
//                document.Add(tblFindDcto);

//                /*PIE DE PAGINA*/
//                PdfContentByte pCb = writer.DirectContent;
//                // PieDePagina(ref pCb);

//                writer.Flush();
//                document.Close();

//                //AddFooterDocMercaLlantas(Ruta + NomArchivo, 180, 180);
//                RutaPdf = NomArchivo;
//                return true;
//            }
//            catch (Exception ex)
//            {
//                strError = ex.ToString();
//                //MessageBox.Show(ex.ToString());
//                return false;
//            }
//        }

//        public bool NotaCreditoTecnoFuegoMalo(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
//        {
//            //NomArchivo = string.Empty;
//            //string Ruta = @"C:\Users\INGJORGEC\source\repos\Reporte_iTextSharp\Reporte_iTextSharp\bin\Debug\PDF\900665411\";
//            //string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
//            //NomArchivo = NombreInvoice + ".pdf";

//            NomArchivo = string.Empty;
//            string Ruta = HttpContext.Current.Server.MapPath("~/PDF/" + NIT + "/");
//            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
//            string RutaImg = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";
//            NomArchivo = NombreInvoice + ".pdf";

//            try
//            {


//                //Validamos Existencia del Directorio.
//                if (!System.IO.Directory.Exists(Ruta))
//                    System.IO.Directory.CreateDirectory(Ruta);

//                //Valido la existencia previa de este archivo.
//                if (System.IO.File.Exists(Ruta + NomArchivo))
//                    System.IO.File.Delete(Ruta + NomArchivo);

//                //Dimenciones del documento.
//                Document document = new Document(iTextSharp.text.PageSize.LETTER, 0, 0, 0, 0);

//                if (DsInvoiceAR.Tables["InvcDtl"].Rows.Count < 30)
//                {
//                    document = new Document(iTextSharp.text.PageSize.LETTER, 0, 0, 0, 125);
//                }
//                //  strError += @"Linea 18533";
//                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));

//                Paragraph separator = new Paragraph("\n");
//                separator.Alignment = Element.ALIGN_CENTER;

//                //  writer.PageEvent = new EventPageTecnoFuego(ref strError, DsInvoiceAR, CUFE, NIT, QRInvoice);

//                // step 3: we open the document     
//                document.Open();



//                #region FUENTES
//                //Arial
//                iTextSharp.text.Font Arial4 = FontFactory.GetFont("Arial", 4, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font Arial5 = FontFactory.GetFont("Arial", 5, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font Arial6 = FontFactory.GetFont("Arial", 6, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font Arial7 = FontFactory.GetFont("Arial", 7, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font Arial8 = FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font Arial9 = FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font Arial10 = FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font Arial11 = FontFactory.GetFont("Arial", 11, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font Arial12 = FontFactory.GetFont("Arial", 12, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font Arial14 = FontFactory.GetFont("Arial", 14, iTextSharp.text.Font.NORMAL);


//                //Arialbold14
//                iTextSharp.text.Font ArialBold4 = FontFactory.GetFont("Arial", 4, iTextSharp.text.Font.BOLD);
//                iTextSharp.text.Font ArialBold5 = FontFactory.GetFont("Arial", 5, iTextSharp.text.Font.BOLD);
//                iTextSharp.text.Font ArialBold6 = FontFactory.GetFont("Arial", 6, iTextSharp.text.Font.BOLD);
//                iTextSharp.text.Font ArialBold7 = FontFactory.GetFont("Arial", 7, iTextSharp.text.Font.BOLD);
//                iTextSharp.text.Font ArialBold8 = FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD);
//                iTextSharp.text.Font ArialBold9 = FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD);
//                iTextSharp.text.Font ArialBold10 = FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD);
//                iTextSharp.text.Font ArialBold11 = FontFactory.GetFont("Arial", 11, iTextSharp.text.Font.BOLD);
//                iTextSharp.text.Font ArialBold12 = FontFactory.GetFont("Arial", 12, iTextSharp.text.Font.BOLD);
//                iTextSharp.text.Font ArialBold14 = FontFactory.GetFont("Arial", 14, iTextSharp.text.Font.BOLD);
//                iTextSharp.text.Font ArialBold18 = FontFactory.GetFont("Arial", 18, iTextSharp.text.Font.BOLD);

//                //HELVETICA
//                iTextSharp.text.Font Helvetica4 = FontFactory.GetFont(FontFactory.HELVETICA, 4, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font Helvetica5 = FontFactory.GetFont(FontFactory.HELVETICA, 5, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font Helvetica6 = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font Helvetica7 = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font Helvetica8 = FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font Helvetica9 = FontFactory.GetFont(FontFactory.HELVETICA, 9, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font Helvetica10 = FontFactory.GetFont(FontFactory.HELVETICA, 10, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font Helvetica11 = FontFactory.GetFont(FontFactory.HELVETICA, 11, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font Helvetica12 = FontFactory.GetFont(FontFactory.HELVETICA, 12, iTextSharp.text.Font.NORMAL);


//                //HELVETICA BOLD
//                iTextSharp.text.Font HelveticBOLD4 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 4, iTextSharp.text.Font.BOLD);
//                iTextSharp.text.Font HelveticaBOLD5 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 5, iTextSharp.text.Font.BOLD);
//                iTextSharp.text.Font HelveticaBOLD6 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 6, iTextSharp.text.Font.BOLD);
//                iTextSharp.text.Font HelveticaBOLD7 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.BOLD);
//                iTextSharp.text.Font HelveticaBOLD8 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 8, iTextSharp.text.Font.BOLD);
//                iTextSharp.text.Font HelveticaBOLD9 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 9, iTextSharp.text.Font.BOLD);
//                iTextSharp.text.Font HelveticaBOLD10 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 10, iTextSharp.text.Font.BOLD);
//                iTextSharp.text.Font HelveticaBOLD11 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 11, iTextSharp.text.Font.BOLD);
//                iTextSharp.text.Font HelveticaBOLD12 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 12, iTextSharp.text.Font.BOLD);


//                //COURIER
//                iTextSharp.text.Font Courier4 = FontFactory.GetFont(FontFactory.COURIER, 4, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font Courier6 = FontFactory.GetFont(FontFactory.COURIER, 6, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font Courier5 = FontFactory.GetFont(FontFactory.COURIER, 5, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font Courier7 = FontFactory.GetFont(FontFactory.COURIER, 7, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font Courier8 = FontFactory.GetFont(FontFactory.COURIER, 8, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font Courier9 = FontFactory.GetFont(FontFactory.COURIER, 9, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font Courier10 = FontFactory.GetFont(FontFactory.COURIER, 10, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font Courier11 = FontFactory.GetFont(FontFactory.COURIER, 11, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font Courier12 = FontFactory.GetFont(FontFactory.COURIER, 12, iTextSharp.text.Font.NORMAL);

//                #endregion

//                // ESPACIO 1
//                PdfDiv divEspacio = new PdfDiv();
//                divEspacio.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
//                divEspacio.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
//                divEspacio.Height = 10;
//                divEspacio.Width = 130;
//                strError += @"Linea 18612
//";


//                //ESPACIO2
//                PdfDiv divEspacio2 = new PdfDiv();
//                divEspacio2.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
//                divEspacio2.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
//                divEspacio2.Height = 2;
//                divEspacio2.Width = 130;


//                //ZONA ENCABEZADO
//                #region DimensionamientoTablaEncabezado
//                PdfPTable TblEncabezadoFactura = new PdfPTable(5);

//                //Dimensiones de tabla.
//                float[] DimensionEncabezado = new float[5];
//                DimensionEncabezado[0] = 1.3F;//
//                DimensionEncabezado[1] = 1.8F;//
//                DimensionEncabezado[2] = 1.0F;//
//                DimensionEncabezado[3] = 0.8F;//
//                DimensionEncabezado[4] = 2.2F;//

//                TblEncabezadoFactura.WidthPercentage = 95;
//                TblEncabezadoFactura.HorizontalAlignment = Element.ALIGN_CENTER;

//                TblEncabezadoFactura.SetWidths(DimensionEncabezado);
//                #endregion

//                strError += @"Linea 18642
//";


//                //CELDA ENCABEZADO FACTURA
//                #region EncabezadoFactura
//                PdfPCell CelLogoFactura = new PdfPCell()
//                {
//                    Border = 0,
//                    HorizontalAlignment = Element.ALIGN_LEFT,
//                };


//                string HeaderDoc = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";
//                // var HeaderDoc = $@"{AppDomain.CurrentDomain.BaseDirectory}\Logo_" + NIT + ".png";

//                iTextSharp.text.Image ITHeaderDoc = iTextSharp.text.Image.GetInstance(HeaderDoc);
//                ITHeaderDoc.ScaleAbsoluteWidth(100);
//                ITHeaderDoc.ScaleAbsoluteHeight(110);
//                ITHeaderDoc.Alignment = Element.ALIGN_LEFT;

//                CelLogoFactura.AddElement(ITHeaderDoc);

//                TblEncabezadoFactura.AddCell(CelLogoFactura);

//                PdfPCell CelInfoCia = new PdfPCell()
//                {
//                    BorderColor = BaseColor.WHITE,
//                    HorizontalAlignment = Element.ALIGN_RIGHT,
//                    Colspan = 3
//                };


//                Paragraph phCompanyName = new Paragraph(DsInvoiceAR.Tables["Company"].Rows[0]["Name"].ToString(), ArialBold18);
//                Paragraph phNIT = new Paragraph("NIT: " + DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"].ToString(), Helvetica12);
//                Paragraph phRegimenComun = new Paragraph("IVA REGIMEN COMÚN", Helvetica12);


//                phCompanyName.Alignment = Element.ALIGN_LEFT;
//                phNIT.Alignment = Element.ALIGN_LEFT;
//                phRegimenComun.Alignment = Element.ALIGN_LEFT;

//                CelInfoCia.AddElement(phCompanyName);
//                CelInfoCia.AddElement(phNIT);
//                CelInfoCia.AddElement(phRegimenComun);


//                TblEncabezadoFactura.AddCell(CelInfoCia);



//                Paragraph phAddress = new Paragraph(DsInvoiceAR.Tables["Company"].Rows[0]["Address1"].ToString(), Helvetica8);
//                Paragraph phPBX = new Paragraph("PBX: " + DsInvoiceAR.Tables["Company"].Rows[0]["PhoneNum"].ToString(), Helvetica8);
//                //  Paragraph phEmailAddress = new Paragraph("e-mail: "+DsInvoiceAR.Tables["Company"].Rows[0]["EmailAddress"].ToString(), Helvetica8);
//                Paragraph phCityState = new Paragraph(DsInvoiceAR.Tables["Company"].Rows[0]["City"].ToString() + " - Colombia", Helvetica8);

//                phAddress.Alignment = Element.ALIGN_CENTER;
//                phPBX.Alignment = Element.ALIGN_CENTER;
//                phCityState.Alignment = Element.ALIGN_CENTER;



//                PdfPCell CelInfoCIA2 = new PdfPCell()
//                {
//                    Border = 0,
//                    HorizontalAlignment = Element.ALIGN_RIGHT,
//                };


//                CelInfoCIA2.AddElement(phAddress);
//                CelInfoCIA2.AddElement(phPBX);
//                //CelInfoCIA2.AddElement(phEmailAddress);
//                CelInfoCIA2.AddElement(phCityState);


//                //Celda que contiene TITULO FACTURA DE VENTA
//                PdfPTable tblFacturaVentaTitle = new PdfPTable(1);
//                Paragraph phFActuraVenta = new Paragraph("NOTA CREDITO", ArialBold12);
//                phFActuraVenta.Alignment = Element.ALIGN_CENTER;
//                tblFacturaVentaTitle.AddCell(new PdfPCell() { Border = 0, BackgroundColor = BaseColor.LIGHT_GRAY, Phrase = phFActuraVenta });

//                //TITULO NUMERO DE FACTURA
//                CelInfoCIA2.AddElement(tblFacturaVentaTitle);


//                //Numero Legal Value
//                Paragraph PHNumeroLegal = new Paragraph("No. " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"].ToString(), ArialBold12);
//                PHNumeroLegal.Alignment = Element.ALIGN_CENTER;
//                CelInfoCIA2.AddElement(PHNumeroLegal);

//                //CELDA INFO CIA Y NUMERO DE FACTURA
//                TblEncabezadoFactura.AddCell(CelInfoCIA2);

//                TblEncabezadoFactura.AddCell(new PdfPCell() { Colspan = 5, Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, Phrase = new Phrase("Representación grafica factura electrónica.        -        CUFE: " + CUFE, Helvetica5) });

//                //RESOLUCION
//                TblEncabezadoFactura.AddCell(new PdfPCell() { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, Colspan = 5, Phrase = new Phrase("SOMOS GRANDES CONTRIBUYENTES, RESOLUCION No. 000076 DEL 2016/12/01 RESOLUCION DIAN FACTURACIÓN POR COMPUTADOR: Autorizada Desde 15001 Hasta 17000 Por Resolucion Dian No. 18762000869536 De 2016 / 10 / 19, Vigencia Hasta el 2018 / 04 / 19. ACTIVIDAD ICA BARRANQUILLA CODIGO: 204 MILAJE: 9.6 x 1000", Helvetica5) });

//                #endregion


//                return true;


//                #region INFO CLIENTE


//                PdfPCell CeldaTitlesCliente = new PdfPCell() { Colspan = 2, HorizontalAlignment = Element.ALIGN_LEFT };

//                Phrase phCliente = new Phrase();
//                Chunk chCliente = new Chunk("CLIENTE:          ", ArialBold10);
//                Chunk chClienteValue = new Chunk(DsInvoiceAR.Tables["Customer"].Rows[0]["Name"].ToString(), Helvetica10);
//                phCliente.Add(chCliente);
//                phCliente.Add(chClienteValue);


//                Phrase phNITCliente = new Phrase();
//                Chunk chNIT = new Chunk("NIT:                   ", ArialBold10);
//                Chunk chNITValue = new Chunk(DsInvoiceAR.Tables["Customer"].Rows[0]["ResaleID"].ToString(), Helvetica10);
//                phNITCliente.Add(chNIT);
//                phNITCliente.Add(chNITValue);

//                Phrase phAddressCliente = new Phrase();
//                Chunk chAddress = new Chunk("DIRECCIÓN:     ", ArialBold10);
//                Chunk chAddressValue = new Chunk(DsInvoiceAR.Tables["Customer"].Rows[0]["Address1"].ToString(), Helvetica10);
//                phAddressCliente.Add(chAddress);
//                phAddressCliente.Add(chAddressValue);


//                Phrase phPhone = new Phrase();
//                Chunk chPhone = new Chunk("TELEFÓNO:      ", ArialBold10);
//                Chunk chPhoneValue = new Chunk(DsInvoiceAR.Tables["Customer"].Rows[0]["PhoneNum"].ToString(), Helvetica10);
//                phPhone.Add(chPhone);
//                phPhone.Add(chPhoneValue);


//                Phrase phCity = new Phrase();
//                Chunk chCity = new Chunk("CIUDAD:           ", ArialBold10);
//                Chunk chCityValue = new Chunk(DsInvoiceAR.Tables["Customer"].Rows[0]["City"].ToString() + " " + DsInvoiceAR.Tables["Customer"].Rows[0]["State"].ToString() + " " + DsInvoiceAR.Tables["Customer"].Rows[0]["Country"].ToString(), Helvetica10);
//                phCity.Add(chCity);
//                phCity.Add(chCityValue);

//                CeldaTitlesCliente.AddElement(phCliente);
//                CeldaTitlesCliente.AddElement(phNITCliente);
//                CeldaTitlesCliente.AddElement(phAddressCliente);
//                CeldaTitlesCliente.AddElement(phPhone);
//                CeldaTitlesCliente.AddElement(phCity);

//                #endregion

//                //MessageBox.Show("linea 18786");

//                #region InfoFactura2 (Fechas)

//                TblEncabezadoFactura.AddCell(CeldaTitlesCliente);
//                PdfPTable tblInfoFactura2 = new PdfPTable(3);
//                float[] DimensionInfoFactura2 = new float[3];
//                DimensionInfoFactura2[0] = 1.5F;//
//                DimensionInfoFactura2[1] = 1.0F;//
//                DimensionInfoFactura2[2] = 1.0F;//
//                tblInfoFactura2.WidthPercentage = 100;
//                tblInfoFactura2.HorizontalAlignment = Element.ALIGN_CENTER;


//                tblInfoFactura2.AddCell(new PdfPCell() { Phrase = new Phrase("FECHA FACTURA", ArialBold9) });
//                tblInfoFactura2.AddCell(new PdfPCell() { Phrase = new Phrase("PLAZO", ArialBold9) });
//                tblInfoFactura2.AddCell(new PdfPCell() { Phrase = new Phrase("VENCIMIENTO", ArialBold9) });

//                var FechaFact = Convert.ToDateTime(DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).Day.ToString() + "-" + CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(Convert.ToInt32(Convert.ToDateTime(DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).Month.ToString())) + "-" + Convert.ToDateTime(DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).Year.ToString();
//                var DueDateFact = Convert.ToDateTime(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"]).Day.ToString() + "-" + CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(Convert.ToInt32(Convert.ToDateTime(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"]).Month.ToString())) + "-" + Convert.ToDateTime(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"]).Year.ToString();

//                tblInfoFactura2.AddCell(new PdfPCell() { Phrase = new Phrase(FechaFact, Helvetica9) });
//                tblInfoFactura2.AddCell(new PdfPCell() { Phrase = new Phrase(DsInvoiceAR.Tables["InvcHead"].Rows[0]["TermsCodeDescription"].ToString(), Helvetica9) });
//                tblInfoFactura2.AddCell(new PdfPCell() { Phrase = new Phrase(DueDateFact, Helvetica9) });


//                tblInfoFactura2.AddCell(new PdfPCell() { HorizontalAlignment = Element.ALIGN_CENTER, Colspan = 3, Phrase = new Phrase("ORDEN DE COMPRA", ArialBold9) });

//                tblInfoFactura2.AddCell(new PdfPCell() { HorizontalAlignment = Element.ALIGN_CENTER, Colspan = 3, Phrase = new Phrase(DsInvoiceAR.Tables["InvcHead"].Rows[0]["PONum"].ToString(), Helvetica9) });

//                tblInfoFactura2.AddCell(new PdfPCell() { HorizontalAlignment = Element.ALIGN_LEFT, Phrase = new Phrase("VENDEDOR", ArialBold9) });

//                tblInfoFactura2.AddCell(new PdfPCell() { Colspan = 2, HorizontalAlignment = Element.ALIGN_LEFT, Phrase = new Phrase(DsInvoiceAR.Tables["InvcHead"].Rows[0]["SalesRepName1"].ToString(), Helvetica9) });


//                TblEncabezadoFactura.AddCell(new PdfPCell(tblInfoFactura2) { Colspan = 3, Border = 0 });

//                #endregion

//                TblEncabezadoFactura.AddCell(new PdfPCell() { HorizontalAlignment = Element.ALIGN_RIGHT, Colspan = 5, Phrase = new Phrase("Moneda: " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["CurrencyCode"].ToString(), Helvetica6) });

//                strError += @"Linea 18829
//";

//                PdfPTable tblLineasFactura = new PdfPTable(6);
//                float[] DimensionesLineasFactura = new float[6];
//                DimensionesLineasFactura[0] = 1.5F;//
//                DimensionesLineasFactura[1] = 1.5F;//
//                DimensionesLineasFactura[2] = 0.8F;//
//                DimensionesLineasFactura[3] = 1.0F;//
//                DimensionesLineasFactura[4] = 1.0F;//
//                DimensionesLineasFactura[5] = 1.0F;//
//                tblLineasFactura.WidthPercentage = 95;
//                tblLineasFactura.HorizontalAlignment = Element.ALIGN_CENTER;
//                tblLineasFactura.SetWidths(DimensionesLineasFactura);


//                PdfPCell CelTituloDesc = new PdfPCell(new Phrase(
//                    "DESCRIPCIÓN", ArialBold8))
//                {
//                    Colspan = 2,
//                    BorderColor = BaseColor.BLACK,
//                    Border = PdfPCell.LEFT_BORDER | PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER,
//                    HorizontalAlignment = Element.ALIGN_RIGHT
//                };

//                tblLineasFactura.AddCell(CelTituloDesc);

//                PdfPCell CelTituloCantidad = new PdfPCell(new Phrase(
//                    "CANTIDAD", ArialBold8))
//                {
//                    Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER,
//                    BorderColor = BaseColor.BLACK,
//                    HorizontalAlignment = Element.ALIGN_LEFT
//                };

//                tblLineasFactura.AddCell(CelTituloCantidad);

//                PdfPCell CelTituloPrecio = new PdfPCell(new Phrase(
//                    "PRECIO", ArialBold8))
//                {
//                    BorderColor = BaseColor.BLACK,
//                    Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER,
//                    HorizontalAlignment = Element.ALIGN_LEFT
//                };

//                tblLineasFactura.AddCell(CelTituloPrecio);

//                PdfPCell CelTitulosubtotal = new PdfPCell(new Phrase(
//                    "SUBTOTAL", ArialBold8))
//                {
//                    BorderColor = BaseColor.BLACK,
//                    Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER | PdfPCell.RIGHT_BORDER,
//                    HorizontalAlignment = Element.ALIGN_LEFT
//                };

//                tblLineasFactura.AddCell(CelTitulosubtotal);

//                PdfPCell CelTituloDcto = new PdfPCell(new Phrase(
//                    "DESCUENTO", ArialBold8))
//                {
//                    BorderColor = BaseColor.BLACK,
//                    Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER,
//                    HorizontalAlignment = Element.ALIGN_LEFT
//                };
//                tblLineasFactura.AddCell(CelTituloDcto);


//                strError += @"Linea 18896
//";

//                //-------------------------------------------------------------------------------

//                PdfPTable tableValUnidades = new PdfPTable(DimensionesLineasFactura);
//                tableValUnidades.WidthPercentage = 95;
//                tableValUnidades.HorizontalAlignment = Element.ALIGN_CENTER;

//                decimal totalDescuento = 0, totalSubtotal = 0;
//                double totalCantidad = 0;
//                foreach (DataRow InvoiceLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
//                {
//                    totalCantidad += Convert.ToDouble(InvoiceLine["SellingShipQty"]);
//                    AddUnidadesNotaCreditoTecnoFuego(InvoiceLine, ref tableValUnidades, Arial7);
//                    totalDescuento += decimal.Parse((string)InvoiceLine["DocDiscount"]);
//                    totalSubtotal += decimal.Parse((string)InvoiceLine["DspDocExtPrice"]);
//                }


//                var cantidadFilasAdd = 20 - DsInvoiceAR.Tables["InvcDtl"].Rows.Count;
//                for (int i = 0; i <= cantidadFilasAdd; i++)
//                {
//                    if (i == cantidadFilasAdd)
//                    {
//                        tableValUnidades.AddCell(new PdfPCell()
//                        {
//                            Colspan = 6,
//                            Border = PdfPCell.LEFT_BORDER | PdfPCell.RIGHT_BORDER
//                        });
//                    }
//                    else
//                    {
//                        tableValUnidades.AddCell(new PdfPCell(new Phrase(" "))
//                        {
//                            Colspan = 6,
//                            //BackgroundColor=BaseColor.LIGHT_GRAY,
//                            Border = PdfPCell.LEFT_BORDER | PdfPCell.RIGHT_BORDER,
//                        });
//                    }
//                }
//                document.Add(divEspacio2);



//                PdfPTable tblFindDcto = new PdfPTable(4);
//                float[] DimensionesFinDctoFactura = new float[4];
//                DimensionesFinDctoFactura[0] = 1.5F;//
//                DimensionesFinDctoFactura[1] = 1.5F;//
//                DimensionesFinDctoFactura[2] = 0.8F;//
//                DimensionesFinDctoFactura[3] = 0.8F;//
//                tblFindDcto.WidthPercentage = 95;
//                tblFindDcto.HorizontalAlignment = Element.ALIGN_CENTER;
//                tblFindDcto.SetWidths(DimensionesFinDctoFactura);

//                strError += @"Linea 18951
//";

//                #region OBSERVACIONES
//                Chunk chObservaciones = new Chunk("OBSERVACIONES:   ", ArialBold9);
//                Chunk chObservacionesVal = new Chunk(DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceComment"].ToString(), Helvetica9);
//                Phrase phObservaciones = new Phrase();
//                phObservaciones.Add(chObservaciones);
//                phObservaciones.Add(chObservacionesVal);

//                tblFindDcto.AddCell(new PdfPCell() { Colspan = 2, Phrase = phObservaciones });

//                #endregion

//                #region TOTALESFACTURA

//                PdfPTable tblTotalesFactura = new PdfPTable(2);
//                float[] DimensionestotalesFactura = new float[2];
//                DimensionestotalesFactura[0] = 1.0F;//
//                DimensionestotalesFactura[1] = 1.0F;//
//                tblTotalesFactura.WidthPercentage = 95;
//                tblTotalesFactura.HorizontalAlignment = Element.ALIGN_CENTER;
//                tblTotalesFactura.SetWidths(DimensionestotalesFactura);

//                tblTotalesFactura.AddCell(new PdfPCell() { Phrase = new Phrase("VALOR", ArialBold9) });
//                tblTotalesFactura.AddCell(new PdfPCell() { HorizontalAlignment = Element.ALIGN_RIGHT, Phrase = new Phrase(Convert.ToDecimal(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubTotal"]).ToString("C0"), Helvetica9) });

//                tblTotalesFactura.AddCell(new PdfPCell() { Phrase = new Phrase("DESCUENTO", ArialBold9) });
//                tblTotalesFactura.AddCell(new PdfPCell() { HorizontalAlignment = Element.ALIGN_RIGHT, Phrase = new Phrase(totalDescuento.ToString("C0"), Helvetica9) });

//                tblTotalesFactura.AddCell(new PdfPCell() { Phrase = new Phrase("IVA", ArialBold9) });
//                tblTotalesFactura.AddCell(new PdfPCell() { HorizontalAlignment = Element.ALIGN_RIGHT, Phrase = new Phrase(Convert.ToDecimal(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DocTaxAmt"]).ToString("C0"), Helvetica9) });

//                tblFindDcto.AddCell(new PdfPCell(tblTotalesFactura) { Colspan = 2 });
//                #endregion

//                #region REMISION
//                Chunk chRemision = new Chunk("REMISIÓN:   ", ArialBold9);
//                Chunk chRemisionVal = new Chunk(DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceComment"].ToString(), Helvetica9);
//                Phrase phremosion = new Phrase();
//                phremosion.Add(chRemision);
//                phremosion.Add(chRemisionVal);

//                tblFindDcto.AddCell(new PdfPCell() { Colspan = 2, Phrase = phremosion });
//                #endregion

//                #region SUBTOTAL TF

//                PdfPTable tblsubTotalTF = new PdfPTable(2);
//                float[] DimensionessubTotalTF = new float[2];
//                DimensionessubTotalTF[0] = 1.0F;//
//                DimensionessubTotalTF[1] = 1.0F;//
//                tblsubTotalTF.WidthPercentage = 95;
//                tblsubTotalTF.HorizontalAlignment = Element.ALIGN_CENTER;
//                tblsubTotalTF.SetWidths(DimensionestotalesFactura);

//                tblsubTotalTF.AddCell(new PdfPCell() { Phrase = new Phrase("SUBTOTAL", ArialBold9) });
//                tblsubTotalTF.AddCell(new PdfPCell() { Phrase = new Phrase(Convert.ToDecimal(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DocSubTotal"]).ToString("C0"), Helvetica9) });

//                tblsubTotalTF.AddCell(new PdfPCell() { Phrase = new Phrase("(-) TF", ArialBold9) });
//                tblsubTotalTF.AddCell(new PdfPCell() { Phrase = new Phrase(totalDescuento.ToString("C0"), Helvetica9) });

//                tblFindDcto.AddCell(new PdfPCell(tblsubTotalTF) { Colspan = 2 });
//                #endregion

//                #region VALORENLETRAS
//                Chunk chValorLetras = new Chunk("VALOR EN LETRAS:   ", ArialBold9);
//                Chunk chValorLetrasVal = new Chunk(Nroenletras(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DocInvoiceAmt"].ToString()), ArialBold9);
//                Phrase phValorLetras = new Phrase();
//                phValorLetras.Add(chValorLetras);
//                // phValorLetras.Add(chValorLetrasVal);
//                //tblFindDcto.AddCell(new PdfPCell() { Colspan = 2, Phrase = phValorLetras });


//                Phrase phTotal = new Phrase("TOTAL:        " + Convert.ToDecimal(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DocInvoiceAmt"]).ToString("C0"), ArialBold9);
//                tblFindDcto.AddCell(new PdfPCell() { Colspan = 2, Phrase = phTotal });

//                #endregion


//                #region FIRMAS

//                Paragraph phGerente = new Paragraph("\n \n \n \n \n \n ING. ARTURO CASTILLO PEREZ \n GERENTE", Helvetica9);
//                phGerente.Alignment = Element.ALIGN_CENTER;



//                tblFindDcto.AddCell(new PdfPCell() { PaddingTop = 4, Phrase = new Phrase("Sirvase Realizar consignacion así: BANCOLOMBIA CTA CTE No. 4753116104 - 5 Suc.Murillo, BARRANQUILLA.", Helvetica6) });
//                tblFindDcto.AddCell(new PdfPCell() { HorizontalAlignment = Element.ALIGN_CENTER, Phrase = phGerente });
//                PdfPCell CellFirma = new PdfPCell() { Colspan = 2 };
//                Paragraph phAceptacion = new Paragraph("Acepto el contenido de la presente factura y hago constar el recibo conforme en ella discriminada", Helvetica5);
//                phAceptacion.Alignment = Element.ALIGN_CENTER;

//                Paragraph phFimaYSello = new Paragraph("\n \n\n \n ______________________________________________________\n Firma y Sello del Cliente", Helvetica6);
//                phFimaYSello.Alignment = Element.ALIGN_CENTER;

//                CellFirma.AddElement(phAceptacion);
//                CellFirma.AddElement(phFimaYSello);
//                tblFindDcto.AddCell(CellFirma);


//                tblFindDcto.AddCell(new PdfPCell() { Border = 0, Phrase = new Phrase("Epicor.ERP 10267", ArialBold6) });
//                tblFindDcto.AddCell(new PdfPCell() { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, Phrase = new Phrase("\n www.tecno-fuego.com.co", ArialBold6) });
//                tblFindDcto.AddCell(new PdfPCell() { HorizontalAlignment = Element.ALIGN_CENTER, Border = 0, Colspan = 2, Phrase = new Phrase("O.V: 0", Helvetica7) });

//                tblFindDcto.AddCell(new PdfPCell() { HorizontalAlignment = Element.ALIGN_CENTER, Border = 0, Colspan = 4, Phrase = new Phrase("ESTA FACTURA DE VENTA ES UN TITULO VALOR EN CUANTO CUMPLE CON LOS REQUERIMIENTOS EXIGIDOS POR LA LEY 1231 / 08.", Helvetica7) });


//                #endregion

//                strError += @"Linea 19061
//";


//                document.Add(TblEncabezadoFactura);
//                document.Add(tblLineasFactura);
//                document.Add(tableValUnidades);
//                document.Add(tblFindDcto);

//                /*PIE DE PAGINA*/
//                PdfContentByte pCb = writer.DirectContent;
//                // PieDePagina(ref pCb);

//                writer.Flush();
//                document.Close();

//                //AddFooterDocMercaLlantas(Ruta + NomArchivo, 180, 180);
//                RutaPdf = NomArchivo;
//                return true;
//            }
//            catch (Exception ex)
//            {
//                strError += @"NotaCreditoTecnoFuego"
//                    + ex.ToString();
//                //MessageBox.Show(ex.ToString());
//                return false;
//            }
//        }

//        private bool AddUnidadesNotaCreditoTecnoFuego(DataRow dataLine, ref PdfPTable pdfPTable, iTextSharp.text.Font font)
//        {
//            try
//            {

//                iTextSharp.text.pdf.PdfPCell celDescripcion = new iTextSharp.text.pdf.PdfPCell(new Phrase(dataLine["LineDesc"].ToString(), font));
//                celDescripcion.Colspan = 2;
//                celDescripcion.HorizontalAlignment = Element.ALIGN_LEFT;
//                celDescripcion.VerticalAlignment = Element.ALIGN_TOP;
//                pdfPTable.AddCell(celDescripcion);

//                iTextSharp.text.pdf.PdfPCell celCantidad = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:N0}", dataLine["SellingShipQty"].ToString()), font));
//                celCantidad.HorizontalAlignment = Element.ALIGN_RIGHT;
//                celCantidad.VerticalAlignment = Element.ALIGN_TOP;
//                pdfPTable.AddCell(celCantidad);



//                iTextSharp.text.pdf.PdfPCell celValorUnitario = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:C2}",
//                Convert.ToDecimal(dataLine["UnitPrice"].ToString())), font));
//                celValorUnitario.HorizontalAlignment = Element.ALIGN_RIGHT;
//                celValorUnitario.VerticalAlignment = Element.ALIGN_TOP;
//                pdfPTable.AddCell(celValorUnitario);


//                iTextSharp.text.pdf.PdfPCell celValorTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:C2}",
//                decimal.Parse(dataLine["DocExtPrice"].ToString())), font));
//                celValorTotal.PaddingTop = 0;
//                celValorTotal.HorizontalAlignment = Element.ALIGN_RIGHT;
//                celValorTotal.VerticalAlignment = Element.ALIGN_TOP;
//                pdfPTable.AddCell(celValorTotal);



//                iTextSharp.text.pdf.PdfPCell CelDcto = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:C2}",
//                decimal.Parse(dataLine["Discount"].ToString())), font));
//                CelDcto.HorizontalAlignment = Element.ALIGN_RIGHT;
//                CelDcto.VerticalAlignment = Element.ALIGN_TOP;
//                pdfPTable.AddCell(CelDcto);


//                return true;
//            }
//            catch (Exception ex)
//            {
//                strError += ex.Message;
//                //  MessageBox.Show(ex.Message);
//                return false;
//            }
//        }

//        #endregion

//        public class EventPageTecnoFuego : PdfPageEventHelper
//        {
//            PdfContentByte cb;

//            // we will put the final number of pages in a template
//            PdfTemplate headerTemplate, footerTemplate;

//            // this is the BaseFont we are going to use for the header / footer
//            BaseFont bf = null;

//            // This keeps track of the creation time
//            DateTime PrintTime = DateTime.Now;

//            private string _header;

//            public string Header
//            {
//                get { return _header; }
//                set { _header = value; }
//            }
//            string CUFE;
//            string NIT;
//            System.Drawing.Image QRInvoice;
//            DataSet DsInvoiceAR;
//            string tracer;

//            public EventPageTecnoFuego(ref string trcer, DataSet DSARInvoice, string pCUFE, string pNIT, System.Drawing.Image pQRInvoice)
//            {
//                this.CUFE = pCUFE;
//                this.NIT = pNIT;
//                this.QRInvoice = pQRInvoice;
//                this.DsInvoiceAR = DSARInvoice;
//                this.tracer = trcer;

//            }

//            public override void OnOpenDocument(PdfWriter writer, Document document)
//            {
//                try
//                {
//                    PrintTime = DateTime.Now;
//                    bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
//                    cb = writer.DirectContent;
//                    headerTemplate = cb.CreateTemplate(100, 100);
//                    footerTemplate = cb.CreateTemplate(50, 50);
//                }
//                catch (DocumentException de)
//                {

//                }
//                catch (System.IO.IOException ioe)
//                {
//                }
//            }

//            public override void OnStartPage(PdfWriter writer, Document document)
//            {
//                base.OnStartPage(writer, document);
//            }

//            public override void OnEndPage(iTextSharp.text.pdf.PdfWriter writer, iTextSharp.text.Document document)
//            {
//                base.OnEndPage(writer, document);


//                tracer += "error 19655";
//                if (writer.PageNumber.ToString() == "1")
//                {
//                    iTextSharp.text.Image QREnd = iTextSharp.text.Image.GetInstance(QRInvoice, BaseColor.WHITE);
//                    QREnd.ScaleAbsoluteWidth(70);
//                    QREnd.ScaleAbsoluteHeight(70);
//                    QREnd.SetAbsolutePosition(330, 710);
//                    tracer += "error 19662";
//                    document.Add(QREnd);
//                    tracer += "error 19664";
//                }

//                tracer += "error 19664";
//            }

//            public override void OnCloseDocument(PdfWriter writer, Document document)
//            {
//                base.OnCloseDocument(writer, document);

//                headerTemplate.BeginText();
//                headerTemplate.SetFontAndSize(bf, 12);
//                headerTemplate.SetTextMatrix(0, 0);
//                headerTemplate.ShowText((writer.PageNumber - 1).ToString());
//                headerTemplate.EndText();
//            }
//        }
//    }
}