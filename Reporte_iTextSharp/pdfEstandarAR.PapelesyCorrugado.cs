﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
//Referenciar y usar.
using System.Data;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Web;
using System.Windows.Forms;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml;
using RulesServicesDIAN2.Adquiriente;

namespace RulesServicesDIAN2.Adquiriente
{
    public partial class pdfEstandarAR
    {

        private bool AddUnidadesVentaPapelesyCorrugado(DataRow dataLine, ref PdfPTable table, iTextSharp.text.Font fontTitleFactura, DataSet dataSet)
        {
            try
            {
                //
                strError += "Add InvoiceLine";
                iTextSharp.text.pdf.PdfPCell celValL = new iTextSharp.text.pdf.PdfPCell(new Phrase((string)dataLine["OrderNum"], fontTitleFactura));
                celValL.Colspan = 1;
                celValL.Padding = 2;
                //celValL.Border = PdfPCell.RIGHT_BORDER | PdfPCell.LEFT_BORDER;
                //celTextL.BorderWidthRight = 1;
                //celValL.BorderWidthLeft = 1;
                celValL.HorizontalAlignment = Element.ALIGN_CENTER;
                celValL.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValL);
                strError += "InvoiceLine OK";

                //
                strError += "Add PartNum";
                iTextSharp.text.pdf.PdfPCell celValCodigo = new iTextSharp.text.pdf.PdfPCell(new Phrase((string)dataLine["PartNum"], fontTitleFactura));
                celValCodigo.Colspan = 1;
                celValCodigo.Padding = 2;
                //celValCodigo.Border = PdfPCell.RIGHT_BORDER;
                //celValCodigo.BorderWidthLeft = 1;
                //celDescripcion.BorderColorBottom = BaseColor.WHITE;
                celValCodigo.HorizontalAlignment = Element.ALIGN_CENTER;
                celValCodigo.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValCodigo);
                strError += "Add PartNum OK";

                //
                strError += "Add SellingShipQty"; 
                iTextSharp.text.pdf.PdfPCell celValDescripcion = new iTextSharp.text.pdf.PdfPCell(new Phrase((string)dataLine["LineDesc"], fontTitleFactura));
                celValDescripcion.Colspan = 1;
                celValDescripcion.Padding = 2;
                //celValDescripcion.Border = PdfPCell.RIGHT_BORDER;
                //celValCantidad.BorderWidthLeft = 1;
                celValDescripcion.HorizontalAlignment = Element.ALIGN_CENTER;
                celValDescripcion.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValDescripcion);
                strError += "Add SellingShipQty OK";

                //
                strError += "Add SalesUM";
                iTextSharp.text.pdf.PdfPCell celValUnidad = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    dataLine["DispPONum"].ToString(), fontTitleFactura));
                celValUnidad.Colspan = 1;
                celValUnidad.Padding = 2;
                //celValUnidad.Border = PdfPCell.RIGHT_BORDER;
                //celValUnidad.BorderWidthLeft = 1;
                celValUnidad.HorizontalAlignment = Element.ALIGN_CENTER;
                celValUnidad.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValUnidad);
                strError += "Add SalesUM OK";

                // 
                strError += "Add LineDesc";
                iTextSharp.text.pdf.PdfPCell celValCantidad = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    decimal.Parse((string)dataLine["SellingShipQty"]).ToString("N2"), fontTitleFactura));
                celValCantidad.Colspan = 1;
                celValCantidad.Padding = 2;
                //celValCantidad.Border = PdfPCell.RIGHT_BORDER;
                //celValDesc.BorderWidthLeft = 1;
                celValCantidad.HorizontalAlignment = Element.ALIGN_RIGHT;
                celValCantidad.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValCantidad);
                strError += "Add LineDesc OK";

                //
                strError += "Add LineDesc";
                iTextSharp.text.pdf.PdfPCell celValCantidad1 = new iTextSharp.text.pdf.PdfPCell(new Phrase("$ " +
                    decimal.Parse((string)dataLine["UnitPrice"]).ToString("N2"), fontTitleFactura));
                celValCantidad1.Colspan = 1;
                celValCantidad1.Padding = 2;
                //celValCantidad1.Border = 0;
                //celValDesc.BorderWidthLeft = 1;
                celValCantidad1.HorizontalAlignment = Element.ALIGN_RIGHT;
                celValCantidad1.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValCantidad1);
                strError += "Add LineDesc OK";

                //
                strError += "Add LineDesc";
                iTextSharp.text.pdf.PdfPCell celValCantidad2 = new iTextSharp.text.pdf.PdfPCell(new Phrase("$ " +
                    decimal.Parse((string)dataLine["DspDocExtPrice"]).ToString("N2"), fontTitleFactura));
                celValCantidad2.Colspan = 1;
                celValCantidad2.Padding = 2;
                //celValCantidad1.Border = 0;
                //celValDesc.BorderWidthLeft = 1;
                celValCantidad2.HorizontalAlignment = Element.ALIGN_RIGHT;
                celValCantidad2.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValCantidad2);
                strError += "Add LineDesc OK";


                return true;
            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.Message;
                return false;
            }
        }

        private bool AddUnidadesVentaPapelesyCorrugado(ref PdfPTable table)
        {
            try
            {
                iTextSharp.text.pdf.PdfPCell celValL = new iTextSharp.text.pdf.PdfPCell(new Phrase(" "));

                table.AddCell(celValL);

                iTextSharp.text.pdf.PdfPCell celValCodigo = new iTextSharp.text.pdf.PdfPCell(new Phrase(" "));

                table.AddCell(celValCodigo);

                iTextSharp.text.pdf.PdfPCell celValCantidad = new iTextSharp.text.pdf.PdfPCell(new Phrase(" "));

                table.AddCell(celValCantidad);

                iTextSharp.text.pdf.PdfPCell celValUnidad = new iTextSharp.text.pdf.PdfPCell(new Phrase(" "));

                table.AddCell(celValUnidad);

                iTextSharp.text.pdf.PdfPCell celValDesc = new iTextSharp.text.pdf.PdfPCell(new Phrase(" "));

                table.AddCell(celValDesc);

                iTextSharp.text.pdf.PdfPCell celValValorUnitario = new iTextSharp.text.pdf.PdfPCell(new Phrase(" "));

                table.AddCell(celValValorUnitario);

                iTextSharp.text.pdf.PdfPCell celValDescuento = new iTextSharp.text.pdf.PdfPCell(new Phrase(" "));

                table.AddCell(celValDescuento);
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return false;
            }
        }

        public bool FacturadeVentaPapelesyCorrugado(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {
            //NomArchivo = string.Empty;
            //string Ruta = HttpContext.Current.Server.MapPath("~/PDF/" + NIT + "/");
            //string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            //string RutaImg = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/logo andina (1).png";
            //string Rutasello = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Veritas.png";
            //string Rutasello2 = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/calidad2.png";
            //NomArchivo = NombreInvoice + ".pdf";
            //strError += "inicia Documento pdf\n";

            NomArchivo = string.Empty;
            string Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            string RutaImg = $@"{AppDomain.CurrentDomain.BaseDirectory}\logo andina (1).png";
            string Rutasello = $@"{AppDomain.CurrentDomain.BaseDirectory}\Veritas.png";
            string Rutasello2 = $@"{AppDomain.CurrentDomain.BaseDirectory}\calidad2.png";
            NomArchivo = NombreInvoice + ".pdf";
            try
            {
                #region Head
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.

                Document document = new Document(iTextSharp.text.PageSize.LETTER);

                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;

                // step 3: we open the document     
                document.Open();

                PdfDiv divEspacio = new PdfDiv();
                divEspacio.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio.Height = 10;
                divEspacio.Width = 130;

                PdfDiv divEspacio2 = new PdfDiv();
                divEspacio2.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio2.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio2.Height = 2;
                divEspacio2.Width = 130;

                //FUENTES----------------------------------------------------------------------------------------------------------
                iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle3 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 9, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle2 = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustom = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustom2 = FontFactory.GetFont(FontFactory.HELVETICA, 5, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitleFactura = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                //FUENTES---------------------------------------------------------------------------------------------------------

                RoundRectangle CelEventBorderRound = new RoundRectangle();


                System.Drawing.Image logo = null;
                var requestLogo = WebRequest.Create(RutaImg);

                using (var responseLogo = requestLogo.GetResponse())
                using (var streamLogo = responseLogo.GetResponseStream())
                {
                    logo = Bitmap.FromStream(streamLogo);
                }
                iTextSharp.text.Image ImgLogo = iTextSharp.text.Image.GetInstance(logo, BaseColor.WHITE);
                ImgLogo.ScaleAbsolute(200, 200);


                System.Drawing.Image logo2 = null;
                var requestLogo2 = WebRequest.Create(Rutasello);

                using (var responseLogo2 = requestLogo2.GetResponse())
                using (var streamLogo2 = responseLogo2.GetResponseStream())
                {
                    logo2 = Bitmap.FromStream(streamLogo2);
                }
                iTextSharp.text.Image ImgLogo2 = iTextSharp.text.Image.GetInstance(logo2, BaseColor.WHITE);
                ImgLogo2.ScaleAbsolute(100, 100);

                System.Drawing.Image logo3 = null;
                var requestLogo3 = WebRequest.Create(Rutasello2);

                using (var responseLogo3 = requestLogo3.GetResponse())
                using (var streamLogo3 = responseLogo3.GetResponseStream())
                {
                    logo3 = Bitmap.FromStream(streamLogo3);
                }
                iTextSharp.text.Image ImgLogo3 = iTextSharp.text.Image.GetInstance(logo3, BaseColor.WHITE);
                ImgLogo3.ScaleAbsolute(50, 50);

                iTextSharp.text.Image ImgQR = iTextSharp.text.Image.GetInstance(QRInvoice, BaseColor.WHITE);
                ImgQR.ScaleAbsolute(110, 110);

                #endregion

                PdfPTable todo = new PdfPTable(1);
                todo.WidthPercentage = 100;
                PdfPCell cell_todo = new PdfPCell()
                { Border = 0, MinimumHeight = 100, };
                todo.AddCell(cell_todo);

                DateTime fecha_inicio = DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]);
                DateTime fecha_fin = DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"]);

                //var fecha_inicio2 =decimal.parse((string)D
                var fechar_resultado = (fecha_fin - fecha_inicio).TotalDays;

                #region Header

                PdfPTable Header = new PdfPTable(new float[] { 0.45f, 0.25f, 0.20f, });
                Header.WidthPercentage = 100;
                PdfPCell Cell_logo = new PdfPCell(new Phrase("")) { MinimumHeight = 15, Border = 0, };
                Cell_logo.AddElement(ImgLogo);
                Header.AddCell(Cell_logo);
                PdfPCell cell_sello1 = new PdfPCell() { MinimumHeight = 15, Border = 0, };
                cell_sello1.AddElement(ImgLogo2);
                Header.AddCell(cell_sello1);
                PdfPCell cell_sello2 = new PdfPCell() { MinimumHeight = 15, Border = 0, };
                cell_sello2.AddElement(ImgLogo3);
                Header.AddCell(cell_sello2);

                PdfPTable nit_info = new PdfPTable(new float[] { 35, 65 });
                nit_info.WidthPercentage = 100;
                PdfPCell email = new PdfPCell(new Phrase("efacturacionelectronica@corrugadosandina.com.co\n" +
                                                         "NIT. 830000556 - 4", fontCustom))
                { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER };
                nit_info.AddCell(email);
                PdfPCell cufe = new PdfPCell(new Phrase("CUFE:" + CUFE, fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_RIGHT };
                nit_info.AddCell(cufe);

                PdfPTable header_info = new PdfPTable(new float[] { 0.5f, 0.35f, 0.25f, });
                header_info.WidthPercentage = 100;
                PdfPCell cell_info1 = new PdfPCell(new Phrase("GRANDES CONTRIBUYENTES Resolución 12506 Dic 26/2002\n" +
                                                              "ratificado por la DIAN Resolución 000076 de 01 / Dic / 2016\n" +
                                                               "IVA REGIMEN COMÚN, Agente Retenedor IVA\n" +
                                                               "Agente Retenedor de ICA en Sesquile(Cundinamarca)\n" +
                                                               "AUTORRETENEDORES Resolución 12052 Dic / 09 / 2015\n", fontCustom))
                { MinimumHeight = 15, };
                header_info.AddCell(cell_info1);
                PdfPCell cell_info2 = new PdfPCell(new Phrase("Impreso por computador según resolución 00055 de 2016. Autorización" +
                                                               "18762002766141 del No. 01 - 50001 al No. 01 - 79999 a partir del" +
                                                               "   31 / 03 / 2017, vigencia 24 meses", fontCustom));
                header_info.AddCell(cell_info2);
                PdfPCell cell_info3 = new PdfPCell() { };
                Paragraph prgfactura = new Paragraph($"    FACTURA DE VENTA",
                    FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 10f, iTextSharp.text.Font.NORMAL));
                Paragraph prgnumero = new Paragraph($"               No. { (string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"] }", fontCustom);
                Paragraph prgff = new Paragraph($"  \n       P&CA-03-PA4S-002F - 15/04/2014 Rev. 5", fontCustom2);
                cell_info3.AddElement(prgfactura);
                cell_info3.AddElement(prgnumero);
                cell_info3.AddElement(prgff);

                header_info.AddCell(cell_info3);
                #endregion

                #region Body

                PdfPTable Body = new PdfPTable(new float[] { 0.5f, 0.25f, 0.25f });
                Body.WidthPercentage = 100;
                PdfPCell cell_customer = new PdfPCell(new Phrase("")) { MinimumHeight = 0, };
                Paragraph señores = new Paragraph(new Phrase("SEÑORES: " + DsInvoiceAR.Tables["Customer"].Rows[0]["Name"], fontCustom));
                Paragraph nit = new Paragraph(new Phrase("NIT: " + $"{(string)DsInvoiceAR.Tables["Customer"].Rows[0]["CustID"]}-" +
                    $"{CalcularDigitoVerificacion((string)DsInvoiceAR.Tables["Customer"].Rows[0]["CustNum"])}\n", fontCustom));
                Paragraph ciudad = new Paragraph(new Phrase("CIUDAD: " + DsInvoiceAR.Tables["Customer"].Rows[0]["City"], fontCustom));
                Paragraph direccion = new Paragraph(new Phrase("DIRECCIÓN: " + DsInvoiceAR.Tables["Customer"].Rows[0]["Address1"], fontCustom));
                Paragraph direccionentrega = new Paragraph(new Phrase("DIRECCIÓN ENTREGA: " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["OrderNum"], fontCustom));
                cell_customer.AddElement(señores);
                cell_customer.AddElement(nit);
                cell_customer.AddElement(ciudad);
                cell_customer.AddElement(direccion);
                cell_customer.AddElement(direccionentrega);
                Body.AddCell(cell_customer);
                PdfPCell cell_fecha = new PdfPCell();

                PdfPTable fecha = new PdfPTable(1);
                fecha.WidthPercentage = 100;
                PdfPCell cell_fechas = new PdfPCell(new Phrase("FECHA: " + DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).ToString("dd/MM/yyyy"), fontCustom)) { MinimumHeight = 20, HorizontalAlignment = Element.ALIGN_CENTER, BackgroundColor = BaseColor.LIGHT_GRAY, };
                fecha.AddCell(cell_fechas);
                cell_fecha.AddElement(fecha);

                PdfPTable vendedor = new PdfPTable(1);
                vendedor.WidthPercentage = 100;
                PdfPCell cell_vendedor = new PdfPCell(new Phrase("VENDEDOR: " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["SalesRepName1"], fontCustom)) { MinimumHeight = 20, HorizontalAlignment = Element.ALIGN_CENTER };
                vendedor.AddCell(cell_vendedor);
                cell_fecha.AddElement(vendedor);

                PdfPTable telefono = new PdfPTable(1);
                telefono.WidthPercentage = 100;
                PdfPCell cell_telefono = new PdfPCell(new Phrase("TELEFONO: " + DsInvoiceAR.Tables["Customer"].Rows[0]["PhoneNum"], fontCustom)) { MinimumHeight = 20, HorizontalAlignment = Element.ALIGN_CENTER };
                telefono.AddCell(cell_telefono);
                cell_fecha.AddElement(telefono);

                Body.AddCell(cell_fecha);

                PdfPCell cell_vence = new PdfPCell();

                PdfPTable vencimiento = new PdfPTable(1);
                vencimiento.WidthPercentage = 100;
                PdfPCell cell_vencimiento = new PdfPCell(new Phrase("VENCIMIENTO: " + DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"]).ToString("dd/MM/yyyy"), fontCustom)) { MinimumHeight = 20, HorizontalAlignment = Element.ALIGN_CENTER, BackgroundColor = BaseColor.LIGHT_GRAY, };
                vencimiento.AddCell(cell_vencimiento);
                cell_vence.AddElement(vencimiento);

                PdfPTable termino = new PdfPTable(1);
                termino.WidthPercentage = 100;
                PdfPCell cell_termino = new PdfPCell(new Phrase("TERMINO: " + fechar_resultado, fontCustom)) { MinimumHeight = 20, HorizontalAlignment = Element.ALIGN_CENTER };
                termino.AddCell(cell_termino);
                cell_vence.AddElement(termino);

                PdfPTable moneda = new PdfPTable(1);
                moneda.WidthPercentage = 100;
                PdfPCell cell_moneda = new PdfPCell(new Phrase("MONEDA: " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["CurrencyCode"], fontCustom)) { MinimumHeight = 20, HorizontalAlignment = Element.ALIGN_CENTER };
                moneda.AddCell(cell_moneda);
                cell_vence.AddElement(moneda);

                Body.AddCell(cell_vence);


                PdfPTable unidades = new PdfPTable(new float[] { 0.1f, 0.1f, 0.3f, 0.12f, 0.12f, 0.12f, 0.14f });
                unidades.WidthPercentage = 100;

                PdfPCell cell_pedido = new PdfPCell(new Phrase("PEDIDO",
                    fontCustom))
                { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, BackgroundColor = BaseColor.LIGHT_GRAY, };
                PdfPCell cell_codigo = new PdfPCell(new Phrase("CODIGO",
                    fontCustom))
                { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, BackgroundColor = BaseColor.LIGHT_GRAY, };
                PdfPCell cell_descripcion = new PdfPCell(new Phrase("DESCRIPCION",
                    fontCustom))
                { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, BackgroundColor = BaseColor.LIGHT_GRAY, };
                PdfPCell cell_orden = new PdfPCell(new Phrase("ORDEN DE COMPRA",
                    fontCustom))
                { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, BackgroundColor = BaseColor.LIGHT_GRAY, };
                PdfPCell cell_cantidad = new PdfPCell(new Phrase("CANTIDAD",
                    fontCustom))
                { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, BackgroundColor = BaseColor.LIGHT_GRAY, };
                PdfPCell cell_valoru = new PdfPCell(new Phrase("VALOR   UNITARIO",
                    fontCustom))
                { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, BackgroundColor = BaseColor.LIGHT_GRAY, };
                PdfPCell cell_total = new PdfPCell(new Phrase("TOTAL",
                    fontCustom))
                { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, BackgroundColor = BaseColor.LIGHT_GRAY, };
                unidades.AddCell(cell_pedido);
                unidades.AddCell(cell_codigo);
                unidades.AddCell(cell_descripcion);
                unidades.AddCell(cell_orden);
                unidades.AddCell(cell_cantidad);
                unidades.AddCell(cell_valoru);
                unidades.AddCell(cell_total);
                #endregion

                #region Unidades

                PdfPTable tableTituloUnidades = new PdfPTable(7);
                //Dimenciones.
                float[] DimencionUnidades = new float[7];
                DimencionUnidades[0] = 0.1f;//CÓDIGO
                DimencionUnidades[1] = 0.1f;//DESCRIPCION
                DimencionUnidades[2] = 0.3f;//CANTIDAD
                DimencionUnidades[3] = 0.12f;//DTO
                DimencionUnidades[4] = 0.12f;//PRECIO
                DimencionUnidades[5] = 0.12f;//SUBTOTAL
                DimencionUnidades[6] = 0.14f;//SUBTOTAL

                PdfPTable tableUnidades = new PdfPTable(new float[] { 0.1f, 0.1f, 0.3f, 0.12f, 0.12f, 0.12f, 0.14f });
                tableUnidades.WidthPercentage = 100;
                tableUnidades.SetWidths(DimencionUnidades);

                foreach (DataRow InvoiceLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
                {
                    if (!AddUnidadesVentaPapelesyCorrugado(InvoiceLine, ref tableUnidades, fontCustom, DsInvoiceAR))
                        return false;
                }

                int numAdLineas = 7 - DsInvoiceAR.Tables["InvcDtl"].Rows.Count;
                PdfPTable tableEspacioUnidades = new PdfPTable(new float[] { 0.1f, 0.1f, 0.3f, 0.12f, 0.12f, 0.12f, 0.14f });
                tableEspacioUnidades.WidthPercentage = 100;
                tableEspacioUnidades.SetWidths(DimencionUnidades);

                for (int i = 0; i < numAdLineas; i++)
                    AddUnidadesVentaPapelesyCorrugado(ref tableUnidades);

                iTextSharp.text.pdf.PdfPCell LineaFinal = new iTextSharp.text.pdf.PdfPCell();
                LineaFinal.Colspan = 7;
                LineaFinal.Padding = 7;
                //LineaFinal.Border = 1;
                LineaFinal.BorderColorBottom = BaseColor.WHITE;
                LineaFinal.HorizontalAlignment = Element.ALIGN_CENTER;
                LineaFinal.VerticalAlignment = Element.ALIGN_TOP;
                tableUnidades.AddCell(LineaFinal);

                #endregion 

                #region Footer

                
                var numLetrascentavos = new Numalet();
                numLetrascentavos.SeparadorDecimalSalida = "pesos con";
                numLetrascentavos.MascaraSalidaDecimal = "centavos";
                numLetrascentavos.ApocoparUnoParteEntera = true;
                numLetrascentavos.ConvertirDecimales = true;

                string textNumLetras = numLetrascentavos.ToCustomCardinal((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspInvoiceAmt"]);


                PdfPTable Footer = new PdfPTable(new float[] { 0.6f, 0.4f });
                Footer.WidthPercentage = 100;
                PdfPCell letra_numero = new PdfPCell(new Phrase(string.Format("Son: {0}",
                                    textNumLetras), fontCustom))
                { HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP, MinimumHeight = 30, };
                PdfPCell cell_subt = new PdfPCell();

                PdfPTable fin = new PdfPTable(1);
                fin.WidthPercentage = 100;
                PdfPCell subtotal = new PdfPCell(new Phrase("SUBTOTAL:                                                          " + "$ " +
                    decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubTotal"]).ToString("N2"), fontCustom))
                { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP };
                PdfPCell subtotal2 = new PdfPCell();
                fin.AddCell(subtotal);
                fin.AddCell(subtotal2);
                cell_subt.AddElement(fin);

                PdfPTable iva = new PdfPTable(1);
                iva.WidthPercentage = 100;
                PdfPCell cell_iva = new PdfPCell(new Phrase("IVA:                                                                       " + "$ " +
                    decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DocTaxAmt"]).ToString("N2"), fontCustom))
                { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT };
                PdfPCell cell_iva2 = new PdfPCell();
                iva.AddCell(cell_iva);
                iva.AddCell(cell_iva2);
                cell_subt.AddElement(iva);

                PdfPTable total = new PdfPTable(1);
                total.WidthPercentage = 100;
                PdfPCell cell_totall = new PdfPCell(new Phrase("TOTAL:                                                                 " + "$ " +
                    decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"]).ToString("N2"), fontCustom))
                { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP, BackgroundColor = BaseColor.LIGHT_GRAY, };
                PdfPCell cell_totall2 = new PdfPCell();
                total.AddCell(cell_totall);
                total.AddCell(cell_totall2);
                cell_subt.AddElement(total);

                Footer.AddCell(letra_numero);
                Footer.AddCell(cell_subt);

                PdfPTable footer2 = new PdfPTable(new float[] { 0.4f, 0.2f, 0.2f, 0.2f, });
                footer2.WidthPercentage = 100;
                PdfPCell nom_recibe = new PdfPCell(new Phrase("NOMBRE DE QUIEN RECIBE", fontCustom)) { MinimumHeight = 15, HorizontalAlignment = Element.ALIGN_CENTER, BackgroundColor = BaseColor.LIGHT_GRAY, };
                PdfPCell fecha_recibe = new PdfPCell(new Phrase("FECHA DE RECIBIDO", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, BackgroundColor = BaseColor.LIGHT_GRAY, };
                PdfPCell sello_cliente = new PdfPCell(new Phrase("SELLO CLIENTE", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, BackgroundColor = BaseColor.LIGHT_GRAY, };
                PdfPCell firma_sello = new PdfPCell(new Phrase("FIRMA Y SELLO FUNCIONARIO P & CA", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, BackgroundColor = BaseColor.LIGHT_GRAY, };
                footer2.AddCell(nom_recibe);
                footer2.AddCell(fecha_recibe);
                footer2.AddCell(sello_cliente);
                footer2.AddCell(firma_sello);

                PdfPTable footer3 = new PdfPTable(new float[] { 0.4f, 0.2f, 0.2f, 0.2f, });
                footer3.WidthPercentage = 100;
                PdfPCell nom_recibe3 = new PdfPCell(new Phrase("", fontCustom)) { MinimumHeight = 35, HorizontalAlignment = Element.ALIGN_CENTER };
                PdfPCell fecha_recibe3 = new PdfPCell(new Phrase("", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, };
                PdfPCell sello_cliente3 = new PdfPCell(new Phrase("", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER };
                PdfPCell firma_sello3 = new PdfPCell(new Phrase("", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER };
                footer3.AddCell(nom_recibe3);
                footer3.AddCell(fecha_recibe3);
                footer3.AddCell(sello_cliente3);
                footer3.AddCell(firma_sello3);

                PdfPTable lectura = new PdfPTable(new float[] { 0.8f, 0.2f });
                lectura.WidthPercentage = 100;

                PdfPCell leyenda = new PdfPCell() { Border = 0 };

                PdfPTable leyenda1 = new PdfPTable(1);
                leyenda1.WidthPercentage = 100;
                PdfPCell cell_leyenda = new PdfPCell(new Phrase("EL PAGO NO OPORTUNO DE ESTA FACTURA DE VENTA CAUSARÁ INTERESES MORATORIOS A LA TASA MÁXIMA AUTORIZADA POR LA SUPERINTENDENCIA FINANCIERA DE\n\n" +
                                                                "COLOMBIA ART. 884 DEL CÓDIGO DE COMERCIO.\n" +
                                                                "ESTA FACTURA DE VENTA SE ASIMILA EN TODOS SUS EFECTOS LEGALES A UN TÍTULO VALOR, LEY 1231 DEL 17 / 10 / 2008.ARTICULO 773 CÓDIGO DE COMERCIO: PASADO 10\n\n" +
                                                                "DÍAS CALENDARIO SIGUIENTE A SU RECEPCIÓN, LA PRESENTE FACTURA SE ENTENDERÁ IRREVOCABLEMENTE ACEPTADA.LAS RECLAMACIONES DEBERÀN\n" +
                                                                "PRESENTARSE DENTRO DE LOS DIEZ DÌAS DESPUÉS DE LA ENTREGA DE LA MERCANCIA.\n" +
                                                                "APRECIADO CLIENTE, FAVOR DILIGENCIAR LOS CAMPOS: NOMBRE DE QUIEN RECIBE, FECHA DE RECIBIDO Y SELLO CLIENTE AL MOMENTO DE RECIBIR ESTA FACTURA.\n\n" +
                                                                "PAGAR EN CHEQUE GIRADO CON CRUCE RESTRICTIVO A PAPELES Y CORRUGADOS ANDINA S.A.Y / O CON TRANSFERENCIA ELECTRÓNICA DE FONDOS.\n\n", fontCustom));
                leyenda1.AddCell(cell_leyenda);
                leyenda.AddElement(leyenda1);

                PdfPTable leyenda2 = new PdfPTable(1);
                leyenda2.WidthPercentage = 100;
                PdfPCell cell_leyenda2 = new PdfPCell(new Phrase("OBSERVACIONES:  " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceComment"], fontCustom)) { MinimumHeight = 35, };
                leyenda2.AddCell(cell_leyenda2);
                leyenda.AddElement(leyenda2);
                lectura.AddCell(leyenda);

                PdfPCell Qr = new PdfPCell() { Border = 0, };
                Qr.AddElement(ImgQR);
                lectura.AddCell(Qr);

                PdfPTable leyendapie = new PdfPTable(3);
                leyendapie.WidthPercentage = 100;
                PdfPCell cell_leyenda_pie = new PdfPCell(new Phrase("Sede Administrativa\n" +
                                                                    "Bogotá D.C.\n" +
                                                                    "Carrera 15 No. 93A - 62 Oficina 802\n" +
                                                                    "PBX: (1)638 3000\n", fontCustom))
                { HorizontalAlignment = Element.ALIGN_LEFT, Border = 0, };
                leyendapie.AddCell(cell_leyenda_pie);
                PdfPCell cell_leyenda_pie2 = new PdfPCell(new Phrase("Ref:." + DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceNum"].ToString()+ "\n"+
                                                                     "ORIGINAL", fontCustom))
                { HorizontalAlignment = Element.ALIGN_CENTER, Border = 0, };
                leyendapie.AddCell(cell_leyenda_pie2);
                PdfPCell cell_leyenda_pie3 = new PdfPCell(new Phrase("Planta de producción\n" +
                                                                     "Sesquilé(Cundinamarca)\n" +
                                                                     "Autopista Norte Km. 37 alto San Isidro\n" +
                                                                     "PBX: (1) 6 38 3010", fontCustom))
                { HorizontalAlignment = Element.ALIGN_RIGHT, Border = 0, };
                leyendapie.AddCell(cell_leyenda_pie3);

                #endregion

                #region Exit

                document.Add(Header);
                document.Add(nit_info);
                document.Add(header_info);
                document.Add(Body);
                document.Add(unidades);
                document.Add(tableTituloUnidades);
                document.Add(tableUnidades);
                document.Add(todo);
                document.Add(Footer);
                document.Add(footer2);
                document.Add(footer3);
                document.Add(lectura);
                document.Add(leyendapie);
                //document.Add(divTextBancoCheque);
                /*PIE DE PAGINA*/
                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();
                RutaPdf = NomArchivo;
                return true;
                #endregion
            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.Message;
                return false;
            }

        }

        public bool NotaCreditoPapelesyCorrugado(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {
            NomArchivo = string.Empty;
            string Ruta = HttpContext.Current.Server.MapPath("~/PDF/" + NIT + "/");
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            string RutaImg = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/logo andina (1).png";
            string Rutasello = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Veritas.png";
            string Rutasello2 = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/calidad2.png";
            NomArchivo = NombreInvoice + ".pdf";
            strError += "inicia Documento pdf\n";

            //NomArchivo = string.Empty;
            //string Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
            //string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            //string RutaImg = $@"{AppDomain.CurrentDomain.BaseDirectory}\logo andina (1).png";
            //string Rutasello = $@"{AppDomain.CurrentDomain.BaseDirectory}\Veritas.png";
            //string Rutasello2 = $@"{AppDomain.CurrentDomain.BaseDirectory}\calidad2.png";
            //NomArchivo = NombreInvoice + ".pdf";
            try
            {
                #region Head
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.

                Document document = new Document(iTextSharp.text.PageSize.LETTER);

                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;

                // step 3: we open the document     
                document.Open();

                PdfDiv divEspacio = new PdfDiv();
                divEspacio.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio.Height = 10;
                divEspacio.Width = 130;

                PdfDiv divEspacio2 = new PdfDiv();
                divEspacio2.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio2.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio2.Height = 2;
                divEspacio2.Width = 130;

                //FUENTES----------------------------------------------------------------------------------------------------------
                iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle3 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 9, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle2 = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustom = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustom2 = FontFactory.GetFont(FontFactory.HELVETICA, 5, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitleFactura = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                //FUENTES---------------------------------------------------------------------------------------------------------

                RoundRectangle CelEventBorderRound = new RoundRectangle();


                System.Drawing.Image logo = null;
                var requestLogo = WebRequest.Create(RutaImg);

                using (var responseLogo = requestLogo.GetResponse())
                using (var streamLogo = responseLogo.GetResponseStream())
                {
                    logo = Bitmap.FromStream(streamLogo);
                }
                iTextSharp.text.Image ImgLogo = iTextSharp.text.Image.GetInstance(logo, BaseColor.WHITE);
                ImgLogo.ScaleAbsolute(200, 200);


                System.Drawing.Image logo2 = null;
                var requestLogo2 = WebRequest.Create(Rutasello);

                using (var responseLogo2 = requestLogo2.GetResponse())
                using (var streamLogo2 = responseLogo2.GetResponseStream())
                {
                    logo2 = Bitmap.FromStream(streamLogo2);
                }
                iTextSharp.text.Image ImgLogo2 = iTextSharp.text.Image.GetInstance(logo2, BaseColor.WHITE);
                ImgLogo2.ScaleAbsolute(100, 100);

                System.Drawing.Image logo3 = null;
                var requestLogo3 = WebRequest.Create(Rutasello2);

                using (var responseLogo3 = requestLogo3.GetResponse())
                using (var streamLogo3 = responseLogo3.GetResponseStream())
                {
                    logo3 = Bitmap.FromStream(streamLogo3);
                }
                iTextSharp.text.Image ImgLogo3 = iTextSharp.text.Image.GetInstance(logo3, BaseColor.WHITE);
                ImgLogo3.ScaleAbsolute(50, 50);

                iTextSharp.text.Image ImgQR = iTextSharp.text.Image.GetInstance(QRInvoice, BaseColor.WHITE);
                ImgQR.ScaleAbsolute(90, 90);

                #endregion

                PdfPTable todo = new PdfPTable(1);
                todo.WidthPercentage = 100;
                PdfPCell cell_todo = new PdfPCell()
                { Border = 0, MinimumHeight = 100, };
                todo.AddCell(cell_todo);

                DateTime fecha_inicio = DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]);
                DateTime fecha_fin = DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"]);

                //var fecha_inicio2 =decimal.parse((string)D
                var fechar_resultado = (fecha_fin - fecha_inicio).TotalDays;

                #region Header

                PdfPTable Header = new PdfPTable(new float[] { 0.45f, 0.25f, 0.20f, });
                Header.WidthPercentage = 100;
                PdfPCell Cell_logo = new PdfPCell(new Phrase("")) { MinimumHeight = 15, Border = 0, };
                Cell_logo.AddElement(ImgLogo);
                Header.AddCell(Cell_logo);
                PdfPCell cell_sello1 = new PdfPCell() { MinimumHeight = 15, Border = 0, };
                cell_sello1.AddElement(ImgLogo2);
                Header.AddCell(cell_sello1);
                PdfPCell cell_sello2 = new PdfPCell() { MinimumHeight = 15, Border = 0, };
                cell_sello2.AddElement(ImgLogo3);
                Header.AddCell(cell_sello2);

                PdfPTable nit_info = new PdfPTable(new float[] { 35, 65 });
                nit_info.WidthPercentage = 100;
                PdfPCell email = new PdfPCell(new Phrase("efacturacionelectronica@corrugadosandina.com.co\n" +
                                                         "NIT. 830000556 - 4", fontCustom))
                { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER };
                nit_info.AddCell(email);
                PdfPCell cufe = new PdfPCell(new Phrase("CUFE:" + CUFE, fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_RIGHT };
                nit_info.AddCell(cufe);

                PdfPTable header_info = new PdfPTable(new float[] { 0.5f, 0.35f, 0.25f, });
                header_info.WidthPercentage = 100;
                PdfPCell cell_info1 = new PdfPCell(new Phrase("GRANDES CONTRIBUYENTES Resolución 12506 Dic 26/2002\n" +
                                                              "ratificado por la DIAN Resolución 000076 de 01 / Dic / 2016\n" +
                                                               "IVA REGIMEN COMÚN, Agente Retenedor IVA\n" +
                                                               "Agente Retenedor de ICA en Sesquile(Cundinamarca)\n" +
                                                               "AUTORRETENEDORES Resolución 12052 Dic / 09 / 2015\n", fontCustom))
                { MinimumHeight = 15, };
                header_info.AddCell(cell_info1);
                PdfPCell cell_info2 = new PdfPCell(new Phrase("Impreso por computador según resolución 00055 de 2016. Autorización" +
                                                               "18762002766141 del No. 01 - 50001 al No. 01 - 79999 a partir del" +
                                                               "31 / 03 / 2017, vigencia 24 meses", fontCustom));
                header_info.AddCell(cell_info2);
                PdfPCell cell_info3 = new PdfPCell() { };
                Paragraph prgfactura = new Paragraph($"    NOTA CREDITO",
                    FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 10f, iTextSharp.text.Font.NORMAL));
                Paragraph prgnumero = new Paragraph($"               No. { (string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"] }" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"], fontCustom);
                Paragraph prgff = new Paragraph($"         P&CA-03-PA4S-002F - 15/04/2014 Rev. 5", fontCustom2);
                cell_info3.AddElement(prgfactura);
                cell_info3.AddElement(prgnumero);
                cell_info3.AddElement(prgff);

                header_info.AddCell(cell_info3);
                #endregion

                #region Body

                PdfPTable Body = new PdfPTable(new float[] { 0.5f, 0.25f, 0.25f });
                Body.WidthPercentage = 100;
                PdfPCell cell_customer = new PdfPCell(new Phrase("")) { MinimumHeight = 0, };
                Paragraph señores = new Paragraph(new Phrase("SEÑORES: " + DsInvoiceAR.Tables["Customer"].Rows[0]["Name"], fontCustom));
                Paragraph nit = new Paragraph(new Phrase("NIT: " + DsInvoiceAR.Tables["Customer"].Rows[0]["CustID"], fontCustom));
                Paragraph ciudad = new Paragraph(new Phrase("CIUDAD: " + DsInvoiceAR.Tables["Customer"].Rows[0]["City"], fontCustom));
                Paragraph direccion = new Paragraph(new Phrase("DIRECCIÓN: " + DsInvoiceAR.Tables["Customer"].Rows[0]["Address1"], fontCustom));
                Paragraph direccionentrega = new Paragraph(new Phrase("DIRESCIÓN ENTREGAR: " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["OrderNum"], fontCustom));
                cell_customer.AddElement(señores);
                cell_customer.AddElement(nit);
                cell_customer.AddElement(ciudad);
                cell_customer.AddElement(direccion);
                cell_customer.AddElement(direccionentrega);
                Body.AddCell(cell_customer);
                PdfPCell cell_fecha = new PdfPCell();

                PdfPTable fecha = new PdfPTable(1);
                fecha.WidthPercentage = 100;
                PdfPCell cell_fechas = new PdfPCell(new Phrase("FECHA: " + DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).ToString("dd/MM/yyyy"), fontCustom)) { MinimumHeight = 20, HorizontalAlignment = Element.ALIGN_CENTER, BackgroundColor = BaseColor.LIGHT_GRAY, };
                fecha.AddCell(cell_fechas);
                cell_fecha.AddElement(fecha);

                PdfPTable vendedor = new PdfPTable(1);
                vendedor.WidthPercentage = 100;
                PdfPCell cell_vendedor = new PdfPCell(new Phrase("VENDEDOR: " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["SalesRepName1"], fontCustom)) { MinimumHeight = 20, HorizontalAlignment = Element.ALIGN_CENTER };
                vendedor.AddCell(cell_vendedor);
                cell_fecha.AddElement(vendedor);

                PdfPTable telefono = new PdfPTable(1);
                telefono.WidthPercentage = 100;
                PdfPCell cell_telefono = new PdfPCell(new Phrase("TELEFONO: " + DsInvoiceAR.Tables["Customer"].Rows[0]["PhoneNum"], fontCustom)) { MinimumHeight = 20, HorizontalAlignment = Element.ALIGN_CENTER };
                telefono.AddCell(cell_telefono);
                cell_fecha.AddElement(telefono);

                Body.AddCell(cell_fecha);

                PdfPCell cell_vence = new PdfPCell();

                PdfPTable vencimiento = new PdfPTable(1);
                vencimiento.WidthPercentage = 100;
                PdfPCell cell_vencimiento = new PdfPCell(new Phrase("VENCIMIENTO: " + DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"]).ToString("dd/MM/yyyy"), fontCustom)) { MinimumHeight = 20, HorizontalAlignment = Element.ALIGN_CENTER, BackgroundColor = BaseColor.LIGHT_GRAY, };
                vencimiento.AddCell(cell_vencimiento);
                cell_vence.AddElement(vencimiento);

                PdfPTable termino = new PdfPTable(1);
                termino.WidthPercentage = 100;
                PdfPCell cell_termino = new PdfPCell(new Phrase("TERMINO: " + fechar_resultado, fontCustom)) { MinimumHeight = 20, HorizontalAlignment = Element.ALIGN_CENTER };
                termino.AddCell(cell_termino);
                cell_vence.AddElement(termino);

                PdfPTable moneda = new PdfPTable(1);
                moneda.WidthPercentage = 100;
                PdfPCell cell_moneda = new PdfPCell(new Phrase("MONEDA: " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["CurrencyCode"], fontCustom)) { MinimumHeight = 20, HorizontalAlignment = Element.ALIGN_CENTER };
                moneda.AddCell(cell_moneda);
                cell_vence.AddElement(moneda);

                Body.AddCell(cell_vence);


                PdfPTable unidades = new PdfPTable(new float[] { 0.1f, 0.1f, 0.3f, 0.12f, 0.12f, 0.12f, 0.14f });
                unidades.WidthPercentage = 100;

                PdfPCell cell_pedido = new PdfPCell(new Phrase("PEDIDO",
                    fontCustom))
                { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, BackgroundColor = BaseColor.LIGHT_GRAY, };
                PdfPCell cell_codigo = new PdfPCell(new Phrase("CODIGO",
                    fontCustom))
                { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, BackgroundColor = BaseColor.LIGHT_GRAY, };
                PdfPCell cell_descripcion = new PdfPCell(new Phrase("DESCRIPCION",
                    fontCustom))
                { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, BackgroundColor = BaseColor.LIGHT_GRAY, };
                PdfPCell cell_orden = new PdfPCell(new Phrase("ORDEN DE COMPRA",
                    fontCustom))
                { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, BackgroundColor = BaseColor.LIGHT_GRAY, };
                PdfPCell cell_cantidad = new PdfPCell(new Phrase("CANTIDAD",
                    fontCustom))
                { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, BackgroundColor = BaseColor.LIGHT_GRAY, };
                PdfPCell cell_valoru = new PdfPCell(new Phrase("VALOR UBNITARIO",
                    fontCustom))
                { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, BackgroundColor = BaseColor.LIGHT_GRAY, };
                PdfPCell cell_total = new PdfPCell(new Phrase("TOTAL",
                    fontCustom))
                { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, BackgroundColor = BaseColor.LIGHT_GRAY, };
                unidades.AddCell(cell_pedido);
                unidades.AddCell(cell_codigo);
                unidades.AddCell(cell_descripcion);
                unidades.AddCell(cell_orden);
                unidades.AddCell(cell_cantidad);
                unidades.AddCell(cell_valoru);
                unidades.AddCell(cell_total);
                #endregion

                #region Unidades

                PdfPTable tableTituloUnidades = new PdfPTable(7);
                //Dimenciones.
                float[] DimencionUnidades = new float[7];
                DimencionUnidades[0] = 0.1f;//CÓDIGO
                DimencionUnidades[1] = 0.1f;//DESCRIPCION
                DimencionUnidades[2] = 0.3f;//CANTIDAD
                DimencionUnidades[3] = 0.12f;//DTO
                DimencionUnidades[4] = 0.12f;//PRECIO
                DimencionUnidades[5] = 0.12f;//SUBTOTAL
                DimencionUnidades[6] = 0.14f;//SUBTOTAL

                PdfPTable tableUnidades = new PdfPTable(7);
                tableUnidades.WidthPercentage = 100;
                tableUnidades.SetWidths(DimencionUnidades);

                foreach (DataRow InvoiceLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
                {
                    if (!AddUnidadesVentaPapelesyCorrugado(InvoiceLine, ref tableUnidades, fontCustom, DsInvoiceAR))
                        return false;
                }

                iTextSharp.text.pdf.PdfPCell LineaFinal = new iTextSharp.text.pdf.PdfPCell();
                LineaFinal.Colspan = 7;
                LineaFinal.Padding = 3;
                LineaFinal.Border = 1;
                LineaFinal.BorderColorBottom = BaseColor.WHITE;
                LineaFinal.HorizontalAlignment = Element.ALIGN_CENTER;
                LineaFinal.VerticalAlignment = Element.ALIGN_TOP;
                tableUnidades.AddCell(LineaFinal);

                #endregion

                #region Footer


                var numLetrascentavos = new Numalet();
                numLetrascentavos.SeparadorDecimalSalida = "pesos con";
                numLetrascentavos.MascaraSalidaDecimal = "centavos";
                numLetrascentavos.ApocoparUnoParteEntera = true;
                numLetrascentavos.ConvertirDecimales = true;

                string textNumLetras = numLetrascentavos.ToCustomCardinal((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspInvoiceAmt"]);


                PdfPTable Footer = new PdfPTable(new float[] { 0.6f, 0.4f });
                Footer.WidthPercentage = 100;
                PdfPCell letra_numero = new PdfPCell(new Phrase(string.Format("Son: {0}",
                                    textNumLetras, fontCustom)))
                { HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP, MinimumHeight = 30, };
                PdfPCell cell_subt = new PdfPCell();

                PdfPTable fin = new PdfPTable(1);
                fin.WidthPercentage = 100;
                PdfPCell subtotal = new PdfPCell(new Phrase("SUBTOTAL:                                                                " + "$ " +
                    decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubTotal"]).ToString("N0"), fontCustom))
                { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP };
                PdfPCell subtotal2 = new PdfPCell();
                fin.AddCell(subtotal);
                fin.AddCell(subtotal2);
                cell_subt.AddElement(fin);

                PdfPTable iva = new PdfPTable(1);
                iva.WidthPercentage = 100;
                PdfPCell cell_iva = new PdfPCell(new Phrase("IVA:                                                                             " + "$ " +
                    decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DocTaxAmt"]).ToString("N0"), fontCustom))
                { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT };
                PdfPCell cell_iva2 = new PdfPCell();
                iva.AddCell(cell_iva);
                iva.AddCell(cell_iva2);
                cell_subt.AddElement(iva);

                PdfPTable total = new PdfPTable(1);
                total.WidthPercentage = 100;
                PdfPCell cell_totall = new PdfPCell(new Phrase("TOTAL:                                                                       " + "$ " +
                    decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"]).ToString("N0"), fontCustom))
                { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP, BackgroundColor = BaseColor.LIGHT_GRAY, };
                PdfPCell cell_totall2 = new PdfPCell();
                total.AddCell(cell_totall);
                total.AddCell(cell_totall2);
                cell_subt.AddElement(total);

                Footer.AddCell(letra_numero);
                Footer.AddCell(cell_subt);

                PdfPTable footer2 = new PdfPTable(new float[] { 0.4f, 0.2f, 0.2f, 0.2f, });
                footer2.WidthPercentage = 100;
                PdfPCell nom_recibe = new PdfPCell(new Phrase("NOMBRE DE QUIEN RECIBE", fontCustom)) { MinimumHeight = 20, HorizontalAlignment = Element.ALIGN_CENTER, BackgroundColor = BaseColor.LIGHT_GRAY, };
                PdfPCell fecha_recibe = new PdfPCell(new Phrase("FECHA DE RECIBIDO", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, BackgroundColor = BaseColor.LIGHT_GRAY, };
                PdfPCell sello_cliente = new PdfPCell(new Phrase("SELLO CLIENTE", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, BackgroundColor = BaseColor.LIGHT_GRAY, };
                PdfPCell firma_sello = new PdfPCell(new Phrase("FIRMA Y SELLO FUNCIONARIO P & CA", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, BackgroundColor = BaseColor.LIGHT_GRAY, };
                footer2.AddCell(nom_recibe);
                footer2.AddCell(fecha_recibe);
                footer2.AddCell(sello_cliente);
                footer2.AddCell(firma_sello);

                PdfPTable footer3 = new PdfPTable(new float[] { 0.4f, 0.2f, 0.2f, 0.2f, });
                footer3.WidthPercentage = 100;
                PdfPCell nom_recibe3 = new PdfPCell(new Phrase("", fontCustom)) { MinimumHeight = 20, HorizontalAlignment = Element.ALIGN_CENTER };
                PdfPCell fecha_recibe3 = new PdfPCell(new Phrase("", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, };
                PdfPCell sello_cliente3 = new PdfPCell(new Phrase("", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER };
                PdfPCell firma_sello3 = new PdfPCell(new Phrase("", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER };
                footer3.AddCell(nom_recibe3);
                footer3.AddCell(fecha_recibe3);
                footer3.AddCell(sello_cliente3);
                footer3.AddCell(firma_sello3);

                PdfPTable lectura = new PdfPTable(new float[] { 0.8f, 0.2f });
                lectura.WidthPercentage = 100;

                PdfPCell leyenda = new PdfPCell();

                PdfPTable leyenda1 = new PdfPTable(1);
                leyenda1.WidthPercentage = 100;
                PdfPCell cell_leyenda = new PdfPCell(new Phrase("EL PAGO NO OPORTUNO DE ESTA FACTURA DE VENTA CAUSARÁ INTERESES MORATORIOS A LA TASA MÁXIMA AUTORIZADA POR LA SUPERINTENDENCIA FINANCIERA DE\n" +
                                                                "COLOMBIA ART. 884 DEL CÓDIGO DE COMERCIO.\n" +
                                                                "ESTA FACTURA DE VENTA SE ASIMILA EN TODOS SUS EFECTOS LEGALES A UN TÍTULO VALOR, LEY 1231 DEL 17 / 10 / 2008.ARTICULO 773 CÓDIGO DE COMERCIO: PASADO 10\n" +
                                                                "DÍAS CALENDARIO SIGUIENTE A SU RECEPCIÓN, LA PRESENTE FACTURA SE ENTENDERÁ IRREVOCABLEMENTE ACEPTADA.LAS RECLAMACIONES DEBERÀN\n" +
                                                                "PRESENTARSE DENTRO DE LOS DIEZ DÌAS DESPUÉS DE LA ENTREGA DE LA MERCANCIA.\n" +
                                                                "APRECIADO CLIENTE, FAVOR DILIGENCIAR LOS CAMPOS: NOMBRE DE QUIEN RECIBE, FECHA DE RECIBIDO Y SELLO CLIENTE AL MOMENTO DE RECIBIR ESTA FACTURA.\n" +
                                                                "PAGAR EN CHEQUE GIRADO CON CRUCE RESTRICTIVO A PAPELES Y CORRUGADOS ANDINA S.A.Y / O CON TRANSFERENCIA ELECTRÓNICA DE FONDOS.", fontCustom));
                leyenda1.AddCell(cell_leyenda);
                leyenda.AddElement(leyenda1);

                PdfPTable leyenda2 = new PdfPTable(1);
                leyenda2.WidthPercentage = 100;
                PdfPCell cell_leyenda2 = new PdfPCell(new Phrase("OBSERVACIONES:  " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceComment"], fontCustom));
                leyenda2.AddCell(cell_leyenda2);
                leyenda.AddElement(leyenda2);
                lectura.AddCell(leyenda);

                PdfPCell Qr = new PdfPCell() { Border = 0, };
                Qr.AddElement(ImgQR);
                lectura.AddCell(Qr);

                PdfPTable leyendapie = new PdfPTable(3);
                leyendapie.WidthPercentage = 100;
                PdfPCell cell_leyenda_pie = new PdfPCell(new Phrase("Sede Administrativa\n" +
                                                                    "Bogotá D.C.\n" +
                                                                    "Carrera 15 No. 93A - 62 Oficina 802\n" +
                                                                    "PBX: (1)638 3000\n", fontCustom))
                { HorizontalAlignment = Element.ALIGN_LEFT, Border = 0, };
                leyendapie.AddCell(cell_leyenda_pie);
                PdfPCell cell_leyenda_pie2 = new PdfPCell(new Phrase("Ref:. 77317\n" +
                                                                     "ORIGINAL", fontCustom))
                { HorizontalAlignment = Element.ALIGN_CENTER, Border = 0, };
                leyendapie.AddCell(cell_leyenda_pie2);
                PdfPCell cell_leyenda_pie3 = new PdfPCell(new Phrase("Planta de producción\n" +
                                                                     "Sesquilé(Cundinamarca)\n" +
                                                                     "Autopista Norte Km. 37 alto San Isidro\n" +
                                                                     "PBX: (1) 6 38 3010", fontCustom))
                { HorizontalAlignment = Element.ALIGN_RIGHT, Border = 0, };
                leyendapie.AddCell(cell_leyenda_pie3);

                #endregion

                #region Exit

                document.Add(Header);
                document.Add(nit_info);
                document.Add(header_info);
                document.Add(Body);
                document.Add(unidades);
                document.Add(tableTituloUnidades);
                document.Add(tableUnidades);
                document.Add(todo);
                document.Add(Footer);
                document.Add(footer2);
                document.Add(footer3);
                document.Add(lectura);
                document.Add(leyendapie);
                //document.Add(divTextBancoCheque);
                /*PIE DE PAGINA*/
                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();
                RutaPdf = NomArchivo;
                return true;
                #endregion
            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.Message;
                return false;
            }

        }

    }
}




                    