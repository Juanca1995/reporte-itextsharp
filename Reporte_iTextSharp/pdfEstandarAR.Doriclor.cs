﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
//Referenciar y usar.
using System.Data;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Web;
using System.Windows.Forms;

namespace RulesServicesDIAN2.Adquiriente
{
    public partial class pdfEstandarAR
    {

        #region FACTURA DE EXPORTACION DORICOLOR

        #region funciones de pagina 

        float dimxdori = 540f, dimydoricolor = 7f;

        protected void AddPageContinuedoricolor(string rutaPDF, float posicionX, float posicionY, iTextSharp.text.Font font)
        {
            byte[] bytes = File.ReadAllBytes(rutaPDF);

            //iTextSharp.text.Font blackFont = FontFactory.GetFont("Arial", 12, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
            using (MemoryStream stream = new MemoryStream())
            {
                PdfReader reader = new PdfReader(bytes);
                using (PdfStamper stamper = new PdfStamper(reader, stream))
                {
                    int pages = reader.NumberOfPages;
                    string textContinue = "CONTINUES/CONTINUA...";
                    for (int i = 1; i <= pages; i++)
                    {
                        if (i == pages)
                            textContinue = string.Empty;

                        ColumnText.ShowTextAligned(
                            stamper.GetUnderContent(i), Element.ALIGN_CENTER, new Phrase($"{textContinue}", font), posicionX, posicionY, 0);
                    }
                }
                bytes = stream.ToArray();
            }
            File.WriteAllBytes(rutaPDF, bytes);
        }

        #endregion

        private bool AddUnidadesDoricolor2(DataRow dataLine, ref PdfPTable table, iTextSharp.text.Font fontTitleFactura, DataSet dataSet)
        {
            try
            {
                //CODIGO
                strError += "Add InvoiceLine";
                iTextSharp.text.pdf.PdfPCell celValL = new iTextSharp.text.pdf.PdfPCell(new Phrase((string)dataLine["CommodityCode"], fontTitleFactura));
                celValL.Colspan = 1;
                celValL.Padding = 2;
                celValL.Border = 0;
                //celTextL.BorderWidthRight = 1;
                //celValL.BorderWidthLeft = 1;
                celValL.HorizontalAlignment = Element.ALIGN_CENTER;
                celValL.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValL);
                strError += "InvoiceLine OK";

                //DESCRIPCION
                strError += "Add PartNum";
                iTextSharp.text.pdf.PdfPCell celValCodigo = new iTextSharp.text.pdf.PdfPCell(new Phrase((string)dataLine["PartNumPartDescription"], fontTitleFactura));
                celValCodigo.Colspan = 1;
                celValCodigo.Padding = 2;
                celValCodigo.Border = 0;
                //celValCodigo.BorderWidthLeft = 1;
                //celDescripcion.BorderColorBottom = BaseColor.WHITE;
                celValCodigo.HorizontalAlignment = Element.ALIGN_LEFT;
                celValCodigo.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValCodigo);
                strError += "Add PartNum OK";

                //UNIDADES
                strError += "Add SellingShipQty";
                iTextSharp.text.pdf.PdfPCell celValDescripcion = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    decimal.Parse((string)dataLine["SellingOrderQty"]).ToString("N0"), fontTitleFactura));
                celValDescripcion.Colspan = 1;
                celValDescripcion.Padding = 2;
                celValDescripcion.Border = 0;
                //celValCantidad.BorderWidthLeft = 1;
                celValDescripcion.HorizontalAlignment = Element.ALIGN_CENTER;
                celValDescripcion.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValDescripcion);
                strError += "Add SellingShipQty OK";

                //PRECIO UNIDAD
                strError += "Add SalesUM";
                iTextSharp.text.pdf.PdfPCell celValUnidad = new iTextSharp.text.pdf.PdfPCell(new Phrase("$ " +
                    decimal.Parse((string)dataLine["DocUnitPrice"]).ToString("N3"), fontTitleFactura));
                celValUnidad.Colspan = 1;
                celValUnidad.Padding = 2;
                celValUnidad.Border = 0;
                //celValUnidad.BorderWidthLeft = 1;
                celValUnidad.HorizontalAlignment = Element.ALIGN_CENTER;
                celValUnidad.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValUnidad);
                strError += "Add SalesUM OK";

                //SUB TOTAL
                strError += "Add LineDesc";
                iTextSharp.text.pdf.PdfPCell celValCantidad = new iTextSharp.text.pdf.PdfPCell(new Phrase("$ " +
                    decimal.Parse((string)dataLine["DspDocExtPrice"]).ToString("N3"), fontTitleFactura));
                celValCantidad.Colspan = 1;
                celValCantidad.Padding = 2;
                celValCantidad.Border = 0;
                //celValDesc.BorderWidthLeft = 1;
                celValCantidad.HorizontalAlignment = Element.ALIGN_CENTER;
                celValCantidad.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValCantidad);
                strError += "Add LineDesc OK";
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return false;
            }
        }

        private bool AddUnidadesDoricolo(ref PdfPTable table)
        {
            try
            {
                iTextSharp.text.pdf.PdfPCell celValL = new iTextSharp.text.pdf.PdfPCell(new Phrase(" "));
                celValL.Colspan = 1;
                celValL.Padding = 3;
                celValL.Border = 0;
                //celTextL.BorderWidthRight = 1;
                celValL.BorderWidthLeft = 1;
                celValL.HorizontalAlignment = Element.ALIGN_CENTER;
                celValL.VerticalAlignment = Element.ALIGN_TOP;

                table.AddCell(celValL);

                iTextSharp.text.pdf.PdfPCell celValCodigo = new iTextSharp.text.pdf.PdfPCell(new Phrase(" "));
                celValCodigo.Colspan = 1;
                celValCodigo.Padding = 3;
                celValCodigo.Border = 0;
                celValCodigo.BorderWidthLeft = 1;
                //celDescripcion.BorderColorBottom = BaseColor.WHITE;
                celValCodigo.HorizontalAlignment = Element.ALIGN_CENTER;
                celValCodigo.VerticalAlignment = Element.ALIGN_TOP;

                table.AddCell(celValCodigo);

                iTextSharp.text.pdf.PdfPCell celValCantidad = new iTextSharp.text.pdf.PdfPCell(new Phrase(" "));
                celValCantidad.Colspan = 1;
                celValCantidad.Padding = 3;
                celValCantidad.Border = 0;
                celValCantidad.BorderWidthLeft = 1;
                celValCantidad.HorizontalAlignment = Element.ALIGN_CENTER;
                celValCantidad.VerticalAlignment = Element.ALIGN_TOP;

                table.AddCell(celValCantidad);

                iTextSharp.text.pdf.PdfPCell celValUnidad = new iTextSharp.text.pdf.PdfPCell(new Phrase(" "));
                celValUnidad.Colspan = 1;
                celValUnidad.Padding = 3;
                celValUnidad.Border = 0;
                celValUnidad.BorderWidthLeft = 1;
                celValUnidad.HorizontalAlignment = Element.ALIGN_CENTER;
                celValUnidad.VerticalAlignment = Element.ALIGN_TOP;

                table.AddCell(celValUnidad);

                iTextSharp.text.pdf.PdfPCell celValDesc = new iTextSharp.text.pdf.PdfPCell(new Phrase(" "));
                celValDesc.Colspan = 1;
                celValDesc.Padding = 3;
                celValDesc.Border = 0;
                celValDesc.BorderWidthLeft = 1;
                celValDesc.HorizontalAlignment = Element.ALIGN_CENTER;
                celValDesc.VerticalAlignment = Element.ALIGN_TOP;

                table.AddCell(celValDesc);

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return false;
            }
        }

        public bool ExportacionCreditoDoriColor(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {
            NomArchivo = string.Empty;
            string Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            string RutaImg = $@"{AppDomain.CurrentDomain.BaseDirectory}\logo_doricolor.png";
            string RutaMarcaAgua = $@"{AppDomain.CurrentDomain.BaseDirectory}\logo_doricolor.png";
            NomArchivo = NombreInvoice + ".pdf";

            //NomArchivo = string.Empty;
            //string Ruta = HttpContext.Current.Server.MapPath("~/PDF/" + NIT + "/");
            //string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            //string RutaImg = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";
            //NomArchivo = NombreInvoice + ".pdf";
            //strError += "inicia Documento pdf\n";
            try
            {
                #region Head
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.

                Document document = new Document(iTextSharp.text.PageSize.LETTER);

                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;

                // step 3: we open the document     
                document.Open();

                PdfDiv divEspacio = new PdfDiv();
                divEspacio.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio.Height = 10;
                divEspacio.Width = 130;

                PdfDiv divEspacio2 = new PdfDiv();
                divEspacio2.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio2.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                divEspacio2.Height = 10;
                divEspacio2.Width = 130;


                //FUENTES----------------------------------------------------------------------------------------------------------
                iTextSharp.text.Font fontTitleBold = FontFactory.GetFont(FontFactory.HELVETICA, 8.5f, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA, 8.5f, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustom = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustomtext = FontFactory.GetFont(FontFactory.COURIER, 8f, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustomBold = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle3 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle4 = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font grande = FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font mediana = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font pequeña = FontFactory.GetFont(FontFactory.HELVETICA, 3, iTextSharp.text.Font.NORMAL);

                //FUENTES---------------------------------------------------------------------------------------------------------

                ///objeto para el borde 
                RoundRectangle CelEventBorderRound = new RoundRectangle();

                PdfPTable vacio = new PdfPTable(1);
                vacio.WidthPercentage = 100;
                PdfPCell vacio1 = new PdfPCell()
                {
                    MinimumHeight = 10,
                    Border = 0
                };
                vacio.AddCell(vacio1);


                System.Drawing.Image Logo;
                System.Drawing.Image LogoBanner = null;


                var request = WebRequest.Create(RutaImg);

                using (var response = request.GetResponse())
                using (var stream = response.GetResponseStream())
                {
                    Logo = Bitmap.FromStream(stream);
                }

                //Banner-------------
                try
                {
                    var requestBanner = WebRequest.Create(RutaImg);

                    using (var responseBanner = requestBanner.GetResponse())
                    using (var streamBanner = responseBanner.GetResponseStream())
                    {
                        LogoBanner = Bitmap.FromStream(streamBanner);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("\n" + ex.Message);
                }


                //agregamos Qr----------------------------------------------
                PdfPTable QRr = new PdfPTable(1);
                Paragraph prgCufe = new Paragraph("CUFE: " + CUFE, mediana);
                prgCufe.Alignment = Element.ALIGN_LEFT;

                iTextSharp.text.Image QRPdf = iTextSharp.text.Image.GetInstance(QRInvoice, BaseColor.WHITE);
                QRPdf.ScaleAbsolute(60f, 60);
                //iTextSharp.text.pdf.PdfPCell celImgQR = new iTextSharp.text.pdf.PdfPCell(QRPdf);
                iTextSharp.text.pdf.PdfPCell celImgQR = new iTextSharp.text.pdf.PdfPCell();
                celImgQR.AddElement(QRPdf);
                celImgQR.AddElement(prgCufe);
                celImgQR.Colspan = 3;
                celImgQR.Padding = 3;
                celImgQR.Border = 0;
                celImgQR.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celImgQR.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                QRr.AddCell(celImgQR);


                //Logo--------------------------------
                iTextSharp.text.Image LogoPdf2 = iTextSharp.text.Image.GetInstance(LogoBanner, BaseColor.WHITE);
                LogoPdf2.ScaleAbsolute(170.0f, 170.0f);
                LogoPdf2.Border = 0;

                #endregion
                //agregamos todo lo del encabezado
                #region Header

                PdfPTable tableEncabezado = new PdfPTable(4);
                float[] dimTableEcabezado = new float[4] {
                    2.0f,1.45f,1.6f,1.7f
                };
                tableEncabezado.SetWidths(dimTableEcabezado);
                tableEncabezado.WidthPercentage = 100;

                System.Drawing.Image logo = null;
                var requestLogo = WebRequest.Create(RutaImg);

                using (var responseLogo = requestLogo.GetResponse())
                using (var streamLogo = responseLogo.GetResponseStream())
                {
                    logo = Bitmap.FromStream(streamLogo);
                }

                PdfDiv divLogo = new PdfDiv();
                divLogo.Float = iTextSharp.text.pdf.PdfDiv.FloatType.LEFT;
                divLogo.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divLogo.BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID;
                divLogo.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                divLogo.Height = 45;
                divLogo.Width = 150;
                //divLogo.BackgroundColor = BaseColor.LIGHT_GRAY;
                iTextSharp.text.Image ImgLogo = iTextSharp.text.Image.GetInstance(logo, BaseColor.WHITE);
                ImgLogo.Alignment = Element.ALIGN_CENTER;
                divLogo.BackgroundImage = ImgLogo;

                PdfPCell celLogo = new PdfPCell()
                {
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    //Colspan = 2,
                    Border = 0,
                    Padding = 3
                };
                celLogo.AddElement(divLogo);
                tableEncabezado.AddCell(celLogo);

                PdfPCell celTextInfoEmpresa = new PdfPCell(new Phrase(
               $"NIT:{(string)DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"]} RÉGIMEN COMÚN\nNO SOMOS GRANDES CONTRIBUYENTES\n" +
               $"RESOL. 000041 DEL 30 DE ENERO DE 2014\nNO SOMOS AUTORRETENEDORES",
               FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL)))
                {
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    //Colspan = 2,
                    Border = 0,
                    Padding = 3
                };
                tableEncabezado.AddCell(celTextInfoEmpresa);

                Paragraph prgInfoEmpresa1 = new Paragraph($"{(string)DsInvoiceAR.Tables["Company"].Rows[0]["Address1"]}\n" +
                    $"PBX: {(string)DsInvoiceAR.Tables["Company"].Rows[0]["PhoneNum"]} -  " +
                    $"FAX: {(string)DsInvoiceAR.Tables["Company"].Rows[0]["FaxNum"]}\n" +
                    $"{(string)DsInvoiceAR.Tables["Company"].Rows[0]["City"]} - " +
                    $"{(string)DsInvoiceAR.Tables["Company"].Rows[0]["State"]}",
                FontFactory.GetFont(FontFactory.HELVETICA, 5.79f, iTextSharp.text.Font.NORMAL));
                prgInfoEmpresa1.Alignment = Element.ALIGN_CENTER;

                Paragraph prgInfoEmpresa2 = new Paragraph("web: www.doricolor.com.co\ne-mail: c.exterior@doricolor.com.co",
                FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 6.3f, iTextSharp.text.Font.NORMAL));
                prgInfoEmpresa2.Alignment = Element.ALIGN_CENTER;

                PdfPCell celTextInfoEmpresa2 = new PdfPCell()
                {
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    //Colspan = 2,
                    Border = 0,
                    Padding = 3
                };
                celTextInfoEmpresa2.AddElement(prgInfoEmpresa1);
                celTextInfoEmpresa2.AddElement(prgInfoEmpresa2);
                tableEncabezado.AddCell(celTextInfoEmpresa2);
                //-------------------------------------------------------------------------------------
                Paragraph prgTextFacturaVenta = new Paragraph("FACTURA DE EXPORTACIÓN",
                FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 12, iTextSharp.text.Font.NORMAL));
                prgTextFacturaVenta.Alignment = Element.ALIGN_CENTER;

                Paragraph prgValFacturaVenta = new Paragraph((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"],
                FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 10, iTextSharp.text.Font.NORMAL));
                prgValFacturaVenta.Alignment = Element.ALIGN_CENTER;

                PdfPTable tableFacturaFechas = new PdfPTable(2);
                PdfPTable tableFacturaFechas2 = new PdfPTable(1);

                PdfPCell celTextFechaFactura = new PdfPCell(new Phrase("Fecha Factura", fontCustom))
                {
                    //CellEvent=CelEventBorderRound,
                    BackgroundColor = BaseColor.LIGHT_GRAY,
                    Border = 0,
                    //Padding = 2
                };
                tableFacturaFechas.AddCell(celTextFechaFactura);

                PdfPCell celTextFechaVence = new PdfPCell(new Phrase("Fecha Vence", fontCustom))
                {
                    //CellEvent = CelEventBorderRound,
                    BackgroundColor = BaseColor.LIGHT_GRAY,
                    Border = 0,
                    //Padding = 2
                };
                tableFacturaFechas.AddCell(celTextFechaVence);

                PdfPCell celValFechaFactura = new PdfPCell(new Phrase(DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).ToString("dd/MM/yyyy"),
                    fontCustomtext))
                {
                    //CellEvent=CelEventBorderRound,
                    //BackgroundColor = BaseColor.LIGHT_GRAY,
                    Border = 0,
                    //Padding = 2
                };
                tableFacturaFechas.AddCell(celValFechaFactura);

                PdfPCell celValFechaVence = new PdfPCell(new Phrase(DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"]).ToString("dd/MM/yyyy")
                    , fontCustomtext))
                {
                    //CellEvent = CelEventBorderRound,
                    //BackgroundColor = BaseColor.LIGHT_GRAY,
                    Border = 0,
                    BorderWidthLeft = 1,
                    //Padding = 2
                };
                tableFacturaFechas.AddCell(celValFechaVence);

                PdfPCell celFacturaFechas = new PdfPCell(tableFacturaFechas)
                {
                    CellEvent = CelEventBorderRound,
                    Border = 0,
                    Padding = 0
                };
                tableFacturaFechas2.AddCell(celFacturaFechas);

                PdfPCell celTextInfoFactura = new PdfPCell()
                {
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    //Colspan = 2,
                    Border = 0,
                    Padding = 3
                };
                celTextInfoFactura.AddElement(prgTextFacturaVenta);
                celTextInfoFactura.AddElement(prgValFacturaVenta);
                celTextInfoFactura.AddElement(tableFacturaFechas2);
                tableEncabezado.AddCell(celTextInfoFactura);

                string descResolucion = string.Empty;

                if (Helpers.Compartido.VerificarExistenciaColumnas(DsInvoiceAR, "InvcHead", "ResolucionFE_c"))
                    descResolucion = (string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["ResolucionFE_c"];

                PdfPCell celTextResolucion = new PdfPCell(new Phrase(descResolucion, fontCustomtext))
                {
                    Colspan = 4,
                    Border = 0,
                    //Padding = 3
                };
                tableEncabezado.AddCell(celTextResolucion);
                #endregion
                //agregamos los encabezados de los detalles

                #region Datos del Cliente
                PdfPTable tableInfoCliente = new PdfPTable(3);
                float[] dimTableInfoCliente = new float[3];
                dimTableInfoCliente[0] = 2.0f;
                dimTableInfoCliente[1] = 0.1f;
                dimTableInfoCliente[2] = 2.0f;

                tableInfoCliente.SetWidths(dimTableInfoCliente);
                tableInfoCliente.WidthPercentage = 100;

                PdfPCell celTextSenores = new PdfPCell()
                {
                    Border = PdfPCell.NO_BORDER,
                    VerticalAlignment = Element.ALIGN_BOTTOM,
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    //Padding = 3,
                    PaddingLeft = 25,
                    Phrase = new Phrase("SEÑORES", fontCustom)
                };
                tableInfoCliente.AddCell(celTextSenores);

                PdfPCell celEspacioInfoCliente = new PdfPCell();
                celEspacioInfoCliente.Border = 0;
                tableInfoCliente.AddCell(celEspacioInfoCliente);

                PdfPCell celTextEnviadoA = new PdfPCell()
                {
                    Border = PdfPCell.NO_BORDER,
                    VerticalAlignment = Element.ALIGN_BOTTOM,
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    //Padding = 3,
                    PaddingLeft = 25,
                    Phrase = new Phrase("ENVIADO A", fontCustom)
                };
                tableInfoCliente.AddCell(celTextEnviadoA);

                string prgDatosCliente = $" { (string)DsInvoiceAR.Tables["Customer"].Rows[0]["Name"] }\n\n" +
                   "NIT/RUC/RUT/ID: " + $"{(string)DsInvoiceAR.Tables["Customer"].Rows[0]["ResaleID"]}\n\n" +
                   "Dirección:   " + $"{(string)DsInvoiceAR.Tables["Customer"].Rows[0]["Address1"]}\n\n" +
                   "Ciudad:   " + $"{(string)DsInvoiceAR.Tables["Customer"].Rows[0]["City"]}\n\n" +
                   "Teléfono:   " + $"{(string)DsInvoiceAR.Tables["Customer"].Rows[0]["PhoneNum"]}\n\n";

                PdfPCell celDatosSenores = new PdfPCell()
                {
                    CellEvent = CelEventBorderRound,
                    Border = PdfPCell.NO_BORDER,
                    //BorderWidthBottom = 0.3f,
                    //Colspan = 3,
                    VerticalAlignment = Element.ALIGN_BOTTOM,
                    Phrase = new Phrase(prgDatosCliente, fontCustomtext),
                    HorizontalAlignment = Element.ALIGN_LEFT
                };
                tableInfoCliente.AddCell(celDatosSenores);

                tableInfoCliente.AddCell(celEspacioInfoCliente);
                //---------------------------------------------------------------------------------------------
                string prgDatosEnviadoA = $" { (string)DsInvoiceAR.Tables["Customer"].Rows[0]["ShipToName"] }\n\n" +
                   "NIT/RUC/RUT/ID: " + $"{(string)DsInvoiceAR.Tables["Customer"].Rows[0]["ShipToNit"]}\n\n" +
                   "Dirección:   " + $"{(string)DsInvoiceAR.Tables["Customer"].Rows[0]["ShipToAddress"]}\n\n" +
                   "Ciudad:   " + $"{(string)DsInvoiceAR.Tables["Customer"].Rows[0]["ShipToCity"]}\n\n" +
                   "Teléfono:   " + $"{(string)DsInvoiceAR.Tables["Customer"].Rows[0]["PhoneNum"]}\n\n";

                PdfPCell celDatosEnviadoA = new PdfPCell()
                {
                    CellEvent = CelEventBorderRound,
                    Border = PdfPCell.NO_BORDER,
                    //BorderWidthBottom = 0.3f,
                    //Colspan = 3,
                    VerticalAlignment = Element.ALIGN_BOTTOM,
                    Phrase = new Phrase(prgDatosEnviadoA, fontCustomtext),
                    HorizontalAlignment = Element.ALIGN_LEFT
                };
                tableInfoCliente.AddCell(celDatosEnviadoA);
                //---------------------------------------------------------------------------------------------
                //tableInfoCliente.AddCell(new PdfPCell(new Phrase("Hola")));

                iTextSharp.text.Image ImgQR = iTextSharp.text.Image.GetInstance(QRInvoice, BaseColor.WHITE);
                ImgQR.Alignment = Element.ALIGN_CENTER;
                ImgQR.ScaleAbsolute(65f, 65f);

                PdfPCell celQr = new PdfPCell()
                {
                    Border = PdfPCell.NO_BORDER,
                };
                celQr.AddElement(ImgQR);

                PdfPTable tableQR = new PdfPTable(new float[] { 1, 3.5f }) { WidthPercentage = 100, };
                tableQR.AddCell(celQr);

                Paragraph prgCufeLeyenda = new Paragraph();
                prgCufeLeyenda.Add(new Phrase("Representación gráfica factura electrónica", fontCustomtext));
                prgCufeLeyenda.Add(new Phrase($"\n\nCUFE:{CUFE}", fontCustomtext));

                tableQR.AddCell(new PdfPCell(prgCufeLeyenda) { Border = PdfPCell.NO_BORDER, VerticalAlignment = Element.ALIGN_CENTER, });
                tableInfoCliente.AddCell(new PdfPCell(tableQR) { Colspan = 2, Border = PdfPCell.NO_BORDER, });

                PdfPTable tableContNoPedido = new PdfPTable(1);
                PdfPTable tableNoPedido = new PdfPTable(2);

                PdfPCell celTextNoPedido = new PdfPCell()
                {
                    Border = PdfPCell.NO_BORDER,
                    BackgroundColor = BaseColor.LIGHT_GRAY,
                    VerticalAlignment = Element.ALIGN_BOTTOM,
                    Phrase = new Phrase("No. de Pedido", fontTitle),
                    HorizontalAlignment = Element.ALIGN_CENTER
                };
                tableNoPedido.AddCell(celTextNoPedido);

                PdfPCell celTextNoDocumento = new PdfPCell()
                {
                    Border = PdfPCell.NO_BORDER,
                    BackgroundColor = BaseColor.LIGHT_GRAY,
                    VerticalAlignment = Element.ALIGN_BOTTOM,
                    Phrase = new Phrase("Documento Número", fontTitle),
                    HorizontalAlignment = Element.ALIGN_CENTER
                };
                tableNoPedido.AddCell(celTextNoDocumento);

                PdfPCell celValtNoPedido = new PdfPCell()
                {
                    Padding = 5,
                    Border = PdfPCell.NO_BORDER,
                    VerticalAlignment = Element.ALIGN_TOP,
                    Phrase = new Phrase((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["OrderNum"], fontCustomtext),
                    HorizontalAlignment = Element.ALIGN_CENTER
                };
                tableNoPedido.AddCell(celValtNoPedido);

                PdfPCell celValNoDocumento = new PdfPCell()
                {
                    Padding = 5,
                    Border = PdfPCell.NO_BORDER,
                    VerticalAlignment = Element.ALIGN_TOP,
                    Phrase = new Phrase((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceNum"], fontCustomtext),
                    HorizontalAlignment = Element.ALIGN_CENTER
                };
                tableNoPedido.AddCell(celValNoDocumento);

                tableNoPedido.AddCell(new PdfPCell() { Colspan = 2, Border = PdfPCell.NO_BORDER, });

                tableContNoPedido.AddCell(new PdfPCell(tableNoPedido)
                {
                    CellEvent = CelEventBorderRound,
                    Border = PdfPCell.NO_BORDER,
                });
                tableContNoPedido.AddCell(new PdfPCell()
                {
                    Border = PdfPCell.NO_BORDER,
                });

                PdfPCell celNoPedido = new PdfPCell(tableContNoPedido)
                {
                    //CellEvent = CelEventBorderRound,
                    Border = PdfPCell.NO_BORDER
                };

                tableInfoCliente.AddCell(celNoPedido);
                #endregion

                #region Body

                //leyenda de oferta
                PdfPTable oferta = new PdfPTable(1);
                oferta.WidthPercentage = 100;
                PdfPCell oferta_cell = new PdfPCell(new Phrase("CONSIGNAR EN CUENTA DE AHORROS DAVIVIENDA No 038200013076",
                FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7.2f, BaseColor.RED))) { MinimumHeight = 15, Border = 0, };
                oferta.AddCell(oferta_cell);


                PdfPTable contenedora = new PdfPTable(1);
                contenedora.WidthPercentage = 100;
                PdfPCell cell_contenedora = new PdfPCell() { Border = 0, CellEvent = CelEventBorderRound, };

                PdfPTable detalle1 = new PdfPTable(new float[] { 4.0f, 2.0f, 2.0f, 2.0f });
                detalle1.WidthPercentage = 100;

                PdfPCell detalle1_titulo = new PdfPCell(new Phrase("Condiciones de pago", fontTitle4))
                {
                    BackgroundColor = BaseColor.LIGHT_GRAY
                    , Border = 0,
                    //CellEvent = CelEventBorderRound,
                    HorizontalAlignment = Element.ALIGN_CENTER,
                };
                PdfPCell detalle2_titulo = new PdfPCell(new Phrase("Asesor comercial", fontTitle4))
                {
                    BackgroundColor = BaseColor.LIGHT_GRAY,
                    Border = 0,
                    //CellEvent = CelEventBorderRound,
                    HorizontalAlignment = Element.ALIGN_CENTER,
                };
                PdfPCell detalle3_titulo = new PdfPCell(new Phrase("Telefono", fontTitle4))
                {
                    BackgroundColor = BaseColor.LIGHT_GRAY,
                    Border = 0,
                    //CellEvent = CelEventBorderRound,
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    Colspan = 2,
                };

                detalle1.AddCell(detalle1_titulo);
                detalle1.AddCell(detalle2_titulo);
                detalle1.AddCell(detalle3_titulo);


                cell_contenedora.AddElement(detalle1);

                PdfPTable detalle1info = new PdfPTable(new float[] { 4.0f, 2.0f, 2.0f, 2.0f });
                detalle1info.WidthPercentage = 100;

                PdfPCell detalle1_info = new PdfPCell(new Phrase("" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["TermsCodeDescription"], fontCustomtext))
                {
                    //CellEvent = CelEventBorderRound,
                    Border = 0,
                    HorizontalAlignment = Element.ALIGN_CENTER,
                };
                PdfPCell detalle2_info = new PdfPCell(new Phrase("" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["SalesRepName1"], fontCustomtext))
                {
                    //CellEvent = CelEventBorderRound,
                    Border = 0,
                    HorizontalAlignment = Element.ALIGN_CENTER,
                };
                PdfPCell detalle3_info = new PdfPCell(new Phrase("" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["SalesRepCellPhone"], fontCustomtext))
                {
                    //CellEvent = CelEventBorderRound,
                    Border = 0,
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    Colspan = 2,
                };


                detalle1info.AddCell(detalle1_info);
                detalle1info.AddCell(detalle2_info);
                detalle1info.AddCell(detalle3_info);


                cell_contenedora.AddElement(detalle1info);
                contenedora.AddCell(cell_contenedora);

                PdfPTable detalle = new PdfPTable(new float[] { 1.5f, 4.0f, 1.5f, 1.5f, 1.5f });
                detalle.WidthPercentage = 100;

                PdfPCell detalles1 = new PdfPCell(new Phrase("COD ARANCEL", fontTitle))
                {
                    Border = 0,
                    MinimumHeight = 5,
                    HorizontalAlignment = Element.ALIGN_CENTER,
                };
                PdfPCell detalles2 = new PdfPCell(new Phrase("DESCRIPCION DE PRODUCTO", fontTitle))
                {
                    Border = 0,
                    HorizontalAlignment = Element.ALIGN_CENTER,
                };
                PdfPCell detalles3 = new PdfPCell(new Phrase("CANTIDAD", fontTitle))
                {
                    Border = 0,
                    HorizontalAlignment = Element.ALIGN_CENTER,
                };
                PdfPCell detalles4 = new PdfPCell(new Phrase("PRECIO", fontTitle))
                {
                    Border = 0,
                    HorizontalAlignment = Element.ALIGN_CENTER,
                };
                PdfPCell detalles5 = new PdfPCell(new Phrase("SUB-TOTAL", fontTitle))
                {
                    Border = 0,
                    HorizontalAlignment = Element.ALIGN_CENTER,
                };

                detalle.AddCell(detalles1);
                detalle.AddCell(detalles2);
                detalle.AddCell(detalles3);
                detalle.AddCell(detalles4);
                detalle.AddCell(detalles5);


                #endregion
                //agregamos las unidades
                #region Unidades


                PdfPTable tableTituloUnidades = new PdfPTable(5);
                //Dimenciones.
                float[] DimencionUnidades = new float[5];
                DimencionUnidades[0] = 1.5f;//CÓDIGO
                DimencionUnidades[1] = 4.0f;//DESCRIPCION
                DimencionUnidades[2] = 1.5f;//CANTIDAD
                DimencionUnidades[3] = 1.5f;//DTO
                DimencionUnidades[4] = 1.5f;//PRECIO


                PdfPTable tableUnidades = new PdfPTable(5);
                tableUnidades.WidthPercentage = 100;
                tableUnidades.SetWidths(DimencionUnidades);

                decimal unidades = 0;

                foreach (DataRow InvoiceLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
                {
                    if (!AddUnidadesDoricolor2(InvoiceLine, ref tableUnidades, fontCustomtext, DsInvoiceAR))
                        return false;

                    unidades += decimal.Parse((string)InvoiceLine["SellingOrderQty"]);
                }

                iTextSharp.text.pdf.PdfPCell LineaFinal = new iTextSharp.text.pdf.PdfPCell();
                LineaFinal.Colspan = 5;
                LineaFinal.Padding = 3;
                LineaFinal.Border = 1;
                LineaFinal.BorderColorBottom = BaseColor.WHITE;
                LineaFinal.HorizontalAlignment = Element.ALIGN_CENTER;
                LineaFinal.VerticalAlignment = Element.ALIGN_TOP;
                tableUnidades.AddCell(LineaFinal);

                #endregion
                //agregamos la segubnda tabla de detalles 
                #region OBSERVACIONES Y TOTALES
                PdfPTable tableObsTotales2 = new PdfPTable(3);
                tableObsTotales2.WidthPercentage = 100;
                float[] dimTableObsTotales2 = new float[3];
                dimTableObsTotales2[0] = 2.0f;
                dimTableObsTotales2[1] = 0.8f;
                dimTableObsTotales2[2] = 0.85f;
                tableObsTotales2.SetWidths(dimTableObsTotales2);


                #endregion
                //agrem,asmos todos los totales y el cvontenido del pie de pagina 
                #region Footer

                PdfPTable pie = new PdfPTable(new float[] { 3.0f, 3.0f, 4.0f });
                pie.WidthPercentage = 100;

                PdfPCell peso = new PdfPCell(new Phrase("Peso Neto: " + decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["PartNetWeight"]).ToString("N3") + " Kg", fontCustomtext)) { Border = 0, };//peso neto
                PdfPCell PesoBruto = new PdfPCell(new Phrase("Peso Bruto: " + decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["PartGrossWeight"]).ToString("N3") + " Kg", fontCustomtext)) { Border = 0, };//peso bruto 4
                PdfPCell pago = new PdfPCell(new Phrase("Gran Total  UDS: " + " $ " + decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"]).ToString("N3"), fontCustomtext)) { Border = 0, };//Granpago

                pie.AddCell(peso);
                pie.AddCell(PesoBruto);
                pie.AddCell(pago);




                //celda de ubnidades 
                PdfPTable unidad = new PdfPTable(1);
                unidad.WidthPercentage = 100;
                PdfPCell uni = new PdfPCell(new Phrase("Unidades:" + unidades.ToString("N0"), fontCustomtext)) { Border = 0, };
                unidad.AddCell(uni);


                //celda de anotacion
                PdfPTable anotacion = new PdfPTable(1);
                anotacion.WidthPercentage = 100;
                PdfPCell nota = new PdfPCell(new Phrase("LOS PRECIOS DE LA FACTURA SON FOB CARTAGENA (INCOTERMS 2010)", fontTitle4)) { Border = 0, };
                anotacion.AddCell(nota);

                //Celda de firma 
                PdfPTable fimas = new PdfPTable(new float[] { 3.5f, 3.0f, 3.5f });
                fimas.WidthPercentage = 100;

                PdfPCell elaborado = new PdfPCell(new Phrase("_______________________________________________" + "Eladorada por:", fontTitle4))
                {
                    Border = 0,
                    MinimumHeight = 80,
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    VerticalAlignment = Element.ALIGN_BOTTOM
                };
                PdfPCell firma_vacio = new PdfPCell()
                {
                    Border = 0,
                };
                PdfPCell recibido = new PdfPCell(new Phrase("_______________________________________________" + "Recibida por:", fontTitle4))
                {
                    Border = 0,
                    MinimumHeight = 80,
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    VerticalAlignment = Element.ALIGN_BOTTOM
                };

                fimas.AddCell(elaborado);
                fimas.AddCell(firma_vacio);
                fimas.AddCell(recibido);

                //agregamos el numero de paque 
                PdfPTable bnumero_enpaque = new PdfPTable(1);
                bnumero_enpaque.WidthPercentage = 100;
                PdfPCell n_enpaq = new PdfPCell(new Phrase("MfgSys Empaque: " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["PlantName"] + "  " + DsInvoiceAR.Tables["InvcDtl"].Rows[0]["PackNum"], fontCustomtext)) { Border = 0, };
                bnumero_enpaque.AddCell(n_enpaq);

                //agregamos la leyenda
                PdfPTable leyenda = new PdfPTable(1);
                leyenda.WidthPercentage = 100;
                PdfPCell leye = new PdfPCell(new Phrase("WE CERTIFY THAT PRICES ON THE INVOICE ARE ORIGINAL AND THE PRODUCT IS MANUFACTURED BY DORICOLOR SAS", fontTitle4))
                {
                    Border = 0,
                    HorizontalAlignment = Element.ALIGN_CENTER,
                };
                leyenda.AddCell(leye);

                //writer.PageEvent = new EventPageEncabezado(tableEncabezado);
                //PdfPTable tableMarcaAgua = new PdfPTable(1);
                //System.Drawing.Image logoMarcaAgua = null;
                //var requestMarcaAgua = WebRequest.Create(RutaMarcaAgua);
                //using (var responseLogo = requestMarcaAgua.GetResponse())
                //using (var streamLogo = responseLogo.GetResponseStream())
                //{
                //    logoMarcaAgua = Bitmap.FromStream(streamLogo);
                //}
                //iTextSharp.text.Image ImgMarcaAgua = iTextSharp.text.Image.GetInstance(logoMarcaAgua, BaseColor.WHITE);
                //ImgMarcaAgua.Alignment = Element.ALIGN_CENTER;
                //ImgMarcaAgua.ScaleAbsolute(500f, 500f);
                //PdfPCell celMarcaAgua = new PdfPCell()
                //{
                //    //BackgroundColor=BaseColor.LIGHT_GRAY,
                //    FixedHeight = 380,
                //    Border = PdfPCell.NO_BORDER,
                //};
                //celMarcaAgua.AddElement(ImgMarcaAgua);
                //tableMarcaAgua.AddCell(celMarcaAgua);
                ////writer.PageEvent = new EventPageMarcaAgua(tableMarcaAgua, 380f);
                //writer.PageEvent = new EventPagePiePag(pie, 145f);

                #endregion
                //agregamos todas la tablas al documento
                #region Exit

                document.Add(tableEncabezado);
                document.Add(vacio);
                document.Add(tableInfoCliente);
                document.Add(vacio);
                document.Add(contenedora);
                document.Add(oferta);
                document.Add(detalle);
                document.Add(tableTituloUnidades);
                document.Add(tableUnidades);
                document.Add(vacio);
                document.Add(pie);
                document.Add(vacio);
                document.Add(unidad);
                document.Add(vacio);
                document.Add(anotacion);
                document.Add(fimas);
                document.Add(vacio);
                document.Add(bnumero_enpaque);
                document.Add(vacio);
                document.Add(leyenda);
                //document.Add(tableMarcaAgua);

                strError += "inicia se agrega el content byte del pie de pagina pdf\n";
                PdfContentByte pCb = writer.DirectContent; PdfContentByte pCb2 = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();
                strError += "inicia Documento pdf\n";
                AddPageNumberPagTo($"{Ruta}{NomArchivo}", dimxdori, 755, fontTitle);
                AddPageContinuedoricolor($"{Ruta}{NomArchivo}", dimydoricolor, 745, fontTitle);
                RutaPdf = NomArchivo;
                strError += "inicia se agrego la numeracion satisfactoriamente OK\n";
                return true;
                #endregion
            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.Message;
                return false;
            }


        }

        #endregion
    }
}