﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
//Referenciar y usar.
using System.Data;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Web;
using System.Windows.Forms;

namespace RulesServicesDIAN2.Adquiriente
{
    public partial class pdfEstandarAR
    {
        #region Formato Sensuel

        private bool AddUnidadesSensuel(DataRow dataLine, ref PdfPTable table, iTextSharp.text.Font font, DataSet dataSet)
        {
            try
            {
                PdfPCell celTextReferencia = new PdfPCell(new Phrase((string)dataLine["PartNum"], font));
                //celTextSenores.Border = 0;
                celTextReferencia.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextReferencia.VerticalAlignment = Element.ALIGN_CENTER;
                table.AddCell(celTextReferencia);

                PdfPCell celTextDetalle = new PdfPCell(new Phrase((string)dataLine["LineDesc"], font));
                //celTextSenores.Border = 0;
                celTextDetalle.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextDetalle.VerticalAlignment = Element.ALIGN_CENTER;
                table.AddCell(celTextDetalle);

                PdfPCell celTextCantidad = new PdfPCell(new Phrase(decimal.Parse((string)dataLine["SellingShipQty"]).ToString("N2"), font));
                //celTextSenores.Border = 0;
                celTextCantidad.HorizontalAlignment = Element.ALIGN_RIGHT;
                celTextCantidad.VerticalAlignment = Element.ALIGN_CENTER;
                table.AddCell(celTextCantidad);

                PdfPCell celTextUnidad = new PdfPCell(new Phrase((string)dataLine["SalesUM"], font));
                //celTextSenores.Border = 0;
                celTextUnidad.HorizontalAlignment = Element.ALIGN_RIGHT;
                celTextUnidad.VerticalAlignment = Element.ALIGN_CENTER;
                table.AddCell(celTextUnidad);

                PdfPCell celTextVrlUnitario = new PdfPCell(new Phrase(decimal.Parse((string)dataLine["DocUnitPrice"]).ToString("N2"), font));
                //celTextSenores.Border = 0;
                celTextVrlUnitario.Padding = 4;
                //celTextVrlUnitario.PaddingTop = 8;
                celTextVrlUnitario.HorizontalAlignment = Element.ALIGN_RIGHT;
                celTextVrlUnitario.VerticalAlignment = Element.ALIGN_CENTER;
                table.AddCell(celTextVrlUnitario);

                PdfPCell celTextVrlTotal = new PdfPCell(new Phrase(decimal.Parse((string)dataLine["DspDocExtPrice"]).ToString("N2"), font));
                //celTextSenores.Border = 0;
                celTextVrlTotal.HorizontalAlignment = Element.ALIGN_RIGHT;
                celTextVrlTotal.VerticalAlignment = Element.ALIGN_CENTER;
                table.AddCell(celTextVrlTotal);
                return true;
            }
            catch (Exception ex)
            {
                strError += "Error al crear columnas de tabla Items: " + ex.Message;
                return false;
            }
        }

        public bool FacturaNacionalSensuel(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {
            //strError += "Lego";
            //NomArchivo = string.Empty;
            //string Ruta = HttpContext.Current.Server.MapPath("~/PDF/" + NIT + "/");
            //string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            //string RutaImg = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";
            //NomArchivo = NombreInvoice + ".pdf";
            //strError += "inicia Documento pdf\n";

            strError += "Lego";
            NomArchivo = string.Empty;
            string Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
            string RutaImg = $@"{AppDomain.CurrentDomain.BaseDirectory}\LINGERIE_LE_SENSUEL_S.A.S.png";
            NomArchivo = NombreInvoice + ".pdf";
            strError += "inicia Documento pdf\n";
            try
            {
                #region Head
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.

                Document document = new Document(iTextSharp.text.PageSize.LETTER, 20f, 15f, 300f, 30f);

                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;

                PdfDiv divEspacio = new PdfDiv();
                divEspacio.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio.Height = 10;
                divEspacio.Width = 130;

                PdfDiv divEspacio2 = new PdfDiv();
                divEspacio2.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio2.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                divEspacio2.Height = 2;
                divEspacio2.Width = 130;

                //FUENTES----------------------------------------------------------------------------------------------------------
                iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle2 = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustom = FontFactory.GetFont(FontFactory.HELVETICA, 8f, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustomI = FontFactory.GetFont(FontFactory.HELVETICA_OBLIQUE, 8f, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustomBold = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 8, iTextSharp.text.Font.NORMAL);
                //FUENTES---------------------------------------------------------------------------------------------------------
                #endregion

                #region ENCABEZADO

                strError += "inicia la creacion de encabezado\n";
                PdfPTable tableEncabezado = new PdfPTable(3);

                float[] dimTableEncabezado = new float[3];
                dimTableEncabezado[0] = 1.7f;
                dimTableEncabezado[1] = 2.9f;
                dimTableEncabezado[2] = 0.9f;
                tableEncabezado.SetWidths(dimTableEncabezado);
                tableEncabezado.WidthPercentage = 100;

                //TODO: Celda de LOGO LEONISA
                strError += "inicia la creacion de logo en el pdf\n";
                System.Drawing.Image logo = null;
                var requestLogo = WebRequest.Create(RutaImg);

                using (var responseLogo = requestLogo.GetResponse())
                using (var streamLogo = responseLogo.GetResponseStream())
                {
                    logo = Bitmap.FromStream(streamLogo);
                }

                PdfDiv divLogo = new PdfDiv()
                {
                    Float = iTextSharp.text.pdf.PdfDiv.FloatType.LEFT,
                    Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE,
                    BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID,
                    Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK,
                    //BackgroundColor = BaseColor.LIGHT_GRAY,
                    Height = 20,
                    Width = 170,
                };

                iTextSharp.text.Image ImgLogo = iTextSharp.text.Image.GetInstance(logo, BaseColor.WHITE);
                divLogo.BackgroundImage = ImgLogo;

                PdfPCell celLogoLeonisa = new PdfPCell()
                {
                    Border = PdfPCell.NO_BORDER,
                    //Colspan =2,
                    VerticalAlignment = Element.ALIGN_TOP,
                };
                celLogoLeonisa.AddElement(ImgLogo);
                tableEncabezado.AddCell(celLogoLeonisa);
                //---------------------------------------------------------------------------------------------------------
                iTextSharp.text.Font fontAcercadeBold = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 8f, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontAcercade = FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL);

                Paragraph prgTituloAcercade1 = new Paragraph($"{(string)DsInvoiceAR.Tables["Company"].Rows[0]["Name"]}\nNit." +
                    $"{(string)DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"]}-" +
                    $"{CalcularDigitoVerificacion((string)DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"])}\n", fontAcercadeBold);
                prgTituloAcercade1.Alignment = Element.ALIGN_CENTER;

                Paragraph prgTituloAcercade2 = new Paragraph(((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar08"]).Replace(",", "\n"),
                    FontFactory.GetFont(FontFactory.HELVETICA, 7.4f, iTextSharp.text.Font.NORMAL));
                prgTituloAcercade2.Alignment = Element.ALIGN_CENTER;
                PdfPCell celInfoEnc1 = new PdfPCell();
                celInfoEnc1.Padding = 0;
                celInfoEnc1.HorizontalAlignment = Element.ALIGN_CENTER;
                celInfoEnc1.VerticalAlignment = Element.ALIGN_CENTER;
                celInfoEnc1.Border = PdfPCell.NO_BORDER;
                celInfoEnc1.AddElement(prgTituloAcercade1);
                celInfoEnc1.AddElement(prgTituloAcercade2);
                tableEncabezado.AddCell(celInfoEnc1);
                //---------------------------------------------------------------------------------------------------------------------

                PdfPCell celContactoEncabezado = new PdfPCell(new Phrase($"{DsInvoiceAR.Tables["Company"].Rows[0]["Address1"].ToString()}\n" +
                    $"{DsInvoiceAR.Tables["Company"].Rows[0]["City"].ToString()} - " +
                    $"{DsInvoiceAR.Tables["Company"].Rows[0]["State"].ToString()}\n" +
                    $"TELÉFONO: {DsInvoiceAR.Tables["Company"].Rows[0]["PhoneNum"].ToString()}",
                    FontFactory.GetFont(FontFactory.HELVETICA, 7.4f, iTextSharp.text.Font.NORMAL)));
                celContactoEncabezado.HorizontalAlignment = Element.ALIGN_CENTER;
                celContactoEncabezado.PaddingTop = 15;
                celContactoEncabezado.VerticalAlignment = Element.ALIGN_BOTTOM;
                celContactoEncabezado.Border = 0;
                tableEncabezado.AddCell(celContactoEncabezado);

                tableEncabezado.AddCell(new PdfPCell(new Phrase(" "))
                { Border = PdfPCell.NO_BORDER, Colspan = 3, HorizontalAlignment = Element.ALIGN_CENTER });

                strError += "inicia la asignacion del CUFE en el pdf\n";
                tableEncabezado.AddCell(new PdfPCell(new Phrase($"CUFE: {CUFE}", fontTitle))
                { Border = PdfPCell.NO_BORDER, Colspan = 3, HorizontalAlignment = Element.ALIGN_CENTER });

                tableEncabezado.AddCell(new PdfPCell(new Phrase("Representación gráfica factura electrónica", fontTitle))
                { Border = PdfPCell.NO_BORDER, Colspan = 3, HorizontalAlignment = Element.ALIGN_CENTER });

                strError += "se creo el encabezado\n";
                #endregion

                #region Datos del Cliente

                PdfPTable tableInfoCliente = new PdfPTable(5);
                float[] dimTableInfoCliente = new float[5];
                dimTableInfoCliente[0] = 0.2f;
                dimTableInfoCliente[1] = 0.5f;
                dimTableInfoCliente[2] = 2.2f;
                dimTableInfoCliente[3] = 0.02f;
                dimTableInfoCliente[4] = 2.0f;
                tableInfoCliente.SetWidths(dimTableInfoCliente);
                tableInfoCliente.WidthPercentage = 100;

                strError += "inicia la traida de la  informacion del cliente\n";

                PdfPCell celEspacioBeforeSenores = new PdfPCell();
                celEspacioBeforeSenores.Border = 0;
                celEspacioBeforeSenores.BorderWidthBottom = 0.3f;
                tableInfoCliente.AddCell(celEspacioBeforeSenores);

                PdfPCell celTextSenores = new PdfPCell(new Phrase("SEÑORES", fontAcercade));
                celTextSenores.Border = 0;
                celTextSenores.HorizontalAlignment = Element.ALIGN_CENTER;
                celTextSenores.VerticalAlignment = Element.ALIGN_BOTTOM;
                tableInfoCliente.AddCell(celTextSenores);

                PdfPCell celEspacioAfterSenores = new PdfPCell();
                celEspacioAfterSenores.Border = 0;
                celEspacioAfterSenores.BorderWidthBottom = 0.3f;
                tableInfoCliente.AddCell(celEspacioAfterSenores);

                PdfPCell celEspacioInfoCliente = new PdfPCell();
                celEspacioInfoCliente.Border = 0;
                tableInfoCliente.AddCell(celEspacioInfoCliente);

                PdfPCell celLineToDatosFactura = new PdfPCell(new Phrase(""));
                celLineToDatosFactura.Border = 0;
                tableInfoCliente.AddCell(celLineToDatosFactura);

                Paragraph prgDatosCliente = new Paragraph($"\n{ (string)DsInvoiceAR.Tables["Customer"].Rows[0]["Name"] }\n\n" +
                    $"NIT: {(string)DsInvoiceAR.Tables["Customer"].Rows[0]["ResaleID"]}\n\n" +
                    $"DIRECCION: {(string)DsInvoiceAR.Tables["Customer"].Rows[0]["Address1"]}   " +
                    $"TELEFONO: {(string)DsInvoiceAR.Tables["Customer"].Rows[0]["PhoneNum"]}\n\n" +
                    $"{(string)DsInvoiceAR.Tables["Customer"].Rows[0]["City"]} {(string)DsInvoiceAR.Tables["Customer"].Rows[0]["State"]}\n",
                    FontFactory.GetFont(FontFactory.HELVETICA, 7.5F, iTextSharp.text.Font.NORMAL));
                PdfPCell celDatosSenores = new PdfPCell(prgDatosCliente);
                celDatosSenores.Colspan = 3;
                celDatosSenores.Border = 0;
                celDatosSenores.BorderWidthRight = 0.3f;
                celDatosSenores.BorderWidthLeft = 0.3f;
                celDatosSenores.BorderWidthBottom = 0.3f;
                tableInfoCliente.AddCell(celDatosSenores);

                tableInfoCliente.AddCell(celEspacioInfoCliente);
                //---------------------------------------------------------------------------------------------
                PdfPTable tableDatosFactura = new PdfPTable(6);
                float[] dimTableDatosFactura = new float[6];
                dimTableDatosFactura[0] = 0.5f;
                dimTableDatosFactura[1] = 0.5f;
                dimTableDatosFactura[2] = 0.5f;
                dimTableDatosFactura[3] = 0.5f;
                dimTableDatosFactura[4] = 0.5f;
                dimTableDatosFactura[5] = 0.5f;
                tableDatosFactura.SetWidths(dimTableDatosFactura);
                tableDatosFactura.WidthPercentage = 100;

                string TipoFactura = "FACTURA DE VENTA No. ";
                if (InvoiceType == "InvoiceType")
                    TipoFactura = "FACTURA DE VENTA No. ";
                else if (InvoiceType == "CreditNoteType")
                    TipoFactura = "NOTA CREDITO No. ";
                else if (InvoiceType == "DebitNoteType")
                    TipoFactura = "NOTA DEBITO No. ";

                PdfPCell celTextNoFactura = new PdfPCell(new Phrase($"{TipoFactura}" +
                    $"{(string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"]}", fontAcercadeBold));
                celTextNoFactura.Colspan = 6;
                celTextNoFactura.Padding = 4;
                tableDatosFactura.AddCell(celTextNoFactura);

                PdfPCell celTextOrderCompra = new PdfPCell(new Phrase($"ORDEN DE COMPRA " +
                    $"{(string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["OrderNum"]}", fontAcercadeBold));
                celTextOrderCompra.Colspan = 6;
                celTextOrderCompra.Border = 0;
                celTextOrderCompra.Padding = 4;
                tableDatosFactura.AddCell(celTextOrderCompra);

                PdfPCell celTextFechaFactura = new PdfPCell(new Phrase("FECHA FACTURA", fontAcercade));
                celTextFechaFactura.Colspan = 3;
                celTextFechaFactura.HorizontalAlignment = Element.ALIGN_CENTER;
                celTextFechaFactura.VerticalAlignment = Element.ALIGN_TOP;
                celTextFechaFactura.Padding = 4;
                tableDatosFactura.AddCell(celTextFechaFactura);

                PdfPCell celTextFechaVencimiento = new PdfPCell(new Phrase("FECHA VENCIMIENTO", fontAcercade));
                celTextFechaVencimiento.Colspan = 3;
                celTextFechaVencimiento.HorizontalAlignment = Element.ALIGN_CENTER;
                celTextFechaVencimiento.VerticalAlignment = Element.ALIGN_TOP;
                celTextFechaVencimiento.Padding = 4;
                tableDatosFactura.AddCell(celTextFechaVencimiento);
                //----------------------------------------------------------------------------------------------
                PdfPCell celDiaFechaFactura = new PdfPCell(new Phrase(
                    $"{DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).Day}\n\nDÍA",
                    fontAcercade));
                celDiaFechaFactura.HorizontalAlignment = Element.ALIGN_CENTER;
                celDiaFechaFactura.VerticalAlignment = Element.ALIGN_TOP;
                tableDatosFactura.AddCell(celDiaFechaFactura);

                PdfPCell celMesFechaFactura = new PdfPCell(new Phrase(
                    $"{DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).Month}\n\nMES",
                    fontAcercade));
                celMesFechaFactura.HorizontalAlignment = Element.ALIGN_CENTER;
                celMesFechaFactura.VerticalAlignment = Element.ALIGN_TOP;
                tableDatosFactura.AddCell(celMesFechaFactura);

                PdfPCell celAnioFechaFactura = new PdfPCell(new Phrase(
                    $"{DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).Year}\n\nAÑO", fontAcercade));
                celAnioFechaFactura.HorizontalAlignment = Element.ALIGN_CENTER;
                celAnioFechaFactura.VerticalAlignment = Element.ALIGN_TOP;
                tableDatosFactura.AddCell(celAnioFechaFactura);

                PdfPCell celDiaFechaVencimiento = new PdfPCell(new Phrase(
                    $"{DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"]).Day}\n\nDÍA",
                    fontAcercade));
                celDiaFechaVencimiento.HorizontalAlignment = Element.ALIGN_CENTER;
                celDiaFechaVencimiento.VerticalAlignment = Element.ALIGN_TOP;
                tableDatosFactura.AddCell(celDiaFechaVencimiento);

                PdfPCell celMesFechaVencimiento = new PdfPCell(new Phrase(
                    $"{DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"]).Month}\n\nMES",
                    fontAcercade));
                celMesFechaVencimiento.HorizontalAlignment = Element.ALIGN_CENTER;
                celMesFechaVencimiento.VerticalAlignment = Element.ALIGN_TOP;
                tableDatosFactura.AddCell(celMesFechaVencimiento);

                PdfPCell celAnioFechaVencimiento = new PdfPCell(new Phrase(
                    $"{DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"]).Year}\n\nAÑO",
                    fontAcercade));
                celAnioFechaVencimiento.HorizontalAlignment = Element.ALIGN_CENTER;
                celAnioFechaVencimiento.VerticalAlignment = Element.ALIGN_TOP;
                tableDatosFactura.AddCell(celAnioFechaVencimiento);
                //-----------------------------------------------------------------------------------------------
                PdfPCell celToDatosFactura = new PdfPCell(tableDatosFactura)
                {

                };
                tableInfoCliente.AddCell(celToDatosFactura);

                tableEncabezado.AddCell(new PdfPCell(tableInfoCliente)
                {
                    Colspan = 3,
                    Border = PdfPCell.NO_BORDER,
                });

                strError += "inicia se importa la informacion del cliente en el pdf\n";
                #endregion

                #region UNIDADES

                strError += "inicia la creacion de la informacion de unidades\n";

                PdfPTable tableEcabezadoUnidades = new PdfPTable(6);
                float[] dimTableEcabezadoUnidades = new float[6];
                dimTableEcabezadoUnidades[0] = 1.0f;
                dimTableEcabezadoUnidades[1] = 2.5f;
                dimTableEcabezadoUnidades[2] = 1.0f;
                dimTableEcabezadoUnidades[3] = 0.89f;
                dimTableEcabezadoUnidades[4] = 1.0f;
                dimTableEcabezadoUnidades[5] = 1.2f;
                tableEcabezadoUnidades.SetWidths(dimTableEcabezadoUnidades);
                tableEcabezadoUnidades.WidthPercentage = 100;

                PdfPCell celTextReferencia = new PdfPCell(new Phrase("REFERENCIA", fontAcercadeBold));
                //celTextSenores.Border = 0;
                celTextReferencia.Padding = 4;
                celTextReferencia.PaddingTop = 8;
                celTextReferencia.BackgroundColor = BaseColor.LIGHT_GRAY;
                celTextReferencia.HorizontalAlignment = Element.ALIGN_CENTER;
                celTextReferencia.VerticalAlignment = Element.ALIGN_CENTER;
                tableEcabezadoUnidades.AddCell(celTextReferencia);

                PdfPCell celTextDetalle = new PdfPCell(new Phrase("DETALLE", fontAcercadeBold));
                //celTextSenores.Border = 0;
                celTextDetalle.Padding = 4;
                celTextDetalle.PaddingTop = 8;
                celTextDetalle.BackgroundColor = BaseColor.LIGHT_GRAY;
                celTextDetalle.HorizontalAlignment = Element.ALIGN_CENTER;
                celTextDetalle.VerticalAlignment = Element.ALIGN_CENTER;
                tableEcabezadoUnidades.AddCell(celTextDetalle);

                PdfPCell celTextCantidad = new PdfPCell(new Phrase("CANTIDAD", fontAcercadeBold));
                //celTextSenores.Border = 0;
                celTextCantidad.Padding = 4;
                celTextCantidad.PaddingTop = 8;
                celTextCantidad.BackgroundColor = BaseColor.LIGHT_GRAY;
                celTextCantidad.HorizontalAlignment = Element.ALIGN_CENTER;
                celTextCantidad.VerticalAlignment = Element.ALIGN_CENTER;
                tableEcabezadoUnidades.AddCell(celTextCantidad);

                PdfPCell celTextUnidad = new PdfPCell(new Phrase("UNIDAD", fontAcercadeBold));
                //celTextSenores.Border = 0;
                celTextUnidad.Padding = 4;
                celTextUnidad.PaddingTop = 8;
                celTextUnidad.BackgroundColor = BaseColor.LIGHT_GRAY;
                celTextUnidad.HorizontalAlignment = Element.ALIGN_CENTER;
                celTextUnidad.VerticalAlignment = Element.ALIGN_CENTER;
                tableEcabezadoUnidades.AddCell(celTextUnidad);

                PdfPCell celTextVrlUnitario = new PdfPCell(new Phrase("VALOR UNITARIO", fontAcercadeBold));
                //celTextSenores.Border = 0;
                celTextVrlUnitario.Padding = 4;
                //celTextVrlUnitario.PaddingTop = 8;
                celTextVrlUnitario.BackgroundColor = BaseColor.LIGHT_GRAY;
                celTextVrlUnitario.HorizontalAlignment = Element.ALIGN_CENTER;
                celTextVrlUnitario.VerticalAlignment = Element.ALIGN_CENTER;
                tableEcabezadoUnidades.AddCell(celTextVrlUnitario);

                PdfPCell celTextVrlTotal = new PdfPCell(new Phrase("VALOR TOTAL", fontAcercadeBold));
                //celTextSenores.Border = 0;
                celTextVrlTotal.Padding = 4;
                celTextVrlTotal.PaddingTop = 8;
                celTextVrlTotal.BackgroundColor = BaseColor.LIGHT_GRAY;
                celTextVrlTotal.HorizontalAlignment = Element.ALIGN_CENTER;
                celTextVrlTotal.VerticalAlignment = Element.ALIGN_CENTER;
                tableEcabezadoUnidades.AddCell(celTextVrlTotal);

                tableEncabezado.AddCell(new PdfPCell(tableEcabezadoUnidades)
                { Colspan = 3, Border = PdfPCell.NO_BORDER, PaddingTop = 10, });
                //--------------------------------------------------------------------------------------
                PdfPTable tableValUnidades = new PdfPTable(dimTableEcabezadoUnidades);
                tableValUnidades.WidthPercentage = 100;

                decimal totaldescuento = 0;
                strError += "inicia el consumo del metodoque agrega la informacion de cada linea\n";
                foreach (DataRow InvoiceLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
                {
                    totaldescuento += decimal.Parse((string)InvoiceLine["DspDocLessDiscount"]);
                    if (!AddUnidadesSensuel(InvoiceLine, ref tableValUnidades, fontAcercade, DsInvoiceAR))
                    {
                        strError += "error en el metodo que agrega la informacion de cada linea de la factura\n";
                        return false;
                    }
                }

                tableValUnidades.AddCell(new PdfPCell(new Phrase(" ")) { Border = PdfPCell.NO_BORDER, });

                tableValUnidades.AddCell(new PdfPCell(new Phrase("TOTAL UNIDADES", fontCustomBold))
                {
                    BackgroundColor = BaseColor.LIGHT_GRAY,
                });

                tableValUnidades.AddCell(new PdfPCell(new Phrase(
                    decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number01"]).ToString("N2"), fontAcercadeBold))
                {
                    HorizontalAlignment = Element.ALIGN_RIGHT,
                });

                tableValUnidades.AddCell(new PdfPCell(new Phrase(" ")) { Colspan = 3, Border = PdfPCell.NO_BORDER, });
                strError += "se agrego la informacion de la factura OK\n";
                #endregion

                #region Totales, QR y Resolucion

                strError += "inicia la creacion del QR resolucion y totales\n";

                PdfPTable tableTotales = new PdfPTable(4);
                float[] dimTableTotales = new float[4];
                dimTableTotales[0] = 1.2f;
                dimTableTotales[1] = 2.5f;
                dimTableTotales[2] = 1.2f;
                dimTableTotales[3] = 1.2f;
                tableTotales.SetWidths(dimTableTotales);
                tableTotales.WidthPercentage = 100;

                iTextSharp.text.Image QRPdf = iTextSharp.text.Image.GetInstance(QRInvoice, BaseColor.WHITE);

                QRPdf.ScaleAbsolute(80f, 80f);
                PdfDiv divQR = new PdfDiv();
                divQR.Float = iTextSharp.text.pdf.PdfDiv.FloatType.LEFT;
                divQR.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divQR.BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID;
                divQR.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                divQR.Height = 80;
                divQR.Width = 80;
                divQR.AddElement(QRPdf);

                iTextSharp.text.pdf.PdfPCell celImgQR = new iTextSharp.text.pdf.PdfPCell();
                celImgQR.Colspan = 1;
                celImgQR.BorderWidthRight = 0;
                celImgQR.Padding = 3;
                celImgQR.PaddingLeft = 15;
                celImgQR.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celImgQR.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                celImgQR.AddElement(QRPdf);
                tableTotales.AddCell(celImgQR);

                string valFacturaNumero = string.Empty;
                //
                if (((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["CurrencyCode"]).ToUpper() != "COP")
                    valFacturaNumero = $"({decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"]).ToString("C2")})";


                string textResolucion = (string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar03"];
                textResolucion += $"\n\nSON: " +
                                    $"{Nroenletras(decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"]).ToString("N2"))} " +
                                    $"{valFacturaNumero}";
                PdfPCell celResolucion = new PdfPCell(new Phrase(textResolucion, fontAcercade));
                celResolucion.BorderWidthLeft = 0;
                celResolucion.Padding = 3;
                celResolucion.HorizontalAlignment = Element.ALIGN_CENTER;
                celResolucion.VerticalAlignment = Element.ALIGN_CENTER;
                tableTotales.AddCell(celResolucion);

                PdfPTable tableTextTotales = new PdfPTable(1);

                PdfPCell celTextTotalBruto = new PdfPCell(new Phrase("TOTAL BRUTO", fontAcercade));
                celTextTotalBruto.Padding = 4;
                tableTextTotales.AddCell(celTextTotalBruto);

                tableTextTotales.AddCell(new PdfPCell(new Phrase("DESCUENTO", fontAcercade)) { Padding = 4, });

                tableTextTotales.AddCell(new PdfPCell(new Phrase("SUBTOTAL", fontAcercadeBold)) { Padding = 4, });

                PdfPCell celTextIva = new PdfPCell(new Phrase("IVA", fontAcercade));
                celTextIva.Padding = 4;
                tableTextTotales.AddCell(celTextIva);

                PdfPCell celTextRFuenteIva = new PdfPCell(new Phrase("RETEFUENTE IVA", fontAcercade));
                celTextRFuenteIva.Padding = 4;
                tableTextTotales.AddCell(celTextRFuenteIva);

                PdfPCell celTextRFuenteRenta = new PdfPCell(new Phrase("RETEFUENTE RENTA", fontAcercade));
                celTextRFuenteRenta.Padding = 4;
                tableTextTotales.AddCell(celTextRFuenteRenta);

                PdfPCell celTextRFuenteIca = new PdfPCell(new Phrase("RETEFUENTE ICA", fontAcercade));
                celTextRFuenteIca.Padding = 4;
                tableTextTotales.AddCell(celTextRFuenteIca);

                PdfPCell celTextValorTotal = new PdfPCell(new Phrase("VALOR TOTAL", fontAcercadeBold));
                celTextValorTotal.Padding = 4;
                tableTextTotales.AddCell(celTextValorTotal);

                PdfPCell celTextTotales = new PdfPCell(tableTextTotales);
                celTextTotales.BorderWidthLeft = 0;
                celTextTotales.BorderWidthTop = 0;
                tableTotales.AddCell(celTextTotales);
                //--------------------------------------------------------------------------------------------
                PdfPTable tableValTotales = new PdfPTable(1);

                var totalBruto = decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubTotal"]) +
                    Math.Abs(totaldescuento);

                //PdfPCell celValTotalBruto = new PdfPCell(new Phrase(
                //    $"{decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubTotal"]).ToString("N2")}",
                //    fontAcercade));

                PdfPCell celValTotalBruto = new PdfPCell(new Phrase(
                    $"{totalBruto.ToString("N2")}",
                    fontAcercade));
                celValTotalBruto.BorderWidthTop = 0.5f;
                celValTotalBruto.Padding = 4;
                celValTotalBruto.HorizontalAlignment = Element.ALIGN_RIGHT;
                tableValTotales.AddCell(celValTotalBruto);

                tableValTotales.AddCell(new PdfPCell(new Phrase(totaldescuento.ToString("N2"), fontAcercade))
                {
                    Padding = 4,
                    HorizontalAlignment = Element.ALIGN_RIGHT,
                });

                //var subtotal = decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubTotal"]) -
                //   Math.Abs(totaldescuento);

                var subtotal = decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubTotal"]);

                tableValTotales.AddCell(new PdfPCell(new Phrase(
                    subtotal.ToString("N2"),
                    fontAcercadeBold))
                {
                    Padding = 4,
                    HorizontalAlignment = Element.ALIGN_RIGHT,
                });

                decimal Iva = GetValImpuestoByID("01", DsInvoiceAR);
                PdfPCell celValIva = new PdfPCell(new Phrase($"{Iva.ToString("N2")}", fontAcercade));
                celValIva.Padding = 4;
                celValIva.HorizontalAlignment = Element.ALIGN_RIGHT;
                tableValTotales.AddCell(celValIva);

                decimal RFuenteIva = GetValImpuestoByID("0B", DsInvoiceAR);
                PdfPCell celValRFuenteIva = new PdfPCell(new Phrase(RFuenteIva.ToString("N2"), fontAcercade));
                celValRFuenteIva.Padding = 4;
                celValRFuenteIva.HorizontalAlignment = Element.ALIGN_RIGHT;
                tableValTotales.AddCell(celValRFuenteIva);

                decimal RFuenteRenta = GetValImpuestoByID("0D", DsInvoiceAR);
                PdfPCell celValRFuenteRenta = new PdfPCell(new Phrase(RFuenteRenta.ToString("N2"), fontAcercade));
                celValRFuenteRenta.Padding = 4;
                celValRFuenteRenta.HorizontalAlignment = Element.ALIGN_RIGHT;
                tableValTotales.AddCell(celValRFuenteRenta);

                decimal RFuenteIca = GetValImpuestoByID("0C", DsInvoiceAR);
                PdfPCell celValRFuenteIca = new PdfPCell(new Phrase(RFuenteIca.ToString("N2"), fontAcercade));
                celValRFuenteIca.Padding = 4;
                celValRFuenteIca.HorizontalAlignment = Element.ALIGN_RIGHT;
                tableValTotales.AddCell(celValRFuenteIca);

                PdfPCell celValVrlTotal = new PdfPCell(new Phrase(
                    $"{decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"]).ToString("N2")}",
                    fontAcercadeBold));
                celValVrlTotal.Padding = 4;
                celValVrlTotal.HorizontalAlignment = Element.ALIGN_RIGHT;
                tableValTotales.AddCell(celValVrlTotal);

                PdfPCell celValTotales = new PdfPCell(tableValTotales);
                celValTotales.BorderWidthLeft = 0;
                tableTotales.AddCell(celValTotales);

                strError += "se agrego satisfactoriamente QR, resoluciones y totales\n";
                #endregion

                strError += "inicia el consumo de event page para el manejo de los encabezados\n";
                writer.PageEvent = new EventPageLonisa1(tableEncabezado);
                document.Open();
                strError += "cosumo de clase event page OK\n";

                strError += "inicia la adicion de cada una de las tablas creadas antes y divs en el documento\n";

                #region Exit
                document.Add(tableValUnidades);
                document.Add(divEspacio);
                document.Add(tableTotales);
                strError += "se agregaron todas las tablas y divs correctamente OK\n";
                /*PIE DE PAGINA*/
                PdfContentByte pCb = writer.DirectContent; PdfContentByte pCb2 = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();
                strError += "inicia metodos para agregar numeracion de pagina y CONTINUE/CONTINUA. Ya se cerro el documento\n";
                AddPageNumberPagTo($"{Ruta}{NomArchivo}", dimxLeonisa, 755, fontTitle);
                AddPageContinueLeonisa($"{Ruta}{NomArchivo}", dimxLeonisa, 745, fontTitle);
                RutaPdf = NomArchivo;
                strError += "inicia se agrego la numeracion satisfactoriamente OK\n";
                return true;
                #endregion
            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.Message;
                return false;
            }
        }

        public bool NotaCreditoSensuel(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {
            strError += "Lego";
            NomArchivo = string.Empty;
            string Ruta = HttpContext.Current.Server.MapPath("~/PDF/" + NIT + "/");
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            string RutaImg = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";
            NomArchivo = NombreInvoice + ".pdf";
            strError += "inicia Documento pdf\n";

            //strError += "Lego";
            //NomArchivo = string.Empty;
            //string Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
            //string RutaImg = $@"{AppDomain.CurrentDomain.BaseDirectory}\DUNNINGTON_CORPORATION.png";
            //NomArchivo = NombreInvoice + ".pdf";
            //strError += "inicia Documento pdf\n";
            try
            {
                #region Head
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.

                Document document = new Document(iTextSharp.text.PageSize.LETTER, 20f, 15f, 300f, 30f);

                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;

                PdfDiv divEspacio = new PdfDiv();
                divEspacio.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio.Height = 10;
                divEspacio.Width = 130;

                PdfDiv divEspacio2 = new PdfDiv();
                divEspacio2.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio2.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                divEspacio2.Height = 2;
                divEspacio2.Width = 130;

                //FUENTES----------------------------------------------------------------------------------------------------------
                iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle2 = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustom = FontFactory.GetFont(FontFactory.HELVETICA, 8f, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustomI = FontFactory.GetFont(FontFactory.HELVETICA_OBLIQUE, 8f, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustomBold = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 8, iTextSharp.text.Font.NORMAL);
                //FUENTES---------------------------------------------------------------------------------------------------------
                #endregion

                #region ENCABEZADO

                strError += "inicia la creacion de encabezado\n";
                PdfPTable tableEncabezado = new PdfPTable(3);

                float[] dimTableEncabezado = new float[3];
                dimTableEncabezado[0] = 1.7f;
                dimTableEncabezado[1] = 2.9f;
                dimTableEncabezado[2] = 0.9f;
                tableEncabezado.SetWidths(dimTableEncabezado);
                tableEncabezado.WidthPercentage = 100;

                //TODO: Celda de LOGO LEONISA
                strError += "inicia la creacion de logo en el pdf\n";
                System.Drawing.Image logo = null;
                var requestLogo = WebRequest.Create(RutaImg);

                using (var responseLogo = requestLogo.GetResponse())
                using (var streamLogo = responseLogo.GetResponseStream())
                {
                    logo = Bitmap.FromStream(streamLogo);
                }

                PdfDiv divLogo = new PdfDiv()
                {
                    Float = iTextSharp.text.pdf.PdfDiv.FloatType.LEFT,
                    Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE,
                    BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID,
                    Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK,
                    //BackgroundColor = BaseColor.LIGHT_GRAY,
                    Height = 20,
                    Width = 170,
                };

                iTextSharp.text.Image ImgLogo = iTextSharp.text.Image.GetInstance(logo, BaseColor.WHITE);
                divLogo.BackgroundImage = ImgLogo;

                PdfPCell celLogoLeonisa = new PdfPCell()
                {
                    Border = PdfPCell.NO_BORDER,
                    //Colspan =2,
                    VerticalAlignment = Element.ALIGN_TOP,
                };
                celLogoLeonisa.AddElement(ImgLogo);
                tableEncabezado.AddCell(celLogoLeonisa);
                //---------------------------------------------------------------------------------------------------------
                iTextSharp.text.Font fontAcercadeBold = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 8f, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontAcercade = FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL);

                Paragraph prgTituloAcercade1 = new Paragraph($"{(string)DsInvoiceAR.Tables["Company"].Rows[0]["Name"]}\nNit." +
                    $"{(string)DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"]}-" +
                    $"{CalcularDigitoVerificacion((string)DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"])}\n", fontAcercadeBold);
                prgTituloAcercade1.Alignment = Element.ALIGN_CENTER;

                Paragraph prgTituloAcercade2 = new Paragraph(((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar08"]).Replace(",", "\n"),
                    FontFactory.GetFont(FontFactory.HELVETICA, 7.4f, iTextSharp.text.Font.NORMAL));
                prgTituloAcercade2.Alignment = Element.ALIGN_CENTER;
                PdfPCell celInfoEnc1 = new PdfPCell();
                celInfoEnc1.Padding = 0;
                celInfoEnc1.HorizontalAlignment = Element.ALIGN_CENTER;
                celInfoEnc1.VerticalAlignment = Element.ALIGN_CENTER;
                celInfoEnc1.Border = PdfPCell.NO_BORDER;
                celInfoEnc1.AddElement(prgTituloAcercade1);
                celInfoEnc1.AddElement(prgTituloAcercade2);
                tableEncabezado.AddCell(celInfoEnc1);
                //---------------------------------------------------------------------------------------------------------------------

                PdfPCell celContactoEncabezado = new PdfPCell(new Phrase($"{DsInvoiceAR.Tables["Company"].Rows[0]["Address1"].ToString()}\n" +
                    $"{DsInvoiceAR.Tables["Company"].Rows[0]["City"].ToString()} - " +
                    $"{DsInvoiceAR.Tables["Company"].Rows[0]["State"].ToString()}\n" +
                    $"TELÉFONO: {DsInvoiceAR.Tables["Company"].Rows[0]["PhoneNum"].ToString()}",
                    FontFactory.GetFont(FontFactory.HELVETICA, 7.4f, iTextSharp.text.Font.NORMAL)));
                celContactoEncabezado.HorizontalAlignment = Element.ALIGN_CENTER;
                celContactoEncabezado.PaddingTop = 15;
                celContactoEncabezado.VerticalAlignment = Element.ALIGN_BOTTOM;
                celContactoEncabezado.Border = 0;
                tableEncabezado.AddCell(celContactoEncabezado);

                tableEncabezado.AddCell(new PdfPCell(new Phrase(" "))
                { Border = PdfPCell.NO_BORDER, Colspan = 3, HorizontalAlignment = Element.ALIGN_CENTER });

                strError += "inicia la asignacion del CUFE en el pdf\n";
                tableEncabezado.AddCell(new PdfPCell(new Phrase($"CUFE: {CUFE}", fontTitle))
                { Border = PdfPCell.NO_BORDER, Colspan = 3, HorizontalAlignment = Element.ALIGN_CENTER });

                tableEncabezado.AddCell(new PdfPCell(new Phrase("Representación gráfica factura electrónica", fontTitle))
                { Border = PdfPCell.NO_BORDER, Colspan = 3, HorizontalAlignment = Element.ALIGN_CENTER });

                strError += "se creo el encabezado\n";
                #endregion

                #region Datos del Cliente

                PdfPTable tableInfoCliente = new PdfPTable(5);
                float[] dimTableInfoCliente = new float[5];
                dimTableInfoCliente[0] = 0.2f;
                dimTableInfoCliente[1] = 0.5f;
                dimTableInfoCliente[2] = 2.2f;
                dimTableInfoCliente[3] = 0.02f;
                dimTableInfoCliente[4] = 2.0f;
                tableInfoCliente.SetWidths(dimTableInfoCliente);
                tableInfoCliente.WidthPercentage = 100;

                strError += "inicia la traida de la  informacion del cliente\n";

                PdfPCell celEspacioBeforeSenores = new PdfPCell();
                celEspacioBeforeSenores.Border = 0;
                celEspacioBeforeSenores.BorderWidthBottom = 0.3f;
                tableInfoCliente.AddCell(celEspacioBeforeSenores);

                PdfPCell celTextSenores = new PdfPCell(new Phrase("SEÑORES", fontAcercade));
                celTextSenores.Border = 0;
                celTextSenores.HorizontalAlignment = Element.ALIGN_CENTER;
                celTextSenores.VerticalAlignment = Element.ALIGN_BOTTOM;
                tableInfoCliente.AddCell(celTextSenores);

                PdfPCell celEspacioAfterSenores = new PdfPCell();
                celEspacioAfterSenores.Border = 0;
                celEspacioAfterSenores.BorderWidthBottom = 0.3f;
                tableInfoCliente.AddCell(celEspacioAfterSenores);

                PdfPCell celEspacioInfoCliente = new PdfPCell();
                celEspacioInfoCliente.Border = 0;
                tableInfoCliente.AddCell(celEspacioInfoCliente);

                PdfPCell celLineToDatosFactura = new PdfPCell(new Phrase(""));
                celLineToDatosFactura.Border = 0;
                tableInfoCliente.AddCell(celLineToDatosFactura);

                Paragraph prgDatosCliente = new Paragraph($"\n{ (string)DsInvoiceAR.Tables["Customer"].Rows[0]["Name"] }\n\n" +
                    $"NIT: {(string)DsInvoiceAR.Tables["Customer"].Rows[0]["ResaleID"]}\n\n" +
                    $"DIRECCION: {(string)DsInvoiceAR.Tables["Customer"].Rows[0]["Address1"]}   " +
                    $"TELEFONO: {(string)DsInvoiceAR.Tables["Customer"].Rows[0]["PhoneNum"]}\n\n" +
                    $"{(string)DsInvoiceAR.Tables["Customer"].Rows[0]["City"]} {(string)DsInvoiceAR.Tables["Customer"].Rows[0]["State"]}\n",
                    FontFactory.GetFont(FontFactory.HELVETICA, 7.5F, iTextSharp.text.Font.NORMAL));
                PdfPCell celDatosSenores = new PdfPCell(prgDatosCliente);
                celDatosSenores.Colspan = 3;
                celDatosSenores.Border = 0;
                celDatosSenores.BorderWidthRight = 0.3f;
                celDatosSenores.BorderWidthLeft = 0.3f;
                celDatosSenores.BorderWidthBottom = 0.3f;
                tableInfoCliente.AddCell(celDatosSenores);

                tableInfoCliente.AddCell(celEspacioInfoCliente);
                //---------------------------------------------------------------------------------------------
                PdfPTable tableDatosFactura = new PdfPTable(6);
                float[] dimTableDatosFactura = new float[6];
                dimTableDatosFactura[0] = 0.5f;
                dimTableDatosFactura[1] = 0.5f;
                dimTableDatosFactura[2] = 0.5f;
                dimTableDatosFactura[3] = 0.5f;
                dimTableDatosFactura[4] = 0.5f;
                dimTableDatosFactura[5] = 0.5f;
                tableDatosFactura.SetWidths(dimTableDatosFactura);
                tableDatosFactura.WidthPercentage = 100;

                string TipoFactura = "FACTURA DE VENTA No. ";
                if (InvoiceType == "InvoiceType")
                    TipoFactura = "FACTURA DE VENTA No. ";
                else if (InvoiceType == "CreditNoteType")
                    TipoFactura = "NOTA CREDITO No. ";
                else if (InvoiceType == "DebitNoteType")
                    TipoFactura = "NOTA DEBITO No. ";

                PdfPCell celTextNoFactura = new PdfPCell(new Phrase($"{TipoFactura}" +
                    $"{(string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"]}", fontAcercadeBold));
                celTextNoFactura.Colspan = 6;
                celTextNoFactura.Padding = 4;
                tableDatosFactura.AddCell(celTextNoFactura);

                PdfPCell celTextOrderCompra = new PdfPCell(new Phrase($"NOTA CREDITO " +
                    $"{(string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["OrderNum"]}", fontAcercadeBold));
                celTextOrderCompra.Colspan = 6;
                celTextOrderCompra.Border = 0;
                celTextOrderCompra.Padding = 4;
                tableDatosFactura.AddCell(celTextOrderCompra);

                PdfPCell celTextFechaFactura = new PdfPCell(new Phrase("FECHA FACTURA", fontAcercade));
                celTextFechaFactura.Colspan = 3;
                celTextFechaFactura.HorizontalAlignment = Element.ALIGN_CENTER;
                celTextFechaFactura.VerticalAlignment = Element.ALIGN_TOP;
                celTextFechaFactura.Padding = 4;
                tableDatosFactura.AddCell(celTextFechaFactura);

                PdfPCell celTextFechaVencimiento = new PdfPCell(new Phrase("FECHA VENCIMIENTO", fontAcercade));
                celTextFechaVencimiento.Colspan = 3;
                celTextFechaVencimiento.HorizontalAlignment = Element.ALIGN_CENTER;
                celTextFechaVencimiento.VerticalAlignment = Element.ALIGN_TOP;
                celTextFechaVencimiento.Padding = 4;
                tableDatosFactura.AddCell(celTextFechaVencimiento);
                //----------------------------------------------------------------------------------------------
                PdfPCell celDiaFechaFactura = new PdfPCell(new Phrase(
                    $"{DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).Day}\n\nDÍA",
                    fontAcercade));
                celDiaFechaFactura.HorizontalAlignment = Element.ALIGN_CENTER;
                celDiaFechaFactura.VerticalAlignment = Element.ALIGN_TOP;
                tableDatosFactura.AddCell(celDiaFechaFactura);

                PdfPCell celMesFechaFactura = new PdfPCell(new Phrase(
                    $"{DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).Month}\n\nMES",
                    fontAcercade));
                celMesFechaFactura.HorizontalAlignment = Element.ALIGN_CENTER;
                celMesFechaFactura.VerticalAlignment = Element.ALIGN_TOP;
                tableDatosFactura.AddCell(celMesFechaFactura);

                PdfPCell celAnioFechaFactura = new PdfPCell(new Phrase(
                    $"{DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).Year}\n\nAÑO", fontAcercade));
                celAnioFechaFactura.HorizontalAlignment = Element.ALIGN_CENTER;
                celAnioFechaFactura.VerticalAlignment = Element.ALIGN_TOP;
                tableDatosFactura.AddCell(celAnioFechaFactura);

                PdfPCell celDiaFechaVencimiento = new PdfPCell(new Phrase(
                    $"{DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"]).Day}\n\nDÍA",
                    fontAcercade));
                celDiaFechaVencimiento.HorizontalAlignment = Element.ALIGN_CENTER;
                celDiaFechaVencimiento.VerticalAlignment = Element.ALIGN_TOP;
                tableDatosFactura.AddCell(celDiaFechaVencimiento);

                PdfPCell celMesFechaVencimiento = new PdfPCell(new Phrase(
                    $"{DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"]).Month}\n\nMES",
                    fontAcercade));
                celMesFechaVencimiento.HorizontalAlignment = Element.ALIGN_CENTER;
                celMesFechaVencimiento.VerticalAlignment = Element.ALIGN_TOP;
                tableDatosFactura.AddCell(celMesFechaVencimiento);

                PdfPCell celAnioFechaVencimiento = new PdfPCell(new Phrase(
                    $"{DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"]).Year}\n\nAÑO",
                    fontAcercade));
                celAnioFechaVencimiento.HorizontalAlignment = Element.ALIGN_CENTER;
                celAnioFechaVencimiento.VerticalAlignment = Element.ALIGN_TOP;
                tableDatosFactura.AddCell(celAnioFechaVencimiento);
                //-----------------------------------------------------------------------------------------------
                PdfPCell celToDatosFactura = new PdfPCell(tableDatosFactura)
                {

                };
                tableInfoCliente.AddCell(celToDatosFactura);

                tableEncabezado.AddCell(new PdfPCell(tableInfoCliente)
                {
                    Colspan = 3,
                    Border = PdfPCell.NO_BORDER,
                });

                strError += "inicia se importa la informacion del cliente en el pdf\n";
                #endregion

                #region UNIDADES

                strError += "inicia la creacion de la informacion de unidades\n";

                PdfPTable tableEcabezadoUnidades = new PdfPTable(6);
                float[] dimTableEcabezadoUnidades = new float[6];
                dimTableEcabezadoUnidades[0] = 1.0f;
                dimTableEcabezadoUnidades[1] = 2.5f;
                dimTableEcabezadoUnidades[2] = 1.0f;
                dimTableEcabezadoUnidades[3] = 0.89f;
                dimTableEcabezadoUnidades[4] = 1.0f;
                dimTableEcabezadoUnidades[5] = 1.2f;
                tableEcabezadoUnidades.SetWidths(dimTableEcabezadoUnidades);
                tableEcabezadoUnidades.WidthPercentage = 100;

                PdfPCell celTextReferencia = new PdfPCell(new Phrase("REFERENCIA", fontAcercadeBold));
                //celTextSenores.Border = 0;
                celTextReferencia.Padding = 4;
                celTextReferencia.PaddingTop = 8;
                celTextReferencia.BackgroundColor = BaseColor.LIGHT_GRAY;
                celTextReferencia.HorizontalAlignment = Element.ALIGN_CENTER;
                celTextReferencia.VerticalAlignment = Element.ALIGN_CENTER;
                tableEcabezadoUnidades.AddCell(celTextReferencia);

                PdfPCell celTextDetalle = new PdfPCell(new Phrase("DETALLE", fontAcercadeBold));
                //celTextSenores.Border = 0;
                celTextDetalle.Padding = 4;
                celTextDetalle.PaddingTop = 8;
                celTextDetalle.BackgroundColor = BaseColor.LIGHT_GRAY;
                celTextDetalle.HorizontalAlignment = Element.ALIGN_CENTER;
                celTextDetalle.VerticalAlignment = Element.ALIGN_CENTER;
                tableEcabezadoUnidades.AddCell(celTextDetalle);

                PdfPCell celTextCantidad = new PdfPCell(new Phrase("CANTIDAD", fontAcercadeBold));
                //celTextSenores.Border = 0;
                celTextCantidad.Padding = 4;
                celTextCantidad.PaddingTop = 8;
                celTextCantidad.BackgroundColor = BaseColor.LIGHT_GRAY;
                celTextCantidad.HorizontalAlignment = Element.ALIGN_CENTER;
                celTextCantidad.VerticalAlignment = Element.ALIGN_CENTER;
                tableEcabezadoUnidades.AddCell(celTextCantidad);

                PdfPCell celTextUnidad = new PdfPCell(new Phrase("UNIDAD", fontAcercadeBold));
                //celTextSenores.Border = 0;
                celTextUnidad.Padding = 4;
                celTextUnidad.PaddingTop = 8;
                celTextUnidad.BackgroundColor = BaseColor.LIGHT_GRAY;
                celTextUnidad.HorizontalAlignment = Element.ALIGN_CENTER;
                celTextUnidad.VerticalAlignment = Element.ALIGN_CENTER;
                tableEcabezadoUnidades.AddCell(celTextUnidad);

                PdfPCell celTextVrlUnitario = new PdfPCell(new Phrase("VALOR UNITARIO", fontAcercadeBold));
                //celTextSenores.Border = 0;
                celTextVrlUnitario.Padding = 4;
                //celTextVrlUnitario.PaddingTop = 8;
                celTextVrlUnitario.BackgroundColor = BaseColor.LIGHT_GRAY;
                celTextVrlUnitario.HorizontalAlignment = Element.ALIGN_CENTER;
                celTextVrlUnitario.VerticalAlignment = Element.ALIGN_CENTER;
                tableEcabezadoUnidades.AddCell(celTextVrlUnitario);

                PdfPCell celTextVrlTotal = new PdfPCell(new Phrase("VALOR TOTAL", fontAcercadeBold));
                //celTextSenores.Border = 0;
                celTextVrlTotal.Padding = 4;
                celTextVrlTotal.PaddingTop = 8;
                celTextVrlTotal.BackgroundColor = BaseColor.LIGHT_GRAY;
                celTextVrlTotal.HorizontalAlignment = Element.ALIGN_CENTER;
                celTextVrlTotal.VerticalAlignment = Element.ALIGN_CENTER;
                tableEcabezadoUnidades.AddCell(celTextVrlTotal);

                tableEncabezado.AddCell(new PdfPCell(tableEcabezadoUnidades)
                { Colspan = 3, Border = PdfPCell.NO_BORDER, PaddingTop = 10, });
                //--------------------------------------------------------------------------------------
                PdfPTable tableValUnidades = new PdfPTable(dimTableEcabezadoUnidades);
                tableValUnidades.WidthPercentage = 100;

                decimal totaldescuento = 0;
                strError += "inicia el consumo del metodoque agrega la informacion de cada linea\n";
                foreach (DataRow InvoiceLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
                {
                    totaldescuento += decimal.Parse((string)InvoiceLine["DspDocLessDiscount"]);
                    if (!AddUnidadesSensuel(InvoiceLine, ref tableValUnidades, fontAcercade, DsInvoiceAR))
                    {
                        strError += "error en el metodo que agrega la informacion de cada linea de la factura\n";
                        return false;
                    }
                }

                tableValUnidades.AddCell(new PdfPCell(new Phrase(" ")) { Border = PdfPCell.NO_BORDER, });

                tableValUnidades.AddCell(new PdfPCell(new Phrase("TOTAL UNIDADES", fontCustomBold))
                {
                    BackgroundColor = BaseColor.LIGHT_GRAY,
                });

                tableValUnidades.AddCell(new PdfPCell(new Phrase(
                    decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number01"]).ToString("N2"), fontAcercadeBold))
                {
                    HorizontalAlignment = Element.ALIGN_RIGHT,
                });

                tableValUnidades.AddCell(new PdfPCell(new Phrase(" ")) { Colspan = 3, Border = PdfPCell.NO_BORDER, });
                strError += "se agrego la informacion de la factura OK\n";
                #endregion

                #region Totales, QR y Resolucion

                strError += "inicia la creacion del QR resolucion y totales\n";

                PdfPTable tableTotales = new PdfPTable(4);
                float[] dimTableTotales = new float[4];
                dimTableTotales[0] = 1.2f;
                dimTableTotales[1] = 2.5f;
                dimTableTotales[2] = 1.2f;
                dimTableTotales[3] = 1.2f;
                tableTotales.SetWidths(dimTableTotales);
                tableTotales.WidthPercentage = 100;

                iTextSharp.text.Image QRPdf = iTextSharp.text.Image.GetInstance(QRInvoice, BaseColor.WHITE);

                QRPdf.ScaleAbsolute(80f, 80f);
                PdfDiv divQR = new PdfDiv();
                divQR.Float = iTextSharp.text.pdf.PdfDiv.FloatType.LEFT;
                divQR.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divQR.BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID;
                divQR.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                divQR.Height = 80;
                divQR.Width = 80;
                divQR.AddElement(QRPdf);

                iTextSharp.text.pdf.PdfPCell celImgQR = new iTextSharp.text.pdf.PdfPCell();
                celImgQR.Colspan = 1;
                celImgQR.BorderWidthRight = 0;
                celImgQR.Padding = 3;
                celImgQR.PaddingLeft = 15;
                celImgQR.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celImgQR.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                celImgQR.AddElement(QRPdf);
                tableTotales.AddCell(celImgQR);

                string valFacturaNumero = string.Empty;
                //
                if (((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["CurrencyCode"]).ToUpper() != "COP")
                    valFacturaNumero = $"({decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"]).ToString("C2")})";


                string textResolucion = (string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar03"];
                textResolucion += $"\n\nSON: " +
                                    $"{Nroenletras(decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"]).ToString("N2"))} " +
                                    $"{valFacturaNumero}";
                PdfPCell celResolucion = new PdfPCell(new Phrase(textResolucion, fontAcercade));
                celResolucion.BorderWidthLeft = 0;
                celResolucion.Padding = 3;
                celResolucion.HorizontalAlignment = Element.ALIGN_CENTER;
                celResolucion.VerticalAlignment = Element.ALIGN_CENTER;
                tableTotales.AddCell(celResolucion);

                PdfPTable tableTextTotales = new PdfPTable(1);

                PdfPCell celTextTotalBruto = new PdfPCell(new Phrase("TOTAL BRUTO", fontAcercade));
                celTextTotalBruto.Padding = 4;
                tableTextTotales.AddCell(celTextTotalBruto);

                tableTextTotales.AddCell(new PdfPCell(new Phrase("DESCUENTO", fontAcercade)) { Padding = 4, });

                tableTextTotales.AddCell(new PdfPCell(new Phrase("SUBTOTAL", fontAcercadeBold)) { Padding = 4, });

                PdfPCell celTextIva = new PdfPCell(new Phrase("IVA", fontAcercade));
                celTextIva.Padding = 4;
                tableTextTotales.AddCell(celTextIva);

                PdfPCell celTextRFuenteIva = new PdfPCell(new Phrase("RETEFUENTE IVA", fontAcercade));
                celTextRFuenteIva.Padding = 4;
                tableTextTotales.AddCell(celTextRFuenteIva);

                PdfPCell celTextRFuenteRenta = new PdfPCell(new Phrase("RETEFUENTE RENTA", fontAcercade));
                celTextRFuenteRenta.Padding = 4;
                tableTextTotales.AddCell(celTextRFuenteRenta);

                PdfPCell celTextRFuenteIca = new PdfPCell(new Phrase("RETEFUENTE ICA", fontAcercade));
                celTextRFuenteIca.Padding = 4;
                tableTextTotales.AddCell(celTextRFuenteIca);

                PdfPCell celTextValorTotal = new PdfPCell(new Phrase("VALOR TOTAL", fontAcercadeBold));
                celTextValorTotal.Padding = 4;
                tableTextTotales.AddCell(celTextValorTotal);

                PdfPCell celTextTotales = new PdfPCell(tableTextTotales);
                celTextTotales.BorderWidthLeft = 0;
                celTextTotales.BorderWidthTop = 0;
                tableTotales.AddCell(celTextTotales);
                //--------------------------------------------------------------------------------------------
                PdfPTable tableValTotales = new PdfPTable(1);

                var totalBruto = decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubTotal"]) +
                    Math.Abs(totaldescuento);

                //PdfPCell celValTotalBruto = new PdfPCell(new Phrase(
                //    $"{decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubTotal"]).ToString("N2")}",
                //    fontAcercade));

                PdfPCell celValTotalBruto = new PdfPCell(new Phrase(
                    $"{totalBruto.ToString("N2")}",
                    fontAcercade));
                celValTotalBruto.BorderWidthTop = 0.5f;
                celValTotalBruto.Padding = 4;
                celValTotalBruto.HorizontalAlignment = Element.ALIGN_RIGHT;
                tableValTotales.AddCell(celValTotalBruto);

                tableValTotales.AddCell(new PdfPCell(new Phrase(totaldescuento.ToString("N2"), fontAcercade))
                {
                    Padding = 4,
                    HorizontalAlignment = Element.ALIGN_RIGHT,
                });

                //var subtotal = decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubTotal"]) -
                //   Math.Abs(totaldescuento);

                var subtotal = decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubTotal"]);

                tableValTotales.AddCell(new PdfPCell(new Phrase(
                    subtotal.ToString("N2"),
                    fontAcercadeBold))
                {
                    Padding = 4,
                    HorizontalAlignment = Element.ALIGN_RIGHT,
                });

                decimal Iva = GetValImpuestoByID("01", DsInvoiceAR);
                PdfPCell celValIva = new PdfPCell(new Phrase($"{Iva.ToString("N2")}", fontAcercade));
                celValIva.Padding = 4;
                celValIva.HorizontalAlignment = Element.ALIGN_RIGHT;
                tableValTotales.AddCell(celValIva);

                decimal RFuenteIva = GetValImpuestoByID("0B", DsInvoiceAR);
                PdfPCell celValRFuenteIva = new PdfPCell(new Phrase(RFuenteIva.ToString("N2"), fontAcercade));
                celValRFuenteIva.Padding = 4;
                celValRFuenteIva.HorizontalAlignment = Element.ALIGN_RIGHT;
                tableValTotales.AddCell(celValRFuenteIva);

                decimal RFuenteRenta = GetValImpuestoByID("0D", DsInvoiceAR);
                PdfPCell celValRFuenteRenta = new PdfPCell(new Phrase(RFuenteRenta.ToString("N2"), fontAcercade));
                celValRFuenteRenta.Padding = 4;
                celValRFuenteRenta.HorizontalAlignment = Element.ALIGN_RIGHT;
                tableValTotales.AddCell(celValRFuenteRenta);

                decimal RFuenteIca = GetValImpuestoByID("0C", DsInvoiceAR);
                PdfPCell celValRFuenteIca = new PdfPCell(new Phrase(RFuenteIca.ToString("N2"), fontAcercade));
                celValRFuenteIca.Padding = 4;
                celValRFuenteIca.HorizontalAlignment = Element.ALIGN_RIGHT;
                tableValTotales.AddCell(celValRFuenteIca);

                PdfPCell celValVrlTotal = new PdfPCell(new Phrase(
                    $"{decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"]).ToString("N2")}",
                    fontAcercadeBold));
                celValVrlTotal.Padding = 4;
                celValVrlTotal.HorizontalAlignment = Element.ALIGN_RIGHT;
                tableValTotales.AddCell(celValVrlTotal);

                PdfPCell celValTotales = new PdfPCell(tableValTotales);
                celValTotales.BorderWidthLeft = 0;
                tableTotales.AddCell(celValTotales);

                strError += "se agrego satisfactoriamente QR, resoluciones y totales\n";
                #endregion

                strError += "inicia el consumo de event page para el manejo de los encabezados\n";
                writer.PageEvent = new EventPageLonisa1(tableEncabezado);
                document.Open();
                strError += "cosumo de clase event page OK\n";

                strError += "inicia la adicion de cada una de las tablas creadas antes y divs en el documento\n";

                #region Exit
                document.Add(tableValUnidades);
                document.Add(divEspacio);
                document.Add(tableTotales);
                strError += "se agregaron todas las tablas y divs correctamente OK\n";
                /*PIE DE PAGINA*/
                PdfContentByte pCb = writer.DirectContent; PdfContentByte pCb2 = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();
                strError += "inicia metodos para agregar numeracion de pagina y CONTINUE/CONTINUA. Ya se cerro el documento\n";
                AddPageNumberPagTo($"{Ruta}{NomArchivo}", dimxLeonisa, 755, fontTitle);
                AddPageContinueLeonisa($"{Ruta}{NomArchivo}", dimxLeonisa, 745, fontTitle);
                RutaPdf = NomArchivo;
                strError += "inicia se agrego la numeracion satisfactoriamente OK\n";
                return true;
                #endregion
            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.Message;
                return false;
            }
        }

        public bool NotaDebitoSensuel(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {
            strError += "Lego";
            NomArchivo = string.Empty;
            string Ruta = HttpContext.Current.Server.MapPath("~/PDF/" + NIT + "/");
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            string RutaImg = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";
            NomArchivo = NombreInvoice + ".pdf";
            strError += "inicia Documento pdf\n";

            //strError += "Lego";
            //NomArchivo = string.Empty;
            //string Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
            //string RutaImg = $@"{AppDomain.CurrentDomain.BaseDirectory}\DUNNINGTON_CORPORATION.png";
            //NomArchivo = NombreInvoice + ".pdf";
            //strError += "inicia Documento pdf\n";
            try
            {
                #region Head
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.

                Document document = new Document(iTextSharp.text.PageSize.LETTER, 20f, 15f, 300f, 30f);

                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;

                PdfDiv divEspacio = new PdfDiv();
                divEspacio.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio.Height = 10;
                divEspacio.Width = 130;

                PdfDiv divEspacio2 = new PdfDiv();
                divEspacio2.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio2.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                divEspacio2.Height = 2;
                divEspacio2.Width = 130;

                //FUENTES----------------------------------------------------------------------------------------------------------
                iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle2 = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustom = FontFactory.GetFont(FontFactory.HELVETICA, 8f, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustomI = FontFactory.GetFont(FontFactory.HELVETICA_OBLIQUE, 8f, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustomBold = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 8, iTextSharp.text.Font.NORMAL);
                //FUENTES---------------------------------------------------------------------------------------------------------
                #endregion

                #region ENCABEZADO

                strError += "inicia la creacion de encabezado\n";
                PdfPTable tableEncabezado = new PdfPTable(3);

                float[] dimTableEncabezado = new float[3];
                dimTableEncabezado[0] = 1.7f;
                dimTableEncabezado[1] = 2.9f;
                dimTableEncabezado[2] = 0.9f;
                tableEncabezado.SetWidths(dimTableEncabezado);
                tableEncabezado.WidthPercentage = 100;

                //TODO: Celda de LOGO LEONISA
                strError += "inicia la creacion de logo en el pdf\n";
                System.Drawing.Image logo = null;
                var requestLogo = WebRequest.Create(RutaImg);

                using (var responseLogo = requestLogo.GetResponse())
                using (var streamLogo = responseLogo.GetResponseStream())
                {
                    logo = Bitmap.FromStream(streamLogo);
                }

                PdfDiv divLogo = new PdfDiv()
                {
                    Float = iTextSharp.text.pdf.PdfDiv.FloatType.LEFT,
                    Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE,
                    BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID,
                    Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK,
                    //BackgroundColor = BaseColor.LIGHT_GRAY,
                    Height = 20,
                    Width = 170,
                };

                iTextSharp.text.Image ImgLogo = iTextSharp.text.Image.GetInstance(logo, BaseColor.WHITE);
                divLogo.BackgroundImage = ImgLogo;

                PdfPCell celLogoLeonisa = new PdfPCell()
                {
                    Border = PdfPCell.NO_BORDER,
                    //Colspan =2,
                    VerticalAlignment = Element.ALIGN_TOP,
                };
                celLogoLeonisa.AddElement(ImgLogo);
                tableEncabezado.AddCell(celLogoLeonisa);
                //---------------------------------------------------------------------------------------------------------
                iTextSharp.text.Font fontAcercadeBold = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 8f, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontAcercade = FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL);

                Paragraph prgTituloAcercade1 = new Paragraph($"{(string)DsInvoiceAR.Tables["Company"].Rows[0]["Name"]}\nNit." +
                    $"{(string)DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"]}-" +
                    $"{CalcularDigitoVerificacion((string)DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"])}\n", fontAcercadeBold);
                prgTituloAcercade1.Alignment = Element.ALIGN_CENTER;

                Paragraph prgTituloAcercade2 = new Paragraph(((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar08"]).Replace(",", "\n"),
                    FontFactory.GetFont(FontFactory.HELVETICA, 7.4f, iTextSharp.text.Font.NORMAL));
                prgTituloAcercade2.Alignment = Element.ALIGN_CENTER;
                PdfPCell celInfoEnc1 = new PdfPCell();
                celInfoEnc1.Padding = 0;
                celInfoEnc1.HorizontalAlignment = Element.ALIGN_CENTER;
                celInfoEnc1.VerticalAlignment = Element.ALIGN_CENTER;
                celInfoEnc1.Border = PdfPCell.NO_BORDER;
                celInfoEnc1.AddElement(prgTituloAcercade1);
                celInfoEnc1.AddElement(prgTituloAcercade2);
                tableEncabezado.AddCell(celInfoEnc1);
                //---------------------------------------------------------------------------------------------------------------------

                PdfPCell celContactoEncabezado = new PdfPCell(new Phrase($"{DsInvoiceAR.Tables["Company"].Rows[0]["Address1"].ToString()}\n" +
                    $"{DsInvoiceAR.Tables["Company"].Rows[0]["City"].ToString()} - " +
                    $"{DsInvoiceAR.Tables["Company"].Rows[0]["State"].ToString()}\n" +
                    $"TELÉFONO: {DsInvoiceAR.Tables["Company"].Rows[0]["PhoneNum"].ToString()}",
                    FontFactory.GetFont(FontFactory.HELVETICA, 7.4f, iTextSharp.text.Font.NORMAL)));
                celContactoEncabezado.HorizontalAlignment = Element.ALIGN_CENTER;
                celContactoEncabezado.PaddingTop = 15;
                celContactoEncabezado.VerticalAlignment = Element.ALIGN_BOTTOM;
                celContactoEncabezado.Border = 0;
                tableEncabezado.AddCell(celContactoEncabezado);

                tableEncabezado.AddCell(new PdfPCell(new Phrase(" "))
                { Border = PdfPCell.NO_BORDER, Colspan = 3, HorizontalAlignment = Element.ALIGN_CENTER });

                strError += "inicia la asignacion del CUFE en el pdf\n";
                tableEncabezado.AddCell(new PdfPCell(new Phrase($"CUFE: {CUFE}", fontTitle))
                { Border = PdfPCell.NO_BORDER, Colspan = 3, HorizontalAlignment = Element.ALIGN_CENTER });

                tableEncabezado.AddCell(new PdfPCell(new Phrase("Representación gráfica factura electrónica", fontTitle))
                { Border = PdfPCell.NO_BORDER, Colspan = 3, HorizontalAlignment = Element.ALIGN_CENTER });

                strError += "se creo el encabezado\n";
                #endregion

                #region Datos del Cliente

                PdfPTable tableInfoCliente = new PdfPTable(5);
                float[] dimTableInfoCliente = new float[5];
                dimTableInfoCliente[0] = 0.2f;
                dimTableInfoCliente[1] = 0.5f;
                dimTableInfoCliente[2] = 2.2f;
                dimTableInfoCliente[3] = 0.02f;
                dimTableInfoCliente[4] = 2.0f;
                tableInfoCliente.SetWidths(dimTableInfoCliente);
                tableInfoCliente.WidthPercentage = 100;

                strError += "inicia la traida de la  informacion del cliente\n";

                PdfPCell celEspacioBeforeSenores = new PdfPCell();
                celEspacioBeforeSenores.Border = 0;
                celEspacioBeforeSenores.BorderWidthBottom = 0.3f;
                tableInfoCliente.AddCell(celEspacioBeforeSenores);

                PdfPCell celTextSenores = new PdfPCell(new Phrase("SEÑORES", fontAcercade));
                celTextSenores.Border = 0;
                celTextSenores.HorizontalAlignment = Element.ALIGN_CENTER;
                celTextSenores.VerticalAlignment = Element.ALIGN_BOTTOM;
                tableInfoCliente.AddCell(celTextSenores);

                PdfPCell celEspacioAfterSenores = new PdfPCell();
                celEspacioAfterSenores.Border = 0;
                celEspacioAfterSenores.BorderWidthBottom = 0.3f;
                tableInfoCliente.AddCell(celEspacioAfterSenores);

                PdfPCell celEspacioInfoCliente = new PdfPCell();
                celEspacioInfoCliente.Border = 0;
                tableInfoCliente.AddCell(celEspacioInfoCliente);

                PdfPCell celLineToDatosFactura = new PdfPCell(new Phrase(""));
                celLineToDatosFactura.Border = 0;
                tableInfoCliente.AddCell(celLineToDatosFactura);

                Paragraph prgDatosCliente = new Paragraph($"\n{ (string)DsInvoiceAR.Tables["Customer"].Rows[0]["Name"] }\n\n" +
                    $"NIT: {(string)DsInvoiceAR.Tables["Customer"].Rows[0]["ResaleID"]}\n\n" +
                    $"DIRECCION: {(string)DsInvoiceAR.Tables["Customer"].Rows[0]["Address1"]}   " +
                    $"TELEFONO: {(string)DsInvoiceAR.Tables["Customer"].Rows[0]["PhoneNum"]}\n\n" +
                    $"{(string)DsInvoiceAR.Tables["Customer"].Rows[0]["City"]} {(string)DsInvoiceAR.Tables["Customer"].Rows[0]["State"]}\n",
                    FontFactory.GetFont(FontFactory.HELVETICA, 7.5F, iTextSharp.text.Font.NORMAL));
                PdfPCell celDatosSenores = new PdfPCell(prgDatosCliente);
                celDatosSenores.Colspan = 3;
                celDatosSenores.Border = 0;
                celDatosSenores.BorderWidthRight = 0.3f;
                celDatosSenores.BorderWidthLeft = 0.3f;
                celDatosSenores.BorderWidthBottom = 0.3f;
                tableInfoCliente.AddCell(celDatosSenores);

                tableInfoCliente.AddCell(celEspacioInfoCliente);
                //---------------------------------------------------------------------------------------------
                PdfPTable tableDatosFactura = new PdfPTable(6);
                float[] dimTableDatosFactura = new float[6];
                dimTableDatosFactura[0] = 0.5f;
                dimTableDatosFactura[1] = 0.5f;
                dimTableDatosFactura[2] = 0.5f;
                dimTableDatosFactura[3] = 0.5f;
                dimTableDatosFactura[4] = 0.5f;
                dimTableDatosFactura[5] = 0.5f;
                tableDatosFactura.SetWidths(dimTableDatosFactura);
                tableDatosFactura.WidthPercentage = 100;

                string TipoFactura = "FACTURA DE VENTA No. ";
                if (InvoiceType == "InvoiceType")
                    TipoFactura = "FACTURA DE VENTA No. ";
                else if (InvoiceType == "CreditNoteType")
                    TipoFactura = "NOTA CREDITO No. ";
                else if (InvoiceType == "DebitNoteType")
                    TipoFactura = "NOTA DEBITO No. ";

                PdfPCell celTextNoFactura = new PdfPCell(new Phrase($"{TipoFactura}" +
                    $"{(string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"]}", fontAcercadeBold));
                celTextNoFactura.Colspan = 6;
                celTextNoFactura.Padding = 4;
                tableDatosFactura.AddCell(celTextNoFactura);

                PdfPCell celTextOrderCompra = new PdfPCell(new Phrase($"NOTA DEBITO " +
                    $"{(string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["OrderNum"]}", fontAcercadeBold));
                celTextOrderCompra.Colspan = 6;
                celTextOrderCompra.Border = 0;
                celTextOrderCompra.Padding = 4;
                tableDatosFactura.AddCell(celTextOrderCompra);

                PdfPCell celTextFechaFactura = new PdfPCell(new Phrase("FECHA FACTURA", fontAcercade));
                celTextFechaFactura.Colspan = 3;
                celTextFechaFactura.HorizontalAlignment = Element.ALIGN_CENTER;
                celTextFechaFactura.VerticalAlignment = Element.ALIGN_TOP;
                celTextFechaFactura.Padding = 4;
                tableDatosFactura.AddCell(celTextFechaFactura);

                PdfPCell celTextFechaVencimiento = new PdfPCell(new Phrase("FECHA VENCIMIENTO", fontAcercade));
                celTextFechaVencimiento.Colspan = 3;
                celTextFechaVencimiento.HorizontalAlignment = Element.ALIGN_CENTER;
                celTextFechaVencimiento.VerticalAlignment = Element.ALIGN_TOP;
                celTextFechaVencimiento.Padding = 4;
                tableDatosFactura.AddCell(celTextFechaVencimiento);
                //----------------------------------------------------------------------------------------------
                PdfPCell celDiaFechaFactura = new PdfPCell(new Phrase(
                    $"{DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).Day}\n\nDÍA",
                    fontAcercade));
                celDiaFechaFactura.HorizontalAlignment = Element.ALIGN_CENTER;
                celDiaFechaFactura.VerticalAlignment = Element.ALIGN_TOP;
                tableDatosFactura.AddCell(celDiaFechaFactura);

                PdfPCell celMesFechaFactura = new PdfPCell(new Phrase(
                    $"{DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).Month}\n\nMES",
                    fontAcercade));
                celMesFechaFactura.HorizontalAlignment = Element.ALIGN_CENTER;
                celMesFechaFactura.VerticalAlignment = Element.ALIGN_TOP;
                tableDatosFactura.AddCell(celMesFechaFactura);

                PdfPCell celAnioFechaFactura = new PdfPCell(new Phrase(
                    $"{DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).Year}\n\nAÑO", fontAcercade));
                celAnioFechaFactura.HorizontalAlignment = Element.ALIGN_CENTER;
                celAnioFechaFactura.VerticalAlignment = Element.ALIGN_TOP;
                tableDatosFactura.AddCell(celAnioFechaFactura);

                PdfPCell celDiaFechaVencimiento = new PdfPCell(new Phrase(
                    $"{DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"]).Day}\n\nDÍA",
                    fontAcercade));
                celDiaFechaVencimiento.HorizontalAlignment = Element.ALIGN_CENTER;
                celDiaFechaVencimiento.VerticalAlignment = Element.ALIGN_TOP;
                tableDatosFactura.AddCell(celDiaFechaVencimiento);

                PdfPCell celMesFechaVencimiento = new PdfPCell(new Phrase(
                    $"{DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"]).Month}\n\nMES",
                    fontAcercade));
                celMesFechaVencimiento.HorizontalAlignment = Element.ALIGN_CENTER;
                celMesFechaVencimiento.VerticalAlignment = Element.ALIGN_TOP;
                tableDatosFactura.AddCell(celMesFechaVencimiento);

                PdfPCell celAnioFechaVencimiento = new PdfPCell(new Phrase(
                    $"{DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"]).Year}\n\nAÑO",
                    fontAcercade));
                celAnioFechaVencimiento.HorizontalAlignment = Element.ALIGN_CENTER;
                celAnioFechaVencimiento.VerticalAlignment = Element.ALIGN_TOP;
                tableDatosFactura.AddCell(celAnioFechaVencimiento);
                //-----------------------------------------------------------------------------------------------
                PdfPCell celToDatosFactura = new PdfPCell(tableDatosFactura)
                {

                };
                tableInfoCliente.AddCell(celToDatosFactura);

                tableEncabezado.AddCell(new PdfPCell(tableInfoCliente)
                {
                    Colspan = 3,
                    Border = PdfPCell.NO_BORDER,
                });

                strError += "inicia se importa la informacion del cliente en el pdf\n";
                #endregion

                #region UNIDADES

                strError += "inicia la creacion de la informacion de unidades\n";

                PdfPTable tableEcabezadoUnidades = new PdfPTable(6);
                float[] dimTableEcabezadoUnidades = new float[6];
                dimTableEcabezadoUnidades[0] = 1.0f;
                dimTableEcabezadoUnidades[1] = 2.5f;
                dimTableEcabezadoUnidades[2] = 1.0f;
                dimTableEcabezadoUnidades[3] = 0.89f;
                dimTableEcabezadoUnidades[4] = 1.0f;
                dimTableEcabezadoUnidades[5] = 1.2f;
                tableEcabezadoUnidades.SetWidths(dimTableEcabezadoUnidades);
                tableEcabezadoUnidades.WidthPercentage = 100;

                PdfPCell celTextReferencia = new PdfPCell(new Phrase("REFERENCIA", fontAcercadeBold));
                //celTextSenores.Border = 0;
                celTextReferencia.Padding = 4;
                celTextReferencia.PaddingTop = 8;
                celTextReferencia.BackgroundColor = BaseColor.LIGHT_GRAY;
                celTextReferencia.HorizontalAlignment = Element.ALIGN_CENTER;
                celTextReferencia.VerticalAlignment = Element.ALIGN_CENTER;
                tableEcabezadoUnidades.AddCell(celTextReferencia);

                PdfPCell celTextDetalle = new PdfPCell(new Phrase("DETALLE", fontAcercadeBold));
                //celTextSenores.Border = 0;
                celTextDetalle.Padding = 4;
                celTextDetalle.PaddingTop = 8;
                celTextDetalle.BackgroundColor = BaseColor.LIGHT_GRAY;
                celTextDetalle.HorizontalAlignment = Element.ALIGN_CENTER;
                celTextDetalle.VerticalAlignment = Element.ALIGN_CENTER;
                tableEcabezadoUnidades.AddCell(celTextDetalle);

                PdfPCell celTextCantidad = new PdfPCell(new Phrase("CANTIDAD", fontAcercadeBold));
                //celTextSenores.Border = 0;
                celTextCantidad.Padding = 4;
                celTextCantidad.PaddingTop = 8;
                celTextCantidad.BackgroundColor = BaseColor.LIGHT_GRAY;
                celTextCantidad.HorizontalAlignment = Element.ALIGN_CENTER;
                celTextCantidad.VerticalAlignment = Element.ALIGN_CENTER;
                tableEcabezadoUnidades.AddCell(celTextCantidad);

                PdfPCell celTextUnidad = new PdfPCell(new Phrase("UNIDAD", fontAcercadeBold));
                //celTextSenores.Border = 0;
                celTextUnidad.Padding = 4;
                celTextUnidad.PaddingTop = 8;
                celTextUnidad.BackgroundColor = BaseColor.LIGHT_GRAY;
                celTextUnidad.HorizontalAlignment = Element.ALIGN_CENTER;
                celTextUnidad.VerticalAlignment = Element.ALIGN_CENTER;
                tableEcabezadoUnidades.AddCell(celTextUnidad);

                PdfPCell celTextVrlUnitario = new PdfPCell(new Phrase("VALOR UNITARIO", fontAcercadeBold));
                //celTextSenores.Border = 0;
                celTextVrlUnitario.Padding = 4;
                //celTextVrlUnitario.PaddingTop = 8;
                celTextVrlUnitario.BackgroundColor = BaseColor.LIGHT_GRAY;
                celTextVrlUnitario.HorizontalAlignment = Element.ALIGN_CENTER;
                celTextVrlUnitario.VerticalAlignment = Element.ALIGN_CENTER;
                tableEcabezadoUnidades.AddCell(celTextVrlUnitario);

                PdfPCell celTextVrlTotal = new PdfPCell(new Phrase("VALOR TOTAL", fontAcercadeBold));
                //celTextSenores.Border = 0;
                celTextVrlTotal.Padding = 4;
                celTextVrlTotal.PaddingTop = 8;
                celTextVrlTotal.BackgroundColor = BaseColor.LIGHT_GRAY;
                celTextVrlTotal.HorizontalAlignment = Element.ALIGN_CENTER;
                celTextVrlTotal.VerticalAlignment = Element.ALIGN_CENTER;
                tableEcabezadoUnidades.AddCell(celTextVrlTotal);

                tableEncabezado.AddCell(new PdfPCell(tableEcabezadoUnidades)
                { Colspan = 3, Border = PdfPCell.NO_BORDER, PaddingTop = 10, });
                //--------------------------------------------------------------------------------------
                PdfPTable tableValUnidades = new PdfPTable(dimTableEcabezadoUnidades);
                tableValUnidades.WidthPercentage = 100;

                decimal totaldescuento = 0;
                strError += "inicia el consumo del metodoque agrega la informacion de cada linea\n";
                foreach (DataRow InvoiceLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
                {
                    totaldescuento += decimal.Parse((string)InvoiceLine["DspDocLessDiscount"]);
                    if (!AddUnidadesSensuel(InvoiceLine, ref tableValUnidades, fontAcercade, DsInvoiceAR))
                    {
                        strError += "error en el metodo que agrega la informacion de cada linea de la factura\n";
                        return false;
                    }
                }

                tableValUnidades.AddCell(new PdfPCell(new Phrase(" ")) { Border = PdfPCell.NO_BORDER, });

                tableValUnidades.AddCell(new PdfPCell(new Phrase("TOTAL UNIDADES", fontCustomBold))
                {
                    BackgroundColor = BaseColor.LIGHT_GRAY,
                });

                tableValUnidades.AddCell(new PdfPCell(new Phrase(
                    decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number01"]).ToString("N2"), fontAcercadeBold))
                {
                    HorizontalAlignment = Element.ALIGN_RIGHT,
                });

                tableValUnidades.AddCell(new PdfPCell(new Phrase(" ")) { Colspan = 3, Border = PdfPCell.NO_BORDER, });
                strError += "se agrego la informacion de la factura OK\n";
                #endregion

                #region Totales, QR y Resolucion

                strError += "inicia la creacion del QR resolucion y totales\n";

                PdfPTable tableTotales = new PdfPTable(4);
                float[] dimTableTotales = new float[4];
                dimTableTotales[0] = 1.2f;
                dimTableTotales[1] = 2.5f;
                dimTableTotales[2] = 1.2f;
                dimTableTotales[3] = 1.2f;
                tableTotales.SetWidths(dimTableTotales);
                tableTotales.WidthPercentage = 100;

                iTextSharp.text.Image QRPdf = iTextSharp.text.Image.GetInstance(QRInvoice, BaseColor.WHITE);

                QRPdf.ScaleAbsolute(80f, 80f);
                PdfDiv divQR = new PdfDiv();
                divQR.Float = iTextSharp.text.pdf.PdfDiv.FloatType.LEFT;
                divQR.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divQR.BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID;
                divQR.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                divQR.Height = 80;
                divQR.Width = 80;
                divQR.AddElement(QRPdf);

                iTextSharp.text.pdf.PdfPCell celImgQR = new iTextSharp.text.pdf.PdfPCell();
                celImgQR.Colspan = 1;
                celImgQR.BorderWidthRight = 0;
                celImgQR.Padding = 3;
                celImgQR.PaddingLeft = 15;
                celImgQR.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celImgQR.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                celImgQR.AddElement(QRPdf);
                tableTotales.AddCell(celImgQR);

                string valFacturaNumero = string.Empty;
                //
                if (((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["CurrencyCode"]).ToUpper() != "COP")
                    valFacturaNumero = $"({decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"]).ToString("C2")})";


                string textResolucion = (string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar03"];
                textResolucion += $"\n\nSON: " +
                                    $"{Nroenletras(decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"]).ToString("N2"))} " +
                                    $"{valFacturaNumero}";
                PdfPCell celResolucion = new PdfPCell(new Phrase(textResolucion, fontAcercade));
                celResolucion.BorderWidthLeft = 0;
                celResolucion.Padding = 3;
                celResolucion.HorizontalAlignment = Element.ALIGN_CENTER;
                celResolucion.VerticalAlignment = Element.ALIGN_CENTER;
                tableTotales.AddCell(celResolucion);

                PdfPTable tableTextTotales = new PdfPTable(1);

                PdfPCell celTextTotalBruto = new PdfPCell(new Phrase("TOTAL BRUTO", fontAcercade));
                celTextTotalBruto.Padding = 4;
                tableTextTotales.AddCell(celTextTotalBruto);

                tableTextTotales.AddCell(new PdfPCell(new Phrase("DESCUENTO", fontAcercade)) { Padding = 4, });

                tableTextTotales.AddCell(new PdfPCell(new Phrase("SUBTOTAL", fontAcercadeBold)) { Padding = 4, });

                PdfPCell celTextIva = new PdfPCell(new Phrase("IVA", fontAcercade));
                celTextIva.Padding = 4;
                tableTextTotales.AddCell(celTextIva);

                PdfPCell celTextRFuenteIva = new PdfPCell(new Phrase("RETEFUENTE IVA", fontAcercade));
                celTextRFuenteIva.Padding = 4;
                tableTextTotales.AddCell(celTextRFuenteIva);

                PdfPCell celTextRFuenteRenta = new PdfPCell(new Phrase("RETEFUENTE RENTA", fontAcercade));
                celTextRFuenteRenta.Padding = 4;
                tableTextTotales.AddCell(celTextRFuenteRenta);

                PdfPCell celTextRFuenteIca = new PdfPCell(new Phrase("RETEFUENTE ICA", fontAcercade));
                celTextRFuenteIca.Padding = 4;
                tableTextTotales.AddCell(celTextRFuenteIca);

                PdfPCell celTextValorTotal = new PdfPCell(new Phrase("VALOR TOTAL", fontAcercadeBold));
                celTextValorTotal.Padding = 4;
                tableTextTotales.AddCell(celTextValorTotal);

                PdfPCell celTextTotales = new PdfPCell(tableTextTotales);
                celTextTotales.BorderWidthLeft = 0;
                celTextTotales.BorderWidthTop = 0;
                tableTotales.AddCell(celTextTotales);
                //--------------------------------------------------------------------------------------------
                PdfPTable tableValTotales = new PdfPTable(1);

                var totalBruto = decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubTotal"]) +
                    Math.Abs(totaldescuento);

                //PdfPCell celValTotalBruto = new PdfPCell(new Phrase(
                //    $"{decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubTotal"]).ToString("N2")}",
                //    fontAcercade));

                PdfPCell celValTotalBruto = new PdfPCell(new Phrase(
                    $"{totalBruto.ToString("N2")}",
                    fontAcercade));
                celValTotalBruto.BorderWidthTop = 0.5f;
                celValTotalBruto.Padding = 4;
                celValTotalBruto.HorizontalAlignment = Element.ALIGN_RIGHT;
                tableValTotales.AddCell(celValTotalBruto);

                tableValTotales.AddCell(new PdfPCell(new Phrase(totaldescuento.ToString("N2"), fontAcercade))
                {
                    Padding = 4,
                    HorizontalAlignment = Element.ALIGN_RIGHT,
                });

                //var subtotal = decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubTotal"]) -
                //   Math.Abs(totaldescuento);

                var subtotal = decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubTotal"]);

                tableValTotales.AddCell(new PdfPCell(new Phrase(
                    subtotal.ToString("N2"),
                    fontAcercadeBold))
                {
                    Padding = 4,
                    HorizontalAlignment = Element.ALIGN_RIGHT,
                });

                decimal Iva = GetValImpuestoByID("01", DsInvoiceAR);
                PdfPCell celValIva = new PdfPCell(new Phrase($"{Iva.ToString("N2")}", fontAcercade));
                celValIva.Padding = 4;
                celValIva.HorizontalAlignment = Element.ALIGN_RIGHT;
                tableValTotales.AddCell(celValIva);

                decimal RFuenteIva = GetValImpuestoByID("0B", DsInvoiceAR);
                PdfPCell celValRFuenteIva = new PdfPCell(new Phrase(RFuenteIva.ToString("N2"), fontAcercade));
                celValRFuenteIva.Padding = 4;
                celValRFuenteIva.HorizontalAlignment = Element.ALIGN_RIGHT;
                tableValTotales.AddCell(celValRFuenteIva);

                decimal RFuenteRenta = GetValImpuestoByID("0D", DsInvoiceAR);
                PdfPCell celValRFuenteRenta = new PdfPCell(new Phrase(RFuenteRenta.ToString("N2"), fontAcercade));
                celValRFuenteRenta.Padding = 4;
                celValRFuenteRenta.HorizontalAlignment = Element.ALIGN_RIGHT;
                tableValTotales.AddCell(celValRFuenteRenta);

                decimal RFuenteIca = GetValImpuestoByID("0C", DsInvoiceAR);
                PdfPCell celValRFuenteIca = new PdfPCell(new Phrase(RFuenteIca.ToString("N2"), fontAcercade));
                celValRFuenteIca.Padding = 4;
                celValRFuenteIca.HorizontalAlignment = Element.ALIGN_RIGHT;
                tableValTotales.AddCell(celValRFuenteIca);

                PdfPCell celValVrlTotal = new PdfPCell(new Phrase(
                    $"{decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"]).ToString("N2")}",
                    fontAcercadeBold));
                celValVrlTotal.Padding = 4;
                celValVrlTotal.HorizontalAlignment = Element.ALIGN_RIGHT;
                tableValTotales.AddCell(celValVrlTotal);

                PdfPCell celValTotales = new PdfPCell(tableValTotales);
                celValTotales.BorderWidthLeft = 0;
                tableTotales.AddCell(celValTotales);

                strError += "se agrego satisfactoriamente QR, resoluciones y totales\n";
                #endregion

                strError += "inicia el consumo de event page para el manejo de los encabezados\n";
                writer.PageEvent = new EventPageLonisa1(tableEncabezado);
                document.Open();
                strError += "cosumo de clase event page OK\n";

                strError += "inicia la adicion de cada una de las tablas creadas antes y divs en el documento\n";

                #region Exit
                document.Add(tableValUnidades);
                document.Add(divEspacio);
                document.Add(tableTotales);
                strError += "se agregaron todas las tablas y divs correctamente OK\n";
                /*PIE DE PAGINA*/
                PdfContentByte pCb = writer.DirectContent; PdfContentByte pCb2 = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();
                strError += "inicia metodos para agregar numeracion de pagina y CONTINUE/CONTINUA. Ya se cerro el documento\n";
                AddPageNumberPagTo($"{Ruta}{NomArchivo}", dimxLeonisa, 755, fontTitle);
                AddPageContinueLeonisa($"{Ruta}{NomArchivo}", dimxLeonisa, 745, fontTitle);
                RutaPdf = NomArchivo;
                strError += "inicia se agrego la numeracion satisfactoriamente OK\n";
                return true;
                #endregion
            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.Message;
                return false;
            }
        }

        #endregion
    }
}
