﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
//Referenciar y usar.
using System.Data;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Web;
using System.Windows.Forms;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml;
using RulesServicesDIAN2.Adquiriente;

namespace RulesServicesDIAN2.Adquiriente
{
    public partial class pdfEstandarAR
    {
        #region FACTURA DE VENTAS XENCO
        private bool AddUnidadesXenco(DataRow dataLine, ref PdfPTable table, iTextSharp.text.Font fontTitleFactura, DataSet dataSet)
        {
            try
            {
                //codigo
                strError += "Add InvoiceLine";
                iTextSharp.text.pdf.PdfPCell celValL = new iTextSharp.text.pdf.PdfPCell(new Phrase((string)dataLine["PartNum"], fontTitleFactura));
                celValL.Colspan = 1;
                celValL.Padding = 2;
                celValL.Border = PdfPCell.RIGHT_BORDER | PdfPCell.LEFT_BORDER;
                //celTextL.BorderWidthRight = 1;
                //celValL.BorderWidthLeft = 1;
                celValL.HorizontalAlignment = Element.ALIGN_LEFT;
                celValL.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValL);
                strError += "InvoiceLine OK";

                //descriccion
                strError += "Add PartNum";
                iTextSharp.text.pdf.PdfPCell celValCodigo = new iTextSharp.text.pdf.PdfPCell(new Phrase((string)dataLine["LineDesc"], fontTitleFactura));
                celValCodigo.Colspan = 1;
                celValCodigo.Padding = 2;
                celValCodigo.Border = PdfPCell.RIGHT_BORDER;
                //celValCodigo.BorderWidthLeft = 1;
                //celDescripcion.BorderColorBottom = BaseColor.WHITE;
                celValCodigo.HorizontalAlignment = Element.ALIGN_LEFT;
                celValCodigo.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValCodigo);
                strError += "Add PartNum OK";

                //cantidad
                strError += "Add SellingShipQty";
                iTextSharp.text.pdf.PdfPCell celValDescripcion = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    decimal.Parse((string)dataLine["SellingShipQty"]).ToString("N0"), fontTitleFactura));
                celValDescripcion.Colspan = 1;
                celValDescripcion.Padding = 2;
                celValDescripcion.Border = PdfPCell.RIGHT_BORDER;
                //celValCantidad.BorderWidthLeft = 1;
                celValDescripcion.Padding = 5;
                celValDescripcion.HorizontalAlignment = Element.ALIGN_RIGHT;
                celValDescripcion.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValDescripcion);
                strError += "Add SellingShipQty OK";

                //valor unitario
                strError += "Add LineDesc";
                iTextSharp.text.pdf.PdfPCell celValCantidad = new iTextSharp.text.pdf.PdfPCell(new Phrase("$ " +
                    decimal.Parse((string)dataLine["UnitPrice"]).ToString("N0"), fontTitleFactura));
                celValCantidad.Colspan = 1;
                celValCantidad.Padding = 2;
                celValCantidad.Border = PdfPCell.RIGHT_BORDER;
                //celValDesc.BorderWidthLeft = 1;
                celValCantidad.Padding = 5;
                celValCantidad.HorizontalAlignment = Element.ALIGN_RIGHT;
                celValCantidad.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValCantidad);
                strError += "Add LineDesc OK";

                //subtotaal
                strError += "Add LineDesc";
                iTextSharp.text.pdf.PdfPCell celValCantidad1 = new iTextSharp.text.pdf.PdfPCell(new Phrase("$ " +
                    decimal.Parse((string)dataLine["DocExtPrice"]).ToString("N0"), fontTitleFactura));
                celValCantidad1.Colspan = 1;
                celValCantidad1.Padding = 2;
                celValCantidad1.Border = 0;
                celValCantidad1.Padding = 5;
                celValCantidad1.Border = PdfPCell.RIGHT_BORDER;
                //celValDesc.BorderWidthLeft = 1;
                celValCantidad1.HorizontalAlignment = Element.ALIGN_RIGHT;
                celValCantidad1.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValCantidad1);
                strError += "Add LineDesc OK";

                return true;
            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.Message;
                return false;
            }
        }

        public bool FacturadeVentaxenco(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {
            //NomArchivo = string.Empty;
            //string Ruta = HttpContext.Current.Server.MapPath("~/PDF/" + NIT + "/");
            //string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            //string RutaImg = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";
            //NomArchivo = NombreInvoice + ".pdf";

            NomArchivo = string.Empty;
            string Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            string RutaImg = $@"{AppDomain.CurrentDomain.BaseDirectory}\LOGO XENCO FRA.png";
            NomArchivo = NombreInvoice + ".pdf";
            try
            {
                #region Head
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.
                Document document = new Document(iTextSharp.text.PageSize.LETTER);
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;

                // step 3: we open the document     
                document.Open();

                PdfDiv divEspacio = new PdfDiv();
                divEspacio.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio.Height = 10;
                divEspacio.Width = 130;

                PdfDiv divEspacio2 = new PdfDiv();
                divEspacio2.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio2.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio2.Height = 2;
                divEspacio2.Width = 130;

                //FUENTES----------------------------------------------------------------------------------------------------------
                iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle3 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 9, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle2 = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustom = FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitleFactura = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                //FUENTES---------------------------------------------------------------------------------------------------------

                RoundRectangle CelEventBorderRound = new RoundRectangle();


                System.Drawing.Image logo = null;
                var requestLogo = WebRequest.Create(RutaImg);

                using (var responseLogo = requestLogo.GetResponse())
                using (var streamLogo = responseLogo.GetResponseStream())
                {
                    logo = Bitmap.FromStream(streamLogo);
                }

                System.Drawing.Image logo2 = null;
                var requestLogo2 = WebRequest.Create(RutaImg);

                using (var responseLogo2 = requestLogo2.GetResponse())
                using (var streamLogo2 = responseLogo2.GetResponseStream())
                {
                    logo2 = Bitmap.FromStream(streamLogo2);
                }

                iTextSharp.text.Image ImgLogo2 = iTextSharp.text.Image.GetInstance(logo2, BaseColor.WHITE);
                ImgLogo2.ScaleAbsolute(130, 130);

                iTextSharp.text.Image ImgLogo3 = iTextSharp.text.Image.GetInstance(logo2, BaseColor.WHITE);
                ImgLogo3.ScaleAbsolute(150, 150);
                #endregion

                PdfPTable salto = new PdfPTable(1);
                salto.WidthPercentage = 100;
                PdfPCell cell_todo = new PdfPCell() { Border = 0, /*CellEvent = CelEventBorderRound*/ MinimumHeight = 5, };
                salto.AddCell(cell_todo);

                #region ENCABEZADO

                PdfPTable Header = new PdfPTable(new float[] { 0.3f, 0.3f, 0.15f, 0.25f, });
                Header.WidthPercentage = 100;

                PdfPCell cell_logo = new PdfPCell() { Border = 0, };
                cell_logo.AddElement(ImgLogo2);
                Header.AddCell(cell_logo);

                PdfPCell cell_info = new PdfPCell() { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, };
                Paragraph prgTitle1 = new Paragraph(string.Format("XENCO S.A."), fontTitle) { Alignment = Element.ALIGN_CENTER, };
                Paragraph prgTitle2 = new Paragraph(string.Format("NIT. 811.009.452-9"), fontTitle) { Alignment = Element.ALIGN_CENTER, };
                Paragraph prgTitle3 = new Paragraph(string.Format("IVA REGIMEN COMUN"), fontTitle) { Alignment = Element.ALIGN_CENTER, };
                Paragraph prgTitle4 = new Paragraph(string.Format("SOMOS AUTORRETENEDORES RESOLUCIÓN 11054 DE DIC-18-2014"), fontTitle) { Alignment = Element.ALIGN_CENTER, };
                Paragraph prgTitle5 = new Paragraph(string.Format("NO SOMO GRANDES CONTRIBUYENTES"), fontTitle) { Alignment = Element.ALIGN_CENTER, };
                Paragraph prgTitle6 = new Paragraph(string.Format("NO SOMO AGENTES RETENEDORES DE IVA"), fontTitle) { Alignment = Element.ALIGN_CENTER, };
                cell_info.AddElement(prgTitle1);
                cell_info.AddElement(prgTitle2);
                cell_info.AddElement(prgTitle3);
                cell_info.AddElement(prgTitle4);
                cell_info.AddElement(prgTitle5);
                cell_info.AddElement(prgTitle6);
                Header.AddCell(cell_info);

                PdfPCell cell_Qr = new PdfPCell() { Border = 0, };
                iTextSharp.text.Image ImgQR = iTextSharp.text.Image.GetInstance(QRInvoice, BaseColor.WHITE);
                ImgQR.ScaleAbsolute(60, 60);
                cell_Qr.AddElement(ImgQR);
                Header.AddCell(cell_Qr);

                PdfPCell cell_factura = new PdfPCell() { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, };
                PdfPTable tableFactura = new PdfPTable(3);

                //Dimenciones.
                float[] DimencionFactura = new float[3];
                DimencionFactura[0] = 1.2F;//
                DimencionFactura[1] = 4.0F;//
                DimencionFactura[2] = 1.0F;//

                tableFactura.WidthPercentage = 100;
                tableFactura.SetWidths(DimencionFactura);

                iTextSharp.text.pdf.PdfPCell celTittle = new iTextSharp.text.pdf.PdfPCell(new Phrase("FACTURA DE VENTAS\n\n", fontTitleFactura)) { Border = 0, };
                celTittle.Colspan = 3;
                celTittle.Padding = 3;
                celTittle.HorizontalAlignment = Element.ALIGN_CENTER;
                celTittle.VerticalAlignment = Element.ALIGN_TOP;
                celTittle.Border = 0;
                celTittle.BorderWidthTop = 1;
                celTittle.BorderWidthLeft = 1;
                celTittle.BorderWidthRight = 1;
                //celTittle.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableFactura.AddCell(celTittle);

                iTextSharp.text.pdf.PdfPCell celNo = new iTextSharp.text.pdf.PdfPCell(new Phrase("No. ", fontTitleFactura)) { Border = 0, };
                celNo.Colspan = 1;
                celNo.Padding = 5;
                celNo.HorizontalAlignment = Element.ALIGN_CENTER;
                celNo.VerticalAlignment = Element.ALIGN_TOP;
                celNo.Border = 0;
                celNo.BorderWidthLeft = 1;
                //celNo.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableFactura.AddCell(celNo);

                string NumLegalFactura = string.Empty;
                if (InvoiceType == "InvoiceType")
                    NumLegalFactura = DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"].ToString(); /*InvoiceRef*/
                else if (InvoiceType == "CreditNoteType")
                    NumLegalFactura = DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"].ToString();
                else
                    NumLegalFactura = DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"].ToString();

                iTextSharp.text.pdf.PdfPCell celNoFactura = new iTextSharp.text.pdf.PdfPCell(new Phrase(NumLegalFactura, fontTitleFactura));
                celNoFactura.Colspan = 1;
                celNoFactura.Padding = 5;
                celNoFactura.Border = 0;
                celNoFactura.HorizontalAlignment = Element.ALIGN_LEFT;
                celNoFactura.VerticalAlignment = Element.ALIGN_TOP;
                //celNoFactura.Border = 1;
                celNoFactura.BackgroundColor = BaseColor.WHITE;
                tableFactura.AddCell(celNoFactura);


                iTextSharp.text.pdf.PdfPCell celEspacioNoFactura = new iTextSharp.text.pdf.PdfPCell(new Phrase(" ", fontTitleFactura));
                celEspacioNoFactura.Colspan = 1;
                celEspacioNoFactura.Border = 0;
                //celNo.Padding = 3;
                celEspacioNoFactura.BorderWidthRight = 1;
                celEspacioNoFactura.HorizontalAlignment = Element.ALIGN_CENTER;
                celEspacioNoFactura.VerticalAlignment = Element.ALIGN_TOP;
                //celEspacioNoFactura.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableFactura.AddCell(celEspacioNoFactura);

                iTextSharp.text.pdf.PdfPCell celRellenoNoFactura = new iTextSharp.text.pdf.PdfPCell(new Phrase(" ", fontTitleFactura));
                celRellenoNoFactura.Colspan = 3;
                celRellenoNoFactura.Padding = 3;
                celRellenoNoFactura.Border = 0;
                celRellenoNoFactura.BorderWidthLeft = 1;
                celRellenoNoFactura.BorderWidthRight = 1;
                celRellenoNoFactura.BorderWidthBottom = 1;
                //celRellenoNoFactura.BackgroundColor = BaseColor.LIGHT_GRAY;
                celRellenoNoFactura.HorizontalAlignment = Element.ALIGN_RIGHT;
                celRellenoNoFactura.VerticalAlignment = Element.ALIGN_TOP;
                tableFactura.AddCell(celRellenoNoFactura);

                iTextSharp.text.pdf.PdfPCell celConsecutivoInterno = new iTextSharp.text.pdf.PdfPCell(new Phrase("CUFE" + CUFE, fontCustom));
                celConsecutivoInterno.Colspan = 4;
                celConsecutivoInterno.BackgroundColor = BaseColor.WHITE;
                celConsecutivoInterno.HorizontalAlignment = Element.ALIGN_RIGHT;
                celConsecutivoInterno.VerticalAlignment = Element.ALIGN_TOP;
                celConsecutivoInterno.Border = 0;
                tableFactura.AddCell(celConsecutivoInterno);

                PdfDiv divNumeroFactura = new PdfDiv();
                divNumeroFactura.Float = iTextSharp.text.pdf.PdfDiv.FloatType.LEFT;
                divNumeroFactura.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divNumeroFactura.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                divNumeroFactura.PaddingLeft = 5;
                divNumeroFactura.Width = 140;
                divNumeroFactura.AddElement(tableFactura);
                cell_factura.AddElement(tableFactura);
                Header.AddCell(cell_factura);

                #endregion

                #region Body

                PdfPTable Body = new PdfPTable(2);
                Body.WidthPercentage = 100;

                PdfPCell señore = new PdfPCell();
                PdfPTable señores = new PdfPTable(1);
                señores.WidthPercentage = 100;
                PdfPCell cell_señores = new PdfPCell(new Phrase("Señores", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, };
                señores.AddCell(cell_señores);
                señore.AddElement(señores);
                PdfPTable señoresV = new PdfPTable(1);
                señoresV.WidthPercentage = 100;
                PdfPCell cell_señoresV = new PdfPCell(new Phrase("" + DsInvoiceAR.Tables["Customer"].Rows[0]["Name"].ToString(), fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, };
                señoresV.AddCell(cell_señoresV);
                señore.AddElement(señoresV);
                PdfPTable direccion = new PdfPTable(1);
                direccion.WidthPercentage = 100;
                PdfPCell cell_direccion = new PdfPCell(new Phrase("Dirección: " + DsInvoiceAR.Tables["Customer"].Rows[0]["Address1"].ToString(), fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, };
                direccion.AddCell(cell_direccion);
                señore.AddElement(direccion);
                PdfPTable ciudad = new PdfPTable(1);
                ciudad.WidthPercentage = 100;
                PdfPCell cell_ciudad = new PdfPCell(new Phrase("Ciudad: " + DsInvoiceAR.Tables["Customer"].Rows[0]["City"].ToString(), fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, };
                ciudad.AddCell(cell_ciudad);
                señore.AddElement(ciudad);
                PdfPTable nit = new PdfPTable(1);
                nit.WidthPercentage = 100;
                PdfPCell cell_nit = new PdfPCell(new Phrase("Nit: " + DsInvoiceAR.Tables["Customer"].Rows[0]["ResaleID"].ToString(), fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, };
                nit.AddCell(cell_nit);
                señore.AddElement(nit);
                PdfPTable tel = new PdfPTable(1);
                tel.WidthPercentage = 100;
                PdfPCell cell_tel = new PdfPCell(new Phrase("Tel: " + DsInvoiceAR.Tables["Customer"].Rows[0]["PhoneNum"].ToString(), fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, };
                tel.AddCell(cell_tel);
                señore.AddElement(tel);

                Body.AddCell(señore);

                PdfPCell fechast = new PdfPCell();
                PdfPTable fechas = new PdfPTable(2);
                fechas.WidthPercentage = 100;
                PdfPCell cell_fecha1 = new PdfPCell(new Phrase("fecha de factura: " + DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).ToString("dd/MM/yyyy"), fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, };
                PdfPCell cell_fecha2 = new PdfPCell(new Phrase("fecha de vencimiento: " + DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"]).ToString("dd/MM/yyyy"), fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, };
                fechas.AddCell(cell_fecha1);
                fechas.AddCell(cell_fecha2);
                fechast.AddElement(fechas);
                PdfPTable vendedors = new PdfPTable(1);
                vendedors.WidthPercentage = 100;
                PdfPCell cell_vendedors = new PdfPCell(new Phrase("Vendedor: " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["SalesRepName1"].ToString(), fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, };
                vendedors.AddCell(cell_vendedors);
                fechast.AddElement(vendedors);
                PdfPTable vendedor = new PdfPTable(1);
                vendedor.WidthPercentage = 100;
                PdfPCell cell_vendedor = new PdfPCell(new Phrase("Condiciones de pago ", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, }; fechas.AddCell(cell_fecha1);
                vendedor.AddCell(cell_vendedor);
                fechast.AddElement(vendedor);
                PdfPTable foma_numero = new PdfPTable(2);
                foma_numero.WidthPercentage = 100;
                PdfPCell cell_pago = new PdfPCell(new Phrase("forma de pago: " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar01"].ToString(), fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, };
                PdfPCell cell_cuentas = new PdfPCell(new Phrase("Nro. cuotas: " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number01"].ToString(), fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, };
                foma_numero.AddCell(cell_pago);
                foma_numero.AddCell(cell_cuentas);
                fechast.AddElement(foma_numero);
                PdfPTable cuotas_info = new PdfPTable(2);
                cuotas_info.WidthPercentage = 100;
                PdfPCell cell_vcuotas = new PdfPCell(new Phrase("Vencimiento  cuotas: " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["ChangeDate"].ToString(), fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, };
                PdfPCell cell_vrcuotas = new PdfPCell(new Phrase("Vr cuotas: " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number02"].ToString(), fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, };
                cuotas_info.AddCell(cell_vcuotas);
                cuotas_info.AddCell(cell_vrcuotas);
                fechast.AddElement(cuotas_info);
                PdfPTable estado_pago = new PdfPTable(2);
                estado_pago.WidthPercentage = 100;
                PdfPCell cell_e_p = new PdfPCell(new Phrase("Estado de pago: " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar02"].ToString(), fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, };
                estado_pago.AddCell(cell_e_p);
                fechast.AddElement(estado_pago);
                Body.AddCell(fechast);

                #endregion

                #region Unidades

                PdfPTable titulo_unidades = new PdfPTable(new float[] { 0.15f, 0.4f, 0.15f, 0.15f, 0.15f, });
                titulo_unidades.WidthPercentage = 100;

                PdfPCell codigo = new PdfPCell(new Phrase("Codigo", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, MinimumHeight = 15, };
                PdfPCell descripcion = new PdfPCell(new Phrase("Descripcion", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, };
                PdfPCell cantuidad = new PdfPCell(new Phrase("Cantuidad", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, };
                PdfPCell vr_unidad = new PdfPCell(new Phrase("Vr. unidad", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, };
                PdfPCell vr_total = new PdfPCell(new Phrase("Vr. total", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, };

                titulo_unidades.AddCell(codigo);
                titulo_unidades.AddCell(descripcion);
                titulo_unidades.AddCell(cantuidad);
                titulo_unidades.AddCell(vr_unidad);
                titulo_unidades.AddCell(vr_total);

                PdfPTable tableTituloUnidades = new PdfPTable(5);
                //Dimenciones.
                float[] DimencionUnidades = new float[5];
                DimencionUnidades[0] = 0.15f;//CÓDIGO
                DimencionUnidades[1] = 0.4f;//DESCRIPCION
                DimencionUnidades[2] = 0.15f;//CANTIDAD
                DimencionUnidades[3] = 0.15f;//Vr_unidad
                DimencionUnidades[4] = 0.15f;//Vr_total


                PdfPTable tableUnidades = new PdfPTable(5);
                tableUnidades.WidthPercentage = 100;
                tableUnidades.SetWidths(DimencionUnidades);

                foreach (DataRow InvoiceLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
                {
                    if (!AddUnidadesXenco(InvoiceLine, ref tableUnidades, fontCustom, DsInvoiceAR))
                        return false;
                }

                iTextSharp.text.pdf.PdfPCell LineaFinal = new iTextSharp.text.pdf.PdfPCell();
                LineaFinal.Colspan = 5;
                LineaFinal.Padding = 3;
                LineaFinal.Border = 1;
                LineaFinal.BorderColorBottom = BaseColor.WHITE;
                LineaFinal.HorizontalAlignment = Element.ALIGN_CENTER;
                LineaFinal.VerticalAlignment = Element.ALIGN_TOP;
                tableUnidades.AddCell(LineaFinal);

                #endregion

                #region Footer

                PdfPTable Footer = new PdfPTable(new float[] { 0.7f, 0.15f, 0.15f });
                Footer.WidthPercentage = 100;

                PdfPCell obserbaciones = new PdfPCell(new Phrase("Observaciones: "+ DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceComment"].ToString(), fontCustom)) { MinimumHeight = 25, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP, };
                Footer.AddCell(obserbaciones);

                PdfPCell tptalesT = new PdfPCell();
                PdfPTable subtotal = new PdfPTable(1);
                subtotal.WidthPercentage = 100;
                PdfPCell cell_subtotal = new PdfPCell(new Phrase("SUBTOTAL: ", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_RIGHT, };
                subtotal.AddCell(cell_subtotal);
                tptalesT.AddElement(subtotal);
                PdfPTable iva = new PdfPTable(1);
                iva.WidthPercentage = 100;
                PdfPCell cell_iva = new PdfPCell(new Phrase("IVA: ", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_RIGHT, };
                iva.AddCell(cell_iva);
                tptalesT.AddElement(iva);
                PdfPTable total = new PdfPTable(1);
                total.WidthPercentage = 100;
                PdfPCell cell_total = new PdfPCell(new Phrase("TOTAL: ", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_RIGHT, };
                total.AddCell(cell_total);
                tptalesT.AddElement(total);
                Footer.AddCell(tptalesT);

                PdfPCell tptalesTV = new PdfPCell();
                PdfPTable subtotalV = new PdfPTable(1);
                subtotalV.WidthPercentage = 100;
                PdfPCell cell_subtotalV = new PdfPCell(new Phrase("$ " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubTotal"].ToString(), fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, };
                subtotalV.AddCell(cell_subtotalV);
                tptalesTV.AddElement(subtotalV);
                PdfPTable ivaV = new PdfPTable(1);
                ivaV.WidthPercentage = 100;
                PdfPCell cell_ivaV = new PdfPCell(new Phrase(string.Format("{0:C2}", Convert.ToDecimal(GetValImpuestoByRateCode("","0A", DsInvoiceAR).ToString())), fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, };
                ivaV.AddCell(cell_ivaV);
                tptalesTV.AddElement(ivaV);
                PdfPTable totalV = new PdfPTable(1);
                totalV.WidthPercentage = 100;
                PdfPCell cell_totalV = new PdfPCell(new Phrase("$ " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"].ToString(), fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, };
                totalV.AddCell(cell_totalV);
                tptalesTV.AddElement(totalV);
                Footer.AddCell(tptalesTV);

                PdfPTable t_pago = new PdfPTable(1);
                t_pago.WidthPercentage = 100;
                PdfPCell terminos_p = new PdfPCell(new Phrase(string.Format("Son: {0}",
                    Nroenletras(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"].ToString())), fontCustom)) { HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP, MinimumHeight = 30, };
                t_pago.AddCell(terminos_p);

                PdfPTable fin_pie = new PdfPTable(2);
                fin_pie.WidthPercentage = 100;
                PdfPCell fin_pie1 = new PdfPCell() { MinimumHeight = 30, };

                PdfPTable terminos_pago = new PdfPTable(1);
                terminos_pago.WidthPercentage = 100;
                PdfPCell cll_terminos_pago = new PdfPCell(new Phrase("Sirvase pagar en Medellin a ordenes de Xenco S.A enla fecha indicadadentro de" +
                                                                    "esta factura para el vencimiento, la suma de que el presente titulo aalor. El no" +
                                                                    "pago oportuno en la fecha de vencimiento, generara el cobro de intereses de mora a favor" +
                                                                    "Formulario DIAN* 18762008797716 de jun-20-208, autoriza FCM 1 al FCM1000.", fontCustom))
                { MinimumHeight = 53, Padding = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP, };
                terminos_pago.AddCell(cll_terminos_pago);
                fin_pie1.AddElement(terminos_pago);

                PdfPTable nombre_vacio = new PdfPTable(1);
                nombre_vacio.WidthPercentage = 100;
                PdfPCell cll_nombre_vacio = new PdfPCell(new Phrase("", fontCustom)) { Padding = 0, MinimumHeight = 10, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP, };
                nombre_vacio.AddCell(cll_nombre_vacio);
                fin_pie1.AddElement(nombre_vacio);

                PdfPTable expedido = new PdfPTable(1);
                expedido.WidthPercentage = 100;
                PdfPCell cll_expedido = new PdfPCell(new Phrase("EXPEDIDA POR:", fontCustom)) { Padding = 0, MinimumHeight = 10, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, };
                expedido.AddCell(cll_expedido);
                fin_pie1.AddElement(expedido);

                fin_pie.AddCell(fin_pie1);

                PdfPCell fin_pie2 = new PdfPCell();

                PdfPTable aceptacion = new PdfPTable(1);
                aceptacion.WidthPercentage = 100;
                PdfPCell cll_aceptacion = new PdfPCell(new Phrase("Aceptación de la Factura", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, };
                aceptacion.AddCell(cll_aceptacion);
                fin_pie2.AddElement(aceptacion);

                PdfPTable declaro = new PdfPTable(1);
                declaro.WidthPercentage = 100;
                PdfPCell cll_declaro = new PdfPCell(new Phrase("", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, };
                declaro.AddCell(cll_declaro);
                fin_pie2.AddElement(declaro);

                PdfPTable nombre = new PdfPTable(2);
                nombre.WidthPercentage = 100;
                PdfPCell cll_nombre = new PdfPCell(new Phrase("Nombre", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, };
                PdfPCell cll_ide = new PdfPCell(new Phrase("Identificacion", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, };
                nombre.AddCell(cll_nombre);
                nombre.AddCell(cll_ide);
                fin_pie2.AddElement(nombre);

                PdfPTable firma = new PdfPTable(2);
                firma.WidthPercentage = 100;
                PdfPCell cll_firma = new PdfPCell(new Phrase("Firma", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, };
                PdfPCell cll_sello = new PdfPCell(new Phrase("Sello", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, };
                firma.AddCell(cll_firma);
                firma.AddCell(cll_sello);
                fin_pie2.AddElement(firma);

                PdfPTable recibo = new PdfPTable(1);
                recibo.WidthPercentage = 100;
                PdfPCell cll_recibo = new PdfPCell(new Phrase("Fecha Recibido", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, }; firma.AddCell(cll_firma);
                recibo.AddCell(cll_recibo);
                fin_pie2.AddElement(recibo);

                PdfPTable leyenda = new PdfPTable(1);
                leyenda.WidthPercentage = 100;
                PdfPCell cll_leyenda = new PdfPCell(new Phrase("Recibo", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, }; firma.AddCell(cll_firma);
                leyenda.AddCell(cll_leyenda);
                fin_pie2.AddElement(leyenda);
                fin_pie.AddCell(fin_pie2);


                PdfPTable fin = new PdfPTable(new float[] { 0.33f, 0.55f, 0.12f, });
                fin.WidthPercentage = 100;
                PdfPCell info_fin = new PdfPCell(new Phrase("")) { Border = 0, };

                Paragraph inf1 = new Paragraph("Oficina Principal Medellin Cra. 46 No. 7 - 128", fontCustom);
                Paragraph inf2 = new Paragraph("Conmutador: 3111 11 44 Fax 266 91 11", fontCustom);
                Paragraph inf3 = new Paragraph("Sucorsal: pereira Conmutador: 3 356 356", fontCustom);
                Paragraph inf4 = new Paragraph("WWW.xenco.com.co", fontCustom);

                info_fin.AddElement(inf1);
                info_fin.AddElement(inf2);
                info_fin.AddElement(inf3);
                info_fin.AddElement(inf4);

                fin.AddCell(info_fin);

                PdfPCell vacio = new PdfPCell() { Border = 0, };
                fin.AddCell(vacio);

                PdfPCell pie_logo = new PdfPCell() { Border = 0, };
                fin.AddCell(pie_logo);


                PdfPTable orinal = new PdfPTable(1);
                orinal.WidthPercentage = 100;
                PdfPCell cell_original = new PdfPCell(new Phrase("ORIGINAL")) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER };
                orinal.AddCell(cell_original);
                #endregion

                #region Exit

                document.Add(Header);
                document.Add(salto);
                document.Add(Body);
                document.Add(salto);
                document.Add(titulo_unidades);
                document.Add(tableTituloUnidades);
                document.Add(tableUnidades);
                document.Add(Footer);
                document.Add(t_pago);
                document.Add(fin_pie);
                document.Add(fin);
                document.Add(orinal);
                //document.Add(divTextBancoCheque);
                /*PIE DE PAGINA*/
                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();
                RutaPdf = NomArchivo;
                return true;
                #endregion
            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.Message;
                return false;
            }

        }
        #endregion

        #region Nota Credito

        private bool AddUnidadesNotaCreditoXenco(DataRow dataLine, ref PdfPTable table, iTextSharp.text.Font fontTitleFactura, DataSet dataSet)
        {
            try
            {
                //codigo
                strError += "Add InvoiceLine";
                iTextSharp.text.pdf.PdfPCell celValL = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                                    decimal.Parse((string)dataLine["Number03"]).ToString("N0"), fontTitleFactura)); celValL.Colspan = 1;
                celValL.Padding = 2;
                celValL.Border = PdfPCell.RIGHT_BORDER | PdfPCell.LEFT_BORDER;
                //celTextL.BorderWidthRight = 1;
                //celValL.BorderWidthLeft = 1;
                celValL.HorizontalAlignment = Element.ALIGN_CENTER;
                celValL.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValL);
                strError += "InvoiceLine OK";

                //descriccion
                strError += "Add PartNum";
                iTextSharp.text.pdf.PdfPCell celValCodigo = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    decimal.Parse((string)dataLine["Number02"]).ToString("N0"), fontTitleFactura));
                celValCodigo.Colspan = 1;
                celValCodigo.Padding = 2;
                celValCodigo.Border = PdfPCell.RIGHT_BORDER;
                //celValCodigo.BorderWidthLeft = 1;
                //celDescripcion.BorderColorBottom = BaseColor.WHITE;
                celValCodigo.HorizontalAlignment = Element.ALIGN_CENTER;
                celValCodigo.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValCodigo);
                strError += "Add PartNum OK";

                //cantidad
                strError += "Add SellingShipQty";
                iTextSharp.text.pdf.PdfPCell celValDescripcion = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    decimal.Parse((string)dataLine["Number01"]).ToString("N0"), fontTitleFactura));
                celValDescripcion.Colspan = 1;
                celValDescripcion.Padding = 2;
                celValDescripcion.Border = PdfPCell.RIGHT_BORDER;
                //celValCantidad.BorderWidthLeft = 1;
                celValDescripcion.Padding = 5;
                celValDescripcion.HorizontalAlignment = Element.ALIGN_CENTER;
                celValDescripcion.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValDescripcion);
                strError += "Add SellingShipQty OK";

                //valor unitario
                strError += "Add LineDesc";
                iTextSharp.text.pdf.PdfPCell celValCantidad = new iTextSharp.text.pdf.PdfPCell(new Phrase((string)dataLine["ShortChar02"].ToString(), fontTitleFactura));
                celValCantidad.Colspan = 1;
                celValCantidad.Padding = 2;
                celValCantidad.Border = PdfPCell.RIGHT_BORDER;
                //celValDesc.BorderWidthLeft = 1;
                celValCantidad.Padding = 5;
                celValCantidad.HorizontalAlignment = Element.ALIGN_CENTER;
                celValCantidad.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValCantidad);
                strError += "Add LineDesc OK";

                //subtotaal
                strError += "Add LineDesc";
                iTextSharp.text.pdf.PdfPCell celValCantidad1 = new iTextSharp.text.pdf.PdfPCell(new Phrase((string)dataLine["ShortChar01"].ToString(), fontTitleFactura));
                celValCantidad1.Colspan = 1;
                celValCantidad1.Padding = 2;
                celValCantidad1.Border = 0;
                celValCantidad1.Padding = 5;
                celValCantidad1.Border = PdfPCell.RIGHT_BORDER;
                //celValDesc.BorderWidthLeft = 1;
                celValCantidad1.HorizontalAlignment = Element.ALIGN_CENTER;
                celValCantidad1.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValCantidad1);
                strError += "Add LineDesc OK";

                return true;
            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.Message;
                return false;
            }
        }


        public bool NotaCreditotaxenco(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {
            //NomArchivo = string.Empty;
            //string Ruta = HttpContext.Current.Server.MapPath("~/PDF/" + NIT + "/");
            //string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            //string RutaImg = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";
            //NomArchivo = NombreInvoice + ".pdf";

            NomArchivo = string.Empty;
            string Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            string RutaImg = $@"{AppDomain.CurrentDomain.BaseDirectory}\LOGO XENCO FRA.PNG";
            NomArchivo = NombreInvoice + ".pdf";
            try
            {
                #region Head
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.
                Document document = new Document(iTextSharp.text.PageSize.LETTER);
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;

                // step 3: we open the document     
                document.Open();

                PdfDiv divEspacio = new PdfDiv();
                divEspacio.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio.Height = 10;
                divEspacio.Width = 130;

                PdfDiv divEspacio2 = new PdfDiv();
                divEspacio2.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio2.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio2.Height = 2;
                divEspacio2.Width = 130;

                //FUENTES----------------------------------------------------------------------------------------------------------
                iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle3 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 9, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle2 = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustom = FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitleFactura = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                //FUENTES---------------------------------------------------------------------------------------------------------

                RoundRectangle CelEventBorderRound = new RoundRectangle();


                System.Drawing.Image Logo;
                System.Drawing.Image logo2 = null;
                var request = WebRequest.Create(RutaImg);
                using (var response = request.GetResponse())
                using (var streamLogo2 = response.GetResponseStream())
                {
                    Logo = Bitmap.FromStream(streamLogo2);
                }

                iTextSharp.text.Image ImgLogo2 = iTextSharp.text.Image.GetInstance(Logo, BaseColor.WHITE);
                ImgLogo2.ScaleAbsolute(130, 130);

                #endregion

                PdfPTable salto = new PdfPTable(1);
                salto.WidthPercentage = 100;
                PdfPCell cell_todo = new PdfPCell() { Border = 0, MinimumHeight = 5, };
                salto.AddCell(cell_todo);

                #region Header

                PdfPTable Header = new PdfPTable(new float[] {0.4f, 0.3f, 0.15f, 0.15f, });
                Header.WidthPercentage = 100;
   
                PdfPCell cell_info = new PdfPCell() { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, };
                Paragraph prgTitle1 = new Paragraph(string.Format("XENCO S.A."), fontTitle) { Alignment = Element.ALIGN_LEFT, };
                Paragraph prgTitle2 = new Paragraph(string.Format("NIT. 811.009.452-9"), fontTitle) { Alignment = Element.ALIGN_LEFT, };
                Paragraph prgTitle3 = new Paragraph(string.Format("IVA REGIMEN COMUN"), fontTitle) { Alignment = Element.ALIGN_LEFT, };
                Paragraph prgTitle4 = new Paragraph(string.Format("SOMOS AUTORRETENEDORES RESOLUCIÓN 11054 DE DIC-18-2014"), fontTitle) { Alignment = Element.ALIGN_LEFT, };
                Paragraph prgTitle5 = new Paragraph(string.Format("NO SOMO GRANDES CONTRIBUYENTES"), fontTitle) { Alignment = Element.ALIGN_LEFT, };
                Paragraph prgTitle6 = new Paragraph(string.Format("NO SOMO AGENTES RETENEDORES DE IVA"), fontTitle) { Alignment = Element.ALIGN_LEFT, };
                cell_info.AddElement(prgTitle1);
                cell_info.AddElement(prgTitle2);
                cell_info.AddElement(prgTitle3);
                cell_info.AddElement(prgTitle4);
                cell_info.AddElement(prgTitle5);
                cell_info.AddElement(prgTitle6);
                Header.AddCell(cell_info);

                PdfPCell cell_logo = new PdfPCell() { Border = 0, };
                cell_logo.AddElement(ImgLogo2);
                Header.AddCell(cell_logo);

                PdfPCell cell_Qr = new PdfPCell() { Border = 0, };
                iTextSharp.text.Image ImgQR = iTextSharp.text.Image.GetInstance(QRInvoice, BaseColor.WHITE);
                ImgQR.ScaleAbsolute(60, 60);
                cell_Qr.AddElement(ImgQR);
                Header.AddCell(cell_Qr);

                PdfPCell cell_n_nota = new PdfPCell() { Border = 0, };
                Paragraph prgTitle11 = new Paragraph(string.Format("NOTA CREDITO"), fontTitle) { Alignment = Element.ALIGN_CENTER, };
                Paragraph prgTitle12 = new Paragraph(string.Format(""+DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceNum"]), fontTitle) { Alignment = Element.ALIGN_CENTER, };
                cell_n_nota.AddElement(prgTitle11);
                cell_n_nota.AddElement(prgTitle12);
                Header.AddCell(cell_n_nota);

                #endregion

                #region Body

                PdfPTable Body = new PdfPTable(1);
                Body.WidthPercentage = 100;

                PdfPCell cell_body = new PdfPCell() { MinimumHeight = 50, };
                
                PdfPTable infocabeza = new PdfPTable(2);
                infocabeza.WidthPercentage = 100;

                PdfPCell coll1 = new PdfPCell() {Border=0 };

                PdfPTable table_coll1 = new PdfPTable(2);
                table_coll1.WidthPercentage = 100;
                PdfPCell cell_table_coll1 = new PdfPCell(new Phrase("Fecha: ", fontCustom)) { Border=0, Padding=0,};
                PdfPCell cell_table_coll2 = new PdfPCell(new Phrase("" + DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).ToString("dd/mm/yyyy"), fontCustom)) { Border = 0, Padding = 0, };
                table_coll1.AddCell(cell_table_coll1);
                table_coll1.AddCell(cell_table_coll2);
                coll1.AddElement(table_coll1);

                PdfPTable table_coll2 = new PdfPTable(2);
                table_coll2.WidthPercentage = 100;
                PdfPCell cell_table_coll2_1 = new PdfPCell(new Phrase("Cliente: ", fontCustom)) { Border = 0, Padding = 0, };
                PdfPCell cell_table_coll2_2 = new PdfPCell(new Phrase("" + DsInvoiceAR.Tables["Customer"].Rows[0]["Name"].ToString(), fontCustom)) { Border = 0, Padding = 0, };
                table_coll2.AddCell(cell_table_coll2_1);
                table_coll2.AddCell(cell_table_coll2_2);
                coll1.AddElement(table_coll2);

                PdfPTable table_coll3 = new PdfPTable(2);
                table_coll3.WidthPercentage = 100;
                PdfPCell cell_table_coll3_1 = new PdfPCell(new Phrase("Codigo: ", fontCustom)) { Border = 0, Padding = 0, };
                PdfPCell cell_table_coll3_2 = new PdfPCell(new Phrase("" + decimal.Parse((string)DsInvoiceAR.Tables["Customer"].Rows[0]["Number05"]).ToString("N0"), fontCustom)) { Border = 0, Padding = 0, };
                table_coll3.AddCell(cell_table_coll3_1);
                table_coll3.AddCell(cell_table_coll3_2);
                coll1.AddElement(table_coll3);

                PdfPTable table_coll4 = new PdfPTable(2);
                table_coll4.WidthPercentage = 100;
                PdfPCell cell_table_coll4_1 = new PdfPCell(new Phrase("Tc: ", fontCustom)) { Border = 0, Padding = 0, };
                PdfPCell cell_table_coll4_2 = new PdfPCell(new Phrase("" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar10"].ToString(), fontCustom)) { Border = 0, Padding = 0, };
                table_coll4.AddCell(cell_table_coll4_1);
                table_coll4.AddCell(cell_table_coll2);
                coll1.AddElement(table_coll4);

                PdfPTable table_coll25 = new PdfPTable(2);
                table_coll25.WidthPercentage = 100;
                PdfPCell cell_table_coll5_1 = new PdfPCell(new Phrase("Concepto: ", fontCustom)) { Border = 0, Padding = 0, };
                PdfPCell cell_table_coll5_2 = new PdfPCell(new Phrase("" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar04"].ToString(), fontCustom)) { Border = 0, Padding = 0, };
                table_coll25.AddCell(cell_table_coll5_1);
                table_coll25.AddCell(cell_table_coll5_2);
                coll1.AddElement(table_coll25);

                PdfPTable table_coll6 = new PdfPTable(2);
                table_coll6.WidthPercentage = 100;
                PdfPCell cell_table_coll6_1 = new PdfPCell(new Phrase("suma: ", fontCustom)) { Border = 0, Padding = 0, };
                PdfPCell cell_table_coll6_2 = new PdfPCell(new Phrase("x", fontCustom)) { Border = 0, Padding = 0, };
                table_coll6.AddCell(cell_table_coll6_1);
                table_coll6.AddCell(cell_table_coll6_2);
                coll1.AddElement(table_coll6);

                infocabeza.AddCell(coll1);
                PdfPCell coll2 = new PdfPCell() {Border=0, };

                PdfPTable _table_coll1 = new PdfPTable(2);
                _table_coll1.WidthPercentage = 100;
                PdfPCell _cell_table_coll1 = new PdfPCell(new Phrase("Nro. Recibo: ", fontCustom)) { Border = 0, Padding = 0, };
                PdfPCell _cell_table_coll2 = new PdfPCell(new Phrase("" + decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number07"]).ToString("N0"), fontCustom)) { Border = 0, Padding = 0, };
                _table_coll1.AddCell(_cell_table_coll1);
                _table_coll1.AddCell(_cell_table_coll2);
                coll2.AddElement(_table_coll1);

                PdfPTable _table_coll2 = new PdfPTable(2);
                _table_coll2.WidthPercentage = 100;
                PdfPCell _cell_table_coll2_1 = new PdfPCell(new Phrase("Valor: ", fontCustom)) { Border = 0, Padding = 0, };
                PdfPCell _cell_table_coll2_2 = new PdfPCell(new Phrase("$ " + decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubTotal"]).ToString("N0"), fontCustom)) { Border = 0, Padding = 0, };
                _table_coll2.AddCell(_cell_table_coll2_1);
                _table_coll2.AddCell(_cell_table_coll2_2);
                coll2.AddElement(_table_coll2);

                PdfPTable _table_coll3 = new PdfPTable(2);
                _table_coll3.WidthPercentage = 100;
                PdfPCell _cell_table_coll3_1 = new PdfPCell(new Phrase("Ret. Fuente: ", fontCustom)) { Border = 0, Padding = 0, };
                PdfPCell _cell_table_coll3_2 = new PdfPCell(new Phrase(string.Format("{0:C2}", Convert.ToDecimal(GetValImpuestoByRateCode("retefuente", "0B", DsInvoiceAR).ToString())), fontCustom)) { Border = 0, Padding = 0, };
                _table_coll3.AddCell(_cell_table_coll3_1);
                _table_coll3.AddCell(_cell_table_coll3_2);
                coll2.AddElement(_table_coll3);

                PdfPTable _table_coll4 = new PdfPTable(2);
                _table_coll4.WidthPercentage = 100;
                PdfPCell _cell_table_coll4_1 = new PdfPCell(new Phrase("Descuento: ", fontCustom)) { Border = 0, Padding = 0, };
                PdfPCell _cell_table_coll4_2 = new PdfPCell(new Phrase("$ " + decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number08"]).ToString("N0"), fontCustom)) { Border = 0, Padding = 0, };
                _table_coll4.AddCell(_cell_table_coll4_1);
                _table_coll4.AddCell(_cell_table_coll4_2);
                coll2.AddElement(_table_coll4);

                PdfPTable _table_coll25 = new PdfPTable(2);
                _table_coll25.WidthPercentage = 100;
                PdfPCell _cell_table_coll5_1 = new PdfPCell(new Phrase("Otros: ", fontCustom)) { Border = 0, Padding = 0, };
                PdfPCell _cell_table_coll5_2 = new PdfPCell(new Phrase("$ " + decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number03"]).ToString("N0"), fontCustom)) { Border = 0, Padding = 0, };
                _table_coll25.AddCell(_cell_table_coll5_1);
                _table_coll25.AddCell(_cell_table_coll5_2);
                coll2.AddElement(_table_coll25);

                PdfPTable _table_coll6 = new PdfPTable(2);
                _table_coll6.WidthPercentage = 100;
                PdfPCell _cell_table_coll6_1 = new PdfPCell(new Phrase("Ret. I.V.A: ", fontCustom) ) { Border = 0, Padding = 0, };
                PdfPCell _cell_table_coll6_2 = new PdfPCell(new Phrase(string.Format("{0:C2}", Convert.ToDecimal(GetValImpuestoByRateCode("RETEIVA", "0A", DsInvoiceAR).ToString())), fontCustom)) { Border = 0, Padding = 0, };
                _table_coll6.AddCell(_cell_table_coll6_1);
                _table_coll6.AddCell(_cell_table_coll6_2);
                coll2.AddElement(_table_coll6);

                PdfPTable _table_coll7 = new PdfPTable(2);
                _table_coll7.WidthPercentage = 100;
                PdfPCell _cell_table_coll7_1 = new PdfPCell(new Phrase("Efectivo: ", fontCustom)) { Border = 0, Padding = 0, };
                PdfPCell _cell_table_coll7_2 = new PdfPCell(new Phrase("$ " + decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number04"]).ToString("N0"), fontCustom)) { Border = 0, Padding = 0, };
                _table_coll7.AddCell(_cell_table_coll7_1);
                _table_coll7.AddCell(_cell_table_coll7_2);
                coll2.AddElement(_table_coll7);

                PdfPTable _table_coll8 = new PdfPTable(2);
                _table_coll8.WidthPercentage = 100;
                PdfPCell _cell_table_coll8_1 = new PdfPCell(new Phrase("Cheque: ", fontCustom)) { Border = 0, Padding = 0, };
                PdfPCell _cell_table_coll8_2 = new PdfPCell(new Phrase("" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar09"].ToString(), fontCustom)) { Border = 0, Padding = 0, };
                _table_coll8.AddCell(_cell_table_coll8_1);
                _table_coll8.AddCell(_cell_table_coll8_2);
                coll2.AddElement(_table_coll8);

                infocabeza.AddCell(coll2);
                cell_body.AddElement(infocabeza);
                Body.AddCell(cell_body);

                PdfPTable titulo_unidades = new PdfPTable(5);
                titulo_unidades.WidthPercentage = 100;

                PdfPCell no_factura = new PdfPCell(new Phrase("No. Factura", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, };
                titulo_unidades.AddCell(no_factura);

                PdfPCell vr_factura = new PdfPCell(new Phrase("Vr. Factura", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, };
                titulo_unidades.AddCell(vr_factura);

                PdfPCell vr_pagado = new PdfPCell(new Phrase("Vr. Pagado", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, };
                titulo_unidades.AddCell(vr_pagado);

                PdfPCell indutencion = new PdfPCell(new Phrase("Imputacion", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, };
                titulo_unidades.AddCell(indutencion);

                PdfPCell tipo_recibo = new PdfPCell(new Phrase("Tipo De Recibo", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, };
                titulo_unidades.AddCell(tipo_recibo);


                #endregion

                #region Unidades

                PdfPTable tableUnidades = new PdfPTable(5);
                tableUnidades.WidthPercentage = 100;
                //tableUnidades.SetWidths(DimencionUnidades);

                foreach (DataRow InvoiceLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
                {
                    if (!AddUnidadesNotaCreditoXenco(InvoiceLine, ref tableUnidades, fontCustom, DsInvoiceAR))
                        return false;
                }

                iTextSharp.text.pdf.PdfPCell LineaFinal = new iTextSharp.text.pdf.PdfPCell();
                LineaFinal.Colspan = 5;
                LineaFinal.Padding = 3;
                LineaFinal.Border = 1;
                LineaFinal.BorderColorBottom = BaseColor.WHITE;
                LineaFinal.HorizontalAlignment = Element.ALIGN_CENTER;
                LineaFinal.VerticalAlignment = Element.ALIGN_TOP;
                tableUnidades.AddCell(LineaFinal);


                #endregion

                #region Footer

                PdfPTable Footer = new PdfPTable(5);
                Footer.WidthPercentage = 100;

                PdfPCell elaborado = new PdfPCell(new Phrase("Elaborado", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, };
                Footer.AddCell(elaborado);

                PdfPCell revisado = new PdfPCell(new Phrase("Revisado", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, };
                Footer.AddCell(revisado);

                PdfPCell aprobado = new PdfPCell(new Phrase("Aprobado", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, };
                Footer.AddCell(aprobado);

                PdfPCell firma_sello = new PdfPCell(new Phrase("Firma y Sello", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, Colspan = 2, };
                Footer.AddCell(firma_sello);

                PdfPCell vv = new PdfPCell();
                Footer.AddCell(vv);


                PdfPTable Footerv = new PdfPTable(5);
                Footerv.WidthPercentage = 100;

                PdfPCell elaboradov = new PdfPCell(new Phrase(""+ DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar05"].ToString(), fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP,MinimumHeight=15, };
                Footerv.AddCell(elaboradov);

                PdfPCell revisadov = new PdfPCell(new Phrase(""+ DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar06"].ToString(), fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, };
                Footerv.AddCell(revisadov);

                PdfPCell aprobadov = new PdfPCell(new Phrase(""+ DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar07"].ToString(), fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, };
                Footerv.AddCell(aprobadov);

                PdfPCell firma_sellov = new PdfPCell(new Phrase("", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, Colspan = 2, };
                Footerv.AddCell(firma_sellov);

                PdfPCell vvv = new PdfPCell();
                Footerv.AddCell(vvv);

                #endregion

                #region Exit

                document.Add(Header);
                document.Add(Body);
                document.Add(titulo_unidades);
                document.Add(tableUnidades);
                document.Add(Footer);
                document.Add(Footerv);


                //document.Add(divTextBancoCheque);
                /*PIE DE PAGINA*/
                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();
                RutaPdf = NomArchivo;
                return true;
                #endregion
            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.Message;
                return false;
            }

        }

        #endregion

    }
}
