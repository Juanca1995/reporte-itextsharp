﻿namespace RulesServicesDIAN2.Adquiriente
{
    using iTextSharp.text;
    using iTextSharp.text.pdf;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Diagnostics;
    using System.Drawing;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.NetworkInformation;
    using System.Text;
    using System.Threading.Tasks;
    using System.Web;
    using System.Windows.Forms;

    public partial class pdfEstandarAR
    {
        #region Atmopel
        public bool FacturaNacionalAtmopel(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {
            bool esLocal = Helpers.Compartido.EsLocal;
            string Ruta = string.Empty;
            string RutaImg = string.Empty;
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            if (esLocal)
            {
                Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
                RutaImg = $@"{AppDomain.CurrentDomain.BaseDirectory}\Logo-Atmopel.png";
            }
            else
            {
                Ruta = HttpContext.Current.Server.MapPath("~/PDF/" + NIT + "/");
                RutaImg = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";
            }
            NomArchivo = string.Empty;
            NomArchivo = NombreInvoice + ".pdf";
            try
            {
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                {
                    System.IO.File.Delete(Ruta + NomArchivo);
                }    

                //Dimenciones del documento.

                Document document = new Document(iTextSharp.text.PageSize.LETTER, 20f, 15f, 290f, 190f);
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));

                //-------- Capturando la tablas
                PdfPTable Encabezado = Atmopel.FacturaNacional.Encabezado(RutaImg, DsInvoiceAR);
                PdfPTable DatosCliente = Atmopel.FacturaNacional.DatosCliente(DsInvoiceAR, CUFE, QRInvoice);
                PdfPTable DetalleHead = Atmopel.FacturaNacional.DetalleHead();
                PdfPTable PiePagina = Atmopel.FacturaNacional.PiePagina(DsInvoiceAR);
                writer.PageEvent = new Helpers.Atmopel.EventPageAtmopel(Encabezado, DatosCliente, DetalleHead, PiePagina);

                // step 3: we open the document     
                document.Open();
                document.Add(Atmopel.FacturaNacional.Detalles(DsInvoiceAR, ref strError));
                //document.Add(Helpers.Plantilla_4.PiePagina(DsInvoiceAR));
                /*PIE DE PAGINA*/
                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();
                Helpers.Simex.AddPageNumberPagToSimexExp($"{Ruta}{NomArchivo}", dimxSimex, dimYSimex + 10, Helpers.Fuentes.SimexLeyenda);
                RutaPdf = NomArchivo;
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return false;
            }
        }
        public bool NotaCreditoAtmopel(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {
            bool esLocal = Helpers.Compartido.EsLocal;
            string Ruta = string.Empty;
            string RutaImg = string.Empty;
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            if (esLocal)
            {
                Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
                RutaImg = $@"{AppDomain.CurrentDomain.BaseDirectory}\Logo-Atmopel.png";
            }
            else
            {
                Ruta = HttpContext.Current.Server.MapPath("~/PDF/" + NIT + "/");
                RutaImg = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";
            }
            NomArchivo = string.Empty;
            NomArchivo = NombreInvoice + ".pdf";
            try
            {
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                {
                    System.IO.File.Delete(Ruta + NomArchivo);
                }

                //Dimenciones del documento.

                Document document = new Document(iTextSharp.text.PageSize.LETTER, 20f, 15f, 290f, 190f);
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));

                //-------- Capturando la tablas
                PdfPTable Encabezado = Atmopel.NotaCredito.Encabezado(RutaImg, DsInvoiceAR);
                PdfPTable DatosCliente = Atmopel.NotaCredito.DatosCliente(DsInvoiceAR, CUFE, QRInvoice);
                PdfPTable DetalleHead = Atmopel.NotaCredito.DetalleHead();
                PdfPTable PiePagina = Atmopel.NotaCredito.PiePagina(DsInvoiceAR);
                writer.PageEvent = new Helpers.Atmopel.EventPageAtmopel(Encabezado, DatosCliente, DetalleHead, PiePagina);

                // step 3: we open the document     
                document.Open();
                document.Add(Atmopel.NotaCredito.Detalles(DsInvoiceAR, ref strError));
                //document.Add(Helpers.Plantilla_4.PiePagina(DsInvoiceAR));
                /*PIE DE PAGINA*/
                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();
                RutaPdf = NomArchivo;
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return false;
            }
        }
        //public bool NotaCreditoAtmopel(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        //{
        //    bool esLocal = Helpers.Compartido.EsLocal;
        //    string Ruta = string.Empty;
        //    string RutaImg = string.Empty;
        //    string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
        //    if (esLocal)
        //    {
        //        Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
        //        RutaImg = $@"{AppDomain.CurrentDomain.BaseDirectory}\Logo-Atmopel.png";
        //    }
        //    else
        //    {
        //        Ruta = HttpContext.Current.Server.MapPath("~/PDF/" + NIT + "/");
        //        RutaImg = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";
        //    }
        //    NomArchivo = string.Empty;
        //    NomArchivo = NombreInvoice + ".pdf";
        //    try
        //    {
        //        //Validamos Existencia del Directorio.
        //        if (!System.IO.Directory.Exists(Ruta))
        //            System.IO.Directory.CreateDirectory(Ruta);

        //        //Valido la existencia previa de este archivo.
        //        if (System.IO.File.Exists(Ruta + NomArchivo))
        //        {
        //            System.IO.File.Delete(Ruta + NomArchivo);
        //        }

        //        //Dimenciones del documento.

        //        //Document document = new Document(iTextSharp.text.PageSize.LETTER);
        //        Document document = Helpers.Atmopel.Notas.Documento;
        //        PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));

        //        //-------- Capturando la tablas
        //        PdfPTable Encabezado = Atmopel.NotaCredito.Encabezado(RutaImg);
        //        PdfPTable DatosCliente = Atmopel.NotaCredito.DatosCliente(DsInvoiceAR, InvoiceType);
        //        PdfPTable DetalleHead = Atmopel.NotaCredito.DetalleHead();
        //        PdfPTable PiePagina = Atmopel.NotaCredito.PiePagina(DsInvoiceAR);
        //        writer.PageEvent = new Helpers.Atmopel.Notas.EventPageAtmopelNotas(Encabezado, DatosCliente, DetalleHead, PiePagina);

        //        // step 3: we open the document     
        //        document.Open();
        //        document.Add(Atmopel.NotaCredito.Detalles(DsInvoiceAR));
        //        /*PIE DE PAGINA*/
        //        PdfContentByte pCb = writer.DirectContent;
        //        //PieDePagina(ref pCb);

        //        writer.Flush();
        //        document.Close();
        //        RutaPdf = NomArchivo;
        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.ToString());
        //        return false;
        //    }
        //}
        public bool NotaDebitoAtmopel(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {
            bool esLocal = Helpers.Compartido.EsLocal;
            string Ruta = string.Empty;
            string RutaImg = string.Empty;
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            if (esLocal)
            {
                Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
                RutaImg = $@"{AppDomain.CurrentDomain.BaseDirectory}\Logo-Atmopel.png";
            }
            else
            {
                Ruta = HttpContext.Current.Server.MapPath("~/PDF/" + NIT + "/");
                RutaImg = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";
            }
            NomArchivo = string.Empty;
            NomArchivo = NombreInvoice + ".pdf";
            try
            {
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                {
                    System.IO.File.Delete(Ruta + NomArchivo);
                }

                //Dimenciones del documento.

                //Document document = new Document(iTextSharp.text.PageSize.LETTER);
                Document document = Helpers.Atmopel.Notas.Documento;
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));

                //-------- Capturando la tablas
                PdfPTable Encabezado = Atmopel.NotaDebito.Encabezado(RutaImg);
                PdfPTable DatosCliente = Atmopel.NotaDebito.DatosCliente(DsInvoiceAR, InvoiceType);
                PdfPTable DetalleHead = Atmopel.NotaDebito.DetalleHead();
                PdfPTable PiePagina = Atmopel.NotaDebito.PiePagina(DsInvoiceAR);
                writer.PageEvent = new Helpers.Atmopel.Notas.EventPageAtmopelNotas(Encabezado, DatosCliente, DetalleHead, PiePagina);

                // step 3: we open the document     
                document.Open();
                document.Add(Atmopel.NotaDebito.Detalles(DsInvoiceAR));
                /*PIE DE PAGINA*/
                PdfContentByte pCb = writer.DirectContent;
                //PieDePagina(ref pCb);

                writer.Flush();
                document.Close();
                RutaPdf = NomArchivo;
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return false;
            }
        }
        public class Atmopel
        {
            public class FacturaNacional
            {
                //----------------------------- Data ---------------------------------
                public static PdfPTable Encabezado(string RutaImg, DataSet ds)
                {
                    string InvcHeadCharacter04 = string.Empty;
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "Character04", 0))
                        InvcHeadCharacter04 = ds.Tables["InvcHead"].Rows[0]["Character04"].ToString();

                    string NIT = "NIT: 890 920 782 – 8";

                    Helpers.Plantilla_4.EncabezadoDatos encabezado = new Helpers.Plantilla_4.EncabezadoDatos();
                    encabezado.Dimensiones = new float[] { 1.4f, 1.3f, 1.2f, 1.2f, 0.4f };
                    encabezado.Empresa = "ALMACENES Y TALLERES MOTO PRECISION S.A.";
                    encabezado.EmpresaTexto =  NIT + @"
IVA - REGIMÉN COMÚN
GRAN CONTRIBUYENTE NO RETENER IVA
Resolución No. 000076 de Dic. 01 de 2016
SOMOS RETENEDORES DEL IVA";
                    encabezado.EmpresaPPal = "SEDE ADMINISTRATIVA";
                    encabezado.EmpresaPPalDireccion = @"Calle 73 A No. 45-71 Itagui, Antioquia
PBX: (4) 444-20-44
Linea gratuita: 01 8000 94 25 25";
                    encabezado.Sede01 = "BAYADERA";
                    encabezado.Sede01Direccion = @"Calle 38 No. 52-97 PBX: (4)232-49-32 Línea gratuita: 01 8000 51 25 25";
                    encabezado.Sede02 = "ITAGUI";
                    encabezado.Sede02Direccion = @"Carrera 50 No. 55-24 Itagui, Antioquia Tel: (4) 373-16-18 citagui@atmopel.com.co";
                    encabezado.Sede03 = "";
                    encabezado.Sede03Direccion = @"";
                    encabezado.Sede04 = "C.C. AVENTURA";
                    encabezado.Sede04Direccion = @"Cra 55 No. 65-108 / local 108 Tel: (4) 366-39-79 caventura@atmopel.com.co";
                    encabezado.Sede05 = "BELLO";
                    encabezado.Sede05Direccion = @"Carrera 50 No. 25 B 15 Tel: (4) 464-42-20 cbello@atmopel.com.co";
                    encabezado.Sede06 = "";
                    encabezado.Sede06Direccion = "";
                    encabezado.AVGCerCalidad = "";
                    return Helpers.Plantilla_4.Encabezado(RutaImg, encabezado);
                }
                public static PdfPTable DatosCliente(DataSet ds, string CUFE, System.Drawing.Image image)
                {
                    //Declaracion de variables
                    string TermsCodeDescription = string.Empty;
                    string CustomerAddress = string.Empty;
                    string CustomerCity = string.Empty;
                    string CustomerState = string.Empty;
                    string InvcHeadLegalNumber = string.Empty;
                    string CustomerName = string.Empty;
                    string CustomerResaleID = string.Empty;
                    string CustomerPhoneNum = string.Empty;
                    string CustomerFaxNum = string.Empty;
                    string InvcHeadCharacter03 = string.Empty;
                    string InvcHeadCharacter01 = string.Empty;

                    //Asignacion de varibles
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "TermsCodeDescription", 0))
                        TermsCodeDescription = ds.Tables["InvcHead"].Rows[0]["TermsCodeDescription"].ToString().ToUpper();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "Customer", "City", 0))
                        CustomerCity = ds.Tables["Customer"].Rows[0]["City"].ToString().ToUpper();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "Customer", "State", 0))
                        CustomerState = ds.Tables["Customer"].Rows[0]["State"].ToString().ToUpper();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "Customer", "Address1", 0))
                        CustomerAddress = ds.Tables["Customer"].Rows[0]["Address1"].ToString().ToUpper();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "LegalNumber", 0))
                        InvcHeadLegalNumber = ds.Tables["InvcHead"].Rows[0]["LegalNumber"].ToString().ToUpper();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "Customer", "Name", 0))
                        CustomerName = ds.Tables["Customer"].Rows[0]["Name"].ToString().ToUpper();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "Customer", "ResaleID", 0))
                        CustomerResaleID = ds.Tables["Customer"].Rows[0]["ResaleID"].ToString().ToUpper();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "Customer", "PhoneNum", 0))
                        CustomerPhoneNum = ds.Tables["Customer"].Rows[0]["PhoneNum"].ToString().ToUpper();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "Customer", "FaxNum", 0))
                        CustomerFaxNum = ds.Tables["Customer"].Rows[0]["FaxNum"].ToString().ToUpper();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "Character03", 0))
                        InvcHeadCharacter03 = ds.Tables["InvcHead"].Rows[0]["Character03"].ToString().ToUpper();
                    if(Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead","Character01",0))
                        InvcHeadCharacter01 = ds.Tables["InvcHead"].Rows[0]["Character01"].ToString().ToUpper();

                    //---Creacion de tabla
                    PdfPTable tableFacturar = new PdfPTable(3);
                    //Dimenciones.
                    float[] DimencionFacturar = new float[3];
                    DimencionFacturar[0] = 1.0F;//
                    DimencionFacturar[1] = 1.0F;//
                    DimencionFacturar[2] = 1.0F;//

                    tableFacturar.WidthPercentage = 100;
                    tableFacturar.SetWidths(DimencionFacturar);
                    //----------------------------------------------------------------------------------------------
                    PdfPCell celNofactura = new PdfPCell(new Phrase(
                        $"FACTURA DE VENTA No. {InvcHeadLegalNumber}",
                        FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 12, iTextSharp.text.Font.NORMAL)));
                    celNofactura.HorizontalAlignment = Element.ALIGN_RIGHT;
                    celNofactura.Colspan = 3;
                    celNofactura.Padding = 3;
                    celNofactura.BorderWidthLeft = 0;
                    celNofactura.BorderWidthTop = 0;
                    celNofactura.BorderWidthRight = 0;
                    tableFacturar.AddCell(celNofactura);
                    //----------------------------------------------------------------------------------------------
                    PdfPTable tableDatosCliente = new PdfPTable(1);
                    float[] DimencionFacturarA = new float[2];
                    DimencionFacturarA[0] = 1.0F;//
                    DimencionFacturarA[1] = 9.0F;//

                    tableDatosCliente.WidthPercentage = 100;
                    //-------------------------- Señores -------------------------------------------------------------------
                    PdfPCell celNombreCliente = new PdfPCell();
                    //celNombreCliente.AddElement(phrSeñores);
                    celNombreCliente.Border = 0;
                    //celNombreCliente.Padding = 0;
                    //string CustomerName = string.Empty;
                    //if(Helpers.Compartido.VerificarExistenciaColumnas(DsInvoiceAR, "Customer", "CustomerName", 0))
                    //    CustomerName = DsInvoiceAR.Tables[""]

                    //Helpers.Plantilla_4.DatosClienteTitulos(ref celNombreCliente, "Señores: ", CustomerName);
                    //tableDatosCliente.AddCell(celNombreCliente);

                    //--------------------------- NIT -------------------------------------------------------------------------
                    string mensajeCompleto = "";
                    mensajeCompleto += @"Señores: " + CustomerName + @"
NIT/CC: " + CustomerResaleID + @"
Teléfono: " + CustomerPhoneNum + "   Fax: " + CustomerFaxNum + @"
Domicilio PPal: " + CustomerAddress + @"
Ciudad: " + CustomerCity + @"
Departamento: " + CustomerState + @"
Forma de pago: " + TermsCodeDescription + @"
Domicilio Embarque: " + InvcHeadCharacter03;

                    Paragraph prgDatos = new Paragraph(mensajeCompleto, Helpers.Fuentes.Plantilla_4_fontTitle2);
                    PdfPCell celDatos = new PdfPCell();
                    celDatos.AddElement(prgDatos);
                    celDatos.Border = 0;
                    tableDatosCliente.AddCell(celDatos);

                    //--------------------------------------------------------------------------------------------------------------
                    iTextSharp.text.pdf.PdfPCell celdatosCliente = new iTextSharp.text.pdf.PdfPCell(tableDatosCliente);
                    celdatosCliente.Colspan = 1;
                    tableFacturar.AddCell(celdatosCliente);
                    //--------------------------------------------------------------------------------------------------------------
                    iTextSharp.text.Font fontTitleFecha = FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL);
                    iTextSharp.text.Font fontDMA = FontFactory.GetFont(FontFactory.HELVETICA, 6, BaseColor.GRAY);
                    iTextSharp.text.Font fontDetalleFecha = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 8, iTextSharp.text.Font.NORMAL);
                    //--------------------------------------------------------------------------------------------------------------
                    PdfPTable tableToCel2 = new PdfPTable(2);
                    float[] DimencionDespacharA = new float[2];
                    DimencionDespacharA[0] = 1.0F;//
                    DimencionDespacharA[1] = 1.0F;//

                    tableToCel2.WidthPercentage = 100;
                    tableToCel2.SetWidths(DimencionDespacharA);

                    PdfPTable tableFechaEmision = new PdfPTable(1);

                    iTextSharp.text.pdf.PdfPCell celTextFechaEmision = new iTextSharp.text.pdf.PdfPCell(new Phrase("Fecha Emisión", fontTitleFecha));
                    celTextFechaEmision.Colspan = 1;
                    //celTextFechaEmision.Padding = 3;
                    celTextFechaEmision.Border = 0;
                    celTextFechaEmision.HorizontalAlignment = Element.ALIGN_CENTER;
                    celTextFechaEmision.VerticalAlignment = Element.ALIGN_TOP;
                    tableFechaEmision.AddCell(celTextFechaEmision);

                    iTextSharp.text.pdf.PdfPCell celDMA = new iTextSharp.text.pdf.PdfPCell(new Phrase("DD/MM/AAAA", fontDMA));
                    celDMA.Colspan = 1;
                    //celDMA.Padding = 3;
                    celDMA.Border = 0;
                    celDMA.HorizontalAlignment = Element.ALIGN_CENTER;
                    celDMA.VerticalAlignment = Element.ALIGN_TOP;
                    tableFechaEmision.AddCell(celDMA);

                    //------ Dato fecha y hora ------
                    iTextSharp.text.pdf.PdfPCell celFechaEmisionVal = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                        DateTime.Parse((string)ds.Tables["InvcHead"].Rows[0]["InvoiceDate"]).ToString("dd/MM/yyyy"), fontDetalleFecha));
                    //iTextSharp.text.pdf.PdfPCell celFechaEmisionVal = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    //    datos.FechayHora, fontDetalleFecha));
                    celFechaEmisionVal.Colspan = 1;
                    //celFechaEmisionVal.Padding = 3;
                    celFechaEmisionVal.Border = 0;
                    celFechaEmisionVal.HorizontalAlignment = Element.ALIGN_CENTER;
                    celFechaEmisionVal.VerticalAlignment = Element.ALIGN_TOP;
                    tableFechaEmision.AddCell(celFechaEmisionVal);
                    PdfPCell celFechaEmision = new PdfPCell(tableFechaEmision);
                    tableToCel2.AddCell(celFechaEmision);

                    //---------------------------------------------------------------------------------------------------------------
                    PdfPTable tableFechaDetalleVencimiento = new PdfPTable(1);

                    iTextSharp.text.pdf.PdfPCell celTextDetalleFechaVencimiento = new iTextSharp.text.pdf.PdfPCell(new Phrase("Fecha Vencimiento", fontTitleFecha));
                    celTextDetalleFechaVencimiento.Colspan = 1;
                    //celTextDetalleFechaVencimiento.Padding = 3;
                    celTextDetalleFechaVencimiento.Border = 0;
                    celTextDetalleFechaVencimiento.HorizontalAlignment = Element.ALIGN_CENTER;
                    celTextDetalleFechaVencimiento.VerticalAlignment = Element.ALIGN_TOP;
                    tableFechaDetalleVencimiento.AddCell(celTextDetalleFechaVencimiento);

                    tableFechaDetalleVencimiento.AddCell(celDMA);

                    iTextSharp.text.pdf.PdfPCell celFechaVencimientoVal = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                        DateTime.Parse((string)ds.Tables["InvcHead"].Rows[0]["DueDate"]).ToString("dd/MM/yyyy"), fontDetalleFecha));
                    celFechaVencimientoVal.Colspan = 1;
                    //celFechaVencimientoVal.Padding = 3;
                    celFechaVencimientoVal.Border = 0;
                    celFechaVencimientoVal.HorizontalAlignment = Element.ALIGN_CENTER;
                    celFechaVencimientoVal.VerticalAlignment = Element.ALIGN_TOP;
                    tableFechaDetalleVencimiento.AddCell(celFechaVencimientoVal);

                    PdfPCell celFechaDetalleVencimiento = new PdfPCell(tableFechaDetalleVencimiento);
                    tableToCel2.AddCell(celFechaDetalleVencimiento);
                    //-------------------------------------------------------------------------------------------------------------

                    iTextSharp.text.pdf.PdfPCell celNoPedido = new PdfPCell();
                    //Helpers.Plantilla_4.DatosClienteTitulos(ref celNoPedido, "Pedido No: ", ds.Tables["InvcHead"].Rows[0]["OrderNum"].ToString());
                    Helpers.Plantilla_4.DatosClienteTitulos(ref celNoPedido, "Pedido No: ", InvcHeadCharacter01);
                    tableToCel2.AddCell(celNoPedido);
                    //-------------------------------------------------------------------------------------
                    iTextSharp.text.pdf.PdfPCell celOrderCompra = new iTextSharp.text.pdf.PdfPCell();
                    string PONum = string.Empty;
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "PONum", 0))
                        PONum = (string)ds.Tables["InvcHead"].Rows[0]["PONum"];
                    Helpers.Plantilla_4.DatosClienteTitulos(ref celOrderCompra, "Orden de Compra: ", PONum);
                    tableToCel2.AddCell(celOrderCompra);
                    //-------------------------------------------------------------------------------------

                    PdfPCell celVendededor = new PdfPCell();//new PdfPCell(tableVendedor);
                    Helpers.Plantilla_4.DatosClienteTitulos(ref celVendededor, "Vendedor: ", ds.Tables["InvcHead"].Rows[0]["SalesRepName1"].ToString());
                    celVendededor.Colspan = 2;
                    tableToCel2.AddCell(celVendededor);
                    //-------------------------------------------------------------------------------------

                    PdfPCell celCodigoVendededor = new PdfPCell();//new PdfPCell(tableCodigoVendedor);
                    Helpers.Plantilla_4.DatosClienteTitulos(ref celCodigoVendededor, "Código Vendedor: ", ds.Tables["InvcHead"].Rows[0]["SalesRepCode1"].ToString());
                    celCodigoVendededor.Colspan = 2;
                    tableToCel2.AddCell(celCodigoVendededor);
                    //-------------------------------------------------------------------------------------
                    iTextSharp.text.pdf.PdfPCell cel2 = new iTextSharp.text.pdf.PdfPCell(tableToCel2);
                    cel2.Colspan = 1;
                    tableFacturar.AddCell(cel2);


                    /*CELL QR*/
                    Paragraph prgCufe = new Paragraph("CUFE: " + CUFE, Helpers.Fuentes.Plantilla_4_fontTitle2);
                    prgCufe.Alignment = Element.ALIGN_LEFT;

                    iTextSharp.text.Image QRPdf = iTextSharp.text.Image.GetInstance(image, BaseColor.WHITE);
                    QRPdf.ScaleAbsolute(90f, 90f);
                    QRPdf.Alignment = Element.ALIGN_CENTER;
                    //iTextSharp.text.pdf.PdfPCell celImgQR = new iTextSharp.text.pdf.PdfPCell(QRPdf);
                    iTextSharp.text.pdf.PdfPCell celImgQR = new iTextSharp.text.pdf.PdfPCell();
                    celImgQR.AddElement(QRPdf);
                    celImgQR.AddElement(prgCufe);
                    celImgQR.Colspan = 2;
                    celImgQR.Padding = 3;
                    celImgQR.Border = 0;
                    celImgQR.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                    celImgQR.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    tableFacturar.AddCell(celImgQR);
                    //----- Retornando tabla
                    return tableFacturar;
                }
                public static PdfPTable DetalleHead()
                {
                    Helpers.Plantilla_4.DetalleHeadMinimumHeight = 311; // se establece el tamaño de ocupa las lineas
                    Helpers.Plantilla_4.DimensionUnidades = new float[] { 0.178f, 0.5f, 0.5f, 0.5f, 0.328f, 1.5f, 0.585f, 0.4f, 0.5f, 0.28f };
                    //return Helpers.Plantilla_4.DetalleHead();
                    PdfPTable tableTituloUnidades = new PdfPTable(10);

                    tableTituloUnidades.WidthPercentage = 100;
                    tableTituloUnidades.SetWidths(Helpers.Plantilla_4.DimensionUnidades);
                    //---- Agregando los encabezados -----------------------------
                    Helpers.Plantilla_4.DetalleHeadEncabezados(ref tableTituloUnidades, "L", 0.5f, 0);
                    Helpers.Plantilla_4.DetalleHeadEncabezados(ref tableTituloUnidades, "Código", 0.5f, 0);
                    Helpers.Plantilla_4.DetalleHeadEncabezados(ref tableTituloUnidades, "Referencia", 0.5f, 0);
                    Helpers.Plantilla_4.DetalleHeadEncabezados(ref tableTituloUnidades, "Cantidad", 0.5f, 0);
                    Helpers.Plantilla_4.DetalleHeadEncabezados(ref tableTituloUnidades, "Unidad", 0.5f, 0);
                    Helpers.Plantilla_4.DetalleHeadEncabezados(ref tableTituloUnidades, "Descripción", 0.5f, 0);
                    Helpers.Plantilla_4.DetalleHeadEncabezados(ref tableTituloUnidades, "Valor Unitario", 0.5f, 0);
                    Helpers.Plantilla_4.DetalleHeadEncabezados(ref tableTituloUnidades, "Descuento", 0.5f, 0);
                    Helpers.Plantilla_4.DetalleHeadEncabezados(ref tableTituloUnidades, "Valor Total", 0.5f, 0);
                    Helpers.Plantilla_4.DetalleHeadEncabezados(ref tableTituloUnidades, "% IVA", 0.5f, 0.5f);
                    //---- Agregando las celdas fijas --------
                    Helpers.Plantilla_4.AddCeldasCabecera(ref tableTituloUnidades, 0, 0.5f, 0, 0); //Linea
                    Helpers.Plantilla_4.AddCeldasCabecera(ref tableTituloUnidades, 0, 0.5f, 0, 0); //Código
                    Helpers.Plantilla_4.AddCeldasCabecera(ref tableTituloUnidades, 0, 0.5f, 0, 0); //Referencia
                    Helpers.Plantilla_4.AddCeldasCabecera(ref tableTituloUnidades, 0, 0.5f, 0, 0); //Cantidad
                    Helpers.Plantilla_4.AddCeldasCabecera(ref tableTituloUnidades, 0, 0.5f, 0, 0); //Unidad
                    Helpers.Plantilla_4.AddCeldasCabecera(ref tableTituloUnidades, 0, 0.5f, 0, 0); //Descripcion
                    Helpers.Plantilla_4.AddCeldasCabecera(ref tableTituloUnidades, 0, 0.5f, 0, 0); //Valor unitario
                    Helpers.Plantilla_4.AddCeldasCabecera(ref tableTituloUnidades, 0, 0.5f, 0, 0); //Descuento
                    Helpers.Plantilla_4.AddCeldasCabecera(ref tableTituloUnidades, 0, 0.5f, 0, 0); //Valor total
                    Helpers.Plantilla_4.AddCeldasCabecera(ref tableTituloUnidades, 0, 0.5f, 0.5f, 0); // % IVA
                    //--- retorno de la tabla
                    return tableTituloUnidades;
                }
                public static PdfPTable Detalles(DataSet ds, ref string strError)
                {
                    //----- Detalles ----------
                    PdfPTable tableUnidades = new PdfPTable(10);
                    tableUnidades.WidthPercentage = 100;
                    tableUnidades.SetWidths(new float[] { 0.178f, 0.5f, 0.5f, 0.5f, 0.328f, 1.5f, 0.585f, 0.4f, 0.5f, 0.28f });
                    decimal totalDescuento = 0;
                    foreach (DataRow InvoiceLine in ds.Tables["InvcDtl"].Rows)
                    {
                        Helpers.Atmopel.AddUnidadesAtmopel(InvoiceLine, ref tableUnidades, Helpers.Fuentes.Plantilla_4_fontCustom, ds, ref strError);
                        if(Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcDtl", "DspDocDiscount"))
                            totalDescuento += decimal.Parse((string)InvoiceLine["DspDocDiscount"]);
                    }
                    return tableUnidades;
                }
                public static PdfPTable PiePagina(DataSet ds)
                {
                    //----- Variables
                    string InvcHeadNumber01 = string.Empty;
                    string InvcHeadNumber02 = string.Empty;
                    string InvcHeadNumber05 = string.Empty;
                    string InvcHeadNumber06 = "0";
                    string InvcHeadNumber03 = "0";
                    string InvcHeadCharacter04 = string.Empty;
                    string InvcHeadNumber04 = string.Empty;
                    string InvcHeadDocTaxAmt = "0";
                    //------ Asignación de variables ----
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "Number04", 0))
                        InvcHeadNumber04 = ds.Tables["InvcHead"].Rows[0]["Number04"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "Character04", 0))
                        InvcHeadCharacter04 = ds.Tables["InvcHead"].Rows[0]["Character04"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "Number01", 0))
                        InvcHeadNumber01 = decimal.Parse((string)ds.Tables["InvcHead"].Rows[0]["Number01"]).ToString("N2");
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "Number02", 0))
                        InvcHeadNumber02 = decimal.Parse((string)ds.Tables["InvcHead"].Rows[0]["Number02"]).ToString("N2");
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "Number05", 0))
                        InvcHeadNumber05 = decimal.Parse((string)ds.Tables["InvcHead"].Rows[0]["Number05"]).ToString("N2");
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "Number06", 0))
                        InvcHeadNumber06 = decimal.Parse((string)ds.Tables["InvcHead"].Rows[0]["Number06"]).ToString("N2");
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "Number03", 0))
                        InvcHeadNumber03 = decimal.Parse((string)ds.Tables["InvcHead"].Rows[0]["Number03"]).ToString("N2");
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "DocTaxAmt", 0))
                        InvcHeadDocTaxAmt = decimal.Parse((string)ds.Tables["InvcHead"].Rows[0]["DocTaxAmt"]).ToString("N2");
                    //--------------

                    Helpers.Plantilla_4.PiePaginaDatos datos = new Helpers.Plantilla_4.PiePaginaDatos();
                    PdfPCell celArticulosTotales = new PdfPCell();
                    celArticulosTotales.Border = 0;
                    //----- realizando consulta --------
                    IEnumerable<Helpers.Plantilla_4.InvcDtl> ListaInvcDtl = null;
                    if(ds.Tables.Contains("InvcDtl"))
                    {
                        if(Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcDtl","XPartNum"))
                        {
                            ListaInvcDtl = from row in ds.Tables["InvcDtl"].AsEnumerable()
                                           select new Helpers.Plantilla_4.InvcDtl
                                           {
                                               Company = row["Company"].ToString(),
                                               InvoiceNum = Convert.ToInt32(row["InvoiceNum"]),
                                               InvoiceLine = Convert.ToInt32(row["InvoiceLine"]),
                                               PartNum = row["PartNum"].ToString(),
                                               XPartNum = row["XPartNum"].ToString(),
                                               SellingShipQty = Convert.ToDecimal(row["SellingShipQty"])
                                           };
                        }
                        else
                        {
                            ListaInvcDtl = from row in ds.Tables["InvcDtl"].AsEnumerable()
                                           select new Helpers.Plantilla_4.InvcDtl
                                           {
                                               Company = row["Company"].ToString(),
                                               InvoiceNum = Convert.ToInt32(row["InvoiceNum"]),
                                               InvoiceLine = Convert.ToInt32(row["InvoiceLine"]),
                                               PartNum = row["PartNum"].ToString(),
                                               XPartNum = string.Empty,
                                               SellingShipQty = Convert.ToDecimal(row["SellingShipQty"])
                                           };
                        }
                        
                    }
                    //---- Sumatoria de las cantidades --------
                    decimal Sumatoria = 0;
                    if(ListaInvcDtl != null && ListaInvcDtl.Count() > 0)
                    {
                        var suma = ListaInvcDtl.Sum(row => row.SellingShipQty);
                        Sumatoria = suma;
                    }

                    //Helpers.Plantilla_4.DatosClienteTitulos(ref celArticulosTotales, "Número total de artículos entregados: ", Sumatoria.ToString("N2"));
                    Helpers.Plantilla_4.DatosClienteTitulos(ref celArticulosTotales, "Número total de artículos entregados: ", Convert.ToDecimal(InvcHeadNumber04).ToString("N2"));
                    datos.CelTotalArticulos = celArticulosTotales;
                    //---- Asignacion de clase de InvcMisc -----
                    decimal Seguro = 0;
                    IEnumerable<Helpers.Plantilla_4.InvcMisc> ListaInvcMisc = null;
                    if(ds.Tables.Contains("InvcMisc"))
                    {
                        ListaInvcMisc = from row in ds.Tables["InvcMisc"].AsEnumerable()
                                        select new Helpers.Plantilla_4.InvcMisc
                                        {
                                            Company = row["Company"].ToString(),
                                            InvoiceNum = Convert.ToInt32(row["InvoiceNum"]),
                                            InvoiceLine = Convert.ToInt32(row["InvoiceLine"]),
                                            MiscCode = row["MiscCode"].ToString(),
                                            Description = row["Description"].ToString(),
                                            MiscAmt = Convert.ToDecimal(row["MiscAmt"]),
                                            DocMiscAmt = Convert.ToDecimal(row["DocMiscAmt"])
                                        };
                    }
                    if(ListaInvcMisc != null && ListaInvcMisc.Count() > 0)
                    {
                        Seguro = ListaInvcMisc.Sum(row => row.DocMiscAmt);
                    }

                    datos.Gravado = true;
                    datos.Excluido = true;
                    datos.Seguro = true;
                    datos.Notas = @"PARA DEVOLUCION DE MERCANCIA DEBE SEGUIR LAS POLITICAS ESTABLECIDAS POR LA EMPRESA – EVITESE INCONVENIENTES - LLAMAR DENTRO DEL TIEMPO A LA LINEA DE ATENCION GRATUITA 018000942525.

LOS ELEMENTOS IMPORTANTES DEL CONTRATO SE DEFINEN EN ITAGUI ANTIOQUIA.";
                    datos.ObservacionesCita = @"Los precios estan sujetos a cambio en cualquier momento y sin previo aviso.
Los productos aquí cotizados estan sujetos a disponibilidad de inventario." +"\n\n";
                    datos.LeyendaCopia = false;
                    datos.NITReferenia = false;
                    datos.BarCode01 = false;
                    datos.BarCode02 = false;
                    datos.SeguroValor = Seguro;
                    //------------------------------------------------------------
                    //return Helpers.Plantilla_4.PiePagina(ds, datos);

                    decimal totalDescuento = 0;
                    foreach (DataRow InvoiceLine in ds.Tables["InvcDtl"].Rows)
                    {
                        if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcDtl", "DspDocDiscount"))
                            totalDescuento += decimal.Parse((string)InvoiceLine["DspDocDiscount"]);
                    }

                    PdfPTable tableObsTotales = new PdfPTable(3);
                    tableObsTotales.WidthPercentage = 100;
                    float[] dimTableObsTotales = new float[3];
                    dimTableObsTotales[0] = 2.0f;
                    dimTableObsTotales[1] = 0.8f;
                    dimTableObsTotales[2] = 0.85f;
                    tableObsTotales.SetWidths(dimTableObsTotales);
                    //---------------------------------------------------------------------
                    PdfPTable tableObsercaciones = new PdfPTable(1);

                    if (datos.CelTotalArticulos != null)
                    {
                        tableObsercaciones.AddCell(datos.CelTotalArticulos);
                    }

                    PdfPCell celTextObservaciones = new PdfPCell(new Phrase("OBSERVACIONES\n\n", Helpers.Fuentes.Plantilla_4_fontTitleFactura));
                    celTextObservaciones.Border = 0;
                    tableObsercaciones.AddCell(celTextObservaciones);

                    Chunk chkObservaciones = new Chunk(datos.ObservacionesCita, Helpers.Fuentes.Plantilla_4_fontTitle2);
                    Chunk chkResolucion = new Chunk(InvcHeadCharacter04, Helpers.Fuentes.Arial7Negrilla);

                    Paragraph prgObservaciones = new Paragraph();
                    prgObservaciones.Add(chkObservaciones);
                    prgObservaciones.Add(chkResolucion);
                    //PdfPCell celTextPedirCita = new PdfPCell(new Phrase(datos.ObservacionesCita, Helpers.Fuentes.Plantilla_4_fontTitle2));
                    PdfPCell celTextPedirCita = new PdfPCell(prgObservaciones);

                    celTextPedirCita.Border = 0;
                    tableObsercaciones.AddCell(celTextPedirCita);

                    PdfPCell celObservaciones = new PdfPCell(tableObsercaciones);
                    tableObsTotales.AddCell(celObservaciones);
                    //----------------------------------------------------------------------
                    PdfPCell celFimarSello = new PdfPCell(new Phrase("\n\n\n\n_______________________________\nFirma y Sello", Helpers.Fuentes.Plantilla_4_fontTitle2));
                    celFimarSello.HorizontalAlignment = Element.ALIGN_CENTER;
                    celFimarSello.VerticalAlignment = Element.ALIGN_BOTTOM;
                    tableObsTotales.AddCell(celFimarSello);
                    //----------------------------------------------------------------------
                    PdfPTable tableTotales = new PdfPTable(2);
                    float[] dimTableTotales = new float[2];
                    dimTableTotales[0] = 1.1f;
                    dimTableTotales[1] = 1.7f;
                    tableTotales.SetWidths(dimTableTotales);

                    Helpers.Plantilla_4.PiePaginaTotales(ref tableTotales, "Subtotal", decimal.Parse(InvcHeadNumber01).ToString("N2"));
                    Helpers.Plantilla_4.PiePaginaTotales(ref tableTotales, "Descuento", InvcHeadNumber05);
                    Helpers.Plantilla_4.PiePaginaTotales(ref tableTotales, "Valor Fletes", decimal.Parse(InvcHeadNumber06).ToString("N2"));
                    //if (datos.Seguro)
                    //    Helpers.Plantilla_4.PiePaginaTotales(ref tableTotales, "Seguro", datos.SeguroValor.ToString("N2"));
                    if (datos.Seguro)
                        Helpers.Plantilla_4.PiePaginaTotales(ref tableTotales, "Seguro", decimal.Parse(InvcHeadNumber03).ToString("N2"));
                    Helpers.Plantilla_4.PiePaginaTotales(ref tableTotales, "Total Bruto", decimal.Parse((string)ds.Tables["InvcHead"].Rows[0]["DspDocSubTotal"]).ToString("N2"));
                    var Iva = Helpers.Compartido.GetValImpuestoByID("01", ds);
                    Helpers.Plantilla_4.PiePaginaTotales(ref tableTotales, "Valor IVA", InvcHeadDocTaxAmt);
                    if (datos.Gravado)
                        Helpers.Plantilla_4.PiePaginaTotales(ref tableTotales, "Gravado", InvcHeadNumber01);
                    if (datos.Excluido)
                        Helpers.Plantilla_4.PiePaginaTotales(ref tableTotales, "Excluido", InvcHeadNumber02);

                    //PiePaginaTotales(ref tableTotales, "Descuento", totalDescuento.ToString("N2"));
                    //Helpers.Plantilla_4.PiePaginaTotales(ref tableTotales, "Subtotal", decimal.Parse((string)ds.Tables["InvcHead"].Rows[0]["DspDocSubTotal"]).ToString("N2"));
                    //Helpers.Plantilla_4.PiePaginaTotales(ref tableTotales, "Rete IVA 15%", "xxxxx");

                    PdfPCell celTotales = new PdfPCell(tableTotales);
                    celTotales.Padding = 2;
                    tableObsTotales.AddCell(celTotales);

                    PdfPCell celTotalLetras = new PdfPCell(new Phrase(
                        $"Son: **{Helpers.Compartido.Nroenletras((string)ds.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"])}**", Helpers.Fuentes.Plantilla_4_fontTitle2));
                    celTotalLetras.Colspan = 2;
                    tableObsTotales.AddCell(celTotalLetras);

                    PdfPTable tableTotalFactura = new PdfPTable(2);
                    tableTotalFactura.SetWidths(dimTableTotales);

                    PdfPCell celTextTotal = new PdfPCell(new Phrase("Total Factura", Helpers.Fuentes.Plantilla_4_fontTitleFactura));
                    celTextTotal.BackgroundColor = BaseColor.LIGHT_GRAY;
                    celTextTotal.Border = 0;
                    tableTotalFactura.AddCell(celTextTotal);

                    PdfPCell celValTotal = new PdfPCell(new Phrase(
                        decimal.Parse((string)ds.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"]).ToString("N2"),
                        Helpers.Fuentes.Plantilla_4_fontTitleFactura));
                    celValTotal.HorizontalAlignment = Element.ALIGN_RIGHT;
                    celValTotal.BackgroundColor = BaseColor.LIGHT_GRAY;
                    celValTotal.Border = 0;
                    tableTotalFactura.AddCell(celValTotal);

                    PdfPCell celTotal = new PdfPCell(tableTotalFactura);
                    tableObsTotales.AddCell(celTotal);
                    //---------------------------------------------------------------------------------------------

                    PdfPTable tableResolucion = new PdfPTable(1);
                    tableResolucion.WidthPercentage = 100;

                    PdfPCell celResolucion = new PdfPCell(new Phrase(datos.Notas, Helpers.Fuentes.Plantilla_4_fontTitle2));
                    celResolucion.HorizontalAlignment = Element.ALIGN_CENTER;
                    celResolucion.Border = 0;
                    tableResolucion.AddCell(celResolucion);
                    //------------------------------------------------------------------------------------------------------------
                    if (datos.LeyendaCopia)
                    {
                        PdfPTable tableTextCopia = new PdfPTable(3);
                        tableTextCopia.WidthPercentage = 100;

                        PdfPCell celTextNumRot = new PdfPCell(new Phrase("NumRot 2.0 Version:Factura30 / FAcx20170213.ps",
                            FontFactory.GetFont(FontFactory.HELVETICA, 5, iTextSharp.text.Font.NORMAL)));
                        celTextNumRot.PaddingTop = 4;
                        celTextNumRot.BorderWidthRight = PdfPCell.NO_BORDER;
                        celTextNumRot.BorderWidthLeft = PdfPCell.NO_BORDER;
                        tableTextCopia.AddCell(celTextNumRot);

                        PdfPCell celTextCopia = new PdfPCell(new Phrase("- COPIA -", Helpers.Fuentes.Plantilla_4_fontTitleFactura));
                        celTextCopia.PaddingTop = 4;
                        celTextCopia.BorderWidthRight = PdfPCell.NO_BORDER;
                        celTextCopia.BorderWidthLeft = PdfPCell.NO_BORDER;
                        celTextCopia.HorizontalAlignment = Element.ALIGN_CENTER;
                        tableTextCopia.AddCell(celTextCopia);

                        PdfPCell celTextPagina = new PdfPCell(new Phrase(" "));
                        celTextPagina.BorderWidthRight = PdfPCell.NO_BORDER;
                        celTextPagina.BorderWidthLeft = PdfPCell.NO_BORDER;
                        tableTextCopia.AddCell(celTextPagina);

                        //----- celda de agregado ---------------
                        PdfPCell celCopia = new PdfPCell();
                        celCopia.AddElement(tableTextCopia);
                        celCopia.Border = 0;
                        tableResolucion.AddCell(celCopia);
                    }
                    //---------------------------------------------------------------------------------------
                    PdfPTable tableTextNitReferencia = new PdfPTable(2);
                    if (datos.NITReferenia)
                    {
                        float[] dimTableTextNitReferencia = new float[2];
                        dimTableTextNitReferencia[0] = 1.068f;
                        dimTableTextNitReferencia[1] = 1.15f;
                        tableTextNitReferencia.SetWidths(dimTableTextNitReferencia);
                        tableTextNitReferencia.WidthPercentage = 100;

                        PdfPCell celTextNitReferenciae = new PdfPCell(new Phrase(" ", Helpers.Fuentes.Plantilla_4_fontTitle));
                        celTextNitReferenciae.HorizontalAlignment = Element.ALIGN_RIGHT;
                        celTextNitReferenciae.Colspan = 1;
                        celTextNitReferenciae.Border = PdfPCell.NO_BORDER;
                        tableTextNitReferencia.AddCell(celTextNitReferenciae);

                        PdfPCell celTextNitReferencia = new PdfPCell(new Phrase(
                            $"ABRASIVOS DE COLOMBIA S.A NIT.{(string)ds.Tables["Company"].Rows[0]["StateTaxID"]}" +
                            $"   Referencia 1 No. xxxxx - Referencia 2 No. {(string)ds.Tables["Customer"].Rows[0]["ResaleID"]}",
                            FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 6f, iTextSharp.text.Font.NORMAL)));
                        celTextNitReferencia.HorizontalAlignment = Element.ALIGN_LEFT;
                        celTextNitReferencia.Colspan = 1;
                        celTextNitReferencia.Border = PdfPCell.NO_BORDER;
                        celTextNitReferencia.BorderWidthTop = 0.5f;
                        tableTextNitReferencia.AddCell(celTextNitReferencia);
                    }
                    //---------------------------------------------------------------------------------------
                    PdfPTable tableBC1 = new PdfPTable(3);
                    tableBC1.HorizontalAlignment = Element.ALIGN_LEFT;
                    float[] dimTableBC1 = new float[3];
                    dimTableBC1[0] = 1.8f;
                    dimTableBC1[1] = 1.0f;
                    dimTableBC1[2] = 0.95f;
                    tableBC1.SetWidths(dimTableBC1);
                    tableBC1.WidthPercentage = 100;

                    PdfPCell celBC1 = new PdfPCell(new Phrase("Bar Code"));
                    celBC1.Border = PdfPCell.NO_BORDER;
                    tableBC1.AddCell(celBC1);

                    PdfPTable tableDatosBC1 = new PdfPTable(2);
                    float[] dimTableDatosBC1 = new float[2];
                    dimTableDatosBC1[0] = 1.3f;
                    dimTableDatosBC1[1] = 0.7f;
                    tableDatosBC1.SetWidths(dimTableDatosBC1);
                    //----------------------------------------------------------------------------------
                    PdfPTable tableDatosComprador = new PdfPTable(2);
                    float[] dimTableDatosComprador = new float[2];
                    dimTableDatosComprador[0] = 0.8f;
                    dimTableDatosComprador[1] = 1.5f;
                    tableDatosComprador.SetWidths(dimTableDatosComprador);

                    PdfPCell celprgComprador = new PdfPCell();
                    celprgComprador.AddElement(new Paragraph("COMPRADOR:", Helpers.Fuentes.Plantilla_4_fontTitle2));
                    celprgComprador.AddElement(new Paragraph((string)ds.Tables["InvcHead"].Rows[0]["CustomerName"]
                        , FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 5, iTextSharp.text.Font.NORMAL)));
                    celprgComprador.Border = PdfPCell.NO_BORDER;
                    celprgComprador.BorderWidthLeft = 0.5f;
                    celprgComprador.Colspan = 2;
                    //celprgComprador.Padding = 3;
                    tableDatosComprador.AddCell(celprgComprador);

                    PdfPCell celprgNit = new PdfPCell();
                    celprgNit.AddElement(new Paragraph("NIT./CC. ", Helpers.Fuentes.Plantilla_4_fontTitle2));
                    celprgNit.Border = PdfPCell.NO_BORDER;
                    celprgNit.BorderWidthLeft = 0.5f;
                    celprgNit.BorderWidthBottom = 0.5f;
                    celprgNit.Colspan = 1;
                    //celprgNit.Padding = 3;
                    tableDatosComprador.AddCell(celprgNit);

                    PdfPCell celprgValNit = new PdfPCell();
                    celprgValNit.AddElement(new Phrase((string)ds.Tables["Customer"].Rows[0]["ResaleID"], Helpers.Fuentes.Plantilla_4_fontTitleFactura));
                    celprgValNit.Border = PdfPCell.NO_BORDER;
                    celprgValNit.BorderWidthBottom = 0.5f;
                    celprgValNit.Colspan = 1;
                    celprgValNit.Padding = 3;
                    tableDatosComprador.AddCell(celprgValNit);

                    PdfPCell celEspacionDatosComprador = new PdfPCell(new Phrase(" "));
                    celEspacionDatosComprador.Colspan = 2;
                    celprgValNit.BorderWidthBottom = 0.5f;
                    celEspacionDatosComprador.Border = PdfPCell.NO_BORDER;
                    tableDatosComprador.AddCell(celEspacionDatosComprador);

                    PdfPCell celDatosComprador = new PdfPCell(tableDatosComprador);
                    celDatosComprador.BorderWidthBottom = PdfPCell.NO_BORDER;
                    celDatosComprador.BorderWidthRight = PdfPCell.NO_BORDER;
                    celDatosComprador.BorderWidthLeft = PdfPCell.NO_BORDER;
                    tableDatosBC1.AddCell(celDatosComprador);
                    //----------------------------------------------------------------------------------
                    PdfPTable tableTotalEfectivo = new PdfPTable(1);
                    Paragraph prgTotalEfectivo = new Paragraph("Total Efectivo\n",
                        FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL));

                    PdfDiv divTotalEfectivo = new iTextSharp.text.pdf.PdfDiv();
                    divTotalEfectivo.Float = iTextSharp.text.pdf.PdfDiv.FloatType.LEFT;
                    divTotalEfectivo.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                    divTotalEfectivo.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                    divTotalEfectivo.Height = 21.2f;
                    divTotalEfectivo.Width = 49f;
                    divTotalEfectivo.PaddingTop = 5;
                    divTotalEfectivo.BackgroundColor = BaseColor.LIGHT_GRAY;

                    PdfPCell celTextTotalEfectivo = new PdfPCell();
                    celTextTotalEfectivo.AddElement(prgTotalEfectivo);
                    celTextTotalEfectivo.BorderWidthBottom = PdfPCell.NO_BORDER;
                    celTextTotalEfectivo.VerticalAlignment = Element.ALIGN_LEFT;
                    celTextTotalEfectivo.HorizontalAlignment = Element.ALIGN_TOP;
                    tableTotalEfectivo.AddCell(celTextTotalEfectivo);

                    PdfPCell celDivTotalEfectivo = new PdfPCell();
                    celDivTotalEfectivo.AddElement(divTotalEfectivo);
                    celDivTotalEfectivo.BorderWidthTop = PdfPCell.NO_BORDER;
                    tableTotalEfectivo.AddCell(celDivTotalEfectivo);

                    PdfPCell celEspacioTotalEfectivo = new PdfPCell(new Phrase(" "));
                    celEspacioTotalEfectivo.Border = PdfPCell.NO_BORDER;
                    tableTotalEfectivo.AddCell(celEspacioTotalEfectivo);

                    PdfPCell celTotalEfectivo = new PdfPCell(tableTotalEfectivo);
                    celTotalEfectivo.BorderWidthBottom = PdfPCell.NO_BORDER;
                    celTotalEfectivo.BorderWidthLeft = PdfPCell.NO_BORDER;
                    celTotalEfectivo.BorderWidthRight = PdfPCell.NO_BORDER;
                    tableDatosBC1.AddCell(celTotalEfectivo);
                    //----------------------------------------------------------------------------------
                    #region DatosConsignarEn
                    PdfPTable tableDatosConsignacion = new PdfPTable(2);
                    tableDatosConsignacion.HorizontalAlignment = Element.ALIGN_RIGHT;
                    tableDatosConsignacion.WidthPercentage = 100;
                    float[] dimTableDatosConsignacion = new float[2];
                    dimTableDatosConsignacion[0] = 1.0f;
                    dimTableDatosConsignacion[1] = 1.5f;
                    tableDatosConsignacion.SetWidths(dimTableDatosConsignacion);

                    PdfPCell celConsignarEn1 = new PdfPCell(new Phrase(
                        "Favor Consignar en:", FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 6.7f, iTextSharp.text.Font.NORMAL)));
                    celConsignarEn1.VerticalAlignment = Element.ALIGN_LEFT;
                    celConsignarEn1.HorizontalAlignment = Element.ALIGN_TOP;
                    celConsignarEn1.Border = PdfPCell.NO_BORDER;
                    celConsignarEn1.Colspan = 2;
                    tableDatosConsignacion.AddCell(celConsignarEn1);

                    PdfPCell celTextBancolombia = new PdfPCell(new Phrase("Bancolombia", FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL)));
                    celTextBancolombia.VerticalAlignment = Element.ALIGN_LEFT;
                    celTextBancolombia.HorizontalAlignment = Element.ALIGN_TOP;
                    celTextBancolombia.Border = PdfPCell.NO_BORDER;
                    celTextBancolombia.Padding = 2;
                    celTextBancolombia.Colspan = 1;
                    tableDatosConsignacion.AddCell(celTextBancolombia);

                    PdfPCell celCtaBancolombia = new PdfPCell(new Phrase("Cta. Cte. 009991132702", FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL)));
                    celCtaBancolombia.VerticalAlignment = Element.ALIGN_LEFT;
                    celCtaBancolombia.Padding = 2;
                    celCtaBancolombia.HorizontalAlignment = Element.ALIGN_TOP;
                    celCtaBancolombia.Border = PdfPCell.NO_BORDER;
                    celCtaBancolombia.Colspan = 1;
                    tableDatosConsignacion.AddCell(celCtaBancolombia);

                    PdfPCell celTextBancoBogota = new PdfPCell(new Phrase("Banco de Bogota", FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL)));
                    celTextBancoBogota.VerticalAlignment = Element.ALIGN_LEFT;
                    celTextBancoBogota.HorizontalAlignment = Element.ALIGN_TOP;
                    celTextBancoBogota.Border = PdfPCell.NO_BORDER;
                    celTextBancoBogota.Padding = 2;
                    celTextBancoBogota.Colspan = 1;
                    tableDatosConsignacion.AddCell(celTextBancoBogota);

                    PdfPCell celCtaBancoBogota = new PdfPCell(new Phrase("Cta. Cte. 349019737", FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL)));
                    celCtaBancoBogota.VerticalAlignment = Element.ALIGN_LEFT;
                    celCtaBancoBogota.HorizontalAlignment = Element.ALIGN_TOP;
                    celCtaBancoBogota.Border = PdfPCell.NO_BORDER;
                    celCtaBancoBogota.Padding = 2;
                    celCtaBancoBogota.Colspan = 1;
                    tableDatosConsignacion.AddCell(celCtaBancoBogota);

                    PdfPCell celTextBancoBBVA = new PdfPCell(new Phrase("Banco BBVA", FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL)));
                    celTextBancoBBVA.VerticalAlignment = Element.ALIGN_LEFT;
                    celTextBancoBBVA.HorizontalAlignment = Element.ALIGN_TOP;
                    celTextBancoBBVA.Border = PdfPCell.NO_BORDER;
                    celTextBancoBBVA.Padding = 2;
                    celTextBancoBBVA.Colspan = 1;
                    tableDatosConsignacion.AddCell(celTextBancoBBVA);

                    PdfPCell celCtaBancoBBVA = new PdfPCell(new Phrase("Cta. Cte. 498000488", FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL)));
                    celCtaBancoBBVA.VerticalAlignment = Element.ALIGN_LEFT;
                    celCtaBancoBBVA.HorizontalAlignment = Element.ALIGN_TOP;
                    celCtaBancoBBVA.Border = PdfPCell.NO_BORDER;
                    celCtaBancoBBVA.Padding = 2;
                    celCtaBancoBBVA.Colspan = 1;
                    tableDatosConsignacion.AddCell(celCtaBancoBBVA);

                    PdfPCell celTextBancoOccidente = new PdfPCell(new Phrase("Banco de Occidente", FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL)));
                    celTextBancoOccidente.VerticalAlignment = Element.ALIGN_LEFT;
                    celTextBancoOccidente.HorizontalAlignment = Element.ALIGN_TOP;
                    celTextBancoOccidente.Border = PdfPCell.NO_BORDER;
                    celTextBancoOccidente.Padding = 2;
                    celTextBancoOccidente.Colspan = 1;
                    tableDatosConsignacion.AddCell(celTextBancoOccidente);

                    PdfPCell celCtaBancoOccidente = new PdfPCell(new Phrase("Cta. Cte. 405059098", FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL)));
                    celCtaBancoOccidente.VerticalAlignment = Element.ALIGN_LEFT;
                    celCtaBancoOccidente.HorizontalAlignment = Element.ALIGN_TOP;
                    celCtaBancoOccidente.Border = PdfPCell.NO_BORDER;
                    celCtaBancoOccidente.Padding = 2;
                    celCtaBancoOccidente.Colspan = 1;
                    tableDatosConsignacion.AddCell(celCtaBancoOccidente);

                    PdfPCell celTextBancoHBank = new PdfPCell(new Phrase("Helm Bank", FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL)));
                    celTextBancoHBank.VerticalAlignment = Element.ALIGN_LEFT;
                    celTextBancoHBank.HorizontalAlignment = Element.ALIGN_TOP;
                    celTextBancoHBank.Border = PdfPCell.NO_BORDER;
                    celTextBancoHBank.Padding = 2;
                    celTextBancoHBank.Colspan = 1;
                    tableDatosConsignacion.AddCell(celTextBancoHBank);

                    PdfPCell celCtaBancoHBank = new PdfPCell(new Phrase("Cta. Cte. 101011062", FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL)));
                    celCtaBancoHBank.VerticalAlignment = Element.ALIGN_LEFT;
                    celCtaBancoHBank.HorizontalAlignment = Element.ALIGN_TOP;
                    celCtaBancoHBank.Border = PdfPCell.NO_BORDER;
                    celCtaBancoHBank.Padding = 2;
                    celCtaBancoHBank.Colspan = 1;
                    tableDatosConsignacion.AddCell(celCtaBancoHBank);
                    #endregion
                    //----------------------------------------------------------------------------------
                    PdfPCell celDatosBC1 = new PdfPCell(tableDatosBC1);
                    celDatosBC1.Border = PdfPCell.NO_BORDER;
                    tableBC1.AddCell(celDatosBC1);

                    PdfPCell celConsignarEn = new PdfPCell(tableDatosConsignacion);
                    celConsignarEn.Border = PdfPCell.NO_BORDER;
                    celConsignarEn.BorderWidthTop = 0.5f;
                    tableBC1.AddCell(celConsignarEn);
                    //------------------------SEGUNTA TABLA DE BC---------------------------------------------------------
                    //PdfDiv divTextBancoEfectivo = new PdfDiv();
                    //divTextBancoEfectivo.BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.DOTTED;
                    //divTextBancoEfectivo.Width = 570;

                    PdfPTable tableTextBancoEfectivo = new PdfPTable(3);
                    tableTextBancoEfectivo.WidthPercentage = 100;

                    PdfPCell celTextBancoEfectivoEspacio1 = new PdfPCell(new Phrase(" "));
                    celTextBancoEfectivoEspacio1.PaddingTop = 4;
                    celTextBancoEfectivoEspacio1.BorderWidthRight = PdfPCell.NO_BORDER;
                    celTextBancoEfectivoEspacio1.BorderWidthLeft = PdfPCell.NO_BORDER;
                    celTextBancoEfectivoEspacio1.BorderWidthTop = PdfPCell.NO_BORDER;
                    tableTextBancoEfectivo.AddCell(celTextBancoEfectivoEspacio1);

                    PdfPCell celTextBancoEfectivoEspacio2 = new PdfPCell(new Phrase(" "));
                    celTextBancoEfectivoEspacio2.PaddingTop = 4;
                    celTextBancoEfectivoEspacio2.BorderWidthRight = PdfPCell.NO_BORDER;
                    celTextBancoEfectivoEspacio2.BorderWidthLeft = PdfPCell.NO_BORDER;
                    celTextBancoEfectivoEspacio2.HorizontalAlignment = Element.ALIGN_CENTER;
                    celTextBancoEfectivoEspacio2.BorderWidthTop = PdfPCell.NO_BORDER;
                    tableTextBancoEfectivo.AddCell(celTextBancoEfectivoEspacio2);

                    PdfPCell celTextBancoEfectivoEspacio = new PdfPCell(new Phrase("- BANCO (EFECTIVO) -", Helpers.Fuentes.Plantilla_4_fontTitleFactura));
                    celTextBancoEfectivoEspacio.BorderWidthRight = PdfPCell.NO_BORDER;
                    celTextBancoEfectivoEspacio.BorderWidthLeft = PdfPCell.NO_BORDER;
                    celTextBancoEfectivoEspacio.BorderWidthTop = PdfPCell.NO_BORDER;
                    tableTextBancoEfectivo.AddCell(celTextBancoEfectivoEspacio);
                    //---
                    PdfPCell celBancoEfectivo = new PdfPCell();
                    celBancoEfectivo.AddElement(tableTextBancoEfectivo);
                    celBancoEfectivo.Colspan = 3;
                    celBancoEfectivo.Padding = 0;
                    celBancoEfectivo.Border = 0;
                    tableBC1.AddCell(celBancoEfectivo);


                    //divTextBancoEfectivo.AddElement(tableTextBancoEfectivo);
                    //---------------------------------------------------------------------------------------------------
                    PdfPTable tableBC2 = new PdfPTable(3);
                    tableBC2.HorizontalAlignment = Element.ALIGN_LEFT;
                    tableBC2.SetWidths(dimTableBC1);
                    tableBC2.WidthPercentage = 100;

                    PdfPCell celBC2 = new PdfPCell(new Phrase("Bar Code 2"));
                    celBC2.Border = PdfPCell.NO_BORDER;
                    tableBC2.AddCell(celBC2);

                    PdfPTable tableDatosBC2 = new PdfPTable(2);
                    tableDatosBC2.SetWidths(dimTableDatosBC1);
                    //----------------------------------------------------------------------------------
                    PdfPTable tableDatosComprador2 = new PdfPTable(2);
                    tableDatosComprador2.SetWidths(dimTableDatosComprador);

                    tableDatosComprador2.AddCell(celprgComprador);

                    PdfPCell celprgNit2 = new PdfPCell();
                    celprgNit2.AddElement(new Paragraph("NIT./CC. ", Helpers.Fuentes.Plantilla_4_fontTitle2));
                    celprgNit2.Border = PdfPCell.NO_BORDER;
                    celprgNit2.BorderWidthLeft = 0.5f;
                    celprgNit2.BorderWidthBottom = 0.5f;
                    celprgNit2.Colspan = 1;
                    //celprgNit.Padding = 3;
                    tableDatosComprador2.AddCell(celprgNit2);

                    PdfPCell celprgValNit2 = new PdfPCell();
                    celprgValNit2.AddElement(new Phrase((string)ds.Tables["Customer"].Rows[0]["ResaleID"], Helpers.Fuentes.Plantilla_4_fontTitleFactura));
                    celprgValNit2.Border = PdfPCell.NO_BORDER;
                    celprgValNit2.BorderWidthBottom = 0.5f;
                    celprgValNit2.Colspan = 1;
                    celprgValNit2.Padding = 3;
                    tableDatosComprador2.AddCell(celprgValNit2);


                    PdfPCell celDatosComprador2 = new PdfPCell(tableDatosComprador2);
                    celDatosComprador2.BorderWidthBottom = PdfPCell.NO_BORDER;
                    celDatosComprador2.BorderWidthRight = PdfPCell.NO_BORDER;
                    celDatosComprador2.BorderWidthLeft = PdfPCell.NO_BORDER;
                    tableDatosBC2.AddCell(celDatosComprador2);
                    //----------------------------------------------------------------------------------
                    PdfPTable tableTotalCheque = new PdfPTable(1);

                    PdfPCell celTextTotalCheques = new PdfPCell();
                    celTextTotalCheques.AddElement(new Paragraph("Total Cheques\n",
                        FontFactory.GetFont(FontFactory.HELVETICA, 6.7f, iTextSharp.text.Font.NORMAL)));
                    celTextTotalCheques.BorderWidthBottom = PdfPCell.NO_BORDER;
                    celTextTotalCheques.VerticalAlignment = Element.ALIGN_LEFT;
                    celTextTotalCheques.HorizontalAlignment = Element.ALIGN_TOP;
                    tableTotalCheque.AddCell(celTextTotalCheques);

                    PdfPCell celDivTotalCheques = new PdfPCell();
                    celDivTotalCheques.AddElement(divTotalEfectivo);
                    celDivTotalCheques.BorderWidthTop = PdfPCell.NO_BORDER;
                    tableTotalCheque.AddCell(celDivTotalCheques);

                    PdfPCell celTotalCheques = new PdfPCell(tableTotalCheque);
                    celTotalCheques.BorderWidthBottom = PdfPCell.NO_BORDER;
                    celTotalCheques.BorderWidthLeft = PdfPCell.NO_BORDER;
                    celTotalCheques.BorderWidthRight = PdfPCell.NO_BORDER;
                    tableDatosBC2.AddCell(celTotalCheques);
                    //----------------------------------------------------------------------------------
                    PdfPTable tableBancoValores = new PdfPTable(3);


                    iTextSharp.text.Font fontCheque = FontFactory.GetFont(FontFactory.HELVETICA, 6.8f, iTextSharp.text.Font.NORMAL);

                    PdfPCell celTextBancoValoresBanco = new PdfPCell(new Phrase("Banco", fontCheque));
                    celTextBancoValoresBanco.Padding = 3;
                    celTextBancoValoresBanco.HorizontalAlignment = Element.ALIGN_CENTER;
                    //celEspacioTotalCheques.Border = PdfPCell.NO_BORDER;
                    tableBancoValores.AddCell(celTextBancoValoresBanco);

                    PdfPCell celTextBancoValoresNoCheque = new PdfPCell(new Phrase("No.Cheque", fontCheque));
                    celTextBancoValoresNoCheque.Padding = 3;
                    celTextBancoValoresNoCheque.HorizontalAlignment = Element.ALIGN_CENTER;
                    tableBancoValores.AddCell(celTextBancoValoresNoCheque);

                    PdfPCell celTextBancoValoresValor = new PdfPCell(new Phrase("Valor", fontCheque));
                    celTextBancoValoresValor.Padding = 3;
                    celTextBancoValoresValor.HorizontalAlignment = Element.ALIGN_CENTER;
                    tableBancoValores.AddCell(celTextBancoValoresValor);

                    for (int i = 0; i < 2; i++)
                    {
                        PdfPCell celBancoValoresBanco = new PdfPCell(new Phrase(" ", Helpers.Fuentes.Plantilla_4_fontTitle));
                        celBancoValoresBanco.Padding = 2.3f;
                        tableBancoValores.AddCell(celBancoValoresBanco);

                        PdfPCell celBancoValoresNoCheque = new PdfPCell(new Phrase(" ", Helpers.Fuentes.Plantilla_4_fontTitle));
                        celBancoValoresNoCheque.Padding = 2.3f;
                        tableBancoValores.AddCell(celBancoValoresNoCheque);

                        PdfPCell celBancoValoresValor = new PdfPCell(new Phrase(" ", Helpers.Fuentes.Plantilla_4_fontTitle));
                        celBancoValoresValor.Padding = 2.3f;
                        tableBancoValores.AddCell(celBancoValoresValor);
                    }

                    PdfPCell celBancoValores = new PdfPCell(tableBancoValores);
                    celBancoValores.Colspan = 2;
                    tableDatosBC2.AddCell(celBancoValores);
                    //----------------------------------------------------------------------------------

                    PdfPCell celDatosBC2 = new PdfPCell(tableDatosBC2);
                    celDatosBC2.Border = PdfPCell.NO_BORDER;
                    tableBC2.AddCell(celDatosBC2);

                    tableBC2.AddCell(celConsignarEn);
                    //---------------------------------------------------------------------------------
                    //PdfDiv divTextBancoCheque = new PdfDiv();
                    //divTextBancoCheque.BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.DOTTED;
                    //divTextBancoCheque.Width = 570;

                    PdfPTable tableTextBancoCheque = new PdfPTable(3);
                    tableTextBancoCheque.WidthPercentage = 100;

                    PdfPCell celTextBancoChequeEspacio1 = new PdfPCell(new Phrase(" "));
                    //celTextNumRot.Border = 0;
                    celTextBancoChequeEspacio1.PaddingTop = 4;
                    celTextBancoChequeEspacio1.Border = PdfPCell.NO_BORDER;
                    tableTextBancoCheque.AddCell(celTextBancoChequeEspacio1);

                    PdfPCell celTextBancoChequeEspacio2 = new PdfPCell(new Phrase(" "));
                    //celTextCopia.Border = 0;
                    celTextBancoChequeEspacio2.PaddingTop = 4;
                    celTextBancoChequeEspacio2.Border = PdfPCell.NO_BORDER;
                    tableTextBancoCheque.AddCell(celTextBancoChequeEspacio2);

                    PdfPCell celTextBancoCheque = new PdfPCell(new Phrase("- BANCO (CHEQUE) -", Helpers.Fuentes.Plantilla_4_fontTitleFactura));
                    celTextBancoCheque.Border = PdfPCell.NO_BORDER;
                    tableTextBancoCheque.AddCell(celTextBancoCheque);

                    PdfPCell celBancoCheque = new PdfPCell();
                    celBancoCheque.AddElement(tableTextBancoCheque);
                    celBancoCheque.Colspan = 3;
                    celBancoCheque.Border = 0;
                    tableBC2.AddCell(celBancoCheque);

                    //divTextBancoCheque.AddElement(tableTextBancoCheque);
                    //------- Combinando todo en una sola tabla
                    PdfPTable tbPiePagina = new PdfPTable(1);
                    tbPiePagina.WidthPercentage = 100;
                    PdfPCell celPiePagina = new PdfPCell();
                    celPiePagina.Border = 0;
                    celPiePagina.Padding = 0;

                    celPiePagina.AddElement(tableObsTotales);
                    celPiePagina.AddElement(Helpers.Plantilla_4.divEspacio2);
                    celPiePagina.AddElement(tableResolucion);
                    //celPiePagina.AddElement(divTextCopia);
                    celPiePagina.AddElement(Helpers.Plantilla_4.divEspacio2);
                    celPiePagina.AddElement(Helpers.Plantilla_4.divEspacio2);
                    if (datos.NITReferenia)
                        celPiePagina.AddElement(tableTextNitReferencia);
                    if (datos.BarCode01)
                    {
                        celPiePagina.AddElement(tableBC1);
                        celPiePagina.AddElement(Helpers.Plantilla_4.divEspacio2);
                        celPiePagina.AddElement(Helpers.Plantilla_4.divEspacio2);
                        //celPiePagina.AddElement(divTextBancoEfectivo);
                        celPiePagina.AddElement(Helpers.Plantilla_4.divEspacio2);
                        celPiePagina.AddElement(Helpers.Plantilla_4.divEspacio2);
                    }
                    if (datos.NITReferenia)
                        celPiePagina.AddElement(tableTextNitReferencia);
                    if (datos.BarCode02)
                    {
                        celPiePagina.AddElement(tableBC2);
                        celPiePagina.AddElement(Helpers.Plantilla_4.divEspacio2);
                        celPiePagina.AddElement(Helpers.Plantilla_4.divEspacio2);
                        //celPiePagina.AddElement(divTextBancoCheque);
                    }
                    tbPiePagina.AddCell(celPiePagina);
                    return tbPiePagina;
                }
            }
            public class NotaCredito
            {

                //public static PdfPTable Encabezado(string RutaImg)
                //{
                //    string[] datos = new string[] 
                //    {
                //        "890.920.782-8",
                //        "ALMACENES Y TALLERES\r\nMOTO PRECISION S.A.\r\nGRAN CONTRIBUYENTE\r\nNO RETENER IVA",
                //        "SOMOS RETENEDORES DEL IVA\r\nCALLE 38 No. 52-97 TELS: 232 49 32 - 232 74 52\r\nFAX 232 01 59\r\nMEDELLIN - COLOMBIA"
                //    };
                //    return Helpers.Atmopel.Notas.Encabezado(RutaImg,datos);
                //}
                //public static PdfPTable DatosCliente(DataSet ds, string tipo)
                //{
                //    return Helpers.Atmopel.Notas.DatosCliente(ds, tipo, "NOTA CREDITO No. ");
                //}
                //public static PdfPTable DetalleHead()
                //{
                //    return Helpers.Atmopel.Notas.DetalleHead(95f);
                //}
                //public static PdfPTable Detalles(DataSet ds)
                //{
                //    return Helpers.Atmopel.Notas.Detalles(ds);
                //}
                //public static PdfPTable PiePagina(DataSet ds)
                //{
                //    return Helpers.Atmopel.Notas.PiePagina(ds);
                //}
                public static PdfPTable Encabezado(string RutaImg, DataSet ds)
                {
                    string InvcHeadCharacter04 = string.Empty;
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "Character04", 0))
                        InvcHeadCharacter04 = ds.Tables["InvcHead"].Rows[0]["Character04"].ToString();

                    string NIT = "NIT: 890 920 782 – 8";

                    Helpers.Plantilla_4.EncabezadoDatos encabezado = new Helpers.Plantilla_4.EncabezadoDatos();
                    encabezado.Dimensiones = new float[] { 1.4f, 1.3f, 1.2f, 1.2f, 0.4f };
                    encabezado.Empresa = "ALMACENES Y TALLERES MOTO PRECISION S.A.";
                    encabezado.EmpresaTexto = NIT + @"
IVA - REGIMÉN COMÚN
GRAN CONTRIBUYENTE NO RETENER IVA
Resolución No. 000076 de Dic. 01 de 2016
SOMOS RETENEDORES DEL IVA";
                    encabezado.EmpresaPPal = "SEDE ADMINISTRATIVA";
                    encabezado.EmpresaPPalDireccion = @"Calle 73 A No. 45-71 Itagui, Antioquia
PBX: (4) 444-20-44
Linea gratuita: 01 8000 94 25 25";
                    encabezado.Sede01 = "BAYADERA";
                    encabezado.Sede01Direccion = @"Calle 38 No. 52-97 PBX: (4)232-49-32 Línea gratuita: 01 8000 51 25 25";
                    encabezado.Sede02 = "ITAGUI";
                    encabezado.Sede02Direccion = @"Carrera 50 No. 55-24 Itagui, Antioquia Tel: (4) 373-16-18 citagui@atmopel.com.co";
                    encabezado.Sede03 = "";
                    encabezado.Sede03Direccion = @"";
                    encabezado.Sede04 = "C.C. AVENTURA";
                    encabezado.Sede04Direccion = @"Cra 55 No. 65-108 / local 108 Tel: (4) 366-39-79 caventura@atmopel.com.co";
                    encabezado.Sede05 = "BELLO";
                    encabezado.Sede05Direccion = @"Carrera 50 No. 25 B 15 Tel: (4) 464-42-20 cbello@atmopel.com.co";
                    encabezado.Sede06 = "";
                    encabezado.Sede06Direccion = "";
                    encabezado.AVGCerCalidad = "";
                    return Helpers.Plantilla_4.Encabezado(RutaImg, encabezado);
                }
                public static PdfPTable DatosCliente(DataSet ds, string CUFE, System.Drawing.Image image)
                {
                    //Declaracion de variables
                    string TermsCodeDescription = string.Empty;
                    string CustomerAddress = string.Empty;
                    string CustomerCity = string.Empty;
                    string CustomerState = string.Empty;
                    string InvcHeadLegalNumber = string.Empty;
                    string CustomerName = string.Empty;
                    string CustomerResaleID = string.Empty;
                    string CustomerPhoneNum = string.Empty;
                    string CustomerFaxNum = string.Empty;
                    string InvcHeadCharacter03 = string.Empty;
                    string InvcHeadCharacter01 = string.Empty;

                    //Asignacion de varibles
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "TermsCodeDescription", 0))
                        TermsCodeDescription = ds.Tables["InvcHead"].Rows[0]["TermsCodeDescription"].ToString().ToUpper();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "Customer", "City", 0))
                        CustomerCity = ds.Tables["Customer"].Rows[0]["City"].ToString().ToUpper();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "Customer", "State", 0))
                        CustomerState = ds.Tables["Customer"].Rows[0]["State"].ToString().ToUpper();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "Customer", "Address1", 0))
                        CustomerAddress = ds.Tables["Customer"].Rows[0]["Address1"].ToString().ToUpper();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "LegalNumber", 0))
                        InvcHeadLegalNumber = ds.Tables["InvcHead"].Rows[0]["LegalNumber"].ToString().ToUpper();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "Customer", "Name", 0))
                        CustomerName = ds.Tables["Customer"].Rows[0]["Name"].ToString().ToUpper();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "Customer", "ResaleID", 0))
                        CustomerResaleID = ds.Tables["Customer"].Rows[0]["ResaleID"].ToString().ToUpper();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "Customer", "PhoneNum", 0))
                        CustomerPhoneNum = ds.Tables["Customer"].Rows[0]["PhoneNum"].ToString().ToUpper();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "Customer", "FaxNum", 0))
                        CustomerFaxNum = ds.Tables["Customer"].Rows[0]["FaxNum"].ToString().ToUpper();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "Character03", 0))
                        InvcHeadCharacter03 = ds.Tables["InvcHead"].Rows[0]["Character03"].ToString().ToUpper();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "Character01", 0))
                        InvcHeadCharacter01 = ds.Tables["InvcHead"].Rows[0]["Character01"].ToString().ToUpper();

                    //---Creacion de tabla
                    PdfPTable tableFacturar = new PdfPTable(3);
                    //Dimenciones.
                    float[] DimencionFacturar = new float[3];
                    DimencionFacturar[0] = 1.0F;//
                    DimencionFacturar[1] = 1.0F;//
                    DimencionFacturar[2] = 1.0F;//

                    tableFacturar.WidthPercentage = 100;
                    tableFacturar.SetWidths(DimencionFacturar);
                    //----------------------------------------------------------------------------------------------
                    PdfPCell celNofactura = new PdfPCell(new Phrase(
                        $"NOTA CREDITO No. {InvcHeadLegalNumber}",
                        FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 12, iTextSharp.text.Font.NORMAL)));
                    celNofactura.HorizontalAlignment = Element.ALIGN_RIGHT;
                    celNofactura.Colspan = 3;
                    celNofactura.Padding = 3;
                    celNofactura.BorderWidthLeft = 0;
                    celNofactura.BorderWidthTop = 0;
                    celNofactura.BorderWidthRight = 0;
                    tableFacturar.AddCell(celNofactura);
                    //----------------------------------------------------------------------------------------------
                    PdfPTable tableDatosCliente = new PdfPTable(1);
                    float[] DimencionFacturarA = new float[2];
                    DimencionFacturarA[0] = 1.0F;//
                    DimencionFacturarA[1] = 9.0F;//

                    tableDatosCliente.WidthPercentage = 100;
                    //-------------------------- Señores -------------------------------------------------------------------
                    PdfPCell celNombreCliente = new PdfPCell();
                    //celNombreCliente.AddElement(phrSeñores);
                    celNombreCliente.Border = 0;

                    //--------------------------- NIT -------------------------------------------------------------------------
                    string mensajeCompleto = "";
                    mensajeCompleto += @"Señores: " + CustomerName + @"
NIT/CC: " + CustomerResaleID + @"
Teléfono: " + CustomerPhoneNum + "   Fax: " + CustomerFaxNum + @"
Domicilio PPal: " + CustomerAddress + @"
Ciudad: " + CustomerCity + @"
Departamento: " + CustomerState + @"
Forma de pago: " + TermsCodeDescription;

                    Paragraph prgDatos = new Paragraph(mensajeCompleto, Helpers.Fuentes.Plantilla_4_fontTitle2);
                    PdfPCell celDatos = new PdfPCell();
                    celDatos.AddElement(prgDatos);
                    celDatos.Border = 0;
                    tableDatosCliente.AddCell(celDatos);
                    //--------------------------------------------------------------------------------------------------------------
                    iTextSharp.text.pdf.PdfPCell celdatosCliente = new iTextSharp.text.pdf.PdfPCell(tableDatosCliente);
                    celdatosCliente.Colspan = 1;
                    tableFacturar.AddCell(celdatosCliente);
                    //--------------------------------------------------------------------------------------------------------------
                    iTextSharp.text.Font fontTitleFecha = FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL);
                    iTextSharp.text.Font fontDMA = FontFactory.GetFont(FontFactory.HELVETICA, 6, BaseColor.GRAY);
                    iTextSharp.text.Font fontDetalleFecha = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 8, iTextSharp.text.Font.NORMAL);
                    //--------------------------------------------------------------------------------------------------------------
                    PdfPTable tableToCel2 = new PdfPTable(2);
                    float[] DimencionDespacharA = new float[2];
                    DimencionDespacharA[0] = 1.0F;//
                    DimencionDespacharA[1] = 1.0F;//

                    tableToCel2.WidthPercentage = 100;
                    tableToCel2.SetWidths(DimencionDespacharA);

                    PdfPTable tableFechaEmision = new PdfPTable(1);

                    iTextSharp.text.pdf.PdfPCell celTextFechaEmision = new iTextSharp.text.pdf.PdfPCell(new Phrase("Fecha Emisión", fontTitleFecha));
                    celTextFechaEmision.Colspan = 1;
                    //celTextFechaEmision.Padding = 3;
                    celTextFechaEmision.Border = 0;
                    celTextFechaEmision.HorizontalAlignment = Element.ALIGN_CENTER;
                    celTextFechaEmision.VerticalAlignment = Element.ALIGN_TOP;
                    tableFechaEmision.AddCell(celTextFechaEmision);

                    iTextSharp.text.pdf.PdfPCell celDMA = new iTextSharp.text.pdf.PdfPCell(new Phrase("DD/MM/AAAA", fontDMA));
                    celDMA.Colspan = 1;
                    //celDMA.Padding = 3;
                    celDMA.Border = 0;
                    celDMA.HorizontalAlignment = Element.ALIGN_CENTER;
                    celDMA.VerticalAlignment = Element.ALIGN_TOP;
                    tableFechaEmision.AddCell(celDMA);

                    //------ Dato fecha y hora ------
                    iTextSharp.text.pdf.PdfPCell celFechaEmisionVal = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                        DateTime.Parse((string)ds.Tables["InvcHead"].Rows[0]["InvoiceDate"]).ToString("dd/MM/yyyy"), fontDetalleFecha));
                    //iTextSharp.text.pdf.PdfPCell celFechaEmisionVal = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    //    datos.FechayHora, fontDetalleFecha));
                    celFechaEmisionVal.Colspan = 1;
                    //celFechaEmisionVal.Padding = 3;
                    celFechaEmisionVal.Border = 0;
                    celFechaEmisionVal.HorizontalAlignment = Element.ALIGN_CENTER;
                    celFechaEmisionVal.VerticalAlignment = Element.ALIGN_TOP;
                    tableFechaEmision.AddCell(celFechaEmisionVal);
                    PdfPCell celFechaEmision = new PdfPCell(tableFechaEmision);
                    tableToCel2.AddCell(celFechaEmision);

                    //---------------------------------------------------------------------------------------------------------------
                    PdfPTable tableFechaDetalleVencimiento = new PdfPTable(1);

                    iTextSharp.text.pdf.PdfPCell celTextDetalleFechaVencimiento = new iTextSharp.text.pdf.PdfPCell(new Phrase("Fecha Vencimiento", fontTitleFecha));
                    celTextDetalleFechaVencimiento.Colspan = 1;
                    //celTextDetalleFechaVencimiento.Padding = 3;
                    celTextDetalleFechaVencimiento.Border = 0;
                    celTextDetalleFechaVencimiento.HorizontalAlignment = Element.ALIGN_CENTER;
                    celTextDetalleFechaVencimiento.VerticalAlignment = Element.ALIGN_TOP;
                    tableFechaDetalleVencimiento.AddCell(celTextDetalleFechaVencimiento);

                    tableFechaDetalleVencimiento.AddCell(celDMA);

                    iTextSharp.text.pdf.PdfPCell celFechaVencimientoVal = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                        DateTime.Parse((string)ds.Tables["InvcHead"].Rows[0]["DueDate"]).ToString("dd/MM/yyyy"), fontDetalleFecha));
                    celFechaVencimientoVal.Colspan = 1;
                    //celFechaVencimientoVal.Padding = 3;
                    celFechaVencimientoVal.Border = 0;
                    celFechaVencimientoVal.HorizontalAlignment = Element.ALIGN_CENTER;
                    celFechaVencimientoVal.VerticalAlignment = Element.ALIGN_TOP;
                    tableFechaDetalleVencimiento.AddCell(celFechaVencimientoVal);

                    PdfPCell celFechaDetalleVencimiento = new PdfPCell(tableFechaDetalleVencimiento);
                    tableToCel2.AddCell(celFechaDetalleVencimiento);
                    //-------------------------------------------------------------------------------------------------------------

                    //iTextSharp.text.pdf.PdfPCell celNoPedido = new PdfPCell();
                    //Helpers.Plantilla_4.DatosClienteTitulos(ref celNoPedido, "Pedido No: ", ds.Tables["InvcHead"].Rows[0]["OrderNum"].ToString());
                    //tableToCel2.AddCell(celNoPedido);
                    //-------------------------------------------------------------------------------------
                    //iTextSharp.text.pdf.PdfPCell celOrderCompra = new iTextSharp.text.pdf.PdfPCell();
                    //string PONum = string.Empty;
                    //if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "PONum", 0))
                    //    PONum = (string)ds.Tables["InvcHead"].Rows[0]["PONum"];
                    //Helpers.Plantilla_4.DatosClienteTitulos(ref celOrderCompra, "Orden de Compra: ", PONum);
                    //tableToCel2.AddCell(celOrderCompra);
                    //-------------------------------------------------------------------------------------

                    PdfPCell celVendededor = new PdfPCell();//new PdfPCell(tableVendedor);
                    Helpers.Plantilla_4.DatosClienteTitulos(ref celVendededor, "Vendedor: ", ds.Tables["InvcHead"].Rows[0]["SalesRepName1"].ToString());
                    celVendededor.Colspan = 2;
                    tableToCel2.AddCell(celVendededor);
                    //-------------------------------------------------------------------------------------

                    PdfPCell celCodigoVendededor = new PdfPCell();//new PdfPCell(tableCodigoVendedor);
                    Helpers.Plantilla_4.DatosClienteTitulos(ref celCodigoVendededor, "Código Vendedor: ", ds.Tables["InvcHead"].Rows[0]["SalesRepCode1"].ToString());
                    celCodigoVendededor.Colspan = 2;
                    tableToCel2.AddCell(celCodigoVendededor);
                    //-------------------------------------------------------------------------------------
                    iTextSharp.text.pdf.PdfPCell cel2 = new iTextSharp.text.pdf.PdfPCell(tableToCel2);
                    cel2.Colspan = 1;
                    tableFacturar.AddCell(cel2);


                    /*CELL QR*/
                    Paragraph prgCufe = new Paragraph("CUFE: " + CUFE, Helpers.Fuentes.Plantilla_4_fontTitle2);
                    prgCufe.Alignment = Element.ALIGN_LEFT;

                    iTextSharp.text.Image QRPdf = iTextSharp.text.Image.GetInstance(image, BaseColor.WHITE);
                    QRPdf.ScaleAbsolute(90f, 90f);
                    QRPdf.Alignment = Element.ALIGN_CENTER;
                    //iTextSharp.text.pdf.PdfPCell celImgQR = new iTextSharp.text.pdf.PdfPCell(QRPdf);
                    iTextSharp.text.pdf.PdfPCell celImgQR = new iTextSharp.text.pdf.PdfPCell();
                    celImgQR.AddElement(QRPdf);
                    celImgQR.AddElement(prgCufe);
                    celImgQR.Colspan = 2;
                    celImgQR.Padding = 3;
                    celImgQR.Border = 0;
                    celImgQR.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                    celImgQR.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    tableFacturar.AddCell(celImgQR);
                    //----- Retornando tabla
                    return tableFacturar;
                }
                public static PdfPTable DetalleHead()
                {
                    Helpers.Plantilla_4.DetalleHeadMinimumHeight = 311; // se establece el tamaño de ocupa las lineas
                    Helpers.Plantilla_4.DimensionUnidades = new float[] { 0.178f, 0.5f, 0.5f, 0.5f, 0.328f, 1.5f, 0.585f, 0.4f, 0.5f, 0.28f };
                    //return Helpers.Plantilla_4.DetalleHead();
                    PdfPTable tableTituloUnidades = new PdfPTable(10);

                    tableTituloUnidades.WidthPercentage = 100;
                    tableTituloUnidades.SetWidths(Helpers.Plantilla_4.DimensionUnidades);
                    //---- Agregando los encabezados -----------------------------
                    Helpers.Plantilla_4.DetalleHeadEncabezados(ref tableTituloUnidades, "L", 0.5f, 0);
                    Helpers.Plantilla_4.DetalleHeadEncabezados(ref tableTituloUnidades, "Código", 0.5f, 0);
                    Helpers.Plantilla_4.DetalleHeadEncabezados(ref tableTituloUnidades, "Referencia", 0.5f, 0);
                    Helpers.Plantilla_4.DetalleHeadEncabezados(ref tableTituloUnidades, "Cantidad", 0.5f, 0);
                    Helpers.Plantilla_4.DetalleHeadEncabezados(ref tableTituloUnidades, "Unidad", 0.5f, 0);
                    Helpers.Plantilla_4.DetalleHeadEncabezados(ref tableTituloUnidades, "Descripción", 0.5f, 0);
                    Helpers.Plantilla_4.DetalleHeadEncabezados(ref tableTituloUnidades, "Valor Unitario", 0.5f, 0);
                    Helpers.Plantilla_4.DetalleHeadEncabezados(ref tableTituloUnidades, "Descuento", 0.5f, 0);
                    Helpers.Plantilla_4.DetalleHeadEncabezados(ref tableTituloUnidades, "Valor Total", 0.5f, 0);
                    Helpers.Plantilla_4.DetalleHeadEncabezados(ref tableTituloUnidades, "% IVA", 0.5f, 0.5f);
                    //---- Agregando las celdas fijas --------
                    Helpers.Plantilla_4.AddCeldasCabecera(ref tableTituloUnidades, 0, 0.5f, 0, 0); //Linea
                    Helpers.Plantilla_4.AddCeldasCabecera(ref tableTituloUnidades, 0, 0.5f, 0, 0); //Código
                    Helpers.Plantilla_4.AddCeldasCabecera(ref tableTituloUnidades, 0, 0.5f, 0, 0); //Referencia
                    Helpers.Plantilla_4.AddCeldasCabecera(ref tableTituloUnidades, 0, 0.5f, 0, 0); //Cantidad
                    Helpers.Plantilla_4.AddCeldasCabecera(ref tableTituloUnidades, 0, 0.5f, 0, 0); //Unidad
                    Helpers.Plantilla_4.AddCeldasCabecera(ref tableTituloUnidades, 0, 0.5f, 0, 0); //Descripcion
                    Helpers.Plantilla_4.AddCeldasCabecera(ref tableTituloUnidades, 0, 0.5f, 0, 0); //Valor unitario
                    Helpers.Plantilla_4.AddCeldasCabecera(ref tableTituloUnidades, 0, 0.5f, 0, 0); //Descuento
                    Helpers.Plantilla_4.AddCeldasCabecera(ref tableTituloUnidades, 0, 0.5f, 0, 0); //Valor total
                    Helpers.Plantilla_4.AddCeldasCabecera(ref tableTituloUnidades, 0, 0.5f, 0.5f, 0); // % IVA
                    //--- retorno de la tabla
                    return tableTituloUnidades;
                }
                public static PdfPTable Detalles(DataSet ds, ref string strError)
                {
                    //----- Detalles ----------
                    PdfPTable tableUnidades = new PdfPTable(10);
                    tableUnidades.WidthPercentage = 100;
                    tableUnidades.SetWidths(new float[] { 0.178f, 0.5f, 0.5f, 0.5f, 0.328f, 1.5f, 0.585f, 0.4f, 0.5f, 0.28f });
                    decimal totalDescuento = 0;
                    foreach (DataRow InvoiceLine in ds.Tables["InvcDtl"].Rows)
                    {
                        Helpers.Atmopel.AddUnidadesAtmopel(InvoiceLine, ref tableUnidades, Helpers.Fuentes.Plantilla_4_fontCustom, ds, ref strError);
                        if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcDtl", "DspDocDiscount"))
                            totalDescuento += decimal.Parse((string)InvoiceLine["DspDocDiscount"]);
                    }
                    return tableUnidades;
                }
                public static PdfPTable PiePagina(DataSet ds)
                {
                    string InvcHeadNumber04 = string.Empty;
                    string InvcHeadCharacter04 = string.Empty;
                    string InvcHeadInvoiceComment = string.Empty;
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "Character04", 0))
                        InvcHeadCharacter04 = ds.Tables["InvcHead"].Rows[0]["Character04"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "Number04", 0))
                        InvcHeadNumber04 = ds.Tables["InvcHead"].Rows[0]["Number04"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "InvoiceComment", 0))
                        InvcHeadInvoiceComment = ds.Tables["InvcHead"].Rows[0]["InvoiceComment"].ToString();

                    Helpers.Plantilla_4.PiePaginaDatos datos = new Helpers.Plantilla_4.PiePaginaDatos();
                    PdfPCell celArticulosTotales = new PdfPCell();
                    celArticulosTotales.Border = 0;
                    //----- realizando consulta --------
                    IEnumerable<Helpers.Plantilla_4.InvcDtl> ListaInvcDtl = null;
                    if (ds.Tables.Contains("InvcDtl"))
                    {
                        if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcDtl", "XPartNum"))
                        {
                            ListaInvcDtl = from row in ds.Tables["InvcDtl"].AsEnumerable()
                                           select new Helpers.Plantilla_4.InvcDtl
                                           {
                                               Company = row["Company"].ToString(),
                                               InvoiceNum = Convert.ToInt32(row["InvoiceNum"]),
                                               InvoiceLine = Convert.ToInt32(row["InvoiceLine"]),
                                               PartNum = row["PartNum"].ToString(),
                                               XPartNum = row["XPartNum"].ToString(),
                                               SellingShipQty = Convert.ToDecimal(row["SellingShipQty"])
                                           };
                        }
                        else
                        {
                            ListaInvcDtl = from row in ds.Tables["InvcDtl"].AsEnumerable()
                                           select new Helpers.Plantilla_4.InvcDtl
                                           {
                                               Company = row["Company"].ToString(),
                                               InvoiceNum = Convert.ToInt32(row["InvoiceNum"]),
                                               InvoiceLine = Convert.ToInt32(row["InvoiceLine"]),
                                               PartNum = row["PartNum"].ToString(),
                                               XPartNum = string.Empty,
                                               SellingShipQty = Convert.ToDecimal(row["SellingShipQty"])
                                           };
                        }

                    }
                    //---- Sumatoria de las cantidades --------
                    decimal Sumatoria = 0;
                    if (ListaInvcDtl != null && ListaInvcDtl.Count() > 0)
                    {
                        var suma = ListaInvcDtl.Sum(row => row.SellingShipQty);
                        Sumatoria = suma;
                    }
                    Helpers.Plantilla_4.DatosClienteTitulos(ref celArticulosTotales, "Número total de artículos entregados: ",decimal.Parse(InvcHeadNumber04).ToString("N2"));
                    datos.CelTotalArticulos = celArticulosTotales;
                    //---- Asignacion de clase de InvcMisc -----
                    decimal Seguro = 0;
                    IEnumerable<Helpers.Plantilla_4.InvcMisc> ListaInvcMisc = null;
                    if (ds.Tables.Contains("InvcMisc"))
                    {
                        ListaInvcMisc = from row in ds.Tables["InvcMisc"].AsEnumerable()
                                        select new Helpers.Plantilla_4.InvcMisc
                                        {
                                            Company = row["Company"].ToString(),
                                            InvoiceNum = Convert.ToInt32(row["InvoiceNum"]),
                                            InvoiceLine = Convert.ToInt32(row["InvoiceLine"]),
                                            MiscCode = row["MiscCode"].ToString(),
                                            Description = row["Description"].ToString(),
                                            MiscAmt = Convert.ToDecimal(row["MiscAmt"]),
                                            DocMiscAmt = Convert.ToDecimal(row["DocMiscAmt"])
                                        };
                    }
                    if (ListaInvcMisc != null && ListaInvcMisc.Count() > 0)
                    {
                        Seguro = ListaInvcMisc.Sum(row => row.DocMiscAmt);
                    }

                    datos.Gravado = true;
                    datos.Excluido = true;
                    datos.Seguro = true;
                    datos.Notas = "\n\n\n" + InvcHeadCharacter04;
                    datos.ObservacionesCita = InvcHeadInvoiceComment + "\n\n" + InvcHeadCharacter04;
                    datos.LeyendaCopia = false;
                    datos.NITReferenia = false;
                    datos.BarCode01 = false;
                    datos.BarCode02 = false;
                    datos.SeguroValor = Seguro;
                    //return Helpers.Plantilla_4.PiePagina(ds, datos);

                    //----- Variables
                    string InvcHeadNumber01 = string.Empty;
                    string InvcHeadNumber02 = string.Empty;
                    string InvcHeadNumber03 = string.Empty;
                    string InvcHeadNumber05 = string.Empty;
                    string InvcHeadNumber06 = "0";
                    //------ Asignación de variables ----
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "Number01", 0))
                        InvcHeadNumber01 = decimal.Parse((string)ds.Tables["InvcHead"].Rows[0]["Number01"]).ToString("N0");
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "Number02", 0))
                        InvcHeadNumber02 = decimal.Parse((string)ds.Tables["InvcHead"].Rows[0]["Number02"]).ToString("N0");
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "Number03", 0))
                        InvcHeadNumber03 = decimal.Parse((string)ds.Tables["InvcHead"].Rows[0]["Number03"]).ToString("N0");
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "Number05", 0))
                        InvcHeadNumber05 = decimal.Parse((string)ds.Tables["InvcHead"].Rows[0]["Number05"]).ToString("N0");
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "Number06", 0))
                        InvcHeadNumber06 = decimal.Parse((string)ds.Tables["InvcHead"].Rows[0]["Number06"]).ToString("N0");
                    //--------------

                    decimal totalDescuento = 0;
                    foreach (DataRow InvoiceLine in ds.Tables["InvcDtl"].Rows)
                    {
                        if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcDtl", "DspDocDiscount"))
                            totalDescuento += decimal.Parse((string)InvoiceLine["DspDocDiscount"]);
                    }

                    PdfPTable tableObsTotales = new PdfPTable(3);
                    tableObsTotales.WidthPercentage = 100;
                    float[] dimTableObsTotales = new float[3];
                    dimTableObsTotales[0] = 2.0f;
                    dimTableObsTotales[1] = 0.8f;
                    dimTableObsTotales[2] = 0.85f;
                    tableObsTotales.SetWidths(dimTableObsTotales);
                    //---------------------------------------------------------------------
                    PdfPTable tableObsercaciones = new PdfPTable(1);

                    if (datos.CelTotalArticulos != null)
                    {
                        tableObsercaciones.AddCell(datos.CelTotalArticulos);
                    }

                    PdfPCell celTextObservaciones = new PdfPCell(new Phrase("OBSERVACIONES\n\n\n", Helpers.Fuentes.Plantilla_4_fontTitleFactura));
                    celTextObservaciones.Border = 0;
                    tableObsercaciones.AddCell(celTextObservaciones);

                    PdfPCell celTextPedirCita = new PdfPCell(new Phrase(datos.ObservacionesCita, Helpers.Fuentes.Plantilla_4_fontTitle2));
                    celTextPedirCita.Border = 0;
                    tableObsercaciones.AddCell(celTextPedirCita);

                    PdfPCell celObservaciones = new PdfPCell(tableObsercaciones);
                    tableObsTotales.AddCell(celObservaciones);
                    //----------------------------------------------------------------------
                    PdfPCell celFimarSello = new PdfPCell(new Phrase("\n\n\n\n_______________________________\nFirma y Sello", Helpers.Fuentes.Plantilla_4_fontTitle2));
                    celFimarSello.HorizontalAlignment = Element.ALIGN_CENTER;
                    celFimarSello.VerticalAlignment = Element.ALIGN_BOTTOM;
                    tableObsTotales.AddCell(celFimarSello);
                    //----------------------------------------------------------------------
                    PdfPTable tableTotales = new PdfPTable(2);
                    float[] dimTableTotales = new float[2];
                    dimTableTotales[0] = 1.1f;
                    dimTableTotales[1] = 1.7f;
                    tableTotales.SetWidths(dimTableTotales);

                    Helpers.Plantilla_4.PiePaginaTotales(ref tableTotales, "Subtotal", decimal.Parse(InvcHeadNumber01).ToString("N0"));
                    Helpers.Plantilla_4.PiePaginaTotales(ref tableTotales, "Descuento", InvcHeadNumber05);
                    Helpers.Plantilla_4.PiePaginaTotales(ref tableTotales, "Valor Fletes", decimal.Parse(InvcHeadNumber06).ToString("N0"));
                    if (datos.Seguro)
                        Helpers.Plantilla_4.PiePaginaTotales(ref tableTotales, "Seguro", decimal.Parse(InvcHeadNumber03).ToString("N0"));
                    Helpers.Plantilla_4.PiePaginaTotales(ref tableTotales, "Total Bruto", decimal.Parse((string)ds.Tables["InvcHead"].Rows[0]["DspDocSubTotal"]).ToString("N0"));
                    var Iva = Helpers.Compartido.GetValImpuestoByID("01", ds);
                    Helpers.Plantilla_4.PiePaginaTotales(ref tableTotales, "Valor IVA", Iva.ToString("N0"));
                    //PiePaginaTotales(ref tableTotales, "Descuento", totalDescuento.ToString("N2"));

                    //Helpers.Plantilla_4.PiePaginaTotales(ref tableTotales, "Rete IVA 15%", "xxxxx");
                    if (datos.Gravado)
                        Helpers.Plantilla_4.PiePaginaTotales(ref tableTotales, "Gravado", InvcHeadNumber01);
                    if (datos.Excluido)
                        Helpers.Plantilla_4.PiePaginaTotales(ref tableTotales, "Excluido", InvcHeadNumber02);
                    

                    PdfPCell celTotales = new PdfPCell(tableTotales);
                    celTotales.Padding = 2;
                    tableObsTotales.AddCell(celTotales);

                    PdfPCell celTotalLetras = new PdfPCell(new Phrase(
                        $"Son: **{Helpers.Compartido.Nroenletras((string)ds.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"])}**", Helpers.Fuentes.Plantilla_4_fontTitle2));
                    celTotalLetras.Colspan = 2;
                    tableObsTotales.AddCell(celTotalLetras);

                    PdfPTable tableTotalFactura = new PdfPTable(2);
                    tableTotalFactura.SetWidths(dimTableTotales);

                    PdfPCell celTextTotal = new PdfPCell(new Phrase("Total Nota Crédito", Helpers.Fuentes.Plantilla_4_fontTitleFactura));
                    celTextTotal.BackgroundColor = BaseColor.LIGHT_GRAY;
                    celTextTotal.Border = 0;
                    tableTotalFactura.AddCell(celTextTotal);

                    PdfPCell celValTotal = new PdfPCell(new Phrase(
                        decimal.Parse((string)ds.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"]).ToString("N0"),
                        Helpers.Fuentes.Plantilla_4_fontTitleFactura));
                    celValTotal.HorizontalAlignment = Element.ALIGN_RIGHT;
                    celValTotal.BackgroundColor = BaseColor.LIGHT_GRAY;
                    celValTotal.Border = 0;
                    tableTotalFactura.AddCell(celValTotal);

                    PdfPCell celTotal = new PdfPCell(tableTotalFactura);
                    tableObsTotales.AddCell(celTotal);
                    //---------------------------------------------------------------------------------------------

                    PdfPTable tableResolucion = new PdfPTable(1);
                    tableResolucion.WidthPercentage = 100;

                    //Paragraph prgResolucion = new Paragraph(datos.Notas);

                    PdfPCell celResolucion = new PdfPCell(new Phrase(datos.Notas, Helpers.Fuentes.Plantilla_4_fontTitle2));
                    celResolucion.HorizontalAlignment = Element.ALIGN_CENTER;
                    celResolucion.Border = 0;
                    tableResolucion.AddCell(celResolucion);
                    //------------------------------------------------------------------------------------------------------------
                    if (datos.LeyendaCopia)
                    {
                        PdfPTable tableTextCopia = new PdfPTable(3);
                        tableTextCopia.WidthPercentage = 100;

                        PdfPCell celTextNumRot = new PdfPCell(new Phrase("NumRot 2.0 Version:Factura30 / FAcx20170213.ps",
                            FontFactory.GetFont(FontFactory.HELVETICA, 5, iTextSharp.text.Font.NORMAL)));
                        celTextNumRot.PaddingTop = 4;
                        celTextNumRot.BorderWidthRight = PdfPCell.NO_BORDER;
                        celTextNumRot.BorderWidthLeft = PdfPCell.NO_BORDER;
                        tableTextCopia.AddCell(celTextNumRot);

                        PdfPCell celTextCopia = new PdfPCell(new Phrase("- COPIA -", Helpers.Fuentes.Plantilla_4_fontTitleFactura));
                        celTextCopia.PaddingTop = 4;
                        celTextCopia.BorderWidthRight = PdfPCell.NO_BORDER;
                        celTextCopia.BorderWidthLeft = PdfPCell.NO_BORDER;
                        celTextCopia.HorizontalAlignment = Element.ALIGN_CENTER;
                        tableTextCopia.AddCell(celTextCopia);

                        PdfPCell celTextPagina = new PdfPCell(new Phrase(" "));
                        celTextPagina.BorderWidthRight = PdfPCell.NO_BORDER;
                        celTextPagina.BorderWidthLeft = PdfPCell.NO_BORDER;
                        tableTextCopia.AddCell(celTextPagina);

                        //----- celda de agregado ---------------
                        PdfPCell celCopia = new PdfPCell();
                        celCopia.AddElement(tableTextCopia);
                        celCopia.Border = 0;
                        tableResolucion.AddCell(celCopia);
                    }
                    //---------------------------------------------------------------------------------------
                    PdfPTable tableTextNitReferencia = new PdfPTable(2);
                    if (datos.NITReferenia)
                    {
                        float[] dimTableTextNitReferencia = new float[2];
                        dimTableTextNitReferencia[0] = 1.068f;
                        dimTableTextNitReferencia[1] = 1.15f;
                        tableTextNitReferencia.SetWidths(dimTableTextNitReferencia);
                        tableTextNitReferencia.WidthPercentage = 100;

                        PdfPCell celTextNitReferenciae = new PdfPCell(new Phrase(" ", Helpers.Fuentes.Plantilla_4_fontTitle));
                        celTextNitReferenciae.HorizontalAlignment = Element.ALIGN_RIGHT;
                        celTextNitReferenciae.Colspan = 1;
                        celTextNitReferenciae.Border = PdfPCell.NO_BORDER;
                        tableTextNitReferencia.AddCell(celTextNitReferenciae);

                        PdfPCell celTextNitReferencia = new PdfPCell(new Phrase(
                            $"ABRASIVOS DE COLOMBIA S.A NIT.{(string)ds.Tables["Company"].Rows[0]["StateTaxID"]}" +
                            $"   Referencia 1 No. xxxxx - Referencia 2 No. {(string)ds.Tables["Customer"].Rows[0]["ResaleID"]}",
                            FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 6f, iTextSharp.text.Font.NORMAL)));
                        celTextNitReferencia.HorizontalAlignment = Element.ALIGN_LEFT;
                        celTextNitReferencia.Colspan = 1;
                        celTextNitReferencia.Border = PdfPCell.NO_BORDER;
                        celTextNitReferencia.BorderWidthTop = 0.5f;
                        tableTextNitReferencia.AddCell(celTextNitReferencia);
                    }
                    //---------------------------------------------------------------------------------------
                    PdfPTable tableBC1 = new PdfPTable(3);
                    tableBC1.HorizontalAlignment = Element.ALIGN_LEFT;
                    float[] dimTableBC1 = new float[3];
                    dimTableBC1[0] = 1.8f;
                    dimTableBC1[1] = 1.0f;
                    dimTableBC1[2] = 0.95f;
                    tableBC1.SetWidths(dimTableBC1);
                    tableBC1.WidthPercentage = 100;

                    PdfPCell celBC1 = new PdfPCell(new Phrase("Bar Code"));
                    celBC1.Border = PdfPCell.NO_BORDER;
                    tableBC1.AddCell(celBC1);

                    PdfPTable tableDatosBC1 = new PdfPTable(2);
                    float[] dimTableDatosBC1 = new float[2];
                    dimTableDatosBC1[0] = 1.3f;
                    dimTableDatosBC1[1] = 0.7f;
                    tableDatosBC1.SetWidths(dimTableDatosBC1);
                    //----------------------------------------------------------------------------------
                    PdfPTable tableDatosComprador = new PdfPTable(2);
                    float[] dimTableDatosComprador = new float[2];
                    dimTableDatosComprador[0] = 0.8f;
                    dimTableDatosComprador[1] = 1.5f;
                    tableDatosComprador.SetWidths(dimTableDatosComprador);

                    PdfPCell celprgComprador = new PdfPCell();
                    celprgComprador.AddElement(new Paragraph("COMPRADOR:", Helpers.Fuentes.Plantilla_4_fontTitle2));
                    celprgComprador.AddElement(new Paragraph((string)ds.Tables["InvcHead"].Rows[0]["CustomerName"]
                        , FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 5, iTextSharp.text.Font.NORMAL)));
                    celprgComprador.Border = PdfPCell.NO_BORDER;
                    celprgComprador.BorderWidthLeft = 0.5f;
                    celprgComprador.Colspan = 2;
                    //celprgComprador.Padding = 3;
                    tableDatosComprador.AddCell(celprgComprador);

                    PdfPCell celprgNit = new PdfPCell();
                    celprgNit.AddElement(new Paragraph("NIT./CC. ", Helpers.Fuentes.Plantilla_4_fontTitle2));
                    celprgNit.Border = PdfPCell.NO_BORDER;
                    celprgNit.BorderWidthLeft = 0.5f;
                    celprgNit.BorderWidthBottom = 0.5f;
                    celprgNit.Colspan = 1;
                    //celprgNit.Padding = 3;
                    tableDatosComprador.AddCell(celprgNit);

                    PdfPCell celprgValNit = new PdfPCell();
                    celprgValNit.AddElement(new Phrase((string)ds.Tables["Customer"].Rows[0]["ResaleID"], Helpers.Fuentes.Plantilla_4_fontTitleFactura));
                    celprgValNit.Border = PdfPCell.NO_BORDER;
                    celprgValNit.BorderWidthBottom = 0.5f;
                    celprgValNit.Colspan = 1;
                    celprgValNit.Padding = 3;
                    tableDatosComprador.AddCell(celprgValNit);

                    PdfPCell celEspacionDatosComprador = new PdfPCell(new Phrase(" "));
                    celEspacionDatosComprador.Colspan = 2;
                    celprgValNit.BorderWidthBottom = 0.5f;
                    celEspacionDatosComprador.Border = PdfPCell.NO_BORDER;
                    tableDatosComprador.AddCell(celEspacionDatosComprador);

                    PdfPCell celDatosComprador = new PdfPCell(tableDatosComprador);
                    celDatosComprador.BorderWidthBottom = PdfPCell.NO_BORDER;
                    celDatosComprador.BorderWidthRight = PdfPCell.NO_BORDER;
                    celDatosComprador.BorderWidthLeft = PdfPCell.NO_BORDER;
                    tableDatosBC1.AddCell(celDatosComprador);
                    //----------------------------------------------------------------------------------
                    PdfPTable tableTotalEfectivo = new PdfPTable(1);
                    Paragraph prgTotalEfectivo = new Paragraph("Total Efectivo\n",
                        FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL));

                    PdfDiv divTotalEfectivo = new iTextSharp.text.pdf.PdfDiv();
                    divTotalEfectivo.Float = iTextSharp.text.pdf.PdfDiv.FloatType.LEFT;
                    divTotalEfectivo.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                    divTotalEfectivo.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                    divTotalEfectivo.Height = 21.2f;
                    divTotalEfectivo.Width = 49f;
                    divTotalEfectivo.PaddingTop = 5;
                    divTotalEfectivo.BackgroundColor = BaseColor.LIGHT_GRAY;

                    PdfPCell celTextTotalEfectivo = new PdfPCell();
                    celTextTotalEfectivo.AddElement(prgTotalEfectivo);
                    celTextTotalEfectivo.BorderWidthBottom = PdfPCell.NO_BORDER;
                    celTextTotalEfectivo.VerticalAlignment = Element.ALIGN_LEFT;
                    celTextTotalEfectivo.HorizontalAlignment = Element.ALIGN_TOP;
                    tableTotalEfectivo.AddCell(celTextTotalEfectivo);

                    PdfPCell celDivTotalEfectivo = new PdfPCell();
                    celDivTotalEfectivo.AddElement(divTotalEfectivo);
                    celDivTotalEfectivo.BorderWidthTop = PdfPCell.NO_BORDER;
                    tableTotalEfectivo.AddCell(celDivTotalEfectivo);

                    PdfPCell celEspacioTotalEfectivo = new PdfPCell(new Phrase(" "));
                    celEspacioTotalEfectivo.Border = PdfPCell.NO_BORDER;
                    tableTotalEfectivo.AddCell(celEspacioTotalEfectivo);

                    PdfPCell celTotalEfectivo = new PdfPCell(tableTotalEfectivo);
                    celTotalEfectivo.BorderWidthBottom = PdfPCell.NO_BORDER;
                    celTotalEfectivo.BorderWidthLeft = PdfPCell.NO_BORDER;
                    celTotalEfectivo.BorderWidthRight = PdfPCell.NO_BORDER;
                    tableDatosBC1.AddCell(celTotalEfectivo);
                    //----------------------------------------------------------------------------------
                    #region DatosConsignarEn
                    PdfPTable tableDatosConsignacion = new PdfPTable(2);
                    tableDatosConsignacion.HorizontalAlignment = Element.ALIGN_RIGHT;
                    tableDatosConsignacion.WidthPercentage = 100;
                    float[] dimTableDatosConsignacion = new float[2];
                    dimTableDatosConsignacion[0] = 1.0f;
                    dimTableDatosConsignacion[1] = 1.5f;
                    tableDatosConsignacion.SetWidths(dimTableDatosConsignacion);

                    PdfPCell celConsignarEn1 = new PdfPCell(new Phrase(
                        "Favor Consignar en:", FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 6.7f, iTextSharp.text.Font.NORMAL)));
                    celConsignarEn1.VerticalAlignment = Element.ALIGN_LEFT;
                    celConsignarEn1.HorizontalAlignment = Element.ALIGN_TOP;
                    celConsignarEn1.Border = PdfPCell.NO_BORDER;
                    celConsignarEn1.Colspan = 2;
                    tableDatosConsignacion.AddCell(celConsignarEn1);

                    PdfPCell celTextBancolombia = new PdfPCell(new Phrase("Bancolombia", FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL)));
                    celTextBancolombia.VerticalAlignment = Element.ALIGN_LEFT;
                    celTextBancolombia.HorizontalAlignment = Element.ALIGN_TOP;
                    celTextBancolombia.Border = PdfPCell.NO_BORDER;
                    celTextBancolombia.Padding = 2;
                    celTextBancolombia.Colspan = 1;
                    tableDatosConsignacion.AddCell(celTextBancolombia);

                    PdfPCell celCtaBancolombia = new PdfPCell(new Phrase("Cta. Cte. 009991132702", FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL)));
                    celCtaBancolombia.VerticalAlignment = Element.ALIGN_LEFT;
                    celCtaBancolombia.Padding = 2;
                    celCtaBancolombia.HorizontalAlignment = Element.ALIGN_TOP;
                    celCtaBancolombia.Border = PdfPCell.NO_BORDER;
                    celCtaBancolombia.Colspan = 1;
                    tableDatosConsignacion.AddCell(celCtaBancolombia);

                    PdfPCell celTextBancoBogota = new PdfPCell(new Phrase("Banco de Bogota", FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL)));
                    celTextBancoBogota.VerticalAlignment = Element.ALIGN_LEFT;
                    celTextBancoBogota.HorizontalAlignment = Element.ALIGN_TOP;
                    celTextBancoBogota.Border = PdfPCell.NO_BORDER;
                    celTextBancoBogota.Padding = 2;
                    celTextBancoBogota.Colspan = 1;
                    tableDatosConsignacion.AddCell(celTextBancoBogota);

                    PdfPCell celCtaBancoBogota = new PdfPCell(new Phrase("Cta. Cte. 349019737", FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL)));
                    celCtaBancoBogota.VerticalAlignment = Element.ALIGN_LEFT;
                    celCtaBancoBogota.HorizontalAlignment = Element.ALIGN_TOP;
                    celCtaBancoBogota.Border = PdfPCell.NO_BORDER;
                    celCtaBancoBogota.Padding = 2;
                    celCtaBancoBogota.Colspan = 1;
                    tableDatosConsignacion.AddCell(celCtaBancoBogota);

                    PdfPCell celTextBancoBBVA = new PdfPCell(new Phrase("Banco BBVA", FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL)));
                    celTextBancoBBVA.VerticalAlignment = Element.ALIGN_LEFT;
                    celTextBancoBBVA.HorizontalAlignment = Element.ALIGN_TOP;
                    celTextBancoBBVA.Border = PdfPCell.NO_BORDER;
                    celTextBancoBBVA.Padding = 2;
                    celTextBancoBBVA.Colspan = 1;
                    tableDatosConsignacion.AddCell(celTextBancoBBVA);

                    PdfPCell celCtaBancoBBVA = new PdfPCell(new Phrase("Cta. Cte. 498000488", FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL)));
                    celCtaBancoBBVA.VerticalAlignment = Element.ALIGN_LEFT;
                    celCtaBancoBBVA.HorizontalAlignment = Element.ALIGN_TOP;
                    celCtaBancoBBVA.Border = PdfPCell.NO_BORDER;
                    celCtaBancoBBVA.Padding = 2;
                    celCtaBancoBBVA.Colspan = 1;
                    tableDatosConsignacion.AddCell(celCtaBancoBBVA);

                    PdfPCell celTextBancoOccidente = new PdfPCell(new Phrase("Banco de Occidente", FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL)));
                    celTextBancoOccidente.VerticalAlignment = Element.ALIGN_LEFT;
                    celTextBancoOccidente.HorizontalAlignment = Element.ALIGN_TOP;
                    celTextBancoOccidente.Border = PdfPCell.NO_BORDER;
                    celTextBancoOccidente.Padding = 2;
                    celTextBancoOccidente.Colspan = 1;
                    tableDatosConsignacion.AddCell(celTextBancoOccidente);

                    PdfPCell celCtaBancoOccidente = new PdfPCell(new Phrase("Cta. Cte. 405059098", FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL)));
                    celCtaBancoOccidente.VerticalAlignment = Element.ALIGN_LEFT;
                    celCtaBancoOccidente.HorizontalAlignment = Element.ALIGN_TOP;
                    celCtaBancoOccidente.Border = PdfPCell.NO_BORDER;
                    celCtaBancoOccidente.Padding = 2;
                    celCtaBancoOccidente.Colspan = 1;
                    tableDatosConsignacion.AddCell(celCtaBancoOccidente);

                    PdfPCell celTextBancoHBank = new PdfPCell(new Phrase("Helm Bank", FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL)));
                    celTextBancoHBank.VerticalAlignment = Element.ALIGN_LEFT;
                    celTextBancoHBank.HorizontalAlignment = Element.ALIGN_TOP;
                    celTextBancoHBank.Border = PdfPCell.NO_BORDER;
                    celTextBancoHBank.Padding = 2;
                    celTextBancoHBank.Colspan = 1;
                    tableDatosConsignacion.AddCell(celTextBancoHBank);

                    PdfPCell celCtaBancoHBank = new PdfPCell(new Phrase("Cta. Cte. 101011062", FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL)));
                    celCtaBancoHBank.VerticalAlignment = Element.ALIGN_LEFT;
                    celCtaBancoHBank.HorizontalAlignment = Element.ALIGN_TOP;
                    celCtaBancoHBank.Border = PdfPCell.NO_BORDER;
                    celCtaBancoHBank.Padding = 2;
                    celCtaBancoHBank.Colspan = 1;
                    tableDatosConsignacion.AddCell(celCtaBancoHBank);
                    #endregion
                    //----------------------------------------------------------------------------------
                    PdfPCell celDatosBC1 = new PdfPCell(tableDatosBC1);
                    celDatosBC1.Border = PdfPCell.NO_BORDER;
                    tableBC1.AddCell(celDatosBC1);

                    PdfPCell celConsignarEn = new PdfPCell(tableDatosConsignacion);
                    celConsignarEn.Border = PdfPCell.NO_BORDER;
                    celConsignarEn.BorderWidthTop = 0.5f;
                    tableBC1.AddCell(celConsignarEn);
                    //------------------------SEGUNTA TABLA DE BC---------------------------------------------------------
                    //PdfDiv divTextBancoEfectivo = new PdfDiv();
                    //divTextBancoEfectivo.BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.DOTTED;
                    //divTextBancoEfectivo.Width = 570;

                    PdfPTable tableTextBancoEfectivo = new PdfPTable(3);
                    tableTextBancoEfectivo.WidthPercentage = 100;

                    PdfPCell celTextBancoEfectivoEspacio1 = new PdfPCell(new Phrase(" "));
                    celTextBancoEfectivoEspacio1.PaddingTop = 4;
                    celTextBancoEfectivoEspacio1.BorderWidthRight = PdfPCell.NO_BORDER;
                    celTextBancoEfectivoEspacio1.BorderWidthLeft = PdfPCell.NO_BORDER;
                    celTextBancoEfectivoEspacio1.BorderWidthTop = PdfPCell.NO_BORDER;
                    tableTextBancoEfectivo.AddCell(celTextBancoEfectivoEspacio1);

                    PdfPCell celTextBancoEfectivoEspacio2 = new PdfPCell(new Phrase(" "));
                    celTextBancoEfectivoEspacio2.PaddingTop = 4;
                    celTextBancoEfectivoEspacio2.BorderWidthRight = PdfPCell.NO_BORDER;
                    celTextBancoEfectivoEspacio2.BorderWidthLeft = PdfPCell.NO_BORDER;
                    celTextBancoEfectivoEspacio2.HorizontalAlignment = Element.ALIGN_CENTER;
                    celTextBancoEfectivoEspacio2.BorderWidthTop = PdfPCell.NO_BORDER;
                    tableTextBancoEfectivo.AddCell(celTextBancoEfectivoEspacio2);

                    PdfPCell celTextBancoEfectivoEspacio = new PdfPCell(new Phrase("- BANCO (EFECTIVO) -", Helpers.Fuentes.Plantilla_4_fontTitleFactura));
                    celTextBancoEfectivoEspacio.BorderWidthRight = PdfPCell.NO_BORDER;
                    celTextBancoEfectivoEspacio.BorderWidthLeft = PdfPCell.NO_BORDER;
                    celTextBancoEfectivoEspacio.BorderWidthTop = PdfPCell.NO_BORDER;
                    tableTextBancoEfectivo.AddCell(celTextBancoEfectivoEspacio);
                    //---
                    PdfPCell celBancoEfectivo = new PdfPCell();
                    celBancoEfectivo.AddElement(tableTextBancoEfectivo);
                    celBancoEfectivo.Colspan = 3;
                    celBancoEfectivo.Padding = 0;
                    celBancoEfectivo.Border = 0;
                    tableBC1.AddCell(celBancoEfectivo);


                    //divTextBancoEfectivo.AddElement(tableTextBancoEfectivo);
                    //---------------------------------------------------------------------------------------------------
                    PdfPTable tableBC2 = new PdfPTable(3);
                    tableBC2.HorizontalAlignment = Element.ALIGN_LEFT;
                    tableBC2.SetWidths(dimTableBC1);
                    tableBC2.WidthPercentage = 100;

                    PdfPCell celBC2 = new PdfPCell(new Phrase("Bar Code 2"));
                    celBC2.Border = PdfPCell.NO_BORDER;
                    tableBC2.AddCell(celBC2);

                    PdfPTable tableDatosBC2 = new PdfPTable(2);
                    tableDatosBC2.SetWidths(dimTableDatosBC1);
                    //----------------------------------------------------------------------------------
                    PdfPTable tableDatosComprador2 = new PdfPTable(2);
                    tableDatosComprador2.SetWidths(dimTableDatosComprador);

                    tableDatosComprador2.AddCell(celprgComprador);

                    PdfPCell celprgNit2 = new PdfPCell();
                    celprgNit2.AddElement(new Paragraph("NIT./CC. ", Helpers.Fuentes.Plantilla_4_fontTitle2));
                    celprgNit2.Border = PdfPCell.NO_BORDER;
                    celprgNit2.BorderWidthLeft = 0.5f;
                    celprgNit2.BorderWidthBottom = 0.5f;
                    celprgNit2.Colspan = 1;
                    //celprgNit.Padding = 3;
                    tableDatosComprador2.AddCell(celprgNit2);

                    PdfPCell celprgValNit2 = new PdfPCell();
                    celprgValNit2.AddElement(new Phrase((string)ds.Tables["Customer"].Rows[0]["ResaleID"], Helpers.Fuentes.Plantilla_4_fontTitleFactura));
                    celprgValNit2.Border = PdfPCell.NO_BORDER;
                    celprgValNit2.BorderWidthBottom = 0.5f;
                    celprgValNit2.Colspan = 1;
                    celprgValNit2.Padding = 3;
                    tableDatosComprador2.AddCell(celprgValNit2);


                    PdfPCell celDatosComprador2 = new PdfPCell(tableDatosComprador2);
                    celDatosComprador2.BorderWidthBottom = PdfPCell.NO_BORDER;
                    celDatosComprador2.BorderWidthRight = PdfPCell.NO_BORDER;
                    celDatosComprador2.BorderWidthLeft = PdfPCell.NO_BORDER;
                    tableDatosBC2.AddCell(celDatosComprador2);
                    //----------------------------------------------------------------------------------
                    PdfPTable tableTotalCheque = new PdfPTable(1);

                    PdfPCell celTextTotalCheques = new PdfPCell();
                    celTextTotalCheques.AddElement(new Paragraph("Total Cheques\n",
                        FontFactory.GetFont(FontFactory.HELVETICA, 6.7f, iTextSharp.text.Font.NORMAL)));
                    celTextTotalCheques.BorderWidthBottom = PdfPCell.NO_BORDER;
                    celTextTotalCheques.VerticalAlignment = Element.ALIGN_LEFT;
                    celTextTotalCheques.HorizontalAlignment = Element.ALIGN_TOP;
                    tableTotalCheque.AddCell(celTextTotalCheques);

                    PdfPCell celDivTotalCheques = new PdfPCell();
                    celDivTotalCheques.AddElement(divTotalEfectivo);
                    celDivTotalCheques.BorderWidthTop = PdfPCell.NO_BORDER;
                    tableTotalCheque.AddCell(celDivTotalCheques);

                    PdfPCell celTotalCheques = new PdfPCell(tableTotalCheque);
                    celTotalCheques.BorderWidthBottom = PdfPCell.NO_BORDER;
                    celTotalCheques.BorderWidthLeft = PdfPCell.NO_BORDER;
                    celTotalCheques.BorderWidthRight = PdfPCell.NO_BORDER;
                    tableDatosBC2.AddCell(celTotalCheques);
                    //----------------------------------------------------------------------------------
                    PdfPTable tableBancoValores = new PdfPTable(3);


                    iTextSharp.text.Font fontCheque = FontFactory.GetFont(FontFactory.HELVETICA, 6.8f, iTextSharp.text.Font.NORMAL);

                    PdfPCell celTextBancoValoresBanco = new PdfPCell(new Phrase("Banco", fontCheque));
                    celTextBancoValoresBanco.Padding = 3;
                    celTextBancoValoresBanco.HorizontalAlignment = Element.ALIGN_CENTER;
                    //celEspacioTotalCheques.Border = PdfPCell.NO_BORDER;
                    tableBancoValores.AddCell(celTextBancoValoresBanco);

                    PdfPCell celTextBancoValoresNoCheque = new PdfPCell(new Phrase("No.Cheque", fontCheque));
                    celTextBancoValoresNoCheque.Padding = 3;
                    celTextBancoValoresNoCheque.HorizontalAlignment = Element.ALIGN_CENTER;
                    tableBancoValores.AddCell(celTextBancoValoresNoCheque);

                    PdfPCell celTextBancoValoresValor = new PdfPCell(new Phrase("Valor", fontCheque));
                    celTextBancoValoresValor.Padding = 3;
                    celTextBancoValoresValor.HorizontalAlignment = Element.ALIGN_CENTER;
                    tableBancoValores.AddCell(celTextBancoValoresValor);

                    for (int i = 0; i < 2; i++)
                    {
                        PdfPCell celBancoValoresBanco = new PdfPCell(new Phrase(" ", Helpers.Fuentes.Plantilla_4_fontTitle));
                        celBancoValoresBanco.Padding = 2.3f;
                        tableBancoValores.AddCell(celBancoValoresBanco);

                        PdfPCell celBancoValoresNoCheque = new PdfPCell(new Phrase(" ", Helpers.Fuentes.Plantilla_4_fontTitle));
                        celBancoValoresNoCheque.Padding = 2.3f;
                        tableBancoValores.AddCell(celBancoValoresNoCheque);

                        PdfPCell celBancoValoresValor = new PdfPCell(new Phrase(" ", Helpers.Fuentes.Plantilla_4_fontTitle));
                        celBancoValoresValor.Padding = 2.3f;
                        tableBancoValores.AddCell(celBancoValoresValor);
                    }

                    PdfPCell celBancoValores = new PdfPCell(tableBancoValores);
                    celBancoValores.Colspan = 2;
                    tableDatosBC2.AddCell(celBancoValores);
                    //----------------------------------------------------------------------------------

                    PdfPCell celDatosBC2 = new PdfPCell(tableDatosBC2);
                    celDatosBC2.Border = PdfPCell.NO_BORDER;
                    tableBC2.AddCell(celDatosBC2);

                    tableBC2.AddCell(celConsignarEn);
                    //---------------------------------------------------------------------------------
                    //PdfDiv divTextBancoCheque = new PdfDiv();
                    //divTextBancoCheque.BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.DOTTED;
                    //divTextBancoCheque.Width = 570;

                    PdfPTable tableTextBancoCheque = new PdfPTable(3);
                    tableTextBancoCheque.WidthPercentage = 100;

                    PdfPCell celTextBancoChequeEspacio1 = new PdfPCell(new Phrase(" "));
                    //celTextNumRot.Border = 0;
                    celTextBancoChequeEspacio1.PaddingTop = 4;
                    celTextBancoChequeEspacio1.Border = PdfPCell.NO_BORDER;
                    tableTextBancoCheque.AddCell(celTextBancoChequeEspacio1);

                    PdfPCell celTextBancoChequeEspacio2 = new PdfPCell(new Phrase(" "));
                    //celTextCopia.Border = 0;
                    celTextBancoChequeEspacio2.PaddingTop = 4;
                    celTextBancoChequeEspacio2.Border = PdfPCell.NO_BORDER;
                    tableTextBancoCheque.AddCell(celTextBancoChequeEspacio2);

                    PdfPCell celTextBancoCheque = new PdfPCell(new Phrase("- BANCO (CHEQUE) -", Helpers.Fuentes.Plantilla_4_fontTitleFactura));
                    celTextBancoCheque.Border = PdfPCell.NO_BORDER;
                    tableTextBancoCheque.AddCell(celTextBancoCheque);

                    PdfPCell celBancoCheque = new PdfPCell();
                    celBancoCheque.AddElement(tableTextBancoCheque);
                    celBancoCheque.Colspan = 3;
                    celBancoCheque.Border = 0;
                    tableBC2.AddCell(celBancoCheque);

                    //divTextBancoCheque.AddElement(tableTextBancoCheque);
                    //------- Combinando todo en una sola tabla
                    PdfPTable tbPiePagina = new PdfPTable(1);
                    tbPiePagina.WidthPercentage = 100;
                    PdfPCell celPiePagina = new PdfPCell();
                    celPiePagina.Border = 0;
                    celPiePagina.Padding = 0;

                    celPiePagina.AddElement(tableObsTotales);
                    celPiePagina.AddElement(Helpers.Plantilla_4.divEspacio2);
                    celPiePagina.AddElement(tableResolucion);
                    //celPiePagina.AddElement(divTextCopia);
                    celPiePagina.AddElement(Helpers.Plantilla_4.divEspacio2);
                    celPiePagina.AddElement(Helpers.Plantilla_4.divEspacio2);
                    if (datos.NITReferenia)
                        celPiePagina.AddElement(tableTextNitReferencia);
                    if (datos.BarCode01)
                    {
                        celPiePagina.AddElement(tableBC1);
                        celPiePagina.AddElement(Helpers.Plantilla_4.divEspacio2);
                        celPiePagina.AddElement(Helpers.Plantilla_4.divEspacio2);
                        //celPiePagina.AddElement(divTextBancoEfectivo);
                        celPiePagina.AddElement(Helpers.Plantilla_4.divEspacio2);
                        celPiePagina.AddElement(Helpers.Plantilla_4.divEspacio2);
                    }
                    if (datos.NITReferenia)
                        celPiePagina.AddElement(tableTextNitReferencia);
                    if (datos.BarCode02)
                    {
                        celPiePagina.AddElement(tableBC2);
                        celPiePagina.AddElement(Helpers.Plantilla_4.divEspacio2);
                        celPiePagina.AddElement(Helpers.Plantilla_4.divEspacio2);
                        //celPiePagina.AddElement(divTextBancoCheque);
                    }
                    tbPiePagina.AddCell(celPiePagina);
                    return tbPiePagina;
                }
            }
            public class NotaDebito
            {
                public static PdfPTable Encabezado(string RutaImg)
                {
                    string[] datos = new string[]
                    {
                        "890.920.782-8",
                        "ALMACENES Y TALLERES\r\nMOTO PRECISION S.A.\r\nGRAN CONTRIBUYENTE\r\nNO RETENER IVA",
                        "SOMOS RETENEDORES DEL IVA\r\nCALLE 38 No. 52-97 TELS: 232 49 32 - 232 74 52\r\nFAX 232 01 59\r\nMEDELLIN - COLOMBIA"
                    };
                    return Helpers.Atmopel.Notas.Encabezado(RutaImg, datos);
                }
                public static PdfPTable DatosCliente(DataSet ds, string tipo)
                {
                    return Helpers.Atmopel.Notas.DatosCliente(ds, tipo, "NOTA DEBITO No. ");
                }
                public static PdfPTable DetalleHead()
                {
                    return Helpers.Atmopel.Notas.DetalleHead(95f);
                }
                public static PdfPTable Detalles(DataSet ds)
                {
                    return Helpers.Atmopel.Notas.Detalles(ds);
                }
                public static PdfPTable PiePagina(DataSet ds)
                {
                    return Helpers.Atmopel.Notas.PiePagina(ds);
                }
            }
        }
        public partial class Helpers
        {
            public class Atmopel
            {
                public class Notas
                {
                    //----- Funciones ----
                    public static Document Documento = new Document(iTextSharp.text.PageSize.LETTER, 20f, 15f, 240f, 450f);
                    public static PdfPCell DetalleHeadCelda(string Texto)
                    {
                        Paragraph prgTexto = new Paragraph(Texto, Helpers.Fuentes.Arial10Normal);
                        prgTexto.Alignment = Element.ALIGN_CENTER;
                        PdfPCell celTexto = new PdfPCell();
                        celTexto.AddElement(prgTexto);
                        return celTexto;
                    }
                    public static PdfPCell DetalleHeadCeldaHead(float height)
                    {
                        PdfPCell celDato = new PdfPCell();
                        celDato.MinimumHeight = height;
                        return celDato;
                    }
                    public static PdfPCell DetallesCeldaData(string dato, int alignment)
                    {
                        Paragraph prgdato = new Paragraph(dato, Helpers.Fuentes.Arial8Normal);
                        prgdato.Alignment = alignment;
                        PdfPCell celDato = new PdfPCell();
                        celDato.AddElement(prgdato);
                        celDato.Border = 0;
                        return celDato;
                    }
                    public class EventPageAtmopelNotas : PdfPageEventHelper
                    {
                        PdfContentByte cb;

                        // we will put the final number of pages in a template
                        PdfTemplate headerTemplate, footerTemplate;

                        // this is the BaseFont we are going to use for the header / footer
                        BaseFont bf = null;

                        // This keeps track of the creation time
                        DateTime PrintTime = DateTime.Now;

                        private string _header;

                        public string Header
                        {
                            get { return _header; }
                            set { _header = value; }
                        }
                        PdfPTable _Encabezado;
                        PdfPTable _DatosCliente;
                        PdfPTable _DetalleHead;
                        PdfPTable _PiePagina;
                        public PdfPTable PiePagina
                        {
                            get
                            {
                                return _PiePagina;
                            }
                            set
                            {
                                _PiePagina = value;
                            }
                        }
                        public PdfPTable Encabezado
                        {
                            get
                            {
                                return _Encabezado;
                            }
                            set
                            {
                                _Encabezado = value;
                            }
                        }
                        public PdfPTable DatosCliente
                        {
                            get
                            {
                                return _DatosCliente;
                            }
                            set
                            {
                                _DatosCliente = value;
                            }
                        }
                        public PdfPTable DetalleHead
                        {
                            get
                            {
                                return _DetalleHead;
                            }
                            set
                            {
                                _DetalleHead = value;
                            }
                        }

                        public EventPageAtmopelNotas(PdfPTable encabezado, PdfPTable DatosCliente, PdfPTable detalleHead, PdfPTable piepagina)
                        {
                            _Encabezado = encabezado;
                            _DatosCliente = DatosCliente;
                            _DetalleHead = detalleHead;
                            _PiePagina = piepagina;
                        }

                        public override void OnOpenDocument(PdfWriter writer, Document document)
                        {
                            try
                            {
                                PrintTime = DateTime.Now;
                                bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                                cb = writer.DirectContent;
                                headerTemplate = cb.CreateTemplate(100, 100);
                                footerTemplate = cb.CreateTemplate(50, 50);
                            }
                            catch (DocumentException de)
                            {
                            }
                            catch (System.IO.IOException ioe)
                            {
                            }
                        }

                        public override void OnEndPage(iTextSharp.text.pdf.PdfWriter writer, iTextSharp.text.Document document)
                        {
                            base.OnEndPage(writer, document);

                            //call WriteSelectedRows of PdfTable. This writes rows from PdfWriter in PdfTable
                            //first param is start row. -1 indicates there is no end row and all the rows to be included to write
                            //Third and fourth param is x and y position to start writing


                            //document.Add(_PiePagina);

                            Encabezado.TotalWidth = document.PageSize.Width - 35f;
                            Encabezado.WriteSelectedRows(0, -1, 20, document.PageSize.Height - 30, writer.DirectContentUnder);

                            _DatosCliente.TotalWidth = document.PageSize.Width - 35f;
                            _DatosCliente.WriteSelectedRows(0, -1, 20, document.PageSize.Height - 100, writer.DirectContentUnder);

                            _DetalleHead.TotalWidth = document.PageSize.Width - 35f;
                            _DetalleHead.WriteSelectedRows(0, -1, 20, document.PageSize.Height - 170, writer.DirectContentUnder);

                            _PiePagina.TotalWidth = document.PageSize.Width - 35f;
                            _PiePagina.WriteSelectedRows(0, -1, 20, document.PageSize.Height - 335, writer.DirectContentUnder);

                            //tableUnidades.TotalWidth = document.PageSize.Width - 80f;
                            //tableUnidades.WriteSelectedRows(0, -1, 40, document.PageSize.Height - 40, writer.DirectContentUnder);
                            //pdfTab.WriteSelectedRows(0, -1, 40, document.PageSize.Height - 30, writer.DirectContent);

                            //cb.MoveTo(40, document.PageSize.Height - 100);
                            //cb.LineTo(document.PageSize.Width - 40, document.PageSize.Height - 100);
                            //cb.Stroke();
                        }

                        public override void OnCloseDocument(PdfWriter writer, Document document)
                        {
                            base.OnCloseDocument(writer, document);

                            headerTemplate.BeginText();
                            headerTemplate.SetFontAndSize(bf, 12);
                            headerTemplate.SetTextMatrix(0, 0);
                            headerTemplate.ShowText((writer.PageNumber - 1).ToString());
                            headerTemplate.EndText();

                            //footerTemplate.BeginText();
                            //footerTemplate.SetFontAndSize(bf, 12);
                            //footerTemplate.SetTextMatrix(0, 0);
                            //footerTemplate.ShowText((writer.PageNumber - 1).ToString());
                            //footerTemplate.EndText();
                        }
                    }
                    //----- Elementos ----
                    public static PdfPTable Encabezado(string RutaImg, params string[] datos)
                    {
                        string NIT = Helpers.Compartido.ValidarListaString(datos, 0);
                        string Texto1 = Helpers.Compartido.ValidarListaString(datos, 1);
                        string Texto2 = Helpers.Compartido.ValidarListaString(datos, 2);

                        PdfPTable table = new PdfPTable(3);
                        table.WidthPercentage = 100;
                        table.SetWidths(new float[] { 1f, 1f, 2f });
                        var Logo = Helpers.Compartido.GetLogo(RutaImg, 100f, 60f);
                        Logo.Alignment = Element.ALIGN_CENTER;
                        //Primera celda
                        Paragraph prgNIT = new Paragraph($"\r\nNit: {NIT}", Helpers.Fuentes.Arial10Normal);
                        prgNIT.Alignment = Element.ALIGN_CENTER;
                        PdfPCell celLogo = new PdfPCell();
                        celLogo.AddElement(Logo);
                        celLogo.AddElement(prgNIT);
                        celLogo.Padding = 0;
                        //Celda 2
                        Paragraph prgText1 = new Paragraph($"{Texto1}", Helpers.Fuentes.Arial10Normal);
                        prgText1.Alignment = Element.ALIGN_CENTER;
                        PdfPCell celTexto1 = new PdfPCell();
                        celTexto1.AddElement(prgText1);
                        //Celda 3
                        Paragraph prgText2 = new Paragraph($"{Texto2}", Helpers.Fuentes.Arial10Normal);
                        prgText2.Alignment = Element.ALIGN_CENTER;
                        PdfPCell celTexto2 = new PdfPCell();
                        celTexto2.AddElement(prgText2);
                        //Dando formato a las celdas
                        celLogo.Border = 0;
                        celTexto1.Border = 0;
                        celTexto2.Border = 0;
                        //Agregando celdas a la tabla
                        table.AddCell(celLogo);
                        table.AddCell(celTexto1);
                        table.AddCell(celTexto2);
                        //retornando tabla
                        return table;
                    }
                    public static PdfPTable DatosCliente(DataSet ds, string Tipo, string Titulo1)
                    {
                        //declaracion de variables
                        string TipoFactura = string.Empty;
                        string LegalNumber = string.Empty;
                        string Fecha = string.Empty;
                        string ClienteTipoIdentificacion = string.Empty;
                        string ClienteNIT = string.Empty;
                        string ClienteTipoDocumento = string.Empty;
                        string ClienteNombre = string.Empty;
                        string ClienteDireccion = string.Empty;
                        string ClienteDireccion1 = string.Empty;
                        string ClienteDireccion2 = string.Empty;
                        string ClienteDireccion3 = string.Empty;
                        string ClienteCiudad = string.Empty;
                        string ClienteTelefonos = string.Empty;
                        string VendedorNombre = string.Empty;
                        //----- Asignando valores ------
                        //Titulo1 = "NOTA DEBITO No. ";
                        if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "LegalNumber", 0))
                            LegalNumber = ds.Tables["InvcHead"].Rows[0]["LegalNumber"].ToString();
                        if(Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead","InvoiceDate",0))
                            Fecha = ds.Tables["InvcHead"].Rows[0]["InvoiceDate"].ToString();
                        if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "Customer","Name",0))
                            ClienteNombre = ds.Tables["Customer"].Rows[0]["Name"].ToString();
                        if(Helpers.Compartido.VerificarExistenciaColumnas(ds, "Customer", "Address1", 0))
                            ClienteDireccion1 = ds.Tables["Customer"].Rows[0]["Address1"].ToString();
                        if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "Customer", "Address2", 0))
                            ClienteDireccion2 = ds.Tables["Customer"].Rows[0]["Address2"].ToString();
                        if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "Customer", "Address3", 0))
                            ClienteDireccion3 = ds.Tables["Customer"].Rows[0]["Address3"].ToString();
                        ClienteDireccion = ClienteDireccion1;
                        if (!string.IsNullOrEmpty(ClienteDireccion2))
                            ClienteDireccion += " " + ClienteDireccion2;
                        if(!string.IsNullOrEmpty(ClienteDireccion3))
                            ClienteDireccion += " " + ClienteDireccion3;
                        if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "Customer", "City", 0))
                            ClienteCiudad = ds.Tables["Customer"].Rows[0]["City"].ToString();
                        if(Helpers.Compartido.VerificarExistenciaColumnas(ds, "Customer", "PhoneNum",0))
                            ClienteTelefonos = ds.Tables["Customer"].Rows[0]["PhoneNum"].ToString();
                        if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "SalesRepName1", 0))
                            VendedorNombre = ds.Tables["InvcHead"].Rows[0]["SalesRepName1"].ToString();
                        if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "COOneTime", "IdentificationType", 0))
                            ClienteTipoIdentificacion = ds.Tables["COOneTime"].Rows[0]["IdentificationType"].ToString();
                        if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "Customer", "ResaleID", 0))
                            ClienteNIT = ds.Tables["Customer"].Rows[0]["ResaleID"].ToString();

                        switch(Tipo)
                        {
                            case "CreditNoteType":
                                TipoFactura = "NCO";
                                Titulo1 = "NOTA CREDITO No. ";
                                break;
                            case "DebitNoteType":
                                TipoFactura = "ND";
                                Titulo1 = "NOTA DEBITO No. ";
                                break;
                        }
                        switch (ClienteTipoIdentificacion)
                        {
                            case "13":
                                ClienteTipoDocumento = "CC";
                                break;
                            case "31":
                                ClienteTipoDocumento = "NIT";
                                break;
                            default:
                                break;
                        }
                        //--------------- creacion de tabla
                        PdfPTable table = new PdfPTable(4);
                        table.WidthPercentage = 100;
                        table.SetWidths(new float[] { 0.5f, 2f, 0.5f, 2f});
                        //primera fila
                        Paragraph prgTipo = new Paragraph(TipoFactura, Helpers.Fuentes.Arial10Normal);
                        PdfPCell celTipo = new PdfPCell();
                        celTipo.AddElement(prgTipo);

                        Paragraph prgTitulo = new Paragraph(Titulo1 + LegalNumber, Helpers.Fuentes.Arial10Normal);
                        PdfPCell celTitulo = new PdfPCell();
                        celTitulo.AddElement(prgTitulo);

                        Paragraph prgFecha = new Paragraph("Fecha: " + Convert.ToDateTime(Fecha).ToShortDateString(), Helpers.Fuentes.Arial10Normal);
                        prgFecha.Alignment = Element.ALIGN_RIGHT;
                        PdfPCell celFecha = new PdfPCell();
                        celFecha.Colspan = 2;
                        celFecha.AddElement(prgFecha);

                        //Segunda fila
                        Paragraph prgTituloCliente = new Paragraph("Cliente:", Helpers.Fuentes.Arial10Normal);
                        PdfPCell celTituloCliente = new PdfPCell();
                        celTituloCliente.AddElement(prgTituloCliente);

                        Paragraph prgValorCliente = new Paragraph(ClienteTipoDocumento + " " + ClienteNIT + " " + ClienteNombre,Helpers.Fuentes.Arial10Normal);
                        PdfPCell celValorCliente = new PdfPCell();
                        celValorCliente.AddElement(prgValorCliente);
                        celValorCliente.Colspan = 3;

                        //Tercera fila
                        Paragraph prgTituloDireccion = new Paragraph("Direccion:",Helpers.Fuentes.Arial10Normal);
                        PdfPCell celTituloDireccion = new PdfPCell();
                        celTituloDireccion.AddElement(prgTituloDireccion);

                        Paragraph prgValorDireccion = new Paragraph(ClienteDireccion, Helpers.Fuentes.Arial10Normal);
                        PdfPCell celValorDireccion = new PdfPCell();
                        celValorDireccion.AddElement(prgValorDireccion);

                        Paragraph prgTituloTelefono = new Paragraph("Teléfonos:", Helpers.Fuentes.Arial10Normal);
                        PdfPCell celTituloTelefono = new PdfPCell();
                        celTituloTelefono.AddElement(prgTituloTelefono);

                        Paragraph prgValorTelefono = new Paragraph(ClienteTelefonos, Helpers.Fuentes.Arial10Normal);
                        PdfPCell celValorTelefono = new PdfPCell();
                        celValorTelefono.AddElement(prgValorTelefono);

                        //cuarta fila
                        Paragraph prgTituloCiudad = new Paragraph("Ciudad:", Helpers.Fuentes.Arial10Normal);
                        PdfPCell celTituloCiudad = new PdfPCell();
                        celTituloCiudad.AddElement(prgTituloCiudad);

                        Paragraph prgValorCiudad = new Paragraph(ClienteCiudad, Helpers.Fuentes.Arial10Normal);
                        PdfPCell celValorCiudad = new PdfPCell();
                        celValorCiudad.AddElement(prgValorCiudad);

                        Paragraph prgTituloVendedor = new Paragraph("Vendedor:", Helpers.Fuentes.Arial10Normal);
                        PdfPCell celTituloVendedor = new PdfPCell();
                        celTituloVendedor.AddElement(prgTituloVendedor);

                        Paragraph prgValorVendedor = new Paragraph(VendedorNombre, Helpers.Fuentes.Arial10Normal);
                        PdfPCell celValorVendedor = new PdfPCell();
                        celValorVendedor.AddElement(prgValorVendedor);
                        //Preparando los bordes de las celdas
                        celTipo.Border = 0;
                        celTitulo.Border = 0;
                        celFecha.Border = 0;
                        celTituloCliente.BorderWidthRight = 0;
                        celTituloCliente.BorderWidthBottom = 0;
                        celValorCliente.BorderWidthBottom = 0;
                        celValorCliente.BorderWidthLeft = 0;
                        celTituloDireccion.BorderWidthRight = 0;
                        celTituloDireccion.BorderWidthBottom = 0;
                        celTituloDireccion.BorderWidthTop = 0;
                        celValorDireccion.Border = 0;
                        celTituloTelefono.Border = 0;
                        celValorTelefono.BorderWidthBottom = 0;
                        celValorTelefono.BorderWidthTop = 0;
                        celValorTelefono.BorderWidthLeft = 0;
                        celTituloCiudad.BorderWidthTop = 0;
                        celTituloCiudad.BorderWidthRight = 0;
                        celValorCiudad.BorderWidthTop = 0;
                        celValorCiudad.BorderWidthLeft = 0;
                        celValorCiudad.BorderWidthRight = 0;
                        celTituloVendedor.BorderWidthLeft = 0;
                        celTituloVendedor.BorderWidthRight = 0;
                        celTituloVendedor.BorderWidthTop = 0;
                        celValorVendedor.BorderWidthTop = 0;
                        celValorVendedor.BorderWidthLeft = 0;

                        //ingreso de las celdas a la tabla
                        //fila 1
                        table.AddCell(celTipo);
                        table.AddCell(celTitulo);
                        table.AddCell(celFecha);
                        //fila 2
                        table.AddCell(celTituloCliente);
                        table.AddCell(celValorCliente);
                        //Fila 3
                        table.AddCell(celTituloDireccion);
                        table.AddCell(celValorDireccion);
                        table.AddCell(celTituloTelefono);
                        table.AddCell(celValorTelefono);
                        //Fila 4
                        table.AddCell(celTituloCiudad);
                        table.AddCell(celValorCiudad);
                        table.AddCell(celTituloVendedor);
                        table.AddCell(celValorVendedor);

                        return table;
                    }
                    public static PdfPTable DetalleHead(float height)
                    {
                        //Declaracion de variables
                        string Texto1 = "Por medio de la presente le rogamos tomar nota de los abonos realizados a su apreciada cuenta.";
                        string Texto2 = "Por Concepto de:";
                        string strCuenta = "Cuenta";
                        string strDescripcion = "Descripción";
                        string strDebito = "Débito";
                        string strCredito = "Crédito";
                        string strValor = "Valor";
                        iTextSharp.text.BaseColor ColorFondo = BaseColor.GRAY;

                        //-------------------------
                        PdfPTable table = new PdfPTable(5);
                        table.SetWidths(new float[] { 1.2f, 2f, 1f, 1f, 1f });
                        table.WidthPercentage = 100;
                        //Fila cero, separador
                        Paragraph prgSeparador = new Paragraph(" ", Helpers.Fuentes.fontCustom);
                        prgSeparador.PaddingTop = 2;
                        PdfPCell celSeparador = new PdfPCell();
                        celSeparador.AddElement(prgSeparador);
                        //primera fila
                        Paragraph prgTexto1 = new Paragraph(Texto1, Helpers.Fuentes.Arial10Normal);
                        prgTexto1.Alignment = Element.ALIGN_CENTER;
                        PdfPCell celTexto1 = new PdfPCell();
                        celTexto1.AddElement(prgTexto1);
                        //segunda fila
                        Paragraph prgTexto2 = new Paragraph(Texto2,Helpers.Fuentes.Arial10Normal);
                        prgTexto2.Alignment = Element.ALIGN_CENTER;
                        PdfPCell celTexto2 = new PdfPCell();
                        celTexto2.AddElement(prgTexto2);
                        //tercera fila
                        PdfPCell celCuenta = Helpers.Atmopel.Notas.DetalleHeadCelda(strCuenta);
                        PdfPCell celDescripcion = Helpers.Atmopel.Notas.DetalleHeadCelda(strDescripcion);
                        PdfPCell celDebito = Helpers.Atmopel.Notas.DetalleHeadCelda(strDebito);
                        PdfPCell celCredito = Helpers.Atmopel.Notas.DetalleHeadCelda(strCredito);
                        PdfPCell celValor = Helpers.Atmopel.Notas.DetalleHeadCelda(strValor);
                        //Cuarta Fila
                        PdfPCell celCuentaData = Helpers.Atmopel.Notas.DetalleHeadCeldaHead(height);
                        PdfPCell celDescripcionData = Helpers.Atmopel.Notas.DetalleHeadCeldaHead(height);
                        PdfPCell celDebitoData = Helpers.Atmopel.Notas.DetalleHeadCeldaHead(height);
                        PdfPCell celCreditoData = Helpers.Atmopel.Notas.DetalleHeadCeldaHead(height);
                        PdfPCell celValorData = Helpers.Atmopel.Notas.DetalleHeadCeldaHead(height);

                        //-- Alterando propiedades de las celdas ----
                        celSeparador.Colspan = 5;
                        celSeparador.Border = 0;
                        celTexto1.Colspan = 5;
                        celTexto1.HorizontalAlignment = Element.ALIGN_MIDDLE;
                        celTexto2.Colspan = 5;
                        celCuenta.BackgroundColor = ColorFondo;
                        celDescripcion.BackgroundColor = ColorFondo;
                        celDebito.BackgroundColor = ColorFondo;
                        celCredito.BackgroundColor = ColorFondo;
                        celValor.BackgroundColor = ColorFondo;

                        //Agregado de celdas
                        //Fila 0
                        table.AddCell(celSeparador);
                        //Fila 1
                        table.AddCell(celTexto1);
                        //Fila 2
                        table.AddCell(celTexto2);
                        //Fila 3
                        table.AddCell(celCuenta);
                        table.AddCell(celDescripcion);
                        table.AddCell(celDebito);
                        table.AddCell(celCredito);
                        table.AddCell(celValor);
                        //Fila 4
                        table.AddCell(celCuentaData);
                        table.AddCell(celDescripcionData);
                        table.AddCell(celDebitoData);
                        table.AddCell(celCreditoData);
                        table.AddCell(celValorData);

                        return table;
                    }
                    public static PdfPTable Detalles(DataSet ds)
                    {
                        //--- variables
                        string strCuenta = string.Empty;
                        string strDescripcion = string.Empty;
                        string strDebito = string.Empty;
                        string strCredito = string.Empty;
                        string strValor = string.Empty;
                        //-------------------
                        PdfPTable table = new PdfPTable(5);
                        table.SetWidths(new float[] { 1.2f, 2f, 1f, 1f, 1f });
                        table.WidthPercentage = 100;
                        //--- Ingreso masivo
                        foreach (DataRow row in ds.Tables["InvcDtl"].Rows)
                        {
                            //----- Capturando valores
                            if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcDtl", "ShortChar02"))
                                strCuenta = row["ShortChar02"].ToString();
                            if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcDtl", "LineDesc"))
                                strDescripcion = row["LineDesc"].ToString();
                            if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcDtl", "Number01"))
                                strDebito = Convert.ToDecimal(row["Number01"]).ToString("N2");
                            if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcDtl", "Number02"))
                                strCredito = Convert.ToDecimal(row["Number02"]).ToString("N2");
                            if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcDtl", "Number03"))
                                strValor = Convert.ToDecimal(row["Number03"]).ToString("N2");
                            //---------------------------------------------
                            PdfPCell celCuenta = Helpers.Atmopel.Notas.DetallesCeldaData(strCuenta, Element.ALIGN_RIGHT);
                            PdfPCell celDescripcion = Helpers.Atmopel.Notas.DetallesCeldaData(strDescripcion, Element.ALIGN_LEFT);
                            PdfPCell celDebito = Helpers.Atmopel.Notas.DetallesCeldaData(strDebito, Element.ALIGN_RIGHT);
                            PdfPCell celCredito = Helpers.Atmopel.Notas.DetallesCeldaData(strCredito, Element.ALIGN_RIGHT);
                            PdfPCell celValor = Helpers.Atmopel.Notas.DetallesCeldaData(strValor, Element.ALIGN_RIGHT);

                            table.AddCell(celCuenta);
                            table.AddCell(celDescripcion);
                            table.AddCell(celDebito);
                            table.AddCell(celCredito);
                            table.AddCell(celValor);
                        }
                        return table;
                    }
                    public static PdfPTable PiePagina(DataSet ds)
                    {
                        //---- variables
                        //Declaracion de variables
                        string strObservaciones = string.Empty;
                        string strValorNota = string.Empty;
                        if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "InvoiceComment", 0))
                            strObservaciones = ds.Tables["InvcHead"].Rows[0]["InvoiceComment"].ToString();
                        if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "DocInvoiceAmt", 0))
                            strValorNota = Convert.ToDecimal(ds.Tables["InvcHead"].Rows[0]["DocInvoiceAmt"]).ToString("N2");
                        //---------------------
                        PdfPTable table = new PdfPTable(5);
                        table.SetWidths(new float[] { 1.2f, 2f, 1f, 1f, 1f });
                        table.WidthPercentage = 100;

                        Paragraph prgObservaciones = new Paragraph("Observaciones: " + strObservaciones,Helpers.Fuentes.Arial10Normal);
                        PdfPCell celObservaciones = new PdfPCell();
                        celObservaciones.AddElement(prgObservaciones);

                        Paragraph prgTituloValor = new Paragraph("Valor nota:", Helpers.Fuentes.Arial10Normal);
                        prgTituloValor.Alignment = Element.ALIGN_CENTER;
                        PdfPCell celTituloValor = new PdfPCell();
                        celTituloValor.AddElement(prgTituloValor);

                        Paragraph prgValorValor = new Paragraph(strValorNota, Helpers.Fuentes.Arial10Normal);
                        prgValorValor.Alignment = Element.ALIGN_RIGHT;
                        PdfPCell celValorValor = new PdfPCell();
                        celValorValor.AddElement(prgValorValor);

                        //Preparando las celdas
                        celObservaciones.Colspan = 3;
                        celObservaciones.BorderWidthRight = 0;
                        celObservaciones.BorderWidthTop = 0;
                        celTituloValor.BorderWidthLeft = 0;
                        celTituloValor.BorderWidthRight = 0;
                        celTituloValor.BorderWidthTop = 0;
                        celValorValor.BorderWidthLeft = 0;
                        celValorValor.BorderWidthTop = 0;
                        //agregando celdas a la tabla
                        table.AddCell(celObservaciones);
                        table.AddCell(celTituloValor);
                        table.AddCell(celValorValor);

                        return table;
                    }
                }
                public class EventPageAtmopel : PdfPageEventHelper
                {
                    PdfContentByte cb;

                    // we will put the final number of pages in a template
                    PdfTemplate headerTemplate, footerTemplate;

                    // this is the BaseFont we are going to use for the header / footer
                    BaseFont bf = null;

                    // This keeps track of the creation time
                    DateTime PrintTime = DateTime.Now;

                    private string _header;

                    public string Header
                    {
                        get { return _header; }
                        set { _header = value; }
                    }
                    PdfPTable _Encabezado;
                    PdfPTable _DatosCliente;
                    PdfPTable _DetalleHead;

                    PdfPTable _PiePagina;
                    public PdfPTable PiePagina
                    {
                        get
                        {
                            return _PiePagina;
                        }
                        set
                        {
                            _PiePagina = value;
                        }
                    }
                    public PdfPTable Encabezado
                    {
                        get
                        {
                            return _Encabezado;
                        }
                        set
                        {
                            _Encabezado = value;
                        }
                    }
                    public PdfPTable DatosCliente
                    {
                        get
                        {
                            return _DatosCliente;
                        }
                        set
                        {
                            _DatosCliente = value;
                        }
                    }
                    public PdfPTable DetalleHead
                    {
                        get
                        {
                            return _DetalleHead;
                        }
                        set
                        {
                            _DetalleHead = value;
                        }
                    }

                    public EventPageAtmopel(PdfPTable encabezado, PdfPTable DatosCliente, PdfPTable detalleHead, PdfPTable piepagina)
                    {
                        _Encabezado = encabezado;
                        _DatosCliente = DatosCliente;
                        _DetalleHead = detalleHead;
                        _PiePagina = piepagina;
                    }

                    public override void OnOpenDocument(PdfWriter writer, Document document)
                    {
                        try
                        {
                            PrintTime = DateTime.Now;
                            bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                            cb = writer.DirectContent;
                            headerTemplate = cb.CreateTemplate(100, 100);
                            footerTemplate = cb.CreateTemplate(50, 50);
                        }
                        catch (DocumentException de)
                        {
                        }
                        catch (System.IO.IOException ioe)
                        {
                        }
                    }

                    public override void OnEndPage(iTextSharp.text.pdf.PdfWriter writer, iTextSharp.text.Document document)
                    {
                        base.OnEndPage(writer, document);

                        //call WriteSelectedRows of PdfTable. This writes rows from PdfWriter in PdfTable
                        //first param is start row. -1 indicates there is no end row and all the rows to be included to write
                        //Third and fourth param is x and y position to start writing

                        Encabezado.TotalWidth = document.PageSize.Width - 35f;
                        Encabezado.WriteSelectedRows(0, -1, 20, document.PageSize.Height - 30, writer.DirectContentUnder);

                        _DatosCliente.TotalWidth = document.PageSize.Width - 35f;
                        _DatosCliente.WriteSelectedRows(0, -1, 20, document.PageSize.Height - 130, writer.DirectContentUnder);

                        _DetalleHead.TotalWidth = document.PageSize.Width - 35f;
                        _DetalleHead.WriteSelectedRows(0, -1, 20, document.PageSize.Height - 275, writer.DirectContentUnder);

                        _PiePagina.TotalWidth = document.PageSize.Width - 35f;
                        _PiePagina.WriteSelectedRows(0, -1, 20, document.PageSize.Height - 600, writer.DirectContentUnder);

                    }

                    public override void OnCloseDocument(PdfWriter writer, Document document)
                    {
                        base.OnCloseDocument(writer, document);

                        headerTemplate.BeginText();
                        headerTemplate.SetFontAndSize(bf, 12);
                        headerTemplate.SetTextMatrix(0, 0);
                        headerTemplate.ShowText((writer.PageNumber - 1).ToString());
                        headerTemplate.EndText();

                        //footerTemplate.BeginText();
                        //footerTemplate.SetFontAndSize(bf, 12);
                        //footerTemplate.SetTextMatrix(0, 0);
                        //footerTemplate.ShowText((writer.PageNumber - 1).ToString());
                        //footerTemplate.EndText();
                    }
                }
                public static bool AddUnidadesAtmopel(DataRow dataLine, ref PdfPTable table, iTextSharp.text.Font fontTitleFactura, DataSet dataSet, ref string strError)
                {
                    string InvcTaxPercent = "0";
                    try
                    {
                        string Referencia = string.Empty;
                        string DiscountPercent = "0";
                        if (Helpers.Compartido.VerificarExistenciaColumnas(dataLine, "DiscountPercent"))
                            DiscountPercent = decimal.Parse(dataLine["DiscountPercent"].ToString()).ToString("N0");
                        try
                        {
                            Referencia = (string)dataLine["ShortChar01"];
                        }
                        catch{ }
                        InvcTaxPercent = AtmopelGetIVAPercent(dataSet, dataLine);
                        AddUnidadesCelda(ref table, (string)dataLine["InvoiceLine"], fontTitleFactura, ref strError, "Add InvoiceLine", "InvoiceLine OK", 0, 0.5f, 0, 0, Element.ALIGN_CENTER);
                        AddUnidadesCelda(ref table, (string)dataLine["PartNum"], fontTitleFactura, ref strError, "Add PartNum", "Add PartNum OK", 0, 0.5f, 0, 0, Element.ALIGN_LEFT);
                        AddUnidadesCelda(ref table, Referencia, fontTitleFactura, ref strError, "Add PartNum", "Add PartNum OK", 0, 0.5f, 0, 0, Element.ALIGN_LEFT);
                        AddUnidadesCelda(ref table, decimal.Parse((string)dataLine["SellingShipQty"]).ToString("N"), fontTitleFactura, ref strError, "Add SellingShipQty", "Add SellingShipQty OK", 0, 0.5f, 0, 0, Element.ALIGN_RIGHT);
                        AddUnidadesCelda(ref table, (string)dataLine["SalesUM"], fontTitleFactura, ref strError, "Add SalesUM", "Add SalesUM OK", 0, 0.5f, 0, 0, Element.ALIGN_LEFT);
                        AddUnidadesCelda(ref table, (string)dataLine["LineDesc"], fontTitleFactura, ref strError, "Add LineDesc", "Add LineDesc OK", 0, 0.5f, 0, 0, Element.ALIGN_LEFT);
                        AddUnidadesCelda(ref table, decimal.Parse((string)dataLine["DocUnitPrice"]).ToString("N2"), fontTitleFactura, ref strError, "Add DocUnitPrice", "Add DocUnitPrice OK", 0, 0.5f, 0, 0, Element.ALIGN_RIGHT);
                        //AddUnidadesCelda(ref table, decimal.Parse((string)dataLine["DspDocLessDiscount"]).ToString("N2"), fontTitleFactura, ref strError, "Add DspDocLessDiscount", "Add DspDocLessDiscount OK", 0, 0.5f, 0, 0, Element.ALIGN_RIGHT);
                        AddUnidadesCelda(ref table, DiscountPercent + " %", fontTitleFactura, ref strError, "Add DspDocLessDiscount", "Add DspDocLessDiscount OK", 0, 0.5f, 0, 0, Element.ALIGN_RIGHT);
                        AddUnidadesCelda(ref table, decimal.Parse((string)dataLine["DspDocExtPrice"]).ToString("N2"), fontTitleFactura, ref strError, "Add DspDocExtPrice", "Add DspDocExtPrice OK", 0, 0.5f, 0, 0, Element.ALIGN_RIGHT);
                        //AddUnidadesCelda(ref table, decimal.Parse((string)dataLine["InvoiceNum"]).ToString("N2"), fontTitleFactura, ref strError, "Add InvoiceNum", "Add InvoiceNum OK", 0, 0.5f, 0.5f, 0, Element.ALIGN_RIGHT);
                        AddUnidadesCelda(ref table, InvcTaxPercent, fontTitleFactura, ref strError, "Add % IVA", "Add % IVA OK", 0, 0.5f, 0.5f, 0, Element.ALIGN_RIGHT);
                        return true;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                        return false;
                    }
                }
                private static PdfPCell AddUnidadesCelda(ref PdfPTable table,
                    string data,
                    iTextSharp.text.Font fontTitleFactura,
                    ref string strError,
                    string mensaje1,
                    string mensaje2,
                    float BorderWidthBottom,
                    float BorderWidthLeft,
                    float BorderWidthRight,
                    float BorderWidthTop,
                    int HorizontalAlignment)
                {
                    strError += mensaje1;
                    iTextSharp.text.pdf.PdfPCell celda = new iTextSharp.text.pdf.PdfPCell(new Phrase(data, fontTitleFactura));
                    celda.Colspan = 1;
                    celda.Padding = 2;
                    celda.Border = 0;
                    celda.BorderWidthBottom = BorderWidthBottom;
                    celda.BorderWidthLeft = BorderWidthLeft;
                    celda.BorderWidthRight = BorderWidthRight;
                    celda.BorderWidthTop = BorderWidthTop;
                    celda.HorizontalAlignment = HorizontalAlignment;
                    celda.VerticalAlignment = Element.ALIGN_TOP;
                    table.AddCell(celda);
                    strError += mensaje2;
                    return celda;
                }
                private static string AtmopelGetIVAPercent(DataSet ds, DataRow rowdtl)
                {
                    string InvcTaxPercent = "0.00";
                    string InvcDtlInvoiceLine = "0";
                    if (ds == null)
                        return InvcTaxPercent;
                    if (Helpers.Compartido.VerificarExistenciaColumnas(rowdtl, "InvoiceLine"))
                        InvcDtlInvoiceLine = rowdtl["InvoiceLine"].ToString();
                    if(ds.Tables.Contains("InvcTax") && ds.Tables["InvcTax"].Rows.Count > 0)
                    {
                        var impuestos = from row in ds.Tables["InvcTax"].AsEnumerable()
                                        where row["InvoiceLine"].ToString() == InvcDtlInvoiceLine
                                        select new
                                        {
                                            Company = row["Company"].ToString(),
                                            InvoiceNum = Convert.ToInt32(row["InvoiceNum"]),
                                            InvoiceLine = Convert.ToInt32(row["InvoiceLine"]),
                                            Percent = Convert.ToDecimal(row["Percent"])
                                        };
                        if(impuestos != null && impuestos.Count() > 0)
                            InvcTaxPercent = impuestos.FirstOrDefault().Percent.ToString("N2");
                    }
                    return InvcTaxPercent;
                }
            }
            public class Plantilla_4
            {
                private static float _DetalleHeadMinimumHeight = 177;
                private static float[] _DimensionUnidades = new float[] { 0.178f, 0.5f, 0.5f, 0.328f, 2.0f, 0.585f, 0.4f, 0.5f, 0.28f};
                public static float DetalleHeadMinimumHeight
                {
                    get
                    {
                        return _DetalleHeadMinimumHeight;
                    }
                    set
                    {
                        _DetalleHeadMinimumHeight = value;
                    }
                }
                public class EventPagePlantilla4 : PdfPageEventHelper
                {
                    PdfContentByte cb;

                    // we will put the final number of pages in a template
                    PdfTemplate headerTemplate, footerTemplate;

                    // this is the BaseFont we are going to use for the header / footer
                    BaseFont bf = null;

                    // This keeps track of the creation time
                    DateTime PrintTime = DateTime.Now;

                    private string _header;

                    public string Header
                    {
                        get { return _header; }
                        set { _header = value; }
                    }
                    PdfPTable _Encabezado;
                    PdfPTable _DatosCliente;
                    PdfPTable _DetalleHead;

                    PdfPTable _PiePagina;
                    public PdfPTable PiePagina
                    {
                        get
                        {
                            return _PiePagina;
                        }
                        set
                        {
                            _PiePagina = value;
                        }
                    }
                    public PdfPTable Encabezado
                    {
                        get
                        {
                            return _Encabezado;
                        }
                        set
                        {
                            _Encabezado = value;
                        }
                    }
                    public PdfPTable DatosCliente
                    {
                        get
                        {
                            return _DatosCliente;
                        }
                        set
                        {
                            _DatosCliente = value;
                        }
                    }
                    public PdfPTable DetalleHead
                    {
                        get
                        {
                            return _DetalleHead;
                        }
                        set
                        {
                            _DetalleHead = value;
                        }
                    }

                    public EventPagePlantilla4(PdfPTable encabezado, PdfPTable DatosCliente, PdfPTable detalleHead, PdfPTable piepagina)
                    {
                        _Encabezado = encabezado;
                        _DatosCliente = DatosCliente;
                        _DetalleHead = detalleHead;
                        _PiePagina = piepagina;
                    }

                    public override void OnOpenDocument(PdfWriter writer, Document document)
                    {
                        try
                        {
                            PrintTime = DateTime.Now;
                            bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                            cb = writer.DirectContent;
                            headerTemplate = cb.CreateTemplate(100, 100);
                            footerTemplate = cb.CreateTemplate(50, 50);
                        }
                        catch (DocumentException de)
                        {
                        }
                        catch (System.IO.IOException ioe)
                        {
                        }
                    }

                    public override void OnEndPage(iTextSharp.text.pdf.PdfWriter writer, iTextSharp.text.Document document)
                    {
                        base.OnEndPage(writer, document);

                        //call WriteSelectedRows of PdfTable. This writes rows from PdfWriter in PdfTable
                        //first param is start row. -1 indicates there is no end row and all the rows to be included to write
                        //Third and fourth param is x and y position to start writing


                        //document.Add(_PiePagina);

                        Encabezado.TotalWidth = document.PageSize.Width - 35f;
                        Encabezado.WriteSelectedRows(0, -1, 20, document.PageSize.Height - 30, writer.DirectContentUnder);

                        _DatosCliente.TotalWidth = document.PageSize.Width - 35f;
                        _DatosCliente.WriteSelectedRows(0, -1, 20, document.PageSize.Height - 110, writer.DirectContentUnder);

                        _DetalleHead.TotalWidth = document.PageSize.Width - 35f;
                        _DetalleHead.WriteSelectedRows(0, -1, 20, document.PageSize.Height - 240, writer.DirectContentUnder);

                        _PiePagina.TotalWidth = document.PageSize.Width - 35f;
                        _PiePagina.WriteSelectedRows(0, -1, 20, document.PageSize.Height - 430, writer.DirectContentUnder);

                        //tableUnidades.TotalWidth = document.PageSize.Width - 80f;
                        //tableUnidades.WriteSelectedRows(0, -1, 40, document.PageSize.Height - 40, writer.DirectContentUnder);
                        //pdfTab.WriteSelectedRows(0, -1, 40, document.PageSize.Height - 30, writer.DirectContent);

                        //cb.MoveTo(40, document.PageSize.Height - 100);
                        //cb.LineTo(document.PageSize.Width - 40, document.PageSize.Height - 100);
                        //cb.Stroke();
                    }

                    public override void OnCloseDocument(PdfWriter writer, Document document)
                    {
                        base.OnCloseDocument(writer, document);

                        headerTemplate.BeginText();
                        headerTemplate.SetFontAndSize(bf, 12);
                        headerTemplate.SetTextMatrix(0, 0);
                        headerTemplate.ShowText((writer.PageNumber - 1).ToString());
                        headerTemplate.EndText();

                        //footerTemplate.BeginText();
                        //footerTemplate.SetFontAndSize(bf, 12);
                        //footerTemplate.SetTextMatrix(0, 0);
                        //footerTemplate.ShowText((writer.PageNumber - 1).ToString());
                        //footerTemplate.EndText();
                    }
                }
                public class EncabezadoDatos
                {
                    string _Empresa = "";
                    string _EmpresaTexto = "";
                    string _EmpresaPPal = "ABRASIVOS DE COLOMBIA S.A.\nNIT. 890.911.327 - 1";
                    string _EmpresaPPalDireccion = "Autopista Norte KM 20 Girardota - Antioquia\nTel:(054)289 0033\nLinea de atención al cliete 01 8000 51 5452\nwww.abracol.com - abracol@abracol.com\nMedellin - Colombia";
                    string _Sede01 = "Santafe de Bogotá";
                    string _Sede01Direcion = "Teléfono:(051)370 0477\n";
                    string _Sede02 = "Pereira";
                    string _Sede02Direccion = "Celular:312 287 5895\n";
                    string _Sede03 = "Bucaramanga";
                    string _Sede03Direccion = "Tel:609 0180 - Cel:312 287 5907";
                    string _Sede04 = "Medellín";
                    string _Sede04Direccion = "Teléfono:(054)262 5335\n";
                    string _Sede05 = "Cali";
                    string _Sede05Direccion = "Teléfono:(052)445 2355\n";
                    string _Sede06 = "Barranquilla";
                    string _Sede06Direccion = "Celular:312 287 5913";
                    string _AVGCerCalidad = "\nAGV-816-F03\nCERT. DE CALIDAD ISO\n9001/2000/REG:CO-368-1";
                    float[] _Dimensiones = new float[5] { 1.3f, 1.3f, 1f, 0.9f, 0.8f };
                    public string Empresa
                    {
                        get
                        {
                            return _Empresa;
                        }
                        set
                        {
                            _Empresa = value;
                        }
                    }
                    public string EmpresaTexto
                    {
                        get
                        {
                            return _EmpresaTexto;
                        }
                        set
                        {
                            _EmpresaTexto = value;
                        }
                    }
                    public string EmpresaPPal
                    {
                        get
                        {
                            return _EmpresaPPal;
                        }
                        set
                        {
                            _EmpresaPPal = value;
                        }
                    }
                    public string EmpresaPPalDireccion
                    {
                        get
                        {
                            return _EmpresaPPalDireccion;
                        }
                        set
                        {
                            _EmpresaPPalDireccion = value;
                        }
                    }
                    public string Sede01
                    {
                        get
                        {
                            return _Sede01;
                        }
                        set
                        {
                            _Sede01 = value;
                        }
                    }
                    public string Sede01Direccion
                    {
                        get
                        {
                            return _Sede01Direcion;
                        }
                        set
                        {
                            _Sede01Direcion = value;
                        }
                    }
                    public string Sede02
                    {
                        get
                        {
                            return _Sede02;
                        }
                        set
                        {
                            _Sede02 = value;
                        }
                    }
                    public string Sede02Direccion
                    {
                        get
                        {
                            return _Sede02Direccion;
                        }
                        set
                        {
                            _Sede02Direccion = value;
                        }
                    }
                    public string Sede03
                    {
                        get
                        {
                            return _Sede03;
                        }
                        set
                        {
                            _Sede03 = value;
                        }
                    }
                    public string Sede03Direccion
                    {
                        get
                        {
                            return _Sede03Direccion;
                        }
                        set
                        {
                            _Sede03Direccion = value;
                        }
                    }
                    public string Sede04
                    {
                        get
                        {
                            return _Sede04;
                        }
                        set
                        {
                            _Sede04 = value;
                        }
                    }
                    public string Sede04Direccion
                    {
                        get
                        {
                            return _Sede04Direccion;
                        }
                        set
                        {
                            _Sede04Direccion = value;
                        }
                    }
                    public string Sede05
                    {
                        get
                        {
                            return _Sede05;
                        }
                        set
                        {
                            _Sede05 = value;
                        }
                    }
                    public string Sede05Direccion
                    {
                        get
                        {
                            return _Sede05Direccion;
                        }
                        set
                        {
                            _Sede05Direccion = value;
                        }
                    }
                    public string Sede06
                    {
                        get
                        {
                            return _Sede06;
                        }
                        set
                        {
                            _Sede06 = value;
                        }
                    }
                    public string Sede06Direccion
                    {
                        get
                        {
                            return _Sede06Direccion;
                        }
                        set
                        {
                            _Sede06Direccion = value;
                        }
                    }
                    public string AVGCerCalidad
                    {
                        get
                        {
                            return _AVGCerCalidad;
                        }
                        set
                        {
                            _AVGCerCalidad = value;
                        }
                    }
                    public float[] Dimensiones
                    {
                        get
                        {
                            return _Dimensiones;
                        }
                        set
                        {
                            _Dimensiones = value;
                        }
                    }
                }
                public class DatosClienteDatos
                {
                    private DataSet ds;
                    private string _FechayHora = string.Empty;
                    private PdfPCell _CelFormaPago;
                    public string FechayHora
                    {
                        get
                        {
                            return _FechayHora;
                        }
                        set
                        {
                            _FechayHora = value;
                        }
                    }
                    //---- Celdas -------
                    public PdfPCell CelNombreCliente { get; set; }
                    public PdfPCell CelNIT { get; set; }
                    public PdfPCell CelTelefonoFax { get; set; }
                    public PdfPCell CelFormaPago
                    {
                        get
                        {
                            return _CelFormaPago;
                        }
                        set
                        {
                            _CelFormaPago = value;
                        }
                    }
                    //-------------------
                    public DatosClienteDatos(DataSet _ds)
                    {
                        ds = _ds;
                        _FechayHora = DateTime.Parse((string)ds.Tables["InvcHead"].Rows[0]["InvoiceDate"]).ToString("dd/MM/yyyy");
                    }
                }
                public class PiePaginaDatos
                {
                    private bool _Gravado = false;
                    private bool _Excluido = false;
                    private bool _Seguro = false;
                    private string _Notas = "xxxxxxxxxxxx xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx xxxxxxxxxxxxxxxxxxxxxxxxxx xxxxxxxxxxxxxxxx xxxxxxxxxxxxxxxxxxxx xxxxxxxxxx" +
                        "xxxxxxxxxxxxxxxxxxxxxxxxxxxx xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx xxxxxxxx" +
                        "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx xxxxxxxxxxxxxxxxxxxxx\n\n";
                    private string _ObservacionesCita = "Pedir cita\nS EDWIN\n\n\n\n";
                    private bool _LeyendaCopia = true;
                    private bool _BarCode01 = true;
                    private bool _BarCode02 = true;
                    private bool _NITReferencia = true;
                    private decimal _SeguroValor = 0;
                    public PdfPCell CelTotalArticulos { get; set; }
                    public bool Gravado
                    {
                        get
                        {
                            return _Gravado;
                        }
                        set
                        {
                            _Gravado = value;
                        }
                    }
                    public bool Excluido
                    {
                        get
                        {
                            return _Excluido;
                        }
                        set
                        {
                            _Excluido = value;
                        }
                    }
                    public bool Seguro
                    {
                        get
                        {
                            return _Seguro;
                        }
                        set
                        {
                            _Seguro = value;
                        }
                    }
                    public string Notas
                    {
                        get
                        {
                            return _Notas;
                        }
                        set
                        {
                            _Notas = value;
                        }
                    }
                    public string ObservacionesCita
                    {
                        get
                        {
                            return _ObservacionesCita;
                        }
                        set
                        {
                            _ObservacionesCita = value;
                        }
                    }
                    public bool LeyendaCopia
                    {
                        get
                        {
                            return _LeyendaCopia;
                        }
                        set
                        {
                            _LeyendaCopia = value;
                        }
                    }
                    public bool BarCode01
                    {
                        get
                        {
                            return _BarCode01;
                        }
                        set
                        {
                            _BarCode01 = value;
                        }
                    }
                    public bool BarCode02
                    {
                        get
                        {
                            return _BarCode02;
                        }
                        set
                        {
                            _BarCode02 = value;
                        }
                    }
                    public bool NITReferenia
                    {
                        get
                        {
                            return _NITReferencia;
                        }
                        set
                        {
                            _NITReferencia = value;
                        }
                    }
                    public decimal SeguroValor
                    {
                        get
                        {
                            return _SeguroValor;
                        }
                        set
                        {
                            _SeguroValor = value;
                        }
                    }
                }
                public static void DatosClienteTitulos(ref PdfPCell celda, string titulo, string valores)
                {
                    Chunk Titulo = new Chunk(titulo, Fuentes.Plantilla_4_fontTitle2);
                    Chunk Valor = new Chunk(valores, Fuentes.Plantilla_4_fontTitleFactura);
                    Phrase phrFrase = new Phrase();
                    phrFrase.Add(Titulo);
                    phrFrase.Add(Valor);
                    //celda.VerticalAlignment = Element.ALIGN_MIDDLE;
                    celda.AddElement(phrFrase);
                }
                public static float[] DimensionUnidades
                {
                    get
                    {
                        return _DimensionUnidades;
                    }
                    set
                    {
                        _DimensionUnidades = value;
                    }
                }
                public static PdfDiv divEspacio2
                {
                    get
                    {
                        PdfDiv divEspacio2 = new PdfDiv();
                        divEspacio2.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                        divEspacio2.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                        //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                        divEspacio2.Height = 2;
                        divEspacio2.Width = 130;
                        return divEspacio2;
                    }
                }
                private static PdfPCell AddUnidadesCelda(ref PdfPTable table,
                    string data,
                    iTextSharp.text.Font fontTitleFactura,
                    ref string strError,
                    string mensaje1,
                    string mensaje2,
                    float BorderWidthBottom,
                    float BorderWidthLeft,
                    float BorderWidthRight,
                    float BorderWidthTop,
                    int HorizontalAlignment)
                {
                    strError += mensaje1;
                    iTextSharp.text.pdf.PdfPCell celda = new iTextSharp.text.pdf.PdfPCell(new Phrase(data, fontTitleFactura));
                    celda.Colspan = 1;
                    celda.Padding = 2;
                    celda.Border = 0;
                    celda.BorderWidthBottom = BorderWidthBottom;
                    celda.BorderWidthLeft = BorderWidthLeft;
                    celda.BorderWidthRight = BorderWidthRight;
                    celda.BorderWidthTop = BorderWidthTop;
                    celda.HorizontalAlignment = HorizontalAlignment;
                    celda.VerticalAlignment = Element.ALIGN_TOP;
                    table.AddCell(celda);
                    strError += mensaje2;
                    return celda;
                }
                public static PdfPCell AddCeldasCabecera(ref PdfPTable table,
                    float BorderWidthBottom,
                    float BorderWidthLeft,
                    float BorderWidthRight,
                    float BorderWidthTop)
                {
                    iTextSharp.text.pdf.PdfPCell celda = new iTextSharp.text.pdf.PdfPCell();
                    celda.Colspan = 1;
                    celda.Padding = 2;
                    celda.Border = 0;
                    celda.BorderWidthBottom = BorderWidthBottom;
                    celda.BorderWidthLeft = BorderWidthLeft;
                    celda.BorderWidthRight = BorderWidthRight;
                    celda.BorderWidthTop = BorderWidthTop;
                    celda.VerticalAlignment = Element.ALIGN_TOP;
                    celda.MinimumHeight = Plantilla_4.DetalleHeadMinimumHeight;
                    table.AddCell(celda);
                    return celda;
                }
                private static bool AddUnidadesPlantilla4(DataRow dataLine, ref PdfPTable table, iTextSharp.text.Font fontTitleFactura, DataSet dataSet, ref string strError)
                {
                    try
                    {
                        AddUnidadesCelda(ref table, (string)dataLine["InvoiceLine"], fontTitleFactura, ref strError, "Add InvoiceLine", "InvoiceLine OK", 0, 0.5f, 0, 0, Element.ALIGN_CENTER);
                        AddUnidadesCelda(ref table, (string)dataLine["PartNum"], fontTitleFactura, ref strError, "Add PartNum", "Add PartNum OK", 0, 0.5f, 0, 0, Element.ALIGN_LEFT);
                        AddUnidadesCelda(ref table, decimal.Parse((string)dataLine["SellingShipQty"]).ToString("N"), fontTitleFactura, ref strError, "Add SellingShipQty", "Add SellingShipQty OK", 0, 0.5f, 0, 0, Element.ALIGN_RIGHT);
                        AddUnidadesCelda(ref table, (string)dataLine["SalesUM"], fontTitleFactura, ref strError, "Add SalesUM", "Add SalesUM OK", 0, 0.5f, 0, 0, Element.ALIGN_LEFT);
                        AddUnidadesCelda(ref table, (string)dataLine["LineDesc"], fontTitleFactura, ref strError, "Add LineDesc", "Add LineDesc OK", 0, 0.5f, 0, 0, Element.ALIGN_LEFT);
                        AddUnidadesCelda(ref table, decimal.Parse((string)dataLine["DocUnitPrice"]).ToString("N2"), fontTitleFactura, ref strError, "Add DocUnitPrice", "Add DocUnitPrice OK", 0, 0.5f, 0, 0, Element.ALIGN_RIGHT);
                        AddUnidadesCelda(ref table, decimal.Parse((string)dataLine["DocUnitPrice"]).ToString("N2"), fontTitleFactura, ref strError, "Add DspDocLessDiscount", "Add DspDocLessDiscount OK", 0, 0.5f, 0, 0, Element.ALIGN_RIGHT);
                        AddUnidadesCelda(ref table, decimal.Parse((string)dataLine["DocUnitPrice"]).ToString("N2"), fontTitleFactura, ref strError, "Add DspDocExtPrice", "Add DspDocExtPrice OK", 0, 0.5f, 0, 0, Element.ALIGN_RIGHT);
                        AddUnidadesCelda(ref table, decimal.Parse((string)dataLine["DocUnitPrice"]).ToString("N2"), fontTitleFactura, ref strError, "Add InvoiceNum", "Add InvoiceNum OK", 0, 0.5f, 0.5f, 0, Element.ALIGN_RIGHT);
                        return true;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                        return false;
                    }
                }
                public static void DetalleHeadEncabezados(ref PdfPTable table, string titulo, float BorderWidthLeft, float BorderWidthRight)
                {
                    PdfPCell celda = new PdfPCell(new Phrase(titulo, Helpers.Fuentes.Plantilla_4_fontTitleFactura));
                    celda.Colspan = 1;
                    celda.Padding = 3;
                    celda.Border = 0;
                    celda.BorderWidthLeft = BorderWidthLeft;
                    celda.BorderWidthRight = BorderWidthRight;
                    celda.BackgroundColor = BaseColor.LIGHT_GRAY;
                    celda.HorizontalAlignment = Element.ALIGN_CENTER;
                    celda.VerticalAlignment = Element.ALIGN_TOP;
                    table.AddCell(celda);
                }
                public static void PiePaginaTotales(ref PdfPTable table, string titulo, string valor)
                {
                    PdfPCell celdaTitulo = new PdfPCell(new Phrase(titulo, Helpers.Fuentes.Plantilla_4_fontTitle2));
                    celdaTitulo.Border = 0;
                    table.AddCell(celdaTitulo);
                    PdfPCell celdaValores = new PdfPCell(new Phrase(valor, Helpers.Fuentes.Plantilla_4_fontTitleFactura));
                    celdaValores.Border = 0;
                    celdaValores.HorizontalAlignment = Element.ALIGN_RIGHT;
                    table.AddCell(celdaValores);
                }
                //------------------- Datos de la factura ------------------------------
                public static PdfPTable Encabezado(string RutaImg, EncabezadoDatos datos)
                {
                    //Creacion de la tabla
                    PdfPTable table = new PdfPTable(5);
                    table.WidthPercentage = 100;
                    table.SetWidths(datos.Dimensiones);
                    //-----------------------------------
                    System.Drawing.Image logo = null;
                    var requestLogo = WebRequest.Create(RutaImg);

                    using (var responseLogo = requestLogo.GetResponse())
                    using (var streamLogo = responseLogo.GetResponseStream())
                    {
                        logo = Bitmap.FromStream(streamLogo);
                    }

                    iTextSharp.text.Image ImgLogo = iTextSharp.text.Image.GetInstance(logo, BaseColor.WHITE);
                    ImgLogo.ScaleAbsolute(145f, 60f);//-----
                    //divLogo.BackgroundImage = ImgLogo;
                    //----- Logo por celda -------
                    PdfPCell celLogo = new PdfPCell();
                    celLogo.AddElement(ImgLogo);
                    celLogo.VerticalAlignment = Element.ALIGN_MIDDLE;
                    //---------
                    if(!string.IsNullOrEmpty(datos.Empresa))
                    {
                        Paragraph prgTitulo = new Paragraph(datos.Empresa, Fuentes.Plantilla_4_fontTitle);
                        celLogo.AddElement(prgTitulo);
                    }
                    if(!string.IsNullOrEmpty(datos.EmpresaTexto))
                    {
                        Paragraph prgTexto = new Paragraph(datos.EmpresaTexto, Fuentes.Plantilla_4_fontTitle);
                        celLogo.AddElement(prgTexto);
                    }
                    //----------- Acerca de por celda --------------------------------------------------------------------------------------
                    PdfPCell celAcercaDe = new PdfPCell();

                    iTextSharp.text.Font fontAcercade = FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL);

                    Paragraph prgTituloAcercade1 = new Paragraph(datos.EmpresaPPal, Helpers.Fuentes.Plantilla_4_fontTitle);
                    prgTituloAcercade1.Alignment = Element.ALIGN_CENTER;
                    //divAcercade.AddElement(prgTituloAcercade1);
                    celAcercaDe.AddElement(prgTituloAcercade1);


                    Paragraph prgTituloAcercade2 = new Paragraph(datos.EmpresaPPalDireccion, Helpers.Fuentes.Plantilla_4_fontTitle2);
                    prgTituloAcercade2.Alignment = Element.ALIGN_CENTER;
                    //divAcercade.AddElement(prgTituloAcercade2);
                    celAcercaDe.AddElement(prgTituloAcercade2);
                    //-----------------------------------------------------------------------------------------------------
                    PdfPTable tableEncabezadoSedes1 = new PdfPTable(1);
                    tableEncabezadoSedes1.WidthPercentage = 100;

                    Paragraph prgTituloCedes1 = new Paragraph(datos.Sede01,Helpers.Fuentes.Plantilla_4_fontTitle);
                    prgTituloCedes1.Alignment = Element.ALIGN_CENTER;
                    Paragraph prgTelCedes1 = new Paragraph(datos.Sede01Direccion, Helpers.Fuentes.Plantilla_4_fontTitle2);
                    prgTelCedes1.Alignment = Element.ALIGN_CENTER;

                    iTextSharp.text.pdf.PdfPCell celTittleCedes1 = new iTextSharp.text.pdf.PdfPCell();
                    celTittleCedes1.AddElement(prgTituloCedes1);
                    celTittleCedes1.AddElement(prgTelCedes1);
                    celTittleCedes1.HorizontalAlignment = Element.ALIGN_CENTER;
                    celTittleCedes1.VerticalAlignment = Element.ALIGN_TOP;
                    celTittleCedes1.Border = 0;
                    tableEncabezadoSedes1.AddCell(celTittleCedes1);

                    Paragraph prgTituloCedes2 = new Paragraph(datos.Sede02, Helpers.Fuentes.Plantilla_4_fontTitle);
                    prgTituloCedes2.Alignment = Element.ALIGN_CENTER;
                    Paragraph prgTelCedes2 = new Paragraph(datos.Sede02Direccion, Helpers.Fuentes.Plantilla_4_fontTitle2);
                    prgTelCedes2.Alignment = Element.ALIGN_CENTER;

                    iTextSharp.text.pdf.PdfPCell celTittleCedes2 = new iTextSharp.text.pdf.PdfPCell();
                    celTittleCedes2.AddElement(prgTituloCedes2);
                    celTittleCedes2.AddElement(prgTelCedes2);
                    celTittleCedes2.HorizontalAlignment = Element.ALIGN_CENTER;
                    celTittleCedes2.VerticalAlignment = Element.ALIGN_TOP;
                    celTittleCedes2.Border = 0;
                    tableEncabezadoSedes1.AddCell(celTittleCedes2);

                    Paragraph prgTituloCedes3 = new Paragraph(datos.Sede03, Helpers.Fuentes.Plantilla_4_fontTitle);
                    prgTituloCedes3.Alignment = Element.ALIGN_CENTER;
                    Paragraph prgTelCedes3 = new Paragraph(datos.Sede03Direccion, Helpers.Fuentes.Plantilla_4_fontTitle2);
                    prgTelCedes3.Alignment = Element.ALIGN_CENTER;

                    iTextSharp.text.pdf.PdfPCell celTittleCedes3 = new iTextSharp.text.pdf.PdfPCell();
                    celTittleCedes3.AddElement(prgTituloCedes3);
                    celTittleCedes3.AddElement(prgTelCedes3);
                    celTittleCedes3.HorizontalAlignment = Element.ALIGN_CENTER;
                    celTittleCedes3.VerticalAlignment = Element.ALIGN_TOP;
                    celTittleCedes3.Border = 0;
                    tableEncabezadoSedes1.AddCell(celTittleCedes3);

                    //divEncabezadoCedes1.AddElement(tableEncabezadoCedes1);
                    PdfPCell celEncabezadoSedes1 = new PdfPCell();
                    celEncabezadoSedes1.AddElement(tableEncabezadoSedes1);
                    //----------------------------------------------------------------------------------------------------
                    PdfPTable tableEncabezadoCedes2 = new PdfPTable(1);
                    tableEncabezadoSedes1.WidthPercentage = 100;

                    Paragraph prgTituloCedes4 = new Paragraph(datos.Sede04, Helpers.Fuentes.Plantilla_4_fontTitle);
                    prgTituloCedes4.Alignment = Element.ALIGN_CENTER;
                    Paragraph prgTelCedes4 = new Paragraph(datos.Sede04Direccion, Helpers.Fuentes.Plantilla_4_fontTitle2);
                    prgTelCedes4.Alignment = Element.ALIGN_CENTER;

                    iTextSharp.text.pdf.PdfPCell celTittleCedes4 = new iTextSharp.text.pdf.PdfPCell();
                    celTittleCedes4.AddElement(prgTituloCedes4);
                    celTittleCedes4.AddElement(prgTelCedes4);
                    celTittleCedes4.HorizontalAlignment = Element.ALIGN_LEFT;
                    celTittleCedes4.VerticalAlignment = Element.ALIGN_TOP;
                    celTittleCedes4.Border = 0;
                    tableEncabezadoCedes2.AddCell(celTittleCedes4);

                    Paragraph prgTituloCedes5 = new Paragraph(datos.Sede05, Helpers.Fuentes.Plantilla_4_fontTitle);
                    prgTituloCedes5.Alignment = Element.ALIGN_CENTER;
                    Paragraph prgTelCedes5 = new Paragraph(datos.Sede05Direccion, Helpers.Fuentes.Plantilla_4_fontTitle2);
                    prgTelCedes5.Alignment = Element.ALIGN_CENTER;

                    iTextSharp.text.pdf.PdfPCell celTittleCedes5 = new iTextSharp.text.pdf.PdfPCell();
                    celTittleCedes5.AddElement(prgTituloCedes5);
                    celTittleCedes5.AddElement(prgTelCedes5);
                    celTittleCedes5.HorizontalAlignment = Element.ALIGN_LEFT;
                    celTittleCedes5.VerticalAlignment = Element.ALIGN_TOP;
                    celTittleCedes5.Border = 0;
                    tableEncabezadoCedes2.AddCell(celTittleCedes5);

                    Paragraph prgTituloCedes6 = new Paragraph(datos.Sede06, Helpers.Fuentes.Plantilla_4_fontTitle);
                    prgTituloCedes6.Alignment = Element.ALIGN_CENTER;
                    Paragraph prgTelCedes6 = new Paragraph(datos.Sede06Direccion, Helpers.Fuentes.Plantilla_4_fontTitle2);
                    prgTelCedes6.Alignment = Element.ALIGN_CENTER;

                    iTextSharp.text.pdf.PdfPCell celTittleCedes6 = new iTextSharp.text.pdf.PdfPCell();
                    celTittleCedes6.AddElement(prgTituloCedes6);
                    celTittleCedes6.AddElement(prgTelCedes6);
                    celTittleCedes6.HorizontalAlignment = Element.ALIGN_LEFT;
                    celTittleCedes6.VerticalAlignment = Element.ALIGN_TOP;
                    celTittleCedes6.Border = 0;
                    tableEncabezadoCedes2.AddCell(celTittleCedes6);

                    //divEncabezadoCedes2.AddElement(tableEncabezadoCedes2);
                    PdfPCell celEncabezadoSedes2 = new PdfPCell();
                    celEncabezadoSedes2.AddElement(tableEncabezadoCedes2);
                    //----------------------------------------------------------------------------------------------------
                    //iTextSharp.text.Font fontTitleFactura = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);

                    PdfPTable tableEncabezadoCertificados = new PdfPTable(1);
                    tableEncabezadoCertificados.WidthPercentage = 100;

                    Paragraph prgEncabezadoAVG = new Paragraph(datos.AVGCerCalidad, Helpers.Fuentes.Plantilla_4_fontTitleFactura);
                    prgEncabezadoAVG.Alignment = Element.ALIGN_RIGHT;

                    iTextSharp.text.pdf.PdfPCell celEncabezadoAGV = new iTextSharp.text.pdf.PdfPCell(prgEncabezadoAVG);
                    celEncabezadoAGV.HorizontalAlignment = Element.ALIGN_RIGHT;
                    celEncabezadoAGV.VerticalAlignment = Element.ALIGN_TOP;
                    celEncabezadoAGV.Border = 0;
                    tableEncabezadoCertificados.AddCell(celEncabezadoAGV);

                    PdfPCell celNumeroFactura = new PdfPCell();
                    celNumeroFactura.AddElement(tableEncabezadoCertificados);
                    //---- Preparacion de las celdas -------
                    celLogo.Padding = 0;
                    celLogo.Border = 0;
                    celAcercaDe.Padding = 0;
                    celAcercaDe.Border = 0;
                    celEncabezadoSedes1.Padding = 0;
                    celEncabezadoSedes1.Border = 0;
                    celEncabezadoSedes2.Padding = 0;
                    celEncabezadoSedes2.Border = 0;
                    celNumeroFactura.Padding = 0;
                    celNumeroFactura.Border = 0;
                    //---- Agregar celdas a la tabla -------
                    table.AddCell(celLogo);
                    table.AddCell(celAcercaDe);
                    table.AddCell(celEncabezadoSedes1);
                    table.AddCell(celEncabezadoSedes2);
                    table.AddCell(celNumeroFactura);

                    return table;
                }
                public static PdfPTable DatosCliente(DataSet DsInvoiceAR, string CUFE, System.Drawing.Image QRInvoice, DatosClienteDatos datos)
                {
                    PdfPTable tableFacturar = new PdfPTable(3);
                    //Dimenciones.
                    float[] DimencionFacturar = new float[3];
                    DimencionFacturar[0] = 1.0F;//
                    DimencionFacturar[1] = 1.0F;//
                    DimencionFacturar[2] = 1.0F;//

                    tableFacturar.WidthPercentage = 100;
                    tableFacturar.SetWidths(DimencionFacturar);
                    //----------------------------------------------------------------------------------------------
                    PdfPCell celNofactura = new PdfPCell(new Phrase(
                        $"FACTURA DE VENTA No. {(string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"]}",
                        FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 12, iTextSharp.text.Font.NORMAL)));
                    celNofactura.HorizontalAlignment = Element.ALIGN_RIGHT;
                    celNofactura.Colspan = 3;
                    celNofactura.Padding = 3;
                    celNofactura.BorderWidthLeft = 0;
                    celNofactura.BorderWidthTop = 0;
                    celNofactura.BorderWidthRight = 0;
                    tableFacturar.AddCell(celNofactura);
                    //----------------------------------------------------------------------------------------------
                    PdfPTable tableDatosCliente = new PdfPTable(1);
                    float[] DimencionFacturarA = new float[2];
                    DimencionFacturarA[0] = 1.0F;//
                    DimencionFacturarA[1] = 9.0F;//

                    tableDatosCliente.WidthPercentage = 100;
                    //-------------------------- Señores -------------------------------------------------------------------
                    PdfPCell celNombreCliente = new PdfPCell();
                    //celNombreCliente.AddElement(phrSeñores);
                    celNombreCliente.Border = 0;
                    //celNombreCliente.Padding = 0;
                    //string CustomerName = string.Empty;
                    //if(Helpers.Compartido.VerificarExistenciaColumnas(DsInvoiceAR, "Customer", "CustomerName", 0))
                    //    CustomerName = DsInvoiceAR.Tables[""]

                    DatosClienteTitulos(ref celNombreCliente, "Señores: ", DsInvoiceAR.Tables["Customer"].Rows[0]["Name"].ToString());
                    tableDatosCliente.AddCell(celNombreCliente);
                    

                    //--------------------------- NIT -------------------------------------------------------------------------

                    Chunk TituloNIT = new Chunk("NIT: ", Fuentes.Plantilla_4_fontTitle2);
                    Chunk ValorNIT = new Chunk((string)DsInvoiceAR.Tables["Customer"].Rows[0]["ResaleID"] + "\r\n", Fuentes.Plantilla_4_fontTitleFactura);
                    Phrase phrNIT = new Phrase();
                    phrNIT.Add(TituloNIT);
                    phrNIT.Add(ValorNIT);
                    PdfPCell celNit = new PdfPCell();
                    celNit.AddElement(phrNIT);
                    celNit.Border = 0;
                    tableDatosCliente.AddCell(celNit);
                    //------------------- Teléfono y Fax --------------------------------------------------------------------------------------------------
                    PdfPTable tableTel = new PdfPTable(2);
                    tableTel.WidthPercentage = 100;
                    tableTel.SetWidths(new float[] {1.5f, 1f });
                    tableTel.PaddingTop = 0;

                    Chunk TituloTelefono = new Chunk("Teléfono: ", Fuentes.Plantilla_4_fontTitle2);
                    string PhoneNum = (string)DsInvoiceAR.Tables["Customer"].Rows[0]["PhoneNum"];
                    PhoneNum = PhoneNum.Replace(Convert.ToChar(13), ' ');
                    PhoneNum = PhoneNum.Replace(Convert.ToChar(10), ' ');
                    Chunk ValorTelefono = new Chunk((string)DsInvoiceAR.Tables["Customer"].Rows[0]["PhoneNum"], Fuentes.Plantilla_4_fontTitleFactura);
                    Phrase phrTelefono = new Phrase();
                    phrTelefono.Add(TituloTelefono);
                    phrTelefono.Add(ValorTelefono);
                    PdfPCell celValTelefonoCliente = new PdfPCell();
                    celValTelefonoCliente.AddElement(phrTelefono);
                    celValTelefonoCliente.Colspan = 1;
                    //celValTelefonoCliente.Padding = 0;
                    celValTelefonoCliente.Border = 0;
                    celValTelefonoCliente.HorizontalAlignment = Element.ALIGN_LEFT;
                    celValTelefonoCliente.VerticalAlignment = Element.ALIGN_TOP;
                    tableTel.AddCell(celValTelefonoCliente);

                    Chunk TituloFax = new Chunk("Fax: ", Fuentes.Plantilla_4_fontTitle2);
                    string FaxNum = string.Empty;
                    if (Helpers.Compartido.VerificarExistenciaColumnas(DsInvoiceAR, "Customer", "FaxNum", 0))
                        FaxNum = DsInvoiceAR.Tables["Customer"].Rows[0]["FaxNum"].ToString();
                    Chunk ValorFax = new Chunk(FaxNum, Fuentes.Plantilla_4_fontTitleFactura);
                    Phrase phrFax = new Phrase();
                    phrFax.Add(TituloFax);
                    phrFax.Add(ValorFax);
                    PdfPCell celValFaxCliente = new PdfPCell();
                    celValFaxCliente.AddElement(phrFax);
                    celValFaxCliente.Colspan = 1;
                    celValFaxCliente.Padding = 0;
                    celValFaxCliente.Border = 0;
                    celValFaxCliente.HorizontalAlignment = Element.ALIGN_LEFT;
                    celValFaxCliente.VerticalAlignment = Element.ALIGN_TOP;
                    tableTel.AddCell(celValFaxCliente);

                    PdfPCell celTel = new PdfPCell(tableTel);
                    celTel.Border = 0;
                    tableDatosCliente.AddCell(celTel);
                    //--------- Evaluando forma de pago -------
                    if(datos.CelFormaPago != null)
                    {
                        tableDatosCliente.AddCell(datos.CelFormaPago);
                    }
                    //---------------- Domicilio principal -----------------------------------------------------------------------------------
                    Chunk TituloDomicilioPPal = new Chunk("Domicilio Ppal: ", Fuentes.Plantilla_4_fontTitle2);
                    Chunk ValorDomicilioPPal = new Chunk(DsInvoiceAR.Tables["Customer"].Rows[0]["Address1"].ToString(), Fuentes.Plantilla_4_fontTitleFactura);
                    Phrase phrDomicilioPPal = new Phrase();
                    phrDomicilioPPal.Add(TituloDomicilioPPal);
                    phrDomicilioPPal.Add(ValorDomicilioPPal);

                    PdfPCell celDomicilioPpal = new PdfPCell(); //new PdfPCell(tableDomicilioPpal);
                    celDomicilioPpal.AddElement(phrDomicilioPPal);
                    celDomicilioPpal.Border = 0;
                    tableDatosCliente.AddCell(celDomicilioPpal);
                    //-------------------- Domicilio Embarque ------------------------------------------------------------------------------------
                    PdfPCell celDomicilioEmbarque = new PdfPCell(); //new PdfPCell(tableDomicilioEmbarque);
                    string DomilicioEmbarque = string.Empty;
                    if (Helpers.Compartido.VerificarExistenciaColumnas(DsInvoiceAR, "InvcHead", "Character03", 0))
                        DomilicioEmbarque = DsInvoiceAR.Tables["InvcHead"].Rows[0]["Character03"].ToString();
                    DatosClienteTitulos(ref celDomicilioEmbarque, "Domicilio Embarque: ", DomilicioEmbarque);
                    celDomicilioEmbarque.Border = 0;
                    tableDatosCliente.AddCell(celDomicilioEmbarque);
                    //--------------------------------------------------------------------------------------------------------------
                    iTextSharp.text.pdf.PdfPCell celdatosCliente = new iTextSharp.text.pdf.PdfPCell(tableDatosCliente);
                    celdatosCliente.Colspan = 1;
                    tableFacturar.AddCell(celdatosCliente);
                    //--------------------------------------------------------------------------------------------------------------
                    iTextSharp.text.Font fontTitleFecha = FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL);
                    iTextSharp.text.Font fontDMA = FontFactory.GetFont(FontFactory.HELVETICA, 6, BaseColor.GRAY);
                    iTextSharp.text.Font fontDetalleFecha = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 8, iTextSharp.text.Font.NORMAL);
                    //--------------------------------------------------------------------------------------------------------------
                    PdfPTable tableToCel2 = new PdfPTable(2);
                    float[] DimencionDespacharA = new float[2];
                    DimencionDespacharA[0] = 1.0F;//
                    DimencionDespacharA[1] = 1.0F;//

                    tableToCel2.WidthPercentage = 100;
                    tableToCel2.SetWidths(DimencionDespacharA);

                    PdfPTable tableFechaEmision = new PdfPTable(1);

                    iTextSharp.text.pdf.PdfPCell celTextFechaEmision = new iTextSharp.text.pdf.PdfPCell(new Phrase("Fecha Emisión", fontTitleFecha));
                    celTextFechaEmision.Colspan = 1;
                    //celTextFechaEmision.Padding = 3;
                    celTextFechaEmision.Border = 0;
                    celTextFechaEmision.HorizontalAlignment = Element.ALIGN_CENTER;
                    celTextFechaEmision.VerticalAlignment = Element.ALIGN_TOP;
                    tableFechaEmision.AddCell(celTextFechaEmision);

                    iTextSharp.text.pdf.PdfPCell celDMA = new iTextSharp.text.pdf.PdfPCell(new Phrase("DD/MM/AAAA", fontDMA));
                    celDMA.Colspan = 1;
                    //celDMA.Padding = 3;
                    celDMA.Border = 0;
                    celDMA.HorizontalAlignment = Element.ALIGN_CENTER;
                    celDMA.VerticalAlignment = Element.ALIGN_TOP;
                    tableFechaEmision.AddCell(celDMA);

                    //------ Dato fecha y hora ------
                    //iTextSharp.text.pdf.PdfPCell celFechaEmisionVal = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    //    DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).ToString("dd/MM/yyyy"), fontDetalleFecha));
                    iTextSharp.text.pdf.PdfPCell celFechaEmisionVal = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                        datos.FechayHora, fontDetalleFecha));
                    celFechaEmisionVal.Colspan = 1;
                    //celFechaEmisionVal.Padding = 3;
                    celFechaEmisionVal.Border = 0;
                    celFechaEmisionVal.HorizontalAlignment = Element.ALIGN_CENTER;
                    celFechaEmisionVal.VerticalAlignment = Element.ALIGN_TOP;
                    tableFechaEmision.AddCell(celFechaEmisionVal);
                    PdfPCell celFechaEmision = new PdfPCell(tableFechaEmision);
                    tableToCel2.AddCell(celFechaEmision);

                    //---------------------------------------------------------------------------------------------------------------
                    PdfPTable tableFechaDetalleVencimiento = new PdfPTable(1);

                    iTextSharp.text.pdf.PdfPCell celTextDetalleFechaVencimiento = new iTextSharp.text.pdf.PdfPCell(new Phrase("Fecha Vencimiento", fontTitleFecha));
                    celTextDetalleFechaVencimiento.Colspan = 1;
                    //celTextDetalleFechaVencimiento.Padding = 3;
                    celTextDetalleFechaVencimiento.Border = 0;
                    celTextDetalleFechaVencimiento.HorizontalAlignment = Element.ALIGN_CENTER;
                    celTextDetalleFechaVencimiento.VerticalAlignment = Element.ALIGN_TOP;
                    tableFechaDetalleVencimiento.AddCell(celTextDetalleFechaVencimiento);

                    tableFechaDetalleVencimiento.AddCell(celDMA);

                    iTextSharp.text.pdf.PdfPCell celFechaVencimientoVal = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                        DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"]).ToString("dd/MM/yyyy"), fontDetalleFecha));
                    celFechaVencimientoVal.Colspan = 1;
                    //celFechaVencimientoVal.Padding = 3;
                    celFechaVencimientoVal.Border = 0;
                    celFechaVencimientoVal.HorizontalAlignment = Element.ALIGN_CENTER;
                    celFechaVencimientoVal.VerticalAlignment = Element.ALIGN_TOP;
                    tableFechaDetalleVencimiento.AddCell(celFechaVencimientoVal);

                    PdfPCell celFechaDetalleVencimiento = new PdfPCell(tableFechaDetalleVencimiento);
                    tableToCel2.AddCell(celFechaDetalleVencimiento);
                    //-------------------------------------------------------------------------------------------------------------

                    iTextSharp.text.pdf.PdfPCell celNoPedido = new PdfPCell();
                    DatosClienteTitulos(ref celNoPedido, "Pedido No: ", DsInvoiceAR.Tables["InvcHead"].Rows[0]["OrderNum"].ToString());
                    tableToCel2.AddCell(celNoPedido);
                    //-------------------------------------------------------------------------------------
                    iTextSharp.text.pdf.PdfPCell celOrderCompra = new iTextSharp.text.pdf.PdfPCell();
                    string PONum = string.Empty;
                    if (Helpers.Compartido.VerificarExistenciaColumnas(DsInvoiceAR, "InvcHead", "PONum", 0))
                        PONum = (string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["PONum"];
                    DatosClienteTitulos(ref celOrderCompra, "Orden de Compra: ", PONum);
                    tableToCel2.AddCell(celOrderCompra);
                    //-------------------------------------------------------------------------------------

                    PdfPCell celVendededor = new PdfPCell();//new PdfPCell(tableVendedor);
                    DatosClienteTitulos(ref celVendededor, "Vendedor: ", DsInvoiceAR.Tables["InvcHead"].Rows[0]["SalesRepName1"].ToString());
                    celVendededor.Colspan = 2;
                    tableToCel2.AddCell(celVendededor);
                    //-------------------------------------------------------------------------------------

                    PdfPCell celCodigoVendededor = new PdfPCell();//new PdfPCell(tableCodigoVendedor);
                    DatosClienteTitulos(ref celCodigoVendededor, "Código Vendedor: ", DsInvoiceAR.Tables["InvcHead"].Rows[0]["SalesRepCode1"].ToString());
                    celCodigoVendededor.Colspan = 2;
                    tableToCel2.AddCell(celCodigoVendededor);
                    //-------------------------------------------------------------------------------------
                    iTextSharp.text.pdf.PdfPCell cel2 = new iTextSharp.text.pdf.PdfPCell(tableToCel2);
                    cel2.Colspan = 1;
                    tableFacturar.AddCell(cel2);


                    /*CELL QR*/
                    Paragraph prgCufe = new Paragraph("CUFE: " + CUFE, Helpers.Fuentes.Plantilla_4_fontTitle2);
                    prgCufe.Alignment = Element.ALIGN_LEFT;

                    iTextSharp.text.Image QRPdf = iTextSharp.text.Image.GetInstance(QRInvoice, BaseColor.WHITE);
                    QRPdf.ScaleAbsolute(90f, 90f);
                    QRPdf.Alignment = Element.ALIGN_CENTER;
                    //iTextSharp.text.pdf.PdfPCell celImgQR = new iTextSharp.text.pdf.PdfPCell(QRPdf);
                    iTextSharp.text.pdf.PdfPCell celImgQR = new iTextSharp.text.pdf.PdfPCell();
                    celImgQR.AddElement(QRPdf);
                    celImgQR.AddElement(prgCufe);
                    celImgQR.Colspan = 2;
                    celImgQR.Padding = 3;
                    celImgQR.Border = 0;
                    celImgQR.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                    celImgQR.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    tableFacturar.AddCell(celImgQR);
                    //----- Retornando tabla
                    return tableFacturar;
                }
                public static PdfPTable DetalleHead()
                {
                    PdfPTable tableTituloUnidades = new PdfPTable(9);

                    tableTituloUnidades.WidthPercentage = 100;
                    tableTituloUnidades.SetWidths(DimensionUnidades);
                    //---- Agregando los encabezados -----------------------------
                    DetalleHeadEncabezados(ref tableTituloUnidades, "L", 0.5f, 0);
                    DetalleHeadEncabezados(ref tableTituloUnidades, "Código", 0.5f, 0);
                    DetalleHeadEncabezados(ref tableTituloUnidades, "Cantidad", 0.5f, 0);
                    DetalleHeadEncabezados(ref tableTituloUnidades, "Unidad", 0.5f, 0);
                    DetalleHeadEncabezados(ref tableTituloUnidades, "Descripción", 0.5f, 0);
                    DetalleHeadEncabezados(ref tableTituloUnidades, "Valor Unitario", 0.5f, 0);
                    DetalleHeadEncabezados(ref tableTituloUnidades, "Descuento", 0.5f, 0);
                    DetalleHeadEncabezados(ref tableTituloUnidades, "Valor Total", 0.5f, 0);
                    DetalleHeadEncabezados(ref tableTituloUnidades, "% IVA", 0.5f, 0.5f);
                    //---- Agregando las celdas fijas --------
                    AddCeldasCabecera(ref tableTituloUnidades, 0, 0.5f, 0, 0); //Linea
                    AddCeldasCabecera(ref tableTituloUnidades, 0, 0.5f, 0, 0); //Código
                    AddCeldasCabecera(ref tableTituloUnidades, 0, 0.5f, 0, 0); //Cantidad
                    AddCeldasCabecera(ref tableTituloUnidades, 0, 0.5f, 0, 0); //Unidad
                    AddCeldasCabecera(ref tableTituloUnidades, 0, 0.5f, 0, 0); //Descripcion
                    AddCeldasCabecera(ref tableTituloUnidades, 0, 0.5f, 0, 0); //Valor unitario
                    AddCeldasCabecera(ref tableTituloUnidades, 0, 0.5f, 0, 0); //Descuento
                    AddCeldasCabecera(ref tableTituloUnidades, 0, 0.5f, 0, 0); //Valor total
                    AddCeldasCabecera(ref tableTituloUnidades, 0, 0.5f, 0.5f, 0); // % IVA
                    //--- retorno de la tabla
                    return tableTituloUnidades;
                }
                public static PdfPTable Detalles(DataSet DsInvoiceAR, ref string strError)
                {
                    //----- Detalles ----------
                    PdfPTable tableUnidades = new PdfPTable(9);
                    tableUnidades.WidthPercentage = 100;
                    tableUnidades.SetWidths(DimensionUnidades);
                    decimal totalDescuento = 0;
                    foreach (DataRow InvoiceLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
                    {
                        AddUnidadesPlantilla4(InvoiceLine, ref tableUnidades, Helpers.Fuentes.Plantilla_4_fontCustom, DsInvoiceAR, ref strError);
                        if(Helpers.Compartido.VerificarExistenciaColumnas(DsInvoiceAR, "InvcDtl", "DspDocDiscount"))
                            totalDescuento += decimal.Parse((string)InvoiceLine["DspDocDiscount"]);
                    }
                    return tableUnidades;
                }
                public static PdfPTable PiePagina(DataSet DsInvoiceAR, PiePaginaDatos datos)
                {
                    //----- Variables
                    string InvcHeadNumber01 = string.Empty;
                    string InvcHeadNumber02 = string.Empty;
                    string InvcHeadNumber05 = string.Empty;
                    //------ Asignación de variables ----
                    if (Helpers.Compartido.VerificarExistenciaColumnas(DsInvoiceAR, "InvcHead", "Number01", 0))
                        InvcHeadNumber01 = decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number01"]).ToString("N2");
                    if (Helpers.Compartido.VerificarExistenciaColumnas(DsInvoiceAR, "InvcHead", "Number02", 0))
                        InvcHeadNumber02 = decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number02"]).ToString("N2");
                    if(Helpers.Compartido.VerificarExistenciaColumnas(DsInvoiceAR, "InvcHead","Number05",0))
                        InvcHeadNumber05 = decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number05"]).ToString("N2");
                    //--------------

                    decimal totalDescuento = 0;
                    foreach (DataRow InvoiceLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
                    {
                        if (Helpers.Compartido.VerificarExistenciaColumnas(DsInvoiceAR, "InvcDtl", "DspDocDiscount"))
                            totalDescuento += decimal.Parse((string)InvoiceLine["DspDocDiscount"]);
                    }

                    PdfPTable tableObsTotales = new PdfPTable(3);
                    tableObsTotales.WidthPercentage = 100;
                    float[] dimTableObsTotales = new float[3];
                    dimTableObsTotales[0] = 2.0f;
                    dimTableObsTotales[1] = 0.8f;
                    dimTableObsTotales[2] = 0.85f;
                    tableObsTotales.SetWidths(dimTableObsTotales);
                    //---------------------------------------------------------------------
                    PdfPTable tableObsercaciones = new PdfPTable(1);

                    if (datos.CelTotalArticulos != null)
                    {
                        tableObsercaciones.AddCell(datos.CelTotalArticulos);
                    }

                    PdfPCell celTextObservaciones = new PdfPCell(new Phrase("OBSERVACIONES\n\n\n", Helpers.Fuentes.Plantilla_4_fontTitleFactura));
                    celTextObservaciones.Border = 0;
                    tableObsercaciones.AddCell(celTextObservaciones);

                    PdfPCell celTextPedirCita = new PdfPCell(new Phrase(datos.ObservacionesCita, Helpers.Fuentes.Plantilla_4_fontTitle2));
                    celTextPedirCita.Border = 0;
                    tableObsercaciones.AddCell(celTextPedirCita);

                    PdfPCell celObservaciones = new PdfPCell(tableObsercaciones);
                    tableObsTotales.AddCell(celObservaciones);
                    //----------------------------------------------------------------------
                    PdfPCell celFimarSello = new PdfPCell(new Phrase("\n\n\n\n_______________________________\nFirma y Sello", Helpers.Fuentes.Plantilla_4_fontTitle2));
                    celFimarSello.HorizontalAlignment = Element.ALIGN_CENTER;
                    celFimarSello.VerticalAlignment = Element.ALIGN_BOTTOM;
                    tableObsTotales.AddCell(celFimarSello);
                    //----------------------------------------------------------------------
                    PdfPTable tableTotales = new PdfPTable(2);
                    float[] dimTableTotales = new float[2];
                    dimTableTotales[0] = 1.1f;
                    dimTableTotales[1] = 1.7f;
                    tableTotales.SetWidths(dimTableTotales);

                    PiePaginaTotales(ref tableTotales, "Total Bruto", decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubTotal"]).ToString("N2"));

                    //PiePaginaTotales(ref tableTotales, "Descuento", totalDescuento.ToString("N2"));
                    PiePaginaTotales(ref tableTotales, "Descuento", InvcHeadNumber05);

                    PiePaginaTotales(ref tableTotales, "Valor Fletes", "xxxx");

                    PiePaginaTotales(ref tableTotales, "Subtotal", decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubTotal"]).ToString("N2"));

                    var Iva = Helpers.Compartido.GetValImpuestoByID("01", DsInvoiceAR);
                    PiePaginaTotales(ref tableTotales, "Valor IVA", Iva.ToString("N2"));

                    PiePaginaTotales(ref tableTotales, "Rete IVA 15%", "xxxxx");
                    if(datos.Gravado)
                        PiePaginaTotales(ref tableTotales, "Gravado", InvcHeadNumber01);
                    if(datos.Excluido)
                        PiePaginaTotales(ref tableTotales, "Excluido", InvcHeadNumber02);
                    if(datos.Seguro)
                        PiePaginaTotales(ref tableTotales, "Seguro", datos.SeguroValor.ToString("N2"));

                    PdfPCell celTotales = new PdfPCell(tableTotales);
                    celTotales.Padding = 2;
                    tableObsTotales.AddCell(celTotales);

                    PdfPCell celTotalLetras = new PdfPCell(new Phrase(
                        $"Son: **{Helpers.Compartido.Nroenletras((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"])}**", Helpers.Fuentes.Plantilla_4_fontTitle2));
                    celTotalLetras.Colspan = 2;
                    tableObsTotales.AddCell(celTotalLetras);

                    PdfPTable tableTotalFactura = new PdfPTable(2);
                    tableTotalFactura.SetWidths(dimTableTotales);

                    PdfPCell celTextTotal = new PdfPCell(new Phrase("Total Factura", Helpers.Fuentes.Plantilla_4_fontTitleFactura));
                    celTextTotal.BackgroundColor = BaseColor.LIGHT_GRAY;
                    celTextTotal.Border = 0;
                    tableTotalFactura.AddCell(celTextTotal);

                    PdfPCell celValTotal = new PdfPCell(new Phrase(
                        decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"]).ToString("N2"),
                        Helpers.Fuentes.Plantilla_4_fontTitleFactura));
                    celValTotal.HorizontalAlignment = Element.ALIGN_RIGHT;
                    celValTotal.BackgroundColor = BaseColor.LIGHT_GRAY;
                    celValTotal.Border = 0;
                    tableTotalFactura.AddCell(celValTotal);

                    PdfPCell celTotal = new PdfPCell(tableTotalFactura);
                    tableObsTotales.AddCell(celTotal);
                    //---------------------------------------------------------------------------------------------

                    PdfPTable tableResolucion = new PdfPTable(1);
                    tableResolucion.WidthPercentage = 100;

                    PdfPCell celResolucion = new PdfPCell(new Phrase(datos.Notas, Helpers.Fuentes.Plantilla_4_fontTitle2));
                    celResolucion.HorizontalAlignment = Element.ALIGN_CENTER;
                    celResolucion.Border = 0;
                    tableResolucion.AddCell(celResolucion);
                    //------------------------------------------------------------------------------------------------------------
                    if(datos.LeyendaCopia)
                    {
                        PdfPTable tableTextCopia = new PdfPTable(3);
                        tableTextCopia.WidthPercentage = 100;

                        PdfPCell celTextNumRot = new PdfPCell(new Phrase("NumRot 2.0 Version:Factura30 / FAcx20170213.ps",
                            FontFactory.GetFont(FontFactory.HELVETICA, 5, iTextSharp.text.Font.NORMAL)));
                        celTextNumRot.PaddingTop = 4;
                        celTextNumRot.BorderWidthRight = PdfPCell.NO_BORDER;
                        celTextNumRot.BorderWidthLeft = PdfPCell.NO_BORDER;
                        tableTextCopia.AddCell(celTextNumRot);

                        PdfPCell celTextCopia = new PdfPCell(new Phrase("- COPIA -", Helpers.Fuentes.Plantilla_4_fontTitleFactura));
                        celTextCopia.PaddingTop = 4;
                        celTextCopia.BorderWidthRight = PdfPCell.NO_BORDER;
                        celTextCopia.BorderWidthLeft = PdfPCell.NO_BORDER;
                        celTextCopia.HorizontalAlignment = Element.ALIGN_CENTER;
                        tableTextCopia.AddCell(celTextCopia);

                        PdfPCell celTextPagina = new PdfPCell(new Phrase(" "));
                        celTextPagina.BorderWidthRight = PdfPCell.NO_BORDER;
                        celTextPagina.BorderWidthLeft = PdfPCell.NO_BORDER;
                        tableTextCopia.AddCell(celTextPagina);

                        //----- celda de agregado ---------------
                        PdfPCell celCopia = new PdfPCell();
                        celCopia.AddElement(tableTextCopia);
                        celCopia.Border = 0;
                        tableResolucion.AddCell(celCopia);
                    }
                    //---------------------------------------------------------------------------------------
                    PdfPTable tableTextNitReferencia = new PdfPTable(2);
                    if (datos.NITReferenia)
                    {
                        float[] dimTableTextNitReferencia = new float[2];
                        dimTableTextNitReferencia[0] = 1.068f;
                        dimTableTextNitReferencia[1] = 1.15f;
                        tableTextNitReferencia.SetWidths(dimTableTextNitReferencia);
                        tableTextNitReferencia.WidthPercentage = 100;

                        PdfPCell celTextNitReferenciae = new PdfPCell(new Phrase(" ", Helpers.Fuentes.Plantilla_4_fontTitle));
                        celTextNitReferenciae.HorizontalAlignment = Element.ALIGN_RIGHT;
                        celTextNitReferenciae.Colspan = 1;
                        celTextNitReferenciae.Border = PdfPCell.NO_BORDER;
                        tableTextNitReferencia.AddCell(celTextNitReferenciae);

                        PdfPCell celTextNitReferencia = new PdfPCell(new Phrase(
                            $"ABRASIVOS DE COLOMBIA S.A NIT.{(string)DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"]}" +
                            $"   Referencia 1 No. xxxxx - Referencia 2 No. {(string)DsInvoiceAR.Tables["Customer"].Rows[0]["ResaleID"]}",
                            FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 6f, iTextSharp.text.Font.NORMAL)));
                        celTextNitReferencia.HorizontalAlignment = Element.ALIGN_LEFT;
                        celTextNitReferencia.Colspan = 1;
                        celTextNitReferencia.Border = PdfPCell.NO_BORDER;
                        celTextNitReferencia.BorderWidthTop = 0.5f;
                        tableTextNitReferencia.AddCell(celTextNitReferencia);
                    }
                    //---------------------------------------------------------------------------------------
                    PdfPTable tableBC1 = new PdfPTable(3);
                    tableBC1.HorizontalAlignment = Element.ALIGN_LEFT;
                    float[] dimTableBC1 = new float[3];
                    dimTableBC1[0] = 1.8f;
                    dimTableBC1[1] = 1.0f;
                    dimTableBC1[2] = 0.95f;
                    tableBC1.SetWidths(dimTableBC1);
                    tableBC1.WidthPercentage = 100;

                    PdfPCell celBC1 = new PdfPCell(new Phrase("Bar Code"));
                    celBC1.Border = PdfPCell.NO_BORDER;
                    tableBC1.AddCell(celBC1);

                    PdfPTable tableDatosBC1 = new PdfPTable(2);
                    float[] dimTableDatosBC1 = new float[2];
                    dimTableDatosBC1[0] = 1.3f;
                    dimTableDatosBC1[1] = 0.7f;
                    tableDatosBC1.SetWidths(dimTableDatosBC1);
                    //----------------------------------------------------------------------------------
                    PdfPTable tableDatosComprador = new PdfPTable(2);
                    float[] dimTableDatosComprador = new float[2];
                    dimTableDatosComprador[0] = 0.8f;
                    dimTableDatosComprador[1] = 1.5f;
                    tableDatosComprador.SetWidths(dimTableDatosComprador);

                    PdfPCell celprgComprador = new PdfPCell();
                    celprgComprador.AddElement(new Paragraph("COMPRADOR:", Helpers.Fuentes.Plantilla_4_fontTitle2));
                    celprgComprador.AddElement(new Paragraph((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["CustomerName"]
                        , FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 5, iTextSharp.text.Font.NORMAL)));
                    celprgComprador.Border = PdfPCell.NO_BORDER;
                    celprgComprador.BorderWidthLeft = 0.5f;
                    celprgComprador.Colspan = 2;
                    //celprgComprador.Padding = 3;
                    tableDatosComprador.AddCell(celprgComprador);

                    PdfPCell celprgNit = new PdfPCell();
                    celprgNit.AddElement(new Paragraph("NIT./CC. ", Helpers.Fuentes.Plantilla_4_fontTitle2));
                    celprgNit.Border = PdfPCell.NO_BORDER;
                    celprgNit.BorderWidthLeft = 0.5f;
                    celprgNit.BorderWidthBottom = 0.5f;
                    celprgNit.Colspan = 1;
                    //celprgNit.Padding = 3;
                    tableDatosComprador.AddCell(celprgNit);

                    PdfPCell celprgValNit = new PdfPCell();
                    celprgValNit.AddElement(new Phrase((string)DsInvoiceAR.Tables["Customer"].Rows[0]["ResaleID"], Helpers.Fuentes.Plantilla_4_fontTitleFactura));
                    celprgValNit.Border = PdfPCell.NO_BORDER;
                    celprgValNit.BorderWidthBottom = 0.5f;
                    celprgValNit.Colspan = 1;
                    celprgValNit.Padding = 3;
                    tableDatosComprador.AddCell(celprgValNit);

                    PdfPCell celEspacionDatosComprador = new PdfPCell(new Phrase(" "));
                    celEspacionDatosComprador.Colspan = 2;
                    celprgValNit.BorderWidthBottom = 0.5f;
                    celEspacionDatosComprador.Border = PdfPCell.NO_BORDER;
                    tableDatosComprador.AddCell(celEspacionDatosComprador);

                    PdfPCell celDatosComprador = new PdfPCell(tableDatosComprador);
                    celDatosComprador.BorderWidthBottom = PdfPCell.NO_BORDER;
                    celDatosComprador.BorderWidthRight = PdfPCell.NO_BORDER;
                    celDatosComprador.BorderWidthLeft = PdfPCell.NO_BORDER;
                    tableDatosBC1.AddCell(celDatosComprador);
                    //----------------------------------------------------------------------------------
                    PdfPTable tableTotalEfectivo = new PdfPTable(1);
                    Paragraph prgTotalEfectivo = new Paragraph("Total Efectivo\n",
                        FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL));

                    PdfDiv divTotalEfectivo = new iTextSharp.text.pdf.PdfDiv();
                    divTotalEfectivo.Float = iTextSharp.text.pdf.PdfDiv.FloatType.LEFT;
                    divTotalEfectivo.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                    divTotalEfectivo.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                    divTotalEfectivo.Height = 21.2f;
                    divTotalEfectivo.Width = 49f;
                    divTotalEfectivo.PaddingTop = 5;
                    divTotalEfectivo.BackgroundColor = BaseColor.LIGHT_GRAY;

                    PdfPCell celTextTotalEfectivo = new PdfPCell();
                    celTextTotalEfectivo.AddElement(prgTotalEfectivo);
                    celTextTotalEfectivo.BorderWidthBottom = PdfPCell.NO_BORDER;
                    celTextTotalEfectivo.VerticalAlignment = Element.ALIGN_LEFT;
                    celTextTotalEfectivo.HorizontalAlignment = Element.ALIGN_TOP;
                    tableTotalEfectivo.AddCell(celTextTotalEfectivo);

                    PdfPCell celDivTotalEfectivo = new PdfPCell();
                    celDivTotalEfectivo.AddElement(divTotalEfectivo);
                    celDivTotalEfectivo.BorderWidthTop = PdfPCell.NO_BORDER;
                    tableTotalEfectivo.AddCell(celDivTotalEfectivo);

                    PdfPCell celEspacioTotalEfectivo = new PdfPCell(new Phrase(" "));
                    celEspacioTotalEfectivo.Border = PdfPCell.NO_BORDER;
                    tableTotalEfectivo.AddCell(celEspacioTotalEfectivo);

                    PdfPCell celTotalEfectivo = new PdfPCell(tableTotalEfectivo);
                    celTotalEfectivo.BorderWidthBottom = PdfPCell.NO_BORDER;
                    celTotalEfectivo.BorderWidthLeft = PdfPCell.NO_BORDER;
                    celTotalEfectivo.BorderWidthRight = PdfPCell.NO_BORDER;
                    tableDatosBC1.AddCell(celTotalEfectivo);
                    //----------------------------------------------------------------------------------
                    #region DatosConsignarEn
                    PdfPTable tableDatosConsignacion = new PdfPTable(2);
                    tableDatosConsignacion.HorizontalAlignment = Element.ALIGN_RIGHT;
                    tableDatosConsignacion.WidthPercentage = 100;
                    float[] dimTableDatosConsignacion = new float[2];
                    dimTableDatosConsignacion[0] = 1.0f;
                    dimTableDatosConsignacion[1] = 1.5f;
                    tableDatosConsignacion.SetWidths(dimTableDatosConsignacion);

                    PdfPCell celConsignarEn1 = new PdfPCell(new Phrase(
                        "Favor Consignar en:", FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 6.7f, iTextSharp.text.Font.NORMAL)));
                    celConsignarEn1.VerticalAlignment = Element.ALIGN_LEFT;
                    celConsignarEn1.HorizontalAlignment = Element.ALIGN_TOP;
                    celConsignarEn1.Border = PdfPCell.NO_BORDER;
                    celConsignarEn1.Colspan = 2;
                    tableDatosConsignacion.AddCell(celConsignarEn1);

                    PdfPCell celTextBancolombia = new PdfPCell(new Phrase("Bancolombia", FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL)));
                    celTextBancolombia.VerticalAlignment = Element.ALIGN_LEFT;
                    celTextBancolombia.HorizontalAlignment = Element.ALIGN_TOP;
                    celTextBancolombia.Border = PdfPCell.NO_BORDER;
                    celTextBancolombia.Padding = 2;
                    celTextBancolombia.Colspan = 1;
                    tableDatosConsignacion.AddCell(celTextBancolombia);

                    PdfPCell celCtaBancolombia = new PdfPCell(new Phrase("Cta. Cte. 009991132702", FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL)));
                    celCtaBancolombia.VerticalAlignment = Element.ALIGN_LEFT;
                    celCtaBancolombia.Padding = 2;
                    celCtaBancolombia.HorizontalAlignment = Element.ALIGN_TOP;
                    celCtaBancolombia.Border = PdfPCell.NO_BORDER;
                    celCtaBancolombia.Colspan = 1;
                    tableDatosConsignacion.AddCell(celCtaBancolombia);

                    PdfPCell celTextBancoBogota = new PdfPCell(new Phrase("Banco de Bogota", FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL)));
                    celTextBancoBogota.VerticalAlignment = Element.ALIGN_LEFT;
                    celTextBancoBogota.HorizontalAlignment = Element.ALIGN_TOP;
                    celTextBancoBogota.Border = PdfPCell.NO_BORDER;
                    celTextBancoBogota.Padding = 2;
                    celTextBancoBogota.Colspan = 1;
                    tableDatosConsignacion.AddCell(celTextBancoBogota);

                    PdfPCell celCtaBancoBogota = new PdfPCell(new Phrase("Cta. Cte. 349019737", FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL)));
                    celCtaBancoBogota.VerticalAlignment = Element.ALIGN_LEFT;
                    celCtaBancoBogota.HorizontalAlignment = Element.ALIGN_TOP;
                    celCtaBancoBogota.Border = PdfPCell.NO_BORDER;
                    celCtaBancoBogota.Padding = 2;
                    celCtaBancoBogota.Colspan = 1;
                    tableDatosConsignacion.AddCell(celCtaBancoBogota);

                    PdfPCell celTextBancoBBVA = new PdfPCell(new Phrase("Banco BBVA", FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL)));
                    celTextBancoBBVA.VerticalAlignment = Element.ALIGN_LEFT;
                    celTextBancoBBVA.HorizontalAlignment = Element.ALIGN_TOP;
                    celTextBancoBBVA.Border = PdfPCell.NO_BORDER;
                    celTextBancoBBVA.Padding = 2;
                    celTextBancoBBVA.Colspan = 1;
                    tableDatosConsignacion.AddCell(celTextBancoBBVA);

                    PdfPCell celCtaBancoBBVA = new PdfPCell(new Phrase("Cta. Cte. 498000488", FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL)));
                    celCtaBancoBBVA.VerticalAlignment = Element.ALIGN_LEFT;
                    celCtaBancoBBVA.HorizontalAlignment = Element.ALIGN_TOP;
                    celCtaBancoBBVA.Border = PdfPCell.NO_BORDER;
                    celCtaBancoBBVA.Padding = 2;
                    celCtaBancoBBVA.Colspan = 1;
                    tableDatosConsignacion.AddCell(celCtaBancoBBVA);

                    PdfPCell celTextBancoOccidente = new PdfPCell(new Phrase("Banco de Occidente", FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL)));
                    celTextBancoOccidente.VerticalAlignment = Element.ALIGN_LEFT;
                    celTextBancoOccidente.HorizontalAlignment = Element.ALIGN_TOP;
                    celTextBancoOccidente.Border = PdfPCell.NO_BORDER;
                    celTextBancoOccidente.Padding = 2;
                    celTextBancoOccidente.Colspan = 1;
                    tableDatosConsignacion.AddCell(celTextBancoOccidente);

                    PdfPCell celCtaBancoOccidente = new PdfPCell(new Phrase("Cta. Cte. 405059098", FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL)));
                    celCtaBancoOccidente.VerticalAlignment = Element.ALIGN_LEFT;
                    celCtaBancoOccidente.HorizontalAlignment = Element.ALIGN_TOP;
                    celCtaBancoOccidente.Border = PdfPCell.NO_BORDER;
                    celCtaBancoOccidente.Padding = 2;
                    celCtaBancoOccidente.Colspan = 1;
                    tableDatosConsignacion.AddCell(celCtaBancoOccidente);

                    PdfPCell celTextBancoHBank = new PdfPCell(new Phrase("Helm Bank", FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL)));
                    celTextBancoHBank.VerticalAlignment = Element.ALIGN_LEFT;
                    celTextBancoHBank.HorizontalAlignment = Element.ALIGN_TOP;
                    celTextBancoHBank.Border = PdfPCell.NO_BORDER;
                    celTextBancoHBank.Padding = 2;
                    celTextBancoHBank.Colspan = 1;
                    tableDatosConsignacion.AddCell(celTextBancoHBank);

                    PdfPCell celCtaBancoHBank = new PdfPCell(new Phrase("Cta. Cte. 101011062", FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL)));
                    celCtaBancoHBank.VerticalAlignment = Element.ALIGN_LEFT;
                    celCtaBancoHBank.HorizontalAlignment = Element.ALIGN_TOP;
                    celCtaBancoHBank.Border = PdfPCell.NO_BORDER;
                    celCtaBancoHBank.Padding = 2;
                    celCtaBancoHBank.Colspan = 1;
                    tableDatosConsignacion.AddCell(celCtaBancoHBank);
                    #endregion
                    //----------------------------------------------------------------------------------
                    PdfPCell celDatosBC1 = new PdfPCell(tableDatosBC1);
                    celDatosBC1.Border = PdfPCell.NO_BORDER;
                    tableBC1.AddCell(celDatosBC1);

                    PdfPCell celConsignarEn = new PdfPCell(tableDatosConsignacion);
                    celConsignarEn.Border = PdfPCell.NO_BORDER;
                    celConsignarEn.BorderWidthTop = 0.5f;
                    tableBC1.AddCell(celConsignarEn);
                    //------------------------SEGUNTA TABLA DE BC---------------------------------------------------------
                    //PdfDiv divTextBancoEfectivo = new PdfDiv();
                    //divTextBancoEfectivo.BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.DOTTED;
                    //divTextBancoEfectivo.Width = 570;

                    PdfPTable tableTextBancoEfectivo = new PdfPTable(3);
                    tableTextBancoEfectivo.WidthPercentage = 100;

                    PdfPCell celTextBancoEfectivoEspacio1 = new PdfPCell(new Phrase(" "));
                    celTextBancoEfectivoEspacio1.PaddingTop = 4;
                    celTextBancoEfectivoEspacio1.BorderWidthRight = PdfPCell.NO_BORDER;
                    celTextBancoEfectivoEspacio1.BorderWidthLeft = PdfPCell.NO_BORDER;
                    celTextBancoEfectivoEspacio1.BorderWidthTop = PdfPCell.NO_BORDER;
                    tableTextBancoEfectivo.AddCell(celTextBancoEfectivoEspacio1);

                    PdfPCell celTextBancoEfectivoEspacio2 = new PdfPCell(new Phrase(" "));
                    celTextBancoEfectivoEspacio2.PaddingTop = 4;
                    celTextBancoEfectivoEspacio2.BorderWidthRight = PdfPCell.NO_BORDER;
                    celTextBancoEfectivoEspacio2.BorderWidthLeft = PdfPCell.NO_BORDER;
                    celTextBancoEfectivoEspacio2.HorizontalAlignment = Element.ALIGN_CENTER;
                    celTextBancoEfectivoEspacio2.BorderWidthTop = PdfPCell.NO_BORDER;
                    tableTextBancoEfectivo.AddCell(celTextBancoEfectivoEspacio2);

                    PdfPCell celTextBancoEfectivoEspacio = new PdfPCell(new Phrase("- BANCO (EFECTIVO) -", Helpers.Fuentes.Plantilla_4_fontTitleFactura));
                    celTextBancoEfectivoEspacio.BorderWidthRight = PdfPCell.NO_BORDER;
                    celTextBancoEfectivoEspacio.BorderWidthLeft = PdfPCell.NO_BORDER;
                    celTextBancoEfectivoEspacio.BorderWidthTop = PdfPCell.NO_BORDER;
                    tableTextBancoEfectivo.AddCell(celTextBancoEfectivoEspacio);
                    //---
                    PdfPCell celBancoEfectivo = new PdfPCell();
                    celBancoEfectivo.AddElement(tableTextBancoEfectivo);
                    celBancoEfectivo.Colspan = 3;
                    celBancoEfectivo.Padding = 0;
                    celBancoEfectivo.Border = 0;
                    tableBC1.AddCell(celBancoEfectivo);


                    //divTextBancoEfectivo.AddElement(tableTextBancoEfectivo);
                    //---------------------------------------------------------------------------------------------------
                    PdfPTable tableBC2 = new PdfPTable(3);
                    tableBC2.HorizontalAlignment = Element.ALIGN_LEFT;
                    tableBC2.SetWidths(dimTableBC1);
                    tableBC2.WidthPercentage = 100;

                    PdfPCell celBC2 = new PdfPCell(new Phrase("Bar Code 2"));
                    celBC2.Border = PdfPCell.NO_BORDER;
                    tableBC2.AddCell(celBC2);

                    PdfPTable tableDatosBC2 = new PdfPTable(2);
                    tableDatosBC2.SetWidths(dimTableDatosBC1);
                    //----------------------------------------------------------------------------------
                    PdfPTable tableDatosComprador2 = new PdfPTable(2);
                    tableDatosComprador2.SetWidths(dimTableDatosComprador);

                    tableDatosComprador2.AddCell(celprgComprador);

                    PdfPCell celprgNit2 = new PdfPCell();
                    celprgNit2.AddElement(new Paragraph("NIT./CC. ", Helpers.Fuentes.Plantilla_4_fontTitle2));
                    celprgNit2.Border = PdfPCell.NO_BORDER;
                    celprgNit2.BorderWidthLeft = 0.5f;
                    celprgNit2.BorderWidthBottom = 0.5f;
                    celprgNit2.Colspan = 1;
                    //celprgNit.Padding = 3;
                    tableDatosComprador2.AddCell(celprgNit2);

                    PdfPCell celprgValNit2 = new PdfPCell();
                    celprgValNit2.AddElement(new Phrase((string)DsInvoiceAR.Tables["Customer"].Rows[0]["ResaleID"], Helpers.Fuentes.Plantilla_4_fontTitleFactura));
                    celprgValNit2.Border = PdfPCell.NO_BORDER;
                    celprgValNit2.BorderWidthBottom = 0.5f;
                    celprgValNit2.Colspan = 1;
                    celprgValNit2.Padding = 3;
                    tableDatosComprador2.AddCell(celprgValNit2);


                    PdfPCell celDatosComprador2 = new PdfPCell(tableDatosComprador2);
                    celDatosComprador2.BorderWidthBottom = PdfPCell.NO_BORDER;
                    celDatosComprador2.BorderWidthRight = PdfPCell.NO_BORDER;
                    celDatosComprador2.BorderWidthLeft = PdfPCell.NO_BORDER;
                    tableDatosBC2.AddCell(celDatosComprador2);
                    //----------------------------------------------------------------------------------
                    PdfPTable tableTotalCheque = new PdfPTable(1);

                    PdfPCell celTextTotalCheques = new PdfPCell();
                    celTextTotalCheques.AddElement(new Paragraph("Total Cheques\n",
                        FontFactory.GetFont(FontFactory.HELVETICA, 6.7f, iTextSharp.text.Font.NORMAL)));
                    celTextTotalCheques.BorderWidthBottom = PdfPCell.NO_BORDER;
                    celTextTotalCheques.VerticalAlignment = Element.ALIGN_LEFT;
                    celTextTotalCheques.HorizontalAlignment = Element.ALIGN_TOP;
                    tableTotalCheque.AddCell(celTextTotalCheques);

                    PdfPCell celDivTotalCheques = new PdfPCell();
                    celDivTotalCheques.AddElement(divTotalEfectivo);
                    celDivTotalCheques.BorderWidthTop = PdfPCell.NO_BORDER;
                    tableTotalCheque.AddCell(celDivTotalCheques);

                    PdfPCell celTotalCheques = new PdfPCell(tableTotalCheque);
                    celTotalCheques.BorderWidthBottom = PdfPCell.NO_BORDER;
                    celTotalCheques.BorderWidthLeft = PdfPCell.NO_BORDER;
                    celTotalCheques.BorderWidthRight = PdfPCell.NO_BORDER;
                    tableDatosBC2.AddCell(celTotalCheques);
                    //----------------------------------------------------------------------------------
                    PdfPTable tableBancoValores = new PdfPTable(3);


                    iTextSharp.text.Font fontCheque = FontFactory.GetFont(FontFactory.HELVETICA, 6.8f, iTextSharp.text.Font.NORMAL);

                    PdfPCell celTextBancoValoresBanco = new PdfPCell(new Phrase("Banco", fontCheque));
                    celTextBancoValoresBanco.Padding = 3;
                    celTextBancoValoresBanco.HorizontalAlignment = Element.ALIGN_CENTER;
                    //celEspacioTotalCheques.Border = PdfPCell.NO_BORDER;
                    tableBancoValores.AddCell(celTextBancoValoresBanco);

                    PdfPCell celTextBancoValoresNoCheque = new PdfPCell(new Phrase("No.Cheque", fontCheque));
                    celTextBancoValoresNoCheque.Padding = 3;
                    celTextBancoValoresNoCheque.HorizontalAlignment = Element.ALIGN_CENTER;
                    tableBancoValores.AddCell(celTextBancoValoresNoCheque);

                    PdfPCell celTextBancoValoresValor = new PdfPCell(new Phrase("Valor", fontCheque));
                    celTextBancoValoresValor.Padding = 3;
                    celTextBancoValoresValor.HorizontalAlignment = Element.ALIGN_CENTER;
                    tableBancoValores.AddCell(celTextBancoValoresValor);

                    for (int i = 0; i < 2; i++)
                    {
                        PdfPCell celBancoValoresBanco = new PdfPCell(new Phrase(" ", Helpers.Fuentes.Plantilla_4_fontTitle));
                        celBancoValoresBanco.Padding = 2.3f;
                        tableBancoValores.AddCell(celBancoValoresBanco);

                        PdfPCell celBancoValoresNoCheque = new PdfPCell(new Phrase(" ", Helpers.Fuentes.Plantilla_4_fontTitle));
                        celBancoValoresNoCheque.Padding = 2.3f;
                        tableBancoValores.AddCell(celBancoValoresNoCheque);

                        PdfPCell celBancoValoresValor = new PdfPCell(new Phrase(" ", Helpers.Fuentes.Plantilla_4_fontTitle));
                        celBancoValoresValor.Padding = 2.3f;
                        tableBancoValores.AddCell(celBancoValoresValor);
                    }

                    PdfPCell celBancoValores = new PdfPCell(tableBancoValores);
                    celBancoValores.Colspan = 2;
                    tableDatosBC2.AddCell(celBancoValores);
                    //----------------------------------------------------------------------------------

                    PdfPCell celDatosBC2 = new PdfPCell(tableDatosBC2);
                    celDatosBC2.Border = PdfPCell.NO_BORDER;
                    tableBC2.AddCell(celDatosBC2);

                    tableBC2.AddCell(celConsignarEn);
                    //---------------------------------------------------------------------------------
                    //PdfDiv divTextBancoCheque = new PdfDiv();
                    //divTextBancoCheque.BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.DOTTED;
                    //divTextBancoCheque.Width = 570;

                    PdfPTable tableTextBancoCheque = new PdfPTable(3);
                    tableTextBancoCheque.WidthPercentage = 100;

                    PdfPCell celTextBancoChequeEspacio1 = new PdfPCell(new Phrase(" "));
                    //celTextNumRot.Border = 0;
                    celTextBancoChequeEspacio1.PaddingTop = 4;
                    celTextBancoChequeEspacio1.Border = PdfPCell.NO_BORDER;
                    tableTextBancoCheque.AddCell(celTextBancoChequeEspacio1);

                    PdfPCell celTextBancoChequeEspacio2 = new PdfPCell(new Phrase(" "));
                    //celTextCopia.Border = 0;
                    celTextBancoChequeEspacio2.PaddingTop = 4;
                    celTextBancoChequeEspacio2.Border = PdfPCell.NO_BORDER;
                    tableTextBancoCheque.AddCell(celTextBancoChequeEspacio2);

                    PdfPCell celTextBancoCheque = new PdfPCell(new Phrase("- BANCO (CHEQUE) -", Helpers.Fuentes.Plantilla_4_fontTitleFactura));
                    celTextBancoCheque.Border = PdfPCell.NO_BORDER;
                    tableTextBancoCheque.AddCell(celTextBancoCheque);

                    PdfPCell celBancoCheque = new PdfPCell();
                    celBancoCheque.AddElement(tableTextBancoCheque);
                    celBancoCheque.Colspan = 3;
                    celBancoCheque.Border = 0;
                    tableBC2.AddCell(celBancoCheque);

                    //divTextBancoCheque.AddElement(tableTextBancoCheque);
                    //------- Combinando todo en una sola tabla
                    PdfPTable tbPiePagina = new PdfPTable(1);
                    tbPiePagina.WidthPercentage = 100;
                    PdfPCell celPiePagina = new PdfPCell();
                    celPiePagina.Border = 0;
                    celPiePagina.Padding = 0;

                    celPiePagina.AddElement(tableObsTotales);
                    celPiePagina.AddElement(divEspacio2);
                    celPiePagina.AddElement(tableResolucion);
                    //celPiePagina.AddElement(divTextCopia);
                    celPiePagina.AddElement(divEspacio2);
                    celPiePagina.AddElement(divEspacio2);
                    if(datos.NITReferenia)
                        celPiePagina.AddElement(tableTextNitReferencia);
                    if(datos.BarCode01)
                    {
                        celPiePagina.AddElement(tableBC1);
                        celPiePagina.AddElement(divEspacio2);
                        celPiePagina.AddElement(divEspacio2);
                        //celPiePagina.AddElement(divTextBancoEfectivo);
                        celPiePagina.AddElement(divEspacio2);
                        celPiePagina.AddElement(divEspacio2);
                    }
                    if(datos.NITReferenia)
                        celPiePagina.AddElement(tableTextNitReferencia);
                    if(datos.BarCode02)
                    {
                        celPiePagina.AddElement(tableBC2);
                        celPiePagina.AddElement(divEspacio2);
                        celPiePagina.AddElement(divEspacio2);
                        //celPiePagina.AddElement(divTextBancoCheque);
                    }
                    tbPiePagina.AddCell(celPiePagina);
                    return tbPiePagina;
                }
                //------ Clases para la consulta --------
                public class InvcDtl
                {
                    public string Company { get; set; }
                    public int InvoiceNum { get; set; }
                    public int InvoiceLine { get; set; }
                    public string PartNum { get; set; }
                    public string XPartNum { get; set; }
                    public decimal SellingShipQty { get; set; }
                }
                public class InvcTax
                {
                    public string Company { get; set; }
                    public int InvoiceNum { get; set; }
                    public int InvoiceLine { get; set; }
                }
                public class InvcMisc
                {
                    public string Company { get; set; }
                    public int InvoiceNum { get; set; }
                    public int InvoiceLine { get; set; }
                    public string MiscCode { get; set; }
                    public string Description { get; set; }
                    public decimal MiscAmt { get; set; }
                    public decimal DocMiscAmt { get; set; }
                }
            }
        }
        #endregion
    }
}
