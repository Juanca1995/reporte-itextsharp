﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
//Referenciar y usar.
using System.Data;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Web;
using System.Windows.Forms;

namespace RulesServicesDIAN2.Adquiriente
{
    public partial class pdfEstandarAR
    {

        #region Factura de venta GKW Consultant
        private bool AddUnidadesGKWConsultant(DataRow dataLine, ref PdfPTable table, iTextSharp.text.Font fontTitleFactura, DataSet dataSet)
        {
            try
            {
                PdfPCell celValL = new PdfPCell(new Phrase((string)dataLine["LineDesc"], fontTitleFactura));
                celValL.Colspan = 1;
                celValL.PaddingLeft = 15;
                celValL.PaddingTop = 15;
                celValL.Border = 0;
                celValL.BorderWidthRight = 1;
                celValL.HorizontalAlignment = Element.ALIGN_JUSTIFIED;
                celValL.VerticalAlignment = Element.ALIGN_JUSTIFIED;
                table.AddCell(celValL);
                
                PdfPCell celValreferencia = new PdfPCell(new Phrase("  ", fontTitleFactura));
                celValreferencia.Colspan = 1;
                celValreferencia.Padding = 2;
                celValreferencia.Border = 0;
                celValreferencia.HorizontalAlignment = Element.ALIGN_CENTER;
                celValreferencia.VerticalAlignment = Element.ALIGN_CENTER;
                table.AddCell(celValreferencia);
                
                PdfPCell celCentAct = new PdfPCell(new Phrase(string.Format("Centro de Actividad: {0}\n Interventor: {1}\n{2}      {3}\n{4}      {5}", (string)dataLine["ShortChar01"],
                    (string)dataLine["ShortChar02"], (string)dataLine["ShortChar03"], (string)dataLine["ShortChar04"], (string)dataLine["ShortChar05"], (string)dataLine["ShortChar06"]), fontTitleFactura));
                celCentAct.Colspan = 1;
                celCentAct.PaddingLeft = 15;
                celCentAct.PaddingTop = 15;
                celCentAct.Border = 0;
                celCentAct.BorderWidthRight = 1;
                celCentAct.HorizontalAlignment = Element.ALIGN_JUSTIFIED;
                celCentAct.VerticalAlignment = Element.ALIGN_JUSTIFIED;
                table.AddCell(celCentAct);
                
                PdfPCell celVacio = new PdfPCell(new Phrase("  ", fontTitleFactura));
                celVacio.Colspan = 1;
                celVacio.Padding = 2;
                celVacio.Border = 0;
                celVacio.HorizontalAlignment = Element.ALIGN_CENTER;
                celVacio.VerticalAlignment = Element.ALIGN_CENTER;
                table.AddCell(celVacio);
                
                PdfPCell celInfo = new PdfPCell(new Phrase((string)dataLine["Character01"], fontTitleFactura));
                celInfo.Colspan = 1;
                celInfo.PaddingLeft = 15;
                celInfo.PaddingTop = 15;
                celInfo.PaddingBottom = 15;
                celInfo.Border = 0;
                celInfo.BorderWidthRight = 1;
                celInfo.HorizontalAlignment = Element.ALIGN_JUSTIFIED;
                celInfo.VerticalAlignment = Element.ALIGN_JUSTIFIED;
                table.AddCell(celInfo);

                PdfPCell celVacio1 = new PdfPCell(new Phrase(decimal.Parse((string)dataLine["DocUnitPrice"]).ToString("N2"), fontTitleFactura));
                celVacio1.Colspan = 1;
                celVacio1.Padding = 2;
                celVacio1.Border = 0;
                celVacio1.HorizontalAlignment = Element.ALIGN_RIGHT;
                celVacio1.VerticalAlignment = Element.ALIGN_RIGHT;
                table.AddCell(celVacio1);

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return false;
            }
        }

        public bool FacturaVentaGKW(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {
            strError += "Lego";
            NomArchivo = string.Empty;
            string Ruta = HttpContext.Current.Server.MapPath("~/PDF/" + NIT + "/");
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            string RutaImg = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";
            string Rutacertificado = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";
            NomArchivo = NombreInvoice + ".pdf";
            strError += "inicia Documento pdf\n";

            //NomArchivo = string.Empty;
            //string Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
            //string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            //string RutaImg = $@"{AppDomain.CurrentDomain.BaseDirectory}\logo_gkw.png";
            //string Rutacertificado = $@"{AppDomain.CurrentDomain.BaseDirectory}\logo_gkw.png";
            //NomArchivo = NombreInvoice + ".pdf";
            try
            {
                #region Head
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.
                Document document = new Document(iTextSharp.text.PageSize.LETTER);
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;

                // step 3: we open the document     
                document.Open();

                //FUENTES----------------------------------------------------------------------------------------------------------
                iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 8, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle2 = FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustom = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 10, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustom2 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                //FUENTES---------------------------------------------------------------------------------------------------------

                ///objeto para el borde 
                RoundRectangle CelEventBorderRound = new RoundRectangle();

                System.Drawing.Image Logo;
                System.Drawing.Image LogoBanner = null;
                var request = WebRequest.Create(RutaImg);
                using (var response = request.GetResponse())
                using (var stream = response.GetResponseStream())
                {
                    Logo = Bitmap.FromStream(stream);
                }
                var requestBanner = WebRequest.Create(RutaImg);
                using (var responseBanner = requestBanner.GetResponse())
                using (var streamBanner = responseBanner.GetResponseStream())
                {
                    LogoBanner = Bitmap.FromStream(streamBanner);
                }


                System.Drawing.Image Logo2;
                System.Drawing.Image LogoBanner2 = null;
                var request2 = WebRequest.Create(Rutacertificado);
                using (var response2 = request2.GetResponse())
                using (var stream2 = response2.GetResponseStream())
                {
                    Logo2 = Bitmap.FromStream(stream2);
                }
                var requestBanner2 = WebRequest.Create(Rutacertificado);
                using (var responseBanner2 = requestBanner2.GetResponse())
                using (var streamBanner2 = responseBanner2.GetResponseStream())
                {
                    LogoBanner2 = Bitmap.FromStream(streamBanner2);
                }


                //Logos--------------------------------
                iTextSharp.text.Image LogoPdf3 = iTextSharp.text.Image.GetInstance(LogoBanner2, BaseColor.WHITE);
                LogoPdf3.ScaleAbsolute(90f, 90f);
                LogoPdf3.Border = 0;


                iTextSharp.text.Image LogoPdf2 = iTextSharp.text.Image.GetInstance(LogoBanner, BaseColor.WHITE);
                LogoPdf2.ScaleAbsolute(150, 150);
                LogoPdf2.Border = 0;

                /*CELL QR*/
                iTextSharp.text.Image QRPdf = iTextSharp.text.Image.GetInstance(QRInvoice, BaseColor.WHITE);
                QRPdf.ScaleAbsolute(80f, 80f);
                iTextSharp.text.pdf.PdfPCell celImgQR = new iTextSharp.text.pdf.PdfPCell(QRPdf);
                celImgQR.HorizontalAlignment = Element.ALIGN_CENTER;
                celImgQR.Border = 0;
                celImgQR.BorderWidthTop = 1;
                celImgQR.PaddingTop = 5;

                PdfPTable espacio = new PdfPTable(1);
                espacio.WidthPercentage = 100;
                PdfPCell salto = new PdfPCell() { Border = 0, MinimumHeight = 10, };
                espacio.AddCell(salto);

                #endregion

                #region Header Principal

                //encabezado de la factura 
                PdfPTable tableAll = new PdfPTable(1);
                tableAll.WidthPercentage = 100;

                PdfPCell tablaAllCel = new PdfPCell()
                {
                    CellEvent = CelEventBorderRound,
                    Border = 0,
                };

                PdfPTable HeaderInfoP = new PdfPTable(new float[] { 1.0f, 1.8f, 0.8f });
                HeaderInfoP.WidthPercentage = 100;

                string TipoFactura = string.Empty;
                if (InvoiceType == "InvoiceType")
                    TipoFactura = "FACTURA DE VENTA";
                else if (InvoiceType == "CreditNoteType")
                    TipoFactura = "NOTA CREDITO";
                else
                    TipoFactura = "NOTA DEBITO";

                PdfPCell logo = new PdfPCell() { };
                logo.Border = 0;
                PdfPCell info = new PdfPCell() { HorizontalAlignment = Element.ALIGN_CENTER, Border = 0,};
                Paragraph prgTitle1 = new Paragraph(new Phrase(DsInvoiceAR.Tables["Company"].Rows[0]["Name"].ToString() + "\n", fontTitle));
                prgTitle1.Alignment = Element.ALIGN_CENTER;
                Paragraph prgTitle2 = new Paragraph(new Phrase(string.Format("Nit: {0}\n{1}\n{2}\n Tel: {3}\n {4} - {5}",
                        DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"].ToString(),
                        DsInvoiceAR.Tables["Company"].Rows[0]["Address1"].ToString(), DsInvoiceAR.Tables["InvcHead"].Rows[0]["PlantName"].ToString(), 
                        DsInvoiceAR.Tables["Company"].Rows[0]["PhoneNum"].ToString(), DsInvoiceAR.Tables["Company"].Rows[0]["City"].ToString(),
                         DsInvoiceAR.Tables["Company"].Rows[0]["Country"].ToString()), FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL)));
                prgTitle2.Alignment = Element.ALIGN_CENTER;
                info.AddElement(prgTitle1);
                info.AddElement(prgTitle2);
                PdfPCell numero_factura = new PdfPCell(new Phrase(TipoFactura + "\n\n   N°" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"].ToString(), FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 10, iTextSharp.text.Font.NORMAL))) { HorizontalAlignment = Element.ALIGN_LEFT, Border = 0, PaddingTop = 20, };

                logo.AddElement(LogoPdf2);

                HeaderInfoP.AddCell(logo);
                HeaderInfoP.AddCell(info);
                HeaderInfoP.AddCell(numero_factura);

                PdfPTable tablaDatesHeader = new PdfPTable(new float[] {0.2f, 5.5f, 1.0f, 2.5f });
                tablaDatesHeader.WidthPercentage = 100;

                PdfPTable tablaDatesHeader1 = new PdfPTable(new float[] { 1.8f, 0.8f, 0.8f, 0.8f, 1.8f, 0.8f, 0.8f, 0.8f });
                tablaDatesHeader1.WidthPercentage = 100;
                PdfPCell fechaExp = new PdfPCell(new Phrase("FECHA DE\nEXPEDICION", fontTitle2)) { Border = 0, Padding = 3, PaddingTop = 7, HorizontalAlignment = Element.ALIGN_CENTER,  }; 
                tablaDatesHeader1.AddCell(fechaExp);
                PdfPCell fechaExp1 = new PdfPCell(new Phrase(DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).ToString("dd"), fontTitle2)) { Border = 0, Padding = 3, PaddingTop = 7, HorizontalAlignment = Element.ALIGN_CENTER, BorderWidthLeft = 1, }; 
                tablaDatesHeader1.AddCell(fechaExp1);
                PdfPCell fechaExp2 = new PdfPCell(new Phrase(DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).ToString("MM"), fontTitle2)) { Border = 0, Padding = 3, PaddingTop = 7, HorizontalAlignment = Element.ALIGN_CENTER, BorderWidthLeft = 1, }; 
                tablaDatesHeader1.AddCell(fechaExp2);
                PdfPCell fechaExp3 = new PdfPCell(new Phrase(DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).ToString("yyyy"), fontTitle2)) { Border = 0, Padding = 3, PaddingTop = 7, HorizontalAlignment = Element.ALIGN_CENTER, BorderWidthLeft = 1, }; 
                tablaDatesHeader1.AddCell(fechaExp3);
                PdfPCell fechaVenc = new PdfPCell(new Phrase("FECHA DE\nVENCIMIENTO", fontTitle2)) { Border = 0, Padding = 3, PaddingTop = 7, HorizontalAlignment = Element.ALIGN_CENTER, BorderWidthLeft = 1, }; 
                tablaDatesHeader1.AddCell(fechaVenc);
                PdfPCell fechaVenc1 = new PdfPCell(new Phrase(DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"]).ToString("dd"), fontTitle2)) { Border = 0, Padding = 3, PaddingTop = 7, HorizontalAlignment = Element.ALIGN_CENTER, BorderWidthLeft = 1, }; 
                tablaDatesHeader1.AddCell(fechaVenc1);
                PdfPCell fechaVenc2 = new PdfPCell(new Phrase(DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"]).ToString("MM"), fontTitle2)) { Border = 0, Padding = 3, PaddingTop = 7, HorizontalAlignment = Element.ALIGN_CENTER, BorderWidthLeft = 1, }; 
                tablaDatesHeader1.AddCell(fechaVenc2);
                PdfPCell fechaVenc3 = new PdfPCell(new Phrase(DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"]).ToString("yyyy"), fontTitle2)) { Border = 0, Padding = 3, PaddingTop = 7, HorizontalAlignment = Element.ALIGN_CENTER, BorderWidthLeft = 1, }; 
                tablaDatesHeader1.AddCell(fechaVenc3);

                tablaDatesHeader.AddCell(salto);

                PdfPCell celDates = new PdfPCell(tablaDatesHeader1)
                {
                    CellEvent = CelEventBorderRound,
                    Border = 0,
                };
                tablaDatesHeader.AddCell(celDates);
                tablaDatesHeader.AddCell(salto);

                PdfPCell resol = new PdfPCell(new Phrase( DsInvoiceAR.Tables["InvcHead"].Rows[0]["Character02"].ToString(), fontTitle2))
                {
                    Border = 0,
                    Padding = 3,
                    HorizontalAlignment = Element.ALIGN_CENTER,
                };
                tablaDatesHeader.AddCell(resol);

                #endregion

                #region Informacion Cliente

                PdfPTable tablaBorde = new PdfPTable(new float[] { 5.0f, 1.5f });
                tablaBorde.WidthPercentage = 100;
                PdfPCell tablaAux = new PdfPCell()
                {
                    Border = 0,
                    BorderWidthTop = 1,
                    MinimumHeight = 15,
                };

                //cliente y informacion de la factura 
                PdfPTable cliente = new PdfPTable(new float[] { 2.0f, 4.2f, 1.0f, 2.0f });
                cliente.WidthPercentage = 100;
                PdfPCell titulosInfoC = new PdfPCell(new Phrase("CLIENTE: ", fontTitle2)) { Border = 0, Padding = 3, };
                cliente.AddCell(titulosInfoC);
                PdfPCell valoresInfo = new PdfPCell(new Phrase(DsInvoiceAR.Tables["Customer"].Rows[0]["Name"].ToString(), fontTitle)) { Border = 0, Padding = 3, };
                cliente.AddCell(valoresInfo);
                PdfPCell titulosInfoC4 = new PdfPCell(new Phrase(" ", fontTitle2)) { Border = 0, Padding = 3, };
                cliente.AddCell(titulosInfoC4);
                PdfPCell valoresInfoC4 = new PdfPCell(new Phrase(" ", fontTitle2)) { Border = 0, Padding = 3, };
                cliente.AddCell(valoresInfoC4);
                PdfPCell titulosInfoC1 = new PdfPCell(new Phrase("REPRESENTANTE: ", fontTitle2)) { Border = 0, Padding = 3, };
                cliente.AddCell(titulosInfoC1);
                PdfPCell valoresInfo1 = new PdfPCell(new Phrase(DsInvoiceAR.Tables["Customer"].Rows[0]["ShortChar01"].ToString(), fontTitle)) { Border = 0, Padding = 3, };
                cliente.AddCell(valoresInfo1);
                PdfPCell titulosInfoC5 = new PdfPCell(new Phrase(" ", fontTitle2)) { Border = 0, Padding = 3, };
                cliente.AddCell(titulosInfoC5);
                PdfPCell valoresInfoC5 = new PdfPCell(new Phrase(" ", fontTitle2)) { Border = 0, Padding = 3, };
                cliente.AddCell(valoresInfoC5);
                PdfPCell titulosInfoC2 = new PdfPCell(new Phrase("DIRECCIÓN: ", fontTitle2)) { Border = 0, Padding = 3, };
                cliente.AddCell(titulosInfoC2);
                PdfPCell valoresInfo2 = new PdfPCell(new Phrase(DsInvoiceAR.Tables["Customer"].Rows[0]["Address1"].ToString(), fontTitle)) { Border = 0, Padding = 3, };
                cliente.AddCell(valoresInfo2);
                PdfPCell titulosInfoC6 = new PdfPCell(new Phrase("TEL: ", fontTitle2)) { Border = 0, Padding = 3, };
                cliente.AddCell(titulosInfoC6);
                PdfPCell valoresInfoC6 = new PdfPCell(new Phrase(DsInvoiceAR.Tables["Customer"].Rows[0]["PhoneNum"].ToString(), fontTitle)) { Border = 0, Padding = 3, };
                cliente.AddCell(valoresInfoC6);
                PdfPCell titulosInfoC3 = new PdfPCell(new Phrase("NIT: ", fontTitle2)) { Border = 0, Padding = 3, };
                cliente.AddCell(titulosInfoC3);
                PdfPCell valoresInfo3 = new PdfPCell(new Phrase(DsInvoiceAR.Tables["Customer"].Rows[0]["CustID"].ToString(), fontTitle)) { Border = 0, Padding = 3, };
                cliente.AddCell(valoresInfo3);
                PdfPCell titulosInfoC7 = new PdfPCell(new Phrase("CIUDAD: ", fontTitle2)) { Border = 0, Padding = 3, };
                cliente.AddCell(titulosInfoC7);
                PdfPCell valoresInfoC7 = new PdfPCell(new Phrase(DsInvoiceAR.Tables["Customer"].Rows[0]["City"].ToString(), fontTitle)) { Border = 0, Padding = 3, };
                cliente.AddCell(valoresInfoC7);
                PdfPCell cufeCel = new PdfPCell(new Phrase("CUFE: ", fontCustom2)) { Border = 0, Padding = 3, };
                cliente.AddCell(cufeCel);
                PdfPCell cufeval = new PdfPCell(new Phrase(CUFE, fontCustom2)) { Border = 0, Padding = 3, };
                cliente.AddCell(cufeval);
                cliente.AddCell(valoresInfoC5);
                cliente.AddCell(valoresInfoC5);

                tablaAux.AddElement(cliente);
                tablaBorde.AddCell(tablaAux); 
                tablaBorde.AddCell(celImgQR);
                #endregion

                #region Items
                PdfPTable tabla_borde3 = new PdfPTable(1);
                tabla_borde3.WidthPercentage = 100;

                PdfPTable Body = new PdfPTable(new float[] { 6.8f, 1.5f });
                Body.WidthPercentage = 100;
                PdfPCell descrp = new PdfPCell(new Phrase("D E S C R I P C I Ó N", fontTitle)) { HorizontalAlignment = Element.ALIGN_CENTER, Border = 0, BackgroundColor = BaseColor.LIGHT_GRAY, };
                PdfPCell valor = new PdfPCell(new Phrase("V A L O R", fontTitle)) { HorizontalAlignment = Element.ALIGN_CENTER, Border = 0, BorderWidthLeft = 1, BackgroundColor = BaseColor.LIGHT_GRAY, };
                
                Body.AddCell(descrp);
                Body.AddCell(valor);
                PdfPCell tablaAux1 = new PdfPCell(Body)
                {
                    Border = 1,
                    BorderWidthBottom = 1,
                    Padding = 2,
                    BackgroundColor = BaseColor.LIGHT_GRAY,
                };
                tabla_borde3.AddCell(tablaAux1);
                
                PdfPTable tableUnidades = new PdfPTable(new float[] { 6.8f, 1.5f });
                tableUnidades.WidthPercentage = 100;
                
                foreach (DataRow InvoiceLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
                {
                    if (!AddUnidadesGKWConsultant(InvoiceLine, ref tableUnidades, FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 9, iTextSharp.text.Font.NORMAL), DsInvoiceAR))
                        return false;
                }

                #endregion

                #region Totales

                PdfPTable tableTotales = new PdfPTable(new float[] { 6.8f, 1.5f, 1.83f });
                tableTotales.WidthPercentage = 100;
                PdfPCell cuentaC = new PdfPCell(new Phrase("Favor consignar el valor en la cuenta\nde BBVA, cuenta corriente No. 501002471", fontCustom)) { Border = 0, PaddingLeft = 15, };
                tableTotales.AddCell(cuentaC);
                PdfPCell ivaV = new PdfPCell(new Phrase("IVA  $", fontCustom)) { Border = 0, Padding = 3, BorderWidthTop = 1, BorderWidthRight = 1, BorderWidthLeft = 1, HorizontalAlignment = Element.ALIGN_CENTER, };
                tableTotales.AddCell(ivaV);
                PdfPCell ivaVal = new PdfPCell(new Phrase(GetValImpuestoByID("01", DsInvoiceAR).ToString("N2"), fontCustom)) { Border = 0, Padding = 3, BorderWidthTop = 1, };
                ivaVal.HorizontalAlignment = Element.ALIGN_RIGHT;
                tableTotales.AddCell(ivaVal);
                PdfPCell tablaCelTotales = new PdfPCell(tableTotales)
                {
                    Border = 0,
                };
                PdfPTable tablaTot = new PdfPTable(1);
                tablaTot.WidthPercentage = 100;
                tablaTot.AddCell(tablaCelTotales);

                PdfPTable tableTotales2 = new PdfPTable(new float[] { 6.8f, 1.5f, 1.83f });
                tableTotales2.WidthPercentage = 100;
                PdfPCell son = new PdfPCell(new Phrase("SON: " + Helpers.Compartido.Nroenletras(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"].ToString()), fontTitle)) { Border = 0, Padding = 7, };
                tableTotales2.AddCell(son);
                PdfPCell totalV = new PdfPCell(new Phrase("TOTAL  $", fontCustom)) { Border = 0, Padding = 7, BorderWidthRight = 1, BorderWidthLeft = 1, HorizontalAlignment = Element.ALIGN_CENTER, };
                totalV.BorderWidthRight = 1;
                tableTotales2.AddCell(totalV);
                PdfPCell totalVal = new PdfPCell(new Phrase(decimal.Parse(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"].ToString()).ToString("N2"), fontCustom)) { Border = 0, Padding = 7, };
                totalVal.HorizontalAlignment = Element.ALIGN_RIGHT;
                tableTotales2.AddCell(totalVal);
                PdfPCell tablaCelTotales2 = new PdfPCell(tableTotales2)
                {
                    Border = 1,
                };
                PdfPTable tablaTot2 = new PdfPTable(1);
                tablaTot2.WidthPercentage = 100;
                tablaTot2.AddCell(tablaCelTotales2);

                PdfPTable tablePagos = new PdfPTable(1);
                tablePagos.WidthPercentage = 100;
                PdfPCell condiciones = new PdfPCell(new Phrase("CONDICIONES DE PAGO: " + DsInvoiceAR.Tables["Customer"].Rows[0]["TermsDescription"].ToString(), fontTitle2)) { Border = 0, Padding = 3, BorderWidthTop = 1, };
                tablePagos.AddCell(condiciones);
                PdfPCell estPago = new PdfPCell(new Phrase("ESTADO DE PAGO: ____________________________________________ ", fontTitle2)) { Border = 0, Padding = 3, PaddingTop = 10, PaddingBottom = 10, };
                tablePagos.AddCell(estPago);
                PdfPCell bases = new PdfPCell(new Phrase("Bases: " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["Character01"].ToString(), fontTitle2)) { Border = 0, Padding = 3, BorderWidthTop = 1, PaddingBottom = 10, PaddingTop = 10, };
                tablePagos.AddCell(bases);

                PdfPTable tableInfoFoot = new PdfPTable(2);
                tableInfoFoot.WidthPercentage = 100;
                PdfPCell emisor = new PdfPCell(new Phrase("EMISOR: ", fontTitle2)) { Border = 0, Padding = 3, BorderWidthTop = 1, HorizontalAlignment = Element.ALIGN_LEFT, };
                tableInfoFoot.AddCell(emisor);
                PdfPCell beneficiario = new PdfPCell(new Phrase("BENEFICIARIO: ", fontTitle2)) { Border = 0, Padding = 3, BorderWidthTop = 1, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_CENTER, BorderWidthLeft = 1, };
                tableInfoFoot.AddCell(beneficiario);
                PdfPCell celVacia = new PdfPCell(new Phrase("    ", fontCustom)) { Border = 0, Padding = 3, };
                tableInfoFoot.AddCell(celVacia);
                PdfPCell nomQ = new PdfPCell(new Phrase("NOMBRE DE QUIEN RECIBE: ______________________________ ", fontTitle2)) { Border = 0, Padding = 3, BorderWidthLeft = 1,};
                tableInfoFoot.AddCell(nomQ);
                tableInfoFoot.AddCell(celVacia);
                PdfPCell cc = new PdfPCell(new Phrase("C.C: ______________________________ ", fontTitle2)) { Border = 0, Padding = 3, BorderWidthLeft = 1, };
                tableInfoFoot.AddCell(cc);
                tableInfoFoot.AddCell(celVacia);
                PdfPCell fecharec = new PdfPCell(new Phrase("FECHA DE RECIBIDO: ______________________________ ", fontTitle2)) { Border = 0, Padding = 3, BorderWidthLeft = 1, };
                tableInfoFoot.AddCell(fecharec);
                tableInfoFoot.AddCell(celVacia);
                PdfPCell firms = new PdfPCell(new Phrase("FIRMA Y SELLO: _____________________________________", fontTitle2)) { Border = 0, Padding = 3, BorderWidthLeft = 1, PaddingTop = 15, };
                tableInfoFoot.AddCell(firms);
                PdfPCell linea = new PdfPCell(new Phrase("_______________________________________", fontTitle2)) { Border = 0, Padding = 3, HorizontalAlignment = Element.ALIGN_CENTER, };
                tableInfoFoot.AddCell(linea);
                PdfPCell acep = new PdfPCell(new Phrase("ACEPTO EL CONTENIDO DE LA PRESENTE FACTURA", fontTitle2)) { Border = 0, Padding = 3, HorizontalAlignment = Element.ALIGN_CENTER, BorderWidthLeft = 1, };
                tableInfoFoot.AddCell(acep);
                PdfPCell sellf = new PdfPCell(new Phrase("Sello y firma autorizada GKW CONSULTANT GMBH SUCURSAL COLOMBIA", fontTitle2)) { Border = 0, Padding = 3, HorizontalAlignment = Element.ALIGN_CENTER, };
                tableInfoFoot.AddCell(sellf);
                PdfPCell celVacia3 = new PdfPCell(new Phrase("Representación gráfica factura electrónica", fontCustom2)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, BorderWidthLeft = 1, };
                tableInfoFoot.AddCell(celVacia3);

                PdfPTable tableInfoFoot2 = new PdfPTable(2);
                tableInfoFoot2.WidthPercentage = 100;
                PdfPCell contrIn = new PdfPCell(new Phrase("NO somos Grandes Contribuyentes\nNO somos Autorretenedores\nIVA Régimen Común\nActividad Económica CIIU 7110", fontTitle2)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, BorderWidthTop = 1, Padding = 5,};
                tableInfoFoot2.AddCell(contrIn);
                PdfPCell incumplIn = new PdfPCell(new Phrase("El incumplimiento en el pago causará intereses de mora a la tasa autorizada sin exceder la máxima legal.", fontTitle2)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, BorderWidthTop = 1, Padding = 5, };
                tableInfoFoot2.AddCell(incumplIn);


                #endregion

                #region Exit

                tablaAllCel.AddElement(HeaderInfoP);
                tablaAllCel.AddElement(espacio);
                tablaAllCel.AddElement(tablaDatesHeader);
                tablaAllCel.AddElement(espacio);
                tablaAllCel.AddElement(tablaBorde);
                tablaAllCel.AddElement(espacio);
                tablaAllCel.AddElement(tabla_borde3);
                tablaAllCel.AddElement(tableUnidades);
                tablaAllCel.AddElement(tablaTot);
                tablaAllCel.AddElement(tablaTot2);
                tablaAllCel.AddElement(tablePagos);
                tablaAllCel.AddElement(tableInfoFoot);
                tablaAllCel.AddElement(tableInfoFoot2);
                tableAll.AddCell(tablaAllCel);
                
                document.Add(tableAll);

                /*PIE DE PAGINA*/
                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();
                return true;
                #endregion
            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.Message;
                return false;
            }

        }
        #endregion

    }
}
