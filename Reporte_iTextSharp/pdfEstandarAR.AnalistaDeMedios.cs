﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
//Referenciar y usar.
using System.Data;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Web;
using System.Windows.Forms;

namespace RulesServicesDIAN2.Adquiriente
{
    public partial class pdfEstandarAR
    {
        #region FORMATOS  ANALISTAS DE MEDIOS 

        private bool AddUnidadesAnalistadeMedios(DataRow dataLine, ref PdfPTable pdfPTable, iTextSharp.text.Font font, DataSet dataSet)
        {
            try
            {
                //codigo
                iTextSharp.text.pdf.PdfPCell celcodigo = new iTextSharp.text.pdf.PdfPCell(new Phrase(dataLine["PartNum"].ToString(), font));
                celcodigo.Colspan = 1;
                celcodigo.Padding = 3;
                celcodigo.Border = 0;
                celcodigo.BorderColorBottom = BaseColor.WHITE;
                celcodigo.HorizontalAlignment = Element.ALIGN_LEFT;
                celcodigo.VerticalAlignment = Element.ALIGN_TOP;
                pdfPTable.AddCell(celcodigo);
                //descripcion
                iTextSharp.text.pdf.PdfPCell celDescripcion = new iTextSharp.text.pdf.PdfPCell(new Phrase(dataLine["LineDesc"].ToString(), font));
                celDescripcion.Colspan = 1;
                celDescripcion.Padding = 3;
                celDescripcion.Border = 0;
                celDescripcion.BorderColorBottom = BaseColor.WHITE;
                celDescripcion.HorizontalAlignment = Element.ALIGN_LEFT;
                celDescripcion.VerticalAlignment = Element.ALIGN_TOP;
                pdfPTable.AddCell(celDescripcion);
                //cantidad
                iTextSharp.text.pdf.PdfPCell celcantidad = new iTextSharp.text.pdf.PdfPCell(new Phrase(dataLine["SellingShipQty"].ToString(), font));
                celcantidad.Colspan = 1;
                celcantidad.Padding = 3;
                celcantidad.Border = 0;
                celcantidad.BorderColorBottom = BaseColor.WHITE;
                celcantidad.HorizontalAlignment = Element.ALIGN_RIGHT;
                celcantidad.VerticalAlignment = Element.ALIGN_TOP;
                pdfPTable.AddCell(celcantidad);
                //Precio
                iTextSharp.text.pdf.PdfPCell celPrecio = new iTextSharp.text.pdf.PdfPCell(new Phrase("$ " + decimal.Parse((String)dataLine["UnitPrice"]).ToString("N0"), font));
                celPrecio.Colspan = 1;
                celPrecio.Padding = 3;
                celPrecio.Border = 0;
                celPrecio.BorderColorBottom = BaseColor.WHITE;
                celPrecio.HorizontalAlignment = Element.ALIGN_RIGHT;
                celPrecio.VerticalAlignment = Element.ALIGN_TOP;
                pdfPTable.AddCell(celPrecio);
                //Iva
                iTextSharp.text.pdf.PdfPCell celIva = new iTextSharp.text.pdf.PdfPCell(new Phrase("$ "  
                   /* decimal.Parse(GetPercentIdImpDIAN((string)dataLine["InvoiceNum"], (string)dataLine["InvoiceLine"], dataSet.Tables["InvcTax"])).ToString("N0")*/, font));
                celIva.Colspan = 1;
                celIva.Padding = 3;
                celIva.Border = 0;
                celIva.BorderColorBottom = BaseColor.WHITE;
                celIva.HorizontalAlignment = Element.ALIGN_RIGHT;
                celIva.VerticalAlignment = Element.ALIGN_TOP;
                pdfPTable.AddCell(celIva);
                //celRete Fuente
                iTextSharp.text.pdf.PdfPCell celReteFuente = new iTextSharp.text.pdf.PdfPCell(new Phrase("$ " + decimal.Parse((String)dataLine["DspDocExtPrice"]).ToString("N0"), font));
                celReteFuente.Colspan = 1;
                celReteFuente.Padding = 3;
                celReteFuente.Border = 0;
                celReteFuente.BorderColorBottom = BaseColor.WHITE;
                celReteFuente.HorizontalAlignment = Element.ALIGN_RIGHT;
                celReteFuente.VerticalAlignment = Element.ALIGN_TOP;
                pdfPTable.AddCell(celReteFuente);
                //Total
                iTextSharp.text.pdf.PdfPCell celTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase("$ "+decimal.Parse((String)dataLine["DspDocExtPrice"]).ToString("N0"), font));
                celTotal.Colspan = 1;
                celTotal.Padding = 3;
                celTotal.Border = 0;
                celTotal.BorderColorBottom = BaseColor.WHITE;
                celTotal.HorizontalAlignment = Element.ALIGN_RIGHT;
                celTotal.VerticalAlignment = Element.ALIGN_TOP;
                pdfPTable.AddCell(celTotal);

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return false;
            }
        }

        public bool FacturaDeVentasAnalistadeMedios(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {
            NomArchivo = string.Empty;
            string Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            string RutaImg = $@"{AppDomain.CurrentDomain.BaseDirectory}\Logo-Analistas-PNG-2.png";
            string RutaMarcaAgua = $@"{AppDomain.CurrentDomain.BaseDirectory}\Logo-Analistas-PNG-2.png";
            NomArchivo = NombreInvoice + ".pdf";
            try
            {
                #region Head
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.
                Document document = new Document(iTextSharp.text.PageSize.LETTER);
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;

                // step 3: we open the document     
                document.Open();

                PdfDiv divEspacio = new PdfDiv();
                divEspacio.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio.Height = 10;
                divEspacio.Width = 130;

                PdfDiv divEspacio2 = new PdfDiv();
                divEspacio2.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio2.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio2.Height = 2;
                divEspacio2.Width = 130;

                //FUENTES----------------------------------------------------------------------------------------------------------
                iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle3 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 9, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle2 = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustom = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitleFactura = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                //FUENTES---------------------------------------------------------------------------------------------------------

                System.Drawing.Image logo = null;
                var requestLogo = WebRequest.Create(RutaImg);

                using (var responseLogo = requestLogo.GetResponse())
                using (var streamLogo = responseLogo.GetResponseStream())
                {
                    logo = Bitmap.FromStream(streamLogo);
                }

                System.Drawing.Image logo2 = null;
                var requestLogo2 = WebRequest.Create(RutaImg);

                using (var responseLogo2 = requestLogo2.GetResponse())
                using (var streamLogo2 = responseLogo2.GetResponseStream())
                {
                    logo2 = Bitmap.FromStream(streamLogo2);
                }
                iTextSharp.text.Image ImgLogo2 = iTextSharp.text.Image.GetInstance(logo2, BaseColor.WHITE);
                ImgLogo2.ScaleAbsolute(180, 180);

                #endregion

                PdfPTable salto = new PdfPTable(1);
                salto.WidthPercentage = 100;
                PdfPCell cell_salto = new PdfPCell() { Border=0,MinimumHeight=30};
                salto.AddCell(cell_salto);

                #region Header

                PdfPTable Header = new PdfPTable(new float[] { 0.32f, 0.3f, 0.4f, });
                Header.WidthPercentage = 100;

                PdfPCell cell_logo = new PdfPCell() { Border = 0 };
                cell_logo.AddElement(ImgLogo2);
                Header.AddCell(cell_logo);

                PdfPCell cell_info = new PdfPCell() { Border = 0 };
                Paragraph prgTitle = new Paragraph($"" + DsInvoiceAR.Tables["Company"].Rows[0]["Address1"], fontCustom);
                Paragraph prgTitle2 = new Paragraph(string.Format("{0}{1}{2}",
                   $"Telefono: " + DsInvoiceAR.Tables["Company"].Rows[0]["PhoneNum"].ToString() + $" / Fax: " + DsInvoiceAR.Tables["Company"].Rows[0]["PhoneNum"].ToString() + "\n",
                   $"NIT:  " + DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"].ToString() + "\n",
                   $"No somos Autorretenedores \n" +
                   $"No somos Grandes Contribuyentes"),
                    fontCustom);
                prgTitle.Alignment = Element.ALIGN_LEFT;
                prgTitle2.Alignment = Element.ALIGN_LEFT;
                cell_info.AddElement(prgTitle);
                cell_info.AddElement(prgTitle2);
                Header.AddCell(cell_info);

                PdfPCell cell_factura = new PdfPCell() { Border = 0 };
                Paragraph prgTitle3 = new Paragraph(string.Format($"FACTURA DE VENTA " + " No." + DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"].ToString(), fontCustom));
                Paragraph prgTitle4 = new Paragraph(string.Format("{0}{1}{2}",
                   $"Numeracion aprobada por la Dian  0000 17380  -   000026000 \n",
                   $"Resolucion No 18762005683346     De: 16/11/2017  \n",
                   $"IVA REGIMENCOMUN \n" +
                   $"Factura impresa por computador. Analista de medios Nit 800152955-2"),
                    fontCustom);
                prgTitle3.Alignment = Element.ALIGN_LEFT;
                prgTitle4.Alignment = Element.ALIGN_LEFT;
                cell_factura.AddElement(prgTitle3);
                cell_factura.AddElement(prgTitle4);

                Header.AddCell(cell_factura);

                PdfPTable header2 = new PdfPTable(new float[] { 0.3f, 0.4f, 0.3f });
                header2.WidthPercentage = 100;

                PdfPCell cell_cliente = new PdfPCell() { Border = 0 };
                Paragraph prgcliente = new Paragraph(string.Format("{0}\n{1}\n{2}\n{3}",
                   $"Cliente/Mandante: " + DsInvoiceAR.Tables["Customer"].Rows[0]["Name"].ToString(),
                   $"Dirección:  " + DsInvoiceAR.Tables["Customer"].Rows[0]["Address1"].ToString(),
                   $"Telefono:   " + DsInvoiceAR.Tables["Customer"].Rows[0]["PhoneNum"].ToString(),
                   $"Contacto:   " + DsInvoiceAR.Tables["Customer"].Rows[0]["Number01"].ToString()),
                    fontCustom);
                prgcliente.Alignment = Element.ALIGN_LEFT;
                cell_cliente.AddElement(prgcliente);
                header2.AddCell(cell_cliente);

                PdfPCell cell_cliente2 = new PdfPCell() { Border = 0 };
                Paragraph prgcliente2 = new Paragraph(string.Format("{0}\n{1}",
                   $"Nit/Cedula: " + DsInvoiceAR.Tables["Customer"].Rows[0]["CustID"].ToString(),
                   $"Ciudad:   " + DsInvoiceAR.Tables["Customer"].Rows[0]["City"].ToString()),
                    fontCustom);
                prgcliente2.Alignment = Element.ALIGN_CENTER;
                cell_cliente2.AddElement(prgcliente2);
                header2.AddCell(cell_cliente2);

                PdfPCell cell_fecha = new PdfPCell() { Border = 0 };
                Paragraph prgfecha = new Paragraph(string.Format("{0}\n{1}\n{2}",
                   $"Fecha Factura:        " + DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).ToString("dd/mm/yyyy"),
                   $"Fecha Vencimiento:       " + DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"]).ToString("dd/mm/yyyy"),
                   $"Orden No:       " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["OrderNum"].ToString()),
                    fontCustom);
                prgfecha.Alignment = Element.ALIGN_LEFT;
                cell_fecha.AddElement(prgfecha);
                header2.AddCell(cell_fecha);

                #endregion

                #region Body

                PdfPTable titulos_unidades = new PdfPTable(new float[] { 0.09f, 0.5f, 0.07f, 0.08f, 0.06f, 0.1f, 0.1f, });
                titulos_unidades.WidthPercentage = 100;

                PdfPCell codigo = new PdfPCell(new Phrase("Codigo", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, MinimumHeight = 15, };
                titulos_unidades.AddCell(codigo);

                PdfPCell descripcion = new PdfPCell(new Phrase("Descripción", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, MinimumHeight = 15, };
                titulos_unidades.AddCell(descripcion);

                PdfPCell cant = new PdfPCell(new Phrase("Cant.", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, MinimumHeight = 15, };
                titulos_unidades.AddCell(cant);

                PdfPCell precio = new PdfPCell(new Phrase("Precio", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, MinimumHeight = 15, };
                titulos_unidades.AddCell(precio);

                PdfPCell iva = new PdfPCell(new Phrase("Iva", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, MinimumHeight = 15, };
                titulos_unidades.AddCell(iva);

                PdfPCell retefuente = new PdfPCell(new Phrase("Rete Fuente", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, MinimumHeight = 15, };
                titulos_unidades.AddCell(retefuente);

                PdfPCell Total = new PdfPCell(new Phrase("Total", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, MinimumHeight = 15, };
                titulos_unidades.AddCell(Total);


                #endregion

                #region Unidades

                PdfPTable tableTituloUnidades = new PdfPTable(7);
                //Dimenciones.
                float[] DimencionUnidades = new float[7];
                DimencionUnidades[0] = 0.09f;//CÓDIGO
                DimencionUnidades[1] = 0.5f;//DESCRIPCION
                DimencionUnidades[2] = 0.07f;//CANTIDAD
                DimencionUnidades[3] = 0.08f;//DTO
                DimencionUnidades[4] = 0.06f;//PRECIO
                DimencionUnidades[5] = 0.1f;//PRECIO
                DimencionUnidades[6] = 0.1f;//PRECIO

                PdfPTable tableUnidades = new PdfPTable(7);
                tableUnidades.WidthPercentage = 100;
                tableUnidades.SetWidths(DimencionUnidades);

                foreach (DataRow InvoiceLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
                {
                    if (!AddUnidadesAnalistadeMedios(InvoiceLine, ref tableUnidades, fontCustom, DsInvoiceAR))
                        return false;
                }

                iTextSharp.text.pdf.PdfPCell LineaFinal = new iTextSharp.text.pdf.PdfPCell();
                LineaFinal.Colspan = 7;
                LineaFinal.Padding = 3;
                LineaFinal.Border = 1;
                LineaFinal.BorderColorBottom = BaseColor.WHITE;
                LineaFinal.HorizontalAlignment = Element.ALIGN_CENTER;
                LineaFinal.VerticalAlignment = Element.ALIGN_TOP;
                tableUnidades.AddCell(LineaFinal);

                #endregion

                #region Footer

                PdfPTable info_unidades = new PdfPTable(new float[]{0.1f, 0.9f, });
                info_unidades.WidthPercentage = 100;

                PdfPCell cell_vacio = new PdfPCell() { Border=0};
                info_unidades.AddCell(cell_vacio);

                PdfPCell cell_unidades_info = new PdfPCell(new Phrase(
                                        "INGRESO RECIBIDO PARA TERCEROS \n" +
                                        "Publisidad en Valla Aeropuerto Jose Maria Cordova-Rionegro Referencia: \n" +
                                        "INSTITUCIONAL-Chevinong, Oct 12 a Nov. 11 \n" +
                                        "..................................... \n" +
                                        "..................................... Ppto M-BTL 0977-18. \n", fontCustom))
                { HorizontalAlignment=Element.ALIGN_LEFT,Border=0};
                info_unidades.AddCell(cell_unidades_info);


                PdfPTable titulos_pie = new PdfPTable(new float[] {0.2f, 0.12f, 0.14f, 0.2f, 0.3f, });
                titulos_pie.WidthPercentage = 100;

                PdfPCell info_1 = new PdfPCell(new Phrase("Proveedor",fontCustom)) { Border = 0 };
                titulos_pie.AddCell(info_1);

                PdfPCell info_2 = new PdfPCell(new Phrase("Nit", fontCustom)) { Border = 0 };
                titulos_pie.AddCell(info_2);

                PdfPCell info_3 = new PdfPCell(new Phrase("Regimen IVA", fontCustom)) { Border = 0 };
                titulos_pie.AddCell(info_3);

                PdfPCell info_4 = new PdfPCell(new Phrase("Tipo Contribuyente", fontCustom)) { Border = 0 };
                titulos_pie.AddCell(info_4);

                PdfPCell info_5 = new PdfPCell(new Phrase("")) { Border= 0 };
                titulos_pie.AddCell(info_5);


                PdfPTable titulos_pie2 = new PdfPTable(new float[] { 0.2f, 0.12f, 0.14f, 0.2f, 0.3f, });
                titulos_pie2.WidthPercentage = 100;

                PdfPCell info_12 = new PdfPCell(new Phrase("" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["SalesRepName1"].ToString(), fontCustom)) { Border = 0 };
                titulos_pie2.AddCell(info_12);

                PdfPCell info_22 = new PdfPCell(new Phrase("" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number01"].ToString(), fontCustom)) { Border = 0 };
                titulos_pie2.AddCell(info_22);

                PdfPCell info_32 = new PdfPCell(new Phrase("" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar01"].ToString(), fontCustom)) { Border = 0 };
                titulos_pie2.AddCell(info_32);

                PdfPCell info_42 = new PdfPCell(new Phrase("" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar02"].ToString(), fontCustom)) { Border = 0 };
                titulos_pie2.AddCell(info_42);

                PdfPCell info_52 = new PdfPCell(new Phrase("")) { Border = 0 };
                titulos_pie2.AddCell(info_52);

                PdfPTable footer = new PdfPTable(new float[] { 0.7f, 0.2f, 0.1f});
                footer.WidthPercentage = 100;

                PdfPCell footer_pie = new PdfPCell(new Phrase("NO EFECTUARA RETENCION EN LA FUENTE SOBRE EL VALOR DE LA PAUTA EN PRENSA, REVISTA, RADIO Y TELEVISION.\n" +
                    "Articulo 8 decreto 1512 de mayo / 95. pagar con cheques cruzado o consignar en cuenta corrientw\n" +
                    "de Bancolombia No. 072-152955-03 a nombre de ANALISTAS DE MEDIOS TACTICA LTDA.\n" +
                    "esta factura de venta es un titulo valor segun la ley 1231 de julio 17/2008. si luego de 10 dias de recibida no ha sido devuelta se da\n" +
                    "por aceptada tras su vencimiento se cobran intereses de mora al maximo legalpermitido.\n" +
                    "Quien firma el recibir esta factura representa plenamente al desrinatario, por lo tanto este asume la responsabilidad \n", fontCustom)) {HorizontalAlignment=Element.ALIGN_LEFT};
                footer.AddCell(footer_pie);

                PdfPCell titulo_tottal = new PdfPCell(new Phrase("")) {HorizontalAlignment=Element.ALIGN_LEFT };
                Paragraph prgtottal = new Paragraph(string.Format("{0}{1}{2}{3}",
                   $"Vr. proveedores y agencia:\n",
                   $"Retencion de la fuente: \n" ,
                   $"IVa agencia: \n",
                   $"Retencion de IVA:\n" +
                   $"VR. a pagar: \n"),
                    fontCustom);
                prgtottal.Alignment = Element.ALIGN_LEFT;
                titulo_tottal.AddElement(prgtottal);
                footer.AddCell(titulo_tottal);


                PdfPCell valor_tottal = new PdfPCell(new Phrase("")) {HorizontalAlignment=Element.ALIGN_RIGHT};
                Paragraph prgvalor = new Paragraph(string.Format("{0}\n{1}\n{2}\n{3}\n{4}",
                 $"$ " + decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number02"]).ToString("N0"),
                 GetValImpuestoByID("0A", DsInvoiceAR).ToString("C0"),
                 $"$ " + decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number03"]).ToString("N0"),
                 GetValImpuestoByID("0B", DsInvoiceAR).ToString("C0"),
                 $"$ " + decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"]).ToString("N0")),
                    fontCustom);
                prgvalor.Alignment = Element.ALIGN_RIGHT;
                valor_tottal.AddElement(prgvalor);
                footer.AddCell(valor_tottal);


                PdfPTable fin_pie = new PdfPTable(new float[] {0.2f, 0.8f,} );
                fin_pie.WidthPercentage = 100;

                PdfPCell sello = new PdfPCell(new Phrase("\n\n\n\n\n______________________________\n" +
                                                         "emisor vendedor", fontCustom))
                {HorizontalAlignment=Element.ALIGN_CENTER };
                fin_pie.AddCell(sello);

                PdfPCell firma_sello = new PdfPCell(new Phrase("Acepto el contenido de la presente factura y manifiesto que he resibido la mercancia o la presentacion del sevicio descrito\n\n\n\n" +
                    "nnombre________________________________firma y sello_________________________________fecha de resibido_______________________________", 
                    fontCustom)) { HorizontalAlignment=Element.ALIGN_LEFT};
                fin_pie.AddCell(firma_sello);

                PdfPTable T_Qr = new PdfPTable(new float[] { 4.0f, 4.5f, 1.5f });
                T_Qr.WidthPercentage = 100;
                PdfPCell cell_1 = new PdfPCell(new Phrase("cufe: " + CUFE, fontCustom)) { Border=0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP, };
                T_Qr.AddCell(cell_1);
                PdfPCell cell_qr = new PdfPCell() { Border=0,};
                T_Qr.AddCell(cell_qr);
                PdfPCell cell_2 = new PdfPCell() { Border=0,};
                iTextSharp.text.Image QRPdf = iTextSharp.text.Image.GetInstance(QRInvoice, BaseColor.WHITE);
                QRPdf.ScaleAbsolute(80f, 80f);//Se cambia el tamaño de la imagen del QR
                iTextSharp.text.pdf.PdfPCell celImgQR = new iTextSharp.text.pdf.PdfPCell(QRPdf);
                celImgQR.Colspan = 1;
                celImgQR.Padding = 3;
                celImgQR.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celImgQR.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                cell_2.AddElement(QRPdf);
                T_Qr.AddCell(cell_2);

                #endregion

                #region Exit


                document.Add(Header);
                document.Add(header2);
                document.Add(titulos_unidades);
                document.Add(tableTituloUnidades);
                document.Add(tableUnidades);
                document.Add(info_unidades);
                document.Add(salto);
                document.Add(titulos_pie);
                document.Add(titulos_pie2);  
                document.Add(footer);
                document.Add(fin_pie);
                document.Add(T_Qr);

                /*PIE DE PAGINA*/
                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();
                RutaPdf = NomArchivo;
                return true;
                #endregion
            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.Message;
                return false;
            }
        }

        public bool NotaCreditoAnalistadeMedios(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {
            NomArchivo = string.Empty;
            string Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            string RutaImg = $@"{AppDomain.CurrentDomain.BaseDirectory}\Logo-Analistas-PNG-2.png";
            string RutaMarcaAgua = $@"{AppDomain.CurrentDomain.BaseDirectory}\Logo-Analistas-PNG-2.png";
            NomArchivo = NombreInvoice + ".pdf";
            try
            {
                #region Head
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.
                Document document = new Document(iTextSharp.text.PageSize.LETTER);
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;

                // step 3: we open the document     
                document.Open();

                PdfDiv divEspacio = new PdfDiv();
                divEspacio.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio.Height = 10;
                divEspacio.Width = 130;

                PdfDiv divEspacio2 = new PdfDiv();
                divEspacio2.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio2.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio2.Height = 2;
                divEspacio2.Width = 130;

                //FUENTES----------------------------------------------------------------------------------------------------------
                iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle3 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 9, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle2 = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustom = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitleFactura = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                //FUENTES---------------------------------------------------------------------------------------------------------

                System.Drawing.Image logo = null;
                var requestLogo = WebRequest.Create(RutaImg);

                using (var responseLogo = requestLogo.GetResponse())
                using (var streamLogo = responseLogo.GetResponseStream())
                {
                    logo = Bitmap.FromStream(streamLogo);
                }

                System.Drawing.Image logo2 = null;
                var requestLogo2 = WebRequest.Create(RutaImg);

                using (var responseLogo2 = requestLogo2.GetResponse())
                using (var streamLogo2 = responseLogo2.GetResponseStream())
                {
                    logo2 = Bitmap.FromStream(streamLogo2);
                }
                iTextSharp.text.Image ImgLogo2 = iTextSharp.text.Image.GetInstance(logo2, BaseColor.WHITE);
                ImgLogo2.ScaleAbsolute(180, 180);

                #endregion

                PdfPTable salto = new PdfPTable(1);
                salto.WidthPercentage = 100;
                PdfPCell cell_salto = new PdfPCell() { Border = 0, MinimumHeight = 30 };
                salto.AddCell(cell_salto);

                #region Header

                PdfPTable Header = new PdfPTable(new float[] { 0.32f, 0.3f, 0.4f, });
                Header.WidthPercentage = 100;

                PdfPCell cell_logo = new PdfPCell() { Border = 0 };
                cell_logo.AddElement(ImgLogo2);
                Header.AddCell(cell_logo);

                PdfPCell cell_info = new PdfPCell() { Border = 0 };
                Paragraph prgTitle = new Paragraph($"" + DsInvoiceAR.Tables["Company"].Rows[0]["Address1"], fontCustom);
                Paragraph prgTitle2 = new Paragraph(string.Format("{0}{1}{2}",
                   $"Telefono: " + DsInvoiceAR.Tables["Company"].Rows[0]["PhoneNum"].ToString() + $" / Fax: " + DsInvoiceAR.Tables["Company"].Rows[0]["PhoneNum"].ToString() + "\n",
                   $"NIT:  " + DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"].ToString() + "\n",
                   $"No somos Autorretenedores \n" +
                   $"No somos Grandes Contribuyentes"),
                    fontCustom);
                prgTitle.Alignment = Element.ALIGN_LEFT;
                prgTitle2.Alignment = Element.ALIGN_LEFT;
                cell_info.AddElement(prgTitle);
                cell_info.AddElement(prgTitle2);
                Header.AddCell(cell_info);

                PdfPCell cell_factura = new PdfPCell() { Border = 0 };
                Paragraph prgTitle3 = new Paragraph(string.Format($"NOTA CREDITO " + " No." + DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"].ToString(), fontCustom));
                Paragraph prgTitle4 = new Paragraph(string.Format("{0}{1}{2}",
                   $"Numeracion aprobada por la Dian  0000 17380  -   000026000 \n",
                   $"Resolucion No 18762005683346     De: 16/11/2017  \n",
                   $"IVA REGIMENCOMUN \n" +
                   $"Factura impresa por computador. Analista de medios Nit 800152955-2"),
                    fontCustom);
                prgTitle3.Alignment = Element.ALIGN_LEFT;
                prgTitle4.Alignment = Element.ALIGN_LEFT;
                cell_factura.AddElement(prgTitle3);
                cell_factura.AddElement(prgTitle4);

                Header.AddCell(cell_factura);

                PdfPTable header2 = new PdfPTable(new float[] { 0.3f, 0.4f, 0.3f });
                header2.WidthPercentage = 100;

                PdfPCell cell_cliente = new PdfPCell() { Border = 0 };
                Paragraph prgcliente = new Paragraph(string.Format("{0}\n{1}\n{2}\n{3}",
                   $"Cliente/Mandante: " + DsInvoiceAR.Tables["Customer"].Rows[0]["Name"].ToString(),
                   $"Dirección:  " + DsInvoiceAR.Tables["Customer"].Rows[0]["Address1"].ToString(),
                   $"Telefono:   " + DsInvoiceAR.Tables["Customer"].Rows[0]["PhoneNum"].ToString(),
                   $"Contacto:   " + DsInvoiceAR.Tables["Customer"].Rows[0]["Number01"].ToString()),
                    fontCustom);
                prgcliente.Alignment = Element.ALIGN_LEFT;
                cell_cliente.AddElement(prgcliente);
                header2.AddCell(cell_cliente);

                PdfPCell cell_cliente2 = new PdfPCell() { Border = 0 };
                Paragraph prgcliente2 = new Paragraph(string.Format("{0}\n{1}",
                   $"Nit/Cedula: " + DsInvoiceAR.Tables["Customer"].Rows[0]["CustID"].ToString(),
                   $"Ciudad:   " + DsInvoiceAR.Tables["Customer"].Rows[0]["City"].ToString()),
                    fontCustom);
                prgcliente2.Alignment = Element.ALIGN_CENTER;
                cell_cliente2.AddElement(prgcliente2);
                header2.AddCell(cell_cliente2);

                PdfPCell cell_fecha = new PdfPCell() { Border = 0 };
                Paragraph prgfecha = new Paragraph(string.Format("{0}\n{1}\n{2}",
                   $"Fecha Factura:        " + DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).ToString("dd/mm/yyyy"),
                   $"Fecha Vencimiento:       " + DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"]).ToString("dd/mm/yyyy"),
                   $"Orden No:       " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["OrderNum"].ToString()),
                    fontCustom);
                prgfecha.Alignment = Element.ALIGN_LEFT;
                cell_fecha.AddElement(prgfecha);
                header2.AddCell(cell_fecha);

                #endregion

                #region Body

                PdfPTable titulos_unidades = new PdfPTable(new float[] { 0.09f, 0.5f, 0.07f, 0.08f, 0.06f, 0.1f, 0.1f, });
                titulos_unidades.WidthPercentage = 100;

                PdfPCell codigo = new PdfPCell(new Phrase("Codigo", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, MinimumHeight = 15, };
                titulos_unidades.AddCell(codigo);

                PdfPCell descripcion = new PdfPCell(new Phrase("Descripción", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, MinimumHeight = 15, };
                titulos_unidades.AddCell(descripcion);

                PdfPCell cant = new PdfPCell(new Phrase("Cant.", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, MinimumHeight = 15, };
                titulos_unidades.AddCell(cant);

                PdfPCell precio = new PdfPCell(new Phrase("Precio", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, MinimumHeight = 15, };
                titulos_unidades.AddCell(precio);

                PdfPCell iva = new PdfPCell(new Phrase("Iva", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, MinimumHeight = 15, };
                titulos_unidades.AddCell(iva);

                PdfPCell retefuente = new PdfPCell(new Phrase("Rete Fuente", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, MinimumHeight = 15, };
                titulos_unidades.AddCell(retefuente);

                PdfPCell Total = new PdfPCell(new Phrase("Total", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, MinimumHeight = 15, };
                titulos_unidades.AddCell(Total);


                #endregion

                #region Unidades

                PdfPTable tableTituloUnidades = new PdfPTable(7);
                //Dimenciones.
                float[] DimencionUnidades = new float[7];
                DimencionUnidades[0] = 0.09f;//CÓDIGO
                DimencionUnidades[1] = 0.5f;//DESCRIPCION
                DimencionUnidades[2] = 0.07f;//CANTIDAD
                DimencionUnidades[3] = 0.08f;//DTO
                DimencionUnidades[4] = 0.06f;//PRECIO
                DimencionUnidades[5] = 0.1f;//PRECIO
                DimencionUnidades[6] = 0.1f;//PRECIO

                PdfPTable tableUnidades = new PdfPTable(7);
                tableUnidades.WidthPercentage = 100;
                tableUnidades.SetWidths(DimencionUnidades);

                foreach (DataRow InvoiceLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
                {
                    if (!AddUnidadesAnalistadeMedios(InvoiceLine, ref tableUnidades, fontCustom, DsInvoiceAR))
                        return false;
                }

                iTextSharp.text.pdf.PdfPCell LineaFinal = new iTextSharp.text.pdf.PdfPCell();
                LineaFinal.Colspan = 7;
                LineaFinal.Padding = 3;
                LineaFinal.Border = 1;
                LineaFinal.BorderColorBottom = BaseColor.WHITE;
                LineaFinal.HorizontalAlignment = Element.ALIGN_CENTER;
                LineaFinal.VerticalAlignment = Element.ALIGN_TOP;
                tableUnidades.AddCell(LineaFinal);

                #endregion

                #region Footer

                PdfPTable info_unidades = new PdfPTable(new float[] { 0.1f, 0.9f, });
                info_unidades.WidthPercentage = 100;

                PdfPCell cell_vacio = new PdfPCell() { Border = 0 };
                info_unidades.AddCell(cell_vacio);

                PdfPCell cell_unidades_info = new PdfPCell(new Phrase(
                                        "INGRESO RECIBIDO PARA TERCEROS \n" +
                                        "Publisidad en Valla Aeropuerto Jose Maria Cordova-Rionegro Referencia: \n" +
                                        "INSTITUCIONAL-Chevinong, Oct 12 a Nov. 11 \n" +
                                        "..................................... \n" +
                                        "..................................... Ppto M-BTL 0977-18. \n", fontCustom))
                { HorizontalAlignment = Element.ALIGN_LEFT, Border = 0 };
                info_unidades.AddCell(cell_unidades_info);


                PdfPTable titulos_pie = new PdfPTable(new float[] { 0.2f, 0.12f, 0.14f, 0.2f, 0.3f, });
                titulos_pie.WidthPercentage = 100;

                PdfPCell info_1 = new PdfPCell(new Phrase("Proveedor", fontCustom)) { Border = 0 };
                titulos_pie.AddCell(info_1);

                PdfPCell info_2 = new PdfPCell(new Phrase("Nit", fontCustom)) { Border = 0 };
                titulos_pie.AddCell(info_2);

                PdfPCell info_3 = new PdfPCell(new Phrase("Regimen IVA", fontCustom)) { Border = 0 };
                titulos_pie.AddCell(info_3);

                PdfPCell info_4 = new PdfPCell(new Phrase("Tipo Contribuyente", fontCustom)) { Border = 0 };
                titulos_pie.AddCell(info_4);

                PdfPCell info_5 = new PdfPCell(new Phrase("")) { Border = 0 };
                titulos_pie.AddCell(info_5);


                PdfPTable titulos_pie2 = new PdfPTable(new float[] { 0.2f, 0.12f, 0.14f, 0.2f, 0.3f, });
                titulos_pie2.WidthPercentage = 100;

                PdfPCell info_12 = new PdfPCell(new Phrase("" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["SalesRepName1"].ToString(), fontCustom)) { Border = 0 };
                titulos_pie2.AddCell(info_12);

                PdfPCell info_22 = new PdfPCell(new Phrase("" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number01"].ToString(), fontCustom)) { Border = 0 };
                titulos_pie2.AddCell(info_22);

                PdfPCell info_32 = new PdfPCell(new Phrase("" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar01"].ToString(), fontCustom)) { Border = 0 };
                titulos_pie2.AddCell(info_32);

                PdfPCell info_42 = new PdfPCell(new Phrase("" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar02"].ToString(), fontCustom)) { Border = 0 };
                titulos_pie2.AddCell(info_42);

                PdfPCell info_52 = new PdfPCell(new Phrase("")) { Border = 0 };
                titulos_pie2.AddCell(info_52);

                PdfPTable footer = new PdfPTable(new float[] { 0.7f, 0.2f, 0.1f });
                footer.WidthPercentage = 100;

                PdfPCell footer_pie = new PdfPCell(new Phrase("NO EFECTUARA RETENCION EN LA FUENTE SOBRE EL VALOR DE LA PAUTA EN PRENSA, REVISTA, RADIO Y TELEVISION.\n" +
                    "Articulo 8 decreto 1512 de mayo / 95. pagar con cheques cruzado o consignar en cuenta corrientw\n" +
                    "de Bancolombia No. 072-152955-03 a nombre de ANALISTAS DE MEDIOS TACTICA LTDA.\n" +
                    "esta factura de venta es un titulo valor segun la ley 1231 de julio 17/2008. si luego de 10 dias de recibida no ha sido devuelta se da\n" +
                    "por aceptada tras su vencimiento se cobran intereses de mora al maximo legalpermitido.\n" +
                    "Quien firma el recibir esta factura representa plenamente al desrinatario, por lo tanto este asume la responsabilidad \n", fontCustom))
                { HorizontalAlignment = Element.ALIGN_LEFT };
                footer.AddCell(footer_pie);

                PdfPCell titulo_tottal = new PdfPCell(new Phrase("")) { HorizontalAlignment = Element.ALIGN_LEFT };
                Paragraph prgtottal = new Paragraph(string.Format("{0}{1}{2}{3}",
                   $"Vr. proveedores y agencia:\n",
                   $"Retencion de la fuente: \n",
                   $"IVa agencia: \n",
                   $"Retencion de IVA:\n" +
                   $"VR. a pagar: \n"),
                    fontCustom);
                prgtottal.Alignment = Element.ALIGN_LEFT;
                titulo_tottal.AddElement(prgtottal);
                footer.AddCell(titulo_tottal);


                PdfPCell valor_tottal = new PdfPCell(new Phrase("")) { HorizontalAlignment = Element.ALIGN_RIGHT };
                Paragraph prgvalor = new Paragraph(string.Format("{0}\n{1}\n{2}\n{3}\n{4}",
                 $"$ " + decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number02"]).ToString("N0"),
                 GetValImpuestoByID("0A", DsInvoiceAR).ToString("C0"),
                 $"$ " + decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number03"]).ToString("N0"),
                 GetValImpuestoByID("0B", DsInvoiceAR).ToString("C0"),
                 $"$ " + decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"]).ToString("N0")),
                    fontCustom);
                prgvalor.Alignment = Element.ALIGN_RIGHT;
                valor_tottal.AddElement(prgvalor);
                footer.AddCell(valor_tottal);


                PdfPTable fin_pie = new PdfPTable(new float[] { 0.2f, 0.8f, });
                fin_pie.WidthPercentage = 100;

                PdfPCell sello = new PdfPCell(new Phrase("\n\n\n\n\n______________________________\n" +
                                                         "emisor vendedor", fontCustom))
                { HorizontalAlignment = Element.ALIGN_CENTER };
                fin_pie.AddCell(sello);

                PdfPCell firma_sello = new PdfPCell(new Phrase("Acepto el contenido de la presente factura y manifiesto que he resibido la mercancia o la presentacion del sevicio descrito\n\n\n\n" +
                    "nnombre________________________________firma y sello_________________________________fecha de resibido_______________________________",
                    fontCustom))
                { HorizontalAlignment = Element.ALIGN_LEFT };
                fin_pie.AddCell(firma_sello);

                #endregion

                #region Exit


                document.Add(Header);
                document.Add(header2);
                document.Add(titulos_unidades);
                document.Add(tableTituloUnidades);
                document.Add(tableUnidades);
                document.Add(info_unidades);
                document.Add(salto);
                document.Add(titulos_pie);
                document.Add(titulos_pie2);
                document.Add(footer);
                document.Add(fin_pie);


                /*PIE DE PAGINA*/
                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();
                RutaPdf = NomArchivo;
                return true;
                #endregion
            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.Message;
                return false;
            }
        }

        public bool NotaDebitoAnalistadeMedios(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {
            NomArchivo = string.Empty;
            string Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            string RutaImg = $@"{AppDomain.CurrentDomain.BaseDirectory}\Logo-Analistas-PNG-2.png";
            string RutaMarcaAgua = $@"{AppDomain.CurrentDomain.BaseDirectory}\Logo-Analistas-PNG-2.png";
            NomArchivo = NombreInvoice + ".pdf";
            try
            {
                #region Head
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.
                Document document = new Document(iTextSharp.text.PageSize.LETTER);
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;

                // step 3: we open the document     
                document.Open();

                PdfDiv divEspacio = new PdfDiv();
                divEspacio.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio.Height = 10;
                divEspacio.Width = 130;

                PdfDiv divEspacio2 = new PdfDiv();
                divEspacio2.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio2.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio2.Height = 2;
                divEspacio2.Width = 130;

                //FUENTES----------------------------------------------------------------------------------------------------------
                iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle3 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 9, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle2 = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustom = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitleFactura = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                //FUENTES---------------------------------------------------------------------------------------------------------

                System.Drawing.Image logo = null;
                var requestLogo = WebRequest.Create(RutaImg);

                using (var responseLogo = requestLogo.GetResponse())
                using (var streamLogo = responseLogo.GetResponseStream())
                {
                    logo = Bitmap.FromStream(streamLogo);
                }

                System.Drawing.Image logo2 = null;
                var requestLogo2 = WebRequest.Create(RutaImg);

                using (var responseLogo2 = requestLogo2.GetResponse())
                using (var streamLogo2 = responseLogo2.GetResponseStream())
                {
                    logo2 = Bitmap.FromStream(streamLogo2);
                }
                iTextSharp.text.Image ImgLogo2 = iTextSharp.text.Image.GetInstance(logo2, BaseColor.WHITE);
                ImgLogo2.ScaleAbsolute(180, 180);

                #endregion

                PdfPTable salto = new PdfPTable(1);
                salto.WidthPercentage = 100;
                PdfPCell cell_salto = new PdfPCell() { Border = 0, MinimumHeight = 30 };
                salto.AddCell(cell_salto);

                #region Header

                PdfPTable Header = new PdfPTable(new float[] { 0.32f, 0.3f, 0.4f, });
                Header.WidthPercentage = 100;

                PdfPCell cell_logo = new PdfPCell() { Border = 0 };
                cell_logo.AddElement(ImgLogo2);
                Header.AddCell(cell_logo);

                PdfPCell cell_info = new PdfPCell() { Border = 0 };
                Paragraph prgTitle = new Paragraph($"" + DsInvoiceAR.Tables["Company"].Rows[0]["Address1"], fontCustom);
                Paragraph prgTitle2 = new Paragraph(string.Format("{0}{1}{2}",
                   $"Telefono: " + DsInvoiceAR.Tables["Company"].Rows[0]["PhoneNum"].ToString() + $" / Fax: " + DsInvoiceAR.Tables["Company"].Rows[0]["PhoneNum"].ToString() + "\n",
                   $"NIT:  " + DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"].ToString() + "\n",
                   $"No somos Autorretenedores \n" +
                   $"No somos Grandes Contribuyentes"),
                    fontCustom);
                prgTitle.Alignment = Element.ALIGN_LEFT;
                prgTitle2.Alignment = Element.ALIGN_LEFT;
                cell_info.AddElement(prgTitle);
                cell_info.AddElement(prgTitle2);
                Header.AddCell(cell_info);

                PdfPCell cell_factura = new PdfPCell() { Border = 0 };
                Paragraph prgTitle3 = new Paragraph(string.Format($"NOTA DEBITO " + " No." + DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"].ToString(), fontCustom));
                Paragraph prgTitle4 = new Paragraph(string.Format("{0}{1}{2}",
                   $"Numeracion aprobada por la Dian  0000 17380  -   000026000 \n",
                   $"Resolucion No 18762005683346     De: 16/11/2017  \n",
                   $"IVA REGIMENCOMUN \n" +
                   $"Factura impresa por computador. Analista de medios Nit 800152955-2"),
                    fontCustom);
                prgTitle3.Alignment = Element.ALIGN_LEFT;
                prgTitle4.Alignment = Element.ALIGN_LEFT;
                cell_factura.AddElement(prgTitle3);
                cell_factura.AddElement(prgTitle4);

                Header.AddCell(cell_factura);

                PdfPTable header2 = new PdfPTable(new float[] { 0.3f, 0.4f, 0.3f });
                header2.WidthPercentage = 100;

                PdfPCell cell_cliente = new PdfPCell() { Border = 0 };
                Paragraph prgcliente = new Paragraph(string.Format("{0}\n{1}\n{2}\n{3}",
                   $"Cliente/Mandante: " + DsInvoiceAR.Tables["Customer"].Rows[0]["Name"].ToString(),
                   $"Dirección:  " + DsInvoiceAR.Tables["Customer"].Rows[0]["Address1"].ToString(),
                   $"Telefono:   " + DsInvoiceAR.Tables["Customer"].Rows[0]["PhoneNum"].ToString(),
                   $"Contacto:   " + DsInvoiceAR.Tables["Customer"].Rows[0]["Number01"].ToString()),
                    fontCustom);
                prgcliente.Alignment = Element.ALIGN_LEFT;
                cell_cliente.AddElement(prgcliente);
                header2.AddCell(cell_cliente);

                PdfPCell cell_cliente2 = new PdfPCell() { Border = 0 };
                Paragraph prgcliente2 = new Paragraph(string.Format("{0}\n{1}",
                   $"Nit/Cedula: " + DsInvoiceAR.Tables["Customer"].Rows[0]["CustID"].ToString(),
                   $"Ciudad:   " + DsInvoiceAR.Tables["Customer"].Rows[0]["City"].ToString()),
                    fontCustom);
                prgcliente2.Alignment = Element.ALIGN_CENTER;
                cell_cliente2.AddElement(prgcliente2);
                header2.AddCell(cell_cliente2);

                PdfPCell cell_fecha = new PdfPCell() { Border = 0 };
                Paragraph prgfecha = new Paragraph(string.Format("{0}\n{1}\n{2}",
                   $"Fecha Factura:        " + DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).ToString("dd/mm/yyyy"),
                   $"Fecha Vencimiento:       " + DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"]).ToString("dd/mm/yyyy"),
                   $"Orden No:       " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["OrderNum"].ToString()),
                    fontCustom);
                prgfecha.Alignment = Element.ALIGN_LEFT;
                cell_fecha.AddElement(prgfecha);
                header2.AddCell(cell_fecha);

                #endregion

                #region Body

                PdfPTable titulos_unidades = new PdfPTable(new float[] { 0.09f, 0.5f, 0.07f, 0.08f, 0.06f, 0.1f, 0.1f, });
                titulos_unidades.WidthPercentage = 100;

                PdfPCell codigo = new PdfPCell(new Phrase("Codigo", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, MinimumHeight = 15, };
                titulos_unidades.AddCell(codigo);

                PdfPCell descripcion = new PdfPCell(new Phrase("Descripción", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, MinimumHeight = 15, };
                titulos_unidades.AddCell(descripcion);

                PdfPCell cant = new PdfPCell(new Phrase("Cant.", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, MinimumHeight = 15, };
                titulos_unidades.AddCell(cant);

                PdfPCell precio = new PdfPCell(new Phrase("Precio", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, MinimumHeight = 15, };
                titulos_unidades.AddCell(precio);

                PdfPCell iva = new PdfPCell(new Phrase("Iva", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, MinimumHeight = 15, };
                titulos_unidades.AddCell(iva);

                PdfPCell retefuente = new PdfPCell(new Phrase("Rete Fuente", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, MinimumHeight = 15, };
                titulos_unidades.AddCell(retefuente);

                PdfPCell Total = new PdfPCell(new Phrase("Total", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, MinimumHeight = 15, };
                titulos_unidades.AddCell(Total);


                #endregion

                #region Unidades

                PdfPTable tableTituloUnidades = new PdfPTable(7);
                //Dimenciones.
                float[] DimencionUnidades = new float[7];
                DimencionUnidades[0] = 0.09f;//CÓDIGO
                DimencionUnidades[1] = 0.5f;//DESCRIPCION
                DimencionUnidades[2] = 0.07f;//CANTIDAD
                DimencionUnidades[3] = 0.08f;//DTO
                DimencionUnidades[4] = 0.06f;//PRECIO
                DimencionUnidades[5] = 0.1f;//PRECIO
                DimencionUnidades[6] = 0.1f;//PRECIO

                PdfPTable tableUnidades = new PdfPTable(7);
                tableUnidades.WidthPercentage = 100;
                tableUnidades.SetWidths(DimencionUnidades);

                foreach (DataRow InvoiceLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
                {
                    if (!AddUnidadesAnalistadeMedios(InvoiceLine, ref tableUnidades, fontCustom, DsInvoiceAR))
                        return false;
                }

                iTextSharp.text.pdf.PdfPCell LineaFinal = new iTextSharp.text.pdf.PdfPCell();
                LineaFinal.Colspan = 7;
                LineaFinal.Padding = 3;
                LineaFinal.Border = 1;
                LineaFinal.BorderColorBottom = BaseColor.WHITE;
                LineaFinal.HorizontalAlignment = Element.ALIGN_CENTER;
                LineaFinal.VerticalAlignment = Element.ALIGN_TOP;
                tableUnidades.AddCell(LineaFinal);

                #endregion

                #region Footer

                PdfPTable info_unidades = new PdfPTable(new float[] { 0.1f, 0.9f, });
                info_unidades.WidthPercentage = 100;

                PdfPCell cell_vacio = new PdfPCell() { Border = 0 };
                info_unidades.AddCell(cell_vacio);

                PdfPCell cell_unidades_info = new PdfPCell(new Phrase(
                                        "INGRESO RECIBIDO PARA TERCEROS \n" +
                                        "Publisidad en Valla Aeropuerto Jose Maria Cordova-Rionegro Referencia: \n" +
                                        "INSTITUCIONAL-Chevinong, Oct 12 a Nov. 11 \n" +
                                        "..................................... \n" +
                                        "..................................... Ppto M-BTL 0977-18. \n", fontCustom))
                { HorizontalAlignment = Element.ALIGN_LEFT, Border = 0 };
                info_unidades.AddCell(cell_unidades_info);


                PdfPTable titulos_pie = new PdfPTable(new float[] { 0.2f, 0.12f, 0.14f, 0.2f, 0.3f, });
                titulos_pie.WidthPercentage = 100;

                PdfPCell info_1 = new PdfPCell(new Phrase("Proveedor", fontCustom)) { Border = 0 };
                titulos_pie.AddCell(info_1);

                PdfPCell info_2 = new PdfPCell(new Phrase("Nit", fontCustom)) { Border = 0 };
                titulos_pie.AddCell(info_2);

                PdfPCell info_3 = new PdfPCell(new Phrase("Regimen IVA", fontCustom)) { Border = 0 };
                titulos_pie.AddCell(info_3);

                PdfPCell info_4 = new PdfPCell(new Phrase("Tipo Contribuyente", fontCustom)) { Border = 0 };
                titulos_pie.AddCell(info_4);

                PdfPCell info_5 = new PdfPCell(new Phrase("")) { Border = 0 };
                titulos_pie.AddCell(info_5);


                PdfPTable titulos_pie2 = new PdfPTable(new float[] { 0.2f, 0.12f, 0.14f, 0.2f, 0.3f, });
                titulos_pie2.WidthPercentage = 100;

                PdfPCell info_12 = new PdfPCell(new Phrase("" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["SalesRepName1"].ToString(), fontCustom)) { Border = 0 };
                titulos_pie2.AddCell(info_12);

                PdfPCell info_22 = new PdfPCell(new Phrase("" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number01"].ToString(), fontCustom)) { Border = 0 };
                titulos_pie2.AddCell(info_22);

                PdfPCell info_32 = new PdfPCell(new Phrase("" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar01"].ToString(), fontCustom)) { Border = 0 };
                titulos_pie2.AddCell(info_32);

                PdfPCell info_42 = new PdfPCell(new Phrase("" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar02"].ToString(), fontCustom)) { Border = 0 };
                titulos_pie2.AddCell(info_42);

                PdfPCell info_52 = new PdfPCell(new Phrase("")) { Border = 0 };
                titulos_pie2.AddCell(info_52);

                PdfPTable footer = new PdfPTable(new float[] { 0.7f, 0.2f, 0.1f });
                footer.WidthPercentage = 100;

                PdfPCell footer_pie = new PdfPCell(new Phrase("NO EFECTUARA RETENCION EN LA FUENTE SOBRE EL VALOR DE LA PAUTA EN PRENSA, REVISTA, RADIO Y TELEVISION.\n" +
                    "Articulo 8 decreto 1512 de mayo / 95. pagar con cheques cruzado o consignar en cuenta corrientw\n" +
                    "de Bancolombia No. 072-152955-03 a nombre de ANALISTAS DE MEDIOS TACTICA LTDA.\n" +
                    "esta factura de venta es un titulo valor segun la ley 1231 de julio 17/2008. si luego de 10 dias de recibida no ha sido devuelta se da\n" +
                    "por aceptada tras su vencimiento se cobran intereses de mora al maximo legalpermitido.\n" +
                    "Quien firma el recibir esta factura representa plenamente al desrinatario, por lo tanto este asume la responsabilidad \n", fontCustom))
                { HorizontalAlignment = Element.ALIGN_LEFT };
                footer.AddCell(footer_pie);

                PdfPCell titulo_tottal = new PdfPCell(new Phrase("")) { HorizontalAlignment = Element.ALIGN_LEFT };
                Paragraph prgtottal = new Paragraph(string.Format("{0}{1}{2}{3}",
                   $"Vr. proveedores y agencia:\n",
                   $"Retencion de la fuente: \n",
                   $"IVa agencia: \n",
                   $"Retencion de IVA:\n" +
                   $"VR. a pagar: \n"),
                    fontCustom);
                prgtottal.Alignment = Element.ALIGN_LEFT;
                titulo_tottal.AddElement(prgtottal);
                footer.AddCell(titulo_tottal);


                PdfPCell valor_tottal = new PdfPCell(new Phrase("")) { HorizontalAlignment = Element.ALIGN_RIGHT };
                Paragraph prgvalor = new Paragraph(string.Format("{0}\n{1}\n{2}\n{3}\n{4}",
                 $"$ " + decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number02"]).ToString("N0"),
                 GetValImpuestoByID("0A", DsInvoiceAR).ToString("C0"),
                 $"$ " + decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number03"]).ToString("N0"),
                 GetValImpuestoByID("0B", DsInvoiceAR).ToString("C0"),
                 $"$ " + decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"]).ToString("N0")),
                    fontCustom);
                prgvalor.Alignment = Element.ALIGN_RIGHT;
                valor_tottal.AddElement(prgvalor);
                footer.AddCell(valor_tottal);


                PdfPTable fin_pie = new PdfPTable(new float[] { 0.2f, 0.8f, });
                fin_pie.WidthPercentage = 100;

                PdfPCell sello = new PdfPCell(new Phrase("\n\n\n\n\n______________________________\n" +
                                                         "emisor vendedor", fontCustom))
                { HorizontalAlignment = Element.ALIGN_CENTER };
                fin_pie.AddCell(sello);

                PdfPCell firma_sello = new PdfPCell(new Phrase("Acepto el contenido de la presente factura y manifiesto que he resibido la mercancia o la presentacion del sevicio descrito\n\n\n\n" +
                    "nnombre________________________________firma y sello_________________________________fecha de resibido_______________________________",
                    fontCustom))
                { HorizontalAlignment = Element.ALIGN_LEFT };
                fin_pie.AddCell(firma_sello);

                #endregion

                #region Exit


                document.Add(Header);
                document.Add(header2);
                document.Add(titulos_unidades);
                document.Add(tableTituloUnidades);
                document.Add(tableUnidades);
                document.Add(info_unidades);
                document.Add(salto);
                document.Add(titulos_pie);
                document.Add(titulos_pie2);
                document.Add(footer);
                document.Add(fin_pie);


                /*PIE DE PAGINA*/
                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();
                RutaPdf = NomArchivo;
                return true;
                #endregion
            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.Message;
                return false;
            }
        }

        #endregion
    }
}
