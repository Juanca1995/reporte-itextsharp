﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
//Referenciar y usar.
using System.Data;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Web;
using System.Windows.Forms;

namespace RulesServicesDIAN2.Adquiriente
{
    public partial class pdfEstandarAR
    {

        #region Formatos de Unilac

        private void AddUnidadesGlobalSeguros(DataRow dataLine, ref PdfPTable pdfPTable, iTextSharp.text.Font font)
        {
            iTextSharp.text.pdf.PdfPCell celCodigo = new iTextSharp.text.pdf.PdfPCell(new Phrase(dataLine["LineDesc"].ToString(), font));
            celCodigo.Colspan = 1;
            celCodigo.Padding = 3;
            //celCodigo.Border = 0;
            celCodigo.Border = PdfPCell.BOTTOM_BORDER;
            //celCodigo.BorderWidthBottom = ;
            //celCodigo.BorderWidthTop = 0;
            //celCodigo.BorderWidthRight = 0;
            celCodigo.BorderColorBottom = BaseColor.WHITE;
            celCodigo.HorizontalAlignment = Element.ALIGN_LEFT;
            celCodigo.VerticalAlignment = Element.ALIGN_TOP;
            pdfPTable.AddCell(celCodigo);

            iTextSharp.text.pdf.PdfPCell celDescripcion = new iTextSharp.text.pdf.PdfPCell(new Phrase("xxxxxxx", font));
            celDescripcion.Colspan = 1;
            celDescripcion.Padding = 3;
            //celDescripcion.Border = 0;
            celDescripcion.Border = PdfPCell.BOTTOM_BORDER;
            //celDescripcion.BorderWidthBottom = 0;
            //celDescripcion.BorderWidthTop = 0;
            //celDescripcion.BorderWidthRight = 0;
            celDescripcion.BorderColorBottom = BaseColor.WHITE;
            celDescripcion.HorizontalAlignment = Element.ALIGN_RIGHT;
            celDescripcion.VerticalAlignment = Element.ALIGN_TOP;
            pdfPTable.AddCell(celDescripcion);

            iTextSharp.text.pdf.PdfPCell celCantidad = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:N0}", dataLine["SellingShipQty"].ToString()), font));
            celCantidad.Colspan = 1;
            celCantidad.Padding = 3;
            //celCantidad.Border = 0;
            celCantidad.Border = PdfPCell.BOTTOM_BORDER;
            //celCantidad.BorderWidthBottom = 1;
            //celCantidad.BorderWidthTop = 0;
            //celCantidad.BorderWidthRight = 0;
            celCantidad.BorderColorBottom = BaseColor.WHITE;
            celCantidad.HorizontalAlignment = Element.ALIGN_LEFT;
            celCantidad.VerticalAlignment = Element.ALIGN_TOP;
            pdfPTable.AddCell(celCantidad);

            iTextSharp.text.pdf.PdfPCell celUnidad = new iTextSharp.text.pdf.PdfPCell(new Phrase("xxxxx", font));
            celUnidad.Colspan = 1;
            celUnidad.Padding = 3;
            //celUnidad.Border = 0;
            celUnidad.Border = PdfPCell.BOTTOM_BORDER;
            //celUnidad.BorderWidthBottom = 0;
            //celUnidad.BorderWidthTop = 0;
            //celUnidad.BorderWidthRight = 0;
            celUnidad.BorderColorBottom = BaseColor.WHITE;
            celUnidad.HorizontalAlignment = Element.ALIGN_RIGHT;
            celUnidad.VerticalAlignment = Element.ALIGN_TOP;
            pdfPTable.AddCell(celUnidad);

            iTextSharp.text.pdf.PdfPCell celValorUnitario = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:C2}",
                Convert.ToDecimal(dataLine["UnitPrice"].ToString())), font));
            celValorUnitario.Colspan = 1;
            celValorUnitario.Padding = 3;
            //celValorUnitario.Border = 0;
            celValorUnitario.Border = PdfPCell.BOTTOM_BORDER;
            //celValorUnitario.BorderWidthBottom = 0;
            //celValorUnitario.BorderWidthTop = 0;
            //celValorUnitario.BorderWidthRight = 0;
            celValorUnitario.BorderColorBottom = BaseColor.WHITE;
            celValorUnitario.HorizontalAlignment = Element.ALIGN_RIGHT;
            celValorUnitario.VerticalAlignment = Element.ALIGN_TOP;
            pdfPTable.AddCell(celValorUnitario);

            iTextSharp.text.pdf.PdfPCell celValorTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:C2}",
                decimal.Parse(dataLine["DocExtPrice"].ToString())), font));
            celValorTotal.Colspan = 1;
            celValorTotal.Padding = 3;
            //celValorTotal.Border = 0;
            celValorTotal.Border = PdfPCell.BOTTOM_BORDER;
            //celValorTotal.BorderWidthBottom = 0;
            //celValorTotal.BorderWidthTop = 0;
            celValorTotal.BorderColorBottom = BaseColor.WHITE;
            celValorTotal.HorizontalAlignment = Element.ALIGN_RIGHT;
            celValorTotal.VerticalAlignment = Element.ALIGN_TOP;
            pdfPTable.AddCell(celValorTotal);
        }

        public bool FacturaNacionalGlobalSeguros(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {
          
            NomArchivo = string.Empty;
            string Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            string RutaImg = $@"{AppDomain.CurrentDomain.BaseDirectory}\LOGOTIPO GLOBOSEGUROS.png";
            string RutaMarcaAgua = $@"{AppDomain.CurrentDomain.BaseDirectory}\LOGOTIPO GLOBOSEGUROS.png";
            NomArchivo = NombreInvoice + ".pdf";
            try
            {
                #region Head
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.
                Document document = new Document(iTextSharp.text.PageSize.LETTER);
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;

                // step 3: we open the document     
                document.Open();

                PdfDiv divEspacio = new PdfDiv();
                divEspacio.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio.Height = 10;
                divEspacio.Width = 130;

                PdfDiv divEspacio2 = new PdfDiv();
                divEspacio2.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio2.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio2.Height = 2;
                divEspacio2.Width = 130;

                //FUENTES----------------------------------------------------------------------------------------------------------
                iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle3 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 9, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle2 = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustom = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitleFactura = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                //FUENTES---------------------------------------------------------------------------------------------------------

                System.Drawing.Image logo = null;
                var requestLogo = WebRequest.Create(RutaImg);

                using (var responseLogo = requestLogo.GetResponse())
                using (var streamLogo = responseLogo.GetResponseStream())
                {
                    logo = Bitmap.FromStream(streamLogo);
                }

                System.Drawing.Image logo2 = null;
                var requestLogo2 = WebRequest.Create(RutaImg);

                using (var responseLogo2 = requestLogo2.GetResponse())
                using (var streamLogo2 = responseLogo2.GetResponseStream())
                {
                    logo2 = Bitmap.FromStream(streamLogo2);
                }
                #endregion

                #region ENCABEZADO
                PdfPTable Header = new PdfPTable(4);
                Header.WidthPercentage = 100;
                //informacion de la empresa
                PdfPCell cell_info = new PdfPCell() { Border = 0, };
                Paragraph prgTitle = new Paragraph(string.Format("{0}\nNIT.{1}", DsInvoiceAR.Tables["Customer"].Rows[0]["CustID"],
                DsInvoiceAR.Tables["InvcHead"].Rows[0]["Company"]), fontTitle);
                Paragraph prgTitle2 = new Paragraph(string.Format("{0}\nPBX:{1}\nA.A. 35155\n{2} {3}\nwww.aldoronline.com",
                    DsInvoiceAR.Tables["Customer"].Rows[0]["Address1"].ToString(),
                    DsInvoiceAR.Tables["Customer"].Rows[0]["PhoneNum"].ToString(),
                    DsInvoiceAR.Tables["Customer"].Rows[0]["City"].ToString(),
                    DsInvoiceAR.Tables["Customer"].Rows[0]["State"].ToString()),
                    fontTitle3);
                prgTitle.Alignment = Element.ALIGN_LEFT;
                prgTitle2.Alignment = Element.ALIGN_LEFT;
                cell_info.AddElement(prgTitle);
                cell_info.AddElement(prgTitle2);
                Header.AddCell(cell_info);
                //logo
                PdfPCell cell_logo = new PdfPCell() { Border = 0, };
                iTextSharp.text.Image ImgLogo2 = iTextSharp.text.Image.GetInstance(logo2, BaseColor.WHITE);
                ImgLogo2.ScaleAbsolute(100, 100);
                cell_logo.AddElement(ImgLogo2);
                Header.AddCell(cell_logo);
                //Qr
                PdfPCell cell_Qr = new PdfPCell() { Border = 0, };
                iTextSharp.text.Image ImgQR = iTextSharp.text.Image.GetInstance(QRInvoice, BaseColor.WHITE);
                ImgQR.ScaleAbsolute(70, 70);
                cell_Qr.AddElement(ImgQR);
                Header.AddCell(cell_Qr);
                //tipo de de documento
                PdfPCell cell_factura = new PdfPCell() { Border = 0, };
                PdfPTable tableFactura = new PdfPTable(3);
                //Dimenciones.
                float[] DimencionFactura = new float[3];
                DimencionFactura[0] = 1.0F;//
                DimencionFactura[1] = 4.0F;//
                DimencionFactura[2] = 0.5F;//

                tableFactura.WidthPercentage = 100;
                tableFactura.SetWidths(DimencionFactura);

                iTextSharp.text.pdf.PdfPCell celTittle = new iTextSharp.text.pdf.PdfPCell(new Phrase("FACTURA DE VENTAS\n\n", fontTitleFactura));
                celTittle.Colspan = 3;
                celTittle.Padding = 3;
                celTittle.HorizontalAlignment = Element.ALIGN_CENTER;
                celTittle.VerticalAlignment = Element.ALIGN_TOP;
                celTittle.Border = 0;
                celTittle.BorderWidthTop = 1;
                celTittle.BorderWidthLeft = 1;
                celTittle.BorderWidthRight = 1;
                celTittle.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableFactura.AddCell(celTittle);

                iTextSharp.text.pdf.PdfPCell celNo = new iTextSharp.text.pdf.PdfPCell(new Phrase("No. ", fontTitleFactura));
                celNo.Colspan = 1;
                celNo.Padding = 5;
                celNo.HorizontalAlignment = Element.ALIGN_CENTER;
                celNo.VerticalAlignment = Element.ALIGN_TOP;
                celNo.Border = 0;
                celNo.BorderWidthLeft = 1;
                celNo.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableFactura.AddCell(celNo);

                string NumLegalFactura = string.Empty;
                if (InvoiceType == "InvoiceType")
                    NumLegalFactura = DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceRef"].ToString();
                else if (InvoiceType == "CreditNoteType")
                    NumLegalFactura = DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"].ToString();
                else
                    NumLegalFactura = DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"].ToString();

                iTextSharp.text.pdf.PdfPCell celNoFactura = new iTextSharp.text.pdf.PdfPCell(new Phrase(NumLegalFactura, fontTitleFactura));
                celNoFactura.Colspan = 1;
                celNoFactura.Padding = 5;
                celNoFactura.HorizontalAlignment = Element.ALIGN_LEFT;
                celNoFactura.VerticalAlignment = Element.ALIGN_TOP;
                //celNoFactura.Border = 1;
                celNoFactura.BackgroundColor = BaseColor.WHITE;
                tableFactura.AddCell(celNoFactura);


                iTextSharp.text.pdf.PdfPCell celEspacioNoFactura = new iTextSharp.text.pdf.PdfPCell(new Phrase(" ", fontTitleFactura));
                celEspacioNoFactura.Colspan = 1;
                celEspacioNoFactura.Border = 0;
                //celNo.Padding = 3;
                celEspacioNoFactura.BorderWidthRight = 1;
                celEspacioNoFactura.HorizontalAlignment = Element.ALIGN_CENTER;
                celEspacioNoFactura.VerticalAlignment = Element.ALIGN_TOP;
                celEspacioNoFactura.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableFactura.AddCell(celEspacioNoFactura);

                iTextSharp.text.pdf.PdfPCell celRellenoNoFactura = new iTextSharp.text.pdf.PdfPCell(new Phrase(" ", fontTitleFactura));
                celRellenoNoFactura.Colspan = 3;
                celRellenoNoFactura.Padding = 3;
                celRellenoNoFactura.Border = 0;
                celRellenoNoFactura.BorderWidthLeft = 1;
                celRellenoNoFactura.BorderWidthRight = 1;
                celRellenoNoFactura.BorderWidthBottom = 1;
                celRellenoNoFactura.BackgroundColor = BaseColor.LIGHT_GRAY;
                celRellenoNoFactura.HorizontalAlignment = Element.ALIGN_RIGHT;
                celRellenoNoFactura.VerticalAlignment = Element.ALIGN_TOP;
                tableFactura.AddCell(celRellenoNoFactura);

                iTextSharp.text.pdf.PdfPCell celConsecutivoInterno = new iTextSharp.text.pdf.PdfPCell(new Phrase("xxxxxxxxxx", fontCustom));
                celConsecutivoInterno.Colspan = 4;
                celConsecutivoInterno.BackgroundColor = BaseColor.WHITE;
                celConsecutivoInterno.HorizontalAlignment = Element.ALIGN_RIGHT;
                celConsecutivoInterno.VerticalAlignment = Element.ALIGN_TOP;
                celConsecutivoInterno.Border = 0;
                tableFactura.AddCell(celConsecutivoInterno);

                PdfDiv divNumeroFactura = new PdfDiv();
                divNumeroFactura.Float = iTextSharp.text.pdf.PdfDiv.FloatType.LEFT;
                divNumeroFactura.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divNumeroFactura.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                divNumeroFactura.PaddingLeft = 5;
                divNumeroFactura.Width = 140;
                divNumeroFactura.AddElement(tableFactura);
                cell_factura.AddElement(tableFactura);
                Header.AddCell(cell_factura);


                #endregion

                #region FACTURAR Y DESPACHAR A
                //------------------------------------------------------------------------------------------------
                PdfPTable tableFacturar = new PdfPTable(3);
                //Dimenciones.
                float[] DimencionFacturar = new float[3];
                DimencionFacturar[0] = 1.0F;//
                DimencionFacturar[1] = 0.01F;//
                DimencionFacturar[2] = 1.0F;//

                tableFacturar.WidthPercentage = 100;
                tableFacturar.SetWidths(DimencionFacturar);
                //----------------------------------------------------------------------------------------------
                PdfPTable tableFacturarA = new PdfPTable(2);
                float[] DimencionFacturarA = new float[2];
                DimencionFacturarA[0] = 0.8F;//
                DimencionFacturarA[1] = 2.0F;//

                tableFacturarA.WidthPercentage = 100;
                tableFacturarA.SetWidths(DimencionFacturarA);

                iTextSharp.text.pdf.PdfPCell celDatosFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase("", fontTitleFactura)) { Border = 0, };
                celDatosFacturarA.Colspan = 2;
                celDatosFacturarA.Padding = 3;
                celDatosFacturarA.Border = 0;
                celDatosFacturarA.BorderColorBottom = BaseColor.WHITE;
                celDatosFacturarA.HorizontalAlignment = Element.ALIGN_CENTER;
                celDatosFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celDatosFacturarA);

                iTextSharp.text.pdf.PdfPCell celTittleFacturarA = new iTextSharp.text.pdf.PdfPCell(tableFacturarA);
                tableFacturar.AddCell(celTittleFacturarA);
                //----------------------------------------------------------------------------------------------------------------------------

                iTextSharp.text.pdf.PdfPCell celEspacio2 = new iTextSharp.text.pdf.PdfPCell(new Phrase(" ", fontTitleFactura));
                celEspacio2.Border = 0;
                tableFacturar.AddCell(celEspacio2);

                //------------------------------------------------------------------------------------------

                PdfPTable tableDespacharA = new PdfPTable(2);
                float[] DimencionDespacharA = new float[2];
                DimencionDespacharA[0] = 0.8F;//
                DimencionDespacharA[1] = 2.0F;//

                tableDespacharA.WidthPercentage = 100;
                tableDespacharA.SetWidths(DimencionDespacharA);

                iTextSharp.text.pdf.PdfPCell celTittleDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase("DESPACHAR A:\n", fontTitleFactura));
                celTittleDespacharA.Colspan = 2;
                celTittleDespacharA.Padding = 3;
                celTittleDespacharA.Border = 0;
                celTittleDespacharA.BorderColorBottom = BaseColor.WHITE;
                celTittleDespacharA.HorizontalAlignment = Element.ALIGN_CENTER;
                celTittleDespacharA.VerticalAlignment = Element.ALIGN_TOP;
                tableDespacharA.AddCell(celTittleDespacharA);

                iTextSharp.text.pdf.PdfPCell celTextClienteDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase("CLIENTE:", fontTitleFactura));
                celTextClienteDespacharA.Colspan = 1;
                celTextClienteDespacharA.Padding = 3;
                celTextClienteDespacharA.Border = 0;
                celTextClienteDespacharA.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextClienteDespacharA.VerticalAlignment = Element.ALIGN_TOP;
                tableDespacharA.AddCell(celTextClienteDespacharA);

                iTextSharp.text.pdf.PdfPCell celClienteDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase("xxxxxxxxxxxxx", fontTitle2));
                celClienteDespacharA.Colspan = 1;
                celClienteDespacharA.Padding = 3;
                celClienteDespacharA.Border = 0;
                celClienteDespacharA.HorizontalAlignment = Element.ALIGN_LEFT;
                celClienteDespacharA.VerticalAlignment = Element.ALIGN_TOP;
                tableDespacharA.AddCell(celClienteDespacharA);

                iTextSharp.text.pdf.PdfPCell celTextNitDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase("NIT:", fontTitleFactura));
                celTextNitDespacharA.Colspan = 1;
                celTextNitDespacharA.Padding = 3;
                celTextNitDespacharA.Border = 0;
                celTextNitDespacharA.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextNitDespacharA.VerticalAlignment = Element.ALIGN_TOP;
                tableDespacharA.AddCell(celTextNitDespacharA);

                iTextSharp.text.pdf.PdfPCell celNitDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase("xxxxxxxxxx", fontTitle2));
                celNitDespacharA.Colspan = 1;
                celNitDespacharA.Padding = 3;
                celNitDespacharA.Border = 0;
                celNitDespacharA.HorizontalAlignment = Element.ALIGN_LEFT;
                celNitDespacharA.VerticalAlignment = Element.ALIGN_TOP;
                tableDespacharA.AddCell(celNitDespacharA);

                iTextSharp.text.pdf.PdfPCell celTextDireccionDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase("DIRECCION:", fontTitleFactura));
                celTextDireccionDespacharA.Colspan = 1;
                celTextDireccionDespacharA.Padding = 3;
                celTextDireccionDespacharA.Border = 0;
                celTextDireccionDespacharA.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextDireccionDespacharA.VerticalAlignment = Element.ALIGN_TOP;
                tableDespacharA.AddCell(celTextDireccionDespacharA);

                iTextSharp.text.pdf.PdfPCell celDireccionDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase("xxxxxxxxxxx", fontTitle2));
                celDireccionDespacharA.Colspan = 1;
                celDireccionDespacharA.Padding = 3;
                celDireccionDespacharA.Border = 0;
                celDireccionDespacharA.HorizontalAlignment = Element.ALIGN_LEFT;
                celDireccionDespacharA.VerticalAlignment = Element.ALIGN_TOP;
                tableDespacharA.AddCell(celDireccionDespacharA);

                iTextSharp.text.pdf.PdfPCell celTextTelDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase("TELEFONO:", fontTitleFactura));
                celTextTelDespacharA.Colspan = 1;
                celTextTelDespacharA.Padding = 3;
                celTextTelDespacharA.Border = 0;
                celTextTelDespacharA.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextTelDespacharA.VerticalAlignment = Element.ALIGN_TOP;
                tableDespacharA.AddCell(celTextTelDespacharA);

                iTextSharp.text.pdf.PdfPCell celTelDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase("xxxxxxxxxx", fontTitle2));
                celTelDespacharA.Colspan = 1;
                celTelDespacharA.Padding = 3;
                celTelDespacharA.Border = 0;
                celTelDespacharA.HorizontalAlignment = Element.ALIGN_LEFT;
                celTelDespacharA.VerticalAlignment = Element.ALIGN_TOP;
                tableDespacharA.AddCell(celTelDespacharA);

                iTextSharp.text.pdf.PdfPCell celTextCiudadDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase("CIUDAD:", fontTitleFactura));
                celTextCiudadDespacharA.Colspan = 1;
                celTextCiudadDespacharA.Padding = 3;
                celTextCiudadDespacharA.Border = 0;
                celTextCiudadDespacharA.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextCiudadDespacharA.VerticalAlignment = Element.ALIGN_TOP;
                tableDespacharA.AddCell(celTextCiudadDespacharA);

                iTextSharp.text.pdf.PdfPCell celCiudadDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase("xxxxxxxxxxxxx", fontTitle2));
                celCiudadDespacharA.Colspan = 1;
                celCiudadDespacharA.Padding = 3;
                celCiudadDespacharA.Border = 0;
                celCiudadDespacharA.HorizontalAlignment = Element.ALIGN_LEFT;
                celCiudadDespacharA.VerticalAlignment = Element.ALIGN_TOP;
                tableDespacharA.AddCell(celCiudadDespacharA);

                iTextSharp.text.pdf.PdfPCell celTextPaisDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase("PAIS:", fontTitleFactura));
                celTextPaisDespacharA.Colspan = 1;
                celTextPaisDespacharA.Padding = 3;
                celTextPaisDespacharA.Border = 0;
                celTextPaisDespacharA.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextPaisDespacharA.VerticalAlignment = Element.ALIGN_TOP;
                tableDespacharA.AddCell(celTextPaisDespacharA);

                iTextSharp.text.pdf.PdfPCell celPaisDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase("xxxxxxxxxxxxx", fontTitle2));
                celPaisDespacharA.Colspan = 1;
                celPaisDespacharA.Padding = 3;
                celPaisDespacharA.Border = 0;
                celPaisDespacharA.HorizontalAlignment = Element.ALIGN_LEFT;
                celPaisDespacharA.VerticalAlignment = Element.ALIGN_TOP;
                tableDespacharA.AddCell(celPaisDespacharA);

                iTextSharp.text.pdf.PdfPCell celDatosDespacharA = new iTextSharp.text.pdf.PdfPCell(tableDespacharA);
                tableFacturar.AddCell(celDatosDespacharA);
                //-------------------------------------------------------------------------------------- 
                #endregion

                #region Tabla de Detalles
                //------------------------------------------------------------------------------------------------
                PdfPTable tableDetalles = new PdfPTable(5);
                tableDetalles.PaddingTop = 20;
                //Dimenciones.
                float[] DimencionDetalles = new float[5];
                DimencionDetalles[0] = 2.5F;//
                DimencionDetalles[1] = 1.0F;//
                DimencionDetalles[2] = 1.0F;//
                DimencionDetalles[3] = 1.0F;//
                DimencionDetalles[4] = 0.8F;//

                tableDetalles.WidthPercentage = 100;
                tableDetalles.SetWidths(DimencionDetalles);

                iTextSharp.text.pdf.PdfPCell celVendedor = new iTextSharp.text.pdf.PdfPCell(new Phrase("VENDEDOR", fontTitleFactura));
                celVendedor.Colspan = 1;
                celVendedor.Padding = 3;
                //celVendedor.Border = 0;
                celVendedor.BorderColorBottom = BaseColor.WHITE;
                celVendedor.HorizontalAlignment = Element.ALIGN_CENTER;
                celVendedor.VerticalAlignment = Element.ALIGN_TOP;
                tableDetalles.AddCell(celVendedor);

                iTextSharp.text.pdf.PdfPCell celOC_Cliente = new iTextSharp.text.pdf.PdfPCell(new Phrase("RETEFUENTE", fontTitleFactura));
                celOC_Cliente.Colspan = 1;
                celOC_Cliente.Padding = 3;
                //celVendedor.Border = 0;
                celOC_Cliente.BorderColorBottom = BaseColor.WHITE;
                celOC_Cliente.HorizontalAlignment = Element.ALIGN_CENTER;
                celOC_Cliente.VerticalAlignment = Element.ALIGN_TOP;
                tableDetalles.AddCell(celOC_Cliente);

                iTextSharp.text.pdf.PdfPCell celOrdenVenta = new iTextSharp.text.pdf.PdfPCell(new Phrase("FLETE", fontTitleFactura));
                celOrdenVenta.Colspan = 1;
                celOrdenVenta.Padding = 3;
                //celVendedor.Border = 0;
                celOrdenVenta.BorderColorBottom = BaseColor.WHITE;
                celOrdenVenta.HorizontalAlignment = Element.ALIGN_CENTER;
                celOrdenVenta.VerticalAlignment = Element.ALIGN_TOP;
                tableDetalles.AddCell(celOrdenVenta);

                iTextSharp.text.pdf.PdfPCell celTerminoPago = new iTextSharp.text.pdf.PdfPCell(new Phrase("TÉRMINO DE PAGO", fontTitleFactura));
                celTerminoPago.Colspan = 1;
                celTerminoPago.Padding = 3;
                //celVendedor.Border = 0;
                celTerminoPago.BorderColorBottom = BaseColor.WHITE;
                celTerminoPago.HorizontalAlignment = Element.ALIGN_CENTER;
                celTerminoPago.VerticalAlignment = Element.ALIGN_TOP;
                tableDetalles.AddCell(celTerminoPago);

                iTextSharp.text.pdf.PdfPCell celRemision = new iTextSharp.text.pdf.PdfPCell(new Phrase("TRANSPORTADOR", fontTitleFactura));
                celRemision.Colspan = 1;
                celRemision.Padding = 3;
                //celVendedor.Border = 0;
                celRemision.BorderColorBottom = BaseColor.WHITE;
                celRemision.HorizontalAlignment = Element.ALIGN_CENTER;
                celRemision.VerticalAlignment = Element.ALIGN_TOP;
                tableDetalles.AddCell(celRemision);

                iTextSharp.text.pdf.PdfPCell celDataVendedor = new iTextSharp.text.pdf.PdfPCell(new Phrase("xxxxxxxxxxx", fontCustom));
                celDataVendedor.Colspan = 1;
                celDataVendedor.Padding = 3;
                //celVendedor.Border = 0;
                celDataVendedor.BorderColorBottom = BaseColor.WHITE;
                celDataVendedor.HorizontalAlignment = Element.ALIGN_CENTER;
                celDataVendedor.VerticalAlignment = Element.ALIGN_TOP;
                tableDetalles.AddCell(celDataVendedor);

                iTextSharp.text.pdf.PdfPCell celDataOC_Cliente = new iTextSharp.text.pdf.PdfPCell(new Phrase("xxxxxxxxxx", fontCustom));
                celDataOC_Cliente.Colspan = 1;
                celDataOC_Cliente.Padding = 3;
                //celVendedor.Border = 0;
                celDataOC_Cliente.BorderColorBottom = BaseColor.WHITE;
                celDataOC_Cliente.HorizontalAlignment = Element.ALIGN_CENTER;
                celDataOC_Cliente.VerticalAlignment = Element.ALIGN_TOP;
                tableDetalles.AddCell(celDataOC_Cliente);

                iTextSharp.text.pdf.PdfPCell celDataOrdenVenta = new iTextSharp.text.pdf.PdfPCell(new Phrase(DsInvoiceAR.Tables["InvcHead"].Rows[0]["OrderNum"].ToString(),
                    fontCustom));
                celDataOrdenVenta.Colspan = 1;
                celDataOrdenVenta.Padding = 3;
                //celVendedor.Border = 0;
                celDataOrdenVenta.BorderColorBottom = BaseColor.WHITE;
                celDataOrdenVenta.HorizontalAlignment = Element.ALIGN_CENTER;
                celDataOrdenVenta.VerticalAlignment = Element.ALIGN_TOP;
                tableDetalles.AddCell(celDataOrdenVenta);

                iTextSharp.text.pdf.PdfPCell celDataTerminoPago = new iTextSharp.text.pdf.PdfPCell(new Phrase("xxxxxxxxxxxxxxxx", fontCustom));
                celDataTerminoPago.Colspan = 1;
                celDataTerminoPago.Padding = 3;
                //celVendedor.Border = 0;
                celDataTerminoPago.BorderColorBottom = BaseColor.WHITE;
                celDataTerminoPago.HorizontalAlignment = Element.ALIGN_CENTER;
                celDataTerminoPago.VerticalAlignment = Element.ALIGN_TOP;
                tableDetalles.AddCell(celDataTerminoPago);

                iTextSharp.text.pdf.PdfPCell celDataRemision = new iTextSharp.text.pdf.PdfPCell(new Phrase("xxxxxxxxxxxx", fontCustom));
                celDataRemision.Colspan = 1;
                celDataRemision.Padding = 3;
                //celVendedor.Border = 0;
                celDataRemision.BorderColorBottom = BaseColor.WHITE;
                celDataRemision.HorizontalAlignment = Element.ALIGN_CENTER;
                celDataRemision.VerticalAlignment = Element.ALIGN_TOP;
                tableDetalles.AddCell(celDataRemision);

                //-------------------------------------------------------------------

                PdfPTable tableDetalles2 = new PdfPTable(3);
                tableDetalles2.WidthPercentage = 100;

                PdfPTable tableFechaFactura = new PdfPTable(2);
                float[] dimecionesTablaFecha = new float[2];
                dimecionesTablaFecha[0] = 0.7F;
                dimecionesTablaFecha[1] = 1.0F;
                tableFechaFactura.SetWidths(dimecionesTablaFecha);

                iTextSharp.text.pdf.PdfPCell celTextFechaFactura = new iTextSharp.text.pdf.PdfPCell(new Phrase("FECHA FACTURA: ",
                    fontTitle));
                celTextFechaFactura.Colspan = 1;
                celTextFechaFactura.Padding = 3;
                celTextFechaFactura.Border = 0;
                celTextFechaFactura.BorderColorBottom = BaseColor.WHITE;
                celTextFechaFactura.HorizontalAlignment = Element.ALIGN_CENTER;
                celTextFechaFactura.VerticalAlignment = Element.ALIGN_CENTER;
                tableFechaFactura.AddCell(celTextFechaFactura);

                iTextSharp.text.pdf.PdfPCell celFechaFactura = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:MM/dd/yyyy}", DateTime.Parse(DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"].ToString())),
                fontCustom));
                celFechaFactura.Colspan = 1;
                celFechaFactura.Padding = 3;
                celFechaFactura.PaddingTop = 4;
                celFechaFactura.Border = 0;
                celFechaFactura.BorderColorBottom = BaseColor.WHITE;
                celFechaFactura.HorizontalAlignment = Element.ALIGN_LEFT;
                celFechaFactura.VerticalAlignment = Element.ALIGN_CENTER;
                tableFechaFactura.AddCell(celFechaFactura);

                PdfPCell _celFechaFactura = new PdfPCell(tableFechaFactura);
                tableDetalles2.AddCell(_celFechaFactura);


                PdfPTable tableFechaVencimiento = new PdfPTable(2);
                float[] dmTablaFechaVencimiento = new float[2];
                dmTablaFechaVencimiento[0] = 1.0F;
                dmTablaFechaVencimiento[1] = 1.0F;
                tableFechaVencimiento.SetWidths(dmTablaFechaVencimiento);

                iTextSharp.text.pdf.PdfPCell celTextFechaVencimiento = new iTextSharp.text.pdf.PdfPCell(new Phrase("FECHA VENCIMIENTO: ",
                    fontTitle));
                celTextFechaVencimiento.Colspan = 1;
                celTextFechaVencimiento.Padding = 3;
                celTextFechaVencimiento.Border = 0;
                celTextFechaVencimiento.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextFechaVencimiento.VerticalAlignment = Element.ALIGN_TOP;
                tableFechaVencimiento.AddCell(celTextFechaVencimiento);

                iTextSharp.text.pdf.PdfPCell celFechaVencimiento = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:MM/dd/yyyy}",
                   DateTime.Parse(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"].ToString())),
                fontCustom));
                celFechaVencimiento.Colspan = 1;
                celFechaVencimiento.Padding = 4;
                celFechaVencimiento.Border = 0;
                celFechaVencimiento.BorderColorBottom = BaseColor.WHITE;
                celFechaVencimiento.HorizontalAlignment = Element.ALIGN_LEFT;
                celFechaVencimiento.VerticalAlignment = Element.ALIGN_TOP;
                tableFechaVencimiento.AddCell(celFechaVencimiento);

                PdfPCell _celFechaFacturaVencimiento = new PdfPCell(tableFechaVencimiento);
                tableDetalles2.AddCell(_celFechaFacturaVencimiento);

                PdfPTable tableMoneda = new PdfPTable(2);
                float[] dimecionesTablaMoneda = new float[2];
                dimecionesTablaMoneda[0] = 0.4F;
                dimecionesTablaMoneda[1] = 1.0F;
                tableMoneda.SetWidths(dimecionesTablaMoneda);

                iTextSharp.text.pdf.PdfPCell celTextMoneda = new iTextSharp.text.pdf.PdfPCell(new Phrase("MONEDA: ",
                    fontTitle));
                celTextMoneda.Colspan = 1;
                celTextMoneda.Padding = 3;
                celTextMoneda.Border = 0;
                celTextMoneda.BorderColorBottom = BaseColor.WHITE;
                celTextMoneda.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextMoneda.VerticalAlignment = Element.ALIGN_TOP;
                tableMoneda.AddCell(celTextMoneda);

                iTextSharp.text.pdf.PdfPCell celMoneda = new iTextSharp.text.pdf.PdfPCell(new Phrase(DsInvoiceAR.Tables["InvcHead"].Rows[0]["CurrencyCode"].ToString(),
                fontCustom));
                celMoneda.Colspan = 1;
                celMoneda.Padding = 4;
                celMoneda.Border = 0;
                celMoneda.BorderColorBottom = BaseColor.WHITE;
                celMoneda.HorizontalAlignment = Element.ALIGN_LEFT;
                celMoneda.VerticalAlignment = Element.ALIGN_TOP;
                tableMoneda.AddCell(celMoneda);

                PdfPCell _celMoneda = new PdfPCell(tableMoneda);
                tableDetalles2.AddCell(_celMoneda);

                #endregion

                #region Tabla Unidades
                PdfPTable tableTituloUnidades = new PdfPTable(6);
                //Dimenciones.
                float[] DimencionUnidades = new float[6];
                DimencionUnidades[0] = 1.0F;//
                DimencionUnidades[1] = 3.0F;//
                DimencionUnidades[2] = 1.0F;//
                DimencionUnidades[3] = 0.5F;//
                DimencionUnidades[4] = 1.0F;//
                DimencionUnidades[5] = 1.5F;//

                tableTituloUnidades.WidthPercentage = 100;
                tableTituloUnidades.SetWidths(DimencionUnidades);

                iTextSharp.text.pdf.PdfPCell celCodigo = new iTextSharp.text.pdf.PdfPCell(new Phrase("CÓDIGO", fontTitleFactura));
                celCodigo.Colspan = 1;
                celCodigo.Padding = 3;
                //celCodigo.Border = 1;
                celCodigo.BorderWidthRight = 0;
                //celCodigo.BorderColorBottom = BaseColor.WHITE;
                celCodigo.HorizontalAlignment = Element.ALIGN_CENTER;
                celCodigo.VerticalAlignment = Element.ALIGN_TOP;
                celCodigo.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableTituloUnidades.AddCell(celCodigo);

                iTextSharp.text.pdf.PdfPCell celDescripcion = new iTextSharp.text.pdf.PdfPCell(new Phrase("DESCRIPCIÓN", fontTitleFactura));
                celDescripcion.Colspan = 1;
                celDescripcion.Padding = 3;
                //celDescripcion.Border = 1;
                celDescripcion.BorderWidthRight = 0;
                //celDescripcion.BorderColorBottom = BaseColor.WHITE;
                celDescripcion.HorizontalAlignment = Element.ALIGN_CENTER;
                celDescripcion.VerticalAlignment = Element.ALIGN_TOP;
                celDescripcion.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableTituloUnidades.AddCell(celDescripcion);

                iTextSharp.text.pdf.PdfPCell celCantidad = new iTextSharp.text.pdf.PdfPCell(new Phrase("CANTIDAD", fontTitleFactura));
                celCantidad.Colspan = 1;
                celCantidad.Padding = 3;
                //celVendedor.Border = 0;
                celCantidad.BorderWidthRight = 0;
                //celCantidad.BorderColorBottom = BaseColor.WHITE;
                celCantidad.HorizontalAlignment = Element.ALIGN_CENTER;
                celCantidad.VerticalAlignment = Element.ALIGN_TOP;
                celCantidad.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableTituloUnidades.AddCell(celCantidad);

                iTextSharp.text.pdf.PdfPCell celUnidad = new iTextSharp.text.pdf.PdfPCell(new Phrase("UNIDAD", fontTitleFactura));
                celUnidad.Colspan = 1;
                celUnidad.Padding = 3;
                //celVendedor.Border = 0;
                celUnidad.BorderWidthRight = 0;
                //celUnidad.BorderColorBottom = BaseColor.WHITE;
                celUnidad.HorizontalAlignment = Element.ALIGN_CENTER;
                celUnidad.VerticalAlignment = Element.ALIGN_TOP;
                celUnidad.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableTituloUnidades.AddCell(celUnidad);

                iTextSharp.text.pdf.PdfPCell celValorUnitario = new iTextSharp.text.pdf.PdfPCell(new Phrase("VALOR UNITARIO", fontTitleFactura));
                celValorUnitario.Colspan = 1;
                celValorUnitario.Padding = 3;
                //celVendedor.Border = 0;
                celValorUnitario.BorderWidthRight = 0;
                //celValorUnitario.BorderColorBottom = BaseColor.WHITE;
                celValorUnitario.HorizontalAlignment = Element.ALIGN_CENTER;
                celValorUnitario.VerticalAlignment = Element.ALIGN_TOP;
                celValorUnitario.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableTituloUnidades.AddCell(celValorUnitario);

                iTextSharp.text.pdf.PdfPCell celValorTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase("VALOR TOTAL", fontTitleFactura));
                celValorTotal.Colspan = 1;
                celValorTotal.Padding = 3;
                //celVendedor.Border = 0;
                //celValorTotal.BorderColorBottom = BaseColor.WHITE;
                celValorTotal.HorizontalAlignment = Element.ALIGN_CENTER;
                celValorTotal.VerticalAlignment = Element.ALIGN_TOP;
                celValorTotal.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableTituloUnidades.AddCell(celValorTotal);

                PdfPTable tableUnidades = new PdfPTable(6);
                tableUnidades.WidthPercentage = 100;
                tableUnidades.SetWidths(DimencionUnidades);

                foreach (DataRow InvoiceLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
                {
                    //    if (!AddUnidadesGlobalSeguros(InvoiceLine, ref tableItems, fuenteDatos, DsInvoiceAR.Tables["InvcTax"]))
                    //        return false;
                    AddUnidadesFRAL(InvoiceLine, ref tableUnidades, fontCustom);
                }

                iTextSharp.text.pdf.PdfPCell LineaFinal = new iTextSharp.text.pdf.PdfPCell();
                LineaFinal.Colspan = 6;
                LineaFinal.Padding = 3;
                LineaFinal.Border = 1;
                LineaFinal.BorderColorBottom = BaseColor.WHITE;
                LineaFinal.HorizontalAlignment = Element.ALIGN_CENTER;
                LineaFinal.VerticalAlignment = Element.ALIGN_TOP;
                tableUnidades.AddCell(LineaFinal);

                //------------------------------------------------------------------------------------------------
                iTextSharp.text.Font fontEspacio = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 0.3F, iTextSharp.text.Font.NORMAL);
                PdfPCell celEspacio3 = new PdfPCell(new Phrase("  ", fontEspacio));
                celEspacio3.Border = 0;

                PdfPTable tableTotalesObs = new PdfPTable(3);

                float[] DimencionTotalesObs = new float[3];
                DimencionTotalesObs[0] = 2.5F;//
                DimencionTotalesObs[1] = 0.01F;//
                DimencionTotalesObs[2] = 1.0F;//

                tableTotalesObs.WidthPercentage = 100;
                tableTotalesObs.SetWidths(DimencionTotalesObs);

                PdfPTable _tableObs = new PdfPTable(1);
                _tableObs.WidthPercentage = 100;

                iTextSharp.text.pdf.PdfPCell celValorLetras = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("VALORES EN LETRA: {0}",
                    Nroenletras(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"].ToString())), fontTitleFactura));
                celValorLetras.Colspan = 1;
                celValorLetras.Padding = 3;
                //celValorLetras.Border = 0;
                celValorLetras.BorderColorBottom = BaseColor.WHITE;
                celValorLetras.HorizontalAlignment = Element.ALIGN_LEFT;
                celValorLetras.VerticalAlignment = Element.ALIGN_TOP;

                iTextSharp.text.pdf.PdfPCell celValorAsegurado = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("X: {0:C2}",
                   decimal.Parse(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubTotal"].ToString())), fontTitle2));
                celValorAsegurado.Colspan = 1;
                celValorAsegurado.Padding = 5;
                //celValorLetras.Border = 0;
                celValorAsegurado.BorderColorBottom = BaseColor.WHITE;
                celValorAsegurado.HorizontalAlignment = Element.ALIGN_LEFT;
                celValorAsegurado.VerticalAlignment = Element.ALIGN_TOP;

                iTextSharp.text.pdf.PdfPCell celObservaciones = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("OBSERVACIONES:\n{0}",
                    DsInvoiceAR.Tables["InvcHead"].Rows[0]["ObservacionCliente_c"].ToString()), fontTitleFactura));
                celObservaciones.Colspan = 1;
                celObservaciones.Padding = 3;
                //celValorLetras.Border = 0;
                celObservaciones.BorderColorBottom = BaseColor.WHITE;
                celObservaciones.HorizontalAlignment = Element.ALIGN_LEFT;
                celObservaciones.VerticalAlignment = Element.ALIGN_TOP;

                _tableObs.AddCell(celValorLetras);
                _tableObs.AddCell(celEspacio3);
                _tableObs.AddCell(celValorAsegurado);
                _tableObs.AddCell(celEspacio3);
                _tableObs.AddCell(celObservaciones);

                iTextSharp.text.pdf.PdfPCell _celObs = new iTextSharp.text.pdf.PdfPCell(_tableObs);
                _celObs.Border = 0;
                tableTotalesObs.AddCell(_celObs);
                //----------------------------------------------------------------------------------------------------------------------------

                iTextSharp.text.pdf.PdfPCell _celEspacio2 = new iTextSharp.text.pdf.PdfPCell(new Phrase(" ", fontTitleFactura));
                _celEspacio2.Border = 0;
                tableTotalesObs.AddCell(_celEspacio2);

                //------------------------------------------------------------------------------------------

                PdfPTable _tableTotales = new PdfPTable(2);
                float[] _DimencionTotales = new float[2];
                _DimencionTotales[0] = 1.5F;//
                _DimencionTotales[1] = 2.5F;//

                _tableTotales.WidthPercentage = 100;
                _tableTotales.SetWidths(_DimencionTotales);

                iTextSharp.text.pdf.PdfPCell _celTextSubTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase("SUBTOTAL:", fontTitleFactura));
                _celTextSubTotal.Colspan = 1;
                _celTextSubTotal.Padding = 3;
                _celTextSubTotal.PaddingBottom = 5;
                _celTextSubTotal.Border = 0;
                _celTextSubTotal.BorderWidthRight = 1;
                _celTextSubTotal.BorderColorBottom = BaseColor.WHITE;
                _celTextSubTotal.HorizontalAlignment = Element.ALIGN_LEFT;
                _celTextSubTotal.VerticalAlignment = Element.ALIGN_TOP;
                _tableTotales.AddCell(_celTextSubTotal);

                iTextSharp.text.pdf.PdfPCell _celSubTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:C2}",
                    decimal.Parse(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubTotal"].ToString())), fontTitle2));
                _celSubTotal.Colspan = 1;
                _celSubTotal.Padding = 3;
                _celSubTotal.PaddingBottom = 5;
                _celSubTotal.Border = 0;
                _celSubTotal.BorderColorBottom = BaseColor.WHITE;
                _celSubTotal.HorizontalAlignment = Element.ALIGN_RIGHT;
                _celSubTotal.VerticalAlignment = Element.ALIGN_TOP;
                _tableTotales.AddCell(_celSubTotal);

                iTextSharp.text.pdf.PdfPCell _celTextDescuentos = new iTextSharp.text.pdf.PdfPCell(new Phrase("DESCUENTOS:", fontTitleFactura));
                _celTextDescuentos.Colspan = 1;
                _celTextDescuentos.Padding = 3;
                _celTextDescuentos.PaddingBottom = 5;
                _celTextDescuentos.Border = 0;
                _celTextDescuentos.BorderWidthRight = 1;
                _celTextDescuentos.BorderColorBottom = BaseColor.WHITE;
                _celTextDescuentos.HorizontalAlignment = Element.ALIGN_LEFT;
                _celTextDescuentos.VerticalAlignment = Element.ALIGN_TOP;
                _tableTotales.AddCell(_celTextDescuentos);

                iTextSharp.text.pdf.PdfPCell _celDescuentos = new iTextSharp.text.pdf.PdfPCell(new Phrase("xxxxxxxxxx", fontTitle2));
                _celDescuentos.Colspan = 1;
                _celDescuentos.Padding = 3;
                _celDescuentos.PaddingBottom = 5;
                _celDescuentos.Border = 0;
                _celDescuentos.BorderColorBottom = BaseColor.WHITE;
                _celDescuentos.HorizontalAlignment = Element.ALIGN_RIGHT;
                _celDescuentos.VerticalAlignment = Element.ALIGN_TOP;
                _tableTotales.AddCell(_celDescuentos);

                iTextSharp.text.pdf.PdfPCell _celTextSubTotal2 = new iTextSharp.text.pdf.PdfPCell(new Phrase("SUBTOTAL2:", fontTitleFactura));
                _celTextSubTotal2.Colspan = 1;
                _celTextSubTotal2.Padding = 3;
                _celTextSubTotal2.PaddingBottom = 5;
                _celTextSubTotal2.Border = 0;
                _celTextSubTotal2.BorderWidthRight = 1;
                _celTextSubTotal2.BorderColorBottom = BaseColor.WHITE;
                _celTextSubTotal2.HorizontalAlignment = Element.ALIGN_LEFT;
                _celTextSubTotal2.VerticalAlignment = Element.ALIGN_TOP;
                _tableTotales.AddCell(_celTextSubTotal2);

                iTextSharp.text.pdf.PdfPCell _celSubTotal2 = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:C2}",
                    decimal.Parse(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DocTaxAmt"].ToString())), fontTitle2));
                _celSubTotal2.Colspan = 1;
                _celSubTotal2.Padding = 3;
                _celSubTotal2.PaddingBottom = 5;
                _celSubTotal2.Border = 0;
                _celSubTotal2.BorderColorBottom = BaseColor.WHITE;
                _celSubTotal2.HorizontalAlignment = Element.ALIGN_RIGHT;
                _celSubTotal2.VerticalAlignment = Element.ALIGN_TOP;
                _tableTotales.AddCell(_celSubTotal2);

                iTextSharp.text.pdf.PdfPCell _celTextIva = new iTextSharp.text.pdf.PdfPCell(new Phrase("IVA:", fontTitleFactura));
                _celTextIva.Colspan = 1;
                _celTextIva.Padding = 3;
                _celTextIva.PaddingBottom = 5;
                _celTextIva.Border = 0;
                _celTextIva.BorderWidthRight = 1;
                //_celTextIva.BorderColorBottom = BaseColor.BLACK;
                _celTextIva.HorizontalAlignment = Element.ALIGN_LEFT;
                _celTextIva.VerticalAlignment = Element.ALIGN_TOP;
                _tableTotales.AddCell(_celTextIva);

                iTextSharp.text.pdf.PdfPCell _celIva = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:C2}",
                    decimal.Parse(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DocWHTaxAmt"].ToString())), fontTitle2));
                _celIva.Colspan = 1;
                _celIva.Padding = 3;
                _celIva.PaddingBottom = 5;
                _celIva.Border = 0;
                //_celIva.BorderColorBottom = BaseColor.BLACK;
                _celIva.HorizontalAlignment = Element.ALIGN_RIGHT;
                _celIva.VerticalAlignment = Element.ALIGN_TOP;
                _tableTotales.AddCell(_celIva);

                iTextSharp.text.pdf.PdfPCell _celTextTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase("TOTAL:", fontTitleFactura));
                _celTextTotal.Colspan = 1;
                _celTextTotal.Padding = 3;
                _celTextTotal.PaddingBottom = 5;
                _celTextTotal.Border = 0;
                _celTextTotal.BorderWidthRight = 1;
                _celTextTotal.BorderWidthTop = 1;
                _celTextTotal.HorizontalAlignment = Element.ALIGN_LEFT;
                _celTextTotal.VerticalAlignment = Element.ALIGN_TOP;
                _tableTotales.AddCell(_celTextTotal);

                iTextSharp.text.pdf.PdfPCell _celTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:C2}",
                    decimal.Parse(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"].ToString())), fontTitleFactura));
                _celTotal.Colspan = 1;
                _celTotal.Padding = 3;
                _celTotal.PaddingBottom = 5;
                _celTotal.Border = 0;
                _celTotal.BorderWidthTop = 1;
                _celTotal.HorizontalAlignment = Element.ALIGN_RIGHT;
                _celTotal.VerticalAlignment = Element.ALIGN_TOP;
                _tableTotales.AddCell(_celTotal);

                iTextSharp.text.pdf.PdfPCell _celTotales = new iTextSharp.text.pdf.PdfPCell(_tableTotales);
                tableTotalesObs.AddCell(_celTotales);

                //------------------------------------------------------------------------
                PdfPTable tableResolucion = new PdfPTable(1);
                tableResolucion.WidthPercentage = 100;
                string strResolucion = "";
                iTextSharp.text.pdf.PdfPCell _celResolucion = new iTextSharp.text.pdf.PdfPCell(new Phrase(strResolucion, fontTitleFactura));
                _celResolucion.Colspan = 1;
                _celResolucion.Padding = 3;
                //_celResolucion.Border = 0;
                _celResolucion.HorizontalAlignment = Element.ALIGN_JUSTIFIED_ALL;
                _celResolucion.VerticalAlignment = Element.ALIGN_TOP;
                tableResolucion.AddCell(_celResolucion);

                //----------------------------------------------------------------------
                PdfPTable tableFirmas = new PdfPTable(5);

                float[] DimencionFirmas = new float[5];
                DimencionFirmas[0] = 1.0F;//
                DimencionFirmas[1] = 0.02F;//
                DimencionFirmas[2] = 1.0F;//
                DimencionFirmas[3] = 0.02F;//
                DimencionFirmas[4] = 1.4F;//

                tableFirmas.WidthPercentage = 100;
                tableFirmas.SetWidths(DimencionFirmas);

                PdfPTable tableDespacahdoPor = new PdfPTable(1);
                tableDespacahdoPor.WidthPercentage = 100;
                iTextSharp.text.pdf.PdfPCell celDespachadoPor = new iTextSharp.text.pdf.PdfPCell(new Phrase("", fontTitleFactura));
                celDespachadoPor.Colspan = 1;
                celDespachadoPor.Padding = 3;
                celDespachadoPor.Border = 0;
                celDespachadoPor.HorizontalAlignment = Element.ALIGN_CENTER;
                celDespachadoPor.VerticalAlignment = Element.ALIGN_TOP;
                tableDespacahdoPor.AddCell(celDespachadoPor);

                iTextSharp.text.pdf.PdfPCell _celDespachadoPor = new iTextSharp.text.pdf.PdfPCell(tableDespacahdoPor);
                tableFirmas.AddCell(_celDespachadoPor);
                //----------------------------------------------------------------------------------------------------------------------------

                tableFirmas.AddCell(celEspacio2);

                //------------------------------------------------------------------------------------------

                PdfPTable tableFirmaConductor = new PdfPTable(1);
                tableFirmaConductor.WidthPercentage = 100;
                iTextSharp.text.pdf.PdfPCell celFirmaConductor = new iTextSharp.text.pdf.PdfPCell(new Phrase("", fontTitleFactura));
                celFirmaConductor.Colspan = 1;
                celFirmaConductor.Padding = 3;
                celFirmaConductor.Border = 0;
                celFirmaConductor.HorizontalAlignment = Element.ALIGN_CENTER;
                celFirmaConductor.VerticalAlignment = Element.ALIGN_TOP;
                tableFirmaConductor.AddCell(celFirmaConductor);

                iTextSharp.text.pdf.PdfPCell _celFirmaConductor = new iTextSharp.text.pdf.PdfPCell(tableFirmaConductor);
                tableFirmas.AddCell(_celFirmaConductor);

                //-------------------------------------------------------------------------------------
                tableFirmas.AddCell(celEspacio2);
                //-------------------------------------------------------------------------------------

                PdfPTable tableSelloCliente = new PdfPTable(1);
                //tableSelloCliente.WidthPercentage = 100;

                iTextSharp.text.pdf.PdfPCell cel1SelloCliente = new iTextSharp.text.pdf.PdfPCell(new Phrase("Apreciado cliente este documento " +
                    "no es endolsable", fontTitleFactura));
                cel1SelloCliente.Colspan = 1;
                cel1SelloCliente.Padding = 5;
                cel1SelloCliente.Border = 1;
                cel1SelloCliente.BorderWidthBottom = 1;
                cel1SelloCliente.HorizontalAlignment = Element.ALIGN_LEFT;
                cel1SelloCliente.VerticalAlignment = Element.ALIGN_TOP;
                tableSelloCliente.AddCell(cel1SelloCliente);

                iTextSharp.text.pdf.PdfPCell cel2SelloFechaRecibido = new iTextSharp.text.pdf.PdfPCell(new Phrase("Fecha de recibido:", fontTitleFactura));

                cel2SelloFechaRecibido.Colspan = 1;
                cel2SelloFechaRecibido.Padding = 3;
                cel2SelloFechaRecibido.Border = 0;
                cel2SelloFechaRecibido.HorizontalAlignment = Element.ALIGN_LEFT;
                cel2SelloFechaRecibido.VerticalAlignment = Element.ALIGN_TOP;
                tableSelloCliente.AddCell(cel2SelloFechaRecibido);

                iTextSharp.text.pdf.PdfPCell cel2SelloNombre = new iTextSharp.text.pdf.PdfPCell(new Phrase("Nombre:", fontTitleFactura));

                cel2SelloNombre.Colspan = 1;
                cel2SelloNombre.Padding = 3;
                cel2SelloNombre.Border = 0;
                cel2SelloNombre.HorizontalAlignment = Element.ALIGN_LEFT;
                cel2SelloNombre.VerticalAlignment = Element.ALIGN_TOP;
                tableSelloCliente.AddCell(cel2SelloNombre);

                iTextSharp.text.pdf.PdfPCell cel2SelloId = new iTextSharp.text.pdf.PdfPCell(new Phrase("Identificacion:", fontTitleFactura));

                cel2SelloId.Colspan = 1;
                cel2SelloId.Padding = 3;
                cel2SelloId.Border = 0;
                cel2SelloId.HorizontalAlignment = Element.ALIGN_LEFT;
                cel2SelloId.VerticalAlignment = Element.ALIGN_TOP;
                tableSelloCliente.AddCell(cel2SelloId);

                iTextSharp.text.pdf.PdfPCell cel2SelloFirma = new iTextSharp.text.pdf.PdfPCell(new Phrase("Firma:\n", fontTitleFactura));

                cel2SelloFirma.Colspan = 1;
                cel2SelloFirma.Padding = 3;
                cel2SelloFirma.Border = 0;
                cel2SelloFirma.HorizontalAlignment = Element.ALIGN_LEFT;
                cel2SelloFirma.VerticalAlignment = Element.ALIGN_TOP;
                tableSelloCliente.AddCell(cel2SelloFirma);

                iTextSharp.text.pdf.PdfPCell cel3SelloCliente = new iTextSharp.text.pdf.PdfPCell(new Phrase("______________________________________________________\n\n" +
                    "RECIBIDO CONFORME FIRMA Y SELLO DEL CLIENTE", fontTitleFactura));

                cel3SelloCliente.Colspan = 1;
                cel3SelloCliente.Padding = 3;
                cel3SelloCliente.Border = 0;
                cel3SelloCliente.HorizontalAlignment = Element.ALIGN_CENTER;
                cel3SelloCliente.VerticalAlignment = Element.ALIGN_TOP;
                tableSelloCliente.AddCell(cel3SelloCliente);

                iTextSharp.text.pdf.PdfPCell _celSelloCliente = new iTextSharp.text.pdf.PdfPCell(tableSelloCliente);
                //_celSelloCliente.Border = 0;
                //_celSelloCliente.BorderColor = BaseColor.WHITE;
                tableFirmas.AddCell(_celSelloCliente);

                PdfPTable origin = new PdfPTable(1);
                origin.WidthPercentage = 100;
                PdfPCell origuinal = new PdfPCell(new Phrase("-ORIGINAL-")) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER };
                origin.AddCell(origuinal);
                #endregion

                #region Exit

                document.Add(Header);
                document.Add(divEspacio);
                document.Add(tableFacturar);
                document.Add(divEspacio);
                document.Add(tableDetalles);
                document.Add(tableDetalles2);
                document.Add(divEspacio2);
                document.Add(tableTituloUnidades);
                document.Add(tableUnidades);
                document.Add(divEspacio2);
                document.Add(divEspacio2);
                document.Add(tableTotalesObs);
                document.Add(divEspacio2);
                document.Add(divEspacio2);
                document.Add(tableResolucion);
                document.Add(divEspacio2);
                document.Add(divEspacio2);
                document.Add(tableFirmas);
                document.Add(divEspacio2);
                document.Add(origin);

                //document.Add(divTextBancoCheque);
                /*PIE DE PAGINA*/
                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();
                RutaPdf = NomArchivo;
                return true;
                #endregion
            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.Message;
                return false;
            }          
        }

        public bool NotaCreditoGlobalSeguros(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {
            
            NomArchivo = string.Empty;
            string Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            string RutaImg = $@"{AppDomain.CurrentDomain.BaseDirectory}\LOGOTIPO GLOBOSEGUROS.png";
            string RutaMarcaAgua = $@"{AppDomain.CurrentDomain.BaseDirectory}\LOGOTIPO GLOBOSEGUROS.png";
            NomArchivo = NombreInvoice + ".pdf";
            try
            {
                #region Head
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.
                Document document = new Document(iTextSharp.text.PageSize.LETTER);
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;

                // step 3: we open the document     
                document.Open();

                PdfDiv divEspacio = new PdfDiv();
                divEspacio.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio.Height = 10;
                divEspacio.Width = 130;

                PdfDiv divEspacio2 = new PdfDiv();
                divEspacio2.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio2.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio2.Height = 2;
                divEspacio2.Width = 130;

                //FUENTES----------------------------------------------------------------------------------------------------------
                iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle3 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 9, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle2 = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustom = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitleFactura = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                //FUENTES---------------------------------------------------------------------------------------------------------

                System.Drawing.Image logo = null;
                var requestLogo = WebRequest.Create(RutaImg);

                using (var responseLogo = requestLogo.GetResponse())
                using (var streamLogo = responseLogo.GetResponseStream())
                {
                    logo = Bitmap.FromStream(streamLogo);
                }

                System.Drawing.Image logo2 = null;
                var requestLogo2 = WebRequest.Create(RutaImg);

                using (var responseLogo2 = requestLogo2.GetResponse())
                using (var streamLogo2 = responseLogo2.GetResponseStream())
                {
                    logo2 = Bitmap.FromStream(streamLogo2);
                }

                iTextSharp.text.Image ImgLogo2 = iTextSharp.text.Image.GetInstance(logo2, BaseColor.WHITE);
                ImgLogo2.ScaleAbsolute(130, 130);

                #endregion

                #region Header

                PdfPTable header = new PdfPTable(1);
                header.WidthPercentage = 100;
                PdfPCell encabezado = new PdfPCell() { Border = 0, };

                //agregamos todos los datos del encabezado en en la celda encabezado
                PdfPTable info1 = new PdfPTable(new float[] { 0.25f, 0.75f });
                info1.WidthPercentage = 100;

                //agregamos logo
                PdfPCell cell_logo = new PdfPCell() { Border = 0, };
                cell_logo.AddElement(ImgLogo2);
                info1.AddCell(cell_logo);

                PdfPCell info2 = new PdfPCell() { Border = 0, };

                PdfPTable info = new PdfPTable(new float[] { 0.35f, 0.30f, 0.35f, });
                info.WidthPercentage = 100;

                //egregamos la informaion tributaria
                PdfPCell cell_info = new PdfPCell() { Border = 0, };
                Paragraph prgTitle = new Paragraph(string.Format("{0}\nNIT.{1}", DsInvoiceAR.Tables["Customer"].Rows[0]["CustID"], DsInvoiceAR.Tables["InvcHead"].Rows[0]["Company"]), fontTitle);
                Paragraph prgTitle2 = new Paragraph(string.Format("{0}\nPBX:{1}\nA.A. 35155\n{2} {3}\nwww.aldoronline.com",
                                                                    DsInvoiceAR.Tables["Customer"].Rows[0]["Address1"].ToString(),
                                                                    DsInvoiceAR.Tables["Customer"].Rows[0]["PhoneNum"].ToString(),
                                                                    DsInvoiceAR.Tables["Customer"].Rows[0]["City"].ToString(),
                                                                    DsInvoiceAR.Tables["Customer"].Rows[0]["State"].ToString()), fontTitle2);
                prgTitle.Alignment = Element.ALIGN_LEFT;
                prgTitle2.Alignment = Element.ALIGN_LEFT;
                cell_info.AddElement(prgTitle);
                cell_info.AddElement(prgTitle2);
                info.AddCell(cell_info);

                //agregamos el codigo Qr
                PdfPCell cell_Qr = new PdfPCell() { Border = 0, };
                iTextSharp.text.Image ImgQR = iTextSharp.text.Image.GetInstance(QRInvoice, BaseColor.WHITE);
                ImgQR.ScaleAbsolute(70, 70);
                cell_Qr.AddElement(ImgQR);
                info.AddCell(cell_Qr);

                //agregamos numeode factura 
                PdfPCell cell_factura = new PdfPCell() { Border = 0, };
                Paragraph prgTitle3 = new Paragraph(string.Format("{0}\nNIT.{1}", DsInvoiceAR.Tables["Customer"].Rows[0]["CustID"], DsInvoiceAR.Tables["InvcHead"].Rows[0]["Company"]), fontTitle2);
                Paragraph prgTitle4 = new Paragraph(string.Format("{0}\nPBX:{1}\nA.A. 35155\n{2} {3}\nwww.aldoronline.com",
                                                                    DsInvoiceAR.Tables["Customer"].Rows[0]["Address1"].ToString(),
                                                                    DsInvoiceAR.Tables["Customer"].Rows[0]["PhoneNum"].ToString(),
                                                                    DsInvoiceAR.Tables["Customer"].Rows[0]["City"].ToString(),
                                                                    DsInvoiceAR.Tables["Customer"].Rows[0]["State"].ToString()), fontTitle2);
                prgTitle3.Alignment = Element.ALIGN_RIGHT;
                prgTitle4.Alignment = Element.ALIGN_RIGHT;
                cell_factura.AddElement(prgTitle3);
                cell_factura.AddElement(prgTitle4);
                info.AddCell(cell_factura);

                info2.AddElement(info);


                PdfPTable info3 = new PdfPTable(new float[] { 0.35f, 0.30f, 0.35f, });
                info3.WidthPercentage = 100;


                //egregamos la informaion tributaria
                PdfPCell cell_info3 = new PdfPCell() { Border = 0, };
                Paragraph prgTitle33 = new Paragraph(string.Format("{0}\nNIT.{1}", DsInvoiceAR.Tables["Customer"].Rows[0]["CustID"], DsInvoiceAR.Tables["InvcHead"].Rows[0]["Company"]), fontTitle2);
                Paragraph prgTitle23 = new Paragraph(string.Format("{0}\nPBX:{1}\nA.A.",
                                                                    DsInvoiceAR.Tables["Customer"].Rows[0]["Address1"].ToString(),
                                                                    DsInvoiceAR.Tables["Customer"].Rows[0]["State"].ToString()), fontTitle2);
                prgTitle33.Alignment = Element.ALIGN_LEFT;
                prgTitle23.Alignment = Element.ALIGN_LEFT;
                cell_info3.AddElement(prgTitle33);
                cell_info3.AddElement(prgTitle23);
                info3.AddCell(cell_info3);

                //agregamos el codigo Qr
                PdfPCell cell_factura5 = new PdfPCell() { Border = 0, };
                Paragraph prgTitle5 = new Paragraph(string.Format("{0}\nNIT.{1}", DsInvoiceAR.Tables["Customer"].Rows[0]["CustID"], DsInvoiceAR.Tables["InvcHead"].Rows[0]["Company"]), fontTitle2);
                Paragraph prgTitle51 = new Paragraph(string.Format("{0}\nPBX:{1}\nA.A.",
                                                                    DsInvoiceAR.Tables["Customer"].Rows[0]["Address1"].ToString(),
                                                                    DsInvoiceAR.Tables["Customer"].Rows[0]["State"].ToString()), fontTitle2);
                prgTitle5.Alignment = Element.ALIGN_CENTER;
                prgTitle51.Alignment = Element.ALIGN_CENTER;
                cell_factura5.AddElement(prgTitle5);
                cell_factura5.AddElement(prgTitle51);
                info3.AddCell(cell_factura5);

                //agregamos numeode factura 
                PdfPCell cell_factura4 = new PdfPCell() { Border = 0, };
                Paragraph prgTitle34 = new Paragraph(string.Format("{0}\nNIT.{1}", DsInvoiceAR.Tables["Customer"].Rows[0]["CustID"], DsInvoiceAR.Tables["InvcHead"].Rows[0]["Company"]), fontTitle2);
                Paragraph prgTitle43 = new Paragraph(string.Format("{0}\nPBX:{0}\nA.A. ",
                                                                    DsInvoiceAR.Tables["Customer"].Rows[0]["State"].ToString()), fontTitle2);
                prgTitle34.Alignment = Element.ALIGN_RIGHT;
                prgTitle43.Alignment = Element.ALIGN_RIGHT;
                cell_factura4.AddElement(prgTitle34);
                cell_factura4.AddElement(prgTitle43);
                info3.AddCell(cell_factura4);


                info2.AddElement(info3);

                info1.AddCell(info2);
                encabezado.AddElement(info1);
                header.AddCell(encabezado);

                #endregion

                #region Body

                PdfPTable Body = new PdfPTable(5);
                Body.WidthPercentage = 100;

                PdfPCell fecha_factura = new PdfPCell();
                //agregamos la fechas de la facrura
                PdfPTable titulo_fecha = new PdfPTable(1);
                titulo_fecha.WidthPercentage = 100;
                PdfPCell titulo_fecha1 = new PdfPCell(new Phrase("FECHA FACTURA", fontTitle)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, };
                titulo_fecha.AddCell(titulo_fecha1);
                fecha_factura.AddElement(titulo_fecha);

                //agregamos la fechas de la facrura
                PdfPTable valor_fecha = new PdfPTable(1);
                valor_fecha.WidthPercentage = 100;
                PdfPCell valor_fecha1 = new PdfPCell(new Phrase("xxxxxxx", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, };
                valor_fecha.AddCell(valor_fecha1);
                fecha_factura.AddElement(valor_fecha);

                Body.AddCell(fecha_factura);

                PdfPCell fecha_vencimiento = new PdfPCell();
                //agregamos la fechas de elvensimiento
                PdfPTable titulo_bencimiento = new PdfPTable(1);
                titulo_bencimiento.WidthPercentage = 100;
                PdfPCell titulo_bencimiento1 = new PdfPCell(new Phrase("FECHA VENCIMIENTO", fontTitle)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, };
                titulo_bencimiento.AddCell(titulo_bencimiento1);
                fecha_vencimiento.AddElement(titulo_bencimiento);

                //agregamos la fechas de elvensimiento
                PdfPTable valor_bencimiento = new PdfPTable(1);
                valor_bencimiento.WidthPercentage = 100;
                PdfPCell valor_bencimiento1 = new PdfPCell(new Phrase("xxxxxxx", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, };
                valor_bencimiento.AddCell(valor_bencimiento1);
                fecha_vencimiento.AddElement(valor_bencimiento);

                Body.AddCell(fecha_vencimiento);

                PdfPCell vendedor = new PdfPCell();
                //agregamos el vendedor
                PdfPTable titulo_vendedor = new PdfPTable(1);
                titulo_vendedor.WidthPercentage = 100;
                PdfPCell titulo_vendedor1 = new PdfPCell(new Phrase("VENDEDOR", fontTitle)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, };
                titulo_vendedor.AddCell(titulo_vendedor1);
                vendedor.AddElement(titulo_vendedor);

                //agregamos el vendedor
                PdfPTable valor_vendedor = new PdfPTable(1);
                valor_vendedor.WidthPercentage = 100;
                PdfPCell valor_vendedor1 = new PdfPCell(new Phrase("xxxxxxx", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, };
                valor_vendedor.AddCell(valor_vendedor1);
                vendedor.AddElement(valor_vendedor);

                Body.AddCell(vendedor);

                PdfPCell forma_pago = new PdfPCell();

                PdfPTable titulo_pago1 = new PdfPTable(1);
                titulo_pago1.WidthPercentage = 100;
                PdfPCell titulo_pogo = new PdfPCell(new Phrase("VENDEDOR", fontTitle)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, };
                titulo_pago1.AddCell(titulo_pogo);
                forma_pago.AddElement(titulo_pago1);

                //agregamos el vendedor
                PdfPTable valor_pago = new PdfPTable(1);
                valor_pago.WidthPercentage = 100;
                PdfPCell valor_pago1 = new PdfPCell(new Phrase("xxxxxxx", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, };
                valor_pago.AddCell(valor_pago1);
                forma_pago.AddElement(valor_pago);

                Body.AddCell(forma_pago);

                PdfPCell transportador = new PdfPCell();
                //agregamos el vendedor
                PdfPTable titulo_transportador = new PdfPTable(1);
                titulo_transportador.WidthPercentage = 100;
                PdfPCell titulo_transportador1 = new PdfPCell(new Phrase("TRANSPORTADOR", fontTitle)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, };
                titulo_transportador.AddCell(titulo_transportador1);
                transportador.AddElement(titulo_transportador);

                PdfPTable valor_transportador = new PdfPTable(1);
                valor_transportador.WidthPercentage = 100;
                PdfPCell valor_transportador1 = new PdfPCell(new Phrase("xxxxxxx", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, };
                valor_transportador.AddCell(valor_transportador1);
                transportador.AddElement(valor_transportador);
                Body.AddCell(transportador);

                //agregamos una leyenda 
                PdfPTable leyenda = new PdfPTable(1);
                leyenda.WidthPercentage = 100;
                PdfPCell leyendas = new PdfPCell(new Phrase("Resolución DIAN No. 18762008997356 de 2018-07-04 Prefijo UN- Numeración 11664.0000 a la 20000.0000, vigencia 24 meses. Impresión por computador", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER };
                leyenda.AddCell(leyendas);



                //agregamos los titulos de los detalles
                PdfPTable detalles = new PdfPTable(new float[] { 0.40f, 0.14f, 0.14f, 0.14f, 0.14f, 0.14f, });
                detalles.WidthPercentage = 100;

                PdfPCell DESCRIPCIÓN = new PdfPCell(new Phrase("DESCRIPCIÓN", fontTitle)) { HorizontalAlignment = Element.ALIGN_LEFT, MinimumHeight = 15, VerticalAlignment = Element.ALIGN_BOTTOM };
                PdfPCell CANTIDAD = new PdfPCell(new Phrase("CANTIDAD", fontTitle)) { HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_BOTTOM };
                PdfPCell UND = new PdfPCell(new Phrase("UND", fontTitle)) { HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_BOTTOM };
                PdfPCell vUNITARIO = new PdfPCell(new Phrase("VR. UNITARIO", fontTitle)) { HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_BOTTOM };
                PdfPCell IVA = new PdfPCell(new Phrase("IVA (%)", fontTitle)) { HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_BOTTOM };
                PdfPCell TOTAL = new PdfPCell(new Phrase("TOTAL", fontTitle)) { HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_BOTTOM };

                detalles.AddCell(DESCRIPCIÓN);
                detalles.AddCell(CANTIDAD);
                detalles.AddCell(UND);
                detalles.AddCell(vUNITARIO);
                detalles.AddCell(IVA);
                detalles.AddCell(TOTAL);

                #endregion

                #region unidades

                PdfPTable tableTituloUnidades = new PdfPTable(6);
                //Dimenciones.
                float[] DimencionUnidades = new float[6];
                DimencionUnidades[0] = 0.40f;//
                DimencionUnidades[1] = 0.14f;//
                DimencionUnidades[2] = 0.14f;//
                DimencionUnidades[3] = 0.14f;//
                DimencionUnidades[4] = 0.14f;//
                DimencionUnidades[5] = 0.14f;//

                PdfPTable tableUnidades = new PdfPTable(6);
                tableUnidades.WidthPercentage = 100;
                tableUnidades.SetWidths(DimencionUnidades);

                foreach (DataRow InvoiceLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
                {
                    AddUnidadesGlobalSeguros(InvoiceLine, ref tableUnidades, fontCustom);
                }

                iTextSharp.text.pdf.PdfPCell LineaFinal = new iTextSharp.text.pdf.PdfPCell();
                LineaFinal.Colspan = 6;
                LineaFinal.Padding = 3;
                LineaFinal.Border = 1;
                LineaFinal.BorderColorBottom = BaseColor.WHITE;
                LineaFinal.HorizontalAlignment = Element.ALIGN_CENTER;
                LineaFinal.VerticalAlignment = Element.ALIGN_TOP;
                tableUnidades.AddCell(LineaFinal);


                #endregion

                #region Footer

                PdfPTable pie = new PdfPTable(new float[] { 0.7f, 0.3f, });
                pie.WidthPercentage = 100;
                //agregamos  una tabla con las notas 
                PdfPCell nota = new PdfPCell() { Border = 0, };

                PdfPTable nota_tabla = new PdfPTable(1);
                nota_tabla.WidthPercentage = 100;
                PdfPCell nota_tabla1 = new PdfPCell(new Phrase("Nota:", fontTitle)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP, MinimumHeight = 20, };
                nota_tabla.AddCell(nota_tabla1);
                nota.AddElement(nota_tabla);

                //agregamos el valor en letras 
                PdfPTable valoe_l = new PdfPTable(1);
                valoe_l.WidthPercentage = 100;
                PdfPCell valoe_l1 = new PdfPCell(new Phrase("VALOR EN LETRA:", fontTitle)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP, MinimumHeight = 20, };
                valoe_l.AddCell(valoe_l1);
                nota.AddElement(valoe_l);

                //agregamos  una tabla con la leyenda 
                PdfPTable leyenda_pie = new PdfPTable(1);
                leyenda_pie.WidthPercentage = 100;
                PdfPCell leyenda_pie1 = new PdfPCell(new Phrase("UNILAC es una entidfad sin ánimo de lucro. Esta factura de venta tiene calidad de titulo valor ley 1231 del" +
                                                               " 2008; La factura se considera irrevocablemente aceptada por el comprador o beneficiario, si no reclamare" +
                                                               "en contra de su contenido dentro de los(10) dias calendarios siguientes a su recepción, ley 1231 de 2008;" +
                                                               " Se hace constar que la firma y sello de una persona distinta al comprador implica que dicha persona esta" +
                                                               " autorizada expresamente por el comprador para firmar, confesar la deuda y obligar al comprador. Factura" +
                                                               " Computador generada por Software de Arco Sistemas Limitada.Nit 830.011.704 - 5." +
                                                               " Factura por Computador Impresa por UNILAC NIT 900.107.388 - 8:", fontTitle))
                { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP, MinimumHeight = 20, };
                leyenda_pie.AddCell(leyenda_pie1);
                nota.AddElement(leyenda_pie);


                pie.AddCell(nota);


                PdfPCell totales = new PdfPCell() { Border = 0, };

                PdfPTable valores_total = new PdfPTable(2);
                valores_total.WidthPercentage = 100;

                PdfPCell titulo_totales = new PdfPCell() { Border = 0, };

                //agregamos el valor subtotal
                PdfPTable subtotal = new PdfPTable(1);
                subtotal.WidthPercentage = 100;
                PdfPCell subtotal1 = new PdfPCell(new Phrase("SUBTOTAL", fontTitle)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_BOTTOM, MinimumHeight = 30, };
                subtotal.AddCell(subtotal1);
                titulo_totales.AddElement(subtotal);

                //agregamos el valor DESCUENTO
                PdfPTable DESCUENTO = new PdfPTable(1);
                DESCUENTO.WidthPercentage = 100;
                PdfPCell DESCUENTO1 = new PdfPCell(new Phrase("DESCUENTO", fontTitle)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_BOTTOM, };
                DESCUENTO.AddCell(DESCUENTO1);
                titulo_totales.AddElement(DESCUENTO);

                //agregamos el valor IVA
                PdfPTable IVA_v = new PdfPTable(1);
                IVA_v.WidthPercentage = 100;
                PdfPCell IVA1 = new PdfPCell(new Phrase("IVA", fontTitle)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_BOTTOM, };
                IVA_v.AddCell(IVA1);
                titulo_totales.AddElement(IVA_v);

                //agregamos el valor RETEFUENTE
                PdfPTable RETEFUENTE = new PdfPTable(1);
                RETEFUENTE.WidthPercentage = 100;
                PdfPCell RETEFUENTE1 = new PdfPCell(new Phrase("RETEFUENTE", fontTitle)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_BOTTOM, };
                RETEFUENTE.AddCell(RETEFUENTE1);
                titulo_totales.AddElement(RETEFUENTE);

                //agregamos el valor FLETE
                PdfPTable FLETE = new PdfPTable(1);
                FLETE.WidthPercentage = 100;
                PdfPCell FLETE1 = new PdfPCell(new Phrase("FLETE", fontTitle)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_BOTTOM, };
                FLETE.AddCell(FLETE1);
                titulo_totales.AddElement(FLETE);

                //agregamos el valor TOTAL
                PdfPTable TOTAL_V = new PdfPTable(1);
                TOTAL_V.WidthPercentage = 100;
                PdfPCell TOTAL_V1 = new PdfPCell(new Phrase("TOTAL", fontTitle)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_BOTTOM, };
                TOTAL_V.AddCell(TOTAL_V1);
                titulo_totales.AddElement(TOTAL_V);

                PdfPCell valores_totales = new PdfPCell() { Border = 0, };

                //agregamos el valor subtotal
                PdfPTable subtotalF = new PdfPTable(1);
                subtotalF.WidthPercentage = 100;
                PdfPCell subtotalF1 = new PdfPCell(new Phrase("XXXXX", fontTitle)) { Border = 0, HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_BOTTOM, MinimumHeight = 30, };
                subtotalF.AddCell(subtotalF1);
                valores_totales.AddElement(subtotalF);

                //agregamos el valor DESCUENTO
                PdfPTable DESCUENTOF = new PdfPTable(1);
                DESCUENTOF.WidthPercentage = 100;
                PdfPCell DESCUENTOF1 = new PdfPCell(new Phrase("XXXXX", fontTitle)) { Border = 0, HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_BOTTOM, };
                DESCUENTOF.AddCell(DESCUENTOF1);
                valores_totales.AddElement(DESCUENTOF);

                //agregamos el valor IVA
                PdfPTable IVA_f = new PdfPTable(1);
                IVA_f.WidthPercentage = 100;
                PdfPCell IVA_f1 = new PdfPCell(new Phrase("XXXXX", fontTitle)) { Border = 0, HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_BOTTOM, };
                IVA_f.AddCell(IVA_f1);
                valores_totales.AddElement(IVA_f);

                //agregamos el valor RETEFUENTE
                PdfPTable RETEFUENTEf = new PdfPTable(1);
                RETEFUENTEf.WidthPercentage = 100;
                PdfPCell RETEFUENTEf1 = new PdfPCell(new Phrase("XXXXX", fontTitle)) { Border = 0, HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_BOTTOM, };
                RETEFUENTEf.AddCell(RETEFUENTEf1);
                valores_totales.AddElement(RETEFUENTEf);

                //agregamos el valor FLETE
                PdfPTable FLETEf = new PdfPTable(1);
                FLETEf.WidthPercentage = 100;
                PdfPCell FLETEf1 = new PdfPCell(new Phrase("XXXXX", fontTitle)) { Border = 0, HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_BOTTOM, };
                FLETEf.AddCell(FLETEf1);
                valores_totales.AddElement(FLETEf);

                //agregamos el valor TOTAL
                PdfPTable TOTAL_f = new PdfPTable(1);
                TOTAL_f.WidthPercentage = 100;
                PdfPCell TOTAL_f1 = new PdfPCell(new Phrase("XXXXX", fontTitle)) { Border = 0, HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_BOTTOM, };
                TOTAL_f.AddCell(TOTAL_f1);
                valores_totales.AddElement(TOTAL_f);

                valores_total.AddCell(titulo_totales);
                valores_total.AddCell(valores_totales);

                totales.AddElement(valores_total);
                pie.AddCell(totales);

                PdfPTable tabla_cufe = new PdfPTable(1);
                tabla_cufe.WidthPercentage = 100;
                PdfPCell cufe = new PdfPCell(new Phrase("CUFE:" + CUFE, fontTitle)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_BOTTOM };
                tabla_cufe.AddCell(cufe);

                #endregion

                #region Exit

                document.Add(header);
                document.Add(Body);
                document.Add(leyenda);
                document.Add(detalles);
                document.Add(tableTituloUnidades);
                document.Add(tableUnidades);
                document.Add(pie);
                document.Add(tabla_cufe);

                //document.Add(divTextBancoCheque);
                /*PIE DE PAGINA*/
                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();
                RutaPdf = NomArchivo;
                return true;
                #endregion
            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.Message;
                return false;
            }     
        }
        #endregion

    }
}
