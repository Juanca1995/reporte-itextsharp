﻿namespace RulesServicesDIAN2.Adquiriente
{
    using iTextSharp.text;
    using iTextSharp.text.pdf;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Diagnostics;
    using System.Drawing;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.NetworkInformation;
    using System.Text;
    using System.Threading.Tasks;
    using System.Web;
    using System.Windows.Forms;

    public partial class pdfEstandarAR
    {
        #region Montipetrol
        public bool FacturaNacionalMontinpetrol(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {
            bool esLocal = Helpers.Compartido.EsLocal;
            string Ruta = string.Empty;
            string RutaImg = string.Empty;
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            if (esLocal)
            {
                Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
                RutaImg = $@"{AppDomain.CurrentDomain.BaseDirectory}\LogoMontipetrol.jpg";
            }
            else
            {
                Ruta = HttpContext.Current.Server.MapPath("~/PDF/" + NIT + "/");
                RutaImg = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";
            }
            NomArchivo = string.Empty;
            NomArchivo = NombreInvoice + ".pdf";
            //string RutaBanner = "https://tinyurl.com/yb3ugbb5/";
            try
            {
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.
                //Document document = new Document(iTextSharp.text.PageSize.LETTER);
                Document document = new Document(iTextSharp.text.PageSize.LETTER, 20f, 15f, 250f, 287f);
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;
                //--- Agregando los elementos estaticos
                //Celda Logo
                System.Drawing.Image Logo;
                //System.Drawing.Image LogoBanner = null;

                var request = WebRequest.Create(RutaImg);

                using (var response = request.GetResponse())
                using (var stream = response.GetResponseStream())
                {
                    Logo = Bitmap.FromStream(stream);
                }

                PdfPTable Encabezado = Montinpetrol.FacturaNacional.Encabezado(DsInvoiceAR, Logo, CUFE);
                PdfPTable DatosCliente = Montinpetrol.FacturaNacional.DatosCliente(DsInvoiceAR, QRInvoice);
                PdfPTable PiePagina = Montinpetrol.FacturaNacional.PiePagina(DsInvoiceAR, Logo, ref strError);
                PdfPTable DetalleHead = Montinpetrol.FacturaNacional.DetalleHead();

                writer.PageEvent = new Helpers.Montinpetrol.EventPageMontinpetrol(Encabezado, DatosCliente, DetalleHead, PiePagina);

                // step 3: we open the document
                document.Open();

                //---- Agregando elementos
                document.Add(Montinpetrol.FacturaNacional.Detalles(DsInvoiceAR, ref strError));
                document.Add(separator);
                document.Add(separator);

                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();
                Helpers.Simex.AddPageNumberPagToSimexExp($"{Ruta}{NomArchivo}", dimxSimex, dimYSimex + 10, Helpers.Fuentes.SimexLeyenda);
                RutaPdf = NomArchivo;
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("1. Error al crear PDF: " + ex);
                return false;
            }
        }
        public class Montinpetrol
        {
            public class FacturaNacional
            {
                public static PdfPTable Encabezado(DataSet ds, System.Drawing.Image Logo, string CUFE)
                {
                    Helpers.Plantilla_6.EncabezadoDatos datos = new Helpers.Plantilla_6.EncabezadoDatos(ds);
                    datos.Title = "MONTINPETROL S A";
                    datos.NewWidht = 140f;
                    datos.NewHeight = 80f;
                    datos.Sucursal1 = "";
                    datos.Sucursal2 = "";
                    datos.Sucursal3 = "";
                    datos.Sucursal4 = "";
                    return Helpers.Plantilla_6.Encabezado(ds, Logo, datos);
                }
                public static PdfPTable DatosCliente(DataSet ds, System.Drawing.Image QRInvoice)
                {
                    return Helpers.Montinpetrol.DatosCliente(ds, QRInvoice);
                }
                public static PdfPTable DetalleHead()
                {
                    return Helpers.Plantilla_6.DetalleHead();
                }
                public static PdfPTable Detalles(DataSet ds, ref string strError)
                {
                    return Helpers.Plantilla_6.Detalles(ds, ref strError);
                }
                public static PdfPTable PiePagina(DataSet ds, System.Drawing.Image Logo, ref string strError)
                {
                    Helpers.Plantilla_6.DatosPlantilla datos = new Helpers.Plantilla_6.DatosPlantilla();
                    datos.PiePagina_GrandesValue = "Resolución 014097 Diciembre 30 de 2010";
                    datos.PiePagina_AutoreeTittle = "NO SOMOS AUTORRETENEDORES DE RENTA";
                    datos.PiePagina_AutorreValue = "\r\n";
                    datos.PiePagina_IcaValue = "De acuerdo con el Articulo 49 de 1990 las actividades industriales solo pagaran impuesto de industria y comercio en el municipio donde se encuentre ubicada la fábrica o planta industrial, por lo tanto clientes fuera del Municipio de Funza, Cundinamarca deben abstenerse de realizar Retención de ICA  en la Actividad  CIIU 3110";
                    datos.PiePagina_Pago = @"FAVOR CONSIGNAR A NOMBRE DE Solinoff Corporation SA
en: Itau Corpbanca Colombia SA Cuenta Corriente 038019915 
y Bancolombia Cuenta Corriente 071-085827-34

ENVIAR SOPORTE A cartera@solinoff.com

De acuerdo a la ley 1231 de Julio de 2008 Art 743 del código de comercio , una vez cumplido el termino de diez (10) días calendario posteriores a la recepción de la presente factura no se acepta devolución, se entenderá que esta factura ha sido aceptada.

El pago no  oportuno  de esta factura causa intereses por mora a la tasa  máxima permitida por la ley vigente y el reporte a centrales de riesgo"; ;
                    datos.PiePagina_ReciboMercancia = "RECIBIDO\n\n" +
                                             "FIRMA\n\n" +
                                             "_________________________\n\n" +
                                             "NOMBRE\n\n" +
                                             "_________________________\n\n" +
                                             "FECHA dd/mm/aaaa\n\n" +
                                             "_________________________\n\n" +
                                             "CC";
                    return Helpers.Plantilla_6.PiePagina(ds, Logo, ref strError, datos);
                }
            }
        }
        public partial class Helpers
        {
            public class Montinpetrol
            {
                public static PdfPTable DatosCliente(DataSet DsInvoiceAR, System.Drawing.Image QRInvoice)
                {
                    /*VENDIDO A*/
                    PdfPTable tableInvoice = Helpers.Plantilla_6.TablaBase;

                    iTextSharp.text.Font fontVendidoA = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 6, iTextSharp.text.Font.NORMAL);
                    iTextSharp.text.Font fontVendidoASubTittle = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);

                    Paragraph prgVendidoA = new Paragraph("SEÑORES", fontVendidoA);
                    prgVendidoA.Alignment = Element.ALIGN_LEFT;

                    string VendidoA = DsInvoiceAR.Tables["Customer"].Rows[0]["Name"].ToString() + "\n" +
                                      DsInvoiceAR.Tables["Customer"].Rows[0]["Address1"].ToString() + "\n" +
                                      DsInvoiceAR.Tables["Customer"].Rows[0]["City"].ToString() + "\n" +
                                      DsInvoiceAR.Tables["Customer"].Rows[0]["State"].ToString() + "\n\n" +
                                      "NIT/CEDULA   " + DsInvoiceAR.Tables["Customer"].Rows[0]["ResaleID"].ToString();

                    Paragraph prgVendidoASubTittle = new Paragraph(VendidoA, fontVendidoASubTittle);
                    prgVendidoASubTittle.Alignment = Element.ALIGN_LEFT;

                    iTextSharp.text.pdf.PdfPCell cellVendidoA = new iTextSharp.text.pdf.PdfPCell();
                    cellVendidoA.AddElement(prgVendidoA);
                    cellVendidoA.AddElement(prgVendidoASubTittle);
                    cellVendidoA.Colspan = 2;
                    cellVendidoA.Padding = 3;
                    cellVendidoA.BorderWidthTop = 0;
                    cellVendidoA.BorderWidthRight = 0;
                    cellVendidoA.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                    cellVendidoA.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    tableInvoice.AddCell(cellVendidoA);

                    /*INFO PEDIDO*/
                    iTextSharp.text.Font fontInfoPedido = FontFactory.GetFont(FontFactory.HELVETICA, 5, iTextSharp.text.Font.NORMAL);
                    iTextSharp.text.pdf.PdfPTable tableInfoPedido = new iTextSharp.text.pdf.PdfPTable(4);
                    tableInfoPedido.HorizontalAlignment = Element.ALIGN_CENTER;
                    tableInfoPedido.WidthPercentage = 100;
                    tableInfoPedido.SetWidths(new float[] { 5.0f, 5.0f, 5.0f, 5.0f});

                    //1-1 Confirma Pedido.
                    iTextSharp.text.pdf.PdfPCell cellConfirmaPedido = new iTextSharp.text.pdf.PdfPCell(new Phrase("CONFIRMACION PEDIDO\n\n\n\n", fontInfoPedido));
                    cellConfirmaPedido.Colspan = 1;
                    cellConfirmaPedido.Padding = 3;
                    cellConfirmaPedido.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                    cellConfirmaPedido.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    tableInfoPedido.AddCell(cellConfirmaPedido);

                    //1-2 No Pedido
                    iTextSharp.text.pdf.PdfPCell cellNoPedido = new iTextSharp.text.pdf.PdfPCell(new Phrase("No. PEDIDO\n" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["OrderNum"].ToString() + "\n\n", fontInfoPedido));
                    cellNoPedido.Colspan = 1;
                    cellNoPedido.Padding = 3;
                    cellNoPedido.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                    cellNoPedido.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    tableInfoPedido.AddCell(cellNoPedido);

                    //1-3 Orden de Compra                
                    iTextSharp.text.pdf.PdfPCell cellOrdenCompra = new iTextSharp.text.pdf.PdfPCell(new Phrase("ORDEN DE COMPRA\n" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["PONum"].ToString() + "\n\n", fontInfoPedido));
                    cellOrdenCompra.Colspan = 1;
                    cellOrdenCompra.Padding = 3;
                    cellOrdenCompra.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                    cellOrdenCompra.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    tableInfoPedido.AddCell(cellOrdenCompra);

                    //1-4 Fecha Vencimiento
                    iTextSharp.text.pdf.PdfPCell cellDueDate = new iTextSharp.text.pdf.PdfPCell(new Phrase("FECHA VENCIMIENTO:\n" + Convert.ToDateTime(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"]).ToString("dd/MM/yyyy") + "\n\n", fontInfoPedido));
                    cellDueDate.Colspan = 1;
                    cellDueDate.Padding = 3;
                    cellDueDate.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                    cellDueDate.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    tableInfoPedido.AddCell(cellDueDate);

                    //1-5 Fecha Factura
                    iTextSharp.text.pdf.PdfPCell cellInvoiceDate = new iTextSharp.text.pdf.PdfPCell(new Phrase("FECHA FACTURA:\n" + Convert.ToDateTime(DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).ToString("dd/MM/yyyy") + "\n\n", fontInfoPedido));
                    cellInvoiceDate.Colspan = 1;
                    cellInvoiceDate.Padding = 3;
                    cellInvoiceDate.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                    cellInvoiceDate.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    tableInfoPedido.AddCell(cellInvoiceDate);

                    //2-1 Telefono
                    iTextSharp.text.pdf.PdfPCell cellTelefono = new iTextSharp.text.pdf.PdfPCell(new Phrase("TELÉFONO\n" + DsInvoiceAR.Tables["Customer"].Rows[0]["PhoneNum"].ToString(), fontInfoPedido));
                    cellTelefono.Colspan = 1;
                    cellTelefono.Padding = 3;
                    cellTelefono.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                    cellTelefono.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    tableInfoPedido.AddCell(cellTelefono);

                    //2-2 InvoiceNum
                    iTextSharp.text.pdf.PdfPCell cellNoFacRef = new iTextSharp.text.pdf.PdfPCell(new Phrase("Nro. Fact. REF\n" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceNum"].ToString(), fontInfoPedido));
                    cellNoFacRef.Colspan = 1;
                    cellNoFacRef.Padding = 3;
                    cellNoFacRef.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                    cellNoFacRef.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    tableInfoPedido.AddCell(cellNoFacRef);

                    //2-3 PLAZO MESES
                    iTextSharp.text.pdf.PdfPCell cellTermsCode = new iTextSharp.text.pdf.PdfPCell(new Phrase("PLAZO MESES\n" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["TermsCodeDescription"].ToString(), fontInfoPedido));
                    cellTermsCode.Colspan = 1;
                    cellTermsCode.Padding = 3;
                    cellTermsCode.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                    cellTermsCode.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    tableInfoPedido.AddCell(cellTermsCode);

                    ////2-4 VENDEDOR
                    //iTextSharp.text.pdf.PdfPCell cellVendedor = new iTextSharp.text.pdf.PdfPCell(new Phrase("VENDEDOR\n" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["SalesRepName1"].ToString(), fontInfoPedido));
                    //cellVendedor.Colspan = 1;
                    //cellVendedor.Padding = 3;
                    //cellVendedor.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                    //cellVendedor.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    //tableInfoPedido.AddCell(cellVendedor);

                    ////2-4 FECHA ENTREGA
                    //iTextSharp.text.pdf.PdfPCell cellFechaEntrega = new iTextSharp.text.pdf.PdfPCell(new Phrase("FECHA ENTREGA", fontInfoPedido));
                    //cellFechaEntrega.Colspan = 1;
                    //cellFechaEntrega.Padding = 3;
                    //cellFechaEntrega.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                    //cellFechaEntrega.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    //tableInfoPedido.AddCell(cellFechaEntrega);

                    iTextSharp.text.pdf.PdfPCell cellInfoPedido = new iTextSharp.text.pdf.PdfPCell(tableInfoPedido);
                    cellInfoPedido.Colspan = 4;
                    cellInfoPedido.Padding = 3;
                    cellInfoPedido.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                    cellInfoPedido.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    cellInfoPedido.BorderWidthTop = 0;
                    tableInvoice.AddCell(cellInfoPedido);

                    /*CELL QR*/
                    iTextSharp.text.Font fontRepGrafica = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                    Paragraph prgQrTittle = new Paragraph("CÓDIGO QR", fontRepGrafica);
                    prgQrTittle.Alignment = Element.ALIGN_CENTER;

                    iTextSharp.text.Image QRPdf = iTextSharp.text.Image.GetInstance(QRInvoice, BaseColor.WHITE);
                    QRPdf.ScaleAbsolute(60f, 60f);
                    QRPdf.Alignment = Element.ALIGN_CENTER;
                    iTextSharp.text.pdf.PdfPCell celImgQR = new iTextSharp.text.pdf.PdfPCell();
                    celImgQR.AddElement(prgQrTittle);
                    celImgQR.AddElement(QRPdf);
                    //iTextSharp.text.Image QRPdf = iTextSharp.text.Image.GetInstance(QRInvoice, BaseColor.WHITE);
                    //iTextSharp.text.pdf.PdfPCell celImgQR = new iTextSharp.text.pdf.PdfPCell(QRPdf);
                    celImgQR.Colspan = 1;
                    celImgQR.Padding = 3;
                    celImgQR.BorderWidthTop = 0;
                    celImgQR.BorderWidthLeft = 0;
                    celImgQR.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                    celImgQR.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    tableInvoice.AddCell(celImgQR);

                    return tableInvoice;
                }
                
                public class EventPageMontinpetrol : PdfPageEventHelper
                {
                    PdfContentByte cb;

                    // we will put the final number of pages in a template
                    PdfTemplate headerTemplate, footerTemplate;

                    // this is the BaseFont we are going to use for the header / footer
                    BaseFont bf = null;

                    // This keeps track of the creation time
                    DateTime PrintTime = DateTime.Now;

                    private string _header;

                    public string Header
                    {
                        get { return _header; }
                        set { _header = value; }
                    }
                    PdfPTable _Encabezado;
                    PdfPTable _DatosCliente;
                    PdfPTable _DetalleHead;

                    PdfPTable _PiePagina;
                    public PdfPTable PiePagina
                    {
                        get
                        {
                            return _PiePagina;
                        }
                        set
                        {
                            _PiePagina = value;
                        }
                    }
                    public PdfPTable Encabezado
                    {
                        get
                        {
                            return _Encabezado;
                        }
                        set
                        {
                            _Encabezado = value;
                        }
                    }
                    public PdfPTable DatosCliente
                    {
                        get
                        {
                            return _DatosCliente;
                        }
                        set
                        {
                            _DatosCliente = value;
                        }
                    }
                    public PdfPTable DetalleHead
                    {
                        get
                        {
                            return _DetalleHead;
                        }
                        set
                        {
                            _DetalleHead = value;
                        }
                    }

                    public EventPageMontinpetrol(PdfPTable encabezado, PdfPTable DatosCliente, PdfPTable detalleHead, PdfPTable piepagina)
                    {
                        _Encabezado = encabezado;
                        _DatosCliente = DatosCliente;
                        _DetalleHead = detalleHead;
                        _PiePagina = piepagina;
                    }

                    public override void OnOpenDocument(PdfWriter writer, Document document)
                    {
                        try
                        {
                            PrintTime = DateTime.Now;
                            bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                            cb = writer.DirectContent;
                            headerTemplate = cb.CreateTemplate(100, 100);
                            footerTemplate = cb.CreateTemplate(50, 50);
                        }
                        catch (DocumentException de)
                        {
                        }
                        catch (System.IO.IOException ioe)
                        {
                        }
                    }

                    public override void OnEndPage(iTextSharp.text.pdf.PdfWriter writer, iTextSharp.text.Document document)
                    {
                        base.OnEndPage(writer, document);

                        //call WriteSelectedRows of PdfTable. This writes rows from PdfWriter in PdfTable
                        //first param is start row. -1 indicates there is no end row and all the rows to be included to write
                        //Third and fourth param is x and y position to start writing


                        //document.Add(_PiePagina);

                        Encabezado.TotalWidth = document.PageSize.Width - 35f;
                        Encabezado.WriteSelectedRows(0, -1, 20, document.PageSize.Height - 30, writer.DirectContentUnder);

                        _DatosCliente.TotalWidth = document.PageSize.Width - 35f;
                        _DatosCliente.WriteSelectedRows(0, -1, 20, document.PageSize.Height - 157, writer.DirectContentUnder);

                        _DetalleHead.TotalWidth = document.PageSize.Width - 35f;
                        _DetalleHead.WriteSelectedRows(0, -1, 20, document.PageSize.Height - 240, writer.DirectContentUnder);

                        _PiePagina.TotalWidth = document.PageSize.Width - 35f;
                        _PiePagina.WriteSelectedRows(0, -1, 20, document.PageSize.Height - 510, writer.DirectContentUnder);

                        //tableUnidades.TotalWidth = document.PageSize.Width - 80f;
                        //tableUnidades.WriteSelectedRows(0, -1, 40, document.PageSize.Height - 40, writer.DirectContentUnder);
                        //pdfTab.WriteSelectedRows(0, -1, 40, document.PageSize.Height - 30, writer.DirectContent);

                        //cb.MoveTo(40, document.PageSize.Height - 100);
                        //cb.LineTo(document.PageSize.Width - 40, document.PageSize.Height - 100);
                        //cb.Stroke();
                    }

                    public override void OnCloseDocument(PdfWriter writer, Document document)
                    {
                        base.OnCloseDocument(writer, document);

                        headerTemplate.BeginText();
                        headerTemplate.SetFontAndSize(bf, 12);
                        headerTemplate.SetTextMatrix(0, 0);
                        headerTemplate.ShowText((writer.PageNumber - 1).ToString());
                        headerTemplate.EndText();

                        //footerTemplate.BeginText();
                        //footerTemplate.SetFontAndSize(bf, 12);
                        //footerTemplate.SetTextMatrix(0, 0);
                        //footerTemplate.ShowText((writer.PageNumber - 1).ToString());
                        //footerTemplate.EndText();
                    }
                }
            }
        }
        #endregion
    }
}
