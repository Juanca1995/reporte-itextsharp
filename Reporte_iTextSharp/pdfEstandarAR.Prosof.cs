﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
//Referenciar y usar.
using System.Data;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Web;
using System.Windows.Forms;

namespace RulesServicesDIAN2.Adquiriente
{
    public partial class pdfEstandarAR
    {
        #region funciones de pagina 

        float dimxProsof = 550f, dimyProsof = 20f;

        protected void AddPageContinueProsof(string rutaPDF, float posicionX, float posicionY, iTextSharp.text.Font font)
        {
            byte[] bytes = File.ReadAllBytes(rutaPDF);

            //iTextSharp.text.Font blackFont = FontFactory.GetFont("Arial", 12, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
            using (MemoryStream stream = new MemoryStream())
            {
                PdfReader reader = new PdfReader(bytes);
                using (PdfStamper stamper = new PdfStamper(reader, stream))
                {
                    int pages = reader.NumberOfPages;
                    string textContinue = "CONTINUES/CONTINUA...";
                    for (int i = 1; i <= pages; i++)
                    {
                        if (i == pages)
                            textContinue = string.Empty;

                        ColumnText.ShowTextAligned(
                            stamper.GetUnderContent(i), Element.ALIGN_CENTER, new Phrase($"{textContinue}", font), posicionX, posicionY, 0);
                    }
                }
                bytes = stream.ToArray();
            }
            File.WriteAllBytes(rutaPDF, bytes);
        }

        #endregion

        #region Add Unidades
        private bool AddUnidadesProsof(DataRow dataLine, ref PdfPTable table, iTextSharp.text.Font fontTitleFactura, DataSet dataSet)
        {
            try
            {
                //CANTIDAD
                strError += "Add InvoiceLine";
                iTextSharp.text.pdf.PdfPCell celValL = new iTextSharp.text.pdf.PdfPCell(new Phrase((string)dataLine["SellingShipQty"], fontTitleFactura));
                celValL.Colspan = 1;
                celValL.Padding = 2;
                celValL.Border = 0;
                celValL.Border = PdfPCell.RIGHT_BORDER | PdfPCell.BOTTOM_BORDER;
                //celTextL.BorderWidthRight = 1;
                //celValL.BorderWidthLeft = 1;
                celValL.HorizontalAlignment = Element.ALIGN_CENTER;
                celValL.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValL);
                strError += "InvoiceLine OK";

                //REFERENCIA
                strError += "Add PartNum";
                iTextSharp.text.pdf.PdfPCell celValCodigo = new iTextSharp.text.pdf.PdfPCell(new Phrase((string)dataLine["ShortChar01"], fontTitleFactura));
                celValCodigo.Colspan = 1;
                celValCodigo.Padding = 2;
                celValCodigo.Border = 0;
                celValCodigo.Border = PdfPCell.RIGHT_BORDER | PdfPCell.BOTTOM_BORDER;
                //celValCodigo.BorderWidthLeft = 1;
                //celDescripcion.BorderColorBottom = BaseColor.WHITE;
                celValCodigo.HorizontalAlignment = Element.ALIGN_LEFT;
                celValCodigo.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValCodigo);
                strError += "Add PartNum OK";

                //DESCRIPCION
                strError += "Add SellingShipQty";
                iTextSharp.text.pdf.PdfPCell celValDescripcion = new iTextSharp.text.pdf.PdfPCell(new Phrase((string)dataLine["PartNumPartDescription"], fontTitleFactura));
                celValDescripcion.Colspan = 1;
                celValDescripcion.Padding = 2;
                celValDescripcion.Border = 0;
                celValDescripcion.Border = PdfPCell.RIGHT_BORDER | PdfPCell.BOTTOM_BORDER;
                //celValCantidad.BorderWidthLeft = 1;
                celValDescripcion.HorizontalAlignment = Element.ALIGN_LEFT;
                celValDescripcion.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValDescripcion);
                strError += "Add SellingShipQty OK";

                //VALOR UNIDAD
                strError += "Add SalesUM";
                iTextSharp.text.pdf.PdfPCell celValUnidad = new iTextSharp.text.pdf.PdfPCell(new Phrase("$ " +
                    decimal.Parse((string)dataLine["DspDocExtPrice"]).ToString("N0"), fontTitleFactura));
                celValUnidad.Colspan = 1;
                celValUnidad.Padding = 2;
                celValUnidad.Border = 0;
                celValUnidad.Border = PdfPCell.RIGHT_BORDER | PdfPCell.BOTTOM_BORDER;
                //celValUnidad.BorderWidthLeft = 1;
                celValUnidad.HorizontalAlignment = Element.ALIGN_RIGHT;
                celValUnidad.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValUnidad);
                strError += "Add SalesUM OK";

                //DESCUENTO
                strError += "Add SalesUM";
                iTextSharp.text.pdf.PdfPCell celValdescuento = new iTextSharp.text.pdf.PdfPCell(new Phrase("$ " +
                    decimal.Parse((string)dataLine["DspDocExtPrice"]).ToString("N0"), fontTitleFactura));
                celValdescuento.Colspan = 1;
                celValdescuento.Padding = 2;
                celValdescuento.Border = 0;
                celValdescuento.Border = PdfPCell.RIGHT_BORDER | PdfPCell.BOTTOM_BORDER;
                //celValUnidad.BorderWidthLeft = 1;
                celValdescuento.HorizontalAlignment = Element.ALIGN_RIGHT;
                celValdescuento.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValdescuento);
                strError += "Add SalesUM OK";

                //SUB TOTAL
                strError += "Add LineDesc";
                iTextSharp.text.pdf.PdfPCell celValCantidad = new iTextSharp.text.pdf.PdfPCell(new Phrase("$ " +
                    decimal.Parse((string)dataLine["DspDocExtPrice"]).ToString("N0"), fontTitleFactura));
                celValCantidad.Colspan = 1;
                celValCantidad.Padding = 2;
                celValCantidad.Border = 0;
                celValCantidad.Border =  PdfPCell.BOTTOM_BORDER;
                //celValDesc.BorderWidthLeft = 1;
                celValCantidad.HorizontalAlignment = Element.ALIGN_RIGHT;
                celValCantidad.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValCantidad);
                strError += "Add LineDesc OK";
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return false;
            }
        }

        private bool AddUnidadesProsof(ref PdfPTable table)
        {
            try
            {
                iTextSharp.text.pdf.PdfPCell celValL = new iTextSharp.text.pdf.PdfPCell(new Phrase(" "));
                celValL.Colspan = 1;
                celValL.Padding = 3;
                celValL.Border = 0;
                //celTextL.BorderWidthRight = 1;
                celValL.BorderWidthLeft = 1;
                celValL.HorizontalAlignment = Element.ALIGN_CENTER;
                celValL.VerticalAlignment = Element.ALIGN_TOP;

                table.AddCell(celValL);

                iTextSharp.text.pdf.PdfPCell celValCodigo = new iTextSharp.text.pdf.PdfPCell(new Phrase(" "));
                celValCodigo.Colspan = 1;
                celValCodigo.Padding = 3;
                celValCodigo.Border = 0;
                celValCodigo.BorderWidthLeft = 1;
                //celDescripcion.BorderColorBottom = BaseColor.WHITE;
                celValCodigo.HorizontalAlignment = Element.ALIGN_CENTER;
                celValCodigo.VerticalAlignment = Element.ALIGN_TOP;

                table.AddCell(celValCodigo);

                iTextSharp.text.pdf.PdfPCell celValCantidad = new iTextSharp.text.pdf.PdfPCell(new Phrase(" "));
                celValCantidad.Colspan = 1;
                celValCantidad.Padding = 3;
                celValCantidad.Border = 0;
                celValCantidad.BorderWidthLeft = 1;
                celValCantidad.HorizontalAlignment = Element.ALIGN_CENTER;
                celValCantidad.VerticalAlignment = Element.ALIGN_TOP;

                table.AddCell(celValCantidad);

                iTextSharp.text.pdf.PdfPCell celValUnidad = new iTextSharp.text.pdf.PdfPCell(new Phrase(" "));
                celValUnidad.Colspan = 1;
                celValUnidad.Padding = 3;
                celValUnidad.Border = 0;
                celValUnidad.BorderWidthLeft = 1;
                celValUnidad.HorizontalAlignment = Element.ALIGN_CENTER;
                celValUnidad.VerticalAlignment = Element.ALIGN_TOP;

                table.AddCell(celValUnidad);

                iTextSharp.text.pdf.PdfPCell celValDesc = new iTextSharp.text.pdf.PdfPCell(new Phrase(" "));
                celValDesc.Colspan = 1;
                celValDesc.Padding = 3;
                celValDesc.Border = 0;
                celValDesc.BorderWidthLeft = 1;
                celValDesc.HorizontalAlignment = Element.ALIGN_CENTER;
                celValDesc.VerticalAlignment = Element.ALIGN_TOP;

                table.AddCell(celValDesc);

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return false;
            }
        }

        #endregion

        #region Factura de Ventas
         
        public bool FacturanacionalProsof(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {
            NomArchivo = string.Empty;
            string Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            string RutaImg = $@"{AppDomain.CurrentDomain.BaseDirectory}\LogoProsof.png";
            NomArchivo = NombreInvoice + ".pdf";

            //NomArchivo = string.Empty;
            //string Ruta = HttpContext.Current.Server.MapPath("~/PDF/" + NIT + "/");
            //string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            //string RutaImg = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";
            //NomArchivo = NombreInvoice + ".pdf";
            //strError += "inicia Documento pdf\n";
            try
            {
                #region Head
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.

                Document document = new Document(iTextSharp.text.PageSize.LETTER);

                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;

                // step 3: we open the document     
                document.Open();

                PdfDiv divEspacio = new PdfDiv();
                divEspacio.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio.Height = 10;
                divEspacio.Width = 130;

                PdfDiv divEspacio2 = new PdfDiv();
                divEspacio2.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio2.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                divEspacio2.Height = 10;
                divEspacio2.Width = 130;


                //FUENTES----------------------------------------------------------------------------------------------------------
                iTextSharp.text.Font fontTitleBold = FontFactory.GetFont(FontFactory.HELVETICA, 8.5f, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA, 8.5f, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustom = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustomtext = FontFactory.GetFont(FontFactory.COURIER, 8f, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustomBold = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle3 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle4 = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font grande = FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font mediana = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font pequeña = FontFactory.GetFont(FontFactory.HELVETICA, 3, iTextSharp.text.Font.NORMAL);

                //FUENTES---------------------------------------------------------------------------------------------------------

                ///objeto para el borde 
                RoundRectangle CelEventBorderRound = new RoundRectangle();

                PdfPTable vacio = new PdfPTable(1);
                vacio.WidthPercentage = 100;
                PdfPCell vacio1 = new PdfPCell()
                {
                    MinimumHeight = 10,
                    Border = 0
                };
                vacio.AddCell(vacio1);
                //imagenes
                System.Drawing.Image Logo;
                var request = WebRequest.Create(RutaImg);
                using (var response = request.GetResponse())
                using (var stream = response.GetResponseStream())
                {
                    Logo = Bitmap.FromStream(stream);
                }
                iTextSharp.text.Image LogoPdf = iTextSharp.text.Image.GetInstance(Logo, BaseColor.WHITE);
                LogoPdf.ScaleAbsolute(80, 80);


                System.Drawing.Image LogoBanner = null;
                var requestBanner = WebRequest.Create(RutaImg);
                using (var responseBanner = requestBanner.GetResponse())
                using (var streamBanner = responseBanner.GetResponseStream())
                {
                    LogoBanner = Bitmap.FromStream(streamBanner);
                }
                iTextSharp.text.Image LogoPdf2 = iTextSharp.text.Image.GetInstance(LogoBanner, BaseColor.WHITE);
                LogoPdf2.ScaleAbsolute(60f, 60f);


                //agregamos Qr----------------------------------------------
                PdfPTable QRr = new PdfPTable(1);
                Paragraph prgCufe = new Paragraph("CUFE: " + CUFE, mediana);
                prgCufe.Alignment = Element.ALIGN_CENTER;

                iTextSharp.text.Image QRPdf = iTextSharp.text.Image.GetInstance(QRInvoice, BaseColor.WHITE);
                QRPdf.ScaleAbsolute(80, 80);
                //iTextSharp.text.pdf.PdfPCell celImgQR = new iTextSharp.text.pdf.PdfPCell(QRPdf);
                iTextSharp.text.pdf.PdfPCell celImgQR = new iTextSharp.text.pdf.PdfPCell();
                celImgQR.AddElement(QRPdf);
                celImgQR.AddElement(prgCufe);
                celImgQR.Colspan = 1;
                celImgQR.Padding = 3;
                celImgQR.Border = 0;
                celImgQR.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celImgQR.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                QRr.AddCell(celImgQR);

                #endregion

                PdfPTable todo = new PdfPTable(1);
                todo.WidthPercentage = 100;
                PdfPCell cell_todo = new PdfPCell() { MinimumHeight = 500, };

                PdfPTable espacio = new PdfPTable(1);
                espacio.WidthPercentage = 100;
                PdfPCell cell_espacio = new PdfPCell() { MinimumHeight = 5, Border = 0, };
                espacio.AddCell(cell_espacio);

                #region Header

                PdfPTable Header = new PdfPTable(new float[] { 0.4f, 0.6f });
                Header.WidthPercentage = 90;

                PdfPCell cell_empresa = new PdfPCell() { Border = 0, };

                //agregamos el logo
                PdfPTable logo = new PdfPTable(1);
                logo.WidthPercentage = 100;
                PdfPCell cell_logo = new PdfPCell() { Border = 0,HorizontalAlignment=Element.ALIGN_CENTER,VerticalAlignment=Element.ALIGN_CENTER, };
                cell_logo.AddElement(LogoPdf);
                logo.AddCell(cell_logo);
                cell_empresa.AddElement(logo);

                PdfPTable info = new PdfPTable(1);
                info.WidthPercentage = 100;
                PdfPCell cell_info = new PdfPCell() { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, };
                Paragraph prgTitle = new Paragraph(string.Format("{0}\n{1}", DsInvoiceAR.Tables["Company"].Rows[0]["Name"],
                    "Nit " + DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"]), fontCustom);
                Paragraph prgTitle2 = new Paragraph(string.Format("{0}\n{1}\n{2}",
                    DsInvoiceAR.Tables["Company"].Rows[0]["Address1"].ToString(),
                    DsInvoiceAR.Tables["Company"].Rows[0]["City"].ToString(),
                    DsInvoiceAR.Tables["Company"].Rows[0]["State"].ToString()),
                    fontCustom);
                prgTitle.Alignment = Element.ALIGN_CENTER;
                prgTitle2.Alignment = Element.ALIGN_CENTER;
                cell_info.AddElement(prgTitle);
                cell_info.AddElement(prgTitle2);
                info.AddCell(cell_info);
                cell_empresa.AddElement(info);
                Header.AddCell(cell_empresa);

                //agregamos qr


                PdfPCell cell_fechas = new PdfPCell() { Border = 0, CellEvent = CelEventBorderRound, MinimumHeight = 15, };

                //agregamos las celdas y tablas para las fechas 
                PdfPTable fechas_1 = new PdfPTable(2);
                fechas_1.WidthPercentage = 100;
                PdfPCell f1 = new PdfPCell() {  Border = 0, };
                f1.Border = PdfPCell.RIGHT_BORDER;

                PdfPTable f_info1 = new PdfPTable(2);
                f_info1.WidthPercentage = 100;
                PdfPCell factura1 = new PdfPCell(new Phrase("FACTURA DE VENTAS NUMERO", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, Border = 0, };
                f_info1.AddCell(factura1);
                PdfPCell factura2 = new PdfPCell(new Phrase("COMERCIAL INVOICE", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, Border = 0, };
                f_info1.AddCell(factura2);
                f1.AddElement(f_info1);

                PdfPTable resol = new PdfPTable(1);
                resol.WidthPercentage = 100;
                PdfPCell cell_resol = new PdfPCell(new Phrase(""+ DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"], fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, Border = 0, MinimumHeight = 26, VerticalAlignment = Element.ALIGN_CENTER };
                cell_resol.Border = PdfPCell.BOTTOM_BORDER;
                resol.AddCell(cell_resol);
                f1.AddElement(resol);

                PdfPTable o_compras = new PdfPTable(1);
                o_compras.WidthPercentage = 100;
                PdfPCell cell_o_compras = new PdfPCell(new Phrase("ORDEN DE COMPRAS No.", fontCustom)) { HorizontalAlignment = Element.ALIGN_LEFT, Border = 0, };
                cell_o_compras.Border = PdfPCell.TOP_BORDER;
                o_compras.AddCell(cell_o_compras);
                f1.AddElement(o_compras);

                PdfPTable requesto = new PdfPTable(1);
                requesto.WidthPercentage = 100;
                PdfPCell cell_requesto = new PdfPCell(new Phrase("REQUIST ORDER No.", fontCustom)) { HorizontalAlignment = Element.ALIGN_LEFT, Border = 0, };
                requesto.AddCell(cell_requesto);
                f1.AddElement(requesto);

                PdfPTable requesto_valor = new PdfPTable(1); 
                requesto_valor.WidthPercentage = 100;
                PdfPCell rv = new PdfPCell(new Phrase("" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceNum"], fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, Border = 0, };
                requesto_valor.AddCell(rv);
                f1.AddElement(requesto_valor);


                fechas_1.AddCell(f1);
                PdfPCell f2 = new PdfPCell() { Border = 0, };

                PdfPTable fecha_date = new PdfPTable(2);
                fecha_date.WidthPercentage = 100;
                PdfPCell FECHA = new PdfPCell(new Phrase("FECHA", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, Border = 0, };
                FECHA.Border = PdfPCell.BOTTOM_BORDER;
                fecha_date.AddCell(FECHA);
                PdfPCell DATE = new PdfPCell(new Phrase("DATE", fontCustom)) { HorizontalAlignment = Element.ALIGN_RIGHT, Border = 0, };
                DATE.Border = PdfPCell.TOP_BORDER;
                DATE.Border = PdfPCell.BOTTOM_BORDER;
                fecha_date.AddCell(DATE);
                f2.AddElement(fecha_date);

                PdfPTable fecha_f_t = new PdfPTable(3);
                fecha_f_t.WidthPercentage = 100;
                PdfPCell d = new PdfPCell(new Phrase("DIA/", fontCustom)) { HorizontalAlignment = Element.ALIGN_RIGHT, Border = 0, };
                fecha_f_t.AddCell(d);
                PdfPCell m = new PdfPCell(new Phrase("MES/", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, Border = 0, };
                fecha_f_t.AddCell(m);
                PdfPCell a = new PdfPCell(new Phrase("AÑO", fontCustom)) { HorizontalAlignment = Element.ALIGN_LEFT, Border = 0, };
                fecha_f_t.AddCell(a);
                f2.AddElement(fecha_f_t);

                PdfPTable fecha_v_t = new PdfPTable(3);
                fecha_v_t.WidthPercentage = 100;
                PdfPCell dv = new PdfPCell(new Phrase("DAY/ ", fontCustom)) { HorizontalAlignment = Element.ALIGN_RIGHT, Border = 0, };
                dv.Border = PdfPCell.BOTTOM_BORDER;
                fecha_v_t.AddCell(dv);
                PdfPCell mv = new PdfPCell(new Phrase("MONTH/", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, Border = 0, };
                mv.Border = PdfPCell.BOTTOM_BORDER;
                fecha_v_t.AddCell(mv);
                PdfPCell av = new PdfPCell(new Phrase("YEAR", fontCustom)) { HorizontalAlignment = Element.ALIGN_LEFT, Border = 0, };
                av.Border = PdfPCell.BOTTOM_BORDER;
                fecha_v_t.AddCell(av);
                f2.AddElement(fecha_v_t);

                PdfPTable fecha_valor = new PdfPTable(1);
                fecha_valor.WidthPercentage = 100;
                PdfPCell fv = new PdfPCell(new Phrase(""+ $"{DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).ToString("dd/MM/yyyy")}", fontCustom))
                { HorizontalAlignment = Element.ALIGN_CENTER, Border = 0, };
                fv.Border = PdfPCell.BOTTOM_BORDER;
                fecha_valor.AddCell(fv);
                f2.AddElement(fecha_valor);

                PdfPTable trabajo = new PdfPTable(1);
                trabajo.WidthPercentage = 100;
                PdfPCell cell_trabajo = new PdfPCell(new Phrase("ORDEN DE TRABAJO No.", fontCustom)) { HorizontalAlignment = Element.ALIGN_LEFT, Border = 0, };
                trabajo.AddCell(cell_trabajo);
                f2.AddElement(trabajo);

                PdfPTable clint = new PdfPTable(1);
                clint.WidthPercentage = 100;
                PdfPCell cell_clint = new PdfPCell(new Phrase("ORDER CLIENT No.", fontCustom)) { HorizontalAlignment = Element.ALIGN_LEFT, Border = 0, };
                clint.AddCell(cell_clint);
                f2.AddElement(clint);

                PdfPTable clint_valor = new PdfPTable(1);
                clint_valor.WidthPercentage = 100;
                PdfPCell cv = new PdfPCell(new Phrase(""+ DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar01"], fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, Border = 0, };
                clint_valor.AddCell(cv);
                f2.AddElement(clint_valor);

                fechas_1.AddCell(f2);

                PdfPTable fechas_2 = new PdfPTable(2);
                fechas_2.WidthPercentage = 100;
                PdfPCell f3 = new PdfPCell() { MinimumHeight = 15, };

                PdfPCell f4 = new PdfPCell();
                fechas_2.AddCell(f2);

                cell_fechas.AddElement(fechas_1);
                cell_fechas.AddElement(fechas_2);
                Header.AddCell(cell_fechas);

                #endregion

                #region Body

                PdfPTable body = new PdfPTable(1);
                body.WidthPercentage = 90;
                PdfPCell cell_body = new PdfPCell() { Border = 0, CellEvent = CelEventBorderRound, MinimumHeight = 15, };

                PdfPTable info_cliente_1 = new PdfPTable(new float[] { 0.55f, 0.45f });
                info_cliente_1.WidthPercentage = 100;
                PdfPCell nombre_c = new PdfPCell(new Phrase("Nombre de cliente Ship to ", fontCustom)) { Border = 0 };
                info_cliente_1.AddCell(nombre_c);
                PdfPCell nit_c = new PdfPCell(new Phrase("Nit Code ID.", fontCustom)) { Border = 0, };
                info_cliente_1.AddCell(nit_c);
                cell_body.AddElement(info_cliente_1);

                PdfPTable valor_cliente_1 = new PdfPTable(new float[] { 0.55f, 0.45f });
                valor_cliente_1.WidthPercentage = 100;
                PdfPCell nombre_c_v = new PdfPCell(new Phrase(""+ DsInvoiceAR.Tables["Customer"].Rows[0]["Name"], fontCustom)) { Border = 0, };
                valor_cliente_1.AddCell(nombre_c_v);
                PdfPCell nit_c_v = new PdfPCell(new Phrase(""+ DsInvoiceAR.Tables["Customer"].Rows[0]["CustID"], fontCustom)) { Border = 0, };
                valor_cliente_1.AddCell(nit_c_v);
                cell_body.AddElement(valor_cliente_1);

                PdfPTable info_cliente_2 = new PdfPTable(new float[] { 0.55f, 0.2f, 0.25f });
                info_cliente_2.WidthPercentage = 100;
                PdfPCell direccion_c = new PdfPCell(new Phrase("Direccion Address ", fontCustom)) { Border = 0 };
                direccion_c.Border = PdfPCell.TOP_BORDER;
                info_cliente_2.AddCell(direccion_c);
                PdfPCell telefono_c = new PdfPCell(new Phrase("Telefono Tel", fontCustom)) { Border = 0, };
                telefono_c.Border = PdfPCell.TOP_BORDER;
                info_cliente_2.AddCell(telefono_c);
                PdfPCell ciudad_c = new PdfPCell(new Phrase("Ciudad City.", fontCustom)) { Border = 0, };
                ciudad_c.Border = PdfPCell.TOP_BORDER;
                info_cliente_2.AddCell(ciudad_c);
                cell_body.AddElement(info_cliente_2);

                PdfPTable valor_cliente_2 = new PdfPTable(new float[] { 0.55f, 0.2f, 0.25f });
                valor_cliente_2.WidthPercentage = 100;
                PdfPCell direcion_c_v = new PdfPCell(new Phrase(""+ DsInvoiceAR.Tables["Customer"].Rows[0]["Address1"], fontCustom)) { Border = 0, };
                valor_cliente_2.AddCell(direcion_c_v);
                PdfPCell telefono_c_v = new PdfPCell(new Phrase("" + DsInvoiceAR.Tables["Customer"].Rows[0]["PhoneNum"], fontCustom)) { Border = 0, };
                valor_cliente_2.AddCell(telefono_c_v);
                PdfPCell ciudad_c_v = new PdfPCell(new Phrase("" + DsInvoiceAR.Tables["Customer"].Rows[0]["City"], fontCustom)) { Border = 0, };
                valor_cliente_2.AddCell(ciudad_c_v);
                cell_body.AddElement(valor_cliente_2);

                body.AddCell(cell_body);

                PdfPTable titulo_unidades = new PdfPTable(1);
                titulo_unidades.WidthPercentage = 90;
                PdfPCell cell_titulo_unidades = new PdfPCell() { Border = 0, CellEvent = CelEventBorderRound, MinimumHeight = 15, };

                PdfPTable encabezado_unidades = new PdfPTable(new float[] { 0.3f, 0.9f, 1.7f, 0.5f, 0.5f, 0.5f });
                encabezado_unidades.WidthPercentage = 100;
                PdfPCell cantidad = new PdfPCell(new Phrase("Cantidad", fontCustom)) { HorizontalAlignment = Element.ALIGN_LEFT, Border = 0, };
                cantidad.Border = PdfPCell.RIGHT_BORDER;
                encabezado_unidades.AddCell(cantidad);
                PdfPCell referencia = new PdfPCell(new Phrase("Referencia", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, Border = 0, };
                referencia.Border = PdfPCell.RIGHT_BORDER;
                encabezado_unidades.AddCell(referencia);
                PdfPCell descripcion = new PdfPCell(new Phrase("Descipción o Identificacion de los Servicios", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, Border = 0, };
                descripcion.Border = PdfPCell.RIGHT_BORDER;
                encabezado_unidades.AddCell(descripcion);
                PdfPCell unitario = new PdfPCell(new Phrase("Valor Unitario", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, Border = 0, };
                unitario.Border = PdfPCell.RIGHT_BORDER;
                encabezado_unidades.AddCell(unitario);
                PdfPCell descuento = new PdfPCell(new Phrase("Descueto", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, Border = 0, };
                descuento.Border = PdfPCell.RIGHT_BORDER;
                encabezado_unidades.AddCell(descuento);
                PdfPCell total = new PdfPCell(new Phrase("Valor Total", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, Border = 0, };
                encabezado_unidades.AddCell(total);

                PdfPTable encabezado_unidades2 = new PdfPTable(new float[] { 0.3f, 0.9f, 1.7f, 0.5f, 0.5f, 0.5f });
                encabezado_unidades2.WidthPercentage = 100;
                PdfPCell cantidad2 = new PdfPCell(new Phrase("Quantity", fontCustom)) { HorizontalAlignment = Element.ALIGN_LEFT, Border = 0, };
                cantidad2.Border = PdfPCell.RIGHT_BORDER | PdfPCell.TOP_BORDER;
                encabezado_unidades2.AddCell(cantidad2);
                PdfPCell referencia2 = new PdfPCell(new Phrase("Ref.", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, Border = 0, };
                referencia2.Border = PdfPCell.RIGHT_BORDER | PdfPCell.TOP_BORDER;
                encabezado_unidades2.AddCell(referencia2);
                PdfPCell descripcion2 = new PdfPCell(new Phrase("Identification and Descripcion of articles of Services", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, Border = 0, };
                descripcion2.Border = PdfPCell.RIGHT_BORDER | PdfPCell.TOP_BORDER;
                encabezado_unidades2.AddCell(descripcion2);
                PdfPCell unitario2 = new PdfPCell(new Phrase("Unit Valor", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, Border = 0, };
                unitario2.Border = PdfPCell.RIGHT_BORDER | PdfPCell.TOP_BORDER;
                encabezado_unidades2.AddCell(unitario2);
                PdfPCell descuento2 = new PdfPCell(new Phrase("Discont", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, Border = 0, };
                descuento2.Border = PdfPCell.RIGHT_BORDER | PdfPCell.TOP_BORDER;
                encabezado_unidades2.AddCell(descuento2) ;
                PdfPCell total2 = new PdfPCell(new Phrase("Total Value", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, Border = 0, };
                total2.Border =  PdfPCell.TOP_BORDER;
                encabezado_unidades2.AddCell(total2);

                cell_titulo_unidades.AddElement(encabezado_unidades);
                cell_titulo_unidades.AddElement(encabezado_unidades2);
                titulo_unidades.AddCell(cell_titulo_unidades);

                #endregion

                #region Unidades

                PdfPTable tablas_u = new PdfPTable(1);
                tablas_u.WidthPercentage = 90;
                PdfPCell cell_u = new PdfPCell() { Border = 0, MinimumHeight = 15, HorizontalAlignment = Element.ALIGN_BOTTOM, VerticalAlignment = Element.ALIGN_TOP, CellEvent = CelEventBorderRound, };

                PdfPTable tableTituloUnidades = new PdfPTable(6);
                //Dimenciones.
                float[] DimencionUnidades = new float[6];
                DimencionUnidades[0] = 0.3f;//CÓDIGO
                DimencionUnidades[1] = 0.9f;//DESCRIPCION
                DimencionUnidades[2] = 1.7f;//CANTIDAD
                DimencionUnidades[3] = 0.5f;//DTO
                DimencionUnidades[4] = 0.5f;//PRECIO
                DimencionUnidades[5] = 0.5f;//PRECIO


                PdfPTable tableUnidades = new PdfPTable(6);
                tableUnidades.WidthPercentage = 100;
                tableUnidades.SetWidths(DimencionUnidades);

                foreach (DataRow InvoiceLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
                {
                    if (!AddUnidadesProsof(InvoiceLine, ref tableUnidades, fontCustom, DsInvoiceAR))
                        return false;
                }

                iTextSharp.text.pdf.PdfPCell LineaFinal = new iTextSharp.text.pdf.PdfPCell();
                LineaFinal.Colspan = 7;
                LineaFinal.Padding = 3;
                LineaFinal.Border = 1;
                LineaFinal.BorderColorBottom = BaseColor.WHITE;
                LineaFinal.HorizontalAlignment = Element.ALIGN_CENTER;
                LineaFinal.VerticalAlignment = Element.ALIGN_TOP;
                tableUnidades.AddCell(LineaFinal);

                cell_u.AddElement(tableTituloUnidades);
                cell_u.AddElement(tableUnidades);
                tablas_u.AddCell(cell_u);

                #endregion

                #region Footer

                PdfPTable Footer = new PdfPTable(1);
                Footer.WidthPercentage = 90;
                PdfPCell cell_Footer = new PdfPCell() { Border = 0, CellEvent = CelEventBorderRound, MinimumHeight = 15, };

                PdfPTable footer2 = new PdfPTable(new float[] { 0.8f, 0.4f, });
                footer2.WidthPercentage = 100;

                PdfPCell footer_l = new PdfPCell() { Border = 0, Padding = 0 };
                footer_l.Border = PdfPCell.RIGHT_BORDER;
                PdfPTable f_pago = new PdfPTable(2);
                f_pago.WidthPercentage = 100;
                PdfPCell cell_f_pago = new PdfPCell(new Phrase("FORMA DE PAGO " + " MODE OF PAYMENT", fontCustom)) { Border = 0, };
                cell_f_pago.Border = PdfPCell.RIGHT_BORDER;
                f_pago.AddCell(cell_f_pago);
                PdfPCell cell_f_pago2 = new PdfPCell(new Phrase("TERMINO DE NEGOCIACION " + " TERM OF SALE ", fontCustom)) { Border = 0, };
                f_pago.AddCell(cell_f_pago2);
                footer_l.AddElement(f_pago);
                PdfPTable f_pagov = new PdfPTable(2);
                f_pagov.WidthPercentage = 100;
                PdfPCell cell_f_pagov = new PdfPCell(new Phrase(""+ DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar02"], fontCustom)) { Border = 0, MinimumHeight = 20, };
                cell_f_pagov.Border = PdfPCell.RIGHT_BORDER;
                f_pagov.AddCell(cell_f_pagov);
                PdfPCell cell_f_pago2v = new PdfPCell(new Phrase("" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar03"], fontCustom)) { Border = 0, MinimumHeight = 20, };
                f_pagov.AddCell(cell_f_pago2v);
                footer_l.AddElement(f_pagov);

                PdfPTable elaboro = new PdfPTable(new float[] { 0.6f, 0.4f, });
                elaboro.WidthPercentage = 100;
                PdfPCell cell_elaboro = new PdfPCell(new Phrase("ELABORO " + " ELABORATED", fontCustom)) { MinimumHeight = 15, };
                cell_elaboro.Border = PdfPCell.RIGHT_BORDER | PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER;
                elaboro.AddCell(cell_elaboro);
                PdfPCell cell_elaborov = new PdfPCell(new Phrase("" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar03"], fontCustom));
                cell_elaborov.Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER | PdfPCell.RIGHT_BORDER; 
                elaboro.AddCell(cell_elaborov);
                footer_l.AddElement(elaboro);

                PdfPTable observaciones = new PdfPTable(1);
                observaciones.WidthPercentage = 100;
                PdfPCell cell_observaciones = new PdfPCell(new Phrase("OBSERVACION " + " OBSERBATION " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["Character01"], fontCustom)) { Border = 0, };
                cell_observaciones.Border = PdfPCell.RIGHT_BORDER;
                observaciones.AddCell(cell_observaciones);
                footer_l.AddElement(observaciones);
                footer2.AddCell(footer_l);

                PdfPCell footer_r = new PdfPCell() { Border = 0, };

                PdfPTable subtotal = new PdfPTable(2);
                subtotal.WidthPercentage = 100;
                PdfPCell cell_subtotal = new PdfPCell(new Phrase("SUBTOTAL", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_RIGHT, };
                cell_subtotal.Border = PdfPCell.RIGHT_BORDER;
                subtotal.AddCell(cell_subtotal);
                PdfPCell cell_subtotal_v = new PdfPCell(new Phrase(string.Format("{0}", decimal.Parse(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubTotal"].ToString()).ToString("N0")), fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_RIGHT, };
                subtotal.AddCell(cell_subtotal_v);
                footer_r.AddElement(subtotal);

                PdfPTable fuente = new PdfPTable(2);
                fuente.WidthPercentage = 100;
                PdfPCell cell_fuente = new PdfPCell(new Phrase("RTE FUENTE", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_RIGHT, };
                cell_fuente.Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER | PdfPCell.RIGHT_BORDER;
                fuente.AddCell(cell_fuente);
                PdfPCell cell_fuente_v = new PdfPCell(new Phrase(string.Format("{0}", GetValImpuestoByID("0A", DsInvoiceAR).ToString("N0")), fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_RIGHT, };
                cell_fuente_v.Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER;
                fuente.AddCell(cell_fuente_v);
                footer_r.AddElement(fuente);

                PdfPTable riva = new PdfPTable(2);
                riva.WidthPercentage = 100;
                PdfPCell cell_riva = new PdfPCell(new Phrase("RTE IVA", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_RIGHT, };
                cell_riva.Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER | PdfPCell.RIGHT_BORDER;
                riva.AddCell(cell_riva);
                PdfPCell cell_riva_v = new PdfPCell(new Phrase(string.Format("{0}",
                    GetValImpuestoByID("0B", DsInvoiceAR).ToString("N0")), fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_RIGHT, };
                cell_riva_v.Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER;
                riva.AddCell(cell_riva_v);
                footer_r.AddElement(riva);

                PdfPTable rica = new PdfPTable(2);
                rica.WidthPercentage = 100;
                PdfPCell cell_rica = new PdfPCell(new Phrase("RTE ICA", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_RIGHT, };
                cell_rica.Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER | PdfPCell.RIGHT_BORDER;
                rica.AddCell(cell_rica);
                PdfPCell cell_rica_v = new PdfPCell(new Phrase(string.Format("{0}",
                    GetValImpuestoByID("0B", DsInvoiceAR).ToString("N0")), fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_RIGHT, };
                cell_rica_v.Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER;
                rica.AddCell(cell_rica_v);
                footer_r.AddElement(rica);

                PdfPTable iva = new PdfPTable(2);
                iva.WidthPercentage = 100;
                PdfPCell cell_iva = new PdfPCell(new Phrase("IVA", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_RIGHT, };
                cell_iva.Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER | PdfPCell.RIGHT_BORDER;
                iva.AddCell(cell_iva);
                PdfPCell cell_iva_v = new PdfPCell(new Phrase(string.Format("{0}",
                    GetValImpuestoByID("01", DsInvoiceAR).ToString("N0")), fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_RIGHT, };
                cell_iva_v.Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER;
                iva.AddCell(cell_iva_v);
                footer_r.AddElement(iva);

                PdfPTable totalf = new PdfPTable(2);
                totalf.WidthPercentage = 100;
                PdfPCell cell_totalf = new PdfPCell(new Phrase("TOTAL", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_RIGHT, };
                cell_totalf.Border = PdfPCell.RIGHT_BORDER;
                totalf.AddCell(cell_totalf);
                PdfPCell cell_totalf_v = new PdfPCell(new Phrase(string.Format("{0}",
                    decimal.Parse(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"].ToString()).ToString("N0")), fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_RIGHT, };
                totalf.AddCell(cell_totalf_v);
                footer_r.AddElement(totalf);

                footer2.AddCell(footer_r);

                cell_Footer.AddElement(footer2);
                Footer.AddCell(cell_Footer);

                PdfPTable tabla_leyenda = new PdfPTable(1);
                tabla_leyenda.WidthPercentage = 100;
                PdfPCell celTextInfoEmpresa = new PdfPCell(new Phrase(
                $" ESTA FACTURA DE VENTA SE ASIMILA EN SUS EFECTOS LEGALES A UNA LETRA DE CAMBIO SEGUN ART. 774 DEL \n" +
                $"CODIGO DEL COMERCIO. EL CLIENTE ACEPTA QUE LAS PERSONAS QUE FIRMAN LA PRESENTE FACTURA DE VENTA TIENE AUTORIZACION \n" +
                $"PARA EL Y POR LO TANTO EN ESE ACTO SON REPRESENTANTES DEL CLIENTE, EL CUAL SE HACE RESPONSABLE DE LA TRANSACCION. .\n" +
                $"Efectuar Retención de la Fuente de 3.5% SEGUN DECRETO 2499 DE DICIEMBRE 6 DE 2012 . \n" +
                $"somos autorretenedores del ICA en el Municipio de Pereira.. \n" +
                $"LEY 1819 de 2016 Art.476 Excluidos de IVA servidores (Hosting), soporte virtual. \n" +
                $"Para comodidad puede cancelar en nuestra zona virtual www.prosof.co \n" +
                $"BANCOLOMBIA Cta. CORRIENTE # 851-57446965. \n" +
                $"Enviar soporte de pago al correo : asistentedegerencia@prosof.co. \n", fontCustom))
                {
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    //Colspan = 2,
                    Border = 0,
                    Padding = 3
                };
                tabla_leyenda.AddCell(celTextInfoEmpresa);

                PdfPTable t_recuadros = new PdfPTable(new float[] { 3.2f, 0.1f, 3.2f, 0.1f, 3.2f, });
                t_recuadros.WidthPercentage = 100;

                PdfPCell t_1 = new PdfPCell(new Phrase("FECHA RESIBIDA O ACPTADA\n \n \n \n " + " ____________________________________________ ", fontCustom))
                { HorizontalAlignment = Element.ALIGN_CENTER, Border = 0, CellEvent = CelEventBorderRound, };
                PdfPCell t_2 = new PdfPCell() { Border = 0, MinimumHeight = 40, };
                PdfPCell t_3 = new PdfPCell(new Phrase("NOMBRE DE QUIEN RESIBE\n \n \n \n " + " ____________________________________________ ", fontCustom))
                { Border = 0, CellEvent = CelEventBorderRound, HorizontalAlignment = Element.ALIGN_CENTER, };
                PdfPCell t_4 = new PdfPCell() { Border = 0, };
                PdfPCell t_5 = new PdfPCell(new Phrase("CARGO DE QUIEN RECIBE\n \n \n \n " + " ____________________________________________ ", fontCustom))
                { Border = 0, CellEvent = CelEventBorderRound, HorizontalAlignment = Element.ALIGN_CENTER, };

                t_recuadros.AddCell(t_1);
                t_recuadros.AddCell(t_2);
                t_recuadros.AddCell(t_3);
                t_recuadros.AddCell(t_4);
                t_recuadros.AddCell(t_5);

                PdfPTable cuadro_mid = new PdfPTable(3);
                cuadro_mid.WidthPercentage = 100;
                PdfPCell cell_cuadro_mid = new PdfPCell() { Border = 0, MinimumHeight = 50, };
                cuadro_mid.AddCell(cell_cuadro_mid);
                PdfPCell cell_cuadro_mid1 = new PdfPCell(new Phrase("FIRMA Y SELLO DE CLIENTE  CLIENT  CERTIFY \n \n \n  " + " ____________________________________________ ", fontCustom))
                { Border = 0, CellEvent = CelEventBorderRound, HorizontalAlignment = Element.ALIGN_CENTER, };
                cuadro_mid.AddCell(cell_cuadro_mid1);
                PdfPCell cell_cuadro_mid2 = new PdfPCell() { Border = 0, };
                cuadro_mid.AddCell(cell_cuadro_mid2);


                PdfPTable cuadro_qr = new PdfPTable(new float[]{0.43f, 0.2f, 0.37f });
                cuadro_qr.WidthPercentage = 100;
                PdfPCell cell_cuadro_qr2 = new PdfPCell() { Border = 0,};
                cuadro_qr.AddCell(cell_cuadro_qr2);
                //PdfPCell cell_cuadro_qr = new PdfPCell(new Phrase("")){ Border = 0,   };
                //cell_cuadro_qr.AddElement(celImgQR);
                cuadro_qr.AddCell(celImgQR);
                //cuadro_qr.AddCell(cell_cuadro_qr);
                PdfPCell cell_cuadro_qr3 = new PdfPCell() { Border = 0, }; 
                cuadro_qr.AddCell(cell_cuadro_qr3);

                #endregion

                cell_todo.AddElement(espacio);
                cell_todo.AddElement(Header);
                cell_todo.AddElement(espacio);
                cell_todo.AddElement(body);
                cell_todo.AddElement(espacio);
                cell_todo.AddElement(titulo_unidades);
                cell_todo.AddElement(tablas_u);
                //cell_todo.AddElement(tableUnidades);
                cell_todo.AddElement(espacio);
                cell_todo.AddElement(Footer);
                cell_todo.AddElement(tabla_leyenda);
                cell_todo.AddElement(t_recuadros);
                cell_todo.AddElement(espacio);
                cell_todo.AddElement(cuadro_mid);
                cell_todo.AddElement(espacio);
                cell_todo.AddElement(cuadro_qr);
                todo.AddCell(cell_todo);

                #region Exit

                document.Add(todo);

                //document.Add(tableMarcaAgua);

                strError += "inicia se agrega el content byte del pie de pagina pdf\n";
                PdfContentByte pCb = writer.DirectContent; PdfContentByte pCb2 = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();
                strError += "inicia Documento pdf\n";
                AddPageNumberPagTo($"{Ruta}{NomArchivo}", dimxProsof, 768, fontTitle);
                RutaPdf = NomArchivo;
                strError += "inicia se agrego la numeracion satisfactoriamente OK\n";
                return true;
                #endregion            
            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.Message;
                return false;
            }
        }
        #endregion

        #region Nota Credito

        private bool AddUnidadesNotacreditoProsof(DataRow dataLine, ref PdfPTable table, iTextSharp.text.Font fontTitleFactura, DataSet dataSet)
        {
            try
            {
                //abonado a
                strError += "Add InvoiceLine";
                iTextSharp.text.pdf.PdfPCell celValL = new iTextSharp.text.pdf.PdfPCell(new Phrase((string)dataLine["ShortChar04"], fontTitleFactura));
                celValL.Colspan = 1;
                celValL.Padding = 2;
                celValL.Border = 0;
                //celValL.Border = PdfPCell.LEFT_BORDER ;
                //celTextL.BorderWidthRight = 1;
                //celValL.BorderWidthLeft = 1;
                celValL.HorizontalAlignment = Element.ALIGN_CENTER;
                celValL.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValL);
                strError += "InvoiceLine OK";

                //valor factura
                strError += "Add PartNum";
                iTextSharp.text.pdf.PdfPCell celValCodigo = new iTextSharp.text.pdf.PdfPCell(new Phrase((string)dataLine["Number02"], fontTitleFactura));
                celValCodigo.Colspan = 1;
                celValCodigo.Padding = 2;
                celValCodigo.Border = 0;
                //celValCodigo.Border = PdfPCell.RIGHT_BORDER | PdfPCell.BOTTOM_BORDER;
                //celValCodigo.BorderWidthLeft = 1;
                //celDescripcion.BorderColorBottom = BaseColor.WHITE;
                celValCodigo.HorizontalAlignment = Element.ALIGN_CENTER;
                celValCodigo.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValCodigo);
                strError += "Add PartNum OK";

                //iva
                strError += "Add SellingShipQty";
                iTextSharp.text.pdf.PdfPCell celValDescripcion = new iTextSharp.text.pdf.PdfPCell(new Phrase((string)dataLine["Number03"], fontTitleFactura));
                celValDescripcion.Colspan = 1;
                celValDescripcion.Padding = 2;
                celValDescripcion.Border = 0;
                //celValDescripcion.Border = PdfPCell.RIGHT_BORDER | PdfPCell.BOTTOM_BORDER;
                //celValCantidad.BorderWidthLeft = 1;
                celValDescripcion.HorizontalAlignment = Element.ALIGN_CENTER;
                celValDescripcion.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValDescripcion);
                strError += "Add SellingShipQty OK";

                //retefuente
                strError += "Add SalesUM";
                iTextSharp.text.pdf.PdfPCell celValUnidad = new iTextSharp.text.pdf.PdfPCell(new Phrase((string)dataLine["Number04"], fontTitleFactura));
                celValUnidad.Colspan = 1;
                celValUnidad.Padding = 2;
                celValUnidad.Border = 0;
                //celValUnidad.Border = PdfPCell.RIGHT_BORDER | PdfPCell.BOTTOM_BORDER;
                //celValUnidad.BorderWidthLeft = 1;
                celValUnidad.HorizontalAlignment = Element.ALIGN_CENTER;
                celValUnidad.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValUnidad);
                strError += "Add SalesUM OK";

                //reteica
                strError += "Add SalesUM";
                iTextSharp.text.pdf.PdfPCell celValx = new iTextSharp.text.pdf.PdfPCell(new Phrase((string)dataLine["Number05"], fontTitleFactura));
                celValx.Colspan = 1;
                celValx.Padding = 2;
                celValx.Border = 0;
                //celValx.Border = PdfPCell.RIGHT_BORDER | PdfPCell.BOTTOM_BORDER;
                //celValUnidad.BorderWidthLeft = 1;
                celValx.HorizontalAlignment = Element.ALIGN_CENTER;
                celValx.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValx);
                strError += "Add SalesUM OK";

                //reteiva
                strError += "Add SalesUM";
                iTextSharp.text.pdf.PdfPCell celValt = new iTextSharp.text.pdf.PdfPCell(new Phrase((string)dataLine["Number06"], fontTitleFactura));
                celValt.Colspan = 1;
                celValt.Padding = 2;
                celValt.Border = 0;
                //celValt.Border = PdfPCell.RIGHT_BORDER | PdfPCell.BOTTOM_BORDER;
                //celValUnidad.BorderWidthLeft = 1;
                celValt.HorizontalAlignment = Element.ALIGN_CENTER;
                celValt.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValt);
                strError += "Add SalesUM OK";

                //imato cree
                strError += "Add SalesUM";
                iTextSharp.text.pdf.PdfPCell celValdescuento = new iTextSharp.text.pdf.PdfPCell(new Phrase((string)dataLine["Number01"], fontTitleFactura));
                celValdescuento.Colspan = 1;
                celValdescuento.Padding = 2;
                celValdescuento.Border = 0;
                //celValdescuento.Border = PdfPCell.RIGHT_BORDER | PdfPCell.BOTTOM_BORDER;
                //celValUnidad.BorderWidthLeft = 1;
                celValdescuento.HorizontalAlignment = Element.ALIGN_CENTER;
                celValdescuento.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValdescuento);
                strError += "Add SalesUM OK";

                //vr canselado
                strError += "Add LineDesc";
                iTextSharp.text.pdf.PdfPCell celValCantidad = new iTextSharp.text.pdf.PdfPCell(new Phrase((string)dataLine["Number07"], fontTitleFactura));
                celValCantidad.Colspan = 1;
                celValCantidad.Padding = 2;
                celValCantidad.Border = 0;
                //celValCantidad.Border = PdfPCell.RIGHT_BORDER;
                //celValDesc.BorderWidthLeft = 1;
                celValCantidad.HorizontalAlignment = Element.ALIGN_CENTER;
                celValCantidad.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValCantidad);
                strError += "Add LineDesc OK";
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return false;
            }
        }

        public bool NotaCreditoProsof(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {         
            NomArchivo = string.Empty;
            string Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            string RutaImg = $@"{AppDomain.CurrentDomain.BaseDirectory}\LogoProsof.png";
            //string Rutacertificado = $@"{AppDomain.CurrentDomain.BaseDirectory}\LOGO_PLASTICOS_AMBIENTALES.png";
            NomArchivo = NombreInvoice + ".pdf";
            try
            {
                #region Head
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.
                //Document document = new Document(iTextSharp.text.PageSize.LETTER);
                Document document = new Document(iTextSharp.text.PageSize.A4.Rotate(), 10, 10, 10, 10);
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;



                // step 3: we open the document     
                document.Open();

                PdfDiv divEspacio = new PdfDiv();
                divEspacio.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio.Height = 10;
                divEspacio.Width = 130;

                PdfDiv divEspacio2 = new PdfDiv();
                divEspacio2.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio2.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio2.Height = 2;
                divEspacio2.Width = 130;

                //FUENTES----------------------------------------------------------------------------------------------------------
                iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle3 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 24, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle2 = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustom = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustom2 = FontFactory.GetFont(FontFactory.HELVETICA, 5, iTextSharp.text.Font.NORMAL);
                //FUENTES---------------------------------------------------------------------------------------------------------

                ///objeto para el borde 
                RoundRectangle CelEventBorderRound = new RoundRectangle();

                System.Drawing.Image Logo;
                System.Drawing.Image LogoBanner = null;
                var request = WebRequest.Create(RutaImg);
                using (var response = request.GetResponse())
                using (var stream = response.GetResponseStream())
                {
                    Logo = Bitmap.FromStream(stream);
                }
                var requestBanner = WebRequest.Create(RutaImg);
                using (var responseBanner = requestBanner.GetResponse())
                using (var streamBanner = responseBanner.GetResponseStream())
                {
                    LogoBanner = Bitmap.FromStream(streamBanner);
                }

                //Logos--------------------------------

                iTextSharp.text.Image LogoPdf2 = iTextSharp.text.Image.GetInstance(LogoBanner, BaseColor.WHITE);
                LogoPdf2.ScaleAbsolute(270, 270);
                LogoPdf2.Border = 0;

                iTextSharp.text.Image QRPdf = iTextSharp.text.Image.GetInstance(QRInvoice, BaseColor.WHITE);
                QRPdf.ScaleAbsolute(100, 100);
                QRPdf.Border = 0;


                PdfPTable espacio = new PdfPTable(1);
                espacio.WidthPercentage = 100;
                PdfPCell salto = new PdfPCell(new Phrase(" 0 " + " LOTE: ")) { Border = 0, MinimumHeight = 20, HorizontalAlignment = Element.ALIGN_CENTER };//se agrega el lote
                espacio.AddCell(salto);

                #endregion

                PdfPTable espacios = new PdfPTable(1);
                espacio.WidthPercentage = 100;
                PdfPCell cell_espacio = new PdfPCell() { Border=0,MinimumHeight=5};
                espacios.AddCell(cell_espacio);

                #region Header

                PdfPTable Header = new PdfPTable(new float[] {0.4f, 0.2f, 0.4f  });
                Header.WidthPercentage = 100;

                PdfPCell cell_info = new PdfPCell() { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, };
                Paragraph prgTitle = new Paragraph(string.Format("{0}\n{1}", DsInvoiceAR.Tables["Company"].Rows[0]["Name"],
                    "Nit " + DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"]), fontCustom);
                Paragraph prgTitle2 = new Paragraph(string.Format("{0}\n{1}\n{2}",
                    DsInvoiceAR.Tables["Company"].Rows[0]["Address1"].ToString(),
                    DsInvoiceAR.Tables["Company"].Rows[0]["City"].ToString(),
                    DsInvoiceAR.Tables["Company"].Rows[0]["State"].ToString()),
                    fontCustom);
                prgTitle.Alignment = Element.ALIGN_CENTER;
                prgTitle2.Alignment = Element.ALIGN_CENTER;
                cell_info.AddElement(prgTitle);
                cell_info.AddElement(prgTitle2);
                Header.AddCell(cell_info);

                PdfPCell cell_Qr = new PdfPCell() { Border=0,};
                cell_Qr.AddElement(QRPdf);
                Header.AddCell(cell_Qr);

                PdfPCell cell_logo = new PdfPCell() { Border=0,};
                cell_logo.AddElement(LogoPdf2);
                Header.AddCell(cell_logo);

                #endregion

                #region Body

                PdfPTable Body = new PdfPTable(1);
                Body.WidthPercentage = 100;
                PdfPCell cell_Body = new PdfPCell() { MinimumHeight =60, };

                PdfPTable cuerpo = new PdfPTable(2);
                cuerpo.WidthPercentage = 100;
                PdfPCell cell_cuerpo1 = new PdfPCell(new Phrase("Señores(a):"+ DsInvoiceAR.Tables["Customer"].Rows[0]["Name"])) { Border=0};
                cuerpo.AddCell(cell_cuerpo1);
                PdfPCell cell_cuerpo2 = new PdfPCell(new Phrase("NOTA CREDITO: " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"] +
                    "\n\n\n\n FECHA: " + $"{DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).ToString("dd/MM/yyyy")}")) { Border=0};
                cuerpo.AddCell(cell_cuerpo2);
                cell_Body.AddElement(cuerpo);
                Body.AddCell(cell_Body);

                PdfPTable titulo_unidades = new PdfPTable(8);
                titulo_unidades.WidthPercentage = 100;
                PdfPCell abono = new PdfPCell(new Phrase("Abono A:")) { HorizontalAlignment=Element.ALIGN_CENTER,VerticalAlignment=Element.ALIGN_TOP, BackgroundColor = BaseColor.LIGHT_GRAY };
                titulo_unidades.AddCell(abono);
                PdfPCell valor_f = new PdfPCell(new Phrase("Valor Factura")) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, BackgroundColor = BaseColor.LIGHT_GRAY };
                titulo_unidades.AddCell(valor_f);
                PdfPCell iva = new PdfPCell(new Phrase("Iva")) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, BackgroundColor = BaseColor.LIGHT_GRAY };
                titulo_unidades.AddCell(iva);
                PdfPCell retefuente = new PdfPCell(new Phrase("ReteFuente")) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, BackgroundColor = BaseColor.LIGHT_GRAY };
                titulo_unidades.AddCell(retefuente);
                PdfPCell reteica = new PdfPCell(new Phrase("ReteIca")) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, BackgroundColor = BaseColor.LIGHT_GRAY };
                titulo_unidades.AddCell(reteica);
                PdfPCell reteiva = new PdfPCell(new Phrase("ReteIva")) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, BackgroundColor = BaseColor.LIGHT_GRAY };
                titulo_unidades.AddCell(reteiva);
                PdfPCell imatocree = new PdfPCell(new Phrase("Imato CREE")) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, BackgroundColor = BaseColor.LIGHT_GRAY };
                titulo_unidades.AddCell(imatocree);
                PdfPCell vr_canselado = new PdfPCell(new Phrase("Vr. Cancelado Ba")) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, BackgroundColor = BaseColor.LIGHT_GRAY };
                titulo_unidades.AddCell(vr_canselado);

                #endregion

                #region Unidades

                PdfPTable tableTituloUnidades = new PdfPTable(8);
                //Dimenciones.
                float[] DimencionUnidades = new float[8];
                DimencionUnidades[0] = 1.0f;//CÓDIGO
                DimencionUnidades[1] = 1.0f;//DESCRIPCION
                DimencionUnidades[2] = 1.0f;//CANTIDAD
                DimencionUnidades[3] = 1.0f;//DTO
                DimencionUnidades[4] = 1.0f;//PRECIO
                DimencionUnidades[5] = 1.0f;//PRECIO
                DimencionUnidades[6] = 1.0f;//PRECIO
                DimencionUnidades[7] = 1.0f;//PRECIO

                PdfPTable tableUnidades = new PdfPTable(8);
                tableUnidades.WidthPercentage = 100;
                tableUnidades.SetWidths(DimencionUnidades);

                foreach (DataRow InvoiceLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
                {
                    if (!AddUnidadesNotacreditoProsof(InvoiceLine, ref tableUnidades, fontCustom, DsInvoiceAR))
                        return false;
                }

                iTextSharp.text.pdf.PdfPCell LineaFinal = new iTextSharp.text.pdf.PdfPCell();
                LineaFinal.Colspan = 8;
                LineaFinal.Padding = 3;
                LineaFinal.Border = 1;
                LineaFinal.BorderColorBottom = BaseColor.WHITE;
                LineaFinal.HorizontalAlignment = Element.ALIGN_CENTER;
                LineaFinal.VerticalAlignment = Element.ALIGN_TOP;
                tableUnidades.AddCell(LineaFinal);

                #endregion

                #region Footer

                PdfPTable Footer = new PdfPTable(8);
                Footer.WidthPercentage = 100;
                PdfPCell abonov = new PdfPCell(new Phrase("")) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, };
                abonov.Border = PdfPCell.TOP_BORDER;
                Footer.AddCell(abonov);
                PdfPCell valor_fv = new PdfPCell(new Phrase("" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"],fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, };
                valor_fv.Border = PdfPCell.TOP_BORDER;
                Footer.AddCell(valor_fv);
                PdfPCell ivav = new PdfPCell(new Phrase(string.Format("{0}",
                    GetValImpuestoByID("0B", DsInvoiceAR).ToString("N0")), fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, };
                ivav.Border = PdfPCell.TOP_BORDER;
                Footer.AddCell(ivav);
                PdfPCell retefuentev = new PdfPCell(new Phrase(string.Format("{0}", GetValImpuestoByID("0A", DsInvoiceAR).ToString("N0")), fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, };
                retefuentev.Border = PdfPCell.TOP_BORDER;
                Footer.AddCell(retefuentev);
                PdfPCell reteicav = new PdfPCell(new Phrase(string.Format("{0}",
                    GetValImpuestoByID("0B", DsInvoiceAR).ToString("N0")), fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, };
                reteicav.Border = PdfPCell.TOP_BORDER;
                Footer.AddCell(reteicav);
                PdfPCell reteivav = new PdfPCell(new Phrase(string.Format("{0}",
                    GetValImpuestoByID("0B", DsInvoiceAR).ToString("N0")), fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, };
                reteivav.Border = PdfPCell.TOP_BORDER;
                Footer.AddCell(reteivav);
                PdfPCell imatocreev = new PdfPCell(new Phrase("" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar04"], fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, };
                imatocreev.Border = PdfPCell.TOP_BORDER;
                Footer.AddCell(imatocreev);
                PdfPCell vr_canseladov = new PdfPCell(new Phrase("" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar05"], fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, };
                vr_canseladov.Border = PdfPCell.TOP_BORDER;
                Footer.AddCell(vr_canseladov);

                PdfPTable pie = new PdfPTable(new float[] { 49.5f, 1.0f, 49.5f});
                pie.WidthPercentage = 100;
                PdfPCell cell_pie1 = new PdfPCell(new Phrase("Obsrvaciones: " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["Character01"])) { MinimumHeight=30,};
                pie.AddCell(cell_pie1);
                PdfPCell cell_pie2 = new PdfPCell() { Border=0,};
                pie.AddCell(cell_pie2);
                PdfPCell cell_pie3 = new PdfPCell(new Phrase("Firma y Sello: "));
                pie.AddCell(cell_pie3);

                #endregion

                #region Exti

                document.Add(Header);
                document.Add(espacios);
                document.Add(Body);
                document.Add(espacios);
                document.Add(titulo_unidades);
                document.Add(tableTituloUnidades);
                document.Add(tableUnidades);
                //document.Add(espacios);
                document.Add(Footer);
                document.Add(pie);

                /*PIE DE PAGINA*/
                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();
                return true;
                #endregion
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return false;
            }    
        }
        #endregion

        #region Nota Debito

        public bool NotaDebitoProsof(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {
            NomArchivo = string.Empty;
            string Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            string RutaImg = $@"{AppDomain.CurrentDomain.BaseDirectory}\LogoProsof.png";
            //string Rutacertificado = $@"{AppDomain.CurrentDomain.BaseDirectory}\LOGO_PLASTICOS_AMBIENTALES.png";
            NomArchivo = NombreInvoice + ".pdf";
            try
            {
                #region Head
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.
                //Document document = new Document(iTextSharp.text.PageSize.LETTER);
                Document document = new Document(iTextSharp.text.PageSize.A4.Rotate(), 10, 10, 10, 10);
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;



                // step 3: we open the document     
                document.Open();

                PdfDiv divEspacio = new PdfDiv();
                divEspacio.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio.Height = 10;
                divEspacio.Width = 130;

                PdfDiv divEspacio2 = new PdfDiv();
                divEspacio2.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio2.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio2.Height = 2;
                divEspacio2.Width = 130;

                //FUENTES----------------------------------------------------------------------------------------------------------
                iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle3 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 24, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle2 = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustom = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustom2 = FontFactory.GetFont(FontFactory.HELVETICA, 5, iTextSharp.text.Font.NORMAL);
                //FUENTES---------------------------------------------------------------------------------------------------------

                ///objeto para el borde 
                RoundRectangle CelEventBorderRound = new RoundRectangle();

                System.Drawing.Image Logo;
                System.Drawing.Image LogoBanner = null;
                var request = WebRequest.Create(RutaImg);
                using (var response = request.GetResponse())
                using (var stream = response.GetResponseStream())
                {
                    Logo = Bitmap.FromStream(stream);
                }
                var requestBanner = WebRequest.Create(RutaImg);
                using (var responseBanner = requestBanner.GetResponse())
                using (var streamBanner = responseBanner.GetResponseStream())
                {
                    LogoBanner = Bitmap.FromStream(streamBanner);
                }

                //Logos--------------------------------

                iTextSharp.text.Image LogoPdf2 = iTextSharp.text.Image.GetInstance(LogoBanner, BaseColor.WHITE);
                LogoPdf2.ScaleAbsolute(270, 270);
                LogoPdf2.Border = 0;

                iTextSharp.text.Image QRPdf = iTextSharp.text.Image.GetInstance(QRInvoice, BaseColor.WHITE);
                QRPdf.ScaleAbsolute(100, 100);
                QRPdf.Border = 0;


                PdfPTable espacio = new PdfPTable(1);
                espacio.WidthPercentage = 100;
                PdfPCell salto = new PdfPCell(new Phrase(" 0 " + " LOTE: ")) { Border = 0, MinimumHeight = 20, HorizontalAlignment = Element.ALIGN_CENTER };//se agrega el lote
                espacio.AddCell(salto);

                #endregion

                PdfPTable espacios = new PdfPTable(1);
                espacio.WidthPercentage = 100;
                PdfPCell cell_espacio = new PdfPCell() { Border = 0, MinimumHeight = 5 };
                espacios.AddCell(cell_espacio);

                #region Header

                PdfPTable Header = new PdfPTable(new float[] { 0.4f, 0.2f, 0.4f });
                Header.WidthPercentage = 100;

                PdfPCell cell_info = new PdfPCell() { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, };
                Paragraph prgTitle = new Paragraph(string.Format("{0}\n{1}", DsInvoiceAR.Tables["Company"].Rows[0]["Name"],
                    "Nit " + DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"]), fontCustom);
                Paragraph prgTitle2 = new Paragraph(string.Format("{0}\n{1}\n{2}",
                    DsInvoiceAR.Tables["Company"].Rows[0]["Address1"].ToString(),
                    DsInvoiceAR.Tables["Company"].Rows[0]["City"].ToString(),
                    DsInvoiceAR.Tables["Company"].Rows[0]["State"].ToString()),
                    fontCustom);
                prgTitle.Alignment = Element.ALIGN_CENTER;
                prgTitle2.Alignment = Element.ALIGN_CENTER;
                cell_info.AddElement(prgTitle);
                cell_info.AddElement(prgTitle2);
                Header.AddCell(cell_info);

                PdfPCell cell_Qr = new PdfPCell() { Border = 0, };
                cell_Qr.AddElement(QRPdf);
                Header.AddCell(cell_Qr);

                PdfPCell cell_logo = new PdfPCell() { Border = 0, };
                cell_logo.AddElement(LogoPdf2);
                Header.AddCell(cell_logo);

                #endregion

                #region Body

                PdfPTable Body = new PdfPTable(1);
                Body.WidthPercentage = 100;
                PdfPCell cell_Body = new PdfPCell() { MinimumHeight = 60, };

                PdfPTable cuerpo = new PdfPTable(2);
                cuerpo.WidthPercentage = 100;
                PdfPCell cell_cuerpo1 = new PdfPCell(new Phrase("Señores(a):" + DsInvoiceAR.Tables["Customer"].Rows[0]["Name"])) { Border = 0 };
                cuerpo.AddCell(cell_cuerpo1);
                PdfPCell cell_cuerpo2 = new PdfPCell(new Phrase("NOTA DEBITO: " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"] +
                    "\n\n\n\n FECHA: " + $"{DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).ToString("dd/MM/yyyy")}"))
                { Border = 0 };
                cuerpo.AddCell(cell_cuerpo2);
                cell_Body.AddElement(cuerpo);
                Body.AddCell(cell_Body);

                PdfPTable titulo_unidades = new PdfPTable(8);
                titulo_unidades.WidthPercentage = 100;
                PdfPCell abono = new PdfPCell(new Phrase("Abono A:")) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, BackgroundColor = BaseColor.LIGHT_GRAY };
                titulo_unidades.AddCell(abono);
                PdfPCell valor_f = new PdfPCell(new Phrase("Valor Factura")) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, BackgroundColor = BaseColor.LIGHT_GRAY };
                titulo_unidades.AddCell(valor_f);
                PdfPCell iva = new PdfPCell(new Phrase("Iva")) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, BackgroundColor = BaseColor.LIGHT_GRAY };
                titulo_unidades.AddCell(iva);
                PdfPCell retefuente = new PdfPCell(new Phrase("ReteFuente")) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, BackgroundColor = BaseColor.LIGHT_GRAY };
                titulo_unidades.AddCell(retefuente);
                PdfPCell reteica = new PdfPCell(new Phrase("ReteIca")) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, BackgroundColor = BaseColor.LIGHT_GRAY };
                titulo_unidades.AddCell(reteica);
                PdfPCell reteiva = new PdfPCell(new Phrase("ReteIva")) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, BackgroundColor = BaseColor.LIGHT_GRAY };
                titulo_unidades.AddCell(reteiva);
                PdfPCell imatocree = new PdfPCell(new Phrase("Imato CREE")) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, BackgroundColor = BaseColor.LIGHT_GRAY };
                titulo_unidades.AddCell(imatocree);
                PdfPCell vr_canselado = new PdfPCell(new Phrase("Vr. Cancelado Ba")) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, BackgroundColor = BaseColor.LIGHT_GRAY };
                titulo_unidades.AddCell(vr_canselado);

                #endregion

                #region Unidades

                PdfPTable tableTituloUnidades = new PdfPTable(8);
                //Dimenciones.
                float[] DimencionUnidades = new float[8];
                DimencionUnidades[0] = 1.0f;//CÓDIGO
                DimencionUnidades[1] = 1.0f;//DESCRIPCION
                DimencionUnidades[2] = 1.0f;//CANTIDAD
                DimencionUnidades[3] = 1.0f;//DTO
                DimencionUnidades[4] = 1.0f;//PRECIO
                DimencionUnidades[5] = 1.0f;//PRECIO
                DimencionUnidades[6] = 1.0f;//PRECIO
                DimencionUnidades[7] = 1.0f;//PRECIO

                PdfPTable tableUnidades = new PdfPTable(8);
                tableUnidades.WidthPercentage = 100;
                tableUnidades.SetWidths(DimencionUnidades);

                foreach (DataRow InvoiceLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
                {
                    if (!AddUnidadesNotacreditoProsof(InvoiceLine, ref tableUnidades, fontCustom, DsInvoiceAR))
                        return false;
                }

                iTextSharp.text.pdf.PdfPCell LineaFinal = new iTextSharp.text.pdf.PdfPCell();
                LineaFinal.Colspan = 8;
                LineaFinal.Padding = 3;
                LineaFinal.Border = 1;
                LineaFinal.BorderColorBottom = BaseColor.WHITE;
                LineaFinal.HorizontalAlignment = Element.ALIGN_CENTER;
                LineaFinal.VerticalAlignment = Element.ALIGN_TOP;
                tableUnidades.AddCell(LineaFinal);

                #endregion

                #region Footer

                PdfPTable Footer = new PdfPTable(8);
                Footer.WidthPercentage = 100;
                PdfPCell abonov = new PdfPCell(new Phrase("")) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, };
                abonov.Border = PdfPCell.TOP_BORDER;
                Footer.AddCell(abonov);
                PdfPCell valor_fv = new PdfPCell(new Phrase("" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"], fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, };
                valor_fv.Border = PdfPCell.TOP_BORDER;
                Footer.AddCell(valor_fv);
                PdfPCell ivav = new PdfPCell(new Phrase(string.Format("{0}",
                    GetValImpuestoByID("0B", DsInvoiceAR).ToString("N0")), fontCustom))
                { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, };
                ivav.Border = PdfPCell.TOP_BORDER;
                Footer.AddCell(ivav);
                PdfPCell retefuentev = new PdfPCell(new Phrase(string.Format("{0}", GetValImpuestoByID("0A", DsInvoiceAR).ToString("N0")), fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, };
                retefuentev.Border = PdfPCell.TOP_BORDER;
                Footer.AddCell(retefuentev);
                PdfPCell reteicav = new PdfPCell(new Phrase(string.Format("{0}",
                    GetValImpuestoByID("0B", DsInvoiceAR).ToString("N0")), fontCustom))
                { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, };
                reteicav.Border = PdfPCell.TOP_BORDER;
                Footer.AddCell(reteicav);
                PdfPCell reteivav = new PdfPCell(new Phrase(string.Format("{0}",
                    GetValImpuestoByID("0B", DsInvoiceAR).ToString("N0")), fontCustom))
                { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, };
                reteivav.Border = PdfPCell.TOP_BORDER;
                Footer.AddCell(reteivav);
                PdfPCell imatocreev = new PdfPCell(new Phrase("" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar04"], fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, };
                imatocreev.Border = PdfPCell.TOP_BORDER;
                Footer.AddCell(imatocreev);
                PdfPCell vr_canseladov = new PdfPCell(new Phrase("" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar05"], fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, };
                vr_canseladov.Border = PdfPCell.TOP_BORDER;
                Footer.AddCell(vr_canseladov);

                PdfPTable pie = new PdfPTable(new float[] { 49.5f, 1.0f, 49.5f });
                pie.WidthPercentage = 100;
                PdfPCell cell_pie1 = new PdfPCell(new Phrase("Obsrvaciones: " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["Character01"])) { MinimumHeight = 30, };
                pie.AddCell(cell_pie1);
                PdfPCell cell_pie2 = new PdfPCell() { Border = 0, };
                pie.AddCell(cell_pie2);
                PdfPCell cell_pie3 = new PdfPCell(new Phrase("Firma y Sello: "));
                pie.AddCell(cell_pie3);

                #endregion

                #region Exti

                document.Add(Header);
                document.Add(espacios);
                document.Add(Body);
                document.Add(espacios);
                document.Add(titulo_unidades);
                document.Add(tableTituloUnidades);
                document.Add(tableUnidades);
                //document.Add(espacios);
                document.Add(Footer);
                document.Add(pie);

                /*PIE DE PAGINA*/
                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();
                return true;
                #endregion
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return false;
            }
        }

        #endregion
    }
}