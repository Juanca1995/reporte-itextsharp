﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
//Referenciar y usar.
using System.Data;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Web;
using System.Windows.Forms;

namespace RulesServicesDIAN2.Adquiriente
{
    public partial class pdfEstandarAR
    {

        #region factura de ventas


        private bool AddUnidadesTomyPuertas(DataRow dataLine, ref PdfPTable table, iTextSharp.text.Font fontTitleFactura, DataSet dataSet)
        {
            try
            {
                //DESCRIPCIÓN
                strError += "Add InvoiceLine";
                iTextSharp.text.pdf.PdfPCell celValL = new iTextSharp.text.pdf.PdfPCell(new Phrase((string)dataLine["LineDesc"], fontTitleFactura));
                celValL.Colspan = 1;
                celValL.Padding = 2;
                celValL.Border = 0;
                celValL.Border = PdfPCell.RIGHT_BORDER | PdfPCell.LEFT_BORDER;
                //celTextL.BorderWidthRight = 1;
                //celValL.BorderWidthLeft = 1;
                celValL.HorizontalAlignment = Element.ALIGN_CENTER;
                celValL.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValL);
                strError += "InvoiceLine OK";

                //CANTIDAD
                strError += "Add PartNum";
                iTextSharp.text.pdf.PdfPCell celValCodigo = new iTextSharp.text.pdf.PdfPCell(new Phrase((string)dataLine["SellingShipQty"], fontTitleFactura));
                celValCodigo.Colspan = 1;
                celValCodigo.Padding = 2;
                celValCodigo.Border = 0;
                celValCodigo.Border = PdfPCell.RIGHT_BORDER;
                //celValCodigo.BorderWidthLeft = 1;
                //celDescripcion.BorderColorBottom = BaseColor.WHITE;
                celValCodigo.HorizontalAlignment = Element.ALIGN_CENTER;
                celValCodigo.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValCodigo);
                strError += "Add PartNum OK";

                //VALOR UNITARIO
                strError += "Add SellingShipQty";
                iTextSharp.text.pdf.PdfPCell celValDescripcion = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    decimal.Parse((string)dataLine["UnitPrice"]).ToString("N0"), fontTitleFactura));
                celValDescripcion.Colspan = 1;
                celValDescripcion.Padding = 2;
                celValDescripcion.Border = 0;
                celValDescripcion.Border = PdfPCell.RIGHT_BORDER;
                //celValCantidad.BorderWidthLeft = 1;
                celValDescripcion.HorizontalAlignment = Element.ALIGN_CENTER;
                celValDescripcion.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValDescripcion);
                strError += "Add SellingShipQty OK";

                // VALOR TOTA
                strError += "Add SalesUM";
                iTextSharp.text.pdf.PdfPCell celValUnidad = new iTextSharp.text.pdf.PdfPCell(new Phrase(" " +
                    decimal.Parse((string)dataLine["DocExtPrice"]).ToString("N0"), fontTitleFactura));
                celValUnidad.Colspan = 1;
                celValUnidad.Padding = 2;
                celValUnidad.Border = 0;
                celValUnidad.Border = PdfPCell.RIGHT_BORDER;
                //celValUnidad.BorderWidthLeft = 1;
                celValUnidad.HorizontalAlignment = Element.ALIGN_CENTER;
                celValUnidad.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValUnidad);
                strError += "Add SalesUM OK";

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return false;
            }
        }

        private bool AddUnidadesTomyPuertas(ref PdfPTable table)
        {
            try
            {
                iTextSharp.text.pdf.PdfPCell celValL = new iTextSharp.text.pdf.PdfPCell(new Phrase(" "));
                celValL.Colspan = 1;
                celValL.Padding = 3;
                celValL.Border = 0;
                //celTextL.BorderWidthRight = 1;
                celValL.BorderWidthLeft = 1;
                celValL.HorizontalAlignment = Element.ALIGN_CENTER;
                celValL.VerticalAlignment = Element.ALIGN_TOP;

                table.AddCell(celValL);

                iTextSharp.text.pdf.PdfPCell celValCodigo = new iTextSharp.text.pdf.PdfPCell(new Phrase(" "));
                celValCodigo.Colspan = 1;
                celValCodigo.Padding = 3;
                celValCodigo.Border = 0;
                celValCodigo.BorderWidthLeft = 1;
                //celDescripcion.BorderColorBottom = BaseColor.WHITE;
                celValCodigo.HorizontalAlignment = Element.ALIGN_CENTER;
                celValCodigo.VerticalAlignment = Element.ALIGN_TOP;

                table.AddCell(celValCodigo);

                iTextSharp.text.pdf.PdfPCell celValCantidad = new iTextSharp.text.pdf.PdfPCell(new Phrase(" "));
                celValCantidad.Colspan = 1;
                celValCantidad.Padding = 3;
                celValCantidad.Border = 0;
                celValCantidad.BorderWidthLeft = 1;
                celValCantidad.HorizontalAlignment = Element.ALIGN_CENTER;
                celValCantidad.VerticalAlignment = Element.ALIGN_TOP;

                table.AddCell(celValCantidad);

                iTextSharp.text.pdf.PdfPCell celValUnidad = new iTextSharp.text.pdf.PdfPCell(new Phrase(" "));
                celValUnidad.Colspan = 1;
                celValUnidad.Padding = 3;
                celValUnidad.Border = 0;
                celValUnidad.BorderWidthLeft = 1;
                celValUnidad.HorizontalAlignment = Element.ALIGN_CENTER;
                celValUnidad.VerticalAlignment = Element.ALIGN_TOP;

                table.AddCell(celValUnidad);

                iTextSharp.text.pdf.PdfPCell celValDesc = new iTextSharp.text.pdf.PdfPCell(new Phrase(" "));
                celValDesc.Colspan = 1;
                celValDesc.Padding = 3;
                celValDesc.Border = 0;
                celValDesc.BorderWidthLeft = 1;
                celValDesc.HorizontalAlignment = Element.ALIGN_CENTER;
                celValDesc.VerticalAlignment = Element.ALIGN_TOP;

                table.AddCell(celValDesc);

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return false;
            }
        }


        public bool FacturaNacionalTomyPuerta(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {
            //NomArchivo = string.Empty;
            //string Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
            //string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            //string RutaImg = $@"{AppDomain.CurrentDomain.BaseDirectory}\Tomy Puerta S.A.S_logo.png";
            //string RutaMarcaAgua = $@"{AppDomain.CurrentDomain.BaseDirectory}\Tomy Puerta S.A.S_logo.png";
            //NomArchivo = NombreInvoice + ".pdf";
            NomArchivo = string.Empty;
            string Ruta = HttpContext.Current.Server.MapPath("~/PDF/" + NIT + "/");
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            string RutaImg = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";
            NomArchivo = NombreInvoice + ".pdf";

            try
            {
                #region Head
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.
                Document document = new Document(iTextSharp.text.PageSize.LETTER);
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;

                // step 3: we open the document     
                document.Open();

                PdfDiv divEspacio = new PdfDiv();
                divEspacio.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio.Height = 10;
                divEspacio.Width = 130;

                PdfDiv divEspacio2 = new PdfDiv();
                divEspacio2.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio2.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio2.Height = 2;
                divEspacio2.Width = 130;

                //FUENTES----------------------------------------------------------------------------------------------------------
                iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle3 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 9, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle2 = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustom = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitleFactura = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                //FUENTES---------------------------------------------------------------------------------------------------------

                System.Drawing.Image logo = null;
                var requestLogo = WebRequest.Create(RutaImg);

                using (var responseLogo = requestLogo.GetResponse())
                using (var streamLogo = responseLogo.GetResponseStream())
                {
                    logo = Bitmap.FromStream(streamLogo);
                }

                System.Drawing.Image logo2 = null;
                var requestLogo2 = WebRequest.Create(RutaImg);

                using (var responseLogo2 = requestLogo2.GetResponse())
                using (var streamLogo2 = responseLogo2.GetResponseStream())
                {
                    logo2 = Bitmap.FromStream(streamLogo2);
                }
                #endregion

                #region ENCABEZADO


                PdfPTable Header = new PdfPTable(4);
                Header.WidthPercentage = 100;

                //logo
                PdfPCell cell_logo = new PdfPCell() { Border = 0, };
                iTextSharp.text.Image ImgLogo2 = iTextSharp.text.Image.GetInstance(logo2, BaseColor.WHITE);
                ImgLogo2.ScaleAbsolute(100, 100);
                cell_logo.AddElement(ImgLogo2);
                Header.AddCell(cell_logo);

                //informacion de la empresa
                PdfPCell cell_info = new PdfPCell() { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, };
                Paragraph prgTitle = new Paragraph(string.Format("{0}.{1}", DsInvoiceAR.Tables["Company"].Rows[0]["Name"],
               "Nit" + DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"]), fontCustom);
                Paragraph prgTitle2 = new Paragraph(string.Format("{0}\n{1}\n \n{2}",
                    DsInvoiceAR.Tables["Company"].Rows[0]["Address1"].ToString(),
                    DsInvoiceAR.Tables["Company"].Rows[0]["City"].ToString(),
                    DsInvoiceAR.Tables["Company"].Rows[0]["State"].ToString()),
                    fontCustom);
                prgTitle.Alignment = Element.ALIGN_CENTER;
                prgTitle2.Alignment = Element.ALIGN_CENTER;
                cell_info.AddElement(prgTitle);
                cell_info.AddElement(prgTitle2);
                Header.AddCell(cell_info);

                //Qr
                PdfPCell cell_Qr = new PdfPCell() { Border = 0, };
                iTextSharp.text.Image ImgQR = iTextSharp.text.Image.GetInstance(QRInvoice, BaseColor.WHITE);
                ImgQR.ScaleAbsolute(70, 70);
                cell_Qr.AddElement(ImgQR);
                Header.AddCell(cell_Qr);
                //tipo de de documento
                PdfPCell cell_factura = new PdfPCell() { Border = 0, };
                PdfPTable tableFactura = new PdfPTable(3);
                //Dimenciones.
                float[] DimencionFactura = new float[3];
                DimencionFactura[0] = 1.0F;//
                DimencionFactura[1] = 4.0F;//
                DimencionFactura[2] = 0.5F;//

                tableFactura.WidthPercentage = 100;
                tableFactura.SetWidths(DimencionFactura);

                iTextSharp.text.pdf.PdfPCell celTittle = new iTextSharp.text.pdf.PdfPCell(new Phrase("FACTURA DE VENTAS\n\n", fontTitleFactura));
                celTittle.Colspan = 3;
                celTittle.Padding = 3;
                celTittle.HorizontalAlignment = Element.ALIGN_CENTER;
                celTittle.VerticalAlignment = Element.ALIGN_TOP;
                celTittle.Border = 0;
                celTittle.BorderWidthTop = 1;
                celTittle.BorderWidthLeft = 1;
                celTittle.BorderWidthRight = 1;
                celTittle.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableFactura.AddCell(celTittle);

                iTextSharp.text.pdf.PdfPCell celNo = new iTextSharp.text.pdf.PdfPCell(new Phrase("No. ", fontTitleFactura));
                celNo.Colspan = 1;
                celNo.Padding = 5;
                celNo.HorizontalAlignment = Element.ALIGN_CENTER;
                celNo.VerticalAlignment = Element.ALIGN_TOP;
                celNo.Border = 0;
                celNo.BorderWidthLeft = 1;
                celNo.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableFactura.AddCell(celNo);

                string NumLegalFactura = string.Empty;
                if (InvoiceType == "InvoiceType")
                    NumLegalFactura = DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceRef"].ToString();
                else if (InvoiceType == "CreditNoteType")
                    NumLegalFactura = DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"].ToString();
                else
                    NumLegalFactura = DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"].ToString();

                iTextSharp.text.pdf.PdfPCell celNoFactura = new iTextSharp.text.pdf.PdfPCell(new Phrase(NumLegalFactura, fontTitleFactura));
                celNoFactura.Colspan = 1;
                celNoFactura.Padding = 5;
                celNoFactura.HorizontalAlignment = Element.ALIGN_LEFT;
                celNoFactura.VerticalAlignment = Element.ALIGN_TOP;
                //celNoFactura.Border = 1;
                celNoFactura.BackgroundColor = BaseColor.WHITE;
                tableFactura.AddCell(celNoFactura);


                iTextSharp.text.pdf.PdfPCell celEspacioNoFactura = new iTextSharp.text.pdf.PdfPCell(new Phrase(" ", fontTitleFactura));
                celEspacioNoFactura.Colspan = 1;
                celEspacioNoFactura.Border = 0;
                //celNo.Padding = 3;
                celEspacioNoFactura.BorderWidthRight = 1;
                celEspacioNoFactura.HorizontalAlignment = Element.ALIGN_CENTER;
                celEspacioNoFactura.VerticalAlignment = Element.ALIGN_TOP;
                celEspacioNoFactura.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableFactura.AddCell(celEspacioNoFactura);

                iTextSharp.text.pdf.PdfPCell celRellenoNoFactura = new iTextSharp.text.pdf.PdfPCell(new Phrase(" ", fontTitleFactura));
                celRellenoNoFactura.Colspan = 3;
                celRellenoNoFactura.Padding = 3;
                celRellenoNoFactura.Border = 0;
                celRellenoNoFactura.BorderWidthLeft = 1;
                celRellenoNoFactura.BorderWidthRight = 1;
                celRellenoNoFactura.BorderWidthBottom = 1;
                celRellenoNoFactura.BackgroundColor = BaseColor.LIGHT_GRAY;
                celRellenoNoFactura.HorizontalAlignment = Element.ALIGN_RIGHT;
                celRellenoNoFactura.VerticalAlignment = Element.ALIGN_TOP;
                tableFactura.AddCell(celRellenoNoFactura);

                iTextSharp.text.pdf.PdfPCell celConsecutivoInterno = new iTextSharp.text.pdf.PdfPCell(new Phrase("CUFE:" + CUFE, fontCustom));
                celConsecutivoInterno.Colspan = 4;
                celConsecutivoInterno.BackgroundColor = BaseColor.WHITE;
                celConsecutivoInterno.HorizontalAlignment = Element.ALIGN_RIGHT;
                celConsecutivoInterno.VerticalAlignment = Element.ALIGN_TOP;
                celConsecutivoInterno.Border = 0;
                tableFactura.AddCell(celConsecutivoInterno);

                PdfDiv divNumeroFactura = new PdfDiv();
                divNumeroFactura.Float = iTextSharp.text.pdf.PdfDiv.FloatType.LEFT;
                divNumeroFactura.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divNumeroFactura.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                divNumeroFactura.PaddingLeft = 5;
                divNumeroFactura.Width = 140;
                divNumeroFactura.AddElement(tableFactura);
                cell_factura.AddElement(tableFactura);
                Header.AddCell(cell_factura);


                #endregion

                #region FACTURAR Y DESPACHAR A
                //------------------------------------------------------------------------------------------------
                PdfPTable tableFacturar = new PdfPTable(1);
                //Dimenciones.
                float[] DimencionFacturar = new float[1];
                DimencionFacturar[0] = 1.0F;//


                tableFacturar.WidthPercentage = 100;
                tableFacturar.SetWidths(DimencionFacturar);
                //----------------------------------------------------------------------------------------------
                PdfPTable tableFacturarA = new PdfPTable(2);
                float[] DimencionFacturarA = new float[2];
                DimencionFacturarA[0] = 0.8F;//
                DimencionFacturarA[1] = 2.0F;//

                tableFacturarA.WidthPercentage = 100;
                tableFacturarA.SetWidths(DimencionFacturarA);

                iTextSharp.text.pdf.PdfPCell celDatosFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase("FACTURAR A:\n", fontTitleFactura)) { Border = 0, };
                celDatosFacturarA.Colspan = 2;
                celDatosFacturarA.Padding = 3;
                celDatosFacturarA.Border = 0;
                celDatosFacturarA.BorderColorBottom = BaseColor.WHITE;
                celDatosFacturarA.HorizontalAlignment = Element.ALIGN_CENTER;
                celDatosFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celDatosFacturarA);

                iTextSharp.text.pdf.PdfPCell celTextClienteDespacharA2 = new iTextSharp.text.pdf.PdfPCell(new Phrase("CLIENTE:", fontTitleFactura));
                celTextClienteDespacharA2.Colspan = 1;
                celTextClienteDespacharA2.Padding = 3;
                celTextClienteDespacharA2.Border = 0;
                celTextClienteDespacharA2.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextClienteDespacharA2.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celTextClienteDespacharA2);

                iTextSharp.text.pdf.PdfPCell celClienteDespacharA2 = new iTextSharp.text.pdf.PdfPCell(new Phrase("" + DsInvoiceAR.Tables["Customer"].Rows[0]["Name"].ToString(), fontTitle2));
                celClienteDespacharA2.Colspan = 1;
                celClienteDespacharA2.Padding = 3;
                celClienteDespacharA2.Border = 0;
                celClienteDespacharA2.HorizontalAlignment = Element.ALIGN_LEFT;
                celClienteDespacharA2.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celClienteDespacharA2);

                iTextSharp.text.pdf.PdfPCell celTextNitDespacharA2 = new iTextSharp.text.pdf.PdfPCell(new Phrase("NIT:", fontTitleFactura));
                celTextNitDespacharA2.Colspan = 1;
                celTextNitDespacharA2.Padding = 3;
                celTextNitDespacharA2.Border = 0;
                celTextNitDespacharA2.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextNitDespacharA2.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celTextNitDespacharA2);

                iTextSharp.text.pdf.PdfPCell celNitDespacharA2 = new iTextSharp.text.pdf.PdfPCell(new Phrase("" + DsInvoiceAR.Tables["Customer"].Rows[0]["CustID"].ToString(), fontTitle2));
                celNitDespacharA2.Colspan = 1;
                celNitDespacharA2.Padding = 3;
                celNitDespacharA2.Border = 0;
                celNitDespacharA2.HorizontalAlignment = Element.ALIGN_LEFT;
                celNitDespacharA2.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celNitDespacharA2);

                iTextSharp.text.pdf.PdfPCell celTextDireccionDespacharA2 = new iTextSharp.text.pdf.PdfPCell(new Phrase("DIRECCION:", fontTitleFactura));
                celTextDireccionDespacharA2.Colspan = 1;
                celTextDireccionDespacharA2.Padding = 3;
                celTextDireccionDespacharA2.Border = 0;
                celTextDireccionDespacharA2.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextDireccionDespacharA2.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celTextDireccionDespacharA2);

                iTextSharp.text.pdf.PdfPCell celDireccionDespacharA2 = new iTextSharp.text.pdf.PdfPCell(new Phrase("" + DsInvoiceAR.Tables["Customer"].Rows[0]["Address1"].ToString(), fontTitle2));
                celDireccionDespacharA2.Colspan = 1;
                celDireccionDespacharA2.Padding = 3;
                celDireccionDespacharA2.Border = 0;
                celDireccionDespacharA2.HorizontalAlignment = Element.ALIGN_LEFT;
                celDireccionDespacharA2.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celDireccionDespacharA2);

                iTextSharp.text.pdf.PdfPCell celTextTelDespacharA2 = new iTextSharp.text.pdf.PdfPCell(new Phrase("TELEFONO:", fontTitleFactura));
                celTextTelDespacharA2.Colspan = 1;
                celTextTelDespacharA2.Padding = 3;
                celTextTelDespacharA2.Border = 0;
                celTextTelDespacharA2.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextTelDespacharA2.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celTextTelDespacharA2);

                iTextSharp.text.pdf.PdfPCell celTelDespacharA2 = new iTextSharp.text.pdf.PdfPCell(new Phrase("" + DsInvoiceAR.Tables["Customer"].Rows[0]["PhoneNum"].ToString(), fontTitle2));
                celTelDespacharA2.Colspan = 1;
                celTelDespacharA2.Padding = 3;
                celTelDespacharA2.Border = 0;
                celTelDespacharA2.HorizontalAlignment = Element.ALIGN_LEFT;
                celTelDespacharA2.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celTelDespacharA2);

                iTextSharp.text.pdf.PdfPCell celTextCiudadDespacharA2 = new iTextSharp.text.pdf.PdfPCell(new Phrase("CIUDAD:", fontTitleFactura));
                celTextCiudadDespacharA2.Colspan = 1;
                celTextCiudadDespacharA2.Padding = 3;
                celTextCiudadDespacharA2.Border = 0;
                celTextCiudadDespacharA2.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextCiudadDespacharA2.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celTextCiudadDespacharA2);

                iTextSharp.text.pdf.PdfPCell celCiudadDespacharA2 = new iTextSharp.text.pdf.PdfPCell(new Phrase("" + DsInvoiceAR.Tables["Customer"].Rows[0]["City"].ToString(), fontTitle2));
                celCiudadDespacharA2.Colspan = 1;
                celCiudadDespacharA2.Padding = 3;
                celCiudadDespacharA2.Border = 0;
                celCiudadDespacharA2.HorizontalAlignment = Element.ALIGN_LEFT;
                celCiudadDespacharA2.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celCiudadDespacharA2);

                iTextSharp.text.pdf.PdfPCell celTextPaisDespacharA2 = new iTextSharp.text.pdf.PdfPCell(new Phrase("PAIS:", fontTitleFactura));
                celTextPaisDespacharA2.Colspan = 1;
                celTextPaisDespacharA2.Padding = 3;
                celTextPaisDespacharA2.Border = 0;
                celTextPaisDespacharA2.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextPaisDespacharA2.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celTextPaisDespacharA2);

                iTextSharp.text.pdf.PdfPCell celPaisDespacharA2 = new iTextSharp.text.pdf.PdfPCell(new Phrase("" + DsInvoiceAR.Tables["Customer"].Rows[0]["ShortChar01"].ToString(), fontTitle2));
                celPaisDespacharA2.Colspan = 1;
                celPaisDespacharA2.Padding = 3;
                celPaisDespacharA2.Border = 0;
                celPaisDespacharA2.HorizontalAlignment = Element.ALIGN_LEFT;
                celPaisDespacharA2.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celPaisDespacharA2);

                iTextSharp.text.pdf.PdfPCell celDatosDespacharA2 = new iTextSharp.text.pdf.PdfPCell(tableFacturarA);
                tableFacturar.AddCell(celDatosDespacharA2);



                //----------------------------------------------------------------------------------------------------------------------------

                iTextSharp.text.pdf.PdfPCell celEspacio2 = new iTextSharp.text.pdf.PdfPCell(new Phrase(" ", fontTitleFactura));
                celEspacio2.Border = 0;
                tableFacturar.AddCell(celEspacio2);

                #endregion

                #region Tabla de Detalles
                //------------------------------------------------------------------------------------------------


                PdfPTable tableDetalles2 = new PdfPTable(3);
                tableDetalles2.WidthPercentage = 100;

                PdfPTable tableFechaFactura = new PdfPTable(2);
                float[] dimecionesTablaFecha = new float[2];
                dimecionesTablaFecha[0] = 0.7F;
                dimecionesTablaFecha[1] = 1.0F;
                tableFechaFactura.SetWidths(dimecionesTablaFecha);

                iTextSharp.text.pdf.PdfPCell celTextFechaFactura = new iTextSharp.text.pdf.PdfPCell(new Phrase("FECHA FACTURA: ",
                    fontTitle));
                celTextFechaFactura.Colspan = 1;
                celTextFechaFactura.Padding = 3;
                celTextFechaFactura.Border = 0;
                celTextFechaFactura.BorderColorBottom = BaseColor.WHITE;
                celTextFechaFactura.HorizontalAlignment = Element.ALIGN_CENTER;
                celTextFechaFactura.VerticalAlignment = Element.ALIGN_CENTER;
                tableFechaFactura.AddCell(celTextFechaFactura);

                iTextSharp.text.pdf.PdfPCell celFechaFactura = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:MM/dd/yyyy}", DateTime.Parse(DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"].ToString())),
                fontCustom));
                celFechaFactura.Colspan = 1;
                celFechaFactura.Padding = 3;
                celFechaFactura.PaddingTop = 4;
                celFechaFactura.Border = 0;
                celFechaFactura.BorderColorBottom = BaseColor.WHITE;
                celFechaFactura.HorizontalAlignment = Element.ALIGN_LEFT;
                celFechaFactura.VerticalAlignment = Element.ALIGN_CENTER;
                tableFechaFactura.AddCell(celFechaFactura);

                PdfPCell _celFechaFactura = new PdfPCell(tableFechaFactura);
                tableDetalles2.AddCell(_celFechaFactura);


                PdfPTable tableFechaVencimiento = new PdfPTable(2);
                float[] dmTablaFechaVencimiento = new float[2];
                dmTablaFechaVencimiento[0] = 1.0F;
                dmTablaFechaVencimiento[1] = 1.0F;
                tableFechaVencimiento.SetWidths(dmTablaFechaVencimiento);

                iTextSharp.text.pdf.PdfPCell celTextFechaVencimiento = new iTextSharp.text.pdf.PdfPCell(new Phrase("FECHA VENCIMIENTO: ",
                    fontTitle));
                celTextFechaVencimiento.Colspan = 1;
                celTextFechaVencimiento.Padding = 3;
                celTextFechaVencimiento.Border = 0;
                celTextFechaVencimiento.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextFechaVencimiento.VerticalAlignment = Element.ALIGN_TOP;
                tableFechaVencimiento.AddCell(celTextFechaVencimiento);

                iTextSharp.text.pdf.PdfPCell celFechaVencimiento = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:MM/dd/yyyy}",
                   DateTime.Parse(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"].ToString())),
                fontCustom));
                celFechaVencimiento.Colspan = 1;
                celFechaVencimiento.Padding = 4;
                celFechaVencimiento.Border = 0;
                celFechaVencimiento.BorderColorBottom = BaseColor.WHITE;
                celFechaVencimiento.HorizontalAlignment = Element.ALIGN_LEFT;
                celFechaVencimiento.VerticalAlignment = Element.ALIGN_TOP;
                tableFechaVencimiento.AddCell(celFechaVencimiento);

                PdfPCell _celFechaFacturaVencimiento = new PdfPCell(tableFechaVencimiento);
                tableDetalles2.AddCell(_celFechaFacturaVencimiento);

                PdfPTable tableMoneda = new PdfPTable(2);
                float[] dimecionesTablaMoneda = new float[2];
                dimecionesTablaMoneda[0] = 0.4F;
                dimecionesTablaMoneda[1] = 1.0F;
                tableMoneda.SetWidths(dimecionesTablaMoneda);

                iTextSharp.text.pdf.PdfPCell celTextMoneda = new iTextSharp.text.pdf.PdfPCell(new Phrase("MONEDA: ",
                    fontTitle));
                celTextMoneda.Colspan = 1;
                celTextMoneda.Padding = 3;
                celTextMoneda.Border = 0;
                celTextMoneda.BorderColorBottom = BaseColor.WHITE;
                celTextMoneda.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextMoneda.VerticalAlignment = Element.ALIGN_TOP;
                tableMoneda.AddCell(celTextMoneda);

                iTextSharp.text.pdf.PdfPCell celMoneda = new iTextSharp.text.pdf.PdfPCell(new Phrase(DsInvoiceAR.Tables["InvcHead"].Rows[0]["CurrencyCode"].ToString(),
                fontCustom));
                celMoneda.Colspan = 1;
                celMoneda.Padding = 4;
                celMoneda.Border = 0;
                celMoneda.BorderColorBottom = BaseColor.WHITE;
                celMoneda.HorizontalAlignment = Element.ALIGN_LEFT;
                celMoneda.VerticalAlignment = Element.ALIGN_TOP;
                tableMoneda.AddCell(celMoneda);

                PdfPCell _celMoneda = new PdfPCell(tableMoneda);
                tableDetalles2.AddCell(_celMoneda);
                #endregion

                #region Tabla Unidades
                PdfPTable tableTituloUnidades = new PdfPTable(4);
                //Dimenciones.
                float[] DimencionUnidades = new float[4];
                DimencionUnidades[0] = 3.0F;//
                DimencionUnidades[1] = 1.0F;//
                DimencionUnidades[2] = 1.0F;//
                DimencionUnidades[3] = 1.5F;//

                tableTituloUnidades.WidthPercentage = 100;
                tableTituloUnidades.SetWidths(DimencionUnidades);


                iTextSharp.text.pdf.PdfPCell celDescripcion = new iTextSharp.text.pdf.PdfPCell(new Phrase("DESCRIPCIÓN", fontTitleFactura));
                celDescripcion.Colspan = 1;
                celDescripcion.Padding = 3;
                //celDescripcion.Border = 1;
                celDescripcion.BorderWidthRight = 0;
                //celDescripcion.BorderColorBottom = BaseColor.WHITE;
                celDescripcion.HorizontalAlignment = Element.ALIGN_CENTER;
                celDescripcion.VerticalAlignment = Element.ALIGN_TOP;
                celDescripcion.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableTituloUnidades.AddCell(celDescripcion);

                iTextSharp.text.pdf.PdfPCell celCantidad = new iTextSharp.text.pdf.PdfPCell(new Phrase("CANTIDAD", fontTitleFactura));
                celCantidad.Colspan = 1;
                celCantidad.Padding = 3;
                //celVendedor.Border = 0;
                celCantidad.BorderWidthRight = 0;
                //celCantidad.BorderColorBottom = BaseColor.WHITE;
                celCantidad.HorizontalAlignment = Element.ALIGN_CENTER;
                celCantidad.VerticalAlignment = Element.ALIGN_TOP;
                celCantidad.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableTituloUnidades.AddCell(celCantidad);

                iTextSharp.text.pdf.PdfPCell celValorUnitario = new iTextSharp.text.pdf.PdfPCell(new Phrase("VALOR UNITARIO", fontTitleFactura));
                celValorUnitario.Colspan = 1;
                celValorUnitario.Padding = 3;
                //celVendedor.Border = 0;
                celValorUnitario.BorderWidthRight = 0;
                //celValorUnitario.BorderColorBottom = BaseColor.WHITE;
                celValorUnitario.HorizontalAlignment = Element.ALIGN_CENTER;
                celValorUnitario.VerticalAlignment = Element.ALIGN_TOP;
                celValorUnitario.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableTituloUnidades.AddCell(celValorUnitario);

                iTextSharp.text.pdf.PdfPCell celValorTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase("VALOR TOTAL", fontTitleFactura));
                celValorTotal.Colspan = 1;
                celValorTotal.Padding = 3;
                //celVendedor.Border = 0;
                //celValorTotal.BorderColorBottom = BaseColor.WHITE;
                celValorTotal.HorizontalAlignment = Element.ALIGN_CENTER;
                celValorTotal.VerticalAlignment = Element.ALIGN_TOP;
                celValorTotal.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableTituloUnidades.AddCell(celValorTotal);

                PdfPTable tableUnidades = new PdfPTable(4);
                tableUnidades.WidthPercentage = 100;
                tableUnidades.SetWidths(DimencionUnidades);

                foreach (DataRow InvoiceLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
                {
                    if (!AddUnidadesTomyPuertas(InvoiceLine, ref tableUnidades, fontCustom, DsInvoiceAR))
                        return false;
                }

                iTextSharp.text.pdf.PdfPCell LineaFinal = new iTextSharp.text.pdf.PdfPCell();
                LineaFinal.Colspan = 4;
                LineaFinal.Padding = 3;
                LineaFinal.Border = 1;
                LineaFinal.BorderColorBottom = BaseColor.WHITE;
                LineaFinal.HorizontalAlignment = Element.ALIGN_CENTER;
                LineaFinal.VerticalAlignment = Element.ALIGN_TOP;
                tableUnidades.AddCell(LineaFinal);

                //------------------------------------------------------------------------------------------------
                iTextSharp.text.Font fontEspacio = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 0.3F, iTextSharp.text.Font.NORMAL);
                PdfPCell celEspacio3 = new PdfPCell(new Phrase("  ", fontEspacio));
                celEspacio3.Border = 0;

                PdfPTable tableTotalesObs = new PdfPTable(3);

                float[] DimencionTotalesObs = new float[3];
                DimencionTotalesObs[0] = 2.5F;//
                DimencionTotalesObs[1] = 0.01F;//
                DimencionTotalesObs[2] = 1.0F;//

                tableTotalesObs.WidthPercentage = 100;
                tableTotalesObs.SetWidths(DimencionTotalesObs);

                PdfPTable _tableObs = new PdfPTable(1);
                _tableObs.WidthPercentage = 100;

                iTextSharp.text.pdf.PdfPCell celValorLetras = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("VALORES EN LETRA: {0}",
                    Nroenletras(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"].ToString())), fontTitleFactura));
                celValorLetras.Colspan = 1;
                celValorLetras.Padding = 3;
                //celValorLetras.Border = 0;
                celValorLetras.BorderColorBottom = BaseColor.WHITE;
                celValorLetras.HorizontalAlignment = Element.ALIGN_LEFT;
                celValorLetras.VerticalAlignment = Element.ALIGN_TOP;


                iTextSharp.text.pdf.PdfPCell celObservaciones = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("OBSERVACIONES:\n{0}",
                    DsInvoiceAR.Tables["InvcHead"].Rows[0]["ObservacionCliente_c"].ToString()), fontTitleFactura));
                celObservaciones.Colspan = 1;
                celObservaciones.Padding = 3;
                //celValorLetras.Border = 0;
                celObservaciones.BorderColorBottom = BaseColor.WHITE;
                celObservaciones.HorizontalAlignment = Element.ALIGN_LEFT;
                celObservaciones.VerticalAlignment = Element.ALIGN_TOP;

                _tableObs.AddCell(celValorLetras);
                _tableObs.AddCell(celEspacio3);
                _tableObs.AddCell(celEspacio3);
                _tableObs.AddCell(celObservaciones);

                iTextSharp.text.pdf.PdfPCell _celObs = new iTextSharp.text.pdf.PdfPCell(_tableObs);
                _celObs.Border = 0;
                tableTotalesObs.AddCell(_celObs);
                //----------------------------------------------------------------------------------------------------------------------------

                iTextSharp.text.pdf.PdfPCell _celEspacio2 = new iTextSharp.text.pdf.PdfPCell(new Phrase(" ", fontTitleFactura));
                _celEspacio2.Border = 0;
                tableTotalesObs.AddCell(_celEspacio2);

                //------------------------------------------------------------------------------------------

                PdfPTable _tableTotales = new PdfPTable(2);
                float[] _DimencionTotales = new float[2];
                _DimencionTotales[0] = 1.5F;//
                _DimencionTotales[1] = 2.5F;//

                _tableTotales.WidthPercentage = 100;
                _tableTotales.SetWidths(_DimencionTotales);

                iTextSharp.text.pdf.PdfPCell _celTextSubTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase("SUBTOTAL", fontTitleFactura));
                _celTextSubTotal.Colspan = 1;
                _celTextSubTotal.Padding = 3;
                _celTextSubTotal.PaddingBottom = 5;
                //_celTextSubTotal.Border = 0;
                _celTextSubTotal.BorderWidthRight = 1;
                _celTextSubTotal.BorderColorBottom = BaseColor.WHITE;
                _celTextSubTotal.HorizontalAlignment = Element.ALIGN_LEFT;
                _celTextSubTotal.VerticalAlignment = Element.ALIGN_TOP;
                _tableTotales.AddCell(_celTextSubTotal);



                iTextSharp.text.pdf.PdfPCell _celSubTotal2 = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:C2}",
                    decimal.Parse(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DocTaxAmt"].ToString())), fontTitle2));
                _celSubTotal2.Colspan = 1;
                _celSubTotal2.Padding = 3;
                _celSubTotal2.PaddingBottom = 5;
                //_celSubTotal2.Border = 0;
                _celSubTotal2.BorderColorBottom = BaseColor.WHITE;
                _celSubTotal2.HorizontalAlignment = Element.ALIGN_RIGHT;
                _celSubTotal2.VerticalAlignment = Element.ALIGN_TOP;
                _tableTotales.AddCell(_celSubTotal2);


                iTextSharp.text.pdf.PdfPCell _celTextIva = new iTextSharp.text.pdf.PdfPCell(new Phrase("IVA", fontTitleFactura));
                _celTextIva.Colspan = 1;
                _celTextIva.Padding = 3;
                _celTextIva.PaddingBottom = 5;
                //_celTextIva.Border = 0;
                _celTextIva.BorderWidthRight = 1;
                //_celTextIva.BorderColorBottom = BaseColor.BLACK;
                _celTextIva.HorizontalAlignment = Element.ALIGN_LEFT;
                _celTextIva.VerticalAlignment = Element.ALIGN_TOP;
                _tableTotales.AddCell(_celTextIva);


                iTextSharp.text.pdf.PdfPCell _celIva = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:C2}",
                    decimal.Parse(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DocWHTaxAmt"].ToString())), fontTitle2));
                _celIva.Colspan = 1;
                _celIva.Padding = 3;
                _celIva.PaddingBottom = 5;
                _celIva.Border = 0;
                //_celIva.BorderColorBottom = BaseColor.BLACK;
                _celIva.HorizontalAlignment = Element.ALIGN_RIGHT;
                _celIva.VerticalAlignment = Element.ALIGN_TOP;
                _tableTotales.AddCell(_celIva);

                iTextSharp.text.pdf.PdfPCell _celTextTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase("TOTAL", fontTitleFactura));
                _celTextTotal.Colspan = 1;
                _celTextTotal.Padding = 3;
                _celTextTotal.PaddingBottom = 5;
                //_celTextTotal.Border = 0;
                _celTextTotal.BorderWidthRight = 1;
                _celTextTotal.BorderWidthTop = 1;
                _celTextTotal.HorizontalAlignment = Element.ALIGN_LEFT;
                _celTextTotal.VerticalAlignment = Element.ALIGN_TOP;
                _tableTotales.AddCell(_celTextTotal);

                iTextSharp.text.pdf.PdfPCell _celTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:C2}",
                    decimal.Parse(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"].ToString())), fontTitleFactura));
                _celTotal.Colspan = 1;
                _celTotal.Padding = 3;
                _celTotal.PaddingBottom = 5;
                //_celTotal.Border = 0;
                _celTotal.BorderWidthTop = 1;
                _celTotal.HorizontalAlignment = Element.ALIGN_RIGHT;
                _celTotal.VerticalAlignment = Element.ALIGN_TOP;
                _tableTotales.AddCell(_celTotal);

                iTextSharp.text.pdf.PdfPCell _celTotales = new iTextSharp.text.pdf.PdfPCell(_tableTotales);
                tableTotalesObs.AddCell(_celTotales);

                //------------------------------------------------------------------------
                PdfPTable tableResolucion = new PdfPTable(1);
                tableResolucion.WidthPercentage = 100;
                string strResolucion = "";
                iTextSharp.text.pdf.PdfPCell _celResolucion = new iTextSharp.text.pdf.PdfPCell(new Phrase(strResolucion, fontTitleFactura));
                _celResolucion.Colspan = 1;
                _celResolucion.Padding = 3;
                //_celResolucion.Border = 0;
                _celResolucion.HorizontalAlignment = Element.ALIGN_JUSTIFIED_ALL;
                _celResolucion.VerticalAlignment = Element.ALIGN_TOP;
                tableResolucion.AddCell(_celResolucion);

                //----------------------------------------------------------------------
                PdfPTable tableFirmas = new PdfPTable(5);

                float[] DimencionFirmas = new float[5];
                DimencionFirmas[0] = 1.0F;//
                DimencionFirmas[1] = 0.02F;//
                DimencionFirmas[2] = 1.0F;//
                DimencionFirmas[3] = 0.02F;//
                DimencionFirmas[4] = 1.4F;//

                tableFirmas.WidthPercentage = 100;
                tableFirmas.SetWidths(DimencionFirmas);

                PdfPTable tableDespacahdoPor = new PdfPTable(1);
                tableDespacahdoPor.WidthPercentage = 100;
                iTextSharp.text.pdf.PdfPCell celDespachadoPor = new iTextSharp.text.pdf.PdfPCell(new Phrase("", fontTitleFactura))
                {
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    VerticalAlignment = Element.ALIGN_BOTTOM,

                };
                celDespachadoPor.Colspan = 1;
                celDespachadoPor.Padding = 3;
                celDespachadoPor.Border = 0;
                celDespachadoPor.HorizontalAlignment = Element.ALIGN_CENTER;
                //celDespachadoPor.VerticalAlignment = Element.ALIGN_TOP;
                tableDespacahdoPor.AddCell(celDespachadoPor);

                iTextSharp.text.pdf.PdfPCell _celDespachadoPor = new iTextSharp.text.pdf.PdfPCell(tableDespacahdoPor);
                tableFirmas.AddCell(_celDespachadoPor);
                //----------------------------------------------------------------------------------------------------------------------------

                tableFirmas.AddCell(celEspacio2);

                //------------------------------------------------------------------------------------------

                PdfPTable tableFirmaConductor = new PdfPTable(1);
                tableFirmaConductor.WidthPercentage = 100;
                iTextSharp.text.pdf.PdfPCell celFirmaConductor = new iTextSharp.text.pdf.PdfPCell(new Phrase("", fontTitleFactura))
                {
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    VerticalAlignment = Element.ALIGN_BOTTOM,
                };
                celFirmaConductor.Colspan = 1;
                celFirmaConductor.Padding = 3;
                celFirmaConductor.Border = 0;
                celFirmaConductor.HorizontalAlignment = Element.ALIGN_CENTER;
                //celFirmaConductor.VerticalAlignment = Element.ALIGN_TOP;
                tableFirmaConductor.AddCell(celFirmaConductor);

                iTextSharp.text.pdf.PdfPCell _celFirmaConductor = new iTextSharp.text.pdf.PdfPCell(tableFirmaConductor);
                tableFirmas.AddCell(_celFirmaConductor);

                //-------------------------------------------------------------------------------------
                tableFirmas.AddCell(celEspacio2);
                //-------------------------------------------------------------------------------------

                PdfPTable tableSelloCliente = new PdfPTable(1);
                //tableSelloCliente.WidthPercentage = 100;

                iTextSharp.text.pdf.PdfPCell cel1SelloCliente = new iTextSharp.text.pdf.PdfPCell(new Phrase(" " +
                    "", fontTitleFactura));
                cel1SelloCliente.Colspan = 1;
                cel1SelloCliente.Padding = 5;
                cel1SelloCliente.Border = 1;
                cel1SelloCliente.BorderWidthBottom = 1;
                cel1SelloCliente.HorizontalAlignment = Element.ALIGN_LEFT;
                cel1SelloCliente.VerticalAlignment = Element.ALIGN_TOP;
                tableSelloCliente.AddCell(cel1SelloCliente);

                iTextSharp.text.pdf.PdfPCell cel2SelloFechaRecibido = new iTextSharp.text.pdf.PdfPCell(new Phrase("", fontTitleFactura));

                cel2SelloFechaRecibido.Colspan = 1;
                cel2SelloFechaRecibido.Padding = 3;
                cel2SelloFechaRecibido.Border = 0;
                cel2SelloFechaRecibido.HorizontalAlignment = Element.ALIGN_LEFT;
                cel2SelloFechaRecibido.VerticalAlignment = Element.ALIGN_TOP;
                tableSelloCliente.AddCell(cel2SelloFechaRecibido);

                iTextSharp.text.pdf.PdfPCell cel2SelloNombre = new iTextSharp.text.pdf.PdfPCell(new Phrase("", fontTitleFactura));

                cel2SelloNombre.Colspan = 1;
                cel2SelloNombre.Padding = 3;
                cel2SelloNombre.Border = 0;
                cel2SelloNombre.HorizontalAlignment = Element.ALIGN_LEFT;
                cel2SelloNombre.VerticalAlignment = Element.ALIGN_TOP;
                tableSelloCliente.AddCell(cel2SelloNombre);

                iTextSharp.text.pdf.PdfPCell cel2SelloId = new iTextSharp.text.pdf.PdfPCell(new Phrase("", fontTitleFactura));

                cel2SelloId.Colspan = 1;
                cel2SelloId.Padding = 3;
                cel2SelloId.Border = 0;
                cel2SelloId.HorizontalAlignment = Element.ALIGN_LEFT;
                cel2SelloId.VerticalAlignment = Element.ALIGN_TOP;
                tableSelloCliente.AddCell(cel2SelloId);

                iTextSharp.text.pdf.PdfPCell cel2SelloFirma = new iTextSharp.text.pdf.PdfPCell(new Phrase("", fontTitleFactura));

                cel2SelloFirma.Colspan = 1;
                cel2SelloFirma.Padding = 3;
                cel2SelloFirma.Border = 0;
                cel2SelloFirma.HorizontalAlignment = Element.ALIGN_LEFT;
                cel2SelloFirma.VerticalAlignment = Element.ALIGN_TOP;
                tableSelloCliente.AddCell(cel2SelloFirma);

                iTextSharp.text.pdf.PdfPCell cel3SelloCliente = new iTextSharp.text.pdf.PdfPCell(new Phrase("\n\n" +
                    "DATOS DE CUENTA BANCARIA: XXXXXXXXXX", fontTitleFactura));

                cel3SelloCliente.Colspan = 1;
                cel3SelloCliente.Padding = 3;
                cel3SelloCliente.Border = 0;
                cel3SelloCliente.HorizontalAlignment = Element.ALIGN_CENTER;
                cel3SelloCliente.VerticalAlignment = Element.ALIGN_TOP;
                tableSelloCliente.AddCell(cel3SelloCliente);

                iTextSharp.text.pdf.PdfPCell _celSelloCliente = new iTextSharp.text.pdf.PdfPCell(tableSelloCliente);
                //_celSelloCliente.Border = 0;
                //_celSelloCliente.BorderColor = BaseColor.WHITE;
                tableFirmas.AddCell(_celSelloCliente);

                PdfPTable origin = new PdfPTable(1);
                origin.WidthPercentage = 100;
                PdfPCell origuinal = new PdfPCell(new Phrase("")) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER };
                origin.AddCell(origuinal);
                #endregion

                #region Exit

                document.Add(Header);
                document.Add(divEspacio);
                document.Add(tableFacturar);
                document.Add(divEspacio);
                document.Add(tableDetalles2);
                document.Add(divEspacio2);
                document.Add(tableTituloUnidades);
                document.Add(tableUnidades);
                document.Add(divEspacio2);
                document.Add(divEspacio2);
                document.Add(tableTotalesObs);
                document.Add(divEspacio2);
                document.Add(divEspacio2);
                document.Add(tableResolucion);
                document.Add(divEspacio2);
                document.Add(divEspacio2);
                document.Add(tableFirmas);
                document.Add(divEspacio2);
                document.Add(origin);

                //document.Add(divTextBancoCheque);
                /*PIE DE PAGINA*/
                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();
                RutaPdf = NomArchivo;
                return true;
                #endregion
            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.Message;
                return false;
            }
        }

        #endregion

        #region NOTA CREDITO Y DEBITO

        private bool AddUnidadesNOtaCreditoTomyPuertas(DataRow dataLine, ref PdfPTable table, iTextSharp.text.Font fontTitleFactura, DataSet dataSet)
        {
            try
            {
                //CODIGO
                strError += "Add InvoiceLine";
                iTextSharp.text.pdf.PdfPCell celValL = new iTextSharp.text.pdf.PdfPCell(new Phrase((string)dataLine["PartNum"], fontTitleFactura));
                celValL.Colspan = 1;
                celValL.Padding = 2;
                celValL.Border = 0;
                celValL.Border = PdfPCell.RIGHT_BORDER | PdfPCell.LEFT_BORDER;
                //celTextL.BorderWidthRight = 1;
                //celValL.BorderWidthLeft = 1;
                celValL.HorizontalAlignment = Element.ALIGN_CENTER;
                celValL.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValL);
                strError += "InvoiceLine OK";

                //DESCRIPCION
                strError += "Add PartNum";
                iTextSharp.text.pdf.PdfPCell celValCodigo = new iTextSharp.text.pdf.PdfPCell(new Phrase((string)dataLine["PartNumPartDescription"], fontTitleFactura));
                celValCodigo.Colspan = 1;
                celValCodigo.Padding = 2;
                celValCodigo.Border = 0;
                celValCodigo.Border = PdfPCell.RIGHT_BORDER;
                //celValCodigo.BorderWidthLeft = 1;
                //celDescripcion.BorderColorBottom = BaseColor.WHITE;
                celValCodigo.HorizontalAlignment = Element.ALIGN_CENTER;
                celValCodigo.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValCodigo);
                strError += "Add PartNum OK";

                //UNIDADES
                strError += "Add SellingShipQty";
                iTextSharp.text.pdf.PdfPCell celValDescripcion = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    decimal.Parse((string)dataLine["InvoiceLine"]).ToString("N0"), fontTitleFactura));
                celValDescripcion.Colspan = 1;
                celValDescripcion.Padding = 2;
                celValDescripcion.Border = 0;
                celValDescripcion.Border = PdfPCell.RIGHT_BORDER;
                //celValCantidad.BorderWidthLeft = 1;
                celValDescripcion.HorizontalAlignment = Element.ALIGN_CENTER;
                celValDescripcion.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValDescripcion);
                strError += "Add SellingShipQty OK";

                //SUB TOTAL
                strError += "Add LineDesc";
                iTextSharp.text.pdf.PdfPCell celValCantidad = new iTextSharp.text.pdf.PdfPCell(new Phrase("$ " +
                    decimal.Parse((string)dataLine["DspDocExtPrice"]).ToString("N0"), fontTitleFactura));
                celValCantidad.Colspan = 1;
                celValCantidad.Padding = 2;
                celValCantidad.Border = 0;
                celValCantidad.Border = PdfPCell.RIGHT_BORDER;
                //celValDesc.BorderWidthLeft = 1;
                celValCantidad.HorizontalAlignment = Element.ALIGN_CENTER;
                celValCantidad.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValCantidad);
                strError += "Add LineDesc OK";


                //iva
                strError += "Add SalesUM";
                iTextSharp.text.pdf.PdfPCell celValUnidad = new iTextSharp.text.pdf.PdfPCell(new Phrase(" " +
                    decimal.Parse((string)dataLine["SalesUM"]).ToString("N0"), fontTitleFactura));
                celValUnidad.Colspan = 1;
                celValUnidad.Padding = 2;
                celValUnidad.Border = 0;
                celValUnidad.Border = PdfPCell.RIGHT_BORDER;
                //celValUnidad.BorderWidthLeft = 1;
                celValUnidad.HorizontalAlignment = Element.ALIGN_CENTER;
                celValUnidad.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValUnidad);
                strError += "Add SalesUM OK";



                //SUB TOTAL
                strError += "Add LineDesc";
                iTextSharp.text.pdf.PdfPCell celValCantidad2 = new iTextSharp.text.pdf.PdfPCell(new Phrase("$ " +
                    decimal.Parse((string)dataLine["DspDocExtPrice"]).ToString("N0"), fontTitleFactura));
                celValCantidad2.Colspan = 1;
                celValCantidad2.Padding = 2;
                celValCantidad2.Border = 0;
                celValCantidad2.Border = PdfPCell.RIGHT_BORDER;
                //celValDesc.BorderWidthLeft = 1;
                celValCantidad2.HorizontalAlignment = Element.ALIGN_CENTER;
                celValCantidad2.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValCantidad2);
                strError += "Add LineDesc OK";

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return false;
            }
        }

        private bool AddUnidadesNOtaCreditoTomyPuertas(ref PdfPTable table)
        {
            try
            {
                iTextSharp.text.pdf.PdfPCell celValL = new iTextSharp.text.pdf.PdfPCell(new Phrase(" "));
                celValL.Colspan = 1;
                celValL.Padding = 3;
                celValL.Border = 0;
                //celTextL.BorderWidthRight = 1;
                celValL.BorderWidthLeft = 1;
                celValL.HorizontalAlignment = Element.ALIGN_CENTER;
                celValL.VerticalAlignment = Element.ALIGN_TOP;

                table.AddCell(celValL);

                iTextSharp.text.pdf.PdfPCell celValCodigo = new iTextSharp.text.pdf.PdfPCell(new Phrase(" "));
                celValCodigo.Colspan = 1;
                celValCodigo.Padding = 3;
                celValCodigo.Border = 0;
                celValCodigo.BorderWidthLeft = 1;
                //celDescripcion.BorderColorBottom = BaseColor.WHITE;
                celValCodigo.HorizontalAlignment = Element.ALIGN_CENTER;
                celValCodigo.VerticalAlignment = Element.ALIGN_TOP;

                table.AddCell(celValCodigo);

                iTextSharp.text.pdf.PdfPCell celValCantidad = new iTextSharp.text.pdf.PdfPCell(new Phrase(" "));
                celValCantidad.Colspan = 1;
                celValCantidad.Padding = 3;
                celValCantidad.Border = 0;
                celValCantidad.BorderWidthLeft = 1;
                celValCantidad.HorizontalAlignment = Element.ALIGN_CENTER;
                celValCantidad.VerticalAlignment = Element.ALIGN_TOP;

                table.AddCell(celValCantidad);

                iTextSharp.text.pdf.PdfPCell celValUnidad = new iTextSharp.text.pdf.PdfPCell(new Phrase(" "));
                celValUnidad.Colspan = 1;
                celValUnidad.Padding = 3;
                celValUnidad.Border = 0;
                celValUnidad.BorderWidthLeft = 1;
                celValUnidad.HorizontalAlignment = Element.ALIGN_CENTER;
                celValUnidad.VerticalAlignment = Element.ALIGN_TOP;

                table.AddCell(celValUnidad);

                iTextSharp.text.pdf.PdfPCell celValDesc = new iTextSharp.text.pdf.PdfPCell(new Phrase(" "));
                celValDesc.Colspan = 1;
                celValDesc.Padding = 3;
                celValDesc.Border = 0;
                celValDesc.BorderWidthLeft = 1;
                celValDesc.HorizontalAlignment = Element.ALIGN_CENTER;
                celValDesc.VerticalAlignment = Element.ALIGN_TOP;

                table.AddCell(celValDesc);

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return false;
            }
        }

        public bool NotaCreditolTomyPuertas(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {
            //NomArchivo = string.Empty;
            //string Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
            //string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            //string RutaImg = $@"{AppDomain.CurrentDomain.BaseDirectory}\LOGO_POLIETILENOS_DEL_VALLE.png";
            //string RutaMarcaAgua = $@"{AppDomain.CurrentDomain.BaseDirectory}\Logos ICONTEC.png";
            //NomArchivo = NombreInvoice + ".pdf";

            NomArchivo = string.Empty;
            string Ruta = HttpContext.Current.Server.MapPath("~/PDF/" + NIT + "/");
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            string RutaImg = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";
            NomArchivo = NombreInvoice + ".pdf";
            try
            {
                #region Head
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.
                Document document = new Document(iTextSharp.text.PageSize.LETTER);
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;

                // step 3: we open the document     
                document.Open();

                PdfDiv divEspacio = new PdfDiv();
                divEspacio.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio.Height = 10;
                divEspacio.Width = 130;

                PdfDiv divEspacio2 = new PdfDiv();
                divEspacio2.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio2.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio2.Height = 2;
                divEspacio2.Width = 130;

                //FUENTES----------------------------------------------------------------------------------------------------------
                iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle3 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 9, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle2 = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustom = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitleFactura = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                //FUENTES---------------------------------------------------------------------------------------------------------

                System.Drawing.Image logo = null;
                var requestLogo = WebRequest.Create(RutaImg);

                using (var responseLogo = requestLogo.GetResponse())
                using (var streamLogo = responseLogo.GetResponseStream())
                {
                    logo = Bitmap.FromStream(streamLogo);
                }

                System.Drawing.Image logo2 = null;
                var requestLogo2 = WebRequest.Create(RutaImg);

                using (var responseLogo2 = requestLogo2.GetResponse())
                using (var streamLogo2 = responseLogo2.GetResponseStream())
                {
                    logo2 = Bitmap.FromStream(streamLogo2);
                }
                #endregion

                #region Header

                PdfPTable Header = new PdfPTable(new float[] { 0.35f, 0.45f, 0.2f, });
                Header.WidthPercentage = 100;

                PdfPCell cell_logo = new PdfPCell() { Border = 0, };
                iTextSharp.text.Image ImgLogo2 = iTextSharp.text.Image.GetInstance(logo2, BaseColor.WHITE);
                ImgLogo2.ScaleAbsolute(170, 170);
                cell_logo.AddElement(ImgLogo2);
                Header.AddCell(cell_logo);

                PdfPCell cell_info = new PdfPCell(new Phrase("")) { Border = 0, };
                Paragraph prgTitle = new Paragraph(string.Format("Polietilenos del Valle S.A ", fontCustom));
                Paragraph prgTitle2 = new Paragraph(string.Format("{0}\n{1}\n \n{2}",
                   "Nit " + DsInvoiceAR.Tables["Company"].Rows[0]["Address1"].ToString(),
                   "Dirección " + DsInvoiceAR.Tables["Company"].Rows[0]["Address1"].ToString(),
                   "Autorretenedores , IVA regimen comun ", fontCustom))
                { };
                prgTitle.Alignment = Element.ALIGN_CENTER;
                prgTitle2.Alignment = Element.ALIGN_CENTER;
                cell_info.AddElement(prgTitle);
                cell_info.AddElement(prgTitle2);
                Header.AddCell(cell_info);

                PdfPCell cell_Qr = new PdfPCell() { HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_CENTER, Border = 0, };
                iTextSharp.text.Image ImgQR = iTextSharp.text.Image.GetInstance(QRInvoice, BaseColor.WHITE);
                ImgQR.ScaleAbsolute(80, 80);
                cell_Qr.AddElement(ImgQR);
                Header.AddCell(cell_Qr);

                PdfPTable cliente = new PdfPTable(2);
                cliente.WidthPercentage = 100;

                PdfPCell cell_info_clientes = new PdfPCell(new Phrase("")) { MinimumHeight = 50, };
                Paragraph prgTitle3 = new Paragraph(string.Format("SR. (ES): " + DsInvoiceAR.Tables["Customer"].Rows[0]["Name"], fontCustom));
                Paragraph prgTitle4 = new Paragraph(string.Format("{0}\n{1}\n{2}\n",
                  "DIR: " + DsInvoiceAR.Tables["Customer"].Rows[0]["Address1"].ToString(),
                  "TEL: " + DsInvoiceAR.Tables["Customer"].Rows[0]["PhoneNum"].ToString(),
                  "CIUDAD: " + DsInvoiceAR.Tables["Customer"].Rows[0]["City"].ToString()),
                    fontCustom);
                prgTitle.Alignment = Element.ALIGN_CENTER;
                prgTitle2.Alignment = Element.ALIGN_CENTER;
                cell_info_clientes.AddElement(prgTitle3);
                cell_info_clientes.AddElement(prgTitle4);
                cliente.AddCell(cell_info_clientes);

                PdfPCell fehcas = new PdfPCell();



                PdfPTable techas_c = new PdfPTable(3);
                techas_c.WidthPercentage = 100;

                PdfPCell cell_fecha_fact = new PdfPCell() { Border = 0, };
                Paragraph prgf_f_t = new Paragraph(string.Format("Fecha Fect", fontCustom));
                Paragraph prgf_f = new Paragraph(string.Format($"{DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).ToString("dd/MM/yyyy")}", fontCustom));
                prgf_f_t.Alignment = Element.ALIGN_CENTER;
                prgf_f.Alignment = Element.ALIGN_CENTER;
                cell_fecha_fact.Border = PdfPCell.RIGHT_BORDER;
                cell_fecha_fact.AddElement(prgf_f_t);
                cell_fecha_fact.AddElement(prgf_f);
                techas_c.AddCell(cell_fecha_fact);

                PdfPCell cell_fecha_ven = new PdfPCell() { Border = 0 };
                Paragraph prgf_f_t2 = new Paragraph(string.Format("Fecha Ven", fontCustom));
                Paragraph prgf_f2 = new Paragraph(string.Format($"{DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"]).ToString("dd/MM/yyyy")}", fontCustom));
                prgf_f_t2.Alignment = Element.ALIGN_CENTER;
                prgf_f2.Alignment = Element.ALIGN_CENTER;
                cell_fecha_ven.Border = PdfPCell.RIGHT_BORDER;
                cell_fecha_ven.AddElement(prgf_f_t2);
                cell_fecha_ven.AddElement(prgf_f);
                techas_c.AddCell(cell_fecha_ven);

                PdfPCell numero_factura = new PdfPCell() { Border = 0 };
                Paragraph prgnota = new Paragraph(string.Format("NOTA CREDITO", fontCustom));
                Paragraph prgnota2 = new Paragraph(string.Format("" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceNum"], fontCustom));
                prgnota.Alignment = Element.ALIGN_CENTER;
                prgnota2.Alignment = Element.ALIGN_CENTER;
                numero_factura.AddElement(prgnota);
                numero_factura.AddElement(prgnota2);
                techas_c.AddCell(numero_factura);

                fehcas.AddElement(techas_c);

                PdfPTable nit = new PdfPTable(1);
                nit.WidthPercentage = 100;
                PdfPCell cell_nit = new PdfPCell(new Phrase("Nit: " + DsInvoiceAR.Tables["Customer"].Rows[0]["CustID"], fontCustom)) { Border = 0, };
                cell_nit.Border = PdfPCell.TOP_BORDER;
                nit.AddCell(cell_nit);
                fehcas.AddElement(nit);
                cliente.AddCell(fehcas);
                #endregion

                #region Body

                PdfPTable Body = new PdfPTable(new float[] { 0.1f, 0.4f, 0.1f, 0.18f, 0.03f, 0.18f, });
                Body.WidthPercentage = 100;

                PdfPCell codigo = new PdfPCell(new Phrase("CODIGO", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_CENTER, MinimumHeight = 15, };
                PdfPCell descripcion = new PdfPCell(new Phrase("DESCRIPCIÓN", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_CENTER };
                PdfPCell cantidad = new PdfPCell(new Phrase("UNIDAD", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_CENTER };
                PdfPCell vr_unitario = new PdfPCell(new Phrase("VR. UNITARIO", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_CENTER };
                PdfPCell iv = new PdfPCell(new Phrase("IV", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_CENTER };
                PdfPCell vr_totral = new PdfPCell(new Phrase("VR. TOTAL", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_CENTER };

                Body.AddCell(codigo);
                Body.AddCell(descripcion);
                Body.AddCell(cantidad);
                Body.AddCell(vr_unitario);
                Body.AddCell(iv);
                Body.AddCell(vr_totral);

                #endregion

                #region Tabla Unidades
                PdfPTable tableTituloUnidades = new PdfPTable(6);
                //Dimenciones.
                float[] DimencionUnidades = new float[6];
                DimencionUnidades[0] = 0.1f;//codigo
                DimencionUnidades[1] = 0.4f;//descripcion
                DimencionUnidades[2] = 0.1f;//cantidad
                DimencionUnidades[3] = 0.18f;//vr. unitario
                DimencionUnidades[4] = 0.03f;//iv
                DimencionUnidades[5] = 0.18f;//vr. total

                PdfPTable tableUnidades = new PdfPTable(6);
                tableUnidades.WidthPercentage = 100;
                tableUnidades.SetWidths(DimencionUnidades);

                foreach (DataRow InvoiceLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
                {
                    if (!AddUnidadesNOtaCreditoPolietilenosDelValle(InvoiceLine, ref tableUnidades, fontCustom, DsInvoiceAR))
                        return false;
                }

                iTextSharp.text.pdf.PdfPCell LineaFinal = new iTextSharp.text.pdf.PdfPCell();
                LineaFinal.Colspan = 6;
                LineaFinal.Padding = 3;
                LineaFinal.Border = 1;
                LineaFinal.BorderColorBottom = BaseColor.WHITE;
                LineaFinal.HorizontalAlignment = Element.ALIGN_CENTER;
                LineaFinal.VerticalAlignment = Element.ALIGN_TOP;
                tableUnidades.AddCell(LineaFinal);


                #endregion

                #region Footer

                PdfPTable Footer = new PdfPTable(new float[] { 0.2f, 0.5f, 0.3f, });
                Footer.WidthPercentage = 100;

                PdfPCell cell_fima = new PdfPCell(new Phrase("", fontCustom)) { HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP, MinimumHeight = 15, };
                PdfPTable sello = new PdfPTable(1);
                sello.WidthPercentage = 100;
                PdfPCell cell_sello = new PdfPCell() { MinimumHeight = 30, Border = 0, };
                sello.AddCell(cell_sello);
                cell_fima.AddElement(sello);
                PdfPTable firma_sello = new PdfPTable(1);
                firma_sello.WidthPercentage = 100;
                PdfPCell cell_firma_sello = new PdfPCell(new Phrase("FIRMA Y SELLO", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP };
                cell_firma_sello.Border = PdfPCell.TOP_BORDER;
                firma_sello.AddCell(cell_firma_sello);
                cell_fima.AddElement(firma_sello);

                Footer.AddCell(cell_fima);

                PdfPCell cell_F_pago = new PdfPCell(new Phrase("", fontCustom)) { HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP, MinimumHeight = 15, };
                Paragraph prgf_pago = new Paragraph(string.Format("Forma de pago: " + DsInvoiceAR.Tables["Company"].Rows[0]["Address1"], fontCustom));
                Paragraph prgubsv = new Paragraph(string.Format("UBVS: " + DsInvoiceAR.Tables["Company"].Rows[0]["Address1"], fontCustom));
                prgf_pago.Alignment = Element.ALIGN_LEFT;
                prgubsv.Alignment = Element.ALIGN_LEFT;
                cell_F_pago.AddElement(prgf_pago);
                cell_F_pago.AddElement(prgubsv);
                Footer.AddCell(cell_F_pago);

                PdfPCell cell_totales = new PdfPCell();

                PdfPTable subtotal = new PdfPTable(2);
                subtotal.WidthPercentage = 100;
                PdfPCell cell_sub_t = new PdfPCell(new Phrase("SUBTOTAL: ", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP };
                iTextSharp.text.pdf.PdfPCell cell_sub_v = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:C2}",
                    decimal.Parse(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DocTaxAmt"].ToString())), fontTitle2))
                { Border = 0, HorizontalAlignment = Element.ALIGN_RIGHT };
                subtotal.AddCell(cell_sub_t);
                subtotal.AddCell(cell_sub_v);
                cell_totales.AddElement(subtotal);

                PdfPTable descuento = new PdfPTable(2);
                descuento.WidthPercentage = 100;
                PdfPCell cell_des_t = new PdfPCell(new Phrase("DESCUENTO: ", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP };
                iTextSharp.text.pdf.PdfPCell cell_des_v = new iTextSharp.text.pdf.PdfPCell(new Phrase("$ " +
                    decimal.Parse((string)DsInvoiceAR.Tables["InvcDtl"].Rows[0]["DiscountPercent"]).ToString("N0"), fontCustom))
                { Border = 0, HorizontalAlignment = Element.ALIGN_RIGHT };
                descuento.AddCell(cell_des_t);
                descuento.AddCell(cell_des_v);
                cell_totales.AddElement(descuento);

                PdfPTable retencion = new PdfPTable(2);
                retencion.WidthPercentage = 100;
                PdfPCell cell_ret_t = new PdfPCell(new Phrase("RETNCION: ", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP };
                iTextSharp.text.pdf.PdfPCell cell_ret_v = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:C2}",
                    decimal.Parse(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubTotal"].ToString())), fontTitle2))
                { Border = 0, HorizontalAlignment = Element.ALIGN_RIGHT };
                retencion.AddCell(cell_ret_t);
                retencion.AddCell(cell_ret_v);
                cell_totales.AddElement(retencion);

                PdfPTable iva = new PdfPTable(2);
                iva.WidthPercentage = 100;
                PdfPCell cell_iva_t = new PdfPCell(new Phrase("IVA: ", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP };
                iTextSharp.text.pdf.PdfPCell cell_iva_v = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:C2}",
                    decimal.Parse(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DocWHTaxAmt"].ToString())), fontTitle2))
                { Border = 0, HorizontalAlignment = Element.ALIGN_RIGHT };
                iva.AddCell(cell_iva_t);
                iva.AddCell(cell_iva_v);
                cell_totales.AddElement(iva);

                PdfPTable toatal = new PdfPTable(2);
                toatal.WidthPercentage = 100;
                PdfPCell cell_toatal_t = new PdfPCell(new Phrase("TOTAL: ", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP };
                iTextSharp.text.pdf.PdfPCell _celTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:C2}",
                    decimal.Parse(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"].ToString())), fontTitleFactura))
                { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP };
                toatal.AddCell(cell_toatal_t);
                toatal.AddCell(cell_sub_v);
                cell_totales.AddElement(toatal);

                Footer.AddCell(cell_totales);

                #endregion

                #region Exit

                document.Add(Header);
                document.Add(cliente);
                document.Add(Body);
                document.Add(tableTituloUnidades);
                document.Add(tableUnidades);
                document.Add(Footer);

                //document.Add(divTextBancoCheque);
                /*PIE DE PAGINA*/
                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();
                RutaPdf = NomArchivo;
                return true;
                #endregion
            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.Message;
                return false;
            }
        }

        public bool NotaDebitolTomyPuertas(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {
            //NomArchivo = string.Empty;
            //string Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
            //string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            //string RutaImg = $@"{AppDomain.CurrentDomain.BaseDirectory}\LOGO_POLIETILENOS_DEL_VALLE.png";
            //string RutaMarcaAgua = $@"{AppDomain.CurrentDomain.BaseDirectory}\Logos ICONTEC.png";
            //NomArchivo = NombreInvoice + ".pdf";

            NomArchivo = string.Empty;
            string Ruta = HttpContext.Current.Server.MapPath("~/PDF/" + NIT + "/");
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            string RutaImg = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";
            NomArchivo = NombreInvoice + ".pdf";
            try
            {
                #region Head
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.
                Document document = new Document(iTextSharp.text.PageSize.LETTER);
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;

                // step 3: we open the document     
                document.Open();

                PdfDiv divEspacio = new PdfDiv();
                divEspacio.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio.Height = 10;
                divEspacio.Width = 130;

                PdfDiv divEspacio2 = new PdfDiv();
                divEspacio2.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio2.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio2.Height = 2;
                divEspacio2.Width = 130;

                //FUENTES----------------------------------------------------------------------------------------------------------
                iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle3 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 9, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle2 = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustom = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitleFactura = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                //FUENTES---------------------------------------------------------------------------------------------------------

                System.Drawing.Image logo = null;
                var requestLogo = WebRequest.Create(RutaImg);

                using (var responseLogo = requestLogo.GetResponse())
                using (var streamLogo = responseLogo.GetResponseStream())
                {
                    logo = Bitmap.FromStream(streamLogo);
                }

                System.Drawing.Image logo2 = null;
                var requestLogo2 = WebRequest.Create(RutaImg);

                using (var responseLogo2 = requestLogo2.GetResponse())
                using (var streamLogo2 = responseLogo2.GetResponseStream())
                {
                    logo2 = Bitmap.FromStream(streamLogo2);
                }
                #endregion

                #region Header

                PdfPTable Header = new PdfPTable(new float[] { 0.35f, 0.45f, 0.2f, });
                Header.WidthPercentage = 100;

                PdfPCell cell_logo = new PdfPCell() { Border = 0, };
                iTextSharp.text.Image ImgLogo2 = iTextSharp.text.Image.GetInstance(logo2, BaseColor.WHITE);
                ImgLogo2.ScaleAbsolute(170, 170);
                cell_logo.AddElement(ImgLogo2);
                Header.AddCell(cell_logo);

                PdfPCell cell_info = new PdfPCell(new Phrase("")) { Border = 0, };
                Paragraph prgTitle = new Paragraph(string.Format("Polietilenos del Valle S.A ", fontCustom));
                Paragraph prgTitle2 = new Paragraph(string.Format("{0}\n{1}\n \n{2}",
                   "Nit " + DsInvoiceAR.Tables["Company"].Rows[0]["Address1"].ToString(),
                   "Dirección " + DsInvoiceAR.Tables["Company"].Rows[0]["Address1"].ToString(),
                   "Autorretenedores , IVA regimen comun ", fontCustom))
                { };
                prgTitle.Alignment = Element.ALIGN_CENTER;
                prgTitle2.Alignment = Element.ALIGN_CENTER;
                cell_info.AddElement(prgTitle);
                cell_info.AddElement(prgTitle2);
                Header.AddCell(cell_info);

                PdfPCell cell_Qr = new PdfPCell() { HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_CENTER, Border = 0, };
                iTextSharp.text.Image ImgQR = iTextSharp.text.Image.GetInstance(QRInvoice, BaseColor.WHITE);
                ImgQR.ScaleAbsolute(80, 80);
                cell_Qr.AddElement(ImgQR);
                Header.AddCell(cell_Qr);

                PdfPTable cliente = new PdfPTable(2);
                cliente.WidthPercentage = 100;

                PdfPCell cell_info_clientes = new PdfPCell(new Phrase("")) { MinimumHeight = 50, };
                Paragraph prgTitle3 = new Paragraph(string.Format("SR. (ES): " + DsInvoiceAR.Tables["Customer"].Rows[0]["Name"], fontCustom));
                Paragraph prgTitle4 = new Paragraph(string.Format("{0}\n{1}\n{2}\n",
                  "DIR: " + DsInvoiceAR.Tables["Customer"].Rows[0]["Address1"].ToString(),
                  "TEL: " + DsInvoiceAR.Tables["Customer"].Rows[0]["PhoneNum"].ToString(),
                  "CIUDAD: " + DsInvoiceAR.Tables["Customer"].Rows[0]["City"].ToString()),
                    fontCustom);
                prgTitle.Alignment = Element.ALIGN_CENTER;
                prgTitle2.Alignment = Element.ALIGN_CENTER;
                cell_info_clientes.AddElement(prgTitle3);
                cell_info_clientes.AddElement(prgTitle4);
                cliente.AddCell(cell_info_clientes);

                PdfPCell fehcas = new PdfPCell();



                PdfPTable techas_c = new PdfPTable(3);
                techas_c.WidthPercentage = 100;

                PdfPCell cell_fecha_fact = new PdfPCell() { Border = 0, };
                Paragraph prgf_f_t = new Paragraph(string.Format("Fecha Fect", fontCustom));
                Paragraph prgf_f = new Paragraph(string.Format($"{DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).ToString("dd/MM/yyyy")}", fontCustom));
                prgf_f_t.Alignment = Element.ALIGN_CENTER;
                prgf_f.Alignment = Element.ALIGN_CENTER;
                cell_fecha_fact.Border = PdfPCell.RIGHT_BORDER;
                cell_fecha_fact.AddElement(prgf_f_t);
                cell_fecha_fact.AddElement(prgf_f);
                techas_c.AddCell(cell_fecha_fact);

                PdfPCell cell_fecha_ven = new PdfPCell() { Border = 0 };
                Paragraph prgf_f_t2 = new Paragraph(string.Format("Fecha Ven", fontCustom));
                Paragraph prgf_f2 = new Paragraph(string.Format($"{DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"]).ToString("dd/MM/yyyy")}", fontCustom));
                prgf_f_t2.Alignment = Element.ALIGN_CENTER;
                prgf_f2.Alignment = Element.ALIGN_CENTER;
                cell_fecha_ven.Border = PdfPCell.RIGHT_BORDER;
                cell_fecha_ven.AddElement(prgf_f_t2);
                cell_fecha_ven.AddElement(prgf_f);
                techas_c.AddCell(cell_fecha_ven);

                PdfPCell numero_factura = new PdfPCell() { Border = 0 };
                Paragraph prgnota = new Paragraph(string.Format("NOTA DEBITO", fontCustom));
                Paragraph prgnota2 = new Paragraph(string.Format("" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceNum"], fontCustom));
                prgnota.Alignment = Element.ALIGN_CENTER;
                prgnota2.Alignment = Element.ALIGN_CENTER;
                numero_factura.AddElement(prgnota);
                numero_factura.AddElement(prgnota2);
                techas_c.AddCell(numero_factura);

                fehcas.AddElement(techas_c);

                PdfPTable nit = new PdfPTable(1);
                nit.WidthPercentage = 100;
                PdfPCell cell_nit = new PdfPCell(new Phrase("Nit: " + DsInvoiceAR.Tables["Customer"].Rows[0]["CustID"], fontCustom)) { Border = 0, };
                cell_nit.Border = PdfPCell.TOP_BORDER;
                nit.AddCell(cell_nit);
                fehcas.AddElement(nit);
                cliente.AddCell(fehcas);
                #endregion

                #region Body

                PdfPTable Body = new PdfPTable(new float[] { 0.1f, 0.4f, 0.1f, 0.18f, 0.03f, 0.18f, });
                Body.WidthPercentage = 100;

                PdfPCell codigo = new PdfPCell(new Phrase("CODIGO", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_CENTER, MinimumHeight = 15, };
                PdfPCell descripcion = new PdfPCell(new Phrase("DESCRIPCIÓN", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_CENTER };
                PdfPCell cantidad = new PdfPCell(new Phrase("UNIDAD", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_CENTER };
                PdfPCell vr_unitario = new PdfPCell(new Phrase("VR. UNITARIO", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_CENTER };
                PdfPCell iv = new PdfPCell(new Phrase("IV", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_CENTER };
                PdfPCell vr_totral = new PdfPCell(new Phrase("VR. TOTAL", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_CENTER };

                Body.AddCell(codigo);
                Body.AddCell(descripcion);
                Body.AddCell(cantidad);
                Body.AddCell(vr_unitario);
                Body.AddCell(iv);
                Body.AddCell(vr_totral);

                #endregion

                #region Tabla Unidades
                PdfPTable tableTituloUnidades = new PdfPTable(6);
                //Dimenciones.
                float[] DimencionUnidades = new float[6];
                DimencionUnidades[0] = 0.1f;//codigo
                DimencionUnidades[1] = 0.4f;//descripcion
                DimencionUnidades[2] = 0.1f;//cantidad
                DimencionUnidades[3] = 0.18f;//vr. unitario
                DimencionUnidades[4] = 0.03f;//iv
                DimencionUnidades[5] = 0.18f;//vr. total

                PdfPTable tableUnidades = new PdfPTable(6);
                tableUnidades.WidthPercentage = 100;
                tableUnidades.SetWidths(DimencionUnidades);

                foreach (DataRow InvoiceLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
                {
                    if (!AddUnidadesNOtaCreditoPolietilenosDelValle(InvoiceLine, ref tableUnidades, fontCustom, DsInvoiceAR))
                        return false;
                }

                iTextSharp.text.pdf.PdfPCell LineaFinal = new iTextSharp.text.pdf.PdfPCell();
                LineaFinal.Colspan = 6;
                LineaFinal.Padding = 3;
                LineaFinal.Border = 1;
                LineaFinal.BorderColorBottom = BaseColor.WHITE;
                LineaFinal.HorizontalAlignment = Element.ALIGN_CENTER;
                LineaFinal.VerticalAlignment = Element.ALIGN_TOP;
                tableUnidades.AddCell(LineaFinal);


                #endregion

                #region Footer

                PdfPTable Footer = new PdfPTable(new float[] { 0.2f, 0.5f, 0.3f, });
                Footer.WidthPercentage = 100;

                PdfPCell cell_fima = new PdfPCell(new Phrase("", fontCustom)) { HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP, MinimumHeight = 15, };
                PdfPTable sello = new PdfPTable(1);
                sello.WidthPercentage = 100;
                PdfPCell cell_sello = new PdfPCell() { MinimumHeight = 30, Border = 0, };
                sello.AddCell(cell_sello);
                cell_fima.AddElement(sello);
                PdfPTable firma_sello = new PdfPTable(1);
                firma_sello.WidthPercentage = 100;
                PdfPCell cell_firma_sello = new PdfPCell(new Phrase("FIRMA Y SELLO", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP };
                cell_firma_sello.Border = PdfPCell.TOP_BORDER;
                firma_sello.AddCell(cell_firma_sello);
                cell_fima.AddElement(firma_sello);

                Footer.AddCell(cell_fima);

                PdfPCell cell_F_pago = new PdfPCell(new Phrase("", fontCustom)) { HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP, MinimumHeight = 15, };
                Paragraph prgf_pago = new Paragraph(string.Format("Forma de pago: " + DsInvoiceAR.Tables["Company"].Rows[0]["Address1"], fontCustom));
                Paragraph prgubsv = new Paragraph(string.Format("UBVS: " + DsInvoiceAR.Tables["Company"].Rows[0]["Address1"], fontCustom));
                prgf_pago.Alignment = Element.ALIGN_LEFT;
                prgubsv.Alignment = Element.ALIGN_LEFT;
                cell_F_pago.AddElement(prgf_pago);
                cell_F_pago.AddElement(prgubsv);
                Footer.AddCell(cell_F_pago);

                PdfPCell cell_totales = new PdfPCell();

                PdfPTable subtotal = new PdfPTable(2);
                subtotal.WidthPercentage = 100;
                PdfPCell cell_sub_t = new PdfPCell(new Phrase("SUBTOTAL: ", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP };
                iTextSharp.text.pdf.PdfPCell cell_sub_v = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:C2}",
                    decimal.Parse(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DocTaxAmt"].ToString())), fontTitle2))
                { Border = 0, HorizontalAlignment = Element.ALIGN_RIGHT };
                subtotal.AddCell(cell_sub_t);
                subtotal.AddCell(cell_sub_v);
                cell_totales.AddElement(subtotal);

                PdfPTable descuento = new PdfPTable(2);
                descuento.WidthPercentage = 100;
                PdfPCell cell_des_t = new PdfPCell(new Phrase("DESCUENTO: ", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP };
                iTextSharp.text.pdf.PdfPCell cell_des_v = new iTextSharp.text.pdf.PdfPCell(new Phrase("$ " +
                    decimal.Parse((string)DsInvoiceAR.Tables["InvcDtl"].Rows[0]["DiscountPercent"]).ToString("N0"), fontCustom))
                { Border = 0, HorizontalAlignment = Element.ALIGN_RIGHT };
                descuento.AddCell(cell_des_t);
                descuento.AddCell(cell_des_v);
                cell_totales.AddElement(descuento);

                PdfPTable retencion = new PdfPTable(2);
                retencion.WidthPercentage = 100;
                PdfPCell cell_ret_t = new PdfPCell(new Phrase("RETNCION: ", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP };
                iTextSharp.text.pdf.PdfPCell cell_ret_v = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:C2}",
                    decimal.Parse(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubTotal"].ToString())), fontTitle2))
                { Border = 0, HorizontalAlignment = Element.ALIGN_RIGHT };
                retencion.AddCell(cell_ret_t);
                retencion.AddCell(cell_ret_v);
                cell_totales.AddElement(retencion);

                PdfPTable iva = new PdfPTable(2);
                iva.WidthPercentage = 100;
                PdfPCell cell_iva_t = new PdfPCell(new Phrase("IVA: ", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP };
                iTextSharp.text.pdf.PdfPCell cell_iva_v = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:C2}",
                    decimal.Parse(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DocWHTaxAmt"].ToString())), fontTitle2))
                { Border = 0, HorizontalAlignment = Element.ALIGN_RIGHT };
                iva.AddCell(cell_iva_t);
                iva.AddCell(cell_iva_v);
                cell_totales.AddElement(iva);

                PdfPTable toatal = new PdfPTable(2);
                toatal.WidthPercentage = 100;
                PdfPCell cell_toatal_t = new PdfPCell(new Phrase("TOTAL: ", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP };
                iTextSharp.text.pdf.PdfPCell _celTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:C2}",
                    decimal.Parse(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"].ToString())), fontTitleFactura))
                { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP };
                toatal.AddCell(cell_toatal_t);
                toatal.AddCell(cell_sub_v);
                cell_totales.AddElement(toatal);

                Footer.AddCell(cell_totales);

                #endregion

                #region Exit

                document.Add(Header);
                document.Add(cliente);
                document.Add(Body);
                document.Add(tableTituloUnidades);
                document.Add(tableUnidades);
                document.Add(Footer);

                //document.Add(divTextBancoCheque);
                /*PIE DE PAGINA*/
                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();
                RutaPdf = NomArchivo;
                return true;
                #endregion
            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.Message;
                return false;
            }
        }

        #endregion

    }
}