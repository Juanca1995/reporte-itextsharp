﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//Referenciar y usar.
using System.Data;
using System.IO;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Windows.Forms;
using System.Web;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Net;
using System.Drawing;
using System.Globalization;
using static RulesServicesDIAN2.Adquiriente.pdfEstandarAR;

namespace RulesServicesDIAN2.Adquiriente
{
    public partial class pdfEstandarAR
    {

        #region Formatos Apoyos Industriales

        private bool AddUnidadesLaboralesMdellin(DataRow dataLine, ref PdfPTable table, iTextSharp.text.Font fontTitleFactura, DataSet dataSet)
        {
            try
            {
                //descripcion
                strError += "Add InvoiceLine";
                iTextSharp.text.pdf.PdfPCell celValL = new iTextSharp.text.pdf.PdfPCell(new Phrase((string)dataLine["LineDesc"], fontTitleFactura));
                celValL.Colspan = 1;
                celValL.Padding = 2;
                celValL.Border = 0;
                celValL.Border = PdfPCell.RIGHT_BORDER;
                //celTextL.BorderWidthRight = 1;
                //celValL.BorderWidthLeft = 1;
                celValL.HorizontalAlignment = Element.ALIGN_CENTER;
                celValL.VerticalAlignment = Element.ALIGN_CENTER;
                table.AddCell(celValL);
                strError += "InvoiceLine OK";

                //valor principal
                strError += "Add PartNum";
                iTextSharp.text.pdf.PdfPCell celValCodigo = new iTextSharp.text.pdf.PdfPCell(new Phrase("$ " +
                    decimal.Parse((string)dataLine["DocDiscount"]).ToString("N0"), fontTitleFactura));
                celValCodigo.Colspan = 1;
                celValCodigo.Padding = 2;
                celValCodigo.Border = PdfPCell.RIGHT_BORDER;
                //celValCodigo.BorderWidthLeft = 1;
                //celDescripcion.BorderColorBottom = BaseColor.WHITE;
                celValCodigo.HorizontalAlignment = Element.ALIGN_CENTER;
                celValCodigo.VerticalAlignment = Element.ALIGN_CENTER;
                table.AddCell(celValCodigo);
                strError += "Add PartNum OK";

                //valor total
                strError += "Add PartNum";
                iTextSharp.text.pdf.PdfPCell celValOC = new iTextSharp.text.pdf.PdfPCell(new Phrase("$ " +
                    decimal.Parse((string)dataLine["DocExtPrice"]).ToString("N0"), fontTitleFactura));
                celValOC.Colspan = 1;
                celValOC.Padding = 2;
                celValOC.Border = 0;
                //celValCodigo.BorderWidthLeft = 1;
                //celDescripcion.BorderColorBottom = BaseColor.WHITE;
                celValOC.HorizontalAlignment = Element.ALIGN_CENTER;
                celValOC.VerticalAlignment = Element.ALIGN_CENTER;
                table.AddCell(celValOC);
                return true;
            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.Message;
                return false;
            }
        }

        public bool FacturaVentasLaboralesMdellin(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {
            //NomArchivo = string.Empty;
            //string Ruta = HttpContext.Current.Server.MapPath("~/PDF/" + NIT + "/");
            //string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            //string RutaImg = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";
            //string Rutacertificado = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";
            //NomArchivo = NombreInvoice + ".pdf";

            NomArchivo = string.Empty;
            string Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            string RutaImg = $@"{AppDomain.CurrentDomain.BaseDirectory}\LaboralesMedellin.png";
            string Rutacertificado = $@"{AppDomain.CurrentDomain.BaseDirectory}\LOGO_PLASTICOS_AMBIENTALES.png";
            NomArchivo = NombreInvoice + ".pdf";
            try
            {
                #region Head
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.
                Document document = new Document(iTextSharp.text.PageSize.LETTER);
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;

                // step 3: we open the document     
                document.Open();

                PdfDiv divEspacio = new PdfDiv();
                divEspacio.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio.Height = 10;
                divEspacio.Width = 130;

                PdfDiv divEspacio2 = new PdfDiv();
                divEspacio2.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio2.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio2.Height = 2;
                divEspacio2.Width = 130;

                //FUENTES----------------------------------------------------------------------------------------------------------
                iTextSharp.text.Font titulo = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 10, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font titulofactura = FontFactory.GetFont(FontFactory.TIMES_BOLD, 10, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle1 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 10, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font titulosdetablas = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 12, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle2 = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustom = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustom2 = FontFactory.GetFont(FontFactory.HELVETICA, 9, iTextSharp.text.Font.NORMAL);
                //FUENTES---------------------------------------------------------------------------------------------------------
                ///objeto para el borde 
                RoundRectangle CelEventBorderRound = new RoundRectangle();


                System.Drawing.Image Logo;
                System.Drawing.Image LogoBanner = null;
                var request = WebRequest.Create(RutaImg);
                using (var response = request.GetResponse())
                using (var stream = response.GetResponseStream())
                {
                    Logo = Bitmap.FromStream(stream);
                }
                var requestBanner = WebRequest.Create(RutaImg);
                using (var responseBanner = requestBanner.GetResponse())
                using (var streamBanner = responseBanner.GetResponseStream())
                {
                    LogoBanner = Bitmap.FromStream(streamBanner);
                }


                System.Drawing.Image Logo2;
                System.Drawing.Image LogoBanner2 = null;
                var request2 = WebRequest.Create(Rutacertificado);
                using (var response2 = request2.GetResponse())
                using (var stream2 = response2.GetResponseStream())
                {
                    Logo2 = Bitmap.FromStream(stream2);
                }
                var requestBanner2 = WebRequest.Create(Rutacertificado);
                using (var responseBanner2 = requestBanner2.GetResponse())
                using (var streamBanner2 = responseBanner2.GetResponseStream())
                {
                    LogoBanner2 = Bitmap.FromStream(streamBanner2);
                }


                //Logos--------------------------------
                iTextSharp.text.Image LogoPdf3 = iTextSharp.text.Image.GetInstance(LogoBanner2, BaseColor.WHITE);
                LogoPdf3.ScaleAbsolute(90f, 90f);
                LogoPdf3.Border = 0;


                iTextSharp.text.Image LogoPdf2 = iTextSharp.text.Image.GetInstance(LogoBanner, BaseColor.WHITE);
                LogoPdf2.ScaleAbsolute(70, 70);
                LogoPdf2.Border = 0;

                iTextSharp.text.Image QRPdf = iTextSharp.text.Image.GetInstance(QRInvoice, BaseColor.WHITE);
                QRPdf.ScaleAbsolute(60, 60);
                QRPdf.Border = 0;


                PdfPTable espacio = new PdfPTable(1);
                espacio.WidthPercentage = 100;
                PdfPCell salto = new PdfPCell() { Border = 0, MinimumHeight = 50, };
                espacio.AddCell(salto);

                #endregion

                #region Header
                ///agregamos los datos del encabwezado 
                PdfPTable header = new PdfPTable(new float[] { 0.15f, 0.30f, 0.25f, 0.15f, 0.15f });
                header.WidthPercentage = 100;

                PdfPCell cell_logo = new PdfPCell() { Border = 0, };
                cell_logo.AddElement(LogoPdf2);

                PdfPCell cell_empresa = new PdfPCell() { Border = 0, };
                Paragraph prgTitle1 = new Paragraph(string.Format("" + DsInvoiceAR.Tables["Company"].Rows[0]["Name"].ToString())) { Alignment = Element.ALIGN_CENTER, };
                Paragraph prgTitle2 = new Paragraph(string.Format("NIt. " + DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"].ToString()), fontTitle) { Alignment = Element.ALIGN_CENTER, };
                Paragraph prgTitle3 = new Paragraph(string.Format("I.V.A REGIMEN COMUN"), fontTitle) { Alignment = Element.ALIGN_CENTER, };
                Paragraph prgTitle4 = new Paragraph(string.Format("NO SOMOS GRANDES CONTRIBUYENTES - NI AUTORETENEDORES"), fontTitle) { Alignment = Element.ALIGN_CENTER, };
                Paragraph prgTitle5 = new Paragraph(string.Format("LICENCIA DE FUNCIONAMIENTO DEL MINISTERIO DE TRABAJO No 00061 de nov. 4 de 1963"), fontTitle) { Alignment = Element.ALIGN_CENTER, };
                cell_empresa.AddElement(prgTitle1);
                cell_empresa.AddElement(prgTitle2);
                cell_empresa.AddElement(prgTitle3);
                cell_empresa.AddElement(prgTitle4);
                cell_empresa.AddElement(prgTitle5);

                PdfPCell sucursal = new PdfPCell() { Border = 0, };
                Paragraph prgTitle6 = new Paragraph(string.Format("Calle 25ª N.43B-33"), fontTitle) { Alignment = Element.ALIGN_CENTER, };
                Paragraph prgTitle7 = new Paragraph(string.Format("CONMUTADOR: 262 55 33 - FAX: 381 56 50"), fontTitle) { Alignment = Element.ALIGN_CENTER, };
                Paragraph prgTitle8 = new Paragraph(string.Format("APARTADO 53277 -MEDELLIN"), fontTitle) { Alignment = Element.ALIGN_CENTER, };
                Paragraph prgTitle9 = new Paragraph(string.Format("E-mail: laborales_fe@laborales.com.co -Pagina: www.laborales.com.co"), fontTitle) { Alignment = Element.ALIGN_CENTER, };
                sucursal.AddElement(prgTitle6);
                sucursal.AddElement(prgTitle7);
                sucursal.AddElement(prgTitle8);
                sucursal.AddElement(prgTitle9);

                PdfPCell cell_QR = new PdfPCell() { Border = 0, };
                cell_QR.AddElement(QRPdf);
                PdfPCell cell_factura = new PdfPCell(new Phrase("", fontTitle)) { Border = 0, };
                Paragraph prgTitle10 = new Paragraph(string.Format("FECTURA DE VENTA No."), fontTitle) { Alignment = Element.ALIGN_CENTER, };
                Paragraph prgTitle11 = new Paragraph(string.Format(DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"].ToString()), fontTitle) { Alignment = Element.ALIGN_CENTER, };
                cell_factura.AddElement(prgTitle10);
                cell_factura.AddElement(prgTitle11);


                header.AddCell(cell_logo);
                header.AddCell(cell_empresa);
                header.AddCell(sucursal);
                header.AddCell(cell_QR);
                header.AddCell(cell_factura);

                ///agregamos los datos de los clientes 
                PdfPTable datos_clientes = new PdfPTable(new float[] { 0.7f, 0.01f, 0.259f });
                datos_clientes.WidthPercentage = 100;

                PdfPCell señores = new PdfPCell(new Phrase("")) { CellEvent = CelEventBorderRound, Border = 0, };
                Paragraph prgcustom0 = new Paragraph(string.Format("SEÑORES: " + DsInvoiceAR.Tables["Customer"].Rows[0]["Name"].ToString()), fontCustom) { Alignment = Element.ALIGN_LEFT, };
                Paragraph prgcustom1 = new Paragraph(string.Format("NIT: " + DsInvoiceAR.Tables["Customer"].Rows[0]["CustID"].ToString()), fontCustom) { Alignment = Element.ALIGN_LEFT, };
                Paragraph prgcustom2 = new Paragraph(string.Format("DIRECCION: " + DsInvoiceAR.Tables["Customer"].Rows[0]["Address1"].ToString()), fontCustom) { Alignment = Element.ALIGN_LEFT, };
                Paragraph prgcustom3 = new Paragraph(string.Format("CIUDAD: " + DsInvoiceAR.Tables["Customer"].Rows[0]["City"].ToString()), fontCustom) { Alignment = Element.ALIGN_LEFT, };
                Paragraph prgcustom4 = new Paragraph(string.Format("CENTRO DE COSTOS: " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number01"].ToString()), fontCustom) { Alignment = Element.ALIGN_LEFT, };
                señores.AddElement(prgcustom0);
                señores.AddElement(prgcustom1);
                señores.AddElement(prgcustom2);
                señores.AddElement(prgcustom3);
                señores.AddElement(prgcustom4);

                PdfPCell medio = new PdfPCell(new Phrase("")) { Border = 0, };

                PdfPCell fechas = new PdfPCell() { CellEvent = CelEventBorderRound, Border = 0, };

                PdfPTable fecha = new PdfPTable(2);
                fecha.WidthPercentage = 100;
                PdfPCell f_factura = new PdfPCell(new Phrase("FECHA FACTURA", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_CENTER, };
                f_factura.Border = PdfPCell.RIGHT_BORDER;
                fecha.AddCell(f_factura);
                PdfPCell f_vencimiento = new PdfPCell(new Phrase("FECHA VENCIMIENTO", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_CENTER };
                fecha.AddCell(f_vencimiento);
                fechas.AddElement(fecha);

                PdfPTable formato_dividido = new PdfPTable(6);
                formato_dividido.WidthPercentage = 100;

                PdfPCell fd = new PdfPCell(new Phrase("DIA", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER };
                fd.Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER | PdfPCell.RIGHT_BORDER;
                PdfPCell fm = new PdfPCell(new Phrase("MES", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER };
                fm.Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER | PdfPCell.RIGHT_BORDER;
                PdfPCell fa = new PdfPCell(new Phrase("AÑO", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER };
                fa.Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER | PdfPCell.RIGHT_BORDER;
                PdfPCell vd = new PdfPCell(new Phrase("DIA", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER };
                vd.Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER | PdfPCell.RIGHT_BORDER;
                PdfPCell vm = new PdfPCell(new Phrase("MES", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER };
                vm.Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER | PdfPCell.RIGHT_BORDER;
                PdfPCell va = new PdfPCell(new Phrase("AÑO", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER };
                va.Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER;

                formato_dividido.AddCell(fd);
                formato_dividido.AddCell(fm);
                formato_dividido.AddCell(fa);
                formato_dividido.AddCell(vd);
                formato_dividido.AddCell(vm);
                formato_dividido.AddCell(va);

                fechas.AddElement(formato_dividido);


                PdfPTable formato_divididoV = new PdfPTable(6);
                formato_divididoV.WidthPercentage = 100;

                PdfPCell fdV = new PdfPCell(new Phrase(DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).ToString("dd"), fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER };
                fdV.Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER | PdfPCell.RIGHT_BORDER;
                PdfPCell fmV = new PdfPCell(new Phrase(DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).ToString("MM"), fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER };
                fmV.Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER | PdfPCell.RIGHT_BORDER;
                PdfPCell faV = new PdfPCell(new Phrase(DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).ToString("yyyy"), fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER };
                faV.Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER | PdfPCell.RIGHT_BORDER;
                PdfPCell vdV = new PdfPCell(new Phrase(DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"]).ToString("dd"), fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER };
                vdV.Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER | PdfPCell.RIGHT_BORDER;
                PdfPCell vmV = new PdfPCell(new Phrase(DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"]).ToString("MM"), fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER };
                vmV.Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER | PdfPCell.RIGHT_BORDER;
                PdfPCell vaV = new PdfPCell(new Phrase(DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"]).ToString("yyyy"), fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER };
                vaV.Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER;

                formato_divididoV.AddCell(fdV);
                formato_divididoV.AddCell(fmV);
                formato_divididoV.AddCell(faV);
                formato_divididoV.AddCell(vdV);
                formato_divididoV.AddCell(vmV);
                formato_divididoV.AddCell(vaV);

                fechas.AddElement(formato_divididoV);

                PdfPTable formato_divididoP = new PdfPTable(1);
                formato_divididoP.WidthPercentage = 100;
                PdfPCell P = new PdfPCell(new Phrase("PERIODO: " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar01"], fontCustom)) { HorizontalAlignment = Element.ALIGN_LEFT, Border = 0, };
                formato_divididoP.AddCell(P);
                fechas.AddElement(formato_divididoP);

                datos_clientes.AddCell(señores);
                datos_clientes.AddCell(medio);
                datos_clientes.AddCell(fechas);

                //agregamos la primera leyenda 
                PdfPTable leyenda1 = new PdfPTable(1);
                leyenda1.WidthPercentage = 100;                                                             /*DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar01"], */
                PdfPCell cell_leyenda1 = new PdfPCell(new Phrase("" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["Character02"], fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, };
                leyenda1.AddCell(cell_leyenda1);

                #endregion

                #region Body

                //esta tabla cubre las regiiones Body y unidades contiene toda la tabla de detalles 
                PdfPTable unidades_detalles = new PdfPTable(1);
                unidades_detalles.WidthPercentage = 100;
                PdfPCell u_d = new PdfPCell() { Border = 0, CellEvent = CelEventBorderRound, };

                //agregamos los titulos de la tabla de detalles 
                PdfPTable body = new PdfPTable(new float[] { 0.7f, 0.15f, 0.15f, });
                body.WidthPercentage = 100;
                PdfPCell descripcion = new PdfPCell(new Phrase("DESCRIPCIÓN", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_CENTER, };
                descripcion.Border = PdfPCell.RIGHT_BORDER | PdfPCell.BOTTOM_BORDER;
                body.AddCell(descripcion);
                PdfPCell v_principal = new PdfPCell(new Phrase("VALOR DESCUENTO", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_CENTER, };
                v_principal.Border = PdfPCell.RIGHT_BORDER | PdfPCell.BOTTOM_BORDER;
                body.AddCell(v_principal);
                PdfPCell V_total = new PdfPCell(new Phrase("VALOR TOTAL", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_CENTER, };
                V_total.Border = PdfPCell.BOTTOM_BORDER;
                body.AddCell(V_total);
                u_d.AddElement(body);

                #endregion

                #region Unidades

                PdfPTable tableUnidades = new PdfPTable(new float[] { 0.7f, 0.15f, 0.15f, });
                tableUnidades.WidthPercentage = 100;

                foreach (DataRow InvoiceLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
                {
                    if (!AddUnidadesLaboralesMdellin(InvoiceLine, ref tableUnidades, fontCustom, DsInvoiceAR))
                        return false;
                }

                iTextSharp.text.pdf.PdfPCell LineaFinal = new iTextSharp.text.pdf.PdfPCell();
                LineaFinal.Colspan = 3;
                LineaFinal.Padding = 3;
                LineaFinal.Border = 1;
                LineaFinal.BorderColorBottom = BaseColor.WHITE;
                LineaFinal.HorizontalAlignment = Element.ALIGN_CENTER;
                LineaFinal.VerticalAlignment = Element.ALIGN_TOP;
                tableUnidades.AddCell(LineaFinal);
                u_d.AddElement(tableUnidades);


                //agregamos los lo valores debajo de los de lles
                PdfPTable fin_detalle = new PdfPTable(new float[] { 0.7f, 0.3f, });
                fin_detalle.WidthPercentage = 100;
                PdfPCell cell1 = new PdfPCell() { Border = 0, Padding = 0, };

                PdfPTable valorneletras = new PdfPTable(1);
                valorneletras.WidthPercentage = 100;

                PdfPCell cell_valorneletras = new PdfPCell(new Phrase("\nVALOR EN LETRAS:  " + string.Format("{0}",
                Nroenletras(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"].ToString())), fontCustom))
                { Border = 0, MinimumHeight = 25, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP };
                cell_valorneletras.Border = PdfPCell.TOP_BORDER;
                valorneletras.AddCell(cell_valorneletras);
                cell1.AddElement(valorneletras);
                
                PdfPTable observaciones = new PdfPTable(1);
                observaciones.WidthPercentage = 100;
                PdfPCell cell_obsevaciones = new PdfPCell(new Phrase("\n\nOBSERVACIÓNES:   " + 
                    DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceComment"], fontCustom))
                { Border = 0, MinimumHeight = 35, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP };
                cell_obsevaciones.Border = PdfPCell.TOP_BORDER;
                observaciones.AddCell(cell_obsevaciones);
                cell1.AddElement(observaciones);

                fin_detalle.AddCell(cell1);

                PdfPCell cell2 = new PdfPCell() { Border = 0, Padding = 0 };

                PdfPTable iva = new PdfPTable(2);
                iva.WidthPercentage = 100;
                PdfPCell cell_iva = new PdfPCell(new Phrase("IVA", fontCustom)) { Border = 0, MinimumHeight = 15 };
                cell_iva.Border = PdfPCell.TOP_BORDER | PdfPCell.LEFT_BORDER;
                PdfPCell cell_ivaV = new PdfPCell(new Phrase("$ "+
                    decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DocTaxAmt"]).ToString("N0"),fontCustom))
                { Border = 0, HorizontalAlignment = Element.ALIGN_RIGHT };
                cell_ivaV.Border = PdfPCell.TOP_BORDER | PdfPCell.LEFT_BORDER;
                iva.AddCell(cell_iva);
                iva.AddCell(cell_ivaV);
                cell2.AddElement(iva);

                PdfPTable retencion = new PdfPTable(2);
                retencion.WidthPercentage = 100;
                PdfPCell cell_retencion = new PdfPCell(new Phrase("RETENCION DE LA IVA ", fontCustom)) { MinimumHeight = 15 };
                cell_retencion.Border = PdfPCell.TOP_BORDER | PdfPCell.LEFT_BORDER;
                PdfPCell cell_retencionV = new PdfPCell(new Phrase(GetValImpuestoByID("0B", DsInvoiceAR).ToString("C0"), fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_RIGHT };
                cell_retencionV.Border = PdfPCell.TOP_BORDER | PdfPCell.LEFT_BORDER;
                retencion.AddCell(cell_retencion);
                retencion.AddCell(cell_retencionV);
                cell2.AddElement(retencion);

                PdfPTable retencionS = new PdfPTable(2);
                retencionS.WidthPercentage = 100;
                PdfPCell cell_retencionS = new PdfPCell(new Phrase("RETENCION EN LA FUENTE SERVICIO ", fontCustom)) { MinimumHeight = 15 };
                cell_retencionS.Border = PdfPCell.TOP_BORDER | PdfPCell.LEFT_BORDER;
                PdfPCell cell_retencionSV = new PdfPCell(new Phrase(decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DocWHTaxAmt"]).ToString("N0"), fontCustom))
                { Border = 0, HorizontalAlignment = Element.ALIGN_RIGHT };
                cell_retencionSV.Border = PdfPCell.TOP_BORDER | PdfPCell.LEFT_BORDER;
                retencionS.AddCell(cell_retencionS);
                retencionS.AddCell(cell_retencionSV);
                cell2.AddElement(retencionS);

                PdfPTable total = new PdfPTable(2);
                total.WidthPercentage = 100;
                PdfPCell cell_total = new PdfPCell(new Phrase("TOTAL A PAGAR", fontCustom)) { MinimumHeight = 14 };
                cell_total.Border = PdfPCell.TOP_BORDER | PdfPCell.LEFT_BORDER;
                PdfPCell cell_totalV = new PdfPCell(new Phrase("$ " +
                    decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"]).ToString("N0"),
                    fontCustom))
                { Border = 0, HorizontalAlignment = Element.ALIGN_RIGHT };
                cell_totalV.Border = PdfPCell.TOP_BORDER | PdfPCell.LEFT_BORDER;
                total.AddCell(cell_total);
                total.AddCell(cell_totalV);
                cell2.AddElement(total);

                fin_detalle.AddCell(cell2);

                u_d.AddElement(fin_detalle);
                unidades_detalles.AddCell(u_d);

                #endregion

                #region Footer

                PdfPTable footer = new PdfPTable(new float[] { 0.4f, 0.01F, 0.25f, 0.01F, 0.25f });
                footer.WidthPercentage = 100;

                PdfPCell beneficiario = new PdfPCell(new Phrase("BENEFICIARIO", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, };
                PdfPCell espacio1 = new PdfPCell(new Phrase("")) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, };
                PdfPCell emisor = new PdfPCell(new Phrase("EMISOR", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, };
                PdfPCell espacio2 = new PdfPCell(new Phrase("")) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, };
                PdfPCell leyenda2 = new PdfPCell(new Phrase("")) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, };

                footer.AddCell(beneficiario);
                footer.AddCell(espacio1);
                footer.AddCell(emisor);
                footer.AddCell(espacio2);
                footer.AddCell(leyenda2);


                PdfPTable footer_text = new PdfPTable(new float[] { 0.4f, 0.01F, 0.25f, 0.01F, 0.25f });
                footer_text.WidthPercentage = 100;

                PdfPCell beneficiariot = new PdfPCell(new Phrase("NOMBRE DE QUIE RECIBE______________________________________________________________" +
                                                                "C.C._________________________________________________________________" +
                                                                "FECHA DE RECIBO______________________________________________________________" +
                                                                "FIRMA Y SELLO________________________________________________________",
                    fontCustom))
                { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP, CellEvent = CelEventBorderRound, MinimumHeight = 50, };
                PdfPCell espacio1t = new PdfPCell(new Phrase("")) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, };
                PdfPCell emisort = new PdfPCell(new Phrase("__________________________________________SELLO Y FIRMA AUTORIZADA", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_BOTTOM, CellEvent = CelEventBorderRound, };
                PdfPCell espacio2t = new PdfPCell(new Phrase("")) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, };
                PdfPCell leyenda2t = new PdfPCell(new Phrase("Esta factura puede ser cancelada con cheque cruzado a nombre de laborales medellin S.A Si cancela por trasferencia eletronica o consignacion directa, por favor informar al fax. 3815650 ó al e-mail cartera@laborales.com.co. SI PASADOS 10 DIAS NO SE HAN REALIZADO RECLAMOS, SE ENTENDE COMO ACEPTADA ESTA FACTURA ", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP, CellEvent = CelEventBorderRound, };

                footer_text.AddCell(beneficiariot);
                footer_text.AddCell(espacio1t);
                footer_text.AddCell(emisort);
                footer_text.AddCell(espacio2t);
                footer_text.AddCell(leyenda2t);


                //agregamos la primera leyenda 3
                PdfPTable leyenda3 = new PdfPTable(2);
                leyenda3.WidthPercentage = 100;
                PdfPCell cell_leyenda3 = new PdfPCell(new Phrase("Acepto el contenido de la siguiente factura y las condiciones de pago ", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, };
                PdfPCell cell_leyenda3_ = new PdfPCell(new Phrase("CUFE:" + CUFE, fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_RIGHT, };
                leyenda3.AddCell(cell_leyenda3);
                leyenda3.AddCell(cell_leyenda3_);


                //agregamos la primera leyenda 4
                PdfPTable leyenda4 = new PdfPTable(1);
                leyenda4.WidthPercentage = 100;
                PdfPCell cell_leyenda4 = new PdfPCell(new Phrase(/*"/*Original-Vendedor-  1ª Copia -Cliente- 2ª Copia -Contabilidad-"*/)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, };
                leyenda4.AddCell(cell_leyenda4);


                #endregion

                #region Exti

                document.Add(header);
                document.Add(datos_clientes);
                document.Add(leyenda1);
                document.Add(unidades_detalles);
                document.Add(footer);
                document.Add(footer_text);
                document.Add(leyenda3);
                document.Add(leyenda4);

                //document.Add(divTextBancoCheque);
                /*PIE DE PAGINA*/
                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();
                RutaPdf = NomArchivo;
                return true;
                #endregion
            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.Message;
                return false;
            }
        }

        public bool NotacreditoLaboralesMdellin(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {
            //NomArchivo = string.Empty;
            //string Ruta = HttpContext.Current.Server.MapPath("~/PDF/" + NIT + "/");
            //string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            //string RutaImg = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";
            //string Rutacertificado = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";
            //NomArchivo = NombreInvoice + ".pdf";

            NomArchivo = string.Empty;
            string Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            string RutaImg = $@"{AppDomain.CurrentDomain.BaseDirectory}\LaboralesMedellin.png";
            string Rutacertificado = $@"{AppDomain.CurrentDomain.BaseDirectory}\LOGO_PLASTICOS_AMBIENTALES.png";
            NomArchivo = NombreInvoice + ".pdf";
            try
            {
                #region Head
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.
                Document document = new Document(iTextSharp.text.PageSize.LETTER);
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;

                // step 3: we open the document     
                document.Open();

                PdfDiv divEspacio = new PdfDiv();
                divEspacio.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio.Height = 10;
                divEspacio.Width = 130;

                PdfDiv divEspacio2 = new PdfDiv();
                divEspacio2.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio2.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio2.Height = 2;
                divEspacio2.Width = 130;

                //FUENTES----------------------------------------------------------------------------------------------------------
                iTextSharp.text.Font titulo = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 10, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font titulofactura = FontFactory.GetFont(FontFactory.TIMES_BOLD, 10, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle1 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 10, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font titulosdetablas = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 12, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle2 = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustom = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustom2 = FontFactory.GetFont(FontFactory.HELVETICA, 9, iTextSharp.text.Font.NORMAL);
                //FUENTES---------------------------------------------------------------------------------------------------------
                ///objeto para el borde 
                RoundRectangle CelEventBorderRound = new RoundRectangle();


                System.Drawing.Image Logo;
                System.Drawing.Image LogoBanner = null;
                var request = WebRequest.Create(RutaImg);
                using (var response = request.GetResponse())
                using (var stream = response.GetResponseStream())
                {
                    Logo = Bitmap.FromStream(stream);
                }
                var requestBanner = WebRequest.Create(RutaImg);
                using (var responseBanner = requestBanner.GetResponse())
                using (var streamBanner = responseBanner.GetResponseStream())
                {
                    LogoBanner = Bitmap.FromStream(streamBanner);
                }


                System.Drawing.Image Logo2;
                System.Drawing.Image LogoBanner2 = null;
                var request2 = WebRequest.Create(Rutacertificado);
                using (var response2 = request2.GetResponse())
                using (var stream2 = response2.GetResponseStream())
                {
                    Logo2 = Bitmap.FromStream(stream2);
                }
                var requestBanner2 = WebRequest.Create(Rutacertificado);
                using (var responseBanner2 = requestBanner2.GetResponse())
                using (var streamBanner2 = responseBanner2.GetResponseStream())
                {
                    LogoBanner2 = Bitmap.FromStream(streamBanner2);
                }


                //Logos--------------------------------
                iTextSharp.text.Image LogoPdf3 = iTextSharp.text.Image.GetInstance(LogoBanner2, BaseColor.WHITE);
                LogoPdf3.ScaleAbsolute(90f, 90f);
                LogoPdf3.Border = 0;


                iTextSharp.text.Image LogoPdf2 = iTextSharp.text.Image.GetInstance(LogoBanner, BaseColor.WHITE);
                LogoPdf2.ScaleAbsolute(70, 70);
                LogoPdf2.Border = 0;

                iTextSharp.text.Image QRPdf = iTextSharp.text.Image.GetInstance(QRInvoice, BaseColor.WHITE);
                QRPdf.ScaleAbsolute(60, 60);
                QRPdf.Border = 0;


                PdfPTable espacio = new PdfPTable(1);
                espacio.WidthPercentage = 100;
                PdfPCell salto = new PdfPCell() { Border = 0, MinimumHeight = 50, };
                espacio.AddCell(salto);

                #endregion

                #region Header
                ///agregamos los datos del encabwezado 
                PdfPTable header = new PdfPTable(new float[] { 0.15f, 0.30f, 0.25f, 0.15f, 0.15f });
                header.WidthPercentage = 100;

                PdfPCell cell_logo = new PdfPCell() { Border = 0, };
                cell_logo.AddElement(LogoPdf2);

                PdfPCell cell_empresa = new PdfPCell() { Border = 0, };
                Paragraph prgTitle1 = new Paragraph(string.Format("" + DsInvoiceAR.Tables["Company"].Rows[0]["Name"].ToString())) { Alignment = Element.ALIGN_CENTER, };
                Paragraph prgTitle2 = new Paragraph(string.Format("NIt. " + DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"].ToString()), fontTitle) { Alignment = Element.ALIGN_CENTER, };
                Paragraph prgTitle3 = new Paragraph(string.Format("I.V.A REGIMEN COMUN"), fontTitle) { Alignment = Element.ALIGN_CENTER, };
                Paragraph prgTitle4 = new Paragraph(string.Format("NO SOMOS GRANDES CONTRIBUYENTES - NI AUTORETENEDORES"), fontTitle) { Alignment = Element.ALIGN_CENTER, };
                Paragraph prgTitle5 = new Paragraph(string.Format("LICENCIA DE FUNCIONAMIENTO DEL MINISTERIO DE TRABAJO No 00061 de nov. 4 de 1963"), fontTitle) { Alignment = Element.ALIGN_CENTER, };
                cell_empresa.AddElement(prgTitle1);
                cell_empresa.AddElement(prgTitle2);
                cell_empresa.AddElement(prgTitle3);
                cell_empresa.AddElement(prgTitle4);
                cell_empresa.AddElement(prgTitle5);

                PdfPCell sucursal = new PdfPCell() { Border = 0, };
                Paragraph prgTitle6 = new Paragraph(string.Format("Calle 25ª N.43B-33"), fontTitle) { Alignment = Element.ALIGN_CENTER, };
                Paragraph prgTitle7 = new Paragraph(string.Format("CONMUTADOR: 262 55 33 - FAX: 381 56 50"), fontTitle) { Alignment = Element.ALIGN_CENTER, };
                Paragraph prgTitle8 = new Paragraph(string.Format("APARTADO 53277 -MEDELLIN"), fontTitle) { Alignment = Element.ALIGN_CENTER, };
                Paragraph prgTitle9 = new Paragraph(string.Format("E-mail: apoyos_fe@laborales.com.co -Pagina: www.laborales.com.co"), fontTitle) { Alignment = Element.ALIGN_CENTER, };
                sucursal.AddElement(prgTitle6);
                sucursal.AddElement(prgTitle7);
                sucursal.AddElement(prgTitle8);
                sucursal.AddElement(prgTitle9);

                PdfPCell cell_QR = new PdfPCell() { Border = 0, };
                cell_QR.AddElement(QRPdf);
                PdfPCell cell_factura = new PdfPCell(new Phrase("", fontTitle)) { Border = 0, };
                Paragraph prgTitle10 = new Paragraph(string.Format("FECTURA DE VENTA No."), fontTitle) { Alignment = Element.ALIGN_CENTER, };
                Paragraph prgTitle11 = new Paragraph(string.Format(DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"].ToString()), fontTitle) { Alignment = Element.ALIGN_CENTER, };
                cell_factura.AddElement(prgTitle10);
                cell_factura.AddElement(prgTitle11);


                header.AddCell(cell_logo);
                header.AddCell(cell_empresa);
                header.AddCell(sucursal);
                header.AddCell(cell_QR);
                header.AddCell(cell_factura);

                ///agregamos los datos de los clientes 
                PdfPTable datos_clientes = new PdfPTable(new float[] { 0.7f, 0.01f, 0.259f });
                datos_clientes.WidthPercentage = 100;

                PdfPCell señores = new PdfPCell(new Phrase("")) { CellEvent = CelEventBorderRound, Border = 0, };
                Paragraph prgcustom0 = new Paragraph(string.Format(" " + DsInvoiceAR.Tables["Customer"].Rows[0]["Name"].ToString()), fontCustom) { Alignment = Element.ALIGN_LEFT, };
                Paragraph prgcustom1 = new Paragraph(string.Format("NIT: " + DsInvoiceAR.Tables["Customer"].Rows[0]["CustID"].ToString()), fontCustom) { Alignment = Element.ALIGN_LEFT, };
                Paragraph prgcustom2 = new Paragraph(string.Format("DIRECCION: " + DsInvoiceAR.Tables["Customer"].Rows[0]["Address1"].ToString()), fontCustom) { Alignment = Element.ALIGN_LEFT, };
                Paragraph prgcustom3 = new Paragraph(string.Format("CIUDAD: " + DsInvoiceAR.Tables["Customer"].Rows[0]["City"].ToString()), fontCustom) { Alignment = Element.ALIGN_LEFT, };
                Paragraph prgcustom4 = new Paragraph(string.Format("CENTRO DE COSTOS: " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number01"].ToString()), fontCustom) { Alignment = Element.ALIGN_LEFT, };
                señores.AddElement(prgcustom0);
                señores.AddElement(prgcustom1);
                señores.AddElement(prgcustom2);
                señores.AddElement(prgcustom3);
                señores.AddElement(prgcustom4);

                PdfPCell medio = new PdfPCell(new Phrase("")) { Border = 0, };

                PdfPCell fechas = new PdfPCell() { CellEvent = CelEventBorderRound, Border = 0, };

                PdfPTable fecha = new PdfPTable(2);
                fecha.WidthPercentage = 100;
                PdfPCell f_factura = new PdfPCell(new Phrase("FECHA FACTURA", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_CENTER, };
                f_factura.Border = PdfPCell.RIGHT_BORDER;
                fecha.AddCell(f_factura);
                PdfPCell f_vencimiento = new PdfPCell(new Phrase("FECHA VENCIMIENTO", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_CENTER };
                fecha.AddCell(f_vencimiento);
                fechas.AddElement(fecha);

                PdfPTable formato_dividido = new PdfPTable(6);
                formato_dividido.WidthPercentage = 100;

                PdfPCell fd = new PdfPCell(new Phrase("DIA", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER };
                fd.Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER | PdfPCell.RIGHT_BORDER;
                PdfPCell fm = new PdfPCell(new Phrase("MES", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER };
                fm.Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER | PdfPCell.RIGHT_BORDER;
                PdfPCell fa = new PdfPCell(new Phrase("AÑO", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER };
                fa.Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER | PdfPCell.RIGHT_BORDER;
                PdfPCell vd = new PdfPCell(new Phrase("DIA", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER };
                vd.Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER | PdfPCell.RIGHT_BORDER;
                PdfPCell vm = new PdfPCell(new Phrase("MES", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER };
                vm.Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER | PdfPCell.RIGHT_BORDER;
                PdfPCell va = new PdfPCell(new Phrase("AÑO", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER };
                va.Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER;

                formato_dividido.AddCell(fd);
                formato_dividido.AddCell(fm);
                formato_dividido.AddCell(fa);
                formato_dividido.AddCell(vd);
                formato_dividido.AddCell(vm);
                formato_dividido.AddCell(va);

                fechas.AddElement(formato_dividido);


                PdfPTable formato_divididoV = new PdfPTable(6);
                formato_divididoV.WidthPercentage = 100;

                PdfPCell fdV = new PdfPCell(new Phrase(DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).ToString("dd"), fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER };
                fdV.Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER | PdfPCell.RIGHT_BORDER;
                PdfPCell fmV = new PdfPCell(new Phrase(DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).ToString("MM"), fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER };
                fmV.Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER | PdfPCell.RIGHT_BORDER;
                PdfPCell faV = new PdfPCell(new Phrase(DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).ToString("yyyy"), fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER };
                faV.Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER | PdfPCell.RIGHT_BORDER;
                PdfPCell vdV = new PdfPCell(new Phrase(DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"]).ToString("dd"), fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER };
                vdV.Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER | PdfPCell.RIGHT_BORDER;
                PdfPCell vmV = new PdfPCell(new Phrase(DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"]).ToString("MM"), fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER };
                vmV.Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER | PdfPCell.RIGHT_BORDER;
                PdfPCell vaV = new PdfPCell(new Phrase(DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"]).ToString("yyyy"), fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER };
                vaV.Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER;

                formato_divididoV.AddCell(fdV);
                formato_divididoV.AddCell(fmV);
                formato_divididoV.AddCell(faV);
                formato_divididoV.AddCell(vdV);
                formato_divididoV.AddCell(vmV);
                formato_divididoV.AddCell(vaV);

                fechas.AddElement(formato_divididoV);

                PdfPTable formato_divididoP = new PdfPTable(1);
                formato_divididoP.WidthPercentage = 100;
                PdfPCell P = new PdfPCell(new Phrase("PERIODO: " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar01"], fontCustom)) { HorizontalAlignment = Element.ALIGN_LEFT, Border = 0, };
                formato_divididoP.AddCell(P);
                fechas.AddElement(formato_divididoP);

                datos_clientes.AddCell(señores);
                datos_clientes.AddCell(medio);
                datos_clientes.AddCell(fechas);

                //agregamos la primera leyenda 
                PdfPTable leyenda1 = new PdfPTable(1);
                leyenda1.WidthPercentage = 100;                                                             /*DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar01"], */
                PdfPCell cell_leyenda1 = new PdfPCell(new Phrase("" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["Character02"], fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, };
                leyenda1.AddCell(cell_leyenda1);

                #endregion

                #region Body

                //esta tabla cubre las regiiones Body y unidades contiene toda la tabla de detalles 
                PdfPTable unidades_detalles = new PdfPTable(1);
                unidades_detalles.WidthPercentage = 100;
                PdfPCell u_d = new PdfPCell() { Border = 0, CellEvent = CelEventBorderRound, };

                //agregamos los titulos de la tabla de detalles 
                PdfPTable body = new PdfPTable(new float[] { 0.7f, 0.15f, 0.15f, });
                body.WidthPercentage = 100;
                PdfPCell descripcion = new PdfPCell(new Phrase("DESCRIPCIÓN", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_CENTER, };
                descripcion.Border = PdfPCell.RIGHT_BORDER | PdfPCell.BOTTOM_BORDER;
                body.AddCell(descripcion);
                PdfPCell v_principal = new PdfPCell(new Phrase("VALOR PRINCIPAL", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_CENTER, };
                v_principal.Border = PdfPCell.RIGHT_BORDER | PdfPCell.BOTTOM_BORDER;
                body.AddCell(v_principal);
                PdfPCell V_total = new PdfPCell(new Phrase("VALOR TOTAL", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_CENTER, };
                V_total.Border = PdfPCell.BOTTOM_BORDER;
                body.AddCell(V_total);
                u_d.AddElement(body);

                #endregion

                #region Unidades

                PdfPTable tableUnidades = new PdfPTable(new float[] { 0.7f, 0.15f, 0.15f, });
                tableUnidades.WidthPercentage = 100;

                foreach (DataRow InvoiceLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
                {
                    if (!AddUnidadesLaboralesMdellin(InvoiceLine, ref tableUnidades, fontCustom, DsInvoiceAR))
                        return false;
                }

                iTextSharp.text.pdf.PdfPCell LineaFinal = new iTextSharp.text.pdf.PdfPCell();
                LineaFinal.Colspan = 3;
                LineaFinal.Padding = 3;
                LineaFinal.Border = 1;
                LineaFinal.BorderColorBottom = BaseColor.WHITE;
                LineaFinal.HorizontalAlignment = Element.ALIGN_CENTER;
                LineaFinal.VerticalAlignment = Element.ALIGN_TOP;
                tableUnidades.AddCell(LineaFinal);
                u_d.AddElement(tableUnidades);


                //agregamos los lo valores debajo de los de lles
                PdfPTable fin_detalle = new PdfPTable(new float[] { 0.7f, 0.3f, });
                fin_detalle.WidthPercentage = 100;
                PdfPCell cell1 = new PdfPCell() { Border = 0, Padding = 0, };

                PdfPTable observaciones = new PdfPTable(1);
                observaciones.WidthPercentage = 100;
                PdfPCell cell_obsevaciones = new PdfPCell(new Phrase("OBSERVACIÓNES" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceComment"], fontCustom)) { Border = 0, MinimumHeight = 35, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP };
                cell_obsevaciones.Border = PdfPCell.TOP_BORDER;
                observaciones.AddCell(cell_obsevaciones);
                cell1.AddElement(observaciones);

                PdfPTable valorneletras = new PdfPTable(1);
                valorneletras.WidthPercentage = 100;
                PdfPCell cell_valorneletras = new PdfPCell(new Phrase("VALOR EN LETRAS:  " + string.Format("{0}",
                Nroenletras(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"].ToString())), fontCustom))
                { Border = 0, MinimumHeight = 25, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP };
                cell_valorneletras.Border = PdfPCell.TOP_BORDER;
                valorneletras.AddCell(cell_valorneletras);
                cell1.AddElement(valorneletras);
                fin_detalle.AddCell(cell1);

                PdfPCell cell2 = new PdfPCell() { Border = 0, Padding = 0 };

                PdfPTable iva = new PdfPTable(2);
                iva.WidthPercentage = 100;
                PdfPCell cell_iva = new PdfPCell(new Phrase("IVA", fontCustom)) { Border = 0, MinimumHeight = 15 };
                cell_iva.Border = PdfPCell.TOP_BORDER | PdfPCell.LEFT_BORDER;
                PdfPCell cell_ivaV = new PdfPCell(new Phrase(GetValImpuestoByID("01", DsInvoiceAR).ToString("C2"), fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_RIGHT };
                cell_ivaV.Border = PdfPCell.TOP_BORDER | PdfPCell.LEFT_BORDER;
                iva.AddCell(cell_iva);
                iva.AddCell(cell_ivaV);
                cell2.AddElement(iva);

                PdfPTable retencion = new PdfPTable(2);
                retencion.WidthPercentage = 100;
                PdfPCell cell_retencion = new PdfPCell(new Phrase("RETENCION DE LA IVA ", fontCustom)) { MinimumHeight = 15 };
                cell_retencion.Border = PdfPCell.TOP_BORDER | PdfPCell.LEFT_BORDER;
                PdfPCell cell_retencionV = new PdfPCell(new Phrase(GetValImpuestoByID("0B", DsInvoiceAR).ToString("C2"), fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_RIGHT };
                cell_retencionV.Border = PdfPCell.TOP_BORDER | PdfPCell.LEFT_BORDER;
                retencion.AddCell(cell_retencion);
                retencion.AddCell(cell_retencionV);
                cell2.AddElement(retencion);

                PdfPTable retencionS = new PdfPTable(2);
                retencionS.WidthPercentage = 100;
                PdfPCell cell_retencionS = new PdfPCell(new Phrase("RETENCION EN LA FUENTE SERVICIO ", fontCustom)) { MinimumHeight = 15 };
                cell_retencionS.Border = PdfPCell.TOP_BORDER | PdfPCell.LEFT_BORDER;
                PdfPCell cell_retencionSV = new PdfPCell(new Phrase(GetValImpuestoByID("0A", DsInvoiceAR).ToString("C2"), fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_RIGHT };
                cell_retencionSV.Border = PdfPCell.TOP_BORDER | PdfPCell.LEFT_BORDER;
                retencionS.AddCell(cell_retencionS);
                retencionS.AddCell(cell_retencionSV);
                cell2.AddElement(retencionS);

                PdfPTable total = new PdfPTable(2);
                total.WidthPercentage = 100;
                PdfPCell cell_total = new PdfPCell(new Phrase("TOTAL A PAGAR", fontCustom)) { MinimumHeight = 14 };
                cell_total.Border = PdfPCell.TOP_BORDER | PdfPCell.LEFT_BORDER;
                PdfPCell cell_totalV = new PdfPCell(new Phrase("$" +
                    decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"]).ToString("N2"),
                    fontCustom))
                { Border = 0, HorizontalAlignment = Element.ALIGN_RIGHT };
                cell_totalV.Border = PdfPCell.TOP_BORDER | PdfPCell.LEFT_BORDER;
                total.AddCell(cell_total);
                total.AddCell(cell_totalV);
                cell2.AddElement(total);

                fin_detalle.AddCell(cell2);

                u_d.AddElement(fin_detalle);
                unidades_detalles.AddCell(u_d);

                #endregion

                #region Footer

                PdfPTable footer = new PdfPTable(new float[] { 0.4f, 0.01F, 0.25f, 0.01F, 0.25f });
                footer.WidthPercentage = 100;

                PdfPCell beneficiario = new PdfPCell(new Phrase("BENEFICIARIO", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, };
                PdfPCell espacio1 = new PdfPCell(new Phrase("")) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, };
                PdfPCell emisor = new PdfPCell(new Phrase("EMISOR", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, };
                PdfPCell espacio2 = new PdfPCell(new Phrase("")) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, };
                PdfPCell leyenda2 = new PdfPCell(new Phrase("")) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, };

                footer.AddCell(beneficiario);
                footer.AddCell(espacio1);
                footer.AddCell(emisor);
                footer.AddCell(espacio2);
                footer.AddCell(leyenda2);


                PdfPTable footer_text = new PdfPTable(new float[] { 0.4f, 0.01F, 0.25f, 0.01F, 0.25f });
                footer_text.WidthPercentage = 100;

                PdfPCell beneficiariot = new PdfPCell(new Phrase("NOMBRE DE QUIE RECIBE______________________________________________________________" +
                                                                "C.C._________________________________________________________________" +
                                                                "FECHA DE RECIBO______________________________________________________________" +
                                                                "FIRMA Y SELLO________________________________________________________",
                    fontCustom))
                { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP, CellEvent = CelEventBorderRound, MinimumHeight = 50, };
                PdfPCell espacio1t = new PdfPCell(new Phrase("")) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, };
                PdfPCell emisort = new PdfPCell(new Phrase("__________________________________________SELLO Y FIRMA AUTORIZADA", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_BOTTOM, CellEvent = CelEventBorderRound, };
                PdfPCell espacio2t = new PdfPCell(new Phrase("")) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, };
                PdfPCell leyenda2t = new PdfPCell(new Phrase("Esta factura puede ser cancelada con cheque cruzado a nombre de laborales medellin S.A Si cancela por trasferencia eletronica o consignacion directa, por favor informar al fax. 3815650 ó al e-mail cartera@laborales.com.co. SI PASADOS 10 DIAS NO SE HAN REALIZADO RECLAMOS, SE ENTENDE COMO ACEPTADA ESTA FACTURA ", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP, CellEvent = CelEventBorderRound, };

                footer_text.AddCell(beneficiariot);
                footer_text.AddCell(espacio1t);
                footer_text.AddCell(emisort);
                footer_text.AddCell(espacio2t);
                footer_text.AddCell(leyenda2t);


                //agregamos la primera leyenda 3
                PdfPTable leyenda3 = new PdfPTable(2);
                leyenda3.WidthPercentage = 100;
                PdfPCell cell_leyenda3 = new PdfPCell(new Phrase("Acepto el contenido de la siguiente factura y las condiciones de pago ", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, };
                PdfPCell cell_leyenda3_ = new PdfPCell(new Phrase("CUFE:" + CUFE, fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_RIGHT, };
                leyenda3.AddCell(cell_leyenda3);
                leyenda3.AddCell(cell_leyenda3_);


                //agregamos la primera leyenda 4
                PdfPTable leyenda4 = new PdfPTable(1);
                leyenda4.WidthPercentage = 100;
                PdfPCell cell_leyenda4 = new PdfPCell(new Phrase(/*"/*Original-Vendedor-  1ª Copia -Cliente- 2ª Copia -Contabilidad-"*/)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, };
                leyenda4.AddCell(cell_leyenda4);


                #endregion

                #region Exti

                document.Add(header);
                document.Add(datos_clientes);
                document.Add(leyenda1);
                document.Add(unidades_detalles);
                document.Add(footer);
                document.Add(footer_text);
                document.Add(leyenda3);
                document.Add(leyenda4);

                //document.Add(divTextBancoCheque);
                /*PIE DE PAGINA*/
                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();
                RutaPdf = NomArchivo;
                return true;
                #endregion
            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.Message;
                return false;
            }


        }

        public bool FacturaVentasApoyosIndustriales(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {
            //NomArchivo = string.Empty;
            //string Ruta = HttpContext.Current.Server.MapPath("~/PDF/" + NIT + "/");
            //string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            //string RutaImg = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";
            //string Rutacertificado = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";
            //NomArchivo = NombreInvoice + ".pdf";

            NomArchivo = string.Empty;
            string Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            string RutaImg = $@"{AppDomain.CurrentDomain.BaseDirectory}\LaboralesMedellin.png";
            string Rutacertificado = $@"{AppDomain.CurrentDomain.BaseDirectory}\LOGO_PLASTICOS_AMBIENTALES.png";
            NomArchivo = NombreInvoice + ".pdf";
            try
            {
                #region Head
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.
                Document document = new Document(iTextSharp.text.PageSize.LETTER);
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;

                // step 3: we open the document     
                document.Open();

                PdfDiv divEspacio = new PdfDiv();
                divEspacio.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio.Height = 10;
                divEspacio.Width = 130;

                PdfDiv divEspacio2 = new PdfDiv();
                divEspacio2.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio2.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio2.Height = 2;
                divEspacio2.Width = 130;

                //FUENTES----------------------------------------------------------------------------------------------------------
                iTextSharp.text.Font titulo = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 10, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font titulofactura = FontFactory.GetFont(FontFactory.TIMES_BOLD, 10, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle1 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 10, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font titulosdetablas = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 12, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle2 = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustom = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustom2 = FontFactory.GetFont(FontFactory.HELVETICA, 9, iTextSharp.text.Font.NORMAL);
                //FUENTES---------------------------------------------------------------------------------------------------------
                ///objeto para el borde 
                RoundRectangle CelEventBorderRound = new RoundRectangle();


                System.Drawing.Image Logo;
                System.Drawing.Image LogoBanner = null;
                var request = WebRequest.Create(RutaImg);
                using (var response = request.GetResponse())
                using (var stream = response.GetResponseStream())
                {
                    Logo = Bitmap.FromStream(stream);
                }
                var requestBanner = WebRequest.Create(RutaImg);
                using (var responseBanner = requestBanner.GetResponse())
                using (var streamBanner = responseBanner.GetResponseStream())
                {
                    LogoBanner = Bitmap.FromStream(streamBanner);
                }


                System.Drawing.Image Logo2;
                System.Drawing.Image LogoBanner2 = null;
                var request2 = WebRequest.Create(Rutacertificado);
                using (var response2 = request2.GetResponse())
                using (var stream2 = response2.GetResponseStream())
                {
                    Logo2 = Bitmap.FromStream(stream2);
                }
                var requestBanner2 = WebRequest.Create(Rutacertificado);
                using (var responseBanner2 = requestBanner2.GetResponse())
                using (var streamBanner2 = responseBanner2.GetResponseStream())
                {
                    LogoBanner2 = Bitmap.FromStream(streamBanner2);
                }


                //Logos--------------------------------
                iTextSharp.text.Image LogoPdf3 = iTextSharp.text.Image.GetInstance(LogoBanner2, BaseColor.WHITE);
                LogoPdf3.ScaleAbsolute(90f, 90f);
                LogoPdf3.Border = 0;


                iTextSharp.text.Image LogoPdf2 = iTextSharp.text.Image.GetInstance(LogoBanner, BaseColor.WHITE);
                LogoPdf2.ScaleAbsolute(70, 70);
                LogoPdf2.Border = 0;

                iTextSharp.text.Image QRPdf = iTextSharp.text.Image.GetInstance(QRInvoice, BaseColor.WHITE);
                QRPdf.ScaleAbsolute(60, 60);
                QRPdf.Border = 0;


                PdfPTable espacio = new PdfPTable(1);
                espacio.WidthPercentage = 100;
                PdfPCell salto = new PdfPCell() { Border = 0, MinimumHeight = 50, };
                espacio.AddCell(salto);

                #endregion

                #region Header
                ///agregamos los datos del encabwezado 
                PdfPTable header = new PdfPTable(new float[] { 0.15f, 0.30f, 0.25f, 0.15f, 0.15f });
                header.WidthPercentage = 100;

                PdfPCell cell_logo = new PdfPCell() { Border = 0, };
                cell_logo.AddElement(LogoPdf2);

                PdfPCell cell_empresa = new PdfPCell() { Border = 0, };
                Paragraph prgTitle1 = new Paragraph(string.Format("" + DsInvoiceAR.Tables["Company"].Rows[0]["Name"].ToString())) { Alignment = Element.ALIGN_CENTER, };
                Paragraph prgTitle2 = new Paragraph(string.Format("NIt. " + DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"].ToString()), fontTitle) { Alignment = Element.ALIGN_CENTER, };
                Paragraph prgTitle3 = new Paragraph(string.Format("I.V.A REGIMEN COMUN"), fontTitle) { Alignment = Element.ALIGN_CENTER, };
                Paragraph prgTitle4 = new Paragraph(string.Format("NO SOMOS GRANDES CONTRIBUYENTES - NI AUTORETENEDORES"), fontTitle) { Alignment = Element.ALIGN_CENTER, };
                Paragraph prgTitle5 = new Paragraph(string.Format("LICENCIA DE FUNCIONAMIENTO DEL MINISTERIO DE TRABAJO No 00061 de nov. 4 de 1963"), fontTitle) { Alignment = Element.ALIGN_CENTER, };
                cell_empresa.AddElement(prgTitle1);
                cell_empresa.AddElement(prgTitle2);
                cell_empresa.AddElement(prgTitle3);
                cell_empresa.AddElement(prgTitle4);
                cell_empresa.AddElement(prgTitle5);

                PdfPCell sucursal = new PdfPCell() { Border = 0, };
                Paragraph prgTitle6 = new Paragraph(string.Format("Calle 25ª N.43B-33"), fontTitle) { Alignment = Element.ALIGN_CENTER, };
                Paragraph prgTitle7 = new Paragraph(string.Format("CONMUTADOR: 262 55 33 - FAX: 381 56 50"), fontTitle) { Alignment = Element.ALIGN_CENTER, };
                Paragraph prgTitle8 = new Paragraph(string.Format("APARTADO 53277 -MEDELLIN"), fontTitle) { Alignment = Element.ALIGN_CENTER, };
                Paragraph prgTitle9 = new Paragraph(string.Format("E-mail: apoyos_fe@laborales.com.co -Pagina: www.laborales.com.co"), fontTitle) { Alignment = Element.ALIGN_CENTER, };
                sucursal.AddElement(prgTitle6);
                sucursal.AddElement(prgTitle7);
                sucursal.AddElement(prgTitle8);
                sucursal.AddElement(prgTitle9);

                PdfPCell cell_QR = new PdfPCell() { Border = 0, };
                cell_QR.AddElement(QRPdf);
                PdfPCell cell_factura = new PdfPCell(new Phrase("", fontTitle)) { Border = 0, };
                Paragraph prgTitle10 = new Paragraph(string.Format("FECTURA DE VENTA No."), fontTitle) { Alignment = Element.ALIGN_CENTER, };
                Paragraph prgTitle11 = new Paragraph(string.Format(DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"].ToString()), fontTitle) { Alignment = Element.ALIGN_CENTER, };
                cell_factura.AddElement(prgTitle10);
                cell_factura.AddElement(prgTitle11);


                header.AddCell(cell_logo);
                header.AddCell(cell_empresa);
                header.AddCell(sucursal);
                header.AddCell(cell_QR);
                header.AddCell(cell_factura);

                ///agregamos los datos de los clientes 
                PdfPTable datos_clientes = new PdfPTable(new float[] { 0.7f, 0.01f, 0.259f });
                datos_clientes.WidthPercentage = 100;

                PdfPCell señores = new PdfPCell(new Phrase("")) { CellEvent = CelEventBorderRound, Border = 0, };
                Paragraph prgcustom0 = new Paragraph(string.Format(" " + DsInvoiceAR.Tables["Customer"].Rows[0]["Name"].ToString()), fontCustom) { Alignment = Element.ALIGN_LEFT, };
                Paragraph prgcustom1 = new Paragraph(string.Format("NIT: " + DsInvoiceAR.Tables["Customer"].Rows[0]["CustID"].ToString()), fontCustom) { Alignment = Element.ALIGN_LEFT, };
                Paragraph prgcustom2 = new Paragraph(string.Format("DIRECCION: " + DsInvoiceAR.Tables["Customer"].Rows[0]["Address1"].ToString()), fontCustom) { Alignment = Element.ALIGN_LEFT, };
                Paragraph prgcustom3 = new Paragraph(string.Format("CIUDAD: " + DsInvoiceAR.Tables["Customer"].Rows[0]["City"].ToString()), fontCustom) { Alignment = Element.ALIGN_LEFT, };
                Paragraph prgcustom4 = new Paragraph(string.Format("CENTRO DE COSTOS: " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number01"].ToString()), fontCustom) { Alignment = Element.ALIGN_LEFT, };
                señores.AddElement(prgcustom0);
                señores.AddElement(prgcustom1);
                señores.AddElement(prgcustom2);
                señores.AddElement(prgcustom3);
                señores.AddElement(prgcustom4);

                PdfPCell medio = new PdfPCell(new Phrase("")) { Border = 0, };

                PdfPCell fechas = new PdfPCell() { CellEvent = CelEventBorderRound, Border = 0, };

                PdfPTable fecha = new PdfPTable(2);
                fecha.WidthPercentage = 100;
                PdfPCell f_factura = new PdfPCell(new Phrase("FECHA FACTURA", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_CENTER, };
                f_factura.Border = PdfPCell.RIGHT_BORDER;
                fecha.AddCell(f_factura);
                PdfPCell f_vencimiento = new PdfPCell(new Phrase("FECHA VENCIMIENTO", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_CENTER };
                fecha.AddCell(f_vencimiento);
                fechas.AddElement(fecha);

                PdfPTable formato_dividido = new PdfPTable(6);
                formato_dividido.WidthPercentage = 100;

                PdfPCell fd = new PdfPCell(new Phrase("DIA", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER };
                fd.Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER | PdfPCell.RIGHT_BORDER;
                PdfPCell fm = new PdfPCell(new Phrase("MES", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER };
                fm.Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER | PdfPCell.RIGHT_BORDER;
                PdfPCell fa = new PdfPCell(new Phrase("AÑO", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER };
                fa.Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER | PdfPCell.RIGHT_BORDER;
                PdfPCell vd = new PdfPCell(new Phrase("DIA", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER };
                vd.Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER | PdfPCell.RIGHT_BORDER;
                PdfPCell vm = new PdfPCell(new Phrase("MES", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER };
                vm.Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER | PdfPCell.RIGHT_BORDER;
                PdfPCell va = new PdfPCell(new Phrase("AÑO", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER };
                va.Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER;

                formato_dividido.AddCell(fd);
                formato_dividido.AddCell(fm);
                formato_dividido.AddCell(fa);
                formato_dividido.AddCell(vd);
                formato_dividido.AddCell(vm);
                formato_dividido.AddCell(va);

                fechas.AddElement(formato_dividido);


                PdfPTable formato_divididoV = new PdfPTable(6);
                formato_divididoV.WidthPercentage = 100;

                PdfPCell fdV = new PdfPCell(new Phrase(DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).ToString("dd"), fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER };
                fdV.Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER | PdfPCell.RIGHT_BORDER;
                PdfPCell fmV = new PdfPCell(new Phrase(DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).ToString("MM"), fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER };
                fmV.Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER | PdfPCell.RIGHT_BORDER;
                PdfPCell faV = new PdfPCell(new Phrase(DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).ToString("yyyy"), fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER };
                faV.Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER | PdfPCell.RIGHT_BORDER;
                PdfPCell vdV = new PdfPCell(new Phrase(DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"]).ToString("dd"), fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER };
                vdV.Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER | PdfPCell.RIGHT_BORDER;
                PdfPCell vmV = new PdfPCell(new Phrase(DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"]).ToString("MM"), fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER };
                vmV.Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER | PdfPCell.RIGHT_BORDER;
                PdfPCell vaV = new PdfPCell(new Phrase(DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"]).ToString("yyyy"), fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER };
                vaV.Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER;

                formato_divididoV.AddCell(fdV);
                formato_divididoV.AddCell(fmV);
                formato_divididoV.AddCell(faV);
                formato_divididoV.AddCell(vdV);
                formato_divididoV.AddCell(vmV);
                formato_divididoV.AddCell(vaV);

                fechas.AddElement(formato_divididoV);

                PdfPTable formato_divididoP = new PdfPTable(1);
                formato_divididoP.WidthPercentage = 100;
                PdfPCell P = new PdfPCell(new Phrase("PERIODO: " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar01"], fontCustom)) { HorizontalAlignment = Element.ALIGN_LEFT, Border = 0, };
                formato_divididoP.AddCell(P);
                fechas.AddElement(formato_divididoP);

                datos_clientes.AddCell(señores);
                datos_clientes.AddCell(medio);
                datos_clientes.AddCell(fechas);

                //agregamos la primera leyenda 
                PdfPTable leyenda1 = new PdfPTable(1);
                leyenda1.WidthPercentage = 100;                                                             /*DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar01"], */
                PdfPCell cell_leyenda1 = new PdfPCell(new Phrase("" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["Character02"], fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, };
                leyenda1.AddCell(cell_leyenda1);

                #endregion

                #region Body

                //esta tabla cubre las regiiones Body y unidades contiene toda la tabla de detalles 
                PdfPTable unidades_detalles = new PdfPTable(1);
                unidades_detalles.WidthPercentage = 100;
                PdfPCell u_d = new PdfPCell() { Border = 0, CellEvent = CelEventBorderRound, };

                //agregamos los titulos de la tabla de detalles 
                PdfPTable body = new PdfPTable(new float[] { 0.7f, 0.15f, 0.15f, });
                body.WidthPercentage = 100;
                PdfPCell descripcion = new PdfPCell(new Phrase("DESCRIPCIÓN", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_CENTER, };
                descripcion.Border = PdfPCell.RIGHT_BORDER | PdfPCell.BOTTOM_BORDER;
                body.AddCell(descripcion);
                PdfPCell v_principal = new PdfPCell(new Phrase("VALOR PRINCIPAL", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_CENTER, };
                v_principal.Border = PdfPCell.RIGHT_BORDER | PdfPCell.BOTTOM_BORDER;
                body.AddCell(v_principal);
                PdfPCell V_total = new PdfPCell(new Phrase("VALOR TOTAL", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_CENTER, };
                V_total.Border = PdfPCell.BOTTOM_BORDER;
                body.AddCell(V_total);
                u_d.AddElement(body);

                #endregion

                #region Unidades

                PdfPTable tableUnidades = new PdfPTable(new float[] { 0.7f, 0.15f, 0.15f, });
                tableUnidades.WidthPercentage = 100;

                foreach (DataRow InvoiceLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
                {
                    if (!AddUnidadesLaboralesMdellin(InvoiceLine, ref tableUnidades, fontCustom, DsInvoiceAR))
                        return false;
                }

                iTextSharp.text.pdf.PdfPCell LineaFinal = new iTextSharp.text.pdf.PdfPCell();
                LineaFinal.Colspan = 3;
                LineaFinal.Padding = 3;
                LineaFinal.Border = 1;
                LineaFinal.BorderColorBottom = BaseColor.WHITE;
                LineaFinal.HorizontalAlignment = Element.ALIGN_CENTER;
                LineaFinal.VerticalAlignment = Element.ALIGN_TOP;
                tableUnidades.AddCell(LineaFinal);
                u_d.AddElement(tableUnidades);


                //agregamos los lo valores debajo de los de lles
                PdfPTable fin_detalle = new PdfPTable(new float[] { 0.7f, 0.3f, });
                fin_detalle.WidthPercentage = 100;
                PdfPCell cell1 = new PdfPCell() { Border = 0, Padding = 0, };

                PdfPTable observaciones = new PdfPTable(1);
                observaciones.WidthPercentage = 100;
                PdfPCell cell_obsevaciones = new PdfPCell(new Phrase("OBSERVACIÓNES" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceComment"], fontCustom)) { Border = 0, MinimumHeight = 35, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP };
                cell_obsevaciones.Border = PdfPCell.TOP_BORDER;
                observaciones.AddCell(cell_obsevaciones);
                cell1.AddElement(observaciones);

                PdfPTable valorneletras = new PdfPTable(1);
                valorneletras.WidthPercentage = 100;
                PdfPCell cell_valorneletras = new PdfPCell(new Phrase("VALOR EN LETRAS:  " + string.Format("{0}",
                Nroenletras(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"].ToString())), fontCustom))
                { Border = 0, MinimumHeight = 25, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP };
                cell_valorneletras.Border = PdfPCell.TOP_BORDER;
                valorneletras.AddCell(cell_valorneletras);
                cell1.AddElement(valorneletras);
                fin_detalle.AddCell(cell1);

                PdfPCell cell2 = new PdfPCell() { Border = 0, Padding = 0 };

                PdfPTable iva = new PdfPTable(2);
                iva.WidthPercentage = 100;
                PdfPCell cell_iva = new PdfPCell(new Phrase("IVA", fontCustom)) { Border = 0, MinimumHeight = 15 };
                cell_iva.Border = PdfPCell.TOP_BORDER | PdfPCell.LEFT_BORDER;
                PdfPCell cell_ivaV = new PdfPCell(new Phrase(GetValImpuestoByID("01", DsInvoiceAR).ToString("C2"), fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_RIGHT };
                cell_ivaV.Border = PdfPCell.TOP_BORDER | PdfPCell.LEFT_BORDER;
                iva.AddCell(cell_iva);
                iva.AddCell(cell_ivaV);
                cell2.AddElement(iva);

                PdfPTable retencion = new PdfPTable(2);
                retencion.WidthPercentage = 100;
                PdfPCell cell_retencion = new PdfPCell(new Phrase("RETENCION DE LA IVA ", fontCustom)) { MinimumHeight = 15 };
                cell_retencion.Border = PdfPCell.TOP_BORDER | PdfPCell.LEFT_BORDER;
                PdfPCell cell_retencionV = new PdfPCell(new Phrase(GetValImpuestoByID("0B", DsInvoiceAR).ToString("C2"), fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_RIGHT };
                cell_retencionV.Border = PdfPCell.TOP_BORDER | PdfPCell.LEFT_BORDER;
                retencion.AddCell(cell_retencion);
                retencion.AddCell(cell_retencionV);
                cell2.AddElement(retencion);

                PdfPTable retencionS = new PdfPTable(2);
                retencionS.WidthPercentage = 100;
                PdfPCell cell_retencionS = new PdfPCell(new Phrase("RETENCION EN LA FUENTE SERVICIO ", fontCustom)) { MinimumHeight = 15 };
                cell_retencionS.Border = PdfPCell.TOP_BORDER | PdfPCell.LEFT_BORDER;
                PdfPCell cell_retencionSV = new PdfPCell(new Phrase(GetValImpuestoByID("0A", DsInvoiceAR).ToString("C2"), fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_RIGHT };
                cell_retencionSV.Border = PdfPCell.TOP_BORDER | PdfPCell.LEFT_BORDER;
                retencionS.AddCell(cell_retencionS);
                retencionS.AddCell(cell_retencionSV);
                cell2.AddElement(retencionS);

                PdfPTable total = new PdfPTable(2);
                total.WidthPercentage = 100;
                PdfPCell cell_total = new PdfPCell(new Phrase("TOTAL A PAGAR", fontCustom)) { MinimumHeight = 14 };
                cell_total.Border = PdfPCell.TOP_BORDER | PdfPCell.LEFT_BORDER;
                PdfPCell cell_totalV = new PdfPCell(new Phrase("$" +
                    decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"]).ToString("N2"),
                    fontCustom))
                { Border = 0, HorizontalAlignment = Element.ALIGN_RIGHT };
                cell_totalV.Border = PdfPCell.TOP_BORDER | PdfPCell.LEFT_BORDER;
                total.AddCell(cell_total);
                total.AddCell(cell_totalV);
                cell2.AddElement(total);

                fin_detalle.AddCell(cell2);

                u_d.AddElement(fin_detalle);
                unidades_detalles.AddCell(u_d);

                #endregion

                #region Footer

                PdfPTable footer = new PdfPTable(new float[] { 0.4f, 0.01F, 0.25f, 0.01F, 0.25f });
                footer.WidthPercentage = 100;

                PdfPCell beneficiario = new PdfPCell(new Phrase("BENEFICIARIO", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, };
                PdfPCell espacio1 = new PdfPCell(new Phrase("")) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, };
                PdfPCell emisor = new PdfPCell(new Phrase("EMISOR", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, };
                PdfPCell espacio2 = new PdfPCell(new Phrase("")) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, };
                PdfPCell leyenda2 = new PdfPCell(new Phrase("")) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, };

                footer.AddCell(beneficiario);
                footer.AddCell(espacio1);
                footer.AddCell(emisor);
                footer.AddCell(espacio2);
                footer.AddCell(leyenda2);


                PdfPTable footer_text = new PdfPTable(new float[] { 0.4f, 0.01F, 0.25f, 0.01F, 0.25f });
                footer_text.WidthPercentage = 100;

                PdfPCell beneficiariot = new PdfPCell(new Phrase("NOMBRE DE QUIE RECIBE______________________________________________________________" +
                                                                "C.C._________________________________________________________________" +
                                                                "FECHA DE RECIBO______________________________________________________________" +
                                                                "FIRMA Y SELLO________________________________________________________",
                    fontCustom))
                { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP, CellEvent = CelEventBorderRound, MinimumHeight = 50, };
                PdfPCell espacio1t = new PdfPCell(new Phrase("")) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, };
                PdfPCell emisort = new PdfPCell(new Phrase("__________________________________________SELLO Y FIRMA AUTORIZADA", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_BOTTOM, CellEvent = CelEventBorderRound, };
                PdfPCell espacio2t = new PdfPCell(new Phrase("")) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, };
                PdfPCell leyenda2t = new PdfPCell(new Phrase("Esta factura puede ser cancelada con cheque cruzado a nombre de laborales medellin S.A Si cancela por trasferencia eletronica o consignacion directa, por favor informar al fax. 3815650 ó al e-mail cartera@laborales.com.co. SI PASADOS 10 DIAS NO SE HAN REALIZADO RECLAMOS, SE ENTENDE COMO ACEPTADA ESTA FACTURA ", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP, CellEvent = CelEventBorderRound, };

                footer_text.AddCell(beneficiariot);
                footer_text.AddCell(espacio1t);
                footer_text.AddCell(emisort);
                footer_text.AddCell(espacio2t);
                footer_text.AddCell(leyenda2t);


                //agregamos la primera leyenda 3
                PdfPTable leyenda3 = new PdfPTable(2);
                leyenda3.WidthPercentage = 100;
                PdfPCell cell_leyenda3 = new PdfPCell(new Phrase("Acepto el contenido de la siguiente factura y las condiciones de pago ", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, };
                PdfPCell cell_leyenda3_ = new PdfPCell(new Phrase("CUFE:" + CUFE, fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_RIGHT, };
                leyenda3.AddCell(cell_leyenda3);
                leyenda3.AddCell(cell_leyenda3_);


                //agregamos la primera leyenda 4
                PdfPTable leyenda4 = new PdfPTable(1);
                leyenda4.WidthPercentage = 100;
                PdfPCell cell_leyenda4 = new PdfPCell(new Phrase(/*"/*Original-Vendedor-  1ª Copia -Cliente- 2ª Copia -Contabilidad-"*/)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, };
                leyenda4.AddCell(cell_leyenda4);


                #endregion

                #region Exti

                document.Add(header);
                document.Add(datos_clientes);
                document.Add(leyenda1);
                document.Add(unidades_detalles);
                document.Add(footer);
                document.Add(footer_text);
                document.Add(leyenda3);
                document.Add(leyenda4);

                //document.Add(divTextBancoCheque);
                /*PIE DE PAGINA*/
                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();
                RutaPdf = NomArchivo;
                return true;
                #endregion
            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.Message;
                return false;
            }
        }

        public bool NotacreditoApoyosIndustriales(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {
            //NomArchivo = string.Empty;
            //string Ruta = HttpContext.Current.Server.MapPath("~/PDF/" + NIT + "/");
            //string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            //string RutaImg = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";
            //string Rutacertificado = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";
            //NomArchivo = NombreInvoice + ".pdf";

            NomArchivo = string.Empty;
            string Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            string RutaImg = $@"{AppDomain.CurrentDomain.BaseDirectory}\LaboralesMedellin.png";
            string Rutacertificado = $@"{AppDomain.CurrentDomain.BaseDirectory}\LOGO_PLASTICOS_AMBIENTALES.png";
            NomArchivo = NombreInvoice + ".pdf";
            try
            {
                #region Head
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.
                Document document = new Document(iTextSharp.text.PageSize.LETTER);
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;

                // step 3: we open the document     
                document.Open();

                PdfDiv divEspacio = new PdfDiv();
                divEspacio.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio.Height = 10;
                divEspacio.Width = 130;

                PdfDiv divEspacio2 = new PdfDiv();
                divEspacio2.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio2.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio2.Height = 2;
                divEspacio2.Width = 130;

                //FUENTES----------------------------------------------------------------------------------------------------------
                iTextSharp.text.Font titulo = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 10, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font titulofactura = FontFactory.GetFont(FontFactory.TIMES_BOLD, 10, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle1 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 10, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font titulosdetablas = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 12, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle2 = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustom = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustom2 = FontFactory.GetFont(FontFactory.HELVETICA, 9, iTextSharp.text.Font.NORMAL);
                //FUENTES---------------------------------------------------------------------------------------------------------
                ///objeto para el borde 
                RoundRectangle CelEventBorderRound = new RoundRectangle();


                System.Drawing.Image Logo;
                System.Drawing.Image LogoBanner = null;
                var request = WebRequest.Create(RutaImg);
                using (var response = request.GetResponse())
                using (var stream = response.GetResponseStream())
                {
                    Logo = Bitmap.FromStream(stream);
                }
                var requestBanner = WebRequest.Create(RutaImg);
                using (var responseBanner = requestBanner.GetResponse())
                using (var streamBanner = responseBanner.GetResponseStream())
                {
                    LogoBanner = Bitmap.FromStream(streamBanner);
                }


                System.Drawing.Image Logo2;
                System.Drawing.Image LogoBanner2 = null;
                var request2 = WebRequest.Create(Rutacertificado);
                using (var response2 = request2.GetResponse())
                using (var stream2 = response2.GetResponseStream())
                {
                    Logo2 = Bitmap.FromStream(stream2);
                }
                var requestBanner2 = WebRequest.Create(Rutacertificado);
                using (var responseBanner2 = requestBanner2.GetResponse())
                using (var streamBanner2 = responseBanner2.GetResponseStream())
                {
                    LogoBanner2 = Bitmap.FromStream(streamBanner2);
                }


                //Logos--------------------------------
                iTextSharp.text.Image LogoPdf3 = iTextSharp.text.Image.GetInstance(LogoBanner2, BaseColor.WHITE);
                LogoPdf3.ScaleAbsolute(90f, 90f);
                LogoPdf3.Border = 0;


                iTextSharp.text.Image LogoPdf2 = iTextSharp.text.Image.GetInstance(LogoBanner, BaseColor.WHITE);
                LogoPdf2.ScaleAbsolute(70, 70);
                LogoPdf2.Border = 0;

                iTextSharp.text.Image QRPdf = iTextSharp.text.Image.GetInstance(QRInvoice, BaseColor.WHITE);
                QRPdf.ScaleAbsolute(60, 60);
                QRPdf.Border = 0;


                PdfPTable espacio = new PdfPTable(1);
                espacio.WidthPercentage = 100;
                PdfPCell salto = new PdfPCell() { Border = 0, MinimumHeight = 50, };
                espacio.AddCell(salto);

                #endregion

                #region Header
                ///agregamos los datos del encabwezado 
                PdfPTable header = new PdfPTable(new float[] { 0.15f, 0.30f, 0.25f, 0.15f, 0.15f });
                header.WidthPercentage = 100;

                PdfPCell cell_logo = new PdfPCell() { Border = 0, };
                cell_logo.AddElement(LogoPdf2);

                PdfPCell cell_empresa = new PdfPCell() { Border = 0, };
                Paragraph prgTitle1 = new Paragraph(string.Format("LABORALES MEDELLIN S.A")) { Alignment = Element.ALIGN_CENTER, };
                Paragraph prgTitle2 = new Paragraph(string.Format("NIT. 900420346-1"), fontTitle) { Alignment = Element.ALIGN_CENTER, };
                Paragraph prgTitle3 = new Paragraph(string.Format("I.V.A REGIMEN COMUN"), fontTitle) { Alignment = Element.ALIGN_CENTER, };
                Paragraph prgTitle4 = new Paragraph(string.Format("NO SOMOS GRANDES CONTRIBUYENTES - NI AUTORETENEDORES"), fontTitle) { Alignment = Element.ALIGN_CENTER, };
                Paragraph prgTitle5 = new Paragraph(string.Format("LICENSIA DE FUNCIONAMIENTO DEL MINISTERIO DE TRABAJO No 00061 de nov. 4 de 1963"), fontTitle) { Alignment = Element.ALIGN_CENTER, };
                cell_empresa.AddElement(prgTitle1);
                cell_empresa.AddElement(prgTitle2);
                cell_empresa.AddElement(prgTitle3);
                cell_empresa.AddElement(prgTitle4);
                cell_empresa.AddElement(prgTitle5);

                PdfPCell sucursal = new PdfPCell() { Border = 0, };
                Paragraph prgTitle6 = new Paragraph(string.Format("CALLE 25 No.43 - 33"), fontTitle) { Alignment = Element.ALIGN_CENTER, };
                Paragraph prgTitle7 = new Paragraph(string.Format("CONMUTADOR: 262 55 33 - FAX: 381 56 50"), fontTitle) { Alignment = Element.ALIGN_CENTER, };
                Paragraph prgTitle8 = new Paragraph(string.Format("APARTADO 53277 -MEDELLIN"), fontTitle) { Alignment = Element.ALIGN_CENTER, };
                Paragraph prgTitle9 = new Paragraph(string.Format("E-mail: facturacion@laboralesmedellin.com.co -www.laborale.com.co"), fontTitle) { Alignment = Element.ALIGN_CENTER, };
                sucursal.AddElement(prgTitle6);
                sucursal.AddElement(prgTitle7);
                sucursal.AddElement(prgTitle8);
                sucursal.AddElement(prgTitle9);

                PdfPCell cell_QR = new PdfPCell() { Border = 0, };
                cell_QR.AddElement(QRPdf);
                PdfPCell cell_factura = new PdfPCell(new Phrase("", fontTitle)) { Border = 0, };
                Paragraph prgTitle10 = new Paragraph(string.Format("NOTA CREDITO No."), fontTitle) { Alignment = Element.ALIGN_CENTER, };
                Paragraph prgTitle11 = new Paragraph(string.Format(DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"].ToString()), fontTitle) { Alignment = Element.ALIGN_CENTER, };
                cell_factura.AddElement(prgTitle10);
                cell_factura.AddElement(prgTitle11);


                header.AddCell(cell_logo);
                header.AddCell(cell_empresa);
                header.AddCell(sucursal);
                header.AddCell(cell_QR);
                header.AddCell(cell_factura);

                ///agregamos los datos de los clientes 
                PdfPTable datos_clientes = new PdfPTable(new float[] { 0.7f, 0.01f, 0.259f });
                datos_clientes.WidthPercentage = 100;

                PdfPCell señores = new PdfPCell(new Phrase("")) { CellEvent = CelEventBorderRound, Border = 0, };
                Paragraph prgcustom0 = new Paragraph(string.Format("SEÑORES: "), fontCustom) { Alignment = Element.ALIGN_LEFT, };
                Paragraph prgcustom1 = new Paragraph(string.Format("NIT: " + DsInvoiceAR.Tables["Customer"].Rows[0]["CustID"].ToString()), fontCustom) { Alignment = Element.ALIGN_LEFT, };
                Paragraph prgcustom2 = new Paragraph(string.Format("DIRECCION: " + DsInvoiceAR.Tables["Customer"].Rows[0]["Address1"].ToString()), fontCustom) { Alignment = Element.ALIGN_LEFT, };
                Paragraph prgcustom3 = new Paragraph(string.Format("CIUDAD: " + DsInvoiceAR.Tables["Customer"].Rows[0]["City"].ToString()), fontCustom) { Alignment = Element.ALIGN_LEFT, };
                Paragraph prgcustom4 = new Paragraph(string.Format("CENTRO DE COSTOS: " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number01"].ToString()), fontCustom) { Alignment = Element.ALIGN_LEFT, };
                señores.AddElement(prgcustom0);
                señores.AddElement(prgcustom1);
                señores.AddElement(prgcustom2);
                señores.AddElement(prgcustom3);
                señores.AddElement(prgcustom4);

                PdfPCell medio = new PdfPCell(new Phrase("")) { Border = 0, };

                PdfPCell fechas = new PdfPCell() { CellEvent = CelEventBorderRound, Border = 0, };

                PdfPTable fecha = new PdfPTable(2);
                fecha.WidthPercentage = 100;
                PdfPCell f_factura = new PdfPCell(new Phrase("FECHA FACTURA", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_CENTER, };
                f_factura.Border = PdfPCell.RIGHT_BORDER;
                fecha.AddCell(f_factura);
                PdfPCell f_vencimiento = new PdfPCell(new Phrase("FECHA VENCIMIENTO", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_CENTER };
                fecha.AddCell(f_vencimiento);
                fechas.AddElement(fecha);

                PdfPTable formato_dividido = new PdfPTable(6);
                formato_dividido.WidthPercentage = 100;

                PdfPCell fd = new PdfPCell(new Phrase("DIA", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER };
                fd.Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER | PdfPCell.RIGHT_BORDER;
                PdfPCell fm = new PdfPCell(new Phrase("MES", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER };
                fm.Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER | PdfPCell.RIGHT_BORDER;
                PdfPCell fa = new PdfPCell(new Phrase("AÑO", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER };
                fa.Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER | PdfPCell.RIGHT_BORDER;
                PdfPCell vd = new PdfPCell(new Phrase("DIA", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER };
                vd.Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER | PdfPCell.RIGHT_BORDER;
                PdfPCell vm = new PdfPCell(new Phrase("MES", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER };
                vm.Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER | PdfPCell.RIGHT_BORDER;
                PdfPCell va = new PdfPCell(new Phrase("AÑO", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER };
                va.Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER;

                formato_dividido.AddCell(fd);
                formato_dividido.AddCell(fm);
                formato_dividido.AddCell(fa);
                formato_dividido.AddCell(vd);
                formato_dividido.AddCell(vm);
                formato_dividido.AddCell(va);

                fechas.AddElement(formato_dividido);


                PdfPTable formato_divididoV = new PdfPTable(6);
                formato_divididoV.WidthPercentage = 100;

                PdfPCell fdV = new PdfPCell(new Phrase(DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).ToString("dd"), fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER };
                fdV.Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER | PdfPCell.RIGHT_BORDER;
                PdfPCell fmV = new PdfPCell(new Phrase(DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).ToString("MM"), fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER };
                fmV.Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER | PdfPCell.RIGHT_BORDER;
                PdfPCell faV = new PdfPCell(new Phrase(DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).ToString("yyyy"), fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER };
                faV.Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER | PdfPCell.RIGHT_BORDER;
                PdfPCell vdV = new PdfPCell(new Phrase(DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"]).ToString("dd"), fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER };
                vdV.Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER | PdfPCell.RIGHT_BORDER;
                PdfPCell vmV = new PdfPCell(new Phrase(DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"]).ToString("MM"), fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER };
                vmV.Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER | PdfPCell.RIGHT_BORDER;
                PdfPCell vaV = new PdfPCell(new Phrase(DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"]).ToString("yyyy"), fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER };
                vaV.Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER;

                formato_divididoV.AddCell(fdV);
                formato_divididoV.AddCell(fmV);
                formato_divididoV.AddCell(faV);
                formato_divididoV.AddCell(vdV);
                formato_divididoV.AddCell(vmV);
                formato_divididoV.AddCell(vaV);

                fechas.AddElement(formato_divididoV);

                PdfPTable formato_divididoP = new PdfPTable(1);
                formato_divididoP.WidthPercentage = 100;
                PdfPCell P = new PdfPCell(new Phrase("PERIODO: " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar01"], fontCustom)) { HorizontalAlignment = Element.ALIGN_LEFT, Border = 0, };
                formato_divididoP.AddCell(P);
                fechas.AddElement(formato_divididoP);

                datos_clientes.AddCell(señores);
                datos_clientes.AddCell(medio);
                datos_clientes.AddCell(fechas);

                //agregamos la primera leyenda 
                PdfPTable leyenda1 = new PdfPTable(1);
                leyenda1.WidthPercentage = 100;
                PdfPCell cell_leyenda1 = new PdfPCell(new Phrase("AUTORISACIÓN DE NUMERACIÓN DE LA FACTURA DIAN No. 18762003547063 DEL  2017/06/06 NUMERACIÓN DEL 180180 AL 190000 HABILITACIÓN - VIGENCIA 24 MESES", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, };
                leyenda1.AddCell(cell_leyenda1);

                #endregion

                #region Body

                //esta tabla cubre las regiiones Body y unidades contiene toda la tabla de detalles 
                PdfPTable unidades_detalles = new PdfPTable(1);
                unidades_detalles.WidthPercentage = 100;
                PdfPCell u_d = new PdfPCell() { Border = 0, CellEvent = CelEventBorderRound, };

                //agregamos los titulos de la tabla de detalles 
                PdfPTable body = new PdfPTable(new float[] { 0.7f, 0.15f, 0.15f, });
                body.WidthPercentage = 100;
                PdfPCell descripcion = new PdfPCell(new Phrase("DESCRIPCIÓN", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_CENTER, };
                descripcion.Border = PdfPCell.RIGHT_BORDER | PdfPCell.BOTTOM_BORDER;
                body.AddCell(descripcion);
                PdfPCell v_principal = new PdfPCell(new Phrase("VALOR PRINSIPAL", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_CENTER, };
                v_principal.Border = PdfPCell.RIGHT_BORDER | PdfPCell.BOTTOM_BORDER;
                body.AddCell(v_principal);
                PdfPCell V_total = new PdfPCell(new Phrase("VALOR TOTAL", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_CENTER, };
                V_total.Border = PdfPCell.BOTTOM_BORDER;
                body.AddCell(V_total);
                u_d.AddElement(body);

                #endregion

                #region Unidades

                PdfPTable tableUnidades = new PdfPTable(new float[] { 0.7f, 0.15f, 0.15f, });
                tableUnidades.WidthPercentage = 100;

                foreach (DataRow InvoiceLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
                {
                    if (!AddUnidadesLaboralesMdellin(InvoiceLine, ref tableUnidades, fontCustom, DsInvoiceAR))
                        return false;
                }

                iTextSharp.text.pdf.PdfPCell LineaFinal = new iTextSharp.text.pdf.PdfPCell();
                LineaFinal.Colspan = 3;
                LineaFinal.Padding = 3;
                LineaFinal.Border = 1;
                LineaFinal.BorderColorBottom = BaseColor.WHITE;
                LineaFinal.HorizontalAlignment = Element.ALIGN_CENTER;
                LineaFinal.VerticalAlignment = Element.ALIGN_TOP;
                tableUnidades.AddCell(LineaFinal);
                u_d.AddElement(tableUnidades);


                //agregamos los lo valores debajo de los de lles
                PdfPTable fin_detalle = new PdfPTable(new float[] { 0.7f, 0.3f, });
                fin_detalle.WidthPercentage = 100;
                PdfPCell cell1 = new PdfPCell() { Border = 0, Padding = 0, };

                PdfPTable observaciones = new PdfPTable(1);
                observaciones.WidthPercentage = 100;
                PdfPCell cell_obsevaciones = new PdfPCell(new Phrase("OBSERVACIÓNES" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceComment"], fontCustom)) { Border = 0, MinimumHeight = 35, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP };
                cell_obsevaciones.Border = PdfPCell.TOP_BORDER;
                observaciones.AddCell(cell_obsevaciones);
                cell1.AddElement(observaciones);

                PdfPTable valorneletras = new PdfPTable(1);
                valorneletras.WidthPercentage = 100;
                PdfPCell cell_valorneletras = new PdfPCell(new Phrase("VALOR EN LETRAS:  " + string.Format("{0}",
                Nroenletras(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"].ToString())), fontCustom))
                { Border = 0, MinimumHeight = 25, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP };
                cell_valorneletras.Border = PdfPCell.TOP_BORDER;
                valorneletras.AddCell(cell_valorneletras);
                cell1.AddElement(valorneletras);
                fin_detalle.AddCell(cell1);

                PdfPCell cell2 = new PdfPCell() { Border = 0, Padding = 0 };

                PdfPTable iva = new PdfPTable(2);
                iva.WidthPercentage = 100;
                PdfPCell cell_iva = new PdfPCell(new Phrase("IVA", fontCustom)) { Border = 0, MinimumHeight = 15 };
                cell_iva.Border = PdfPCell.TOP_BORDER | PdfPCell.LEFT_BORDER;
                PdfPCell cell_ivaV = new PdfPCell(new Phrase(GetValImpuestoByID("01", DsInvoiceAR).ToString("C2"), fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_RIGHT };
                cell_ivaV.Border = PdfPCell.TOP_BORDER | PdfPCell.LEFT_BORDER;
                iva.AddCell(cell_iva);
                iva.AddCell(cell_ivaV);
                cell2.AddElement(iva);

                PdfPTable retencion = new PdfPTable(2);
                retencion.WidthPercentage = 100;
                PdfPCell cell_retencion = new PdfPCell(new Phrase("RETENCION DE LA IVA ", fontCustom)) { MinimumHeight = 15 };
                cell_retencion.Border = PdfPCell.TOP_BORDER | PdfPCell.LEFT_BORDER;
                PdfPCell cell_retencionV = new PdfPCell(new Phrase(GetValImpuestoByID("0B", DsInvoiceAR).ToString("C2"), fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_RIGHT };
                cell_retencionV.Border = PdfPCell.TOP_BORDER | PdfPCell.LEFT_BORDER;
                retencion.AddCell(cell_retencion);
                retencion.AddCell(cell_retencionV);
                cell2.AddElement(retencion);

                PdfPTable retencionS = new PdfPTable(2);
                retencionS.WidthPercentage = 100;
                PdfPCell cell_retencionS = new PdfPCell(new Phrase("RETENCION EN LA FUENTE SERVICIO ", fontCustom)) { MinimumHeight = 15 };
                cell_retencionS.Border = PdfPCell.TOP_BORDER | PdfPCell.LEFT_BORDER;
                PdfPCell cell_retencionSV = new PdfPCell(new Phrase(GetValImpuestoByID("0c", DsInvoiceAR).ToString("C2"), fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_RIGHT };
                cell_retencionSV.Border = PdfPCell.TOP_BORDER | PdfPCell.LEFT_BORDER;
                retencionS.AddCell(cell_retencionS);
                retencionS.AddCell(cell_retencionSV);
                cell2.AddElement(retencionS);

                PdfPTable total = new PdfPTable(2);
                total.WidthPercentage = 100;
                PdfPCell cell_total = new PdfPCell(new Phrase("TOTAL A PAGAR", fontCustom)) { MinimumHeight = 14 };
                cell_total.Border = PdfPCell.TOP_BORDER | PdfPCell.LEFT_BORDER;
                PdfPCell cell_totalV = new PdfPCell(new Phrase("$" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"], fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_RIGHT };
                cell_totalV.Border = PdfPCell.TOP_BORDER | PdfPCell.LEFT_BORDER;
                total.AddCell(cell_total);
                total.AddCell(cell_totalV);
                cell2.AddElement(total);

                fin_detalle.AddCell(cell2);

                u_d.AddElement(fin_detalle);
                unidades_detalles.AddCell(u_d);

                #endregion

                #region Footer

                PdfPTable footer = new PdfPTable(new float[] { 0.4f, 0.01F, 0.25f, 0.01F, 0.25f });
                footer.WidthPercentage = 100;

                PdfPCell beneficiario = new PdfPCell(new Phrase("BENEFICIARIO", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, };
                PdfPCell espacio1 = new PdfPCell(new Phrase("")) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, };
                PdfPCell emisor = new PdfPCell(new Phrase("EMISOR", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, };
                PdfPCell espacio2 = new PdfPCell(new Phrase("")) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, };
                PdfPCell leyenda2 = new PdfPCell(new Phrase("")) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, };

                footer.AddCell(beneficiario);
                footer.AddCell(espacio1);
                footer.AddCell(emisor);
                footer.AddCell(espacio2);
                footer.AddCell(leyenda2);


                PdfPTable footer_text = new PdfPTable(new float[] { 0.4f, 0.01F, 0.25f, 0.01F, 0.25f });
                footer_text.WidthPercentage = 100;

                PdfPCell beneficiariot = new PdfPCell(new Phrase("NOMBRE DE QUIE RECIBE______________________________________________________________" +
                                                                "C.C._________________________________________________________________" +
                                                                "FECHA DE RECIBO______________________________________________________________" +
                                                                "FIRMA Y SELLO________________________________________________________",
                    fontCustom))
                { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP, CellEvent = CelEventBorderRound, MinimumHeight = 50, };
                PdfPCell espacio1t = new PdfPCell(new Phrase("")) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, };
                PdfPCell emisort = new PdfPCell(new Phrase("__________________________________________SELLO Y FIRMA AUTORIZADA", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_BOTTOM, CellEvent = CelEventBorderRound, };
                PdfPCell espacio2t = new PdfPCell(new Phrase("")) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, };
                PdfPCell leyenda2t = new PdfPCell(new Phrase("Esta factura puede ser cancelada con cheque cruzado a nombre de laborales medellin S.A Si cancela por trasferencia eletronica o consignacion directa, por favor informar al fax. 3815650 ó al e-mail cartera@laborales.com.co. SI PASADOS 10 DIAS NO SE HAN REALIZADO RECLAMOS, SE ENTENDE COMO ACEPTADA ESTA FACTURA ", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP, CellEvent = CelEventBorderRound, };

                footer_text.AddCell(beneficiariot);
                footer_text.AddCell(espacio1t);
                footer_text.AddCell(emisort);
                footer_text.AddCell(espacio2t);
                footer_text.AddCell(leyenda2t);


                //agregamos la primera leyenda 3
                PdfPTable leyenda3 = new PdfPTable(2);
                leyenda3.WidthPercentage = 100;
                PdfPCell cell_leyenda3 = new PdfPCell(new Phrase("Acepto el contenido de la siguiente factura y las condiciones de pago ", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, };
                PdfPCell cell_leyenda3_ = new PdfPCell(new Phrase("CUFE:" + CUFE, fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_RIGHT, };
                leyenda3.AddCell(cell_leyenda3);
                leyenda3.AddCell(cell_leyenda3_);


                //agregamos la primera leyenda 4
                PdfPTable leyenda4 = new PdfPTable(1);
                leyenda4.WidthPercentage = 100;
                PdfPCell cell_leyenda4 = new PdfPCell(new Phrase("Original-Vendedor-  1ª Copia -Cliente- 2ª Copia -Contabilidad-")) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, };
                leyenda4.AddCell(cell_leyenda4);


                #endregion

                #region Exti

                document.Add(header);
                document.Add(datos_clientes);
                document.Add(leyenda1);
                document.Add(unidades_detalles);
                document.Add(footer);
                document.Add(footer_text);
                document.Add(leyenda3);
                document.Add(leyenda4);

                //document.Add(divTextBancoCheque);
                /*PIE DE PAGINA*/
                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();
                RutaPdf = NomArchivo;
                return true;
                #endregion
            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.Message;
                return false;
            }


        }

        #endregion

    }
}
