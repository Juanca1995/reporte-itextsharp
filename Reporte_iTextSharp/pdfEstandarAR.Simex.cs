﻿namespace RulesServicesDIAN2.Adquiriente
{
    using iTextSharp.text;
    using iTextSharp.text.pdf;
    using System;
    using System.Collections.Generic;
    //Referenciar y usar.
    using System.Data;
    using System.Drawing;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.NetworkInformation;
    using System.Net.Sockets;
    using System.Web;
    using System.Windows.Forms;

    public partial class pdfEstandarAR
    {
        #region Formatos Facturacion SIMEX
        float dimxSimex = 540f, dimYSimex = 7f;
        public bool FacturaExportacionSimex(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {
            bool sucess = false;
            bool esLocal = Helpers.Simex.EsLocal;
            string Ruta = string.Empty;
            string RutaImg = string.Empty;
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            if (esLocal)
            {
                Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
                //RutaImg = RulesServicesDIAN2.Properties.Settings.Default.UrlFE + "/images/EMPRESAS/Empresa_/" + NIT + "/Logo_" + NIT + ".png";
            }
            else
            {
                Ruta = HttpContext.Current.Server.MapPath("~/PDF/" + NIT + "/");
                RutaImg = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";
            }
            NomArchivo = string.Empty;
            NomArchivo = NombreInvoice + ".pdf";

            try
            {
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.

                //Document document = new Document(iTextSharp.text.PageSize.LETTER);
                Document document = Helpers.Simex.Documento;

                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;

                PdfPTable Encabezado = Simex.Exportacion.Encabezado(DsInvoiceAR, QRInvoice, RutaImg, esLocal);
                PdfPTable DatosCliente = Simex.Exportacion.DatosCliente(DsInvoiceAR);
                PdfPTable DetalleHead = Simex.Exportacion.DetalleHead();
                PdfPTable Mercaderia = Simex.Exportacion.Mercaderia(DsInvoiceAR);
                PdfPTable PiePagina = Simex.Exportacion.PiePagina(DsInvoiceAR, CUFE);

                writer.PageEvent = new Helpers.Simex.EventPageSimex(Encabezado, DatosCliente, DetalleHead, Mercaderia, PiePagina);
                // step 3: we open the document     
                document.Open();
                //----Detalles------
                document.Add(Simex.Exportacion.Detalles(DsInvoiceAR));
                /*PIE DE PAGINA*/
                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();

                Helpers.Simex.AddPageNumberPagToSimexExp($"{Ruta}{NomArchivo}", dimxSimex, dimYSimex + 10, Helpers.Fuentes.SimexLeyenda);
                sucess = true;
                RutaPdf = NomArchivo;
            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.ToString();
                sucess = false;
            }
            return sucess;
        }
        public bool FacturaNacionalSimex(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {
            bool sucess = false;
            bool esLocal = Helpers.Simex.EsLocal;
            string Ruta = string.Empty;
            string RutaImg = string.Empty;
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            if (esLocal)
            {
                Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
                //RutaImg = RulesServicesDIAN2.Properties.Settings.Default.UrlFE + "/images/EMPRESAS/Empresa_/" + NIT + "/Logo_" + NIT + ".png";
            }
            else
            {
                Ruta = HttpContext.Current.Server.MapPath("~/PDF/" + NIT + "/");
                RutaImg = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";
            }
            NomArchivo = string.Empty;
            NomArchivo = NombreInvoice + ".pdf";

            try
            {
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.

                //Document document = new Document(iTextSharp.text.PageSize.LETTER);
                Document document = Helpers.Simex.Documento;

                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;
                //writer.PageEvent = new Helpers.Simex.EventPageSimex(Simex.Nacional.PiePagina(DsInvoiceAR, CUFE));
                PdfPTable Encabezado = Simex.Nacional.Encabezado(DsInvoiceAR, QRInvoice, RutaImg, esLocal);
                PdfPTable DatosCliente = Simex.Nacional.DatosCliente(DsInvoiceAR);
                PdfPTable DetalleHead = Simex.Nacional.DetalleHead();
                PdfPTable Mercaderia = Simex.Nacional.Mercaderia(DsInvoiceAR);
                PdfPTable PiePagina = Simex.Nacional.PiePagina(DsInvoiceAR, CUFE);

                writer.PageEvent = new Helpers.Simex.EventPageSimex(Encabezado, DatosCliente, DetalleHead, Mercaderia, PiePagina);
                // step 3: we open the document     
                document.Open();

                //document.Add(Simex.Nacional.Encabezado(DsInvoiceAR, QRInvoice, RutaImg, esLocal));
                //document.Add(Simex.Nacional.DatosCliente(DsInvoiceAR));
                document.Add(Simex.Nacional.Detalles(DsInvoiceAR));
                //document.Add(Simex.Nacional.Mercaderia(DsInvoiceAR));
                //document.Add(Simex.Nacional.PiePagina(DsInvoiceAR, CUFE));
                //document.Add(Simex.Exportacion.PiePagina(DsInvoiceAR, CUFE));
                /*PIE DE PAGINA*/
                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();

                Helpers.Simex.AddPageNumberPagToSimexExp($"{Ruta}{NomArchivo}", dimxSimex, dimYSimex + 10, Helpers.Fuentes.SimexLeyenda);
                sucess = true;
                RutaPdf = NomArchivo;
            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.ToString();
                sucess = false;
            }
            return sucess;
        }
        public bool NotaCreditoSimex(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {
            bool sucess = false;
            bool esLocal = Helpers.Simex.EsLocal;
            string Ruta = string.Empty;
            string RutaImg = string.Empty;
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            if (esLocal)
            {
                Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
                //RutaImg = RulesServicesDIAN2.Properties.Settings.Default.UrlFE + "/images/EMPRESAS/Empresa_/" + NIT + "/Logo_" + NIT + ".png";
            }
            else
            {
                Ruta = HttpContext.Current.Server.MapPath("~/PDF/" + NIT + "/");
                RutaImg = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";
            }
            NomArchivo = string.Empty;
            NomArchivo = NombreInvoice + ".pdf";

            try
            {
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.

                //Document document = new Document(iTextSharp.text.PageSize.LETTER);
                Document document = Helpers.Simex.Documento;

                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;

                PdfPTable Encabezado = Simex.Credito.Encabezado(DsInvoiceAR, QRInvoice, RutaImg, esLocal);
                PdfPTable DatosCliente = Simex.Credito.DatosCliente(DsInvoiceAR);
                PdfPTable DetalleHead = Simex.Credito.DetalleHead();
                PdfPTable Mercaderia = Simex.Credito.Mercaderia(DsInvoiceAR);
                PdfPTable PiePagina = Simex.Credito.PiePagina(DsInvoiceAR, CUFE);

                writer.PageEvent = new Helpers.Simex.EventPageSimex(Encabezado, DatosCliente, DetalleHead, Mercaderia, PiePagina);
                // step 3: we open the document     
                document.Open();
                //----Detalles -----------------
                document.Add(Simex.Credito.Detalles(DsInvoiceAR));
                //document.Add(Simex.Exportacion.PiePagina(DsInvoiceAR, CUFE));
                /*PIE DE PAGINA*/
                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();

                Helpers.Simex.AddPageNumberPagToSimexExp($"{Ruta}{NomArchivo}", dimxSimex, dimYSimex + 10, Helpers.Fuentes.SimexLeyenda);
                sucess = true;
                RutaPdf = NomArchivo;
            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.ToString();
                sucess = false;
            }
            return sucess;
        }
        public bool NotaDebitoSimex(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {
            bool sucess = false;
            bool esLocal = Helpers.Simex.EsLocal;
            string Ruta = string.Empty;
            string RutaImg = string.Empty;
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            if (esLocal)
            {
                Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
                //RutaImg = RulesServicesDIAN2.Properties.Settings.Default.UrlFE + "/images/EMPRESAS/Empresa_/" + NIT + "/Logo_" + NIT + ".png";
            }
            else
            {
                Ruta = HttpContext.Current.Server.MapPath("~/PDF/" + NIT + "/");
                RutaImg = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";
            }
            NomArchivo = string.Empty;
            NomArchivo = NombreInvoice + ".pdf";

            try
            {
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.

                //Document document = new Document(iTextSharp.text.PageSize.LETTER);
                Document document = Helpers.Simex.Documento;

                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;

                PdfPTable Encabezado = Simex.Debito.Encabezado(DsInvoiceAR, QRInvoice, RutaImg, esLocal);
                PdfPTable DatosCliente = Simex.Debito.DatosCliente(DsInvoiceAR);
                PdfPTable DetalleHead = Simex.Debito.DetalleHead();
                PdfPTable Mercaderia = Simex.Debito.Mercaderia(DsInvoiceAR);
                PdfPTable PiePagina = Simex.Debito.PiePagina(DsInvoiceAR, CUFE);

                writer.PageEvent = new Helpers.Simex.EventPageSimex(Encabezado, DatosCliente, DetalleHead, Mercaderia, PiePagina);
                // step 3: we open the document     
                document.Open();

                //---Detalles
                document.Add(Simex.Debito.Detalles(DsInvoiceAR));
                //document.Add(Simex.Exportacion.PiePagina(DsInvoiceAR, CUFE));
                /*PIE DE PAGINA*/
                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();

                Helpers.Simex.AddPageNumberPagToSimexExp($"{Ruta}{NomArchivo}", dimxSimex, dimYSimex + 10, Helpers.Fuentes.SimexLeyenda);
                sucess = true;
                RutaPdf = NomArchivo;
            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.ToString();
                sucess = false;
            }
            return sucess;
        }
        public bool FacturaNacionalPlastinovo(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {
            bool sucess = false;
            bool esLocal = Helpers.Simex.EsLocal;
            string Ruta = string.Empty;
            string RutaImg = string.Empty;
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            if (esLocal)
            {
                Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
                //RutaImg = RulesServicesDIAN2.Properties.Settings.Default.UrlFE + "/images/EMPRESAS/Empresa_/" + NIT + "/Logo_" + NIT + ".png";
            }
            else
            {
                Ruta = HttpContext.Current.Server.MapPath("~/PDF/" + NIT + "/");
                RutaImg = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";
            }
            NomArchivo = string.Empty;
            NomArchivo = NombreInvoice + ".pdf";

            try
            {
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.

                //Document document = new Document(iTextSharp.text.PageSize.LETTER);
                Document document = Helpers.Simex.Documento;

                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;

                PdfPTable Encabezado = Plastinovo.Nacional.Encabezado(DsInvoiceAR, QRInvoice, RutaImg, esLocal);
                PdfPTable DatosCliente = Plastinovo.Nacional.DatosCliente(DsInvoiceAR);
                PdfPTable DetalleHead = Plastinovo.Nacional.DetalleHead();
                PdfPTable Mercaderia = Plastinovo.Nacional.Mercaderia(DsInvoiceAR);
                PdfPTable PiePagina = Plastinovo.Nacional.PiePagina(DsInvoiceAR, CUFE);

                writer.PageEvent = new Helpers.Simex.EventPageSimex(Encabezado, DatosCliente, DetalleHead, Mercaderia, PiePagina);
                // step 3: we open the document     
                document.Open();
                //----- Detalles ------------
                document.Add(Plastinovo.Nacional.Detalles(DsInvoiceAR));
                //document.Add(Simex.Exportacion.PiePagina(DsInvoiceAR, CUFE));
                /*PIE DE PAGINA*/
                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();

                Helpers.Simex.AddPageNumberPagToSimexExp($"{Ruta}{NomArchivo}", dimxSimex, dimYSimex + 10, Helpers.Fuentes.SimexLeyenda);
                sucess = true;
                RutaPdf = NomArchivo;
            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.ToString();
                sucess = false;
            }
            return sucess;
        }
        public bool FacturaExportacionPlastinovo(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {
            bool sucess = false;
            bool esLocal = Helpers.Simex.EsLocal;
            string Ruta = string.Empty;
            string RutaImg = string.Empty;
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            if (esLocal)
            {
                Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
                //RutaImg = RulesServicesDIAN2.Properties.Settings.Default.UrlFE + "/images/EMPRESAS/Empresa_/" + NIT + "/Logo_" + NIT + ".png";
            }
            else
            {
                Ruta = HttpContext.Current.Server.MapPath("~/PDF/" + NIT + "/");
                RutaImg = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";
            }
            NomArchivo = string.Empty;
            NomArchivo = NombreInvoice + ".pdf";

            try
            {
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.

                //Document document = new Document(iTextSharp.text.PageSize.LETTER);
                Document document = Helpers.Simex.Documento;

                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;

                PdfPTable Encabezado = Plastinovo.Exportacion.Encabezado(DsInvoiceAR, QRInvoice, RutaImg, esLocal);
                PdfPTable DatosCliente = Plastinovo.Exportacion.DatosCliente(DsInvoiceAR);
                PdfPTable DetalleHead = Plastinovo.Exportacion.DetalleHead();
                PdfPTable Mercaderia = Plastinovo.Exportacion.Mercaderia(DsInvoiceAR);
                PdfPTable PiePagina = Plastinovo.Exportacion.PiePagina(DsInvoiceAR, CUFE);

                writer.PageEvent = new Helpers.Simex.EventPageSimex(Encabezado, DatosCliente, DetalleHead, Mercaderia, PiePagina);
                // step 3: we open the document     
                document.Open();

                document.Add(Plastinovo.Exportacion.Detalles(DsInvoiceAR));
                /*PIE DE PAGINA*/
                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();

                Helpers.Simex.AddPageNumberPagToSimexExp($"{Ruta}{NomArchivo}", dimxSimex, dimYSimex + 10, Helpers.Fuentes.SimexLeyenda);
                sucess = true;
                RutaPdf = NomArchivo;
            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.ToString();
                sucess = false;
            }
            return sucess;
        }
        public bool NotaCreditoPlastinovo(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {
            bool sucess = false;
            bool esLocal = Helpers.Simex.EsLocal;
            string Ruta = string.Empty;
            string RutaImg = string.Empty;
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            if (esLocal)
            {
                Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
                //RutaImg = RulesServicesDIAN2.Properties.Settings.Default.UrlFE + "/images/EMPRESAS/Empresa_/" + NIT + "/Logo_" + NIT + ".png";
            }
            else
            {
                Ruta = HttpContext.Current.Server.MapPath("~/PDF/" + NIT + "/");
                RutaImg = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";
            }
            NomArchivo = string.Empty;
            NomArchivo = NombreInvoice + ".pdf";

            try
            {
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.

                //Document document = new Document(iTextSharp.text.PageSize.LETTER);
                Document document = Helpers.Simex.Documento;

                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;

                PdfPTable Encabezado = Plastinovo.Credito.Encabezado(DsInvoiceAR, QRInvoice, RutaImg, esLocal);
                PdfPTable DatosCliente = Plastinovo.Credito.DatosCliente(DsInvoiceAR);
                PdfPTable DetalleHead = Plastinovo.Credito.DetalleHead();
                PdfPTable Mercaderia = Plastinovo.Credito.Mercaderia(DsInvoiceAR);
                PdfPTable PiePagina = Plastinovo.Credito.PiePagina(DsInvoiceAR, CUFE);

                writer.PageEvent = new Helpers.Simex.EventPageSimex(Encabezado, DatosCliente, DetalleHead, Mercaderia, PiePagina);
                // step 3: we open the document     
                document.Open();

                document.Add(Plastinovo.Credito.Detalles(DsInvoiceAR));
                //document.Add(Simex.Exportacion.PiePagina(DsInvoiceAR, CUFE));
                /*PIE DE PAGINA*/
                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();

                Helpers.Simex.AddPageNumberPagToSimexExp($"{Ruta}{NomArchivo}", dimxSimex, dimYSimex + 10, Helpers.Fuentes.SimexLeyenda);
                sucess = true;
                RutaPdf = NomArchivo;
            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.ToString();
                sucess = false;
            }
            return sucess;
        }
        public bool NotaDebitoPlastinovo(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {
            bool sucess = false;
            bool esLocal = Helpers.Simex.EsLocal;
            string Ruta = string.Empty;
            string RutaImg = string.Empty;
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            if (esLocal)
            {
                Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
                //RutaImg = RulesServicesDIAN2.Properties.Settings.Default.UrlFE + "/images/EMPRESAS/Empresa_/" + NIT + "/Logo_" + NIT + ".png";
            }
            else
            {
                Ruta = HttpContext.Current.Server.MapPath("~/PDF/" + NIT + "/");
                RutaImg = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";
            }
            NomArchivo = string.Empty;
            NomArchivo = NombreInvoice + ".pdf";

            try
            {
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.

                //Document document = new Document(iTextSharp.text.PageSize.LETTER);
                Document document = Helpers.Simex.Documento;

                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;

                PdfPTable Encabezado = Plastinovo.Debito.Encabezado(DsInvoiceAR, QRInvoice, RutaImg, esLocal);
                PdfPTable DatosCliente = Plastinovo.Debito.DatosCliente(DsInvoiceAR);
                PdfPTable DetalleHead = Plastinovo.Debito.DetalleHead();
                PdfPTable Mercaderia = Plastinovo.Debito.Mercaderia(DsInvoiceAR);
                PdfPTable PiePagina = Plastinovo.Debito.PiePagina(DsInvoiceAR, CUFE);

                writer.PageEvent = new Helpers.Simex.EventPageSimex(Encabezado, DatosCliente, DetalleHead, Mercaderia, PiePagina);
                // step 3: we open the document     
                document.Open();

                document.Add(Plastinovo.Debito.Detalles(DsInvoiceAR));
                //document.Add(Simex.Exportacion.PiePagina(DsInvoiceAR, CUFE));
                /*PIE DE PAGINA*/
                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();

                Helpers.Simex.AddPageNumberPagToSimexExp($"{Ruta}{NomArchivo}", dimxSimex, dimYSimex + 10, Helpers.Fuentes.SimexLeyenda);
                sucess = true;
                RutaPdf = NomArchivo;
            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.ToString();
                sucess = false;
            }
            return sucess;
        }
        public bool FacturaExportacionSoinco(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {
            bool sucess = false;
            bool esLocal = Helpers.Simex.EsLocal;
            string Ruta = string.Empty;
            string RutaImg = string.Empty;
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            if (esLocal)
            {
                Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
                //RutaImg = RulesServicesDIAN2.Properties.Settings.Default.UrlFE + "/images/EMPRESAS/Empresa_/" + NIT + "/Logo_" + NIT + ".png";
            }
            else
            {
                Ruta = HttpContext.Current.Server.MapPath("~/PDF/" + NIT + "/");
                RutaImg = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";
            }
            NomArchivo = string.Empty;
            NomArchivo = NombreInvoice + ".pdf";

            try
            {
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.

                //Document document = new Document(iTextSharp.text.PageSize.LETTER);
                Document document = Helpers.Simex.Documento;

                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;

                PdfPTable Encabezado = Soinco.Exportacion.Encabezado(DsInvoiceAR, QRInvoice, RutaImg, esLocal);
                PdfPTable DatosCliente = Soinco.Exportacion.DatosCliente(DsInvoiceAR);
                PdfPTable DetalleHead = Soinco.Exportacion.DetalleHead();
                PdfPTable Mercaderia = Soinco.Exportacion.Mercaderia(DsInvoiceAR);
                PdfPTable PiePagina = Soinco.Exportacion.PiePagina(DsInvoiceAR, CUFE);

                writer.PageEvent = new Helpers.Simex.EventPageSimex(Encabezado, DatosCliente, DetalleHead, Mercaderia, PiePagina);
                // step 3: we open the document     
                document.Open();

                document.Add(Soinco.Exportacion.Detalles(DsInvoiceAR));
                //document.Add(Simex.Exportacion.PiePagina(DsInvoiceAR, CUFE));
                /*PIE DE PAGINA*/
                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();

                Helpers.Simex.AddPageNumberPagToSimexExp($"{Ruta}{NomArchivo}", dimxSimex, dimYSimex + 10, Helpers.Fuentes.SimexLeyenda);
                sucess = true;
                RutaPdf = NomArchivo;
            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.ToString();
                sucess = false;
            }
            return sucess;
        }
        public bool FacturaNacionalSoinco(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {
            bool sucess = false;
            bool esLocal = Helpers.Simex.EsLocal;
            string Ruta = string.Empty;
            string RutaImg = string.Empty;
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            if (esLocal)
            {
                Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
                //RutaImg = RulesServicesDIAN2.Properties.Settings.Default.UrlFE + "/images/EMPRESAS/Empresa_/" + NIT + "/Logo_" + NIT + ".png";
            }
            else
            {
                Ruta = HttpContext.Current.Server.MapPath("~/PDF/" + NIT + "/");
                RutaImg = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";
            }
            NomArchivo = string.Empty;
            NomArchivo = NombreInvoice + ".pdf";

            try
            {
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.

                //Document document = new Document(iTextSharp.text.PageSize.LETTER);
                Document document = Helpers.Simex.Documento;

                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;

                PdfPTable Encabezado = Soinco.Nacional.Encabezado(DsInvoiceAR, QRInvoice, RutaImg, esLocal);
                PdfPTable DatosCliente = Soinco.Nacional.DatosCliente(DsInvoiceAR);
                PdfPTable DetalleHead = Soinco.Nacional.DetalleHead();
                PdfPTable Mercaderia = Soinco.Nacional.Mercaderia(DsInvoiceAR);
                PdfPTable PiePagina = Soinco.Nacional.PiePagina(DsInvoiceAR, CUFE);

                writer.PageEvent = new Helpers.Simex.EventPageSimex(Encabezado, DatosCliente, DetalleHead, Mercaderia, PiePagina);
                // step 3: we open the document     
                document.Open();

                document.Add(Soinco.Nacional.Detalles(DsInvoiceAR));
                //document.Add(Simex.Exportacion.PiePagina(DsInvoiceAR, CUFE));
                /*PIE DE PAGINA*/
                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();

                Helpers.Simex.AddPageNumberPagToSimexExp($"{Ruta}{NomArchivo}", dimxSimex, dimYSimex + 10, Helpers.Fuentes.SimexLeyenda);
                sucess = true;
                RutaPdf = NomArchivo;
            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.ToString();
                sucess = false;
            }
            return sucess;
        }
        public bool NotaCreditoSoinco(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {
            bool sucess = false;
            bool esLocal = Helpers.Simex.EsLocal;
            string Ruta = string.Empty;
            string RutaImg = string.Empty;
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            if (esLocal)
            {
                Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
                //RutaImg = RulesServicesDIAN2.Properties.Settings.Default.UrlFE + "/images/EMPRESAS/Empresa_/" + NIT + "/Logo_" + NIT + ".png";
            }
            else
            {
                Ruta = HttpContext.Current.Server.MapPath("~/PDF/" + NIT + "/");
                RutaImg = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";
            }
            NomArchivo = string.Empty;
            NomArchivo = NombreInvoice + ".pdf";

            try
            {
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.

                //Document document = new Document(iTextSharp.text.PageSize.LETTER);
                Document document = Helpers.Simex.Documento;

                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;

                PdfPTable Encabezado = Soinco.Credito.Encabezado(DsInvoiceAR, QRInvoice, RutaImg, esLocal);
                PdfPTable DatosCliente = Soinco.Credito.DatosCliente(DsInvoiceAR);
                PdfPTable DetalleHead = Soinco.Credito.DetalleHead();
                PdfPTable Mercaderia = Soinco.Credito.Mercaderia(DsInvoiceAR);
                PdfPTable PiePagina = Soinco.Credito.PiePagina(DsInvoiceAR, CUFE);

                writer.PageEvent = new Helpers.Simex.EventPageSimex(Encabezado, DatosCliente, DetalleHead, Mercaderia, PiePagina);
                // step 3: we open the document     
                document.Open();

                document.Add(Soinco.Credito.Detalles(DsInvoiceAR));
                //document.Add(Simex.Exportacion.PiePagina(DsInvoiceAR, CUFE));
                /*PIE DE PAGINA*/
                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();

                Helpers.Simex.AddPageNumberPagToSimexExp($"{Ruta}{NomArchivo}", dimxSimex, dimYSimex + 10, Helpers.Fuentes.SimexLeyenda);
                sucess = true;
                RutaPdf = NomArchivo;
            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.ToString();
                sucess = false;
            }
            return sucess;
        }
        public bool NotaDebitoSoinco(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {
            bool sucess = false;
            bool esLocal = Helpers.Simex.EsLocal;
            string Ruta = string.Empty;
            string RutaImg = string.Empty;
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            if (esLocal)
            {
                Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
                //RutaImg = RulesServicesDIAN2.Properties.Settings.Default.UrlFE + "/images/EMPRESAS/Empresa_/" + NIT + "/Logo_" + NIT + ".png";
            }
            else
            {
                Ruta = HttpContext.Current.Server.MapPath("~/PDF/" + NIT + "/");
                RutaImg = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";
            }
            NomArchivo = string.Empty;
            NomArchivo = NombreInvoice + ".pdf";

            try
            {
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.

                //Document document = new Document(iTextSharp.text.PageSize.LETTER);
                Document document = Helpers.Simex.Documento;

                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;

                PdfPTable Encabezado = Soinco.Debito.Encabezado(DsInvoiceAR, QRInvoice, RutaImg, esLocal);
                PdfPTable DatosCliente = Soinco.Debito.DatosCliente(DsInvoiceAR);
                PdfPTable DetalleHead = Soinco.Debito.DetalleHead();
                PdfPTable Mercaderia = Soinco.Debito.Mercaderia(DsInvoiceAR);
                PdfPTable PiePagina = Soinco.Debito.PiePagina(DsInvoiceAR, CUFE);

                writer.PageEvent = new Helpers.Simex.EventPageSimex(Encabezado, DatosCliente, DetalleHead, Mercaderia, PiePagina);
                // step 3: we open the document     
                document.Open();

                document.Add(Soinco.Debito.Detalles(DsInvoiceAR));
                //document.Add(Simex.Exportacion.PiePagina(DsInvoiceAR, CUFE));
                /*PIE DE PAGINA*/
                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();

                Helpers.Simex.AddPageNumberPagToSimexExp($"{Ruta}{NomArchivo}", dimxSimex, dimYSimex + 10, Helpers.Fuentes.SimexLeyenda);
                sucess = true;
                RutaPdf = NomArchivo;
            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.ToString();
                sucess = false;
            }
            return sucess;
        }

        public class Simex
        {
            public class Nacional
            {
                public static PdfPTable Encabezado(DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string Logo, bool Local)
                {
                    return Helpers.Simex.Encabezado(DsInvoiceAR, QRInvoice, Helpers.Simex.Empresa.SIMEX, Helpers.Simex.TipoDocumento.FacturaNacional, Logo, Local);
                }
                public static PdfPTable DatosCliente(DataSet ds)
                {
                    return Helpers.Simex.DatosCliente(ds, Helpers.Simex.TipoDocumento.FacturaNacional);
                }
                public static PdfPTable DetalleHead()
                {
                    return Helpers.Simex.DetallesHead(Helpers.Simex.TipoDocumento.FacturaNacional);
                }
                public static PdfPTable Detalles(DataSet ds)
                {
                    return Helpers.Simex.Detalles(ds, Helpers.Simex.TipoDocumento.FacturaNacional, Helpers.Simex.Empresa.SIMEX);
                }
                public static PdfPTable Mercaderia(DataSet ds)
                {
                    return Helpers.Simex.Mercaderia(ds, Helpers.Simex.TipoDocumento.FacturaNacional);
                }
                public static PdfPTable PiePagina(DataSet ds, string CUFE)
                {
                    return Helpers.Simex.PiePagina(ds, CUFE, Helpers.Simex.TipoDocumento.FacturaNacional, Helpers.Simex.Empresa.SOINCO);
                }
            }
            public class Exportacion
            {
                public static PdfPTable Encabezado(DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string Logo, bool Local)
                {
                    return Helpers.Simex.Encabezado(DsInvoiceAR, QRInvoice, Helpers.Simex.Empresa.SIMEX, Helpers.Simex.TipoDocumento.FacturaExportacion, Logo, Local);
                }
                public static PdfPTable DatosCliente(DataSet ds)
                {
                    return Helpers.Simex.DatosCliente(ds, Helpers.Simex.TipoDocumento.FacturaExportacion);
                }
                public static PdfPTable DetalleHead()
                {
                    return Helpers.Simex.DetallesHead(Helpers.Simex.TipoDocumento.FacturaExportacion);
                }
                public static PdfPTable Detalles(DataSet ds)
                {
                    return Helpers.Simex.Detalles(ds, Helpers.Simex.TipoDocumento.FacturaExportacion, Helpers.Simex.Empresa.SIMEX);
                }
                public static PdfPTable Mercaderia(DataSet ds)
                {
                    return Helpers.Simex.Mercaderia(ds, Helpers.Simex.TipoDocumento.FacturaExportacion);
                }
                public static PdfPTable PiePagina(DataSet ds, string CUFE)
                {
                    return Helpers.Simex.PiePagina(ds, CUFE, Helpers.Simex.TipoDocumento.FacturaExportacion, Helpers.Simex.Empresa.SOINCO);
                }
            }
            public class Credito
            {
                public static PdfPTable Encabezado(DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string Logo, bool Local)
                {
                    return Helpers.Simex.Encabezado(DsInvoiceAR, QRInvoice, Helpers.Simex.Empresa.SIMEX, Helpers.Simex.TipoDocumento.NotaCredito, Logo, Local);
                }
                public static PdfPTable DatosCliente(DataSet ds)
                {
                    return Helpers.Simex.DatosCliente(ds, Helpers.Simex.TipoDocumento.NotaCredito);
                }
                public static PdfPTable DetalleHead()
                {
                    return Helpers.Simex.DetallesHead(Helpers.Simex.TipoDocumento.NotaCredito);
                }
                public static PdfPTable Detalles(DataSet ds)
                {
                    return Helpers.Simex.Detalles(ds, Helpers.Simex.TipoDocumento.NotaCredito, Helpers.Simex.Empresa.SIMEX);
                }
                public static PdfPTable Mercaderia(DataSet ds)
                {
                    return Helpers.Simex.Mercaderia(ds, Helpers.Simex.TipoDocumento.NotaCredito);
                }
                public static PdfPTable PiePagina(DataSet ds, string CUFE)
                {
                    return Helpers.Simex.PiePagina(ds, CUFE, Helpers.Simex.TipoDocumento.NotaCredito, Helpers.Simex.Empresa.SOINCO);
                }
            }
            public class Debito

            {
                public static PdfPTable Encabezado(DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string Logo, bool Local)
                {
                    return Helpers.Simex.Encabezado(DsInvoiceAR, QRInvoice, Helpers.Simex.Empresa.SIMEX, Helpers.Simex.TipoDocumento.NotaDebito, Logo, Local);
                }
                public static PdfPTable DatosCliente(DataSet ds)
                {
                    return Helpers.Simex.DatosCliente(ds, Helpers.Simex.TipoDocumento.NotaDebito);
                }
                public static PdfPTable DetalleHead()
                {
                    return Helpers.Simex.DetallesHead(Helpers.Simex.TipoDocumento.NotaDebito);
                }
                public static PdfPTable Detalles(DataSet ds)
                {
                    return Helpers.Simex.Detalles(ds, Helpers.Simex.TipoDocumento.NotaDebito, Helpers.Simex.Empresa.SIMEX);
                }
                public static PdfPTable Mercaderia(DataSet ds)
                {
                    return Helpers.Simex.Mercaderia(ds, Helpers.Simex.TipoDocumento.NotaDebito);
                }
                public static PdfPTable PiePagina(DataSet ds, string CUFE)
                {
                    return Helpers.Simex.PiePagina(ds, CUFE, Helpers.Simex.TipoDocumento.NotaDebito, Helpers.Simex.Empresa.SOINCO);
                }
            }
        }
        public class Plastinovo
        {
            public class Nacional
            {
                public static PdfPTable Encabezado(DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string Logo, bool Local)
                {
                    return Helpers.Simex.Encabezado(DsInvoiceAR, QRInvoice, Helpers.Simex.Empresa.PLASTINOVO, Helpers.Simex.TipoDocumento.FacturaNacional, Logo, Local);
                }
                public static PdfPTable DatosCliente(DataSet ds)
                {
                    return Helpers.Simex.DatosCliente(ds, Helpers.Simex.TipoDocumento.FacturaNacional);
                }
                public static PdfPTable DetalleHead()
                {
                    return Helpers.Simex.DetallesHead(Helpers.Simex.TipoDocumento.FacturaNacional);
                }
                public static PdfPTable Detalles(DataSet ds)
                {
                    return Helpers.Simex.Detalles(ds, Helpers.Simex.TipoDocumento.FacturaNacional, Helpers.Simex.Empresa.PLASTINOVO);
                }
                public static PdfPTable Mercaderia(DataSet ds)
                {
                    return Helpers.Simex.Mercaderia(ds, Helpers.Simex.TipoDocumento.FacturaNacional);
                }
                public static PdfPTable PiePagina(DataSet ds, string CUFE)
                {
                    return Helpers.Simex.PiePagina(ds, CUFE, Helpers.Simex.TipoDocumento.FacturaNacional, Helpers.Simex.Empresa.SOINCO);
                }
            }
            public class Exportacion
            {
                public static PdfPTable Encabezado(DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string Logo, bool Local)
                {
                    return Helpers.Simex.Encabezado(DsInvoiceAR, QRInvoice, Helpers.Simex.Empresa.PLASTINOVO, Helpers.Simex.TipoDocumento.FacturaExportacion, Logo, Local);
                }
                public static PdfPTable DatosCliente(DataSet ds)
                {
                    return Helpers.Simex.DatosCliente(ds, Helpers.Simex.TipoDocumento.FacturaExportacion);
                }
                public static PdfPTable DetalleHead()
                {
                    return Helpers.Simex.DetallesHead(Helpers.Simex.TipoDocumento.FacturaExportacion);
                }
                public static PdfPTable Detalles(DataSet ds)
                {
                    return Helpers.Simex.Detalles(ds, Helpers.Simex.TipoDocumento.FacturaExportacion, Helpers.Simex.Empresa.PLASTINOVO);
                }
                public static PdfPTable Mercaderia(DataSet ds)
                {
                    return Helpers.Simex.Mercaderia(ds, Helpers.Simex.TipoDocumento.FacturaExportacion);
                }
                public static PdfPTable PiePagina(DataSet ds, string CUFE)
                {
                    return Helpers.Simex.PiePagina(ds, CUFE, Helpers.Simex.TipoDocumento.FacturaExportacion, Helpers.Simex.Empresa.SOINCO);
                }
            }
            public class Credito
            {
                public static PdfPTable Encabezado(DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string Logo, bool Local)
                {
                    return Helpers.Simex.Encabezado(DsInvoiceAR, QRInvoice, Helpers.Simex.Empresa.PLASTINOVO, Helpers.Simex.TipoDocumento.NotaCredito, Logo, Local);
                }
                public static PdfPTable DatosCliente(DataSet ds)
                {
                    return Helpers.Simex.DatosCliente(ds, Helpers.Simex.TipoDocumento.NotaCredito);
                }
                public static PdfPTable DetalleHead()
                {
                    return Helpers.Simex.DetallesHead(Helpers.Simex.TipoDocumento.NotaCredito);
                }
                public static PdfPTable Detalles(DataSet ds)
                {
                    return Helpers.Simex.Detalles(ds, Helpers.Simex.TipoDocumento.NotaCredito, Helpers.Simex.Empresa.PLASTINOVO);
                }
                public static PdfPTable Mercaderia(DataSet ds)
                {
                    return Helpers.Simex.Mercaderia(ds, Helpers.Simex.TipoDocumento.NotaCredito);
                }
                public static PdfPTable PiePagina(DataSet ds, string CUFE)
                {
                    return Helpers.Simex.PiePagina(ds, CUFE, Helpers.Simex.TipoDocumento.NotaCredito, Helpers.Simex.Empresa.SOINCO);
                }
            }
            public class Debito

            {
                public static PdfPTable Encabezado(DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string Logo, bool Local)
                {
                    return Helpers.Simex.Encabezado(DsInvoiceAR, QRInvoice, Helpers.Simex.Empresa.PLASTINOVO, Helpers.Simex.TipoDocumento.NotaDebito, Logo, Local);
                }
                public static PdfPTable DatosCliente(DataSet ds)
                {
                    return Helpers.Simex.DatosCliente(ds, Helpers.Simex.TipoDocumento.NotaDebito);
                }
                public static PdfPTable DetalleHead()
                {
                    return Helpers.Simex.DetallesHead(Helpers.Simex.TipoDocumento.NotaDebito);
                }
                public static PdfPTable Detalles(DataSet ds)
                {
                    return Helpers.Simex.Detalles(ds, Helpers.Simex.TipoDocumento.NotaDebito, Helpers.Simex.Empresa.PLASTINOVO);
                }
                public static PdfPTable Mercaderia(DataSet ds)
                {
                    return Helpers.Simex.Mercaderia(ds, Helpers.Simex.TipoDocumento.NotaDebito);
                }
                public static PdfPTable PiePagina(DataSet ds, string CUFE)
                {
                    return Helpers.Simex.PiePagina(ds, CUFE, Helpers.Simex.TipoDocumento.NotaDebito, Helpers.Simex.Empresa.SOINCO);
                }
            }
        }
        public class Soinco
        {
            public class Nacional
            {
                public static PdfPTable Encabezado(DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string Logo, bool Local)
                {
                    return Helpers.Simex.Encabezado(DsInvoiceAR, QRInvoice, Helpers.Simex.Empresa.SOINCO, Helpers.Simex.TipoDocumento.FacturaNacional, Logo, Local);
                }
                public static PdfPTable DatosCliente(DataSet ds)
                {
                    return Helpers.Simex.DatosCliente(ds, Helpers.Simex.TipoDocumento.FacturaNacional);
                }
                public static PdfPTable DetalleHead()
                {
                    return Helpers.Simex.DetallesHead(Helpers.Simex.TipoDocumento.FacturaNacional);
                }
                public static PdfPTable Detalles(DataSet ds)
                {
                    return Helpers.Simex.Detalles(ds, Helpers.Simex.TipoDocumento.FacturaNacional, Helpers.Simex.Empresa.SOINCO);
                }
                public static PdfPTable Mercaderia(DataSet ds)
                {
                    return Helpers.Simex.Mercaderia(ds, Helpers.Simex.TipoDocumento.FacturaNacional);
                }
                public static PdfPTable PiePagina(DataSet ds, string CUFE)
                {
                    return Helpers.Simex.PiePagina(ds, CUFE, Helpers.Simex.TipoDocumento.FacturaNacional, Helpers.Simex.Empresa.SOINCO);
                }
            }
            public class Exportacion
            {
                public static PdfPTable Encabezado(DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string Logo, bool Local)
                {
                    return Helpers.Simex.Encabezado(DsInvoiceAR, QRInvoice, Helpers.Simex.Empresa.SOINCO, Helpers.Simex.TipoDocumento.FacturaExportacion, Logo, Local);
                }
                public static PdfPTable DatosCliente(DataSet ds)
                {
                    return Helpers.Simex.DatosCliente(ds, Helpers.Simex.TipoDocumento.FacturaExportacion);
                }
                public static PdfPTable DetalleHead()
                {
                    return Helpers.Simex.DetallesHead(Helpers.Simex.TipoDocumento.FacturaExportacion);
                }
                public static PdfPTable Detalles(DataSet ds)
                {
                    return Helpers.Simex.Detalles(ds, Helpers.Simex.TipoDocumento.FacturaExportacion, Helpers.Simex.Empresa.SOINCO);
                }
                public static PdfPTable Mercaderia(DataSet ds)
                {
                    return Helpers.Simex.Mercaderia(ds, Helpers.Simex.TipoDocumento.FacturaExportacion);
                }
                public static PdfPTable PiePagina(DataSet ds, string CUFE)
                {
                    return Helpers.Simex.PiePagina(ds, CUFE, Helpers.Simex.TipoDocumento.FacturaExportacion, Helpers.Simex.Empresa.SOINCO);
                }
            }
            public class Credito
            {
                public static PdfPTable Encabezado(DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string Logo, bool Local)
                {
                    return Helpers.Simex.Encabezado(DsInvoiceAR, QRInvoice, Helpers.Simex.Empresa.SOINCO, Helpers.Simex.TipoDocumento.NotaCredito, Logo, Local);
                }
                public static PdfPTable DatosCliente(DataSet ds)
                {
                    return Helpers.Simex.DatosCliente(ds, Helpers.Simex.TipoDocumento.NotaCredito);
                }
                public static PdfPTable DetalleHead()
                {
                    return Helpers.Simex.DetallesHead(Helpers.Simex.TipoDocumento.NotaCredito);
                }
                public static PdfPTable Detalles(DataSet ds)
                {
                    return Helpers.Simex.Detalles(ds, Helpers.Simex.TipoDocumento.NotaCredito, Helpers.Simex.Empresa.SOINCO);
                }
                public static PdfPTable Mercaderia(DataSet ds)
                {
                    return Helpers.Simex.Mercaderia(ds, Helpers.Simex.TipoDocumento.NotaCredito);
                }
                public static PdfPTable PiePagina(DataSet ds, string CUFE)
                {
                    return Helpers.Simex.PiePagina(ds, CUFE, Helpers.Simex.TipoDocumento.NotaCredito, Helpers.Simex.Empresa.SOINCO);
                }
            }
            public class Debito

            {
                public static PdfPTable Encabezado(DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string Logo, bool Local)
                {
                    return Helpers.Simex.Encabezado(DsInvoiceAR, QRInvoice, Helpers.Simex.Empresa.SOINCO, Helpers.Simex.TipoDocumento.NotaDebito, Logo, Local);
                }
                public static PdfPTable DatosCliente(DataSet ds)
                {
                    return Helpers.Simex.DatosCliente(ds, Helpers.Simex.TipoDocumento.NotaDebito);
                }
                public static PdfPTable DetalleHead()
                {
                    return Helpers.Simex.DetallesHead(Helpers.Simex.TipoDocumento.NotaDebito);
                }
                public static PdfPTable Detalles(DataSet ds)
                {
                    return Helpers.Simex.Detalles(ds, Helpers.Simex.TipoDocumento.NotaDebito, Helpers.Simex.Empresa.SOINCO);
                }
                public static PdfPTable Mercaderia(DataSet ds)
                {
                    return Helpers.Simex.Mercaderia(ds, Helpers.Simex.TipoDocumento.NotaDebito);
                }
                public static PdfPTable PiePagina(DataSet ds, string CUFE)
                {
                    return Helpers.Simex.PiePagina(ds, CUFE, Helpers.Simex.TipoDocumento.NotaDebito, Helpers.Simex.Empresa.SOINCO);
                }
            }
        }

        #endregion
        #region "Metodos Privados"
        public partial class Helpers
        {
            public class Fuentes
            {
                public static iTextSharp.text.Font fontTitleBold
                {
                    get
                    {
                        return FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7.57f, iTextSharp.text.Font.NORMAL);
                    }
                }
                public static iTextSharp.text.Font fontTitle
                {
                    get
                    {
                        return FontFactory.GetFont(FontFactory.HELVETICA, 7.57f, iTextSharp.text.Font.NORMAL);
                    }
                }
                public static iTextSharp.text.Font fontTitleItalic
                {
                    get
                    {
                        return FontFactory.GetFont(FontFactory.HELVETICA, 7.57f, iTextSharp.text.Font.ITALIC | iTextSharp.text.Font.BOLD);
                    }
                }
                public static iTextSharp.text.Font fontTitleBold2
                {
                    get
                    {
                        return FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 8f, iTextSharp.text.Font.NORMAL);
                    }
                }
                public static iTextSharp.text.Font fontTitle2
                {
                    get
                    {
                        return FontFactory.GetFont(FontFactory.HELVETICA, 8f, iTextSharp.text.Font.NORMAL);
                    }
                }
                public static iTextSharp.text.Font fontCustom
                {
                    get
                    {
                        return FontFactory.GetFont(FontFactory.HELVETICA, 6f, iTextSharp.text.Font.NORMAL);
                    }
                }
                public static iTextSharp.text.Font fontCustomI
                {
                    get
                    {
                        return FontFactory.GetFont(FontFactory.HELVETICA_OBLIQUE, 6f, iTextSharp.text.Font.NORMAL);
                    }
                }
                public static iTextSharp.text.Font fontCustomBold
                {
                    get
                    {
                        return FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 6, iTextSharp.text.Font.NORMAL);
                    }
                }
                public static iTextSharp.text.Font fontTitleLittleBold
                {
                    get
                    {
                        return FontFactory.GetFont(FontFactory.HELVETICA, 6f, iTextSharp.text.Font.BOLD | iTextSharp.text.Font.ITALIC);
                    }
                }
                public static iTextSharp.text.Font SimexTituloEsp
                {
                    get
                    {
                        return FontFactory.GetFont("Arial", 7f, iTextSharp.text.Font.BOLD);
                    }
                }
                public static iTextSharp.text.Font SimexTituloIng
                {
                    get
                    {
                        return FontFactory.GetFont("Arial", 6f, iTextSharp.text.Font.ITALIC);
                    }
                }
                public static iTextSharp.text.Font SimexLeyenda
                {
                    get
                    {
                        return FontFactory.GetFont("Arial", 6f, iTextSharp.text.Font.NORMAL);
                    }
                }
                public static iTextSharp.text.Font SimexDatos
                {
                    get
                    {
                        return FontFactory.GetFont("Arial", 7f, iTextSharp.text.Font.NORMAL);
                    }
                }
                public static iTextSharp.text.Font fontTitleFactura
                {
                    get
                    {
                        return FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                    }
                }
                public static iTextSharp.text.Font Plantilla_6_fontTitle1
                {
                    get
                    {
                        return FontFactory.GetFont(FontFactory.HELVETICA, 5, iTextSharp.text.Font.NORMAL);
                    }
                }
                public static iTextSharp.text.Font Plantilla_6_fuenteDatos
                {
                    get
                    {
                        return FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);
                    }
                }
                public static iTextSharp.text.Font Plantilla_4_fontTitle
                {
                    get
                    {
                        return FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                    }
                }
                public static iTextSharp.text.Font Plantilla_4_fontTitle2
                {
                    get
                    {
                        return FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                    }
                }
                public static iTextSharp.text.Font Arial7Negrilla
                {
                    get
                    {
                        return FontFactory.GetFont("Arial", 7, iTextSharp.text.Font.BOLD);
                    }
                }
                public static iTextSharp.text.Font Plantilla_4_fontCustom
                {
                    get
                    {
                        return FontFactory.GetFont(FontFactory.HELVETICA, 5, iTextSharp.text.Font.NORMAL);
                    }
                }
                public static iTextSharp.text.Font Plantilla_4_fontTitleFactura
                {
                    get
                    {
                        return FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                    }
                }
                public static iTextSharp.text.Font Plantilla_2_fontTitle
                {
                    get
                    {
                        return FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                    }
                }
                public static iTextSharp.text.Font Plantilla_2_fontTitle2
                {
                    get
                    {
                        return FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                    }
                }
                public static iTextSharp.text.Font Plantilla_2_fontCustom
                {
                    get
                    {
                        return FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);
                    }
                }
                public static iTextSharp.text.Font Plantilla_2_fontTitleFactura
                {
                    get
                    {
                        return FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                    }
                }
                public static iTextSharp.text.Font Plantilla_2_fontEspacio
                {
                    get
                    {
                        return FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 0.3F, iTextSharp.text.Font.NORMAL);
                    }
                }
                public static iTextSharp.text.Font Arial10Normal
                {
                    get
                    {
                        return FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.NORMAL);
                    }
                }
                public static iTextSharp.text.Font Arial9Normal
                {
                    get
                    {
                        return FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL);
                    }
                }
                public static iTextSharp.text.Font Arial8Normal
                {
                    get
                    {
                        return FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL);
                    }
                }
            }
            public class Simex
            {
                public static bool EsLocal = true;
                public enum Empresa
                {
                    SIMEX,
                    PLASTINOVO,
                    SOINCO
                }
                public enum TipoDocumento
                {
                    FacturaNacional,
                    FacturaExportacion,
                    NotaCredito,
                    NotaDebito
                }
                public static Document Documento
                {
                    get
                    {
                        return new Document(iTextSharp.text.PageSize.LETTER, 20f, 15f, 330f, 310f);
                    }
                }
                public static string AjusteFechas(string Dato)
                {
                    string result = string.Empty;
                    string Fecha = string.Empty;
                    try
                    {
                        Fecha = Dato.Substring(0, 10);
                        string[] separado = Fecha.Split('-');
                        string año = separado[0];
                        string mes = separado[1];
                        string dia = separado[2];
                        result = $"{dia}/{mes}/{año}";

                    }
                    catch
                    {

                    }
                    return result;
                }
                public static void TitulosImpuestos(ref PdfPTable table, DataSet ds, Helpers.Simex.Empresa empresa)
                {
                    PdfPCell celTituloSubtotal = new PdfPCell();
                    Paragraph prgTituloSubtotal = new Paragraph("SUBTOTAL", Helpers.Fuentes.SimexDatos);
                    celTituloSubtotal.AddElement(prgTituloSubtotal);
                    foreach (DataRow item in ds.Tables["HedTaxSum"].Rows)
                    {
                        string TituloImp = string.Empty;
                        switch (item["TaxCode"].ToString())
                        {
                            case "IVA-AR":
                                string IVA = string.Empty;
                                ConvertirFormatoEntero(item["Percent"], out IVA);
                                TituloImp = string.Format("IVA {0}%", IVA);
                                Paragraph prgTituloImpuesto = new Paragraph(TituloImp, Helpers.Fuentes.SimexDatos);
                                celTituloSubtotal.AddElement(prgTituloImpuesto);
                                break;
                            case "ICA-AR":
                                break;
                            case "RET/FTE-CE":
                                break;
                            case "RET/FTE-CR":
                                switch (empresa)
                                {
                                    case Empresa.PLASTINOVO:
                                    case Empresa.SIMEX:
                                        break;
                                    default:
                                        Paragraph prgTituloImpuestoDef = new Paragraph(item["TaxDescription"].ToString(), Helpers.Fuentes.SimexDatos);
                                        celTituloSubtotal.AddElement(prgTituloImpuestoDef);
                                        break;
                                }
                                break;
                            default:
                                switch (empresa)
                                {
                                    case Empresa.SIMEX:
                                        break;
                                    default:
                                        Paragraph prgTituloImpuestoDef = new Paragraph(item["TaxDescription"].ToString(), Helpers.Fuentes.SimexDatos);
                                        celTituloSubtotal.AddElement(prgTituloImpuestoDef);
                                        break;
                                }
                                break;
                        }

                    }
                    celTituloSubtotal.Border = 0;
                    table.AddCell(celTituloSubtotal);
                    //Valores
                    PdfPCell celSubtotal = new PdfPCell();
                    string Subtotal = string.Empty;
                    ConvertirFormatoDecimal(ds.Tables["Invchead"].Rows[0]["DspDocSubTotal"], out Subtotal);
                    Paragraph prgSubtotal = new Paragraph(Subtotal, Helpers.Fuentes.SimexDatos);
                    prgSubtotal.Alignment = Element.ALIGN_RIGHT;
                    celSubtotal.AddElement(prgSubtotal);
                    foreach (DataRow item in ds.Tables["hedTaxSum"].Rows)
                    {
                        string Valor = string.Empty;
                        Paragraph prgImpuesto;
                        switch (item["TaxCode"].ToString())
                        {
                            case "IVA-AR":
                                ConvertirFormatoDecimal(item["DocTaxAmt"], out Valor);
                                prgImpuesto = new Paragraph(Valor, Fuentes.SimexDatos);
                                prgImpuesto.Alignment = Element.ALIGN_RIGHT;
                                celSubtotal.AddElement(prgImpuesto);
                                break;
                            case "ICA-AR":
                                break;
                            case "RET/FTE-CE":
                                break;
                            case "RET/FTE-CR":
                                switch (empresa)
                                {
                                    case Empresa.PLASTINOVO:
                                    case Empresa.SIMEX:
                                        break;
                                    default:
                                        ConvertirFormatoDecimal(item["DocTaxAmt"], out Valor);
                                        prgImpuesto = new Paragraph(Valor, Fuentes.SimexDatos);
                                        prgImpuesto.Alignment = Element.ALIGN_RIGHT;
                                        celSubtotal.AddElement(prgImpuesto);
                                        break;
                                }
                                break;
                            default:
                                switch (empresa)
                                {
                                    case Empresa.SIMEX:
                                        break;
                                    default:
                                        ConvertirFormatoDecimal(item["DocTaxAmt"], out Valor);
                                        prgImpuesto = new Paragraph(Valor, Fuentes.SimexDatos);
                                        prgImpuesto.Alignment = Element.ALIGN_RIGHT;
                                        celSubtotal.AddElement(prgImpuesto);
                                        break;
                                }
                                break;
                        }
                    }
                    celSubtotal.Border = 0;
                    table.AddCell(celSubtotal);
                }
                public static void CeldaBlanco(ref PdfPTable table, int colspan)
                {
                    PdfPCell celBlanco = new PdfPCell(new Phrase(" ", FontFactory.GetFont("Arial", 3f, iTextSharp.text.Font.BOLD)));
                    celBlanco.Colspan = colspan;
                    celBlanco.Border = 0;
                    table.AddCell(celBlanco);
                }
                public static void AgregarDataMercaderia(ref PdfPTable table, string tituloEsp, string tituloIng, int colspan, int rowspan, params string[] data)
                {
                    DetalleTitulos(ref table, tituloEsp, tituloIng, 1);
                    PdfPCell celda = new PdfPCell();
                    foreach (string item in data)
                    {
                        Paragraph prgDato = new Paragraph(item, Fuentes.SimexDatos);
                        celda.AddElement(prgDato);
                    }
                    celda.Border = 0;
                    celda.Colspan = colspan;
                    celda.Rowspan = rowspan;
                    celda.HorizontalAlignment = Element.ALIGN_LEFT;
                    //celda.VerticalAlignment = Element.ALIGN_MIDDLE;
                    table.AddCell(celda);
                }
                public static void DetalleTitulos(ref PdfPTable table, string tituloEsp, string tituloIng, int colspan)
                {
                    Paragraph prgTituloEsp = new Paragraph(tituloEsp, Fuentes.SimexTituloEsp);
                    Paragraph prgTituloIng = new Paragraph(tituloIng, Fuentes.SimexTituloIng);
                    PdfPCell celTitulo = new PdfPCell();
                    celTitulo.Border = 0;
                    celTitulo.Colspan = colspan;
                    celTitulo.AddElement(prgTituloEsp);
                    celTitulo.AddElement(prgTituloIng);
                    table.AddCell(celTitulo);
                }
                public static void Titulos2(ref PdfPTable table, string tituloEsp, string tituloIng, int colspan)
                {
                    Paragraph prgTituloEsp = new Paragraph(tituloEsp, Fuentes.SimexDatos);
                    Paragraph prgTituloIng = new Paragraph(tituloIng, Fuentes.SimexTituloIng);
                    PdfPCell celTitulo = new PdfPCell();
                    celTitulo.Border = 0;
                    celTitulo.AddElement(prgTituloEsp);
                    celTitulo.AddElement(prgTituloIng);
                    celTitulo.Colspan = colspan;
                    table.AddCell(celTitulo);
                }
                public static void DetalleValores(ref PdfPTable table, DataSet ds, Helpers.Simex.TipoDocumento TipoDoc)
                {
                    string PONum = ds.Tables["InvcHead"].Rows[0]["PONum"].ToString();
                    foreach (DataRow item in ds.Tables["InvcDtl"].Rows)
                    {
                        //---- Codigo del cliente ----
                        Paragraph prgCodigo = new Paragraph(item["PartNum"].ToString(), Fuentes.SimexDatos);
                        PdfPCell celCodigo = new PdfPCell();
                        celCodigo.AddElement(prgCodigo);
                        celCodigo.Border = 0;
                        celCodigo.PaddingRight = 2;
                        celCodigo.HorizontalAlignment = Element.ALIGN_RIGHT;
                        table.AddCell(celCodigo);
                        //------ Descripcion -----
                        Paragraph prgDescripcionCliente = new Paragraph(item["LineDesc"].ToString(), Fuentes.SimexDatos);
                        Paragraph prgDescripcionEmpresa = new Paragraph(item["DescripcionEmpresa"].ToString(), Fuentes.SimexDatos);
                        Paragraph prgDescripcionAduana = new Paragraph(item["DescripcionAduana"].ToString(), Fuentes.SimexDatos);
                        PdfPCell celDescripcion = new PdfPCell();
                        celDescripcion.AddElement(prgDescripcionCliente);
                        celDescripcion.AddElement(prgDescripcionEmpresa);
                        celDescripcion.AddElement(prgDescripcionAduana);
                        celDescripcion.HorizontalAlignment = Element.ALIGN_LEFT;
                        celDescripcion.Border = 0;
                        table.AddCell(celDescripcion);
                        //------ Orden de compra ------
                        Paragraph prgOrdenCompra = new Paragraph(PONum, Fuentes.SimexDatos);
                        PdfPCell celOrdenCompra = new PdfPCell();
                        celOrdenCompra.AddElement(prgOrdenCompra);
                        celOrdenCompra.HorizontalAlignment = Element.ALIGN_LEFT;
                        celOrdenCompra.Border = 0;
                        table.AddCell(celOrdenCompra);
                        //------ Referencia cliente ------
                        string RefCliente = item["XPartNum"].ToString();
                        if (RefCliente.IndexOf('_') > 0)
                            RefCliente = RefCliente.Substring(0, RefCliente.IndexOf('_') - 1);
                        Paragraph prgReferencia = new Paragraph(RefCliente, Fuentes.SimexDatos);
                        PdfPCell celReferencia = new PdfPCell();
                        celReferencia.AddElement(prgReferencia);
                        celReferencia.HorizontalAlignment = Element.ALIGN_CENTER;
                        celReferencia.Border = 0;
                        table.AddCell(celReferencia);
                        //---- Unidad de medida, Lote
                        Paragraph prgUN = new Paragraph(item["IUM"].ToString(), Fuentes.SimexDatos);
                        Paragraph prgLoteEsp = new Paragraph("Lote", Fuentes.SimexTituloEsp);
                        Paragraph prgLoteIng = new Paragraph("Batch", Fuentes.SimexTituloIng);
                        PdfPCell celUN = new PdfPCell();
                        celUN.Border = 0;
                        celUN.AddElement(prgUN);
                        celUN.AddElement(prgLoteEsp);
                        celUN.AddElement(prgLoteIng);
                        table.AddCell(celUN);
                        //-------- cantidad, info lote ---------
                        //Paragraph prgCant = new Paragraph(item["SellingShipQty"].ToString(), Fuentes.SimexDatos);
                        string cantidad = string.Empty;
                        ConvertirFormatoDecimal(item["SellingShipQty"], out cantidad);
                        Paragraph prgCant = new Paragraph(cantidad, Fuentes.SimexDatos);
                        Paragraph prgLote = new Paragraph(item["LotNum"].ToString(), Fuentes.SimexDatos);
                        PdfPCell celCant = new PdfPCell();
                        celCant.Border = 0;
                        celCant.AddElement(prgCant);
                        celCant.AddElement(prgLote);
                        table.AddCell(celCant);
                        //--------- Valor unitario ---------
                        string ValorUnitario = string.Empty;
                        ConvertirFormatoDecimal(item["DocUnitPrice"], TipoDoc, out ValorUnitario);
                        Paragraph prgValorUnitario = new Paragraph(ValorUnitario, Fuentes.SimexDatos);
                        PdfPCell celValorUnit = new PdfPCell();
                        celValorUnit.AddElement(prgValorUnitario);
                        celValorUnit.HorizontalAlignment = Element.ALIGN_LEFT;
                        celValorUnit.Border = 0;
                        table.AddCell(celValorUnit);
                        //------- Total USD ----------------
                        string TotalUSD = string.Empty;
                        ConvertirFormatoDecimal(item["DocExtPrice"], out TotalUSD);
                        PdfPCell celTotalUSD = new PdfPCell(new Phrase(TotalUSD, Fuentes.SimexDatos));
                        celTotalUSD.HorizontalAlignment = Element.ALIGN_LEFT;
                        celTotalUSD.Border = 0;
                        table.AddCell(celTotalUSD);
                    }
                }
                public static void ConvertirFormatoEntero(object data, out string formato)
                {
                    decimal result = 0;
                    formato = string.Empty;
                    try
                    {
                        result = Convert.ToDecimal(data);
                        //formato = string.Format("{0:0.00}", result);
                        formato = string.Format("{0:0}", result);
                    }
                    catch
                    {
                        formato = data.ToString();
                    }
                }
                public static void ConvertirFormatoDecimal(object data, Helpers.Simex.TipoDocumento TipoDoc, out string formato)
                {
                    decimal result = 0;
                    formato = string.Empty;
                    try
                    {
                        result = Convert.ToDecimal(data);
                        //formato = string.Format("{0:0.00}", result);
                        switch (TipoDoc)
                        {
                            case TipoDocumento.FacturaExportacion:
                                formato = string.Format("{0:N5}", result);
                                break;
                            default:
                                formato = string.Format("{0:N2}", result);
                                break;
                        }
                    }
                    catch
                    {
                        formato = data.ToString();
                    }
                }
                public static void ConvertirFormatoDecimal(object data, out string formato)
                {
                    decimal result = 0;
                    formato = string.Empty;
                    try
                    {
                        result = Convert.ToDecimal(data);
                        formato = string.Format("{0:N2}", result);
                    }
                    catch
                    {
                        formato = data.ToString();
                    }
                }
                public static void DobleLinea(ref PdfPTable table, int colspan)
                {
                    PdfPCell celDobleLinea = new PdfPCell(new Phrase(" ", FontFactory.GetFont("Arial", 1.5f, iTextSharp.text.Font.NORMAL)));
                    celDobleLinea.Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER;
                    celDobleLinea.Colspan = colspan;
                    celDobleLinea.Padding = 0;
                    table.AddCell(celDobleLinea);
                }
                public static void AgregarDatosCliente(ref PdfPTable table, string tituloEsp, string tituloIng, object data)
                {
                    Paragraph prgTituloEsp = new Paragraph(tituloEsp, Fuentes.SimexTituloEsp);
                    Paragraph prgTituloIng = new Paragraph(tituloIng, Fuentes.SimexTituloIng);
                    PdfPCell celTitulo = new PdfPCell();
                    celTitulo.Border = 0;
                    celTitulo.AddElement(prgTituloEsp);
                    celTitulo.AddElement(prgTituloIng);
                    table.AddCell(celTitulo);

                    Paragraph prgDato = new Paragraph(string.Format("{0}", data), Fuentes.SimexDatos);
                    //PdfPCell celDato = new PdfPCell(new Phrase(string.Format("{0}", data), Fuentes.SimexDatos));
                    PdfPCell celDato = new PdfPCell();
                    celDato.AddElement(prgDato);
                    celDato.Border = 0;
                    celDato.VerticalAlignment = Element.ALIGN_TOP;
                    celDato.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(celDato);
                }
                public static void AddPageNumberPagToSimexExp(string rutaPDF, float posicionX, float posicionY, iTextSharp.text.Font font)
                {
                    byte[] bytes = File.ReadAllBytes(rutaPDF);

                    //iTextSharp.text.Font blackFont = FontFactory.GetFont("Arial", 12, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
                    using (MemoryStream stream = new MemoryStream())
                    {
                        PdfReader reader = new PdfReader(bytes);
                        using (PdfStamper stamper = new PdfStamper(reader, stream))
                        {
                            int pages = reader.NumberOfPages;
                            for (int i = 1; i <= pages; i++)
                            {
                                ColumnText.ShowTextAligned(
                                    stamper.GetUnderContent(i), Element.ALIGN_CENTER, new Phrase($"Pagina: {i.ToString()} de {pages}\r\nPage", font), posicionX, posicionY, 0);
                            }
                        }
                        bytes = stream.ToArray();
                    }
                    File.WriteAllBytes(rutaPDF, bytes);
                }
                public static decimal MercaderiaTotalCajas(DataSet ds)
                {
                    //primero se aloja la informacion de las partes y los detalles en las clases
                    var Partes = ListaPartes(ds);
                    var dtls = ListaInvcDtl(ds);
                    //recorrido de los detalles de la factura
                    decimal TotalCajas = 0;
                    foreach (var item in dtls)
                    {
                        //se busca la parte correspondiente a la linea
                        var Parte = (from row in Partes
                                     where row.PartNum == item.PartNum
                                     select row).FirstOrDefault();
                        if (Parte != null) //si hay resultados
                        {
                            if (Parte.PartsPerContainer > 0)
                            {
                                if (item.SalesUM == "MI")
                                    TotalCajas += Math.Ceiling((item.SellingShipQty * 1000) / Parte.PartsPerContainer);
                                else if (item.SalesUM == "UN")
                                    TotalCajas += Math.Ceiling(item.SellingShipQty / Parte.PartsPerContainer);
                            }
                        }
                    }
                    return TotalCajas;
                }
                public static decimal MercaderiaPesoNeto(DataSet ds)
                {
                    //primero se aloja la informacion de las partes y los detalles en las clases
                    var Partes = ListaPartes(ds);
                    var dtls = ListaInvcDtl(ds);
                    //recorrido de los detalles de la factura
                    decimal PesoNeto = 0;
                    foreach (var item in dtls)
                    {
                        //se busca la parte correspondiente a la linea
                        var Parte = (from row in Partes
                                     where row.PartNum == item.PartNum
                                     select row).FirstOrDefault();
                        if (Parte != null) //si hay resultados
                        {
                            if (item.SalesUM == "MI")
                                PesoNeto += (Parte.NetWeight * item.SellingShipQty * 1000) / 1000;
                            else if (item.SalesUM == "UN")
                                PesoNeto += (Parte.NetWeight * item.SellingShipQty) / 1000;
                        }
                    }
                    return PesoNeto;
                }
                public static decimal MercaderiaPesoBruto(DataSet ds)
                {
                    //primero se aloja la informacion de las partes y los detalles en las clases
                    var Partes = ListaPartes(ds);
                    var dtls = ListaInvcDtl(ds);
                    //recorrido de los detalles de la factura
                    decimal PesoBruto = 0;
                    foreach (var item in dtls)
                    {
                        //se busca la parte correspondiente a la linea
                        var Parte = (from row in Partes
                                     where row.PartNum == item.PartNum
                                     select row).FirstOrDefault();
                        if (Parte != null) //si hay resultados
                        {
                            //calculo peso bruto
                            if (Parte.PartsPerContainer > item.SellingShipQty && item.SalesUM != "MI")
                                PesoBruto += (Parte.GrossWeight + Parte.NetWeight * item.SellingShipQty) / 1000;
                            else
                            {
                                if (Parte.PartsPerContainer > 0)
                                {
                                    if (item.SalesUM == "MI")
                                        PesoBruto += (((Parte.GrossWeight / Parte.PartsPerContainer) + Parte.NetWeight) * item.SellingShipQty * 1000) / 1000;
                                    if (item.SalesUM == "UN")
                                        PesoBruto += (((Parte.GrossWeight / Parte.PartsPerContainer) + Parte.NetWeight) * item.SellingShipQty) / 1000;
                                }
                            }
                        }
                    }
                    return PesoBruto;
                }
                public static void MercaderiaPosAran(DataSet ds, out string PosAran, out string PosAranDes)
                {
                    PosAran = string.Empty;
                    PosAranDes = string.Empty;
                    try
                    {
                        var dtls = ListaInvcDtl(ds);
                        var cusx = ListaCustXPrt(ds);
                        foreach (var item in dtls)
                        {
                            var cux = (from row in cusx
                                       where row.PartNum == item.PartNum
                                       select row).FirstOrDefault();
                            if (cux != null)
                            {
                                PosAran = cux.PosArancelaria;
                                PosAranDes = cux.PosArancelariaDes;
                                break;
                            }
                        }
                    }
                    catch
                    {

                    }

                }
                //Listado de las partes
                private static List<Part> ListaPartes(DataSet ds)
                {
                    var Partes = from row in ds.Tables["Part"].AsEnumerable()
                                 select new Part
                                 {
                                     PartNum = row["PartNum"].ToString(),
                                     PartsPerContainer = Convert.ToInt32(row["PartsPerContainer"]),
                                     NetWeight = Convert.ToDecimal(row["NetWeight"]),
                                     GrossWeight = Convert.ToDecimal(row["GrossWeight"])
                                 };
                    return Partes.ToList();
                }
                //Listado de Invcdtl
                private static List<InvcDtl> ListaInvcDtl(DataSet ds)
                {
                    var Detalles = from row in ds.Tables["InvcDtl"].AsEnumerable()
                                   select new InvcDtl
                                   {
                                       InvoiceNum = Convert.ToInt32(row["InvoiceNum"]),
                                       InvoiceLine = Convert.ToInt32(row["InvoiceLine"]),
                                       PartNum = row["PartNum"].ToString(),
                                       SalesUM = row["SalesUM"].ToString(),
                                       SellingShipQty = Convert.ToDecimal(row["SellingShipQty"])
                                   };
                    return Detalles.ToList();
                }
                //Listado de CustXPrt
                private static List<CustXPrt> ListaCustXPrt(DataSet ds)
                {
                    var cusx = from row in ds.Tables["CustXPrt"].AsEnumerable()
                               select new CustXPrt
                               {
                                   CustNum = Convert.ToInt32(row["CustNum"]),
                                   PartNum = row["PartNum"].ToString(),
                                   XPartNum = row["XPartNum"].ToString(),
                                   PosArancelaria = row["Character01"].ToString(),
                                   PosArancelariaDes = ""
                               };
                    return cusx.ToList();
                }
                public class EventPageSimex : PdfPageEventHelper
                {
                    PdfContentByte cb;

                    // we will put the final number of pages in a template
                    PdfTemplate headerTemplate, footerTemplate;

                    // this is the BaseFont we are going to use for the header / footer
                    BaseFont bf = null;

                    // This keeps track of the creation time
                    DateTime PrintTime = DateTime.Now;

                    private string _header;

                    public string Header
                    {
                        get { return _header; }
                        set { _header = value; }
                    }
                    PdfPTable _Encabezado;
                    PdfPTable _DatosCliente;
                    PdfPTable _DetalleHead;
                    PdfPTable _Mercaderia;

                    PdfPTable _PiePagina;
                    public PdfPTable PiePagina
                    {
                        get
                        {
                            return _PiePagina;
                        }
                        set
                        {
                            _PiePagina = value;
                        }
                    }
                    public PdfPTable Encabezado
                    {
                        get
                        {
                            return _Encabezado;
                        }
                        set
                        {
                            _Encabezado = value;
                        }
                    }
                    public PdfPTable DatosCliente
                    {
                        get
                        {
                            return _DatosCliente;
                        }
                        set
                        {
                            _DatosCliente = value;
                        }
                    }
                    public PdfPTable DetalleHead
                    {
                        get
                        {
                            return _DetalleHead;
                        }
                        set
                        {
                            _DetalleHead = value;
                        }
                    }
                    public PdfPTable Mercaderia
                    {
                        get
                        {
                            return _Mercaderia;
                        }
                        set
                        {
                            _Mercaderia = value;
                        }
                    }

                    public EventPageSimex(PdfPTable encabezado, PdfPTable DatosCliente, PdfPTable detalleHead, PdfPTable mercaderia, PdfPTable piepagina)
                    {
                        _Encabezado = encabezado;
                        _DatosCliente = DatosCliente;
                        _DetalleHead = detalleHead;
                        _Mercaderia = mercaderia;
                        _PiePagina = piepagina;
                    }

                    public override void OnOpenDocument(PdfWriter writer, Document document)
                    {
                        try
                        {
                            PrintTime = DateTime.Now;
                            bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                            cb = writer.DirectContent;
                            headerTemplate = cb.CreateTemplate(100, 100);
                            footerTemplate = cb.CreateTemplate(50, 50);
                        }
                        catch (DocumentException de)
                        {
                        }
                        catch (System.IO.IOException ioe)
                        {
                        }
                    }

                    public override void OnEndPage(iTextSharp.text.pdf.PdfWriter writer, iTextSharp.text.Document document)
                    {
                        base.OnEndPage(writer, document);

                        //call WriteSelectedRows of PdfTable. This writes rows from PdfWriter in PdfTable
                        //first param is start row. -1 indicates there is no end row and all the rows to be included to write
                        //Third and fourth param is x and y position to start writing


                        //document.Add(_PiePagina);

                        Encabezado.TotalWidth = document.PageSize.Width - 35f;
                        Encabezado.WriteSelectedRows(0, -1, 20, document.PageSize.Height - 40, writer.DirectContentUnder);

                        _DatosCliente.TotalWidth = document.PageSize.Width - 35f;
                        _DatosCliente.WriteSelectedRows(0, -1, 20, document.PageSize.Height - 180, writer.DirectContentUnder);

                        _DetalleHead.TotalWidth = document.PageSize.Width - 35f;
                        _DetalleHead.WriteSelectedRows(0, -1, 20, document.PageSize.Height - 300, writer.DirectContentUnder);


                        _Mercaderia.TotalWidth = document.PageSize.Width - 35f;
                        _Mercaderia.WriteSelectedRows(0, -1, 20, document.PageSize.Height - 480, writer.DirectContentUnder);

                        _PiePagina.TotalWidth = document.PageSize.Width - 35f;
                        _PiePagina.WriteSelectedRows(0, -1, 20, document.PageSize.Height - 580, writer.DirectContentUnder);

                        //tableUnidades.TotalWidth = document.PageSize.Width - 80f;
                        //tableUnidades.WriteSelectedRows(0, -1, 40, document.PageSize.Height - 40, writer.DirectContentUnder);
                        //pdfTab.WriteSelectedRows(0, -1, 40, document.PageSize.Height - 30, writer.DirectContent);

                        //cb.MoveTo(40, document.PageSize.Height - 100);
                        //cb.LineTo(document.PageSize.Width - 40, document.PageSize.Height - 100);
                        //cb.Stroke();
                    }

                    public override void OnCloseDocument(PdfWriter writer, Document document)
                    {
                        base.OnCloseDocument(writer, document);

                        headerTemplate.BeginText();
                        headerTemplate.SetFontAndSize(bf, 12);
                        headerTemplate.SetTextMatrix(0, 0);
                        headerTemplate.ShowText((writer.PageNumber - 1).ToString());
                        headerTemplate.EndText();

                        //footerTemplate.BeginText();
                        //footerTemplate.SetFontAndSize(bf, 12);
                        //footerTemplate.SetTextMatrix(0, 0);
                        //footerTemplate.ShowText((writer.PageNumber - 1).ToString());
                        //footerTemplate.EndText();
                    }
                }
                //-----------------------------------------------------------------------------------
                //--------------------- Datos comunes para todos los formatos -----------------------
                //-----------------------------------------------------------------------------------
                public static PdfPTable Encabezado(DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, Helpers.Simex.Empresa empresa, Helpers.Simex.TipoDocumento TipoDoc, string Logo, bool Local)
                {
                    //----- Recolectando informacion de las compañias -------
                    EncabezadoData data = new EncabezadoData();
                    switch (empresa)
                    {
                        case Empresa.SIMEX:
                            data.CodPostal = "055422";
                            data.Direccion = "Carrera 48 No. 48 SUR 181 (102)";
                            data.Email = "info@simex.com.co";
                            if (Local)
                                data.Logo = $@"{AppDomain.CurrentDomain.BaseDirectory}\logoSIMEX2.png";
                            else
                                data.Logo = Logo;
                            data.NIT = "890.906.904-1";
                            data.Nombre = "SIMEX S.A.S";
                            data.Telefono = "(57-4) 305 19 00";
                            data.Ubicacion = "Envigado (Antioquia) – Colombia";
                            data.Web = "www.simex.com.co";
                            break;
                        case Empresa.PLASTINOVO:
                            data.CodPostal = "055460";
                            data.Direccion = "Carrera 55 No. 79 C SUR 02";
                            data.Email = "contabilidad@plastinovo.com";
                            if (Local)
                                data.Logo = $@"{AppDomain.CurrentDomain.BaseDirectory}\LogoPlastinovo.png";
                            else
                                data.Logo = Logo;
                            data.NIT = "890.941.376-0";
                            data.Nombre = "PLASTINOVO S.A.S";
                            data.Telefono = "(57-4) 379 20 20";
                            data.Ubicacion = "La Estrella (Antioquia) - Colombia";
                            data.Web = "www.plastinovo.com";
                            break;
                        case Empresa.SOINCO:
                            data.CodPostal = "055450";
                            data.Direccion = "Carrera 44 No. 50 SUR 100";
                            data.Email = "contacto@soinco-sas.com";
                            if (Local)
                                data.Logo = $@"{AppDomain.CurrentDomain.BaseDirectory}\LogoSoinco.png";
                            else
                                data.Logo = Logo;
                            data.NIT = "800.253.534-9";
                            data.Nombre = "SOINCO S.A.S";
                            data.Telefono = "(57-4) 288 74 74";
                            data.Ubicacion = "Sabaneta (Antioquia) - Colombia";
                            data.Web = "www.soinco-sas.com";
                            break;
                        default:
                            break;
                    }

                    PdfPTable tbPreEncabezado = new PdfPTable(2);
                    tbPreEncabezado.SetWidths(new float[] { 4.64f, 1.1f });
                    tbPreEncabezado.WidthPercentage = 100;
                    //----- Parte 1: Logo de la compañia y su informacion --------
                    PdfPTable tbLogoInfo = new PdfPTable(1);
                    tbLogoInfo.WidthPercentage = 100;
                    //----- Inicio Logo de la compañia -------------
                    System.Drawing.Image logo = null;
                    //var requestLogo = WebRequest.Create($@"{AppDomain.CurrentDomain.BaseDirectory}\{data.Logo}");
                    var requestLogo = WebRequest.Create(data.Logo);
                    using (var responseLogo = requestLogo.GetResponse())
                    using (var streamLogo = responseLogo.GetResponseStream())
                    {
                        logo = Bitmap.FromStream(streamLogo);
                    }
                    PdfDiv divLogo = new PdfDiv();
                    divLogo.Float = iTextSharp.text.pdf.PdfDiv.FloatType.LEFT;
                    divLogo.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                    divLogo.BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID;
                    divLogo.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                    divLogo.Height = 40;
                    divLogo.Width = 200;
                    //divLogo.BackgroundColor = BaseColor.LIGHT_GRAY;
                    iTextSharp.text.Image ImgLogo = iTextSharp.text.Image.GetInstance(logo, BaseColor.WHITE);
                    ImgLogo.Alignment = Element.ALIGN_CENTER;
                    divLogo.BackgroundImage = ImgLogo;
                    PdfPCell celLogo = new PdfPCell()
                    {
                        HorizontalAlignment = Element.ALIGN_LEFT,
                        //Colspan = 2,
                        Border = 0
                    };
                    celLogo.AddElement(divLogo);
                    tbLogoInfo.AddCell(celLogo);
                    //----- Fin Logo compañia -----------
                    //------Datos de la compañia -----------
                    Paragraph prgNombreEmpresa = new Paragraph(data.Nombre, Helpers.Fuentes.fontTitle);
                    Paragraph prgNIT = new Paragraph(string.Format("NIT {0}", data.NIT), Helpers.Fuentes.fontTitle);
                    Paragraph prgDireccion = new Paragraph(data.Direccion, Helpers.Fuentes.fontTitle);
                    Paragraph prgUbicacion = new Paragraph(string.Format("{0}", data.Ubicacion), Helpers.Fuentes.fontTitle);
                    Paragraph prgCodPostal = new Paragraph($"Código Postal {data.CodPostal}", Helpers.Fuentes.fontTitle);
                    Paragraph prgTelefono = new Paragraph(string.Format("Teléfono {0}", data.Telefono), Helpers.Fuentes.fontTitle);
                    Paragraph prgEmail = new Paragraph(data.Email, Helpers.Fuentes.fontTitle);
                    Paragraph prgWeb = new Paragraph(data.Web, Helpers.Fuentes.fontTitle);
                    //------ Fin datos de la compañia -----------
                    PdfPCell celTextInfoEmpresa = new PdfPCell()
                    {
                        HorizontalAlignment = Element.ALIGN_LEFT,
                        //Colspan = 2,
                        Border = 0,
                        Padding = 0
                    };
                    celTextInfoEmpresa.AddElement(prgNombreEmpresa);
                    celTextInfoEmpresa.AddElement(prgNIT);
                    celTextInfoEmpresa.AddElement(prgDireccion);
                    celTextInfoEmpresa.AddElement(prgUbicacion);
                    celTextInfoEmpresa.AddElement(prgCodPostal);
                    celTextInfoEmpresa.AddElement(prgTelefono);
                    celTextInfoEmpresa.AddElement(prgEmail);
                    celTextInfoEmpresa.AddElement(prgWeb);
                    tbLogoInfo.AddCell(celTextInfoEmpresa);
                    PdfPCell celLogoInfo = new PdfPCell();
                    celLogoInfo.Border = 0;
                    celLogoInfo.AddElement(tbLogoInfo);
                    tbPreEncabezado.AddCell(celLogoInfo);
                    //----- Parte 2, Código QR, numero de factura
                    PdfPTable tbQRInvoice = new PdfPTable(1);
                    tbQRInvoice.WidthPercentage = 100;
                    //---- Código QR
                    PdfDiv divQR = new PdfDiv();
                    divQR.Float = iTextSharp.text.pdf.PdfDiv.FloatType.RIGHT;
                    divQR.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                    divQR.BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID;
                    divQR.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                    divQR.Height = 75;
                    divQR.Width = 75;
                    divQR.BackgroundColor = BaseColor.BLUE;
                    iTextSharp.text.Image ImgQR = iTextSharp.text.Image.GetInstance(QRInvoice, BaseColor.WHITE);
                    ImgQR.Alignment = Element.ALIGN_RIGHT;
                    divQR.BackgroundImage = ImgQR;
                    var celQR = new PdfPCell()
                    {
                        Border = PdfPCell.NO_BORDER,
                        HorizontalAlignment = Element.ALIGN_RIGHT
                    };
                    celQR.AddElement(divQR);
                    tbQRInvoice.AddCell(celQR);
                    //----- Fin código QR ---------
                    //----- Informacion adicional --------
                    //Paragraph prgRepresentacion = new Paragraph("Representacion gráfica factura electrónica", FontFactory.GetFont("Arial", 5f, iTextSharp.text.Font.NORMAL));
                    //prgRepresentacion.Alignment = Element.ALIGN_RIGHT;

                    switch (TipoDoc)
                    {
                        case TipoDocumento.FacturaNacional:
                        case TipoDocumento.FacturaExportacion:
                            data.TituloEsp = "FACTURA DE VENTA No.";
                            data.TituloIng = "INVOICE No.";
                            break;
                        case TipoDocumento.NotaCredito:
                            data.TituloEsp = "NOTA CRÉDITO No.";
                            data.TituloIng = "CREDIT NOTE No.";
                            break;
                        case TipoDocumento.NotaDebito:
                            data.TituloEsp = "NOTA DÉBITO No.";
                            data.TituloIng = "DEBIT NOTE No.";
                            break;
                    }
                    Paragraph prgTituloFacturaEsp = new Paragraph(data.TituloEsp, FontFactory.GetFont("Arial", 7f, iTextSharp.text.Font.NORMAL));
                    prgTituloFacturaEsp.Alignment = Element.ALIGN_RIGHT;
                    Paragraph prgTituloFacturaIng = new Paragraph(data.TituloIng, Helpers.Fuentes.fontTitleLittleBold);
                    prgTituloFacturaIng.Alignment = Element.ALIGN_RIGHT;
                    Paragraph prgNumeroLegal = new Paragraph(string.Format("{0}", DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"]), FontFactory.GetFont("Arial", 10f, iTextSharp.text.Font.BOLD));
                    prgNumeroLegal.Alignment = Element.ALIGN_RIGHT;
                    Paragraph prgBlanco = new Paragraph(" ", Helpers.Fuentes.fontTitle);
                    Paragraph prgInvoiceNum = new Paragraph(string.Format("V: {0}", DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceNum"]), FontFactory.GetFont("Arial", 5, iTextSharp.text.Font.BOLD));
                    prgInvoiceNum.Alignment = Element.ALIGN_RIGHT;
                    PdfPCell celInfo = new PdfPCell()
                    {
                        HorizontalAlignment = Element.ALIGN_RIGHT,
                        Border = 0,
                        Padding = 0
                    };
                    //celInfo.AddElement(prgRepresentacion);
                    celInfo.AddElement(prgTituloFacturaEsp);
                    celInfo.AddElement(prgTituloFacturaIng);
                    celInfo.AddElement(prgNumeroLegal);
                    celInfo.AddElement(prgBlanco);
                    celInfo.AddElement(prgInvoiceNum);


                    //Phrase phTitulo = new Phrase();
                    //Chunk chunk = new Chunk("Invoice No.", Fuentes.fontTitleItalic);
                    //Chunk chunk2 = new Chunk("Factura", Fuentes.fontTitleBold);
                    //phTitulo.Add(chunk);
                    //phTitulo.Add(chunk2);
                    //PdfPCell celtest = new PdfPCell();
                    //celtest.AddElement(phTitulo);
                    tbQRInvoice.AddCell(celInfo);
                    //----- Fin informacion adicional ---------
                    PdfPCell celda2 = new PdfPCell();
                    celda2.Padding = 0;
                    celda2.Border = 0;
                    celda2.AddElement(tbQRInvoice);
                    tbPreEncabezado.AddCell(celda2);
                    Helpers.Simex.CeldaBlanco(ref tbPreEncabezado, 2);
                    return tbPreEncabezado;
                }
                public static PdfPTable DatosCliente(DataSet ds, Helpers.Simex.TipoDocumento TipoDoc)
                {
                    //-----------------------------------------------------------------------
                    //------------------ Informacion de cliente -----------------------------
                    //-----------------------------------------------------------------------
                    PdfPTable tbClienteInfo = new PdfPTable(6);
                    tbClienteInfo.SetWidths(new float[] { 1f, 2.2f, 1.2f, 1f, 1.2f, 1f });
                    tbClienteInfo.WidthPercentage = 100;
                    //-------- celda en blanco
                    Helpers.Simex.CeldaBlanco(ref tbClienteInfo, 6);
                    //--------- Doble linea
                    Helpers.Simex.DobleLinea(ref tbClienteInfo, 6);
                    //------- Nombre cliente
                    Helpers.Simex.AgregarDatosCliente(ref tbClienteInfo, "Cliente", "Customer", ds.Tables["Customer"].Rows[0]["Name"]);

                    string Direccion = string.Empty;
                    switch (TipoDoc)
                    {
                        case TipoDocumento.FacturaNacional:
                            //-------------- Moneda ------------------------
                            Helpers.Simex.AgregarDatosCliente(ref tbClienteInfo, "Moneda", "Currency", ds.Tables["InvcHead"].Rows[0]["CurrencyCode"]);
                            //------------------Casilla en blanco --------------
                            Helpers.Simex.AgregarDatosCliente(ref tbClienteInfo, " ", " ", " ");
                            //------------------ Direccion ------------------------------------
                            Direccion += ds.Tables["Customer"].Rows[0]["Address1"].ToString() + "\r\n";
                            Direccion += ds.Tables["Customer"].Rows[0]["City"].ToString() + "-" + ds.Tables["Customer"].Rows[0]["State"].ToString() + "\r\n";
                            Direccion += ds.Tables["Customer"].Rows[0]["Country"].ToString();
                            Helpers.Simex.AgregarDatosCliente(ref tbClienteInfo, "Dirección Cliente", "Address", Direccion);
                            //--------- Fecha de expedicion ------------------------------------------------------
                            Helpers.Simex.AgregarDatosCliente(ref tbClienteInfo, "Fecha de Expedicion", "Issue Date", Helpers.Simex.AjusteFechas(ds.Tables["InvcHead"].Rows[0]["InvoiceDate"].ToString()));
                            //------------------Casilla en blanco --------------
                            Helpers.Simex.AgregarDatosCliente(ref tbClienteInfo, " ", " ", " ");
                            //-------- Teléfono ------------------------------
                            Helpers.Simex.AgregarDatosCliente(ref tbClienteInfo, "Teléfono", "Phone", ds.Tables["Customer"].Rows[0]["PhoneNum"]);
                            //--------------- Fecha de vencimiento -----------------------------
                            Helpers.Simex.AgregarDatosCliente(ref tbClienteInfo, "Fecha de Vencimiento", "Due Date", Helpers.Simex.AjusteFechas(ds.Tables["InvcHead"].Rows[0]["DueDate"].ToString()));
                            //------------------Casilla en blanco --------------
                            Helpers.Simex.AgregarDatosCliente(ref tbClienteInfo, " ", " ", " ");
                            //--------------- NIT -------------
                            Helpers.Simex.AgregarDatosCliente(ref tbClienteInfo, "NIT", "Tax ID Number", ds.Tables["COOneTime"].Rows[0]["COOneTimeID"]);
                            //------------------ Condiciones de pago -------------
                            Helpers.Simex.AgregarDatosCliente(ref tbClienteInfo, "Condiciones de Pago", "Payment Conditions", ds.Tables["Customer"].Rows[0]["TermsDescription"]);
                            //------------------Casilla en blanco --------------
                            Helpers.Simex.AgregarDatosCliente(ref tbClienteInfo, " ", " ", " ");
                            break;
                        default:
                            //------------ Codigo cliente
                            Helpers.Simex.AgregarDatosCliente(ref tbClienteInfo, "Código Cliente", "Customer Code", ds.Tables["Customer"].Rows[0]["CustID"]);
                            //-------------- Moneda ------------------------
                            Helpers.Simex.AgregarDatosCliente(ref tbClienteInfo, "Moneda", "Currency", ds.Tables["InvcHead"].Rows[0]["CurrencyCode"]);
                            //------------------ Direccion ------------------------------------
                            Direccion += ds.Tables["Customer"].Rows[0]["Address1"].ToString() + "\r\n";
                            Direccion += ds.Tables["Customer"].Rows[0]["City"].ToString() + "-" + ds.Tables["Customer"].Rows[0]["State"].ToString() + "\r\n";
                            Direccion += ds.Tables["Customer"].Rows[0]["Country"].ToString();
                            Helpers.Simex.AgregarDatosCliente(ref tbClienteInfo, "Dirección Cliente", "Address", Direccion);
                            //------------------ Incoterms ------------------------------------
                            Helpers.Simex.AgregarDatosCliente(ref tbClienteInfo, "INCOTERMS", "INCOTERMS", ds.Tables["InvcHead"].Rows[0]["FOB"]);
                            //--------- Fecha de expedicion ------------------------------------------------------
                            Helpers.Simex.AgregarDatosCliente(ref tbClienteInfo, "Fecha de Expedicion", "Issue Date", Helpers.Simex.AjusteFechas(ds.Tables["InvcHead"].Rows[0]["InvoiceDate"].ToString()));
                            //-------- Teléfono ------------------------------
                            Helpers.Simex.AgregarDatosCliente(ref tbClienteInfo, "Teléfono", "Phone", ds.Tables["Customer"].Rows[0]["PhoneNum"]);
                            //------------- Lugar de entrega -----------------------------------------
                            Helpers.Simex.AgregarDatosCliente(ref tbClienteInfo, "Lugar de entrega", "Place of delivery", "Medellín");
                            //--------------- Fecha de vencimiento -----------------------------
                            Helpers.Simex.AgregarDatosCliente(ref tbClienteInfo, "Fecha de Vencimiento", "Due Date", Helpers.Simex.AjusteFechas(ds.Tables["InvcHead"].Rows[0]["DueDate"].ToString()));
                            //--------------- NIT -------------
                            switch (TipoDoc)
                            {
                                case TipoDocumento.FacturaExportacion:
                                    Helpers.Simex.AgregarDatosCliente(ref tbClienteInfo, "Código Fiscal", "Tax ID Number", ds.Tables["COOneTime"].Rows[0]["character04"]);
                                    break;
                                default:
                                    Helpers.Simex.AgregarDatosCliente(ref tbClienteInfo, "NIT", "Tax ID Number", ds.Tables["COOneTime"].Rows[0]["COOneTimeID"]);
                                    break;
                            }
                            //------------------Casilla en blanco --------------
                            Helpers.Simex.AgregarDatosCliente(ref tbClienteInfo, " ", " ", " ");
                            //------------------ Condiciones de pago -------------
                            Helpers.Simex.AgregarDatosCliente(ref tbClienteInfo, "Condiciones de Pago", "Payment Conditions", ds.Tables["Customer"].Rows[0]["TermsDescription"]);
                            break;
                    }
                    //---------------celda en blanco
                    Helpers.Simex.CeldaBlanco(ref tbClienteInfo, 6);
                    return tbClienteInfo;
                }
                public static PdfPTable DetallesHead(Helpers.Simex.TipoDocumento TipoDoc)
                {
                    PdfPTable tbDetallesHead = new PdfPTable(8);
                    tbDetallesHead.WidthPercentage = 100;
                    tbDetallesHead.SetWidths(new float[] { 0.5f, 1.8f, 1f, 1f, 0.4f, 1f, 1f, 1f });
                    //Doble linea -----------
                    Helpers.Simex.DobleLinea(ref tbDetallesHead, 8);
                    //Encabezado detalles ---------------------
                    Helpers.Simex.DetalleTitulos(ref tbDetallesHead, "Código", "Code", 1);
                    Helpers.Simex.DetalleTitulos(ref tbDetallesHead, "Descripción", "Description", 1);
                    Helpers.Simex.DetalleTitulos(ref tbDetallesHead, "OC", "Purchase Order", 1);
                    Helpers.Simex.DetalleTitulos(ref tbDetallesHead, "Ref Cliente", "Item No.", 1);
                    Helpers.Simex.DetalleTitulos(ref tbDetallesHead, "UND", "UOM", 1);
                    Helpers.Simex.DetalleTitulos(ref tbDetallesHead, "Cantidad", "Quantity", 1);
                    Helpers.Simex.DetalleTitulos(ref tbDetallesHead, "Vr Unitario", "Unit Price", 1);
                    switch (TipoDoc)
                    {
                        case TipoDocumento.FacturaExportacion:
                            Helpers.Simex.DetalleTitulos(ref tbDetallesHead, "Total USD", "Total USD", 1);
                            break;
                        default:
                            Helpers.Simex.DetalleTitulos(ref tbDetallesHead, "Total COP", "Total COP", 1);
                            break;
                    }
                    return tbDetallesHead;
                }
                public static PdfPTable Detalles(DataSet ds, Helpers.Simex.TipoDocumento TipoDoc, Helpers.Simex.Empresa empresa)
                {
                    PdfPTable tbDetalles = new PdfPTable(8);
                    tbDetalles.WidthPercentage = 100;
                    tbDetalles.SetWidths(new float[] { 0.5f, 1.8f, 1f, 1f, 0.4f, 1f, 1f, 1f });

                    ////--------- Doble linea
                    //PdfPCell celDobleLinea = new PdfPCell(new Phrase(" ", FontFactory.GetFont("Arial", 1.5f, iTextSharp.text.Font.NORMAL)));
                    //celDobleLinea.Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER;
                    //celDobleLinea.Colspan = 8;
                    //celDobleLinea.Padding = 0;
                    //tbDetalles.AddCell(celDobleLinea);
                    ////Encabezado código
                    //Helpers.Simex.DetalleTitulos(ref tbDetalles, "Código", "Code", 1);
                    //Helpers.Simex.DetalleTitulos(ref tbDetalles, "Descripción", "Description", 1);
                    //Helpers.Simex.DetalleTitulos(ref tbDetalles, "OC", "Purchase Order", 1);
                    //Helpers.Simex.DetalleTitulos(ref tbDetalles, "Ref Cliente", "Item No.", 1);
                    //Helpers.Simex.DetalleTitulos(ref tbDetalles, "UND", "UOM", 1);
                    //Helpers.Simex.DetalleTitulos(ref tbDetalles, "Cantidad", "Quantity", 1);
                    //Helpers.Simex.DetalleTitulos(ref tbDetalles, "Vr Unitario", "Unit Price", 1);
                    //switch (TipoDoc)
                    //{
                    //    case TipoDocumento.FacturaExportacion:
                    //        Helpers.Simex.DetalleTitulos(ref tbDetalles, "Total USD", "Total USD", 1);
                    //        break;
                    //    default:
                    //        Helpers.Simex.DetalleTitulos(ref tbDetalles, "Total COP", "Total COP", 1);
                    //        break;
                    //}
                    //detalles de la factura
                    Helpers.Simex.DetalleValores(ref tbDetalles, ds, TipoDoc);
                    //---------------Total de la factura ----------
                    PdfPTable tbTotal = new PdfPTable(2);
                    tbTotal.WidthPercentage = 100;
                    tbTotal.SetWidths(new float[] { 1f, 2f });
                    Paragraph prgTotalEsp;
                    Paragraph prgTotalIng;
                    PdfPCell celTituloTotal = new PdfPCell();
                    switch (TipoDoc)
                    {
                        case TipoDocumento.FacturaExportacion:
                            Helpers.Simex.TitulosImpuestos(ref tbTotal, ds, empresa);
                            prgTotalEsp = new Paragraph("TOTAL USD", FontFactory.GetFont("Arial", 10f, iTextSharp.text.Font.BOLDITALIC));
                            prgTotalIng = new Paragraph("TOTAL USD", FontFactory.GetFont("Arial", 7f, iTextSharp.text.Font.BOLDITALIC));
                            break;
                        default:
                            Helpers.Simex.TitulosImpuestos(ref tbTotal, ds, empresa);
                            if (ds.Tables["InvcHead"].Rows[0]["CurrencyCode"].ToString() == "COP")
                            {
                                prgTotalEsp = new Paragraph("TOTAL COP", FontFactory.GetFont("Arial", 10f, iTextSharp.text.Font.BOLDITALIC));
                                prgTotalIng = new Paragraph("TOTAL COP", FontFactory.GetFont("Arial", 7f, iTextSharp.text.Font.BOLDITALIC));
                            }
                            else
                            {
                                prgTotalEsp = new Paragraph("TOTAL USD", FontFactory.GetFont("Arial", 10f, iTextSharp.text.Font.BOLDITALIC));
                                prgTotalIng = new Paragraph("TOTAL USD", FontFactory.GetFont("Arial", 7f, iTextSharp.text.Font.BOLDITALIC));
                            }
                            
                            //prgTotalEsp = new Paragraph("TOTAL COP", FontFactory.GetFont("Arial", 10f, iTextSharp.text.Font.BOLDITALIC));
                            //prgTotalIng = new Paragraph("TOTAL COP", FontFactory.GetFont("Arial", 7f, iTextSharp.text.Font.BOLDITALIC));
                            break;
                    }
                    celTituloTotal.AddElement(prgTotalEsp);
                    celTituloTotal.AddElement(prgTotalIng);
                    celTituloTotal.Border = 0;
                    tbTotal.AddCell(celTituloTotal);
                    //Valor total
                    string Total = string.Empty;
                    Helpers.Simex.ConvertirFormatoDecimal(ds.Tables["InvcHead"].Rows[0]["DocInvoiceAmt"], out Total);
                    PdfPCell celValorTotal = new PdfPCell(new Phrase(Total, FontFactory.GetFont("Arial", 10f, iTextSharp.text.Font.BOLD)));
                    celValorTotal.Border = 0;
                    celValorTotal.HorizontalAlignment = Element.ALIGN_RIGHT;
                    celValorTotal.VerticalAlignment = Element.ALIGN_MIDDLE;
                    tbTotal.AddCell(celValorTotal);
                    //Agregando la tabla de total
                    //Agregando la tabla de total
                    //celda en blanco
                    Helpers.Simex.CeldaBlanco(ref tbDetalles, 4);

                    PdfPCell celTotal = new PdfPCell();
                    celTotal.AddElement(tbTotal);
                    celTotal.Colspan = 4;
                    tbDetalles.AddCell(celTotal);
                    //celda blanco
                    Helpers.Simex.CeldaBlanco(ref tbDetalles, 8);

                    return tbDetalles;
                }
                public static PdfPTable Mercaderia(DataSet ds, Helpers.Simex.TipoDocumento TipoDoc)
                {
                    PdfPTable tbMercaderia = new PdfPTable(6);
                    tbMercaderia.WidthPercentage = 100;
                    tbMercaderia.SetWidths(new float[] { 1.2f, 1f, 1.2f, 1f, 1.2f, 1.5f });
                    //--------- Doble linea
                    Helpers.Simex.DobleLinea(ref tbMercaderia, 6);
                    switch (TipoDoc)
                    {
                        case TipoDocumento.FacturaNacional:
                        case TipoDocumento.FacturaExportacion:
                            //-------- Titulo principal --------------------
                            Paragraph prgTituloEsp = new Paragraph("MERCANCIA NUEVA Y ORIGINARIA DE COLOMBIA", Helpers.Fuentes.SimexDatos);
                            Paragraph prgTituloIng = new Paragraph("NEW GOODS MANUFACTURED IN COLOMBIA", Helpers.Fuentes.SimexTituloIng);
                            PdfPCell celTitulo = new PdfPCell();
                            celTitulo.Border = 0;
                            celTitulo.Colspan = 6;
                            celTitulo.HorizontalAlignment = Element.ALIGN_LEFT;
                            celTitulo.AddElement(prgTituloEsp);
                            celTitulo.AddElement(prgTituloIng);
                            tbMercaderia.AddCell(celTitulo);
                            //------ Datos ----------
                            string TotalCajas = ds.Tables["InvcHead"].Rows[0]["TotalBoxes"].ToString();
                            decimal PesoNeto = Convert.ToDecimal(ds.Tables["InvcHead"].Rows[0]["PesoNeto"]);
                            string RemisionEmbarque = ds.Tables["InvcDtl"].Rows[0]["PackNum"].ToString();
                            string DimensionCaja = ds.Tables["InvcHead"].Rows[0]["DimensionesCajas"].ToString();
                            decimal PesoBruto = Convert.ToDecimal(ds.Tables["InvcHead"].Rows[0]["PesoBruto"]);
                            string ObservacionesFac = ds.Tables["InvcHead"].Rows[0]["InvoiceComment"].ToString();
                            string ObservacionesEmb = ds.Tables["InvcHead"].Rows[0]["ShipComment"].ToString();
                            string PosAran = ds.Tables["InvcHead"].Rows[0]["PosicionAran"].ToString();
                            string PosAranDes = ds.Tables["InvcHead"].Rows[0]["PosicionAranDestino"].ToString();

                            //------------------ Total cajas ---------------------
                            //Helpers.Simex.AgregarDataMercaderia(ref tbMercaderia, "Total Cajas", "Total Boxes", string.Format("{0}", Helpers.Simex.MercaderiaTotalCajas(ds)));
                            Helpers.Simex.AgregarDataMercaderia(ref tbMercaderia, "Total Cajas", "Total Boxes", 1, 1, string.Format("{0}", TotalCajas));
                            switch (TipoDoc)
                            {
                                case TipoDocumento.FacturaNacional:
                                    //---------------- Observaciones ------------------------
                                    Helpers.Simex.AgregarDataMercaderia(ref tbMercaderia, "Observaciones", "Comments", 3, 2, ObservacionesFac, ObservacionesEmb);
                                    //---------------- Remision de embarque ------------------------
                                    Helpers.Simex.AgregarDataMercaderia(ref tbMercaderia, "Remisión de Embarque", "Shipping Reference", 1, 1, RemisionEmbarque);
                                    //----------------- Celda blanco 2 ---------------------------------------
                                    Helpers.Simex.AgregarDataMercaderia(ref tbMercaderia, "", "", 1, 1, string.Format(" "));
                                    break;
                                default:
                                    //----------------- Peso Neto ------------------------
                                    //Helpers.Simex.AgregarDataMercaderia(ref tbMercaderia, "Peso Neto", "Net Weight", string.Format("{0:0.00} KG", Helpers.Simex.MercaderiaPesoNeto(ds)));
                                    Helpers.Simex.AgregarDataMercaderia(ref tbMercaderia, "Peso Neto", "Net Weight", 1, 1, string.Format("{0:N2} KG", PesoNeto));
                                    //---------------- Remision de embarque ------------------------
                                    //Helpers.Simex.AgregarDataMercaderia(ref tbMercaderia, "Remisión de Embarque", "Shipping Reference", ds.Tables["InvcDtl"].Rows[0]["PackNum"].ToString());
                                    Helpers.Simex.AgregarDataMercaderia(ref tbMercaderia, "Remisión de Embarque", "Shipping Reference", 1, 1, RemisionEmbarque);
                                    //------------------ Dimensiones Caja ---------------------------
                                    Helpers.Simex.AgregarDataMercaderia(ref tbMercaderia, "Dimensión Caja", "Box Dimension", 1, 1, string.Format("{0}", DimensionCaja));
                                    //------------------ Peso bruto -----------------------
                                    //Helpers.Simex.AgregarDataMercaderia(ref tbMercaderia, "Peso Bruto", "Gross Weight", string.Format("{0:0.00} KG", Helpers.Simex.MercaderiaPesoBruto(ds)));
                                    Helpers.Simex.AgregarDataMercaderia(ref tbMercaderia, "Peso Bruto", "Gross Weight", 1, 1, string.Format("{0:N2} KG", PesoBruto));
                                    //---------------- Observaciones ------------------------
                                    Helpers.Simex.AgregarDataMercaderia(ref tbMercaderia, "Observaciones", "Comments", 1, 1, ObservacionesFac, ObservacionesEmb);
                                    //------------------ Pos. Arancelaria ---------------------------------
                                    //Helpers.Simex.MercaderiaPosAran(ds, out PosAran, out PosAranDes);
                                    Helpers.Simex.AgregarDataMercaderia(ref tbMercaderia, "Pos. Arancelaria", "Tariff Code", 1, 1, string.Format("{0}", PosAran));
                                    //--------------- Pos. Arancelaria Destino ------------------------
                                    Helpers.Simex.AgregarDataMercaderia(ref tbMercaderia, "Pos. Arancelaria Destino", "Tariff Code", 1, 1, string.Format("{0}", PosAranDes));
                                    //----------------- Celda blanco 2 ---------------------------------------
                                    Helpers.Simex.AgregarDataMercaderia(ref tbMercaderia, "", "", 1, 1, string.Format(" "));
                                    break;
                            }
                            break;

                        case TipoDocumento.NotaCredito:
                        case TipoDocumento.NotaDebito:

                            Helpers.Simex.DetalleTitulos(ref tbMercaderia, "Observaciones", "Comments", 2);
                            Paragraph prgComentarios = new Paragraph(ds.Tables["InvcHead"].Rows[0]["InvoiceComment"].ToString(), Fuentes.SimexDatos);
                            PdfPCell celComentarios = new PdfPCell();
                            celComentarios.MinimumHeight = 80;
                            celComentarios.AddElement(prgComentarios);
                            celComentarios.Colspan = 4;
                            celComentarios.Border = 1;
                            tbMercaderia.AddCell(celComentarios);
                            //PdfDiv div = new PdfDiv();
                            //div.Height = 5;

                            break;
                    }

                    return tbMercaderia;
                }
                public static PdfPTable PiePagina(DataSet ds, string CUFE, Helpers.Simex.TipoDocumento TipoDoc, Helpers.Simex.Empresa empresa)
                {
                    PdfPTable tbPiePagina = new PdfPTable(5);
                    tbPiePagina.WidthPercentage = 100;
                    tbPiePagina.SetWidths(new float[] { 1.8f, 0.2f, 1.8f, 0.2f, 2.5f });
                    //--------- Doble linea
                    Helpers.Simex.DobleLinea(ref tbPiePagina, 6);
                    //------ Titulos -----
                    Helpers.Simex.Titulos2(ref tbPiePagina, "ACEPTADA", "APPROVED BY", 2);
                    switch (TipoDoc)
                    {
                        case TipoDocumento.FacturaNacional:
                        case TipoDocumento.FacturaExportacion:
                            Helpers.Simex.Titulos2(ref tbPiePagina, "LA MERCANCÍA RELACIONADA FUE RECIBIDA REAL Y MATERIALMENTE", "GOODS INDICATED WERE RECEIVED", 3);
                            break;
                        case TipoDocumento.NotaCredito:
                        case TipoDocumento.NotaDebito:
                            Helpers.Simex.Titulos2(ref tbPiePagina, "", "", 3);
                            break;
                        default:
                            break;
                    }
                    //-------- Celda de la firma -----------------------
                    PdfPCell celFirma1 = new PdfPCell(new Phrase(" ", FontFactory.GetFont("Arial", 30f, iTextSharp.text.Font.BOLD)));
                    celFirma1.MinimumHeight = 1f;
                    celFirma1.Border = PdfPCell.BOTTOM_BORDER;
                    tbPiePagina.AddCell(celFirma1);
                    //------------ celda separador 1 ----------------------
                    PdfPCell separador1 = new PdfPCell();
                    separador1.MinimumHeight = 1f;
                    separador1.Border = 0;
                    tbPiePagina.AddCell(separador1);
                    //------------ Celda firma 2 -------------------------------
                    PdfPCell celFirma2 = new PdfPCell();
                    celFirma2.MinimumHeight = 1f;
                    celFirma2.Border = PdfPCell.BOTTOM_BORDER;
                    tbPiePagina.AddCell(celFirma2);
                    //------------ celda en blanco ----------------------
                    PdfPCell celFirma3 = new PdfPCell();
                    celFirma3.MinimumHeight = 1f;
                    celFirma3.Border = 0;
                    celFirma3.Colspan = 2;
                    tbPiePagina.AddCell(celFirma3);
                    //----- Titulo firma y sello
                    Helpers.Simex.Titulos2(ref tbPiePagina, "FIRMA Y SELLO DEL VENDEDOR", "SIGNATURE AND STAMP FROM SUPPLIER", 2);
                    Helpers.Simex.Titulos2(ref tbPiePagina, "FIRMA Y SELLO DEL COMPRADOR", "SIGNATURE AND STAMP FROM BUYER", 3);
                    Helpers.Simex.CeldaBlanco(ref tbPiePagina, 5);
                    switch (TipoDoc)
                    {
                        case TipoDocumento.FacturaNacional:
                        case TipoDocumento.FacturaExportacion:
                            //------- Doble linea ----------------------
                            Helpers.Simex.DobleLinea(ref tbPiePagina, 6);
                            //----- Leyenda pagos efectuados -------------
                            string Leyenda = "LOS PAGOS EFECTUADOS DESPUÉS DE LA FECHA DE VENCIMIENTO CAUSARÁN INTERESES DE MORA A LA TASA LEGAL VIGENTE CERTIFICADA Y AUTORIZADA POR LA SUPERINTENDENCIA FINANCIERA DE COLOMBIA.";
                            PdfPCell celLeyenda = new PdfPCell(new Phrase(Leyenda, Helpers.Fuentes.SimexLeyenda));
                            celLeyenda.Border = 0;
                            celLeyenda.Colspan = 5;
                            celLeyenda.Padding = 5;
                            tbPiePagina.AddCell(celLeyenda);
                            break;
                        case TipoDocumento.NotaCredito:
                        case TipoDocumento.NotaDebito:
                        default:
                            break;
                    }
                    //--------- Doble linea -------------------
                    Helpers.Simex.DobleLinea(ref tbPiePagina, 6);
                    //------------- Leyenda 2 -----------------------
                    Paragraph prgLey1 = new Paragraph("", Helpers.Fuentes.SimexDatos);
                    Paragraph prgLey2 = new Paragraph("", Helpers.Fuentes.SimexDatos);
                    Paragraph prgLey3 = new Paragraph("", Helpers.Fuentes.SimexDatos);
                    //Paragraph prgLey5 = new Paragraph("", Helpers.Fuentes.SimexDatos);
                    switch (empresa)
                    {
                        case Empresa.SIMEX:
                            prgLey1 = new Paragraph("IVA RÉGIMEN COMÚN - AGENTE RETENEDOR DE IVA", Helpers.Fuentes.SimexDatos);
                            prgLey2 = new Paragraph("SOMOS AUTO-RETENEDORES SEGUN RESOLUCIÓN 979 DE JULIO 29/87", Helpers.Fuentes.SimexDatos);
                            prgLey3 = new Paragraph("GRANDES CONTRIBUYENTES RESOLUCIÓN 000076 DE DICIEMBRE 01/2016", Helpers.Fuentes.SimexDatos);
                            //prgLey5 = new Paragraph(ds.Tables["InvcHead"].Rows[0]["Resolucion"].ToString(), Helpers.Fuentes.SimexDatos);
                            break;
                        case Empresa.PLASTINOVO:
                            prgLey1 = new Paragraph("IVA RÉGIMEN COMÚN", Helpers.Fuentes.SimexDatos);
                            prgLey2 = new Paragraph("NO SOMOS GRANDES CONTRIBUYENTES", Helpers.Fuentes.SimexDatos);
                            prgLey3 = new Paragraph("", Helpers.Fuentes.SimexDatos);
                            //prgLey5 = new Paragraph(ds.Tables["InvcHead"].Rows[0]["Resolucion"].ToString(), Helpers.Fuentes.SimexDatos);
                            break;
                        case Empresa.SOINCO:
                            prgLey1 = new Paragraph("IVA RÉGIMEN COMÚN", Helpers.Fuentes.SimexDatos);
                            prgLey2 = new Paragraph("NO SOMOS GRANDES CONTRIBUYENTES", Helpers.Fuentes.SimexDatos);
                            prgLey3 = new Paragraph("", Helpers.Fuentes.SimexDatos);
                            //prgLey5 = new Paragraph(ds.Tables["InvcHead"].Rows[0]["Resolucion"].ToString(), Helpers.Fuentes.SimexDatos);
                            break;
                        default:
                            break;
                    }

                    PdfPCell celLey1 = new PdfPCell();
                    celLey1.Border = 0;
                    celLey1.Colspan = 4;
                    celLey1.AddElement(prgLey1);
                    celLey1.AddElement(prgLey2);
                    celLey1.AddElement(prgLey3);
                    //celLey1.AddElement(prgLey5);
                    tbPiePagina.AddCell(celLey1);
                    //--------------- Leyenda 3 ---------------------------
                    tbPiePagina.CompleteRow();
                    //Paragraph prgLey4 = new Paragraph(string.Format("CUFE: {0}", CUFE), Helpers.Fuentes.SimexDatos);
                    //Paragraph prgRepresentacion = new Paragraph("Representacion gráfica factura electrónica", FontFactory.GetFont("Arial", 5f, iTextSharp.text.Font.NORMAL));
                    //prgRepresentacion.Alignment = Element.ALIGN_LEFT;
                    ////Paragraph prgLey5 = new Paragraph(string.Format("Pagina {0}", ""), Fuentes.SimexDatos);
                    ////Paragraph prgLey6 = new Paragraph(string.Format("Page"), Fuentes.SimexTituloIng);
                    //PdfPCell celLey2 = new PdfPCell();
                    //celLey2.Border = 0;
                    //celLey2.AddElement(prgLey4);
                    //celLey2.AddElement(prgRepresentacion);
                    ////celLey2.AddElement(prgLey5);
                    ////celLey2.AddElement(prgLey6);
                    //tbPiePagina.AddCell(celLey2);
                    //----Resolucion de la factura
                    Paragraph prgresolucion = new Paragraph(ds.Tables["InvcHead"].Rows[0]["Resolucion"].ToString(), Helpers.Fuentes.SimexDatos);
                    PdfPCell celResolucion = new PdfPCell();
                    celResolucion.Colspan = 5;
                    celResolucion.AddElement(prgresolucion);
                    celResolucion.Border = 0;
                    tbPiePagina.AddCell(celResolucion);

                    return tbPiePagina;
                }
                //----Clases para el anejo de datos
                //Clase parte
                public class Part
                {
                    public string PartNum { get; set; }
                    public int PartsPerContainer { get; set; }
                    public decimal NetWeight { get; set; }
                    public decimal GrossWeight { get; set; }
                }
                //Clase InvcDtl
                public class InvcDtl
                {
                    public int InvoiceNum { get; set; }
                    public int InvoiceLine { get; set; }
                    public string PartNum { get; set; }
                    public string SalesUM { get; set; }
                    public decimal SellingShipQty { get; set; }
                }
                //Clase CustXPrt
                public class CustXPrt
                {
                    public int CustNum { get; set; }
                    public string PartNum { get; set; }
                    public string XPartNum { get; set; }
                    public string PosArancelaria { get; set; }
                    public string PosArancelariaDes { get; set; }
                }
                //Clase datos encabezado de los documentos
                public class EncabezadoData
                {
                    public string Logo { get; set; }
                    public string Nombre { get; set; }
                    public string NIT { get; set; }
                    public string Direccion { get; set; }
                    public string Ubicacion { get; set; }
                    public string CodPostal { get; set; }
                    public string Telefono { get; set; }
                    public string Email { get; set; }
                    public string Web { get; set; }
                    public string TituloEsp { get; set; }
                    public string TituloIng { get; set; }
                }
            }
        }

        #endregion
    }
}
