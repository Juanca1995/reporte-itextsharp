﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

////Referenciar y usar.
//using System.Data;
//using System.IO;
//using iTextSharp;
//using iTextSharp.text;
//using iTextSharp.text.pdf;
//using System.Windows.Forms;
//using System.Web;
//using System.Net.NetworkInformation;
//using System.Net.Sockets;
//using System.Net;
//using System.Drawing;
//using System.Globalization;
//using static RulesServicesDIAN2.Adquiriente.pdfEstandarAR;

//namespace RulesServicesDIAN2.Adquiriente
//{
//    public partial class pdfEstandarAR
//    {

//        #region Hotel San juaquin
//        private bool AddUnidadesSanJoaquin(DataRow dataLine, ref PdfPTable table, iTextSharp.text.Font fontCustom, DataSet dataSet, decimal impuesto)
//        {
//            try
//            {
//                //(string)dataLine["SellingShipQty"], fontCustom)
//                PdfPCell celTextConcepto = new PdfPCell()
//                {
//                    //Border = PdfPCell.NO_BORDER,
//                    VerticalAlignment = Element.ALIGN_CENTER,
//                    Phrase = new Phrase((string)dataLine["LineDesc"], fontCustom),
//                    HorizontalAlignment = Element.ALIGN_LEFT,
//                    BorderWidthBottom = PdfPCell.NO_BORDER,
//                    BorderWidthTop = PdfPCell.NO_BORDER,
//                    //BorderWidthRight = PdfPCell.NO_BORDER,
//                };
//                table.AddCell(celTextConcepto);

//                PdfPCell celTextDetalle = new PdfPCell()
//                {
//                    //Border = PdfPCell.NO_BORDER,
//                    VerticalAlignment = Element.ALIGN_CENTER,
//                    Phrase = new Phrase(" ", fontCustom),
//                    HorizontalAlignment = Element.ALIGN_LEFT,
//                    BorderWidthRight = PdfPCell.NO_BORDER,
//                    BorderWidthBottom = PdfPCell.NO_BORDER,
//                    BorderWidthTop = PdfPCell.NO_BORDER,
//                    //BorderWidthLeft = PdfPCell.NO_BORDER,
//                };
//                table.AddCell(celTextDetalle);

//                PdfPCell celTextValor = new PdfPCell()
//                {
//                    //Border = PdfPCell.NO_BORDER,
//                    VerticalAlignment = Element.ALIGN_CENTER,
//                    Phrase = new Phrase(decimal.Parse((string)dataLine["DocUnitPrice"]).ToString("N0"), fontCustom),
//                    HorizontalAlignment = Element.ALIGN_RIGHT,
//                    BorderWidthRight = PdfPCell.NO_BORDER,
//                    BorderWidthBottom = PdfPCell.NO_BORDER,
//                    BorderWidthTop = PdfPCell.NO_BORDER,
//                };
//                table.AddCell(celTextValor);

//                //var impuesto = (decimal.Parse((string)dataLine["DocUnitPrice"])*
//                //    decimal.Parse(GetPercentIdImpDIAN((string)dataLine["InvoiceNum"], (string)dataLine["InvoiceLine"], dataSet.Tables["InvcTax"]).Trim(',','0','.')))/100;
//                PdfPCell celTextImpuestos = new PdfPCell()
//                {
//                    //Border = PdfPCell.NO_BORDER,
//                    VerticalAlignment = Element.ALIGN_CENTER,
//                    Phrase = new Phrase(impuesto.ToString("N0"), fontCustom),
//                    HorizontalAlignment = Element.ALIGN_RIGHT,
//                    BorderWidthRight = PdfPCell.NO_BORDER,
//                    BorderWidthBottom = PdfPCell.NO_BORDER,
//                    BorderWidthTop = PdfPCell.NO_BORDER,
//                };
//                table.AddCell(celTextImpuestos);

//                PdfPCell celTextTotal = new PdfPCell()
//                {
//                    //Border = PdfPCell.NO_BORDER,
//                    VerticalAlignment = Element.ALIGN_CENTER,
//                    Phrase = new Phrase(decimal.Parse((string)dataLine["DspDocExtPrice"]).ToString("N0"), fontCustom),
//                    HorizontalAlignment = Element.ALIGN_RIGHT,
//                    BorderWidthRight = PdfPCell.NO_BORDER,
//                    BorderWidthBottom = PdfPCell.NO_BORDER,
//                    BorderWidthTop = PdfPCell.NO_BORDER,
//                };
//                table.AddCell(celTextTotal);

//                PdfPCell celTextDescuento = new PdfPCell()
//                {
//                    //Border = PdfPCell.NO_BORDER,
//                    VerticalAlignment = Element.ALIGN_CENTER,
//                    Phrase = new Phrase(decimal.Parse((string)dataLine["DocDiscount"]).ToString("N0"), fontCustom),
//                    HorizontalAlignment = Element.ALIGN_RIGHT,
//                    BorderWidthRight = PdfPCell.NO_BORDER,
//                    BorderWidthBottom = PdfPCell.NO_BORDER,
//                    BorderWidthTop = PdfPCell.NO_BORDER,
//                };
//                table.AddCell(celTextDescuento);

//                PdfPCell celTextAbonos = new PdfPCell()
//                {
//                    //Border = PdfPCell.NO_BORDER,
//                    VerticalAlignment = Element.ALIGN_CENTER,
//                    Phrase = new Phrase("******", fontCustom),
//                    HorizontalAlignment = Element.ALIGN_RIGHT,
//                    BorderWidthRight = PdfPCell.NO_BORDER,
//                    BorderWidthBottom = PdfPCell.NO_BORDER,
//                    BorderWidthTop = PdfPCell.NO_BORDER,
//                };
//                table.AddCell(celTextAbonos);

//                PdfPCell celTextSaldo = new PdfPCell()
//                {
//                    //Border = PdfPCell.NO_BORDER,
//                    VerticalAlignment = Element.ALIGN_CENTER,
//                    Phrase = new Phrase("*****", fontCustom),
//                    HorizontalAlignment = Element.ALIGN_RIGHT,
//                    BorderWidthBottom = PdfPCell.NO_BORDER,
//                    BorderWidthTop = PdfPCell.NO_BORDER,
//                    //BorderWidthRight = PdfPCell.NO_BORDER,
//                    //BorderWidthLeft = PdfPCell.NO_BORDER,
//                };
//                table.AddCell(celTextSaldo);
//                return true;
//            }
//            catch (Exception ex)
//            {
//                MessageBox.Show(ex.ToString());
//                return false;
//            }
//        }

//        private bool AddUnidadesNcdSanJoaquin(DataRow dataLine, ref PdfPTable table, iTextSharp.text.Font fontTitleFactura, DataSet dataSet)
//        {
//            try
//            {
//                //CODIGO
//                strError += "Add InvoiceLine";
//                iTextSharp.text.pdf.PdfPCell celValL = new iTextSharp.text.pdf.PdfPCell(new Phrase((string)dataLine["InvoiceLine"], fontTitleFactura));
//                celValL.Colspan = 1;
//                celValL.Padding = 2;
//                celValL.Border = PdfPCell.LEFT_BORDER | PdfPCell.RIGHT_BORDER;
//                celValL.HorizontalAlignment = Element.ALIGN_CENTER;
//                celValL.VerticalAlignment = Element.ALIGN_TOP;
//                table.AddCell(celValL);
//                strError += "InvoiceLine OK";

//                //DESCRIPCIÓN ARTÍCULO
//                strError += "Add PartNum";
//                iTextSharp.text.pdf.PdfPCell celValCodigo = new iTextSharp.text.pdf.PdfPCell(new Phrase((string)dataLine["PartNumPartDescription"], fontTitleFactura));
//                celValCodigo.Colspan = 1;
//                celValCodigo.Padding = 2;
//                celValCodigo.Border = PdfPCell.RIGHT_BORDER;
//                //celValCodigo.BorderWidthLeft = 1;
//                //celDescripcion.BorderColorBottom = BaseColor.WHITE;
//                celValCodigo.HorizontalAlignment = Element.ALIGN_LEFT;
//                celValCodigo.VerticalAlignment = Element.ALIGN_TOP;
//                table.AddCell(celValCodigo);
//                strError += "Add PartNum OK";

//                //CANTIDAD
//                strError += "Add LineDesc";
//                iTextSharp.text.pdf.PdfPCell celValCantidad = new iTextSharp.text.pdf.PdfPCell(new Phrase((string)dataLine["PartNum"], fontTitleFactura));
//                celValCantidad.Colspan = 1;
//                celValCantidad.Padding = 2;
//                celValCantidad.Border = PdfPCell.RIGHT_BORDER;
//                //celValDesc.BorderWidthLeft = 1;
//                celValCantidad.HorizontalAlignment = Element.ALIGN_RIGHT;
//                celValCantidad.VerticalAlignment = Element.ALIGN_TOP;
//                table.AddCell(celValCantidad);
//                strError += "Add LineDesc OK";


//                //VR. UNITARIO
//                strError += "Add SellingShipQty";
//                iTextSharp.text.pdf.PdfPCell celValDescripcion = new iTextSharp.text.pdf.PdfPCell(new Phrase(
//                    decimal.Parse((string)dataLine["SalesUM"]).ToString("N0"), fontTitleFactura));
//                celValDescripcion.Colspan = 1;
//                celValDescripcion.Padding = 2;
//                celValDescripcion.Border = PdfPCell.RIGHT_BORDER;
//                //celValCantidad.BorderWidthLeft = 1;
//                celValDescripcion.HorizontalAlignment = Element.ALIGN_RIGHT;
//                celValDescripcion.VerticalAlignment = Element.ALIGN_TOP;
//                table.AddCell(celValDescripcion);
//                strError += "Add SellingShipQty OK";
//                return true;
//            }
//            catch (Exception ex)
//            {
//                strError += "1. Error al crear PDF: " + ex.Message;
//                return false;
//            }
//        }

//        #region FACTURA DE VENTAS
//        public bool FacturaNacionalaSanjuaquin(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
//        {
//            //NomArchivo = string.Empty;
//            //string Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
//            //string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
//            //string RutaImg = $@"{AppDomain.CurrentDomain.BaseDirectory}\LOGO_HOTEL_SAN_MARTIN.png";
//            //NomArchivo = NombreInvoice + ".pdf";

//            strError += "Lego";
//            NomArchivo = string.Empty;
//            string Ruta = HttpContext.Current.Server.MapPath("~/PDF/" + NIT + "/");
//            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
//            string RutaImg = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";
//            NomArchivo = NombreInvoice + ".pdf";
//            strError += "inicia Documento pdf\n";
//            try
//            {
//                #region Head
//                strError += "1\n";
//                //Validamos Existencia del Directorio.
//                if (!System.IO.Directory.Exists(Ruta))
//                    System.IO.Directory.CreateDirectory(Ruta);

//                //Valido la existencia previa de este archivo.
//                if (System.IO.File.Exists(Ruta + NomArchivo))
//                    System.IO.File.Delete(Ruta + NomArchivo);

//                //Dimenciones del documento.

//                Document document = new Document(iTextSharp.text.PageSize.LETTER);

//                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
//                Paragraph separator = new Paragraph("\n");
//                separator.Alignment = Element.ALIGN_CENTER;

//                // step 3: we open the document     
//                document.Open();

//                PdfDiv divEspacio = new PdfDiv();
//                divEspacio.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
//                divEspacio.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
//                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
//                divEspacio.Height = 10;
//                divEspacio.Width = 130;

//                PdfDiv divEspacio2 = new PdfDiv();
//                divEspacio2.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
//                divEspacio2.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
//                divEspacio2.Height = 2;
//                divEspacio2.Width = 130;

//                string valoresx = "xxxx";
//                //FUENTES----------------------------------------------------------------------------------------------------------
//                iTextSharp.text.Font fontTitleBold = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 8, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font fontCustom = FontFactory.GetFont(FontFactory.HELVETICA, 6f, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font fontCustomI = FontFactory.GetFont(FontFactory.HELVETICA_OBLIQUE, 6f, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font fontCustomBold = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 6, iTextSharp.text.Font.NORMAL);
//                //FUENTES---------------------------------------------------------------------------------------------------------
//                RoundRectangle CelEventBorderRound = new RoundRectangle();

//                strError += "2\n";

//                #endregion
//                strError += "2\n";
//                #region "ENCABEZADO"
//                System.Drawing.Image logo = null;
//                var requestLogo = WebRequest.Create(RutaImg);

//                PdfPTable tableEncabezado = new PdfPTable(new float[] { 2.0f, 3, 2.3f });
//                tableEncabezado.WidthPercentage = 100;

//                using (var responseLogo = requestLogo.GetResponse())
//                using (var streamLogo = responseLogo.GetResponseStream())
//                {
//                    logo = Bitmap.FromStream(streamLogo);
//                }

//                PdfPCell celLogoEncabezado = new PdfPCell()
//                {
//                    Border = PdfPCell.NO_BORDER
//                };

//                PdfDiv divLogo = new PdfDiv();
//                divLogo.Float = iTextSharp.text.pdf.PdfDiv.FloatType.LEFT;
//                divLogo.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
//                divLogo.BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID;
//                divLogo.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
//                divLogo.Height = 100;
//                divLogo.Width = 100;
//                //divLogo.BackgroundColor = BaseColor.LIGHT_GRAY;
//                iTextSharp.text.Image ImgLogo = iTextSharp.text.Image.GetInstance(logo, BaseColor.WHITE);
//                divLogo.BackgroundImage = ImgLogo;
//                celLogoEncabezado.AddElement(divLogo);
//                tableEncabezado.AddCell(celLogoEncabezado);
//                //---------------------------------------------------------------------------------------------------------
//                PdfDiv divAcercade = new PdfDiv();
//                divAcercade.Float = iTextSharp.text.pdf.PdfDiv.FloatType.RIGHT;
//                divAcercade.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
//                divAcercade.BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID;
//                divAcercade.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
//                //divAcercade.BackgroundColor = BaseColor.LIGHT_GRAY;
//                divAcercade.Width = 310;

//                iTextSharp.text.Font fontAcercadeBold = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 8f, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font fontAcercade = FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL);
//                //{(string)DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"]}
//                Paragraph prgTituloAcercade1 = new Paragraph($"HOTEL SAN juaquin .",
//                    FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 14f, iTextSharp.text.Font.NORMAL));
//                prgTituloAcercade1.Alignment = Element.ALIGN_CENTER;

//                Paragraph prgTituloAcercade2 = new Paragraph($"NIT  {(string)DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"]}-" +
//                    $"{CalcularDigitoVerificacion((string)DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"])}\n" +
//                    $"{(string)DsInvoiceAR.Tables["Company"].Rows[0]["Address1"]}" +
//                    $"TEL {"4445151"/*(string)DsInvoiceAR.Tables["Company"].Rows[0]["PhoneNum"]*/} -Fax{"2681316"/*(string)DsInvoiceAR.Tables["Company"].Rows[0]["FaxNum"]*/}\n" +
//                    $"{(string)DsInvoiceAR.Tables["Company"].Rows[0]["City"]} - COLOMBIA\n" +
//                    $"" +
//                    $"",
//                    fontCustomBold);
//                prgTituloAcercade2.Alignment = Element.ALIGN_CENTER;
//                PdfPCell celInfoEnc1 = new PdfPCell()
//                {
//                    Border = 0,
//                    Padding = 0,
//                    HorizontalAlignment = Element.ALIGN_CENTER
//                };
//                celInfoEnc1.AddElement(prgTituloAcercade1);
//                celInfoEnc1.AddElement(prgTituloAcercade2);
//                tableEncabezado.AddCell(celInfoEnc1);
//                //---------------------------------------------------------------------------------------------------------------------
//                Paragraph prgNoFactura = new Paragraph($"FACTURA DE VENTA No A {(string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"]}\nORIGINAL",
//                    FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 10f, iTextSharp.text.Font.NORMAL));
//                prgNoFactura.Alignment = Element.ALIGN_CENTER;

//                Paragraph prgNumeracionFactura = new Paragraph("",
//                    FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 6f, iTextSharp.text.Font.NORMAL));
//                prgNumeracionFactura.Alignment = Element.ALIGN_CENTER;

//                //Paragraph prgFechaFactura = new Paragraph($"Fecha Factura: " +
//                //    $"{DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Date01"]).ToString("DD/MM/YYYY")}", fontTitle);
//                //prgFechaFactura.Alignment = Element.ALIGN_RIGHT;

//                //Paragraph prgFechaVencimientoFactura = new Paragraph($"Fecha Vencimiento: " +
//                //    $"{DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Date01"]).ToString("DD/MM/YYYY")}", fontTitle);
//                //prgFechaVencimientoFactura.Alignment = Element.ALIGN_RIGHT;

//                //Paragraph prgHoraFactura = new Paragraph($"Hora: " +
//                //    $"{DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Date01"]).ToString("DD/MM/YYYY")}", fontTitle);
//                //prgHoraFactura.Alignment = Element.ALIGN_RIGHT;

//                PdfPCell celNoFactura = new PdfPCell()
//                {
//                    HorizontalAlignment = Element.ALIGN_CENTER,
//                    VerticalAlignment = Element.ALIGN_TOP,
//                    Padding = 0,
//                    Border = PdfPCell.NO_BORDER
//                };
//                celNoFactura.AddElement(prgNoFactura);
//                celNoFactura.AddElement(prgNumeracionFactura);
//                //celNoFactura.AddElement(prgFechaFactura);
//                //celNoFactura.AddElement(prgFechaVencimientoFactura);
//                //celNoFactura.AddElement(prgHoraFactura);
//                tableEncabezado.AddCell(celNoFactura);
//                #endregion
//                strError += "3\n";
//                #region Datos del Cliente
//                PdfPTable tableInfoCliente = new PdfPTable(new float[6] { 1.65f, 0.5f, 0.5f, 0.5f, 0.3f, 0.6f });
//                tableInfoCliente.WidthPercentage = 100;

//                PdfPCell celCompania = new PdfPCell(new Phrase($"Compañia:\n" +
//                    $"{(string)DsInvoiceAR.Tables["Customer"].Rows[0]["Name"]}", fontTitleBold))
//                {
//                    Padding = 2,
//                    PaddingBottom = 5
//                };
//                tableInfoCliente.AddCell(celCompania);

//                PdfPCell celNit = new PdfPCell(new Phrase($"C.C/Nit\n" +
//                    $"{(string)DsInvoiceAR.Tables["Customer"].Rows[0]["Company"]}", fontTitleBold))
//                {
//                    Colspan = 2,
//                    Padding = 2,
//                    PaddingBottom = 5
//                };
//                tableInfoCliente.AddCell(celNit);

//                PdfPCell celNoReserva = new PdfPCell(new Phrase("Reserva No.\n" +
//                    $"{(string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar02"]}", fontTitleBold))
//                {
//                    Padding = 2,
//                    PaddingBottom = 5
//                };
//                tableInfoCliente.AddCell(celNoReserva);

//                //----------------------------------------------------------------------------------------
//                PdfPTable tableHabitacionNoches = new PdfPTable(new float[] { 2, 1 });

//                PdfPCell celHabitacion = new PdfPCell(new Phrase("Habitación/Salón\n" +
//                    $"{(string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar03"]}", fontTitleBold))
//                {
//                    Padding = 2,
//                    PaddingBottom = 5
//                };
//                tableHabitacionNoches.AddCell(celHabitacion);

//                PdfPCell celNoches = new PdfPCell(new Phrase("Noches:\n" +
//                    $"{(string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number01"]}", fontTitleBold))
//                {
//                    Padding = 2,
//                    PaddingBottom = 5,
//                    HorizontalAlignment = Element.ALIGN_RIGHT
//                };
//                tableHabitacionNoches.AddCell(celNoches);

//                PdfPCell celHabitacionNoche = new PdfPCell(tableHabitacionNoches) { Colspan = 2 };
//                tableInfoCliente.AddCell(celHabitacionNoche);
//                //-------------------------------------------------------------------------------------
//                PdfPCell celDireccion = new PdfPCell(new Phrase("Dirección:\n" +
//                    $"{(string)DsInvoiceAR.Tables["Customer"].Rows[0]["Address1"]}", fontTitleBold))
//                {
//                    Padding = 2,
//                    PaddingBottom = 5
//                };
//                tableInfoCliente.AddCell(celDireccion);

//                PdfPCell celCiudad = new PdfPCell(new Phrase("Ciudad:\n" +
//                    $"{(string)DsInvoiceAR.Tables["Customer"].Rows[0]["City"]}", fontTitleBold))
//                {
//                    Padding = 2,
//                    PaddingBottom = 5
//                };
//                tableInfoCliente.AddCell(celCiudad);

//                PdfPCell celFechaLlegada = new PdfPCell(new Phrase("Fecha Llegada:\n+" +
//                    $"{ (string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Date01"]}", fontTitleBold))
//                {
//                    Padding = 2,
//                    PaddingBottom = 5
//                };
//                tableInfoCliente.AddCell(celFechaLlegada);

//                PdfPCell celFechaSalida = new PdfPCell(new Phrase("Fecha Salida:\n" +
//                    $"{ (string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Date02"]}", fontTitleBold))
//                {
//                    Padding = 2,
//                    PaddingBottom = 5
//                };
//                tableInfoCliente.AddCell(celFechaSalida);

//                PdfPCell celUsuario = new PdfPCell(new Phrase("Usuario:\n" +
//                    $"{ (string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar04"]}", fontTitleBold))
//                {
//                    Padding = 2,
//                    PaddingBottom = 5
//                };
//                tableInfoCliente.AddCell(celUsuario);

//                PdfPCell celCentroCosto = new PdfPCell(new Phrase("Centro de Costo:\n" +
//                    $"{ (string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar05"]}", fontTitleBold))
//                {
//                    Padding = 2,
//                    PaddingBottom = 5
//                };
//                tableInfoCliente.AddCell(celCentroCosto);
//                //----------------------------------------------------------------------------------------
//                PdfPCell celHuesped = new PdfPCell(new Phrase("Huesped:\n" +
//                    $"{ (string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar06"]}", fontTitleBold))
//                {
//                    Padding = 2,
//                    PaddingBottom = 5
//                };
//                tableInfoCliente.AddCell(celHuesped);

//                PdfPCell celTelefono = new PdfPCell(new Phrase("Teléfono:\n" +
//                    $"{(string)DsInvoiceAR.Tables["Customer"].Rows[0]["PhoneNum"]}", fontTitleBold))
//                {
//                    Padding = 2,
//                    HorizontalAlignment = Element.ALIGN_CENTER,
//                    PaddingBottom = 5,
//                    Colspan = 3
//                };
//                tableInfoCliente.AddCell(celTelefono);

//                PdfPTable tableNumPersonas = new PdfPTable(2);

//                PdfPCell celTextNumPersonas = new PdfPCell(new Phrase("Número de Personas:", fontTitleBold))
//                {
//                    Padding = 0,
//                    Border = 0,
//                    Colspan = 2,
//                };
//                tableNumPersonas.AddCell(celTextNumPersonas);

//                PdfPCell celNumAdultos = new PdfPCell(new Phrase("Adultos:" +
//                    $"{ (string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number02"]}", fontTitleBold))
//                {
//                    Padding = 2,
//                    Border = 0,
//                };
//                tableNumPersonas.AddCell(celNumAdultos);

//                PdfPCell celTextNumNinos = new PdfPCell(new Phrase("Niños: " +
//                    $"{ (string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number03"]}", fontTitleBold))
//                {
//                    Padding = 2,
//                    Border = 0,
//                };
//                tableNumPersonas.AddCell(celTextNumNinos);

//                PdfPCell celNumPersonas = new PdfPCell(tableNumPersonas)
//                {
//                    Padding = 2,
//                    Colspan = 2
//                };
//                tableInfoCliente.AddCell(celNumPersonas);

//                PdfPCell celEspacioInfoCliente = new PdfPCell(new Phrase(" "))
//                {
//                    Colspan = 6
//                };
//                tableInfoCliente.AddCell(celEspacioInfoCliente);
//                #endregion
//                strError += "4\n";
//                #region UNIDADES
//                PdfPTable tableEncabezadoUnidades = new PdfPTable(new float[] { 2.5f, 1.2f, 1, 1, 1.3f, 0.8f, 1, 1 });
//                tableEncabezadoUnidades.WidthPercentage = 100;
//                var fontEncabezado = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 6, BaseColor.WHITE);

//                PdfPCell celTextConcepto = new PdfPCell()
//                {
//                    //Border = PdfPCell.NO_BORDER,
//                    VerticalAlignment = Element.ALIGN_CENTER,
//                    Phrase = new Phrase("Concepto", fontEncabezado),
//                    HorizontalAlignment = Element.ALIGN_CENTER,
//                    BorderWidthRight = PdfPCell.NO_BORDER,
//                    BackgroundColor = BaseColor.LIGHT_GRAY,
//                };
//                tableEncabezadoUnidades.AddCell(celTextConcepto);

//                PdfPCell celTextDetalle = new PdfPCell()
//                {
//                    //Border = PdfPCell.NO_BORDER,
//                    VerticalAlignment = Element.ALIGN_CENTER,
//                    Phrase = new Phrase("Detalle", fontEncabezado),
//                    HorizontalAlignment = Element.ALIGN_CENTER,
//                    BorderWidthRight = PdfPCell.NO_BORDER,
//                    BorderWidthLeft = PdfPCell.NO_BORDER,
//                    BackgroundColor = BaseColor.LIGHT_GRAY,
//                };
//                tableEncabezadoUnidades.AddCell(celTextDetalle);

//                PdfPCell celTextValor = new PdfPCell()
//                {
//                    //Border = PdfPCell.NO_BORDER,
//                    VerticalAlignment = Element.ALIGN_CENTER,
//                    Phrase = new Phrase("Valor", fontEncabezado),
//                    HorizontalAlignment = Element.ALIGN_CENTER,
//                    BorderWidthRight = PdfPCell.NO_BORDER,
//                    BorderWidthLeft = PdfPCell.NO_BORDER,
//                    BackgroundColor = BaseColor.LIGHT_GRAY,
//                };
//                tableEncabezadoUnidades.AddCell(celTextValor);

//                PdfPCell celTextImpuestos = new PdfPCell()
//                {
//                    //Border = PdfPCell.NO_BORDER,
//                    VerticalAlignment = Element.ALIGN_CENTER,
//                    Phrase = new Phrase("Impuestos", fontEncabezado),
//                    HorizontalAlignment = Element.ALIGN_CENTER,
//                    BorderWidthRight = PdfPCell.NO_BORDER,
//                    BorderWidthLeft = PdfPCell.NO_BORDER,
//                    BackgroundColor = BaseColor.LIGHT_GRAY,
//                };
//                tableEncabezadoUnidades.AddCell(celTextImpuestos);

//                PdfPCell celTextTotal = new PdfPCell()
//                {
//                    //Border = PdfPCell.NO_BORDER,
//                    VerticalAlignment = Element.ALIGN_CENTER,
//                    Phrase = new Phrase("Total", fontEncabezado),
//                    HorizontalAlignment = Element.ALIGN_CENTER,
//                    BorderWidthRight = PdfPCell.NO_BORDER,
//                    BorderWidthLeft = PdfPCell.NO_BORDER,
//                    BackgroundColor = BaseColor.LIGHT_GRAY,
//                };
//                tableEncabezadoUnidades.AddCell(celTextTotal);

//                PdfPCell celTextDescuento = new PdfPCell()
//                {
//                    //Border = PdfPCell.NO_BORDER,
//                    VerticalAlignment = Element.ALIGN_CENTER,
//                    Phrase = new Phrase("Descuento", fontEncabezado),
//                    HorizontalAlignment = Element.ALIGN_CENTER,
//                    BorderWidthRight = PdfPCell.NO_BORDER,
//                    BorderWidthLeft = PdfPCell.NO_BORDER,
//                    BackgroundColor = BaseColor.LIGHT_GRAY,
//                };
//                tableEncabezadoUnidades.AddCell(celTextDescuento);

//                PdfPCell celTextAbonos = new PdfPCell()
//                {
//                    //Border = PdfPCell.NO_BORDER,
//                    VerticalAlignment = Element.ALIGN_CENTER,
//                    Phrase = new Phrase("Abonos", fontEncabezado),
//                    HorizontalAlignment = Element.ALIGN_CENTER,
//                    BorderWidthRight = PdfPCell.NO_BORDER,
//                    BorderWidthLeft = PdfPCell.NO_BORDER,
//                    BackgroundColor = BaseColor.LIGHT_GRAY,
//                };
//                tableEncabezadoUnidades.AddCell(celTextAbonos);

//                PdfPCell celTextSaldo = new PdfPCell()
//                {
//                    //Border = PdfPCell.NO_BORDER,
//                    VerticalAlignment = Element.ALIGN_CENTER,
//                    Phrase = new Phrase("Saldo", fontEncabezado),
//                    HorizontalAlignment = Element.ALIGN_CENTER,
//                    //BorderWidthRight = PdfPCell.NO_BORDER,
//                    BorderWidthLeft = PdfPCell.NO_BORDER,
//                    BackgroundColor = BaseColor.LIGHT_GRAY,
//                };
//                tableEncabezadoUnidades.AddCell(celTextSaldo);
//                //--------------------------------------------------------------------------------------
//                PdfPTable tableValUnidades = new PdfPTable(new float[] { 2.5f, 1.2f, 1, 1, 1.3f, 0.8f, 1, 1 });
//                tableValUnidades.WidthPercentage = 100;
//                decimal subTotalValor = 0, subTotalImpuesto = 0, subTotalTotal = 0, subTotalDescuento = 0, subTotalAbono = 0, subTotalSaldo = 0, totalIva16 = 0, totalIva19 = 0;

//                strError += "4.1\n";

//                foreach (DataRow InvoiceLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
//                {
//                    strError += "A\n";
//                    var porcentageIva = GetPercentIdImpDIAN((string)InvoiceLine["InvoiceNum"], (string)InvoiceLine["InvoiceLine"], DsInvoiceAR.Tables["InvcTax"]).Trim(',', '.');

//                    strError += porcentageIva.ToString() + "% B\n";
//                    var impuesto = (decimal.Parse((string)InvoiceLine["DocUnitPrice"]) * decimal.Parse(porcentageIva)) / 100;

//                    strError += "C\n";
//                    if (decimal.Parse(porcentageIva) == 16)
//                        totalIva16 += impuesto;
//                    else if (decimal.Parse(porcentageIva) == 19)
//                        totalIva19 += impuesto;

//                    strError += "D\n";
//                    if (!AddUnidadesSanJoaquin(InvoiceLine, ref tableValUnidades, fontCustom, DsInvoiceAR, impuesto))
//                        return false;

//                    strError += "E\n";
//                    subTotalValor += decimal.Parse((string)InvoiceLine["DocUnitPrice"]);
//                    subTotalImpuesto += impuesto;
//                    //subTotalTotal += decimal.Parse((string)InvoiceLine["DspDocExtPrice"]);
//                    //subTotalDescuento += decimal.Parse((string)InvoiceLine["DocDiscount"]);
//                    subTotalSaldo = decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"]);
//                    strError += "F\n";
//                }

//                strError += "4.2\n";

//                PdfPCell celTextSubtotales = new PdfPCell(new Phrase("Sub Totales", fontTitleBold))
//                {
//                    Colspan = 2,
//                    BorderWidthRight = PdfPCell.NO_BORDER,
//                    PaddingLeft = 25,
//                };
//                tableValUnidades.AddCell(celTextSubtotales);

//                PdfPCell celSubtotalValor = new PdfPCell(new Phrase(subTotalValor.ToString("N0"), fontTitleBold))
//                {
//                    BorderWidthRight = PdfPCell.NO_BORDER,
//                    BorderWidthLeft = PdfPCell.NO_BORDER,
//                    HorizontalAlignment = Element.ALIGN_RIGHT,
//                };
//                tableValUnidades.AddCell(celSubtotalValor);

//                PdfPCell celSubtotalImpuesto = new PdfPCell(new Phrase(subTotalImpuesto.ToString("N0"), fontTitleBold))
//                {
//                    BorderWidthRight = PdfPCell.NO_BORDER,
//                    BorderWidthLeft = PdfPCell.NO_BORDER,
//                    HorizontalAlignment = Element.ALIGN_RIGHT,
//                };
//                tableValUnidades.AddCell(celSubtotalImpuesto);

//                PdfPCell celSubtotalTotal = new PdfPCell(new Phrase(subTotalTotal.ToString("N0"), fontTitleBold))
//                {
//                    BorderWidthRight = PdfPCell.NO_BORDER,
//                    BorderWidthLeft = PdfPCell.NO_BORDER,
//                    HorizontalAlignment = Element.ALIGN_RIGHT,
//                };
//                tableValUnidades.AddCell(celSubtotalTotal);

//                PdfPCell celSubtotalDescuento = new PdfPCell(new Phrase(subTotalDescuento.ToString("N0"), fontTitleBold))
//                {
//                    BorderWidthRight = PdfPCell.NO_BORDER,
//                    BorderWidthLeft = PdfPCell.NO_BORDER,
//                    HorizontalAlignment = Element.ALIGN_RIGHT,
//                };
//                tableValUnidades.AddCell(celSubtotalDescuento);

//                PdfPCell celSubtotalAbono = new PdfPCell(new Phrase(subTotalAbono.ToString("N0"), fontTitleBold))
//                {
//                    BorderWidthRight = PdfPCell.NO_BORDER,
//                    BorderWidthLeft = PdfPCell.NO_BORDER,
//                    HorizontalAlignment = Element.ALIGN_RIGHT,
//                };
//                tableValUnidades.AddCell(celSubtotalAbono);

//                PdfPCell celSubtotal = new PdfPCell(new Phrase(subTotalSaldo.ToString("N0"), fontTitleBold))
//                {
//                    BorderWidthLeft = PdfPCell.NO_BORDER,
//                    HorizontalAlignment = Element.ALIGN_RIGHT,
//                };
//                tableValUnidades.AddCell(celSubtotal);
//                //----------------------------------------------------------------------------------

//                PdfPCell celRCargos = new PdfPCell()
//                {
//                    //Border = PdfPCell.NO_BORDER,
//                    VerticalAlignment = Element.ALIGN_CENTER,
//                    Phrase = new Phrase("Resumen Cargos", fontEncabezado),
//                    HorizontalAlignment = Element.ALIGN_LEFT,
//                    BorderWidthRight = PdfPCell.NO_BORDER,
//                    BackgroundColor = BaseColor.LIGHT_GRAY,
//                    Colspan = 4,
//                };
//                tableValUnidades.AddCell(celRCargos);

//                PdfPCell celRImpuestos = new PdfPCell()
//                {
//                    //Border = PdfPCell.NO_BORDER,
//                    VerticalAlignment = Element.ALIGN_CENTER,
//                    Phrase = new Phrase("Resumen Impuestos", fontEncabezado),
//                    HorizontalAlignment = Element.ALIGN_LEFT,
//                    BorderWidthLeft = PdfPCell.NO_BORDER,
//                    BackgroundColor = BaseColor.LIGHT_GRAY,
//                    Colspan = 4,
//                };
//                tableValUnidades.AddCell(celRImpuestos);
//                //------------------------------------------------------------------------------------
//                Paragraph prgNoCargosG = new Paragraph("Total Cargos No Gravados", fontTitle);
//                prgNoCargosG.Alignment = Element.ALIGN_RIGHT;

//                Paragraph prgCargosG = new Paragraph("Total Cargos Gravados:", fontTitle);
//                prgCargosG.Alignment = Element.ALIGN_RIGHT;

//                Paragraph prgCargosG8 = new Paragraph("Total Cargos Gravados 8%:", fontTitle);
//                prgCargosG8.Alignment = Element.ALIGN_RIGHT;

//                Paragraph prgCargosG16 = new Paragraph("Total Cargos Gravados 16%:", fontTitle);
//                prgCargosG16.Alignment = Element.ALIGN_RIGHT;

//                Paragraph prgCargosG19 = new Paragraph("Total Cargos Gravados 19%:", fontTitle);
//                prgCargosG19.Alignment = Element.ALIGN_RIGHT;

//                Paragraph prgTotalImpuestoConsumo = new Paragraph("Total Imp. Consumo 8%:", fontTitle);
//                prgTotalImpuestoConsumo.Alignment = Element.ALIGN_RIGHT;

//                Paragraph prgIva16 = new Paragraph("Total IVA 16%:", fontTitle);
//                prgIva16.Alignment = Element.ALIGN_RIGHT;

//                Paragraph prgIva19 = new Paragraph("Total IVA 19%:", fontTitle);
//                prgIva19.Alignment = Element.ALIGN_RIGHT;

//                Paragraph prgTotalIva = new Paragraph("Total IVA:", fontTitle);
//                prgTotalIva.Alignment = Element.ALIGN_RIGHT;

//                Paragraph prgTotalAbonosPagos = new Paragraph("Total Abonos y Pagos:", fontTitle);
//                prgTotalAbonosPagos.Alignment = Element.ALIGN_RIGHT;

//                Paragraph prgTotalCxC = new Paragraph("Total CxC:", fontTitle);
//                prgTotalCxC.Alignment = Element.ALIGN_RIGHT;

//                PdfPCell celValRCargos = new PdfPCell()
//                {
//                    //Border = PdfPCell.NO_BORDER,
//                    VerticalAlignment = Element.ALIGN_CENTER,
//                    HorizontalAlignment = Element.ALIGN_LEFT,
//                    BorderWidthRight = PdfPCell.NO_BORDER,
//                    BorderWidthBottom = PdfPCell.NO_BORDER,
//                    Colspan = 4,
//                };
//                celValRCargos.AddElement(prgNoCargosG);
//                celValRCargos.AddElement(prgCargosG);
//                celValRCargos.AddElement(prgCargosG8);
//                celValRCargos.AddElement(prgCargosG16);
//                celValRCargos.AddElement(prgCargosG19);
//                celValRCargos.AddElement(prgTotalImpuestoConsumo);
//                celValRCargos.AddElement(prgIva16);
//                celValRCargos.AddElement(prgIva19);
//                celValRCargos.AddElement(prgTotalIva);
//                celValRCargos.AddElement(prgTotalAbonosPagos);
//                celValRCargos.AddElement(prgTotalCxC);
//                tableValUnidades.AddCell(celValRCargos);
//                //------------------------------------------------------------------------------------
//                //Paragraph prgValNoCargosG = new Paragraph($"{(string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Name"]}", fontTitle);
//                //prgValNoCargosG.Alignment = Element.ALIGN_RIGHT; 

//                Paragraph prgValCargosG = new Paragraph(decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number05"]).ToString("N0"), fontTitle);
//                prgValCargosG.Alignment = Element.ALIGN_RIGHT;

//                Paragraph prgValCargosG8 = new Paragraph(decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number06"]).ToString("N0"), fontTitle);
//                prgValCargosG8.Alignment = Element.ALIGN_RIGHT;

//                Paragraph prgValCargosG16 = new Paragraph(decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number07"]).ToString("N0"), fontTitle);
//                prgValCargosG16.Alignment = Element.ALIGN_RIGHT;

//                Paragraph prgValCargosG19 = new Paragraph(decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number08"]).ToString("N0"), fontTitle);
//                prgValCargosG19.Alignment = Element.ALIGN_RIGHT;

//                Paragraph prgValTotalImpuestoConsumo = new Paragraph(decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number09"]).ToString("N0"), fontTitle);
//                prgValTotalImpuestoConsumo.Alignment = Element.ALIGN_RIGHT;

//                Paragraph prgValIva16 = new Paragraph(totalIva16.ToString("N0"), fontTitle);
//                prgValIva16.Alignment = Element.ALIGN_RIGHT;

//                Paragraph prgValIva19 = new Paragraph(totalIva19.ToString("N0"), fontTitle);
//                prgValIva19.Alignment = Element.ALIGN_RIGHT;

//                Paragraph prgValTotalIva = new Paragraph(decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["SalesRepName1"]).ToString("N0"), fontTitle);
//                prgValTotalIva.Alignment = Element.ALIGN_RIGHT;

//                Paragraph prgValTotalAbonosPagos = new Paragraph(decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number11"]).ToString("N0"), fontTitle);
//                prgValTotalAbonosPagos.Alignment = Element.ALIGN_RIGHT;

//                Paragraph prgValTotalCxC = new Paragraph(decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"]).ToString("N0"), fontTitle);
//                prgValTotalCxC.Alignment = Element.ALIGN_RIGHT;
//                PdfPCell celValRImpuestos = new PdfPCell()
//                {
//                    //Border = PdfPCell.NO_BORDER,
//                    VerticalAlignment = Element.ALIGN_CENTER,

//                    HorizontalAlignment = Element.ALIGN_LEFT,
//                    BorderWidthLeft = PdfPCell.NO_BORDER,
//                    BorderWidthBottom = PdfPCell.NO_BORDER,
//                    Colspan = 4,
//                };
//                //celValRImpuestos.AddElement(prgValNoCargosG);
//                celValRImpuestos.AddElement(prgValCargosG);
//                celValRImpuestos.AddElement(prgValCargosG8);
//                celValRImpuestos.AddElement(prgValCargosG16);
//                celValRImpuestos.AddElement(prgValCargosG19);
//                celValRImpuestos.AddElement(prgValTotalImpuestoConsumo);
//                celValRImpuestos.AddElement(prgValIva16);
//                celValRImpuestos.AddElement(prgValIva19);
//                celValRImpuestos.AddElement(prgValTotalIva);
//                celValRImpuestos.AddElement(prgValTotalAbonosPagos);
//                celValRImpuestos.AddElement(prgValTotalCxC);
//                celValRImpuestos.PaddingRight = 80;
//                tableValUnidades.AddCell(celValRImpuestos);

//                PdfPCell celVacioValUnidades = new PdfPCell(new Phrase(" "))
//                {
//                    Colspan = 8,
//                    BorderWidthBottom = PdfPCell.NO_BORDER,
//                    BorderWidthTop = PdfPCell.NO_BORDER,
//                };
//                var cant = Math.Abs(5 - DsInvoiceAR.Tables["InvcDtl"].Rows.Count);
//                for (int i = 0; i < cant; i++)
//                    tableValUnidades.AddCell(celVacioValUnidades);
//                //-----------------------------------------------------------------------------------
//                PdfPTable tableEmitidaPor = new PdfPTable(new float[] { 2, 1, 1.3f, 1.1f, 1 });
//                tableEmitidaPor.WidthPercentage = 100;

//                iTextSharp.text.Image QRPdf = iTextSharp.text.Image.GetInstance(QRInvoice, BaseColor.WHITE);

//                PdfDiv divQR = new PdfDiv()
//                {
//                    Float = iTextSharp.text.pdf.PdfDiv.FloatType.RIGHT,
//                    Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE,
//                    BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID,
//                    Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK,
//                    Height = 85,
//                    Width = 85,
//                };
//                divQR.AddElement(QRPdf);

//                PdfPCell celImgQR = new PdfPCell()
//                {
//                    Padding = 3,
//                    PaddingLeft = 15,
//                    HorizontalAlignment = iTextSharp.text.Element.ALIGN_RIGHT,
//                    VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP,
//                    Colspan = 5,
//                    Border = PdfPCell.LEFT_BORDER | PdfPCell.RIGHT_BORDER | PdfPCell.BOTTOM_BORDER,
//                };
//                celImgQR.AddElement(divQR);
//                tableEmitidaPor.AddCell(celImgQR);

//                PdfPCell celTextEmitidaPor = new PdfPCell(new Phrase("Emitida Por\n" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["SalesRepName1"].ToString(), fontTitle))
//                {
//                    HorizontalAlignment = Element.ALIGN_LEFT,
//                    VerticalAlignment = Element.ALIGN_TOP,
//                    PaddingBottom = 10,
//                };
//                //celTextEmitidaPor.AddElement(prgSubRallado);
//                tableEmitidaPor.AddCell(celTextEmitidaPor);

//                PdfPCell celTextTotalCargos = new PdfPCell(new Phrase($"Total Cargos: { decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number11"]).ToString("N0") }", fontCustomBold));
//                tableEmitidaPor.AddCell(celTextTotalCargos);

//                PdfPCell celTextTotalIva = new PdfPCell(new Phrase($"Total Iva: { decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DocTaxAmt"]).ToString("N0") }", fontCustomBold));
//                tableEmitidaPor.AddCell(celTextTotalIva);

//                PdfPCell celTextTotalAbonos = new PdfPCell(new Phrase($"Total Abonos: {subTotalAbono.ToString("N2")}", fontCustomBold));
//                tableEmitidaPor.AddCell(celTextTotalAbonos);

//                PdfPCell celTextSaldo2 = new PdfPCell(new Phrase($"Saldo: {decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number13"]).ToString("N2")}", fontCustomBold));
//                tableEmitidaPor.AddCell(celTextSaldo2);
//                #endregion
//                strError += "5\n";
//                #region Pie
//                PdfPTable tableTraspasoParticulares = new PdfPTable(2);
//                tableTraspasoParticulares.WidthPercentage = 100;

//                Paragraph prgTraspasoParticulares = new Paragraph();

//                PdfPCell celTextTParticulares = new PdfPCell(new Phrase(
//                    "TRASPASO A PARTICULARES", fontEncabezado))
//                {
//                    HorizontalAlignment = Element.ALIGN_CENTER,
//                    Padding = 0,
//                    PaddingBottom = 3,
//                    Border = PdfPCell.NO_BORDER,
//                    BackgroundColor = BaseColor.GRAY,
//                };
//                tableTraspasoParticulares.AddCell(celTextTParticulares);

//                PdfPCell celVacio = new PdfPCell(new Phrase(" ", fontCustom))
//                {
//                    HorizontalAlignment = Element.ALIGN_LEFT,
//                    Padding = 0,
//                    BorderWidthBottom = PdfPCell.NO_BORDER,
//                    //BorderWidthTop=1,
//                };
//                tableTraspasoParticulares.AddCell(celVacio);
//                //-------------------------------------------------------------------
//                PdfPTable tableFirma = new PdfPTable(new float[] { 1.5f, 1 });
//                tableFirma.WidthPercentage = 100;

//                PdfPCell celTextPagare = new PdfPCell(new Phrase(
//                   "Pagare incondicionalemente a la vista a la orden de Hotel Dann Carlton Medellin S.A." +
//                   "  en la ciudad de Medellin la suma a cancelar aqui especificada", fontCustom))
//                {
//                    HorizontalAlignment = Element.ALIGN_LEFT,
//                    Padding = 3,
//                    Border = PdfPCell.NO_BORDER,
//                };
//                tableFirma.AddCell(celTextPagare);

//                PdfPCell celTextFirma = new PdfPCell(new Phrase(
//                    "Firma:\n______________________________", fontCustom))
//                {
//                    HorizontalAlignment = Element.ALIGN_LEFT,
//                    Padding = 0,
//                    VerticalAlignment = Element.ALIGN_BOTTOM,
//                    Border = PdfPCell.NO_BORDER,
//                };
//                tableFirma.AddCell(celTextFirma);

//                PdfPCell celTextValorPagare = new PdfPCell(new Phrase(
//                   "El Valor de este pagare junto con los intereses a la tasa maxima " +
//                   "permitida por la ley", fontCustom))
//                {
//                    HorizontalAlignment = Element.ALIGN_LEFT,
//                    Padding = 3,
//                    Border = PdfPCell.NO_BORDER,
//                };
//                tableFirma.AddCell(celTextValorPagare);

//                PdfPCell celTextCC = new PdfPCell(new Phrase(
//                    "C.C", fontCustom))
//                {
//                    HorizontalAlignment = Element.ALIGN_LEFT,
//                    Padding = 3,
//                    Border = PdfPCell.NO_BORDER,
//                };
//                tableFirma.AddCell(celTextCC);

//                PdfPCell celFirma = new PdfPCell(tableFirma);
//                tableTraspasoParticulares.AddCell(celFirma);
//                //-------------------------------------------------------------------
//                PdfPTable tableAceptada = new PdfPTable(1);
//                tableAceptada.WidthPercentage = 100;

//                PdfPCell celTextAceptada = new PdfPCell(new Phrase(
//                    "ACEPTADA", FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 12, iTextSharp.text.Font.NORMAL)))
//                {
//                    HorizontalAlignment = Element.ALIGN_CENTER,
//                    Padding = 3,
//                    BorderWidthTop = PdfPCell.NO_BORDER,
//                };
//                tableAceptada.AddCell(celTextAceptada);

//                PdfPCell celVacioAceptada = new PdfPCell(new Phrase(
//                     " ", FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 12, iTextSharp.text.Font.NORMAL)))
//                {
//                    HorizontalAlignment = Element.ALIGN_CENTER,
//                    Padding = 10,
//                };
//                tableAceptada.AddCell(celVacioAceptada);
//                //tableAceptada.AddCell(celVacioAceptada);

//                PdfPCell celAceptada = new PdfPCell(tableAceptada);
//                celAceptada.BorderWidthTop = PdfPCell.NO_BORDER;
//                tableTraspasoParticulares.AddCell(celAceptada);

//                PdfPCell lastCelText = new PdfPCell(new Phrase(
//                   "Esta factura de venta se asimilara en sus efectos legales a una letra de cambio, segun ley 1231 de julio de 2008.  " +
//                   "Condiciones de Pago: Contado Esta factura es de pago inmediato de lo contrario causaria interes de mora a la tasa vigente\n" +
//                   "Consignar a la cuenta Banco de Occidente 415806017 de Ahorros  a nombre de Hotel Dann Carlton Medellin S.A\n" +
//                   "En virtud de la ley 1266 de 2008, el aceptante de la presente factura de venta AUTORIZA al creador de la misma para  ser consultado " +
//                   "y/o reportado ante las entidades de riesgo DATACREDITO, PROCREDITO , CIFIN o cualquier otra Entidad que llegare a crearse para tal fin. " +
//                   "Declara asķ mismo el creador de la presente factura de venta que las politicas y de protección de datos y habeas data consagradas en las " +
//                   "leyes 1266 de 2008 y 1581 de 2012, se encuentran publicadas en nuestra pįgina web: http://www.danncarlton.com, de ser consultadas por " +
//                   "el aceptante.\n Asi mismo con la aceptación de esta factura autorizo a HOTEL DANN CARLTON MEDELLIN S.A para tratar mi información personal",
//                   fontCustomBold))
//                {
//                    HorizontalAlignment = Element.ALIGN_LEFT,
//                    Padding = 0,
//                    PaddingBottom = 2,
//                    Colspan = 2,
//                    //Border = PdfPCell.NO_BORDER,
//                };
//                tableTraspasoParticulares.AddCell(lastCelText);
//                #endregion
//                strError += "6\n";
//                #region Exit
//                document.Add(tableEncabezado);
//                document.Add(divEspacio);
//                document.Add(tableInfoCliente);
//                document.Add(tableEncabezadoUnidades);
//                document.Add(tableValUnidades);
//                document.Add(tableEmitidaPor);
//                document.Add(tableTraspasoParticulares);

//                /*PIE DE PAGINA*/
//                PdfContentByte pCb = writer.DirectContent;
//                PieDePagina(ref pCb);

//                writer.Flush();
//                document.Close();

//                RutaPdf = NomArchivo;
//                return true;
//                #endregion
//            }
//            catch (Exception ex)
//            {
//                strError += "1. Error al crear PDF: " + ex.Message;
//                return false;
//            }
//        }
//        #endregion

//        #region NOTA CREDITO
//        public bool NotaCreditoHotelSSanjuaquinn(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
//        {
//            //NomArchivo = string.Empty;
//            //string Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
//            //string RutaImg = $@"{AppDomain.CurrentDomain.BaseDirectory}\LOGO_HOTEL_SAN_MARTIN.png";
//            //string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
//            //NomArchivo = NombreInvoice + ".pdf";

//            strError += "Lego";
//            NomArchivo = string.Empty;
//            string Ruta = HttpContext.Current.Server.MapPath("~/PDF/" + NIT + "/");
//            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
//            string RutaImg = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";
//            NomArchivo = NombreInvoice + ".pdf";
//            strError += "inicia Documento pdf\n";
//            try
//            {
//                #region Head
//                //Validamos Existencia del Directorio.
//                if (!System.IO.Directory.Exists(Ruta))
//                    System.IO.Directory.CreateDirectory(Ruta);

//                //Valido la existencia previa de este archivo.
//                if (System.IO.File.Exists(Ruta + NomArchivo))
//                    System.IO.File.Delete(Ruta + NomArchivo);

//                //Dimenciones del documento.

//                Document document = new Document(iTextSharp.text.PageSize.LETTER, 20f, 10f, 30f, 30f);

//                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
//                Paragraph separator = new Paragraph("\n");
//                separator.Alignment = Element.ALIGN_CENTER;

//                // step 3: we open the document     
//                document.Open();

//                PdfDiv divEspacio = new PdfDiv();
//                divEspacio.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
//                divEspacio.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
//                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
//                divEspacio.Height = 10;
//                divEspacio.Width = 130;

//                PdfDiv divEspacio2 = new PdfDiv();
//                divEspacio2.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
//                divEspacio2.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
//                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
//                divEspacio2.Height = 2;
//                divEspacio2.Width = 130;

//                //FUENTES----------------------------------------------------------------------------------------------------------
//                iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font fontTitle2 = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font fontCustom = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font fontTitleFecha = FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font fontDMA = FontFactory.GetFont(FontFactory.HELVETICA, 6, BaseColor.GRAY);
//                iTextSharp.text.Font fontDetalleFecha = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 8, iTextSharp.text.Font.NORMAL);
//                iTextSharp.text.Font fontTitleFactura = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
//                //FUENTES---------------------------------------------------------------------------------------------------------


//                /////saltos de trabla 
//                PdfPTable salto = new PdfPTable(1);
//                salto.WidthPercentage = 100;
//                PdfPCell vacio = new PdfPCell() { MinimumHeight = 2, Border = 0, };
//                salto.AddCell(vacio);
//                ////-------------------------------------
//                ///

//                System.Drawing.Image logo = null;
//                var requestLogo = WebRequest.Create(RutaImg);

//                using (var responseLogo = requestLogo.GetResponse())
//                using (var streamLogo = responseLogo.GetResponseStream())
//                {
//                    logo = Bitmap.FromStream(streamLogo);
//                }

//                PdfDiv divLogo = new PdfDiv();
//                divLogo.Float = iTextSharp.text.pdf.PdfDiv.FloatType.LEFT;
//                divLogo.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
//                divLogo.BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID;
//                divLogo.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
//                divLogo.Height = 60;
//                divLogo.Width = 145;
//                //divLogo.BackgroundColor = BaseColor.LIGHT_GRAY;
//                iTextSharp.text.Image ImgLogo = iTextSharp.text.Image.GetInstance(logo, BaseColor.WHITE);
//                divLogo.BackgroundImage = ImgLogo;
//                //---------------------------------------------------------------------------------------------------------
//                PdfDiv divAcercade = new PdfDiv();
//                divAcercade.Float = iTextSharp.text.pdf.PdfDiv.FloatType.LEFT;
//                divAcercade.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
//                divAcercade.BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID;
//                divAcercade.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
//                //divAcercade.BackgroundColor = BaseColor.LIGHT_GRAY;
//                //divAcercade.PaddingLeft = 5;
//                //divAcercade.PaddingRight = 10;
//                divAcercade.Width = 140;


//                iTextSharp.text.Image QRPdf = iTextSharp.text.Image.GetInstance(QRInvoice, BaseColor.WHITE);
//                QRPdf.ScaleAbsolute(100, 100);
//                QRPdf.Border = 0;

//                #endregion

//                #region Header

//                PdfPTable header = new PdfPTable(new float[] { 0.30f, 0.50f, 0.20f, });
//                header.WidthPercentage = 100;
//                //agregamos logo
//                PdfPCell cell_logo = new PdfPCell() { Border = 0, };
//                cell_logo.AddElement(ImgLogo);

//                //agregamos cufe y numero de factura
//                PdfPCell cell_factura = new PdfPCell() { Border = 0, };

//                ///agregamos Qr
//                PdfPCell cell_Qr = new PdfPCell() { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, };
//                cell_Qr.AddElement(QRPdf);

//                header.AddCell(cell_logo);
//                header.AddCell(cell_factura);
//                header.AddCell(cell_Qr);

//                PdfPTable encabezado = new PdfPTable(1);
//                encabezado.WidthPercentage = 100;
//                PdfPCell cabecera = new PdfPCell() { Border = 0, };

//                PdfPTable Advanced = new PdfPTable(2);
//                Advanced.WidthPercentage = 100;

//                PdfPCell info_inicio = new PdfPCell();
//                //agregamos el nimbre de la empresa
//                PdfPTable nombre = new PdfPTable(1);
//                nombre.WidthPercentage = 100;
//                PdfPCell nombre_em = new PdfPCell(new Phrase(DsInvoiceAR.Tables["Company"].Rows[0]["Name"].ToString(), fontTitle2)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP, MinimumHeight = 50, };
//                nombre.AddCell(nombre_em);
//                info_inicio.AddElement(nombre);

//                //agregamos el codigo postal de la empresa
//                PdfPTable c_postal = new PdfPTable(1);
//                c_postal.WidthPercentage = 100;
//                PdfPCell postal_ = new PdfPCell(new Phrase("C. Postal: " + DsInvoiceAR.Tables["Company"].Rows[0]["ShortChar01"].ToString(), fontTitle2)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP, };
//                c_postal.AddCell(postal_);
//                info_inicio.AddElement(c_postal);

//                //agregamos el codigo postal de la empresa
//                PdfPTable pais = new PdfPTable(1);
//                pais.WidthPercentage = 100;
//                PdfPCell pais_ = new PdfPCell(new Phrase("Pais: " + DsInvoiceAR.Tables["Company"].Rows[0]["Country"].ToString(), fontTitle2)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP, };
//                pais.AddCell(pais_);
//                info_inicio.AddElement(pais);


//                //agregamos el telefono postal de la empresa
//                PdfPTable telefono = new PdfPTable(1);
//                telefono.WidthPercentage = 100;
//                PdfPCell telefono_ = new PdfPCell(new Phrase("Telefono: " + DsInvoiceAR.Tables["Company"].Rows[0]["PhoneNum"].ToString(), fontTitle2)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP, };
//                telefono.AddCell(telefono_);
//                info_inicio.AddElement(telefono);


//                //agregamos el fical postal de la empresa
//                PdfPTable fiscal = new PdfPTable(1);
//                fiscal.WidthPercentage = 100;
//                PdfPCell fiscal_ = new PdfPCell(new Phrase("Nŗ Fiscal: " + DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"].ToString() + "-" + CalcularDigitoVerificacion(DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"].ToString()), fontTitle2)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP, };
//                fiscal.AddCell(fiscal_);
//                info_inicio.AddElement(fiscal);

//                Advanced.AddCell(info_inicio);

//                PdfPCell info_inicio2 = new PdfPCell() { Border = 0, };

//                PdfPTable Adquiriente = new PdfPTable(1);
//                Adquiriente.WidthPercentage = 100;
//                PdfPCell div = new PdfPCell(new Phrase(DsInvoiceAR.Tables["Customer"].Rows[0]["Name"].ToString(), fontTitle));
//                Adquiriente.AddCell(div);
//                info_inicio2.AddElement(Adquiriente);

//                string infoAdquiriente = "Dirección: " + DsInvoiceAR.Tables["Customer"].Rows[0]["Address1"].ToString() + "\nCiudad: " + DsInvoiceAR.Tables["Customer"].Rows[0]["City"].ToString() + "\nTelefono: " + DsInvoiceAR.Tables["Customer"].Rows[0]["PhoneNum"].ToString();
//                PdfPTable tablaInfoAdquiriente = new PdfPTable(1);
//                tablaInfoAdquiriente.WidthPercentage = 100;
//                PdfPCell cell_InfoAdquiriente = new PdfPCell(new Phrase(infoAdquiriente, fontTitle2)) { MinimumHeight = 35, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP, };
//                tablaInfoAdquiriente.AddCell(cell_InfoAdquiriente);
//                info_inicio2.AddElement(tablaInfoAdquiriente);

//                PdfPTable tablaEsp = new PdfPTable(1);
//                tablaEsp.WidthPercentage = 100;
//                PdfPCell cell_esp = new PdfPCell() { Border = 0, MinimumHeight = 8, };
//                tablaEsp.AddCell(cell_esp);
//                info_inicio2.AddElement(tablaEsp);

//                PdfPTable nr = new PdfPTable(1);
//                nr.WidthPercentage = 100;
//                PdfPCell nr_r = new PdfPCell(new Phrase("Nŗ Fiscal: " + DsInvoiceAR.Tables["Customer"].Rows[0]["ResaleID"].ToString(), fontTitle));
//                nr.AddCell(nr_r);
//                info_inicio2.AddElement(nr);

//                Advanced.AddCell(info_inicio2);

//                cabecera.AddElement(Advanced);

//                encabezado.AddCell(cabecera);

//                #endregion

//                #region Body

//                PdfPTable cabeceta_nota = new PdfPTable(new float[] { 0.35f, 0.15f, 0.10f, 0.18f, 0.35f });
//                cabeceta_nota.WidthPercentage = 100;


//                PdfPCell numerof = new PdfPCell() { };
//                //agregamos el campo para el numero de la factura
//                PdfPTable _numerof_ = new PdfPTable(1);
//                _numerof_.WidthPercentage = 100;
//                PdfPCell numerof_ = new PdfPCell(new Phrase("Nota de Crédito", fontTitle2)) { HorizontalAlignment = Element.ALIGN_CENTER, Border = 0, };
//                _numerof_.AddCell(numerof_);
//                numerof.AddElement(_numerof_);
//                //agregamos el numeo de la factura
//                PdfPTable _numerof_r = new PdfPTable(1);
//                _numerof_r.WidthPercentage = 100;
//                PdfPCell numerof_r = new PdfPCell(new Phrase((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"], fontTitle2)) { HorizontalAlignment = Element.ALIGN_CENTER, Border = 0, };
//                _numerof_r.AddCell(numerof_r);
//                numerof.AddElement(_numerof_r);


//                PdfPCell fecha = new PdfPCell();
//                //agregamos el campo para el fehca de la factura
//                PdfPTable _fecha_ = new PdfPTable(1);
//                _fecha_.WidthPercentage = 100;
//                PdfPCell fecha_ = new PdfPCell(new Phrase("Fecha", fontTitle2)) { HorizontalAlignment = Element.ALIGN_CENTER, Border = 0, };
//                _fecha_.AddCell(fecha_);
//                fecha.AddElement(_fecha_);
//                //agregamos la fecha de la factura
//                PdfPTable _fecha_r = new PdfPTable(1);
//                _fecha_r.WidthPercentage = 100;
//                PdfPCell fecha_r = new PdfPCell(new Phrase(DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).ToString("dd/MM/yyyy"), fontTitle2)) { HorizontalAlignment = Element.ALIGN_CENTER, Border = 0, };
//                _fecha_r.AddCell(fecha_r);
//                fecha.AddElement(_fecha_r);

//                PdfPCell moneda = new PdfPCell();
//                //agregamos el campo para la moneda de la factura
//                PdfPTable _Moneda_ = new PdfPTable(1);
//                _Moneda_.WidthPercentage = 100;
//                PdfPCell Moneda_ = new PdfPCell(new Phrase("Moneda", fontTitle2)) { HorizontalAlignment = Element.ALIGN_CENTER, Border = 0, };
//                _Moneda_.AddCell(Moneda_);
//                moneda.AddElement(_Moneda_);
//                //agregamos la moneda de la factura
//                PdfPTable _Moneda_r = new PdfPTable(1);
//                _Moneda_r.WidthPercentage = 100;
//                PdfPCell Moneda_r = new PdfPCell(new Phrase((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["CurrencyCode"], fontTitle2)) { HorizontalAlignment = Element.ALIGN_CENTER, Border = 0, };
//                _Moneda_r.AddCell(Moneda_r);
//                moneda.AddElement(_Moneda_r);

//                PdfPCell sobre = new PdfPCell();
//                //agregamos el campo para la moneda de la factura
//                PdfPTable _sobre_ = new PdfPTable(1);
//                _sobre_.WidthPercentage = 100;
//                PdfPCell sobre_ = new PdfPCell(new Phrase("Sobre Factura", fontTitle2)) { HorizontalAlignment = Element.ALIGN_CENTER, Border = 0, };
//                _sobre_.AddCell(sobre_);
//                sobre.AddElement(_sobre_);
//                //agregamos la moneda de la factura
//                PdfPTable _sobre_r = new PdfPTable(1);
//                _sobre_r.WidthPercentage = 100;
//                PdfPCell sobre_r = new PdfPCell(new Phrase((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceRef"], fontTitle2)) { HorizontalAlignment = Element.ALIGN_CENTER, Border = 0, };
//                _sobre_r.AddCell(sobre_r);
//                sobre.AddElement(_sobre_r);


//                PdfPCell paginas = new PdfPCell() { Border = 0, };
//                //agregamos el campo para la moneda de la factura
//                PdfPTable _paginas_ = new PdfPTable(1);
//                _paginas_.WidthPercentage = 100;
//                PdfPCell paginas_ = new PdfPCell(new Phrase("pagina " + document.PageNumber, fontTitle2)) { HorizontalAlignment = Element.ALIGN_CENTER, };
//                _paginas_.AddCell(paginas_);
//                paginas.AddElement(_paginas_);
//                //agregamos la moneda de la factura
//                PdfPTable _paginas_r = new PdfPTable(1);
//                _paginas_r.WidthPercentage = 100;
//                PdfPCell paginas_r = new PdfPCell(new Phrase("COPIA\nCONTRAVALOR", fontTitle2)) { HorizontalAlignment = Element.ALIGN_CENTER, Border = 0, };
//                _paginas_r.AddCell(paginas_r);
//                paginas.AddElement(_paginas_r);

//                cabeceta_nota.AddCell(numerof);
//                cabeceta_nota.AddCell(fecha);
//                cabeceta_nota.AddCell(moneda);
//                cabeceta_nota.AddCell(sobre);
//                cabeceta_nota.AddCell(paginas);


//                #endregion

//                #region unidades

//                //Dimenciones.
//                float[] DimencionUnidades = new float[4];
//                DimencionUnidades[0] = 0.10f;//L
//                DimencionUnidades[1] = 0.60f;//Codigo
//                DimencionUnidades[2] = 0.15f;//Descripcion
//                DimencionUnidades[3] = 0.15f;//Unidad

//                PdfPTable tableTituloUnidades = new PdfPTable(DimencionUnidades);
//                tableTituloUnidades.WidthPercentage = 100;

//                PdfPCell fecha_d = new PdfPCell(new Phrase("Fecha", fontTitle2)) { HorizontalAlignment = Element.ALIGN_CENTER, };
//                PdfPCell Descripción = new PdfPCell(new Phrase("Descripción", fontTitle2)) { HorizontalAlignment = Element.ALIGN_LEFT, };
//                PdfPCell Total = new PdfPCell(new Phrase("Total", fontTitle2)) { HorizontalAlignment = Element.ALIGN_RIGHT };
//                PdfPCell Saldo = new PdfPCell(new Phrase("Saldo", fontTitle2)) { HorizontalAlignment = Element.ALIGN_RIGHT };

//                tableTituloUnidades.AddCell(fecha_d);
//                tableTituloUnidades.AddCell(Descripción);
//                tableTituloUnidades.AddCell(Total);
//                tableTituloUnidades.AddCell(Saldo);


//                PdfPTable tableUnidades = new PdfPTable(4);
//                tableUnidades.WidthPercentage = 100;
//                tableUnidades.SetWidths(DimencionUnidades);
//                decimal totalDescuento = 0;
//                foreach (DataRow InvoiceLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
//                {
//                    if (!AddUnidadesNcdSanJoaquin(InvoiceLine, ref tableUnidades, fontCustom, DsInvoiceAR))
//                        return false;
//                    totalDescuento += decimal.Parse((string)InvoiceLine["DspDocLessDiscount"]);
//                }

//                int numAdLineas = 10 - DsInvoiceAR.Tables["InvcDtl"].Rows.Count;
//                PdfPTable tableEspacioUnidades = new PdfPTable(4);
//                tableEspacioUnidades.WidthPercentage = 100;
//                tableEspacioUnidades.SetWidths(DimencionUnidades);

//                PdfPTable tableLineaFinal = new PdfPTable(4);
//                tableLineaFinal.WidthPercentage = 100;
//                tableLineaFinal.SetWidths(DimencionUnidades);
//                iTextSharp.text.pdf.PdfPCell LineaFinal = new iTextSharp.text.pdf.PdfPCell();
//                LineaFinal.Colspan = 4;
//                LineaFinal.Padding = 3;
//                LineaFinal.BorderColorBottom = BaseColor.WHITE;
//                LineaFinal.HorizontalAlignment = Element.ALIGN_CENTER;
//                LineaFinal.VerticalAlignment = Element.ALIGN_TOP;
//                tableLineaFinal.AddCell(LineaFinal);

//                #endregion

//                #region Footer

//                PdfPTable total = new PdfPTable(new float[] { 0.10f, 0.60f, 0.15f, 0.15f, });
//                total.WidthPercentage = 100;

//                PdfPCell vacio1 = new PdfPCell(new Phrase("", fontTitle2)) { HorizontalAlignment = Element.ALIGN_CENTER, };
//                PdfPCell vacio2 = new PdfPCell(new Phrase("", fontTitle2)) { HorizontalAlignment = Element.ALIGN_LEFT, };
//                PdfPCell vacio3 = new PdfPCell(new Phrase("Total", fontTitle2)) { HorizontalAlignment = Element.ALIGN_RIGHT, };
//                PdfPCell vacio4 = new PdfPCell(new Phrase(decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"]).ToString("C2"), fontTitle2)) { HorizontalAlignment = Element.ALIGN_RIGHT, };


//                total.AddCell(vacio1);
//                total.AddCell(vacio2);
//                total.AddCell(vacio3);
//                total.AddCell(vacio4);

//                PdfPTable pie = new PdfPTable(2);
//                pie.WidthPercentage = 100;

//                PdfPCell porsentuales = new PdfPCell() { MinimumHeight = 30, Padding = 0, };

//                PdfPTable insidencia = new PdfPTable(3);
//                insidencia.WidthPercentage = 100;
//                //agregamos el 19%
//                PdfPCell porcentaje = new PdfPCell(new Phrase("%\n19",
//                    fontTitle2))
//                { HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_BOTTOM, MinimumHeight = 35, };
//                insidencia.AddCell(porcentaje);
//                //agregamos insidencia
//                PdfPCell insi = new PdfPCell(new Phrase("insidencia\n" + decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number15"]).ToString("C2"),
//                    fontTitle2))
//                { HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_BOTTOM };
//                insidencia.AddCell(insi);
//                //agregamos iva
//                PdfPCell iva = new PdfPCell(new Phrase("iva",
//                    fontTitle2))
//                { HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_BOTTOM, MinimumHeight = 20, };
//                insidencia.AddCell(iva);
//                porsentuales.AddElement(insidencia);


//                PdfPTable insidencia2 = new PdfPTable(3);
//                insidencia2.WidthPercentage = 100;
//                //agregamos el 19%
//                PdfPCell porsentaje2 = new PdfPCell(new Phrase("",
//                    fontTitle2))
//                { HorizontalAlignment = Element.ALIGN_RIGHT };
//                insidencia2.AddCell(porsentaje2);
//                //agregamos insidencia
//                PdfPCell insi2 = new PdfPCell(new Phrase(decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number15"]).ToString("C2"),
//                    fontTitle2))
//                { HorizontalAlignment = Element.ALIGN_RIGHT };
//                insidencia2.AddCell(insi2);
//                //agregamos iva
//                PdfPCell iva2 = new PdfPCell(new Phrase(decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DocTaxAmt"]).ToString("C2"),
//                    fontTitle2))
//                { HorizontalAlignment = Element.ALIGN_RIGHT, MinimumHeight = 25, };
//                insidencia2.AddCell(iva2);
//                porsentuales.AddElement(insidencia2);

//                PdfPCell funcionario = new PdfPCell() { };

//                PdfPTable func = new PdfPTable(1);
//                //func.WidthPercentage = 100;
//                //agregamos el nombred el funcionario 
//                PdfPCell funcionari = new PdfPCell(new Phrase("Funcionario",
//                    fontTitle2))
//                { HorizontalAlignment = Element.ALIGN_CENTER, MinimumHeight = 20, Padding = 3, };
//                func.AddCell(funcionari);
//                funcionario.AddElement(func);

//                PdfPTable firma = new PdfPTable(1);
//                //func.WidthPercentage = 100;
//                //agregamos el la firma 
//                PdfPCell fima_ = new PdfPCell(new Phrase("_________________________________________________________" +
//                                                        "Firma/Signature",
//                                                        fontTitle2))
//                { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_BOTTOM, MinimumHeight = 30, Padding = 3, };
//                firma.AddCell(fima_);
//                funcionario.AddElement(firma);


//                pie.AddCell(porsentuales);
//                pie.AddCell(funcionario);

//                PdfPTable procesado = new PdfPTable(1);
//                procesado.WidthPercentage = 100;
//                PdfPCell procesad = new PdfPCell(new Phrase("Procesado por computadora", fontTitle2)) { HorizontalAlignment = Element.ALIGN_RIGHT, Border = 0, };
//                procesado.AddCell(procesad);

//                #endregion

//                #region Exit

//                document.Add(header);
//                document.Add(encabezado);
//                document.Add(salto);
//                document.Add(cabeceta_nota);
//                document.Add(salto);
//                document.Add(salto);
//                document.Add(tableTituloUnidades);
//                document.Add(tableUnidades);
//                document.Add(tableEspacioUnidades);
//                document.Add(tableLineaFinal);
//                document.Add(total);
//                document.Add(pie);
//                document.Add(procesado);

//                //document.Add(divTextBancoCheque);
//                /*PIE DE PAGINA*/
//                PdfContentByte pCb = writer.DirectContent;
//                PieDePagina(ref pCb);

//                writer.Flush();
//                document.Close();
//                RutaPdf = NomArchivo;
//                return true;
//                #endregion

//            }
//            catch (Exception ex)
//            {
//                strError += "1. Error al crear PDF: " + ex.Message;
//                return false;
//            }
//        }
//        #endregion

//        #endregion

//    }
//}
