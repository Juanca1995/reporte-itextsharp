﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//Referenciar y usar.
using System.Data;
using System.IO;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Windows.Forms;
using System.Web;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Net;
using System.Drawing;
using System.Globalization;
using static RulesServicesDIAN2.Adquiriente.pdfEstandarAR;

namespace RulesServicesDIAN2.Adquiriente
{
    public partial class pdfEstandarAR
    {

        #region "Formatos Facturas Asocaba"

        public bool Asocaba(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {
            //NomArchivo = string.Empty;
            //string Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
            //string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            //string RutaImg = $@"{AppDomain.CurrentDomain.BaseDirectory}\Asocaba.png";
            //NomArchivo = NombreInvoice + ".pdf";

            NomArchivo = string.Empty;
            string Ruta = HttpContext.Current.Server.MapPath("~/PDF/" + NIT + "/");
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            string RutaImg = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";
            NomArchivo = NombreInvoice + ".pdf";
            try
            {
                #region Hesd
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.
                Document document = new Document(iTextSharp.text.PageSize.LETTER);
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;

                // step 3: we open the document
                document.Open();

                /*TABLA FACTURA*/
                iTextSharp.text.pdf.PdfPTable tableInvoice = new iTextSharp.text.pdf.PdfPTable(5);
                tableInvoice.HorizontalAlignment = Element.ALIGN_CENTER;

                //Dimenciones.
                float[] DimencionInvoice = new float[5];
                DimencionInvoice[0] = 2.0F;//CODIGO
                DimencionInvoice[1] = 5.0F;//Descripcion
                DimencionInvoice[2] = 2.0F;//CANTIDAD
                DimencionInvoice[3] = 3.0F;//Precio Unitario
                DimencionInvoice[4] = 4.0F;//VR TOTAL                                

                tableInvoice.WidthPercentage = 100;
                tableInvoice.SetWidths(DimencionInvoice);
                #endregion

                #region Encabezado
                PdfPTable tableEncabezado = new PdfPTable(new float[] { 1.6f, 0.8f, 0.8f, 1.75f, 0.8f, 0.8f })
                {
                    WidthPercentage = 100,
                };

                //Celda Logo
                System.Drawing.Image Logo;
                //System.Drawing.Image LogoBanner = null;

                var request = WebRequest.Create(RutaImg);

                using (var response = request.GetResponse())
                using (var stream = response.GetResponseStream())
                {
                    Logo = Bitmap.FromStream(stream);
                }

                iTextSharp.text.Image LogoPdf = iTextSharp.text.Image.GetInstance(Logo, BaseColor.WHITE);

                iTextSharp.text.pdf.PdfPCell celLogoImg = new iTextSharp.text.pdf.PdfPCell();
                celLogoImg.AddElement(LogoPdf);
                celLogoImg.Colspan = 1;
                celLogoImg.Padding = 3;
                celLogoImg.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celLogoImg.VerticalAlignment = iTextSharp.text.Element.ALIGN_BOTTOM;
                celLogoImg.Border = 0;
                tableEncabezado.AddCell(celLogoImg);

                string strTitulo1 = "";

                strTitulo1 = string.Empty;

                iTextSharp.text.Font fontTitle1 = FontFactory.GetFont(FontFactory.HELVETICA, 5, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.pdf.PdfPCell celTittle1 = new iTextSharp.text.pdf.PdfPCell(new Phrase(strTitulo1, fontTitle1));
                celTittle1.Colspan = 1;
                celTittle1.Padding = 3;
                celTittle1.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celTittle1.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                celTittle1.Border = 0;
                tableEncabezado.AddCell(celTittle1);

                string strTitulo2 = "";

                strTitulo2 = string.Empty;

                iTextSharp.text.pdf.PdfPCell celTittle2 = new iTextSharp.text.pdf.PdfPCell(new Phrase(strTitulo2, fontTitle1));
                celTittle2.Colspan = 1;
                celTittle2.Padding = 3;
                celTittle2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celTittle2.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                celTittle2.Border = 0;
                tableEncabezado.AddCell(celTittle2);

                iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 10, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitleSubtittle = FontFactory.GetFont(FontFactory.HELVETICA, 5, iTextSharp.text.Font.NORMAL);
                Paragraph prgTitle = new Paragraph(DsInvoiceAR.Tables["Company"].Rows[0]["Name"].ToString(), fontTitle);
                prgTitle.Alignment = Element.ALIGN_CENTER;
                Paragraph prgTitle3 = new Paragraph("REALIZADA POR " +
                    DsInvoiceAR.Tables["Company"].Rows[0]["Name"] +
                    " NIT: " + DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"] + "-" +
                    CalcularDigitoVerificacion((string)DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"]), fontTitleSubtittle);
                prgTitle3.Alignment = Element.ALIGN_CENTER;

                string strTitulo3 = "";

                Paragraph prgTitle4 = new Paragraph(strTitulo3, fontTitle1);
                prgTitle4.Alignment = Element.ALIGN_CENTER;

                iTextSharp.text.pdf.PdfPCell cellTitle = new iTextSharp.text.pdf.PdfPCell();
                cellTitle.AddElement(prgTitle);
                cellTitle.AddElement(prgTitle3);
                cellTitle.AddElement(prgTitle4);
                cellTitle.Padding = 3;
                cellTitle.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                cellTitle.VerticalAlignment = iTextSharp.text.Element.ALIGN_BOTTOM;
                cellTitle.Border = 0;
                cellTitle.BorderColorRight = BaseColor.WHITE;
                tableEncabezado.AddCell(cellTitle);

                string strTitulo4 = "";
                strTitulo4 = string.Empty;

                iTextSharp.text.pdf.PdfPCell celTittle4 = new iTextSharp.text.pdf.PdfPCell(new Phrase(strTitulo4, fontTitle1));
                celTittle4.Colspan = 1;
                celTittle4.Padding = 3;
                celTittle4.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celTittle4.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                celTittle4.Border = 0;
                tableEncabezado.AddCell(celTittle4);

                string strTitulo5 = "";

                strTitulo5 = string.Empty;

                iTextSharp.text.pdf.PdfPCell celTittle5 = new iTextSharp.text.pdf.PdfPCell(new Phrase(strTitulo5, fontTitle1));
                celTittle5.Padding = 3;
                celTittle5.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celTittle5.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                celTittle5.Border = 0;
                tableEncabezado.AddCell(celTittle5);

                iTextSharp.text.Font fontLogo = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.BOLD);

                string TipoFactura = string.Empty;
                if (InvoiceType == "InvoiceType")
                {
                    TipoFactura = $"NIT: {(string)DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"]}-" +
                        $"{CalcularDigitoVerificacion((string)DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"])}\n" +
                        $"FACTURA DE VENTA N°\n\n" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"].ToString();
                }
                else if (InvoiceType == "CreditNoteType" || InvoiceType == "DebitNoteType")
                {
                    TipoFactura = $"NIT: {(string)DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"]}-" +
                        $"{CalcularDigitoVerificacion((string)DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"])}\n" +
                        $"NOTA CREDITO N°\n\n" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"].ToString();
                }

                //Celda LegalNum
                iTextSharp.text.Font fontLegalNum = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.pdf.PdfPCell celLegalNum = new iTextSharp.text.pdf.PdfPCell(new Phrase(TipoFactura, fontLegalNum));
                celLegalNum.Colspan = 6;
                celLegalNum.Padding = 3;
                celLegalNum.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                celLegalNum.VerticalAlignment = iTextSharp.text.Element.ALIGN_BOTTOM;
                celLegalNum.Border = 0;
                tableEncabezado.AddCell(celLegalNum);

                //Celda Regimen Comun
                iTextSharp.text.Font fontRegComun = FontFactory.GetFont(FontFactory.HELVETICA, 5, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.pdf.PdfPCell celRegComun = new iTextSharp.text.pdf.PdfPCell(new Phrase("IVA RÉGIMEN COMÚN - " +
                    "NO SOMOS AUTORETENEDORES - SOMOS RETENEDORES DE RETE FUENTE \n\n", fontRegComun));
                celRegComun.Colspan = 6;
                celRegComun.Padding = 3;
                celRegComun.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                celRegComun.VerticalAlignment = iTextSharp.text.Element.ALIGN_BOTTOM;
                celRegComun.Border = PdfPCell.NO_BORDER;
                tableEncabezado.AddCell(celRegComun);
                #endregion

                #region InfoCliente_InfoFactura

                PdfPTable tableContInfoCliente_Factura = new PdfPTable(new float[] { 1.8f, 2, 0.8f })
                { WidthPercentage = 100, };

                /*VENDIDO A*/
                iTextSharp.text.Font fontVendidoA = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontVendidoASubTittle = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);

                Paragraph prgVendidoA = new Paragraph("SEÑORES", fontVendidoA);
                prgVendidoA.Alignment = Element.ALIGN_LEFT;

                string VendidoA = DsInvoiceAR.Tables["Customer"].Rows[0]["Name"].ToString() + "\n" +
                                  DsInvoiceAR.Tables["Customer"].Rows[0]["Address1"].ToString() + "\n" +
                                  DsInvoiceAR.Tables["Customer"].Rows[0]["City"].ToString() + "\n" +
                                  DsInvoiceAR.Tables["Customer"].Rows[0]["State"].ToString() + "\n\n" +
                                  "NIT/CEDULA   " + DsInvoiceAR.Tables["Customer"].Rows[0]["ResaleID"].ToString();

                Paragraph prgVendidoASubTittle = new Paragraph(VendidoA, fontVendidoASubTittle);
                prgVendidoASubTittle.Alignment = Element.ALIGN_LEFT;

                iTextSharp.text.pdf.PdfPCell cellVendidoA = new iTextSharp.text.pdf.PdfPCell();
                cellVendidoA.AddElement(prgVendidoA);
                cellVendidoA.AddElement(prgVendidoASubTittle);
                cellVendidoA.Padding = 3;
                cellVendidoA.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellVendidoA.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableContInfoCliente_Factura.AddCell(cellVendidoA);

                /*INFO PEDIDO*/
                iTextSharp.text.Font fontInfoPedido = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);
                {
                    iTextSharp.text.pdf.PdfPTable tableInfoPedido = new iTextSharp.text.pdf.PdfPTable(5);
                    tableInfoPedido.HorizontalAlignment = Element.ALIGN_CENTER;
                    tableInfoPedido.WidthPercentage = 100;

                    //1-1 Confirma Pedido.
                    iTextSharp.text.pdf.PdfPCell cellConfirmaPedido = new iTextSharp.text.pdf.PdfPCell(new Phrase("CONFIRMACION PEDIDO\n" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar01"].ToString() + "\n\n", fontInfoPedido));
                    cellConfirmaPedido.Colspan = 1;
                    cellConfirmaPedido.Padding = 1;
                    cellConfirmaPedido.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                    cellConfirmaPedido.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    tableInfoPedido.AddCell(cellConfirmaPedido);

                    //1-2 No Pedido
                    iTextSharp.text.pdf.PdfPCell cellNoPedido = new iTextSharp.text.pdf.PdfPCell(new Phrase("No. PEDIDO\n" +
                        DsInvoiceAR.Tables["InvcHead"].Rows[0]["OrderNum"].ToString() + "\n\n", fontInfoPedido));
                    cellNoPedido.Colspan = 1;
                    cellNoPedido.Padding = 1;
                    cellNoPedido.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                    cellNoPedido.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    tableInfoPedido.AddCell(cellNoPedido);

                    //1-3 Orden de Compra                
                    iTextSharp.text.pdf.PdfPCell cellOrdenCompra = new iTextSharp.text.pdf.PdfPCell(
                        new Phrase("ORDEN DE COMPRA\n" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["PONum"].ToString() + "\n\n", fontInfoPedido));
                    cellOrdenCompra.Colspan = 1;
                    cellOrdenCompra.Padding = 1;
                    cellOrdenCompra.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                    cellOrdenCompra.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    tableInfoPedido.AddCell(cellOrdenCompra);

                    //1-4 Fecha Vencimiento
                    iTextSharp.text.pdf.PdfPCell cellDueDate = new iTextSharp.text.pdf.PdfPCell(
                        new Phrase("FECHA VENCIMIENTO:\n" +
                        Convert.ToDateTime(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"]).ToString("dd/MM/yyyy") + "\n\n", fontInfoPedido));
                    cellDueDate.Colspan = 1;
                    cellDueDate.Padding = 1;
                    cellDueDate.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                    cellDueDate.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    tableInfoPedido.AddCell(cellDueDate);

                    //1-5 Fecha Factura
                    iTextSharp.text.pdf.PdfPCell cellInvoiceDate = new iTextSharp.text.pdf.PdfPCell(new Phrase("FECHA FACTURA:\n" +
                        Convert.ToDateTime(DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).ToString("dd/MM/yyyy") + "\n\n", fontInfoPedido));
                    cellInvoiceDate.Colspan = 1;
                    cellInvoiceDate.Padding = 1;
                    cellInvoiceDate.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                    cellInvoiceDate.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    tableInfoPedido.AddCell(cellInvoiceDate);

                    //2-1 Telefono
                    iTextSharp.text.pdf.PdfPCell cellTelefono = new iTextSharp.text.pdf.PdfPCell(new Phrase("TELÉFONO\n" +
                        DsInvoiceAR.Tables["Customer"].Rows[0]["PhoneNum"].ToString(), fontInfoPedido));
                    cellTelefono.Colspan = 1;
                    cellTelefono.Padding = 1;
                    cellTelefono.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                    cellTelefono.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    tableInfoPedido.AddCell(cellTelefono);

                    //2-2 InvoiceNum
                    iTextSharp.text.pdf.PdfPCell cellNoFacRef = new iTextSharp.text.pdf.PdfPCell(new Phrase("Nro. Fact. REF\n" +
                        DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceNum"].ToString(), fontInfoPedido));
                    cellNoFacRef.Colspan = 1;
                    cellNoFacRef.Padding = 1;
                    cellNoFacRef.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                    cellNoFacRef.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    tableInfoPedido.AddCell(cellNoFacRef);

                    //2-3 PLAZO MESES
                    iTextSharp.text.pdf.PdfPCell cellTermsCode = new iTextSharp.text.pdf.PdfPCell(
                        new Phrase("PLAZO MESES\n" + DsInvoiceAR.Tables["Customer"].Rows[0]["TermsDescription"].ToString(), fontInfoPedido));
                    cellTermsCode.Colspan = 1;
                    cellTermsCode.Padding = 1;
                    cellTermsCode.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                    cellTermsCode.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    tableInfoPedido.AddCell(cellTermsCode);

                    //2-4 VENDEDOR
                    iTextSharp.text.pdf.PdfPCell cellVendedor = new iTextSharp.text.pdf.PdfPCell(new Phrase("VENDEDOR\n" +
                        DsInvoiceAR.Tables["InvcHead"].Rows[0]["SalesRepName1"].ToString(), fontInfoPedido));
                    cellVendedor.Colspan = 1;
                    cellVendedor.Padding = 1;
                    cellVendedor.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                    cellVendedor.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    tableInfoPedido.AddCell(cellVendedor);

                    //2-4 FECHA ENTREGA
                    iTextSharp.text.pdf.PdfPCell cellFechaEntrega = new iTextSharp.text.pdf.PdfPCell(new Phrase("FECHA DE ENTREGA\n" +
                        Convert.ToDateTime(DsInvoiceAR.Tables["InvcHead"].Rows[0]["Date01"]).ToString("dd/MM/yyyy") + "\n\n", fontInfoPedido));
                    cellFechaEntrega.Colspan = 1;
                    cellFechaEntrega.Padding = 1;
                    cellFechaEntrega.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                    cellFechaEntrega.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    tableInfoPedido.AddCell(cellFechaEntrega);

                    tableContInfoCliente_Factura.AddCell(new PdfPCell(tableInfoPedido) { Padding = 3, });
                }

                /*CELL QR*/
                iTextSharp.text.Image QRPdf = iTextSharp.text.Image.GetInstance(QRInvoice, BaseColor.WHITE);
                QRPdf.ScaleAbsolute(80f, 80f);//Se cambia el tamaño de la imagen del QR
                iTextSharp.text.pdf.PdfPCell celImgQR = new iTextSharp.text.pdf.PdfPCell(QRPdf);
                celImgQR.Colspan = 1;
                celImgQR.Padding = 3;
                celImgQR.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celImgQR.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableContInfoCliente_Factura.AddCell(celImgQR);
                #endregion

                iTextSharp.text.pdf.PdfPCell cellBlank = new iTextSharp.text.pdf.PdfPCell(new Phrase("\n", fontInfoPedido));
                cellBlank.Colspan = 3;
                cellBlank.Padding = 3;
                cellBlank.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellBlank.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                cellBlank.BorderWidth = 0;
                tableContInfoCliente_Factura.AddCell(cellBlank);

                #region Unidades

                PdfPTable tableUnidades = new PdfPTable(new float[] { 1, 2.5f, 1, 1.3f, 2 })
                {
                    WidthPercentage = 100,
                };

                tableUnidades.AddCell(new PdfPCell(new Phrase($"CUFE: {CUFE}", fontVendidoASubTittle))
                { Border = PdfPCell.NO_BORDER, Colspan = 2, });

                tableUnidades.AddCell(new PdfPCell(new Phrase($"Representación gráfica factura electrónica", fontVendidoASubTittle))
                { Border = PdfPCell.NO_BORDER, Colspan = 3, HorizontalAlignment = Element.ALIGN_RIGHT });

                //Configuramos las columnas.
                if (!AddColumnsInvoiceLineAsocaba(ref tableUnidades))
                    return false;

                /*Tabla - ITEMS FACTURA.*/
                //Filas Factura
                iTextSharp.text.Font fuenteDatos = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);

                decimal TotalDescountInvc = 0;

                //Lineas de Factura.
                foreach (DataRow InvoiceLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
                {
                    //Totales de las Lineas
                    TotalDescountInvc += Convert.ToDecimal(InvoiceLine["DspDocLessDiscount"]);
                    if (!AddRowInvoiceAsocaba(InvoiceLine, ref tableUnidades, fuenteDatos, DsInvoiceAR.Tables["InvcTax"]))
                        return false;
                }

                iTextSharp.text.pdf.PdfPCell cellShipToAddres = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    " ", fuenteDatos));
                cellShipToAddres.Colspan = 7;
                cellShipToAddres.Padding = 3;
                cellShipToAddres.BorderWidthLeft = 0;
                cellShipToAddres.BorderWidthRight = 0;
                cellShipToAddres.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellShipToAddres.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableInvoice.AddCell(cellShipToAddres);
                #endregion

                #region PiePag  
                PdfPTable tablePiePag = new PdfPTable(new float[] { 4, 2.5f }) { WidthPercentage = 100, };

                tablePiePag.AddCell(new PdfPCell()
                {
                    Border = PdfPCell.NO_BORDER,
                    Colspan = 2,
                    FixedHeight = 10,
                });

                iTextSharp.text.Font fuenteObs = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.pdf.PdfPCell cellObs = new iTextSharp.text.pdf.PdfPCell(new Phrase("OBSERVACIONES:" +
                    DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceComment"].ToString(), fuenteObs));
                cellObs.Padding = 3;
                cellObs.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellObs.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tablePiePag.AddCell(cellObs);
                string Resolucion = string.Empty;

                iTextSharp.text.pdf.PdfPCell cellResolucion = new iTextSharp.text.pdf.PdfPCell();
                cellResolucion.Colspan = 3;
                cellResolucion.Padding = 3;
                cellResolucion.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellResolucion.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tablePiePag.AddCell(cellResolucion);

                iTextSharp.text.Font fuenteObs2 = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);
                //Observacion 3 Factura.                
                iTextSharp.text.pdf.PdfPCell cellObs3 = new iTextSharp.text.pdf.PdfPCell(
                    new Phrase(DsInvoiceAR.Tables["InvcHead"].Rows[0]["Character01"].ToString(), fuenteObs2));
                cellObs3.Padding = 3;
                cellObs3.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellObs3.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tablePiePag.AddCell(cellObs3);

                PdfPTable tableSubtotal = new PdfPTable(2) { WidthPercentage = 100, };

                /*TOTAL Factura.*/
                iTextSharp.text.Font fontTotalesTittle = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTotales = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);

                tableSubtotal.AddCell(new PdfPCell(new Phrase("SUBTOTAL", fontTotalesTittle))
                {
                    Border = PdfPCell.RIGHT_BORDER | PdfPCell.LEFT_BORDER,
                    HorizontalAlignment = iTextSharp.text.Element.ALIGN_RIGHT,
                    VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP,
                });

                //1-2 SUBTOTAL VALUE
                decimal SubTotal = Convert.ToDecimal(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubTotal"].ToString());

                tableSubtotal.AddCell(new PdfPCell(new Phrase(SubTotal.ToString("N2"), fontTotales))
                {
                    Border = PdfPCell.LEFT_BORDER | PdfPCell.RIGHT_BORDER,
                    HorizontalAlignment = iTextSharp.text.Element.ALIGN_RIGHT,
                    VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP,
                });

                tablePiePag.AddCell(new PdfPCell(tableSubtotal) { Border = PdfPCell.NO_BORDER, });

                PdfPTable tableEmisor_Firmas = new PdfPTable(3);

                /*INFORMACION ENTREGA.*/
                iTextSharp.text.Font fontInfoEntrega = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);

                //Emisor Factura.
                Paragraph prgEmisor = new Paragraph("EMISOR DE FACTURA", fontInfoEntrega);
                prgEmisor.Alignment = Element.ALIGN_LEFT;

                iTextSharp.text.Image LogoPdf2 = iTextSharp.text.Image.GetInstance(Logo, BaseColor.WHITE);
                LogoPdf2.ScaleAbsolute(60f, 60f);

                iTextSharp.text.pdf.PdfPCell cellEmisor = new iTextSharp.text.pdf.PdfPCell();
                cellEmisor.AddElement(prgEmisor);
                //cellEmisor.AddElement(LogoPdf2);
                cellEmisor.AddElement(new Phrase((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Character02"],
                    fontInfoEntrega));
                cellEmisor.Padding = 3;
                cellEmisor.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                cellEmisor.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                tableEmisor_Firmas.AddCell(cellEmisor);

                //Leyenda de resoluciones
                tableEmisor_Firmas.AddCell(new PdfPCell(new Phrase((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Character03"],
                    fontInfoEntrega))
                { });


                //RECIBO DE MERCANCIA.
                string ReciboMercancia = "RECIBO DE MERCANCÍA\n\n" +
                                         "FIRMA\n\n" +
                                         "_________________________\n\n" +
                                         "NOMBRE\n\n" +
                                         "_________________________\n\n" +
                                         "FECHA dd/mm/aaaa\n\n" +
                                         "_________________________\n\n" +
                                         "CC";
                tableEmisor_Firmas.AddCell(new PdfPCell(new Phrase(ReciboMercancia, fontInfoEntrega)) { });

                tablePiePag.AddCell(new PdfPCell(tableEmisor_Firmas)
                {
                    //Border =PdfPCell.NO_BORDER,
                    Padding = 2,
                });

                //----------------------Totales-------------------------------------
                PdfPTable tableTotales = new PdfPTable(2) { WidthPercentage = 100, };

                tableTotales.AddCell(new PdfPCell(new Phrase("DESCUENTO", fontTotalesTittle))
                {
                    Border = PdfPCell.RIGHT_BORDER,
                    HorizontalAlignment = Element.ALIGN_RIGHT,
                    VerticalAlignment = Element.ALIGN_TOP,
                });

                tableTotales.AddCell(new PdfPCell(new Phrase(TotalDescountInvc.ToString("N2"), fontTotales))
                {
                    Border = PdfPCell.LEFT_BORDER,
                    HorizontalAlignment = Element.ALIGN_RIGHT,
                    VerticalAlignment = Element.ALIGN_TOP,
                });

                tableTotales.AddCell(new PdfPCell(new Phrase("IVA", fontTotalesTittle))
                {
                    Border = PdfPCell.RIGHT_BORDER,
                    HorizontalAlignment = Element.ALIGN_RIGHT,
                    VerticalAlignment = Element.ALIGN_TOP,
                });

                //4-2 IVA VALUE                                
                decimal Iva = Convert.ToDecimal(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DocTaxAmt"].ToString());
                tableTotales.AddCell(new PdfPCell(new Phrase(Iva.ToString("N2"), fontTotales))
                {
                    Border = PdfPCell.LEFT_BORDER,
                    HorizontalAlignment = Element.ALIGN_RIGHT,
                    VerticalAlignment = Element.ALIGN_TOP,
                });

                tableTotales.AddCell(new PdfPCell(new Phrase("RETE FUENTE", fontTotalesTittle))
                {
                    Border = PdfPCell.RIGHT_BORDER,
                    HorizontalAlignment = Element.ALIGN_RIGHT,
                    VerticalAlignment = Element.ALIGN_TOP,
                });

                //4-2 IVA VALUE                                
                decimal reteFuente = GetValImpuestoByID("0A", DsInvoiceAR);
                tableTotales.AddCell(new PdfPCell(new Phrase(reteFuente.ToString("N2"), fontTotales))
                {
                    Border = PdfPCell.LEFT_BORDER,
                    HorizontalAlignment = Element.ALIGN_RIGHT,
                    VerticalAlignment = Element.ALIGN_TOP,
                });

                PdfPTable tableTotalesCop = new PdfPTable(new float[] { 0.7f, 0.3f, 1 })
                {
                    WidthPercentage = 100,
                };

                tableTotalesCop.AddCell(new PdfPCell(new Phrase("TOTAL",
                    FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 8, iTextSharp.text.Font.NORMAL, BaseColor.WHITE)))
                { BackgroundColor = BaseColor.BLACK, Border = PdfPCell.TOP_BORDER, });

                tableTotalesCop.AddCell(new PdfPCell(new Phrase(
                    (String)DsInvoiceAR.Tables["InvcHead"].Rows[0]["CurrencyCodeCurrencyID"], fontTotalesTittle))
                {
                    Border = PdfPCell.TOP_BORDER,
                    HorizontalAlignment = Element.ALIGN_RIGHT,
                    VerticalAlignment = Element.ALIGN_TOP,
                });

                decimal Total = Convert.ToDecimal(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"].ToString());
                tableTotalesCop.AddCell(new PdfPCell(new Phrase(Total.ToString("N2"), fontTotales))
                {
                    Border = PdfPCell.TOP_BORDER,
                    HorizontalAlignment = Element.ALIGN_RIGHT,
                    VerticalAlignment = Element.ALIGN_TOP,
                });


                tableTotales.AddCell(new PdfPCell(tableTotalesCop)
                { Border = PdfPCell.NO_BORDER, Colspan = 2, });

                tableTotales.AddCell(new PdfPCell()
                {
                    Border = PdfPCell.NO_BORDER,
                    Colspan = 2,
                });

                tablePiePag.AddCell(new PdfPCell(tableTotales)
                {
                    Border = PdfPCell.RIGHT_BORDER | PdfPCell.LEFT_BORDER | PdfPCell.BOTTOM_BORDER,
                    HorizontalAlignment = Element.ALIGN_RIGHT,
                    VerticalAlignment = Element.ALIGN_TOP,
                    PaddingLeft = 2,
                    PaddingRight = 2,
                });


                tablePiePag.AddCell(new PdfPCell(new Phrase("ESTA FACTURA DE VENTA ES UN TITULO " +
                    "VALOR SEGUN LEY 1231 DE JULIO 17 DE 2008. DECLARAMOS HABER RECIBIDO DE CONFORMIDAD" +
                    " REAL Y MATERIALMENTE LAS MERCANCIAS OBJETO DE LA PRESENTE." +
                    "OBLIGANDONOS A SU PAGO EN LA FORMA AQUI PACTADA MAS EL RECARGO POR INTEERSES DE MORA %MENSUAL.", fuenteObs2))
                {
                    Colspan = 2,
                    Border = PdfPCell.NO_BORDER,
                });
                //---------------------------------------------------------------------------------------------------
                PdfPTable tableSucursales = new PdfPTable(8) { WidthPercentage = 100, PaddingTop = 10, };

                tableSucursales.AddCell(new PdfPCell() { FixedHeight = 10, Border = PdfPCell.NO_BORDER, Colspan = 8, });

                string _strTitulo1 = "";

                tableSucursales.AddCell(new PdfPCell(new Phrase(_strTitulo1, fontTitle1))
                {
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    VerticalAlignment = Element.ALIGN_TOP,
                    Border = PdfPCell.NO_BORDER,
                });

                string _strTitulo2 = "";

                tableSucursales.AddCell(new PdfPCell(new Phrase(_strTitulo2, fontTitle1))
                {
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    VerticalAlignment = Element.ALIGN_TOP,
                    Border = PdfPCell.NO_BORDER,
                });

                string _strTitulo3 = "";

                tableSucursales.AddCell(new PdfPCell(new Phrase(_strTitulo3, fontTitle1))
                {
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    VerticalAlignment = Element.ALIGN_TOP,
                    Border = PdfPCell.NO_BORDER,
                });

                string _strTitulo4 = "";

                tableSucursales.AddCell(new PdfPCell(new Phrase(_strTitulo4, fontTitle1))
                {
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    VerticalAlignment = Element.ALIGN_TOP,
                    Border = PdfPCell.NO_BORDER,
                });

                string _strTitulo5 = "";

                tableSucursales.AddCell(new PdfPCell(new Phrase(_strTitulo5, fontTitle1))
                {
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    VerticalAlignment = Element.ALIGN_TOP,
                    Border = PdfPCell.NO_BORDER,
                });

                string _strTitulo6 = "";

                tableSucursales.AddCell(new PdfPCell(new Phrase(_strTitulo6, fontTitle1))
                {
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    VerticalAlignment = Element.ALIGN_TOP,
                    Border = PdfPCell.NO_BORDER,
                });

                string _strTitulo7 = "";

                tableSucursales.AddCell(new PdfPCell(new Phrase(_strTitulo7, fontTitle1))
                {
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    VerticalAlignment = Element.ALIGN_TOP,
                    Border = PdfPCell.NO_BORDER,
                });

                string _strTitulo8 = "";

                tableSucursales.AddCell(new PdfPCell(new Phrase(_strTitulo8, fontTitle1))
                {
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    VerticalAlignment = Element.ALIGN_TOP,
                    Border = PdfPCell.NO_BORDER,
                });

                string _strTitulo9 = "";

                tableSucursales.AddCell(new PdfPCell(new Phrase(_strTitulo9, fontTitle1))
                {
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    VerticalAlignment = Element.ALIGN_TOP,
                    Border = PdfPCell.NO_BORDER,
                });

                //PdfPTable tableSucursales2 = new PdfPTable(8) { WidthPercentage = 100, };
                #endregion

                #region Exit

                document.Add(tableEncabezado);
                document.Add(tableContInfoCliente_Factura);
                document.Add(tableUnidades);
                document.Add(tablePiePag);
                document.Add(tableSucursales);

                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();
                RutaPdf = NomArchivo;
                return true;
                #endregion
            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.Message;
                return false;
            }
        }

        private bool AddColumnsInvoiceLineAsocaba(ref iTextSharp.text.pdf.PdfPTable table)
        {
            try
            {
                iTextSharp.text.Font fuenteEncabezado = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);

                iTextSharp.text.pdf.PdfPCell celda1 = new iTextSharp.text.pdf.PdfPCell(new Phrase("CÓDIGO", fuenteEncabezado));
                celda1.Padding = 3;
                celda1.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celda1.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                table.AddCell(celda1);

                iTextSharp.text.pdf.PdfPCell celda2 = new iTextSharp.text.pdf.PdfPCell(new Phrase("DESCRIPCIÓN", fuenteEncabezado));
                celda2.Padding = 3;
                celda2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celda2.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                table.AddCell(celda2);

                iTextSharp.text.pdf.PdfPCell celda3 = new iTextSharp.text.pdf.PdfPCell(new Phrase("CANTIDAD", fuenteEncabezado));
                celda3.Padding = 3;
                celda3.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celda3.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                table.AddCell(celda3);

                iTextSharp.text.pdf.PdfPCell celda5 = new iTextSharp.text.pdf.PdfPCell(new Phrase("VR. UNITARIO", fuenteEncabezado));
                celda5.Padding = 3;
                celda5.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celda5.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                table.AddCell(celda5);

                iTextSharp.text.pdf.PdfPCell celda6 = new iTextSharp.text.pdf.PdfPCell(new Phrase("VALOR TOTAL", fuenteEncabezado));
                celda6.Padding = 3;
                celda6.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celda6.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                table.AddCell(celda6);

                return true;
            }
            catch (Exception ex)
            {
                strError = "Error addLineInvoice: " + ex.Message;
                return false;
            }
        }

        private bool AddRowInvoiceAsocaba(DataRow InvoiceLine, ref iTextSharp.text.pdf.PdfPTable table, iTextSharp.text.Font fuenteDatos, DataTable DtInvcTax)
        {
            try
            {
                //CODIGO
                iTextSharp.text.pdf.PdfPCell celCodigo = new iTextSharp.text.pdf.PdfPCell(new Phrase(InvoiceLine["PartNum"].ToString(), fuenteDatos));
                celCodigo.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celCodigo.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                table.AddCell(celCodigo);

                //DESCRIPCION
                iTextSharp.text.pdf.PdfPCell celDesc = new iTextSharp.text.pdf.PdfPCell(new Phrase(InvoiceLine["LineDesc"].ToString(), fuenteDatos));
                celDesc.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                celDesc.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                table.AddCell(celDesc);

                //CANTIDAD
                decimal Qty = Convert.ToDecimal(InvoiceLine["SellingShipQty"].ToString());
                iTextSharp.text.pdf.PdfPCell celCant = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:N0}", Qty), fuenteDatos));
                celCant.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celCant.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                table.AddCell(celCant);

                //PRECIO
                decimal UnitPrice = Convert.ToDecimal(InvoiceLine["DocUnitPrice"].ToString());
                iTextSharp.text.pdf.PdfPCell cellUnitPrice = new iTextSharp.text.pdf.PdfPCell(new Phrase("$" + string.Format("{0:N2}", UnitPrice), fuenteDatos));
                cellUnitPrice.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                cellUnitPrice.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                table.AddCell(cellUnitPrice);

                //EXT PRICE
                decimal PriceLine = Convert.ToDecimal(InvoiceLine["DocExtPrice"].ToString());
                iTextSharp.text.pdf.PdfPCell celExtPrise = new iTextSharp.text.pdf.PdfPCell(new Phrase("$" + string.Format("{0:N2}", PriceLine), fuenteDatos));
                celExtPrise.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                celExtPrise.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                table.AddCell(celExtPrise);

                return true;
            }
            catch (Exception ex)
            {
                strError = "Error addLineInvoice: " + ex.Message;
                return false;
            }
        }

        #endregion

    }
}
