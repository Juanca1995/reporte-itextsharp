﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//Referenciar y usar.
using System.Data;
using System.IO;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Windows.Forms;
using System.Web;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Net;
using System.Drawing;
using System.Globalization;
using static RulesServicesDIAN2.Adquiriente.pdfEstandarAR;

namespace RulesServicesDIAN2.Adquiriente
{
    public partial class pdfEstandarAR
    {
        #region Formatos Yobel

            public bool FacturaNacionalYobel(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
            {
                bool esLocal = Helpers.Compartido.EsLocal;
                string Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
                string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
                string RutaImg = $@"{AppDomain.CurrentDomain.BaseDirectory}\electro logo.png";
                NomArchivo = string.Empty;
                NomArchivo = NombreInvoice + ".pdf";

                //bool esLocal = Helpers.Compartido.EsLocal;
                //string  Ruta = HttpContext.Current.Server.MapPath("~/PDF/" + NIT + "/");
                //string  LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
                //string  RutaImg = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";
                //NomArchivo = string.Empty;
                //NomArchivo = NombreInvoice + ".pdf";
                try
                {
                    #region Head
                    //Validamos Existencia del Directorio.
                    if (!System.IO.Directory.Exists(Ruta))
                        System.IO.Directory.CreateDirectory(Ruta);

                    //Valido la existencia previa de este archivo.
                    if (System.IO.File.Exists(Ruta + NomArchivo))
                        System.IO.File.Delete(Ruta + NomArchivo);

                    //Dimenciones del documento.

                    Document document = new Document(iTextSharp.text.PageSize.LETTER, 20f, 10f, 30f, 30f);

                    PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                    Paragraph separator = new Paragraph("\n");
                    separator.Alignment = Element.ALIGN_CENTER;

                    // step 3: we open the document     
                    document.Open();

                    PdfDiv divEspacio = new PdfDiv();
                    divEspacio.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                    divEspacio.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                    //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                    divEspacio.Height = 10;
                    divEspacio.Width = 130;

                    PdfDiv divEspacio2 = new PdfDiv();
                    divEspacio2.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                    divEspacio2.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                    //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                    divEspacio2.Height = 2;
                    divEspacio2.Width = 130;

                    //FUENTES----------------------------------------------------------------------------------------------------------
                    iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                    iTextSharp.text.Font fontTitle2 = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                    iTextSharp.text.Font fontCustom = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);
                    iTextSharp.text.Font fontAcercade = FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL);
                    iTextSharp.text.Font fontTitleFactura = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                    //FUENTES---------------------------------------------------------------------------------------------------------

                    System.Drawing.Image logo = null;
                    var requestLogo = WebRequest.Create(RutaImg);

                    using (var responseLogo = requestLogo.GetResponse())
                    using (var streamLogo = responseLogo.GetResponseStream())
                    {
                        logo = Bitmap.FromStream(streamLogo);
                    }

                    iTextSharp.text.Image LogoPdf2 = iTextSharp.text.Image.GetInstance(logo, BaseColor.WHITE);
                    LogoPdf2.ScaleAbsolute(70.0f, 70.0f);
                    LogoPdf2.Border = 0;

                    #endregion

                    #region ENCABEZADO

                    PdfPTable Encabezado = new PdfPTable(4);
                    Encabezado.WidthPercentage = 100;

                    PdfPCell cell_logo = new PdfPCell() { Border = 0, };
                    cell_logo.AddElement(LogoPdf2);
                    Encabezado.AddCell(cell_logo);

                    PdfPCell cell_info = new PdfPCell(new Phrase("")) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, Colspan = 2, };

                    Paragraph prgInfoEmpresa2 = new Paragraph($"{(string)DsInvoiceAR.Tables["Company"].Rows[0]["Name"]}\n",
                    FontFactory.GetFont(FontFactory.HELVETICA, 5.79f, iTextSharp.text.Font.NORMAL));
                    prgInfoEmpresa2.Alignment = Element.ALIGN_CENTER;
                    Paragraph prgInfoEmpresa1 = new Paragraph($"Direccion: {(string)DsInvoiceAR.Tables["Company"].Rows[0]["Address1"]}\n" +
                        $"PBX: {(string)DsInvoiceAR.Tables["Company"].Rows[0]["PhoneNum"]} -  " +
                        $"FAX: {(string)DsInvoiceAR.Tables["Company"].Rows[0]["FaxNum"]}\n" +
                        $"Ciudad: {(string)DsInvoiceAR.Tables["Company"].Rows[0]["City"]} - " +
                        $"Departamento: {(string)DsInvoiceAR.Tables["Company"].Rows[0]["State"]}",
                    FontFactory.GetFont(FontFactory.HELVETICA, 5.79f, iTextSharp.text.Font.NORMAL));

                    prgInfoEmpresa2.Alignment = Element.ALIGN_CENTER;
                    prgInfoEmpresa1.Alignment = Element.ALIGN_CENTER;

                    cell_info.AddElement(prgInfoEmpresa2);
                    cell_info.AddElement(prgInfoEmpresa1);

                    Encabezado.AddCell(cell_info);

                    PdfPCell cell_factura = new PdfPCell(new Phrase("FACTURA DE VENTAS\n" +
                                                                    "Nro.  " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"]))
                    { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, };
                    Encabezado.AddCell(cell_factura);

                    #endregion

                    #region DATOS CUSTOMER Y FACTURA
                    //------------------------------------------------------------------------------------------------
                    float[] DimencionFacturar = new float[3];
                    DimencionFacturar[0] = 1.0F;//
                    DimencionFacturar[1] = 1.0F;//
                    DimencionFacturar[2] = 1.0F;//

                    iTextSharp.text.Font fontTitleFecha = FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL);
                    iTextSharp.text.Font fontDMA = FontFactory.GetFont(FontFactory.HELVETICA, 6, BaseColor.GRAY);
                    iTextSharp.text.Font fontDetalleFecha = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 8, iTextSharp.text.Font.NORMAL);

                    #endregion

                    #region Tabla Unidades
                    PdfPTable tableTituloUnidades = new PdfPTable(4);
                    //Dimenciones.
                    float[] DimencionUnidades = new float[4];
                    DimencionUnidades[0] = 0.5F;//Cantidad
                    DimencionUnidades[1] = 2.0F;//Descripcion
                    DimencionUnidades[2] = 0.585F;//Valor Unitario
                    DimencionUnidades[3] = 0.5F;//Valor Total

                    tableTituloUnidades.WidthPercentage = 100;
                    tableTituloUnidades.SetWidths(DimencionUnidades);

                    iTextSharp.text.pdf.PdfPCell celTextCantidad = new iTextSharp.text.pdf.PdfPCell(new Phrase("Cantidad", fontTitleFactura));
                    celTextCantidad.Colspan = 1;
                    celTextCantidad.Padding = 3;
                    celTextCantidad.Border = 0;
                    celTextCantidad.BorderWidthLeft = 1;
                    celTextCantidad.BackgroundColor = BaseColor.LIGHT_GRAY;
                    celTextCantidad.HorizontalAlignment = Element.ALIGN_CENTER;
                    celTextCantidad.VerticalAlignment = Element.ALIGN_TOP;

                    tableTituloUnidades.AddCell(celTextCantidad);

                    iTextSharp.text.pdf.PdfPCell celTextDesc = new iTextSharp.text.pdf.PdfPCell(new Phrase("Descripción", fontTitleFactura));
                    celTextDesc.Colspan = 1;
                    celTextDesc.Padding = 3;
                    celTextDesc.BackgroundColor = BaseColor.LIGHT_GRAY;
                    celTextDesc.Border = 0;
                    celTextDesc.BorderWidthLeft = 1;
                    celTextDesc.HorizontalAlignment = Element.ALIGN_CENTER;
                    celTextDesc.VerticalAlignment = Element.ALIGN_TOP;

                    tableTituloUnidades.AddCell(celTextDesc);

                    iTextSharp.text.pdf.PdfPCell celTextValorUnitario = new iTextSharp.text.pdf.PdfPCell(new Phrase("Valor Unitario", fontTitleFactura));
                    celTextValorUnitario.Colspan = 1;
                    celTextValorUnitario.Border = 0;
                    celTextValorUnitario.BorderWidthLeft = 1;
                    celTextValorUnitario.BackgroundColor = BaseColor.LIGHT_GRAY;
                    celTextValorUnitario.Padding = 3;
                    celTextValorUnitario.HorizontalAlignment = Element.ALIGN_CENTER;
                    celTextValorUnitario.VerticalAlignment = Element.ALIGN_TOP;

                    tableTituloUnidades.AddCell(celTextValorUnitario);

                    iTextSharp.text.pdf.PdfPCell celTextValorTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase("Valor Total", fontTitleFactura));
                    celTextValorTotal.Colspan = 1;
                    celTextValorTotal.Padding = 3;
                    celTextValorTotal.Border = 0;
                    celTextValorTotal.BorderWidthLeft = 1;
                    celTextValorTotal.BorderWidthRight = 1;
                    celTextValorTotal.BackgroundColor = BaseColor.LIGHT_GRAY;
                    celTextValorTotal.HorizontalAlignment = Element.ALIGN_CENTER;
                    celTextValorTotal.VerticalAlignment = Element.ALIGN_TOP;

                    tableTituloUnidades.AddCell(celTextValorTotal);

                    PdfPTable tableUnidades = new PdfPTable(4);
                    tableUnidades.WidthPercentage = 100;
                    tableUnidades.SetWidths(DimencionUnidades);
                    decimal totalDescuento = 0;
                    decimal decTotalUnd = 0;
                    foreach (DataRow InvoiceLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
                    {
                        if (!AddUnidadesYobel(InvoiceLine, ref tableUnidades, fontCustom, DsInvoiceAR, ref decTotalUnd))
                            return false;
                        if (Helpers.Compartido.VerificarExistenciaColumnas(DsInvoiceAR, "InvcDtl", "DspDocDiscount"))
                            totalDescuento += decimal.Parse((string)InvoiceLine["DspDocDiscount"]);
                    }

                    PdfPTable tableLineaFinal = new PdfPTable(4);
                    tableLineaFinal.WidthPercentage = 100;
                    tableLineaFinal.SetWidths(DimencionUnidades);
                    iTextSharp.text.pdf.PdfPCell LineaFinal = new iTextSharp.text.pdf.PdfPCell();
                    LineaFinal.Colspan = 4;
                    LineaFinal.Padding = 3;
                    LineaFinal.Border = 1;
                    LineaFinal.BorderColorBottom = BaseColor.WHITE;
                    LineaFinal.HorizontalAlignment = Element.ALIGN_CENTER;
                    LineaFinal.VerticalAlignment = Element.ALIGN_TOP;
                    tableLineaFinal.AddCell(LineaFinal);
                    #endregion

                    #region OBSERVACIONES Y TOTALES
                    PdfPTable tableObsTotales = new PdfPTable(3);
                    tableObsTotales.WidthPercentage = 100;
                    float[] dimTableObsTotales = new float[3];
                    dimTableObsTotales[0] = 2.0f;
                    dimTableObsTotales[1] = 0.8f;
                    dimTableObsTotales[2] = 0.85f;
                    tableObsTotales.SetWidths(dimTableObsTotales);
                    //---------------------------------------------------------------------
                    PdfPTable tableObsercaciones = new PdfPTable(1);
                    PdfPTable tableTotalLinea = new PdfPTable(2);
                    tableTotalLinea.SetWidths(new float[] { 0.5f, 1f });

                    PdfPCell celTextTotalLinea = new PdfPCell(new Phrase("TOTAL UNIDADES", fontTitleFactura));
                    celTextTotalLinea.Border = 0;
                    celTextTotalLinea.Colspan = 1;

                    PdfPCell celTextDato = new PdfPCell(new Phrase(decTotalUnd.ToString("N2"), fontTitleFactura));
                    celTextDato.Border = 0;

                    tableTotalLinea.AddCell(celTextTotalLinea);
                    tableTotalLinea.AddCell(celTextDato);

                    PdfPCell celTextObservaciones = new PdfPCell(new Phrase("OBSERVACIONES\n\n\n", fontTitleFactura));
                    celTextObservaciones.Border = 0;

                    tableObsercaciones.AddCell(tableTotalLinea);
                    tableObsercaciones.AddCell(celTextObservaciones);

                    string Observaciones = string.Empty;
                    if (Helpers.Compartido.VerificarExistenciaColumnas(DsInvoiceAR, "InvcHead", "InvoiceComment", 0))
                        Observaciones = DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceComment"].ToString();

                    PdfPCell celTextPedirCita = new PdfPCell(new Phrase(Observaciones, fontTitle2));
                    celTextPedirCita.Border = 0;
                    tableObsercaciones.AddCell(celTextPedirCita);

                    PdfPCell celObservaciones = new PdfPCell(tableObsercaciones);
                    tableObsTotales.AddCell(celObservaciones);
                    //----------------------------------------------------------------------
                    PdfPCell celFimarSello = new PdfPCell(new Phrase("\n\n\n\n_______________________________\nFirma y Sello", fontTitle2));
                    celFimarSello.HorizontalAlignment = Element.ALIGN_CENTER;
                    celFimarSello.VerticalAlignment = Element.ALIGN_BOTTOM;
                    tableObsTotales.AddCell(celFimarSello);
                    //----------------------------------------------------------------------
                    PdfPTable tableTotales = new PdfPTable(2);
                    float[] dimTableTotales = new float[2];
                    dimTableTotales[0] = 1.9f;
                    dimTableTotales[1] = 0.95f;
                    tableTotales.SetWidths(dimTableTotales);

                    PdfPCell celTextTotalBruto = new PdfPCell(new Phrase("Total Bruto / Importe Neto  " , fontTitle2));
                    celTextTotalBruto.Border = 0;
                    tableTotales.AddCell(celTextTotalBruto);
                    //PdfPCell celValTotalBruto = new PdfPCell(new Phrase(
                    //    decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubTotal"]).ToString("N2"), fontTitleFactura));
                    PdfPCell celValTotalBruto = new PdfPCell(new Phrase("$ "+decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number02"]).ToString("N0"), fontTitleFactura));
                    celValTotalBruto.Border = 0;
                    celValTotalBruto.HorizontalAlignment = Element.ALIGN_RIGHT;
                    tableTotales.AddCell(celValTotalBruto);

                    PdfPCell celTotalTextDescuento = new PdfPCell(new Phrase("Descuento", fontTitle2));
                    celTotalTextDescuento.Border = 0;
                    tableTotales.AddCell(celTotalTextDescuento);
                    PdfPCell celTotalValDescuento = new PdfPCell(new Phrase(totalDescuento.ToString("N0"), fontTitleFactura));
                    celTotalValDescuento.Border = 0;
                    celTotalValDescuento.HorizontalAlignment = Element.ALIGN_RIGHT;
                    tableTotales.AddCell(celTotalValDescuento);

                    PdfPCell celTextTotalValorFletes = new PdfPCell(new Phrase("Valor Fletes / Valor Iva ", fontTitle2));
                    celTextTotalValorFletes.Border = 0;
                    tableTotales.AddCell(celTextTotalValorFletes);
                    PdfPCell celValTotalValorFletes = new PdfPCell(new Phrase("$ " + decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number03"]).ToString("N0"), fontTitleFactura));
                    celValTotalValorFletes.Border = 0;
                    celValTotalValorFletes.HorizontalAlignment = Element.ALIGN_RIGHT;
                    tableTotales.AddCell(celValTotalValorFletes);

                    PdfPCell celTextSubTotal = new PdfPCell(new Phrase("Subtotal / Rete Fuente", fontTitle2));
                    celTextSubTotal.Border = 0;
                    tableTotales.AddCell(celTextSubTotal);
                    PdfPCell celValSubTotal = new PdfPCell(new Phrase(
                        decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubTotal"]).ToString("N0"), fontTitleFactura));
                    celValSubTotal.Border = 0;
                    celValSubTotal.HorizontalAlignment = Element.ALIGN_RIGHT;
                    tableTotales.AddCell(celValSubTotal);

                    PdfPCell celTextValorIva = new PdfPCell(new Phrase("Valor IVA / Rete Iva", fontTitle2));
                    celTextValorIva.Border = 0;
                    tableTotales.AddCell(celTextValorIva);
                    var Iva = GetValImpuestoByID("01", DsInvoiceAR);
                    PdfPCell celValValorIva = new PdfPCell(new Phrase(Iva.ToString("N0"), fontTitleFactura));
                    celValValorIva.Border = 0;
                    celValValorIva.HorizontalAlignment = Element.ALIGN_RIGHT;
                    tableTotales.AddCell(celValValorIva);

                    PdfPCell celTextReteIva = new PdfPCell(new Phrase("", fontTitle2));
                    celTextReteIva.Border = 0;
                    tableTotales.AddCell(celTextReteIva);
                    PdfPCell celValReteIva = new PdfPCell(new Phrase("", fontTitleFactura));
                    celValReteIva.Border = 0;
                    celValReteIva.HorizontalAlignment = Element.ALIGN_RIGHT;
                    tableTotales.AddCell(celValReteIva);

                    PdfPCell celTotales = new PdfPCell(tableTotales);
                    celTotales.Padding = 2;
                    tableObsTotales.AddCell(celTotales);

                    PdfPCell celTotalLetras = new PdfPCell(new Phrase(
                        $"Son: **{Nroenletras((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"])}**",
                        fontTitle2));
                    celTotalLetras.Colspan = 2;
                    tableObsTotales.AddCell(celTotalLetras);

                    PdfPTable tableTotalFactura = new PdfPTable(2);
                    tableTotalFactura.SetWidths(dimTableTotales);

                    PdfPCell celTextTotal = new PdfPCell(new Phrase("Total Factura", fontTitleFactura));
                    celTextTotal.BackgroundColor = BaseColor.LIGHT_GRAY;
                    celTextTotal.Border = 0;
                    tableTotalFactura.AddCell(celTextTotal);

                    PdfPCell celValTotal = new PdfPCell(new Phrase(
                        decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"]).ToString("N0"),
                        fontTitleFactura));
                    celValTotal.HorizontalAlignment = Element.ALIGN_RIGHT;
                    celValTotal.BackgroundColor = BaseColor.LIGHT_GRAY;
                    celValTotal.Border = 0;
                    tableTotalFactura.AddCell(celValTotal);

                    PdfPCell celTotal = new PdfPCell(tableTotalFactura);
                    tableObsTotales.AddCell(celTotal);
                    //---------------------------------------------------------------------------------------------

                    PdfPTable tableResolucion = new PdfPTable(1);
                    tableResolucion.WidthPercentage = 100;

                    PdfPCell celResolucion = new PdfPCell(new Phrase(DsInvoiceAR.Tables["InvcHead"].Rows[0]["Character02"].ToString(),
                        fontTitle2));
                    celResolucion.HorizontalAlignment = Element.ALIGN_CENTER;
                    celResolucion.Border = 0;
                    tableResolucion.AddCell(celResolucion);
                    //------------------------------------------------------------------------------------------------------------
                    PdfDiv divTextCopia = new PdfDiv();
                    divTextCopia.BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.DOTTED;
                    divTextCopia.Width = 582;

                    PdfPTable fin = new PdfPTable(new float[] { 0.34f, 0.33f, 0.03f, 0.3f });
                    fin.WidthPercentage = 100;

                    PdfPCell cell_sello = new PdfPCell(new Phrase("ACEPTO:\n\n\n\n" +
                                                                "_______________________________________________\n" +
                                                                "c.c. sello", fontTitleFactura))
                    { HorizontalAlignment = Element.ALIGN_LEFT };
                    fin.AddCell(cell_sello);

                    PdfPCell cell_leyenda = new PdfPCell(new Phrase("La presente factura de ventas se asimila para todos sus efectos\n" +
                                                                    "legales a una letra de cambio según articulo 774 del C.de C.y\n" +
                                                                    "causará un interes por mora mesual a la tasa máxiam legal\n" +
                                                                    "autorizada.", fontCustom))
                    { HorizontalAlignment = Element.ALIGN_LEFT };
                    fin.AddCell(cell_leyenda);

                    PdfPCell cell_line = new PdfPCell() { Border = 0, };
                    fin.AddCell(cell_line);

                    PdfPCell cell_totales = new PdfPCell();

                    PdfPTable totalvs = new PdfPTable(2);
                    totalvs.WidthPercentage = 100;

                    PdfPCell vst = new PdfPCell(new Phrase("Total ventas excluida", fontTitleFactura)) { HorizontalAlignment = Element.ALIGN_LEFT, };
                    totalvs.AddCell(vst);
                    PdfPCell vsv = new PdfPCell(new Phrase("$ " + decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number01"]).ToString("N2"), fontTitleFactura)) { HorizontalAlignment = Element.ALIGN_RIGHT };
                    totalvs.AddCell(vsv);
                    cell_totales.AddElement(totalvs);

                    PdfPTable tvgt = new PdfPTable(2);
                    tvgt.WidthPercentage = 100;
                    PdfPCell vgt = new PdfPCell(new Phrase("Total ventas gravada", fontTitleFactura)) { HorizontalAlignment = Element.ALIGN_LEFT, };
                    tvgt.AddCell(vgt);
                    PdfPCell vgv = new PdfPCell(new Phrase("$ " + decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number02"]).ToString("N2"), fontTitleFactura)) { HorizontalAlignment = Element.ALIGN_RIGHT };
                    tvgt.AddCell(vgv);
                    cell_totales.AddElement(tvgt);

                    PdfPTable tiva = new PdfPTable(2);
                    tiva.WidthPercentage = 100;
                    PdfPCell iva = new PdfPCell(new Phrase("Iva", fontTitleFactura)) { HorizontalAlignment = Element.ALIGN_LEFT, };
                    tiva.AddCell(iva);
                    PdfPCell cell_iva = new PdfPCell(new Phrase("$ " + decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DocTaxAmt"]).ToString("N2"), fontTitleFactura)) { HorizontalAlignment = Element.ALIGN_RIGHT };
                    tiva.AddCell(cell_iva);
                    cell_totales.AddElement(tiva);

                    PdfPTable ttotal = new PdfPTable(2);
                    ttotal.WidthPercentage = 100;
                    PdfPCell total = new PdfPCell(new Phrase("Total", fontTitleFactura)) { HorizontalAlignment = Element.ALIGN_LEFT, };
                    ttotal.AddCell(total);
                    PdfPCell cell_total = new PdfPCell(new Phrase("$ " + decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"]).ToString("N2"), fontTitleFactura)) { HorizontalAlignment = Element.ALIGN_RIGHT };
                    ttotal.AddCell(cell_total);
                    cell_totales.AddElement(ttotal);

                    fin.AddCell(cell_totales);


                    #endregion

                    #region Exit
                    document.Add(Encabezado);
                    document.Add(divEspacio);
                    document.Add(Yobel.FacturaNacional.DatosCliente(DsInvoiceAR, CUFE, QRInvoice));
                    document.Add(divEspacio2);
                    document.Add(divEspacio2);
                    document.Add(tableTituloUnidades);
                    document.Add(tableUnidades);
                    //document.Add(tableEspacioUnidades);
                    //document.Add(tableLineaFinal);
                    document.Add(divEspacio2);
                    document.Add(tableObsTotales);
                    document.Add(divEspacio2);
                    document.Add(tableResolucion);
                    //document.Add(divEspacio2);
                    document.Add(divTextCopia);
                    document.Add(divEspacio2);
                    document.Add(divEspacio2);
                    //document.Add(fin);
                    PdfContentByte pCb = writer.DirectContent;
                    PieDePagina(ref pCb);

                    writer.Flush();
                    document.Close();
                    Helpers.Simex.AddPageNumberPagToSimexExp($"{Ruta}{NomArchivo}", dimxSimex, dimYSimex + 10, Helpers.Fuentes.SimexLeyenda);
                    RutaPdf = NomArchivo;
                    return true;
                    #endregion
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                    return false;
                }
            }

            public bool NotaCreditoYobel(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
            {
            NomArchivo = string.Empty;
            string Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            string RutaImg = $@"{AppDomain.CurrentDomain.BaseDirectory}\logocoode.png";
            NomArchivo = NombreInvoice + ".pdf";
            try
            {
                #region Head
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.
                Document document = new Document(iTextSharp.text.PageSize.LETTER);
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;

                // step 3: we open the document     
                document.Open();

                PdfDiv divEspacio = new PdfDiv();
                divEspacio.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio.Height = 10;
                divEspacio.Width = 130;

                PdfDiv divEspacio2 = new PdfDiv();
                divEspacio2.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio2.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio2.Height = 2;
                divEspacio2.Width = 130;

                //FUENTES----------------------------------------------------------------------------------------------------------
                iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle3 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 9, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle2 = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustom = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitleFactura = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                //FUENTES---------------------------------------------------------------------------------------------------------

                System.Drawing.Image logo = null;
                var requestLogo = WebRequest.Create(RutaImg);

                using (var responseLogo = requestLogo.GetResponse())
                using (var streamLogo = responseLogo.GetResponseStream())
                {
                    logo = Bitmap.FromStream(streamLogo);
                }

                System.Drawing.Image logo2 = null;
                var requestLogo2 = WebRequest.Create(RutaImg);

                using (var responseLogo2 = requestLogo2.GetResponse())
                using (var streamLogo2 = responseLogo2.GetResponseStream())
                {
                    logo2 = Bitmap.FromStream(streamLogo2);
                }

                iTextSharp.text.Image ImgLogo2 = iTextSharp.text.Image.GetInstance(logo2, BaseColor.WHITE);
                ImgLogo2.ScaleAbsolute(130, 130);

                #endregion

                #region Header

                PdfPTable header = new PdfPTable(1);
                header.WidthPercentage = 100;
                PdfPCell encabezado = new PdfPCell() { Border = 0, };

                //agregamos todos los datos del encabezado en en la celda encabezado
                PdfPTable info1 = new PdfPTable(new float[] { 0.25f, 0.75f });
                info1.WidthPercentage = 100;

                //agregamos logo
                PdfPCell cell_logo = new PdfPCell() { Border = 0, };
                cell_logo.AddElement(ImgLogo2);
                info1.AddCell(cell_logo);

                PdfPCell info2 = new PdfPCell() { Border = 0, };

                PdfPTable info = new PdfPTable(new float[] { 0.35f, 0.30f, 0.35f, });
                info.WidthPercentage = 100;

                //egregamos la informaion tributaria
                PdfPCell cell_info = new PdfPCell() { Border = 0, };
                Paragraph prgTitle = new Paragraph(string.Format("{0}\nNIT.{1}", DsInvoiceAR.Tables["Customer"].Rows[0]["CustID"], DsInvoiceAR.Tables["InvcHead"].Rows[0]["Company"]), fontTitle);
                Paragraph prgTitle2 = new Paragraph(string.Format("{0}\nPBX:{1}\nA.A. 35155\n{2} {3}\nwww.aldoronline.com",
                                                                    DsInvoiceAR.Tables["Customer"].Rows[0]["Address1"].ToString(),
                                                                    DsInvoiceAR.Tables["Customer"].Rows[0]["PhoneNum"].ToString(),
                                                                    DsInvoiceAR.Tables["Customer"].Rows[0]["City"].ToString(),
                                                                    DsInvoiceAR.Tables["Customer"].Rows[0]["State"].ToString()), fontTitle2);
                prgTitle.Alignment = Element.ALIGN_LEFT;
                prgTitle2.Alignment = Element.ALIGN_LEFT;
                cell_info.AddElement(prgTitle);
                cell_info.AddElement(prgTitle2);
                info.AddCell(cell_info);

                //agregamos el codigo Qr
                PdfPCell cell_Qr = new PdfPCell() { Border = 0, };
                iTextSharp.text.Image ImgQR = iTextSharp.text.Image.GetInstance(QRInvoice, BaseColor.WHITE);
                ImgQR.ScaleAbsolute(70, 70);
                cell_Qr.AddElement(ImgQR);
                info.AddCell(cell_Qr);

                //agregamos numeode factura 
                PdfPCell cell_factura = new PdfPCell() { Border = 0, };
                Paragraph prgTitle3 = new Paragraph(string.Format("NOTA CREDITO", fontTitle2));
                Paragraph prgTitle4 = new Paragraph(string.Format("" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"].ToString(), fontTitle2));
                prgTitle3.Alignment = Element.ALIGN_RIGHT;
                prgTitle4.Alignment = Element.ALIGN_RIGHT;
                cell_factura.AddElement(prgTitle3);
                cell_factura.AddElement(prgTitle4);
                info.AddCell(cell_factura);

                info2.AddElement(info);


                PdfPTable info3 = new PdfPTable(new float[] { 0.35f, 0.30f, 0.35f, });
                info3.WidthPercentage = 100;


                //egregamos la informaion tributaria
                PdfPCell cell_info3 = new PdfPCell() { Border = 0, };
                Paragraph prgTitle33 = new Paragraph(string.Format("", fontTitle2));
                Paragraph prgTitle23 = new Paragraph(string.Format("", fontTitle2));
                prgTitle33.Alignment = Element.ALIGN_LEFT;
                prgTitle23.Alignment = Element.ALIGN_LEFT;
                cell_info3.AddElement(prgTitle33);
                cell_info3.AddElement(prgTitle23);
                info3.AddCell(cell_info3);

                //agregamos el codigo Qr
                PdfPCell cell_factura5 = new PdfPCell() { Border = 0, };
                Paragraph prgTitle5 = new Paragraph(string.Format("{0}\nNIT.{1}", DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"], DsInvoiceAR.Tables["Company"].Rows[0]["Name"]), fontTitle2);
                Paragraph prgTitle51 = new Paragraph(string.Format("{0}\nPBX:{1}\nA.A.",
                                                                    DsInvoiceAR.Tables["Company"].Rows[0]["Address1"].ToString(),
                                                                    DsInvoiceAR.Tables["Company"].Rows[0]["State"].ToString()), fontTitle2);
                prgTitle5.Alignment = Element.ALIGN_CENTER;
                prgTitle51.Alignment = Element.ALIGN_CENTER;
                cell_factura5.AddElement(prgTitle5);
                cell_factura5.AddElement(prgTitle51);
                info3.AddCell(cell_factura5);

                //agregamos numeode factura 
                PdfPCell cell_factura4 = new PdfPCell() { Border = 0, };
                Paragraph prgTitle34 = new Paragraph(string.Format("", fontTitle2));
                Paragraph prgTitle43 = new Paragraph(string.Format("", fontTitle2));
                prgTitle34.Alignment = Element.ALIGN_RIGHT;
                prgTitle43.Alignment = Element.ALIGN_RIGHT;
                cell_factura4.AddElement(prgTitle34);
                cell_factura4.AddElement(prgTitle43);
                info3.AddCell(cell_factura4);


                info2.AddElement(info3);

                info1.AddCell(info2);
                encabezado.AddElement(info1);
                header.AddCell(encabezado);

                #endregion

                #region Body

                PdfPTable Body = new PdfPTable(5);
                Body.WidthPercentage = 100;

                PdfPCell fecha_factura = new PdfPCell();
                //agregamos la fechas de la facrura
                PdfPTable titulo_fecha = new PdfPTable(1);
                titulo_fecha.WidthPercentage = 100;
                PdfPCell titulo_fecha1 = new PdfPCell(new Phrase("FECHA FACTURA", fontTitle)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, };
                titulo_fecha.AddCell(titulo_fecha1);
                fecha_factura.AddElement(titulo_fecha);

                //agregamos la fechas de la facrura
                PdfPTable valor_fecha = new PdfPTable(1);
                valor_fecha.WidthPercentage = 100;
                PdfPCell valor_fecha1 = new PdfPCell(new Phrase("" + DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).ToString("dd/mm/yyyy"), fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, };
                valor_fecha.AddCell(valor_fecha1);
                fecha_factura.AddElement(valor_fecha);

                Body.AddCell(fecha_factura);

                PdfPCell fecha_vencimiento = new PdfPCell();
                //agregamos la fechas de elvensimiento
                PdfPTable titulo_bencimiento = new PdfPTable(1);
                titulo_bencimiento.WidthPercentage = 100;
                PdfPCell titulo_bencimiento1 = new PdfPCell(new Phrase("FECHA VENCIMIENTO", fontTitle)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, };
                titulo_bencimiento.AddCell(titulo_bencimiento1);
                fecha_vencimiento.AddElement(titulo_bencimiento);

                //agregamos la fechas de elvensimiento
                PdfPTable valor_bencimiento = new PdfPTable(1);
                valor_bencimiento.WidthPercentage = 100;
                PdfPCell valor_bencimiento1 = new PdfPCell(new Phrase("" + DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"]).ToString("dd/mm/yyyy"), fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, };
                valor_bencimiento.AddCell(valor_bencimiento1);
                fecha_vencimiento.AddElement(valor_bencimiento);

                Body.AddCell(fecha_vencimiento);

                PdfPCell vendedor = new PdfPCell();
                //agregamos el vendedor
                PdfPTable titulo_vendedor = new PdfPTable(1);
                titulo_vendedor.WidthPercentage = 100;
                PdfPCell titulo_vendedor1 = new PdfPCell(new Phrase("NOMBRE C. COMPRADOR", fontTitle)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, };
                titulo_vendedor.AddCell(titulo_vendedor1);
                vendedor.AddElement(titulo_vendedor);

                //agregamos el vendedor
                PdfPTable valor_vendedor = new PdfPTable(1);
                valor_vendedor.WidthPercentage = 100;
                PdfPCell valor_vendedor1 = new PdfPCell(new Phrase("" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar03"].ToString(), fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, };
                valor_vendedor.AddCell(valor_vendedor1);
                vendedor.AddElement(valor_vendedor);

                Body.AddCell(vendedor);

                PdfPCell forma_pago = new PdfPCell();

                PdfPTable titulo_pago1 = new PdfPTable(1);
                titulo_pago1.WidthPercentage = 100;
                PdfPCell titulo_pogo = new PdfPCell(new Phrase("CORREO DEL CONTACTO", fontTitle)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, };
                titulo_pago1.AddCell(titulo_pogo);
                forma_pago.AddElement(titulo_pago1);

                //agregamos el vendedor
                PdfPTable valor_pago = new PdfPTable(1);
                valor_pago.WidthPercentage = 100;
                PdfPCell valor_pago1 = new PdfPCell(new Phrase("" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar04"].ToString(), fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, };
                valor_pago.AddCell(valor_pago1);
                forma_pago.AddElement(valor_pago);

                Body.AddCell(forma_pago);

                PdfPCell transportador = new PdfPCell();
                //agregamos el vendedor
                PdfPTable titulo_transportador = new PdfPTable(1);
                titulo_transportador.WidthPercentage = 100;
                PdfPCell titulo_transportador1 = new PdfPCell(new Phrase("Nro. DE FACTURA", fontTitle)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, };
                titulo_transportador.AddCell(titulo_transportador1);
                transportador.AddElement(titulo_transportador);

                PdfPTable valor_transportador = new PdfPTable(1);
                valor_transportador.WidthPercentage = 100;
                PdfPCell valor_transportador1 = new PdfPCell(new Phrase("" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar02"].ToString(), fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, };
                valor_transportador.AddCell(valor_transportador1);
                transportador.AddElement(valor_transportador);
                Body.AddCell(transportador);

                //agregamos una leyenda 
                PdfPTable leyenda = new PdfPTable(1);
                leyenda.WidthPercentage = 100;
                PdfPCell leyendas = new PdfPCell(new Phrase("Resolución DIAN No. 18762008997356 de 2018-07-04 Prefijo UN- Numeración 11664.0000 a la 20000.0000, vigencia 24 meses. Impresión por computador", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER };
                leyenda.AddCell(leyendas);



                //agregamos los titulos de los detalles
                PdfPTable detalles = new PdfPTable(new float[] { 0.40f, 0.14f, 0.14f, 0.14f, 0.14f, 0.14f, });
                detalles.WidthPercentage = 100;

                PdfPCell DESCRIPCIÓN = new PdfPCell(new Phrase("DESCRIPCIÓN", fontTitle)) { HorizontalAlignment = Element.ALIGN_LEFT, MinimumHeight = 15, VerticalAlignment = Element.ALIGN_BOTTOM };
                PdfPCell CANTIDAD = new PdfPCell(new Phrase("CANTIDAD", fontTitle)) { HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_BOTTOM };
                PdfPCell UND = new PdfPCell(new Phrase("UND", fontTitle)) { HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_BOTTOM };
                PdfPCell vUNITARIO = new PdfPCell(new Phrase("VR. UNITARIO", fontTitle)) { HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_BOTTOM };
                PdfPCell IVA = new PdfPCell(new Phrase("", fontTitle)) { HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_BOTTOM };
                PdfPCell TOTAL = new PdfPCell(new Phrase("TOTAL", fontTitle)) { HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_BOTTOM };

                detalles.AddCell(DESCRIPCIÓN);
                detalles.AddCell(CANTIDAD);
                detalles.AddCell(UND);
                detalles.AddCell(vUNITARIO);
                detalles.AddCell(IVA);
                detalles.AddCell(TOTAL);

                #endregion

                #region unidades

                PdfPTable tableTituloUnidades = new PdfPTable(6);
                //Dimenciones.
                float[] DimencionUnidades = new float[6];
                DimencionUnidades[0] = 0.40f;//
                DimencionUnidades[1] = 0.14f;//
                DimencionUnidades[2] = 0.14f;//
                DimencionUnidades[3] = 0.14f;//
                DimencionUnidades[4] = 0.14f;//
                DimencionUnidades[5] = 0.14f;//

                PdfPTable tableUnidades = new PdfPTable(6);
                tableUnidades.WidthPercentage = 100;
                tableUnidades.SetWidths(DimencionUnidades);

                foreach (DataRow InvoiceLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
                {
                    AddUnidadesnotacreditoCoodesival(InvoiceLine, ref tableUnidades, fontCustom);
                }

                iTextSharp.text.pdf.PdfPCell LineaFinal = new iTextSharp.text.pdf.PdfPCell();
                LineaFinal.Colspan = 6;
                LineaFinal.Padding = 3;
                LineaFinal.Border = 1;
                LineaFinal.BorderColorBottom = BaseColor.WHITE;
                LineaFinal.HorizontalAlignment = Element.ALIGN_CENTER;
                LineaFinal.VerticalAlignment = Element.ALIGN_TOP;
                tableUnidades.AddCell(LineaFinal);


                #endregion

                #region Footer

                PdfPTable pie = new PdfPTable(new float[] { 0.7f, 0.3f, });
                pie.WidthPercentage = 100;
                //agregamos  una tabla con las notas 
                PdfPCell nota = new PdfPCell() { Border = 0, };

                PdfPTable nota_tabla = new PdfPTable(1);
                nota_tabla.WidthPercentage = 100;
                PdfPCell nota_tabla1 = new PdfPCell(new Phrase("Nota:", fontTitle)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP, MinimumHeight = 20, };
                nota_tabla.AddCell(nota_tabla1);
                nota.AddElement(nota_tabla);

                //agregamos el valor en letras 
                PdfPTable valoe_l = new PdfPTable(1);
                valoe_l.WidthPercentage = 100;
                PdfPCell valoe_l1 = new PdfPCell(new Phrase("VALOR EN LETRA:", fontTitle)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP, MinimumHeight = 20, };
                valoe_l.AddCell(valoe_l1);
                nota.AddElement(valoe_l);

                //agregamos  una tabla con la leyenda 
                PdfPTable leyenda_pie = new PdfPTable(1);
                leyenda_pie.WidthPercentage = 100;
                PdfPCell leyenda_pie1 = new PdfPCell(new Phrase("", fontTitle))
                { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP, MinimumHeight = 20, };
                leyenda_pie.AddCell(leyenda_pie1);
                nota.AddElement(leyenda_pie);


                pie.AddCell(nota);


                PdfPCell totales = new PdfPCell() { Border = 0, };

                PdfPTable valores_total = new PdfPTable(2);
                valores_total.WidthPercentage = 100;

                PdfPCell titulo_totales = new PdfPCell() { Border = 0, };

                //agregamos el valor subtotal
                PdfPTable subtotal = new PdfPTable(1);
                subtotal.WidthPercentage = 100;
                PdfPCell subtotal1 = new PdfPCell(new Phrase("SUBTOTAL", fontTitle)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_BOTTOM, MinimumHeight = 30, };
                subtotal.AddCell(subtotal1);
                titulo_totales.AddElement(subtotal);

                //agregamos el valor DESCUENTO
                PdfPTable DESCUENTO = new PdfPTable(1);
                DESCUENTO.WidthPercentage = 100;
                PdfPCell DESCUENTO1 = new PdfPCell(new Phrase("IVA", fontTitle)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_BOTTOM, };
                DESCUENTO.AddCell(DESCUENTO1);
                titulo_totales.AddElement(DESCUENTO);

                //agregamos el valor IVA
                PdfPTable IVA_v = new PdfPTable(1);
                IVA_v.WidthPercentage = 100;
                PdfPCell IVA1 = new PdfPCell(new Phrase("RETE IVA", fontTitle)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_BOTTOM, };
                IVA_v.AddCell(IVA1);
                titulo_totales.AddElement(IVA_v);

                //agregamos el valor RETEFUENTE
                PdfPTable RETEFUENTE = new PdfPTable(1);
                RETEFUENTE.WidthPercentage = 100;
                PdfPCell RETEFUENTE1 = new PdfPCell(new Phrase("RETEFUENTE", fontTitle)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_BOTTOM, };
                RETEFUENTE.AddCell(RETEFUENTE1);
                titulo_totales.AddElement(RETEFUENTE);

                //agregamos el valor FLETE
                PdfPTable FLETE = new PdfPTable(1);
                FLETE.WidthPercentage = 100;
                PdfPCell FLETE1 = new PdfPCell(new Phrase("TOTAL", fontTitle)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_BOTTOM, };
                FLETE.AddCell(FLETE1);
                titulo_totales.AddElement(FLETE);


                PdfPCell valores_totales = new PdfPCell() { Border = 0, };

                //agregamos el valor subtotal
                PdfPTable subtotalF = new PdfPTable(1);
                subtotalF.WidthPercentage = 100;
                PdfPCell subtotalF1 = new PdfPCell(new Phrase(
                    decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubTotal"]).ToString("N0"), fontTitle))
                { Border = 0, HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_BOTTOM, MinimumHeight = 30, };
                subtotalF.AddCell(subtotalF1);
                valores_totales.AddElement(subtotalF);

                //agregamos el valor DESCUENTO
                PdfPTable DESCUENTOF = new PdfPTable(1);
                DESCUENTOF.WidthPercentage = 100;
                PdfPCell DESCUENTOF1 = new PdfPCell(new Phrase(
                    decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DocWHTaxAmt"]).ToString("N0"), fontTitle))
                { Border = 0, HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_BOTTOM, };
                DESCUENTOF.AddCell(DESCUENTOF1);
                valores_totales.AddElement(DESCUENTOF);

                //agregamos el valor IVA
                PdfPTable IVA_f = new PdfPTable(1);
                IVA_f.WidthPercentage = 100;
                PdfPCell IVA_f1 = new PdfPCell(new Phrase(string.Format("{0}",
                    GetValImpuestoByID("0B", DsInvoiceAR).ToString("N0")), fontTitle))
                { Border = 0, HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_BOTTOM, };
                IVA_f.AddCell(IVA_f1);
                valores_totales.AddElement(IVA_f);

                //agregamos el valor RETEFUENTE
                PdfPTable RETEFUENTEf = new PdfPTable(1);
                RETEFUENTEf.WidthPercentage = 100;
                PdfPCell RETEFUENTEf1 = new PdfPCell(new Phrase(string.Format("{0}", GetValImpuestoByID("0A", DsInvoiceAR).ToString("N0")), fontTitle)) { Border = 0, HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_BOTTOM, };
                RETEFUENTEf.AddCell(RETEFUENTEf1);
                valores_totales.AddElement(RETEFUENTEf);

                //agregamos el valor FLETE
                PdfPTable FLETEf = new PdfPTable(1);
                FLETEf.WidthPercentage = 100;
                PdfPCell FLETEf1 = new PdfPCell(new Phrase(
                    decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"]).ToString("N0"), fontTitle))
                { Border = 0, HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_BOTTOM, };
                FLETEf.AddCell(FLETEf1);
                valores_totales.AddElement(FLETEf);


                valores_total.AddCell(titulo_totales);
                valores_total.AddCell(valores_totales);

                totales.AddElement(valores_total);
                pie.AddCell(totales);

                PdfPTable tabla_cufe = new PdfPTable(1);
                tabla_cufe.WidthPercentage = 100;
                PdfPCell cufe = new PdfPCell(new Phrase("CUFE:" + CUFE, fontTitle)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_BOTTOM };
                tabla_cufe.AddCell(cufe);

                #endregion

                #region Exit

                document.Add(header);
                document.Add(Body);
                document.Add(leyenda);
                document.Add(detalles);
                document.Add(tableTituloUnidades);
                document.Add(tableUnidades);
                document.Add(pie);
                document.Add(tabla_cufe);

                //document.Add(divTextBancoCheque);
                /*PIE DE PAGINA*/
                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();
                RutaPdf = NomArchivo;
                return true;
                #endregion
            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.Message;
                return false;
            }

        }

            public bool NotaDebitoYobel(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {
            NomArchivo = string.Empty;
            string Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            string RutaImg = $@"{AppDomain.CurrentDomain.BaseDirectory}\logocoode.png";
            NomArchivo = NombreInvoice + ".pdf";
            try
            {
                #region Head
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.
                Document document = new Document(iTextSharp.text.PageSize.LETTER);
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;

                // step 3: we open the document     
                document.Open();

                PdfDiv divEspacio = new PdfDiv();
                divEspacio.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio.Height = 10;
                divEspacio.Width = 130;

                PdfDiv divEspacio2 = new PdfDiv();
                divEspacio2.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio2.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio2.Height = 2;
                divEspacio2.Width = 130;

                //FUENTES----------------------------------------------------------------------------------------------------------
                iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle3 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 9, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle2 = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustom = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitleFactura = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                //FUENTES---------------------------------------------------------------------------------------------------------

                System.Drawing.Image logo = null;
                var requestLogo = WebRequest.Create(RutaImg);

                using (var responseLogo = requestLogo.GetResponse())
                using (var streamLogo = responseLogo.GetResponseStream())
                {
                    logo = Bitmap.FromStream(streamLogo);
                }

                System.Drawing.Image logo2 = null;
                var requestLogo2 = WebRequest.Create(RutaImg);

                using (var responseLogo2 = requestLogo2.GetResponse())
                using (var streamLogo2 = responseLogo2.GetResponseStream())
                {
                    logo2 = Bitmap.FromStream(streamLogo2);
                }

                iTextSharp.text.Image ImgLogo2 = iTextSharp.text.Image.GetInstance(logo2, BaseColor.WHITE);
                ImgLogo2.ScaleAbsolute(130, 130);

                #endregion

                #region Header

                PdfPTable header = new PdfPTable(1);
                header.WidthPercentage = 100;
                PdfPCell encabezado = new PdfPCell() { Border = 0, };

                //agregamos todos los datos del encabezado en en la celda encabezado
                PdfPTable info1 = new PdfPTable(new float[] { 0.25f, 0.75f });
                info1.WidthPercentage = 100;

                //agregamos logo
                PdfPCell cell_logo = new PdfPCell() { Border = 0, };
                cell_logo.AddElement(ImgLogo2);
                info1.AddCell(cell_logo);

                PdfPCell info2 = new PdfPCell() { Border = 0, };

                PdfPTable info = new PdfPTable(new float[] { 0.35f, 0.30f, 0.35f, });
                info.WidthPercentage = 100;

                //egregamos la informaion tributaria
                PdfPCell cell_info = new PdfPCell() { Border = 0, };
                Paragraph prgTitle = new Paragraph(string.Format("{0}\nNIT.{1}", DsInvoiceAR.Tables["Customer"].Rows[0]["CustID"], DsInvoiceAR.Tables["InvcHead"].Rows[0]["Company"]), fontTitle);
                Paragraph prgTitle2 = new Paragraph(string.Format("{0}\nPBX:{1}\nA.A. 35155\n{2} {3}\nwww.aldoronline.com",
                                                                    DsInvoiceAR.Tables["Customer"].Rows[0]["Address1"].ToString(),
                                                                    DsInvoiceAR.Tables["Customer"].Rows[0]["PhoneNum"].ToString(),
                                                                    DsInvoiceAR.Tables["Customer"].Rows[0]["City"].ToString(),
                                                                    DsInvoiceAR.Tables["Customer"].Rows[0]["State"].ToString()), fontTitle2);
                prgTitle.Alignment = Element.ALIGN_LEFT;
                prgTitle2.Alignment = Element.ALIGN_LEFT;
                cell_info.AddElement(prgTitle);
                cell_info.AddElement(prgTitle2);
                info.AddCell(cell_info);

                //agregamos el codigo Qr
                PdfPCell cell_Qr = new PdfPCell() { Border = 0, };
                iTextSharp.text.Image ImgQR = iTextSharp.text.Image.GetInstance(QRInvoice, BaseColor.WHITE);
                ImgQR.ScaleAbsolute(70, 70);
                cell_Qr.AddElement(ImgQR);
                info.AddCell(cell_Qr);

                //agregamos numeode factura 
                PdfPCell cell_factura = new PdfPCell() { Border = 0, };
                Paragraph prgTitle3 = new Paragraph(string.Format("NOTA DEBITO, fontTitle2"));
                Paragraph prgTitle4 = new Paragraph(string.Format("" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"].ToString(), fontTitle2));
                prgTitle3.Alignment = Element.ALIGN_RIGHT;
                prgTitle4.Alignment = Element.ALIGN_RIGHT;
                cell_factura.AddElement(prgTitle3);
                cell_factura.AddElement(prgTitle4);
                info.AddCell(cell_factura);

                info2.AddElement(info);


                PdfPTable info3 = new PdfPTable(new float[] { 0.35f, 0.30f, 0.35f, });
                info3.WidthPercentage = 100;


                //egregamos la informaion tributaria
                PdfPCell cell_info3 = new PdfPCell() { Border = 0, };
                Paragraph prgTitle33 = new Paragraph(string.Format("", fontTitle2));
                Paragraph prgTitle23 = new Paragraph(string.Format("", fontTitle2));
                prgTitle33.Alignment = Element.ALIGN_LEFT;
                prgTitle23.Alignment = Element.ALIGN_LEFT;
                cell_info3.AddElement(prgTitle33);
                cell_info3.AddElement(prgTitle23);
                info3.AddCell(cell_info3);

                //agregamos el codigo Qr
                PdfPCell cell_factura5 = new PdfPCell() { Border = 0, };
                Paragraph prgTitle5 = new Paragraph(string.Format("{0}\nNIT.{1}", DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"], DsInvoiceAR.Tables["Company"].Rows[0]["Name"]), fontTitle2);
                Paragraph prgTitle51 = new Paragraph(string.Format("{0}\nPBX:{1}\nA.A.",
                                                                    DsInvoiceAR.Tables["Company"].Rows[0]["Address1"].ToString(),
                                                                    DsInvoiceAR.Tables["Company"].Rows[0]["State"].ToString()), fontTitle2);
                prgTitle5.Alignment = Element.ALIGN_CENTER;
                prgTitle51.Alignment = Element.ALIGN_CENTER;
                cell_factura5.AddElement(prgTitle5);
                cell_factura5.AddElement(prgTitle51);
                info3.AddCell(cell_factura5);

                //agregamos numeode factura 
                PdfPCell cell_factura4 = new PdfPCell() { Border = 0, };
                Paragraph prgTitle34 = new Paragraph(string.Format("", fontTitle2));
                Paragraph prgTitle43 = new Paragraph(string.Format("", fontTitle2));
                prgTitle34.Alignment = Element.ALIGN_RIGHT;
                prgTitle43.Alignment = Element.ALIGN_RIGHT;
                cell_factura4.AddElement(prgTitle34);
                cell_factura4.AddElement(prgTitle43);
                info3.AddCell(cell_factura4);


                info2.AddElement(info3);

                info1.AddCell(info2);
                encabezado.AddElement(info1);
                header.AddCell(encabezado);

                #endregion

                #region Body

                PdfPTable Body = new PdfPTable(5);
                Body.WidthPercentage = 100;

                PdfPCell fecha_factura = new PdfPCell();
                //agregamos la fechas de la facrura
                PdfPTable titulo_fecha = new PdfPTable(1);
                titulo_fecha.WidthPercentage = 100;
                PdfPCell titulo_fecha1 = new PdfPCell(new Phrase("FECHA FACTURA", fontTitle)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, };
                titulo_fecha.AddCell(titulo_fecha1);
                fecha_factura.AddElement(titulo_fecha);

                //agregamos la fechas de la facrura
                PdfPTable valor_fecha = new PdfPTable(1);
                valor_fecha.WidthPercentage = 100;
                PdfPCell valor_fecha1 = new PdfPCell(new Phrase("" + DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).ToString("dd/mm/yyyy"), fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, };
                valor_fecha.AddCell(valor_fecha1);
                fecha_factura.AddElement(valor_fecha);

                Body.AddCell(fecha_factura);

                PdfPCell fecha_vencimiento = new PdfPCell();
                //agregamos la fechas de elvensimiento
                PdfPTable titulo_bencimiento = new PdfPTable(1);
                titulo_bencimiento.WidthPercentage = 100;
                PdfPCell titulo_bencimiento1 = new PdfPCell(new Phrase("FECHA VENCIMIENTO", fontTitle)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, };
                titulo_bencimiento.AddCell(titulo_bencimiento1);
                fecha_vencimiento.AddElement(titulo_bencimiento);

                //agregamos la fechas de elvensimiento
                PdfPTable valor_bencimiento = new PdfPTable(1);
                valor_bencimiento.WidthPercentage = 100;
                PdfPCell valor_bencimiento1 = new PdfPCell(new Phrase("" + DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"]).ToString("dd/mm/yyyy"), fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, };
                valor_bencimiento.AddCell(valor_bencimiento1);
                fecha_vencimiento.AddElement(valor_bencimiento);

                Body.AddCell(fecha_vencimiento);

                PdfPCell vendedor = new PdfPCell();
                //agregamos el vendedor
                PdfPTable titulo_vendedor = new PdfPTable(1);
                titulo_vendedor.WidthPercentage = 100;
                PdfPCell titulo_vendedor1 = new PdfPCell(new Phrase("NOMBRE C. COMPRADOR", fontTitle)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, };
                titulo_vendedor.AddCell(titulo_vendedor1);
                vendedor.AddElement(titulo_vendedor);

                //agregamos el vendedor
                PdfPTable valor_vendedor = new PdfPTable(1);
                valor_vendedor.WidthPercentage = 100;
                PdfPCell valor_vendedor1 = new PdfPCell(new Phrase("" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar03"].ToString(), fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, };
                valor_vendedor.AddCell(valor_vendedor1);
                vendedor.AddElement(valor_vendedor);

                Body.AddCell(vendedor);

                PdfPCell forma_pago = new PdfPCell();

                PdfPTable titulo_pago1 = new PdfPTable(1);
                titulo_pago1.WidthPercentage = 100;
                PdfPCell titulo_pogo = new PdfPCell(new Phrase("CORREO DEL CONTACTO", fontTitle)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, };
                titulo_pago1.AddCell(titulo_pogo);
                forma_pago.AddElement(titulo_pago1);

                //agregamos el vendedor
                PdfPTable valor_pago = new PdfPTable(1);
                valor_pago.WidthPercentage = 100;
                PdfPCell valor_pago1 = new PdfPCell(new Phrase("" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar04"].ToString(), fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, };
                valor_pago.AddCell(valor_pago1);
                forma_pago.AddElement(valor_pago);

                Body.AddCell(forma_pago);

                PdfPCell transportador = new PdfPCell();
                //agregamos el vendedor
                PdfPTable titulo_transportador = new PdfPTable(1);
                titulo_transportador.WidthPercentage = 100;
                PdfPCell titulo_transportador1 = new PdfPCell(new Phrase("Nro. DE FACTURA", fontTitle)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, };
                titulo_transportador.AddCell(titulo_transportador1);
                transportador.AddElement(titulo_transportador);

                PdfPTable valor_transportador = new PdfPTable(1);
                valor_transportador.WidthPercentage = 100;
                PdfPCell valor_transportador1 = new PdfPCell(new Phrase("" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar02"].ToString(), fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, };
                valor_transportador.AddCell(valor_transportador1);
                transportador.AddElement(valor_transportador);
                Body.AddCell(transportador);

                //agregamos una leyenda 
                PdfPTable leyenda = new PdfPTable(1);
                leyenda.WidthPercentage = 100;
                PdfPCell leyendas = new PdfPCell(new Phrase("Resolución DIAN No. 18762008997356 de 2018-07-04 Prefijo UN- Numeración 11664.0000 a la 20000.0000, vigencia 24 meses. Impresión por computador", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER };
                leyenda.AddCell(leyendas);



                //agregamos los titulos de los detalles
                PdfPTable detalles = new PdfPTable(new float[] { 0.40f, 0.14f, 0.14f, 0.14f, 0.14f, 0.14f, });
                detalles.WidthPercentage = 100;

                PdfPCell DESCRIPCIÓN = new PdfPCell(new Phrase("DESCRIPCIÓN", fontTitle)) { HorizontalAlignment = Element.ALIGN_LEFT, MinimumHeight = 15, VerticalAlignment = Element.ALIGN_BOTTOM };
                PdfPCell CANTIDAD = new PdfPCell(new Phrase("CANTIDAD", fontTitle)) { HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_BOTTOM };
                PdfPCell UND = new PdfPCell(new Phrase("UND", fontTitle)) { HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_BOTTOM };
                PdfPCell vUNITARIO = new PdfPCell(new Phrase("VR. UNITARIO", fontTitle)) { HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_BOTTOM };
                PdfPCell IVA = new PdfPCell(new Phrase("", fontTitle)) { HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_BOTTOM };
                PdfPCell TOTAL = new PdfPCell(new Phrase("TOTAL", fontTitle)) { HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_BOTTOM };

                detalles.AddCell(DESCRIPCIÓN);
                detalles.AddCell(CANTIDAD);
                detalles.AddCell(UND);
                detalles.AddCell(vUNITARIO);
                detalles.AddCell(IVA);
                detalles.AddCell(TOTAL);

                #endregion

                #region unidades

                PdfPTable tableTituloUnidades = new PdfPTable(6);
                //Dimenciones.
                float[] DimencionUnidades = new float[6];
                DimencionUnidades[0] = 0.40f;//
                DimencionUnidades[1] = 0.14f;//
                DimencionUnidades[2] = 0.14f;//
                DimencionUnidades[3] = 0.14f;//
                DimencionUnidades[4] = 0.14f;//
                DimencionUnidades[5] = 0.14f;//

                PdfPTable tableUnidades = new PdfPTable(6);
                tableUnidades.WidthPercentage = 100;
                tableUnidades.SetWidths(DimencionUnidades);

                foreach (DataRow InvoiceLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
                {
                    AddUnidadesnotacreditoCoodesival(InvoiceLine, ref tableUnidades, fontCustom);
                }

                iTextSharp.text.pdf.PdfPCell LineaFinal = new iTextSharp.text.pdf.PdfPCell();
                LineaFinal.Colspan = 6;
                LineaFinal.Padding = 3;
                LineaFinal.Border = 1;
                LineaFinal.BorderColorBottom = BaseColor.WHITE;
                LineaFinal.HorizontalAlignment = Element.ALIGN_CENTER;
                LineaFinal.VerticalAlignment = Element.ALIGN_TOP;
                tableUnidades.AddCell(LineaFinal);


                #endregion

                #region Footer

                PdfPTable pie = new PdfPTable(new float[] { 0.7f, 0.3f, });
                pie.WidthPercentage = 100;
                //agregamos  una tabla con las notas 
                PdfPCell nota = new PdfPCell() { Border = 0, };

                PdfPTable nota_tabla = new PdfPTable(1);
                nota_tabla.WidthPercentage = 100;
                PdfPCell nota_tabla1 = new PdfPCell(new Phrase("Nota:", fontTitle)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP, MinimumHeight = 20, };
                nota_tabla.AddCell(nota_tabla1);
                nota.AddElement(nota_tabla);

                //agregamos el valor en letras 
                PdfPTable valoe_l = new PdfPTable(1);
                valoe_l.WidthPercentage = 100;
                PdfPCell valoe_l1 = new PdfPCell(new Phrase("VALOR EN LETRA:", fontTitle)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP, MinimumHeight = 20, };
                valoe_l.AddCell(valoe_l1);
                nota.AddElement(valoe_l);

                //agregamos  una tabla con la leyenda 
                PdfPTable leyenda_pie = new PdfPTable(1);
                leyenda_pie.WidthPercentage = 100;
                PdfPCell leyenda_pie1 = new PdfPCell(new Phrase("", fontTitle))
                { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP, MinimumHeight = 20, };
                leyenda_pie.AddCell(leyenda_pie1);
                nota.AddElement(leyenda_pie);


                pie.AddCell(nota);


                PdfPCell totales = new PdfPCell() { Border = 0, };

                PdfPTable valores_total = new PdfPTable(2);
                valores_total.WidthPercentage = 100;

                PdfPCell titulo_totales = new PdfPCell() { Border = 0, };

                //agregamos el valor subtotal
                PdfPTable subtotal = new PdfPTable(1);
                subtotal.WidthPercentage = 100;
                PdfPCell subtotal1 = new PdfPCell(new Phrase("SUBTOTAL", fontTitle)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_BOTTOM, MinimumHeight = 30, };
                subtotal.AddCell(subtotal1);
                titulo_totales.AddElement(subtotal);

                //agregamos el valor DESCUENTO
                PdfPTable DESCUENTO = new PdfPTable(1);
                DESCUENTO.WidthPercentage = 100;
                PdfPCell DESCUENTO1 = new PdfPCell(new Phrase("IVA", fontTitle)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_BOTTOM, };
                DESCUENTO.AddCell(DESCUENTO1);
                titulo_totales.AddElement(DESCUENTO);

                //agregamos el valor IVA
                PdfPTable IVA_v = new PdfPTable(1);
                IVA_v.WidthPercentage = 100;
                PdfPCell IVA1 = new PdfPCell(new Phrase("RETE IVA", fontTitle)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_BOTTOM, };
                IVA_v.AddCell(IVA1);
                titulo_totales.AddElement(IVA_v);

                //agregamos el valor RETEFUENTE
                PdfPTable RETEFUENTE = new PdfPTable(1);
                RETEFUENTE.WidthPercentage = 100;
                PdfPCell RETEFUENTE1 = new PdfPCell(new Phrase("RETEFUENTE", fontTitle)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_BOTTOM, };
                RETEFUENTE.AddCell(RETEFUENTE1);
                titulo_totales.AddElement(RETEFUENTE);

                //agregamos el valor FLETE
                PdfPTable FLETE = new PdfPTable(1);
                FLETE.WidthPercentage = 100;
                PdfPCell FLETE1 = new PdfPCell(new Phrase("TOTAL", fontTitle)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_BOTTOM, };
                FLETE.AddCell(FLETE1);
                titulo_totales.AddElement(FLETE);


                PdfPCell valores_totales = new PdfPCell() { Border = 0, };

                //agregamos el valor subtotal
                PdfPTable subtotalF = new PdfPTable(1);
                subtotalF.WidthPercentage = 100;
                PdfPCell subtotalF1 = new PdfPCell(new Phrase(
                    decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubTotal"]).ToString("N0"), fontTitle))
                { Border = 0, HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_BOTTOM, MinimumHeight = 30, };
                subtotalF.AddCell(subtotalF1);
                valores_totales.AddElement(subtotalF);

                //agregamos el valor DESCUENTO
                PdfPTable DESCUENTOF = new PdfPTable(1);
                DESCUENTOF.WidthPercentage = 100;
                PdfPCell DESCUENTOF1 = new PdfPCell(new Phrase(
                    decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DocWHTaxAmt"]).ToString("N0"), fontTitle))
                { Border = 0, HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_BOTTOM, };
                DESCUENTOF.AddCell(DESCUENTOF1);
                valores_totales.AddElement(DESCUENTOF);

                //agregamos el valor IVA
                PdfPTable IVA_f = new PdfPTable(1);
                IVA_f.WidthPercentage = 100;
                PdfPCell IVA_f1 = new PdfPCell(new Phrase(string.Format("{0}",
                    GetValImpuestoByID("0B", DsInvoiceAR).ToString("N0")), fontTitle))
                { Border = 0, HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_BOTTOM, };
                IVA_f.AddCell(IVA_f1);
                valores_totales.AddElement(IVA_f);

                //agregamos el valor RETEFUENTE
                PdfPTable RETEFUENTEf = new PdfPTable(1);
                RETEFUENTEf.WidthPercentage = 100;
                PdfPCell RETEFUENTEf1 = new PdfPCell(new Phrase(string.Format("{0}", GetValImpuestoByID("0A", DsInvoiceAR).ToString("N0")), fontTitle)) { Border = 0, HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_BOTTOM, };
                RETEFUENTEf.AddCell(RETEFUENTEf1);
                valores_totales.AddElement(RETEFUENTEf);

                //agregamos el valor FLETE
                PdfPTable FLETEf = new PdfPTable(1);
                FLETEf.WidthPercentage = 100;
                PdfPCell FLETEf1 = new PdfPCell(new Phrase(
                    decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"]).ToString("N0"), fontTitle))
                { Border = 0, HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_BOTTOM, };
                FLETEf.AddCell(FLETEf1);
                valores_totales.AddElement(FLETEf);


                valores_total.AddCell(titulo_totales);
                valores_total.AddCell(valores_totales);

                totales.AddElement(valores_total);
                pie.AddCell(totales);

                PdfPTable tabla_cufe = new PdfPTable(1);
                tabla_cufe.WidthPercentage = 100;
                PdfPCell cufe = new PdfPCell(new Phrase("CUFE:" + CUFE, fontTitle)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_BOTTOM };
                tabla_cufe.AddCell(cufe);

                #endregion

                #region Exit

                document.Add(header);
                document.Add(Body);
                document.Add(leyenda);
                document.Add(detalles);
                document.Add(tableTituloUnidades);
                document.Add(tableUnidades);
                document.Add(pie);
                document.Add(tabla_cufe);

                //document.Add(divTextBancoCheque);
                /*PIE DE PAGINA*/
                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();
                RutaPdf = NomArchivo;
                return true;
                #endregion
            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.Message;
                return false;
            }

        }

            private bool AddUnidadesYobel(DataRow dataLine, ref PdfPTable table, iTextSharp.text.Font fontTitleFactura, DataSet dataSet, ref decimal TotalUnd)
            {
                try
                {

                    strError += "Add SellingShipQty";
                    iTextSharp.text.pdf.PdfPCell celValCantidad = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                        decimal.Parse((string)dataLine["SellingShipQty"]).ToString("N2"), fontTitleFactura));
                    celValCantidad.Colspan = 1;
                    celValCantidad.Padding = 2;
                    celValCantidad.Border = 0;
                    celValCantidad.BorderWidthLeft = 1;
                    celValCantidad.HorizontalAlignment = Element.ALIGN_RIGHT;
                    celValCantidad.VerticalAlignment = Element.ALIGN_TOP;
                    table.AddCell(celValCantidad);
                    TotalUnd += Convert.ToDecimal(dataLine["SellingShipQty"]);
                    strError += "Add SellingShipQty OK";


                    strError += "Add LineDesc";
                    iTextSharp.text.pdf.PdfPCell celValDesc = new iTextSharp.text.pdf.PdfPCell(new Phrase
                        ((string)dataLine["LineDesc"], fontTitleFactura));
                    celValDesc.Colspan = 1;
                    celValDesc.Padding = 2;
                    celValDesc.Border = 0;
                    celValDesc.BorderWidthLeft = 1;
                    celValDesc.HorizontalAlignment = Element.ALIGN_LEFT;
                    celValDesc.VerticalAlignment = Element.ALIGN_TOP;
                    table.AddCell(celValDesc);
                    strError += "Add LineDesc OK";

                    strError += "Add DocUnitPrice";
                    iTextSharp.text.pdf.PdfPCell celValValorUnitario = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                        decimal.Parse((string)dataLine["DocUnitPrice"]).ToString("N2"), fontTitleFactura));
                    celValValorUnitario.Colspan = 1;
                    celValValorUnitario.Border = 0;
                    celValValorUnitario.BorderWidthLeft = 1;
                    celValValorUnitario.Padding = 2;
                    celValValorUnitario.HorizontalAlignment = Element.ALIGN_RIGHT;
                    celValValorUnitario.VerticalAlignment = Element.ALIGN_TOP;
                    table.AddCell(celValValorUnitario);
                    strError += "Add DocUnitPrice OK";


                    strError += "Add DspDocExtPrice";
                    iTextSharp.text.pdf.PdfPCell celValValorTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                        decimal.Parse((string)dataLine["DspDocExtPrice"]).ToString("N2"),
                        fontTitleFactura));
                    celValValorTotal.Colspan = 1;
                    celValValorTotal.Padding = 2;
                    celValValorTotal.Border = 0;
                    celValValorTotal.BorderWidthLeft = 1;
                    celValValorTotal.BorderWidthRight = 1;
                    celValValorTotal.HorizontalAlignment = Element.ALIGN_RIGHT;
                    celValValorTotal.VerticalAlignment = Element.ALIGN_TOP;
                    table.AddCell(celValValorTotal);
                    strError += "Add DspDocExtPrice OK";

                    return true;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                    return false;
                }
            }

            private bool AddUnidadesYobel(DataRow dataLine, ref PdfPTable table, iTextSharp.text.Font fontTitleFactura, DataSet dataSet)
            {
                try
                {
                    //FACTURAS
                    strError += "Add InvoiceLine";
                    iTextSharp.text.pdf.PdfPCell celValL = new iTextSharp.text.pdf.PdfPCell(new Phrase((string)dataLine["InvoiceLine"], fontTitleFactura));
                    celValL.Colspan = 1;
                    celValL.Padding = 2;
                    celValL.Border = 0;
                    //celTextL.BorderWidthRight = 1;
                    //celValL.BorderWidthLeft = 1;
                    celValL.HorizontalAlignment = Element.ALIGN_LEFT;
                    celValL.VerticalAlignment = Element.ALIGN_TOP;
                    table.AddCell(celValL);
                    strError += "InvoiceLine OK";

                    //REFERENCIA
                    strError += "Add PartNum";
                    iTextSharp.text.pdf.PdfPCell celValCodigo = new iTextSharp.text.pdf.PdfPCell(new Phrase((string)dataLine["PartNumPartDescription"], fontTitleFactura));
                    celValCodigo.Colspan = 1;
                    celValCodigo.Padding = 2;
                    celValCodigo.Border = 0;
                    //celValCodigo.BorderWidthLeft = 1;
                    //celDescripcion.BorderColorBottom = BaseColor.WHITE;
                    celValCodigo.HorizontalAlignment = Element.ALIGN_LEFT;
                    celValCodigo.VerticalAlignment = Element.ALIGN_TOP;
                    table.AddCell(celValCodigo);
                    strError += "Add PartNum OK";

                    //CANTIDAD
                    strError += "Add SellingShipQty";
                    iTextSharp.text.pdf.PdfPCell celValDescripcion = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                        decimal.Parse((string)dataLine["SellingShipQty"]).ToString("N0"), fontTitleFactura));
                    celValDescripcion.Colspan = 1;
                    celValDescripcion.Padding = 2;
                    celValDescripcion.Border = 0;
                    //celValCantidad.BorderWidthLeft = 1;
                    celValDescripcion.HorizontalAlignment = Element.ALIGN_LEFT;
                    celValDescripcion.VerticalAlignment = Element.ALIGN_TOP;
                    table.AddCell(celValDescripcion);
                    strError += "Add SellingShipQty OK";

                    //VALOR
                    strError += "Add SalesUM";
                    iTextSharp.text.pdf.PdfPCell celValUnidad = new iTextSharp.text.pdf.PdfPCell(new Phrase("$ " +
                        decimal.Parse((string)dataLine["DocUnitPrice"]).ToString("N2"), fontTitleFactura));
                    celValUnidad.Colspan = 1;
                    celValUnidad.Padding = 2;
                    celValUnidad.Border = 0;
                    //celValUnidad.BorderWidthLeft = 1;
                    celValUnidad.HorizontalAlignment = Element.ALIGN_LEFT;
                    celValUnidad.VerticalAlignment = Element.ALIGN_TOP;
                    table.AddCell(celValUnidad);
                    strError += "Add SalesUM OK";

                    //DESCRIPCION
                    strError += "Add LineDesc";
                    iTextSharp.text.pdf.PdfPCell celValCantidad = new iTextSharp.text.pdf.PdfPCell(new Phrase("$ " +
                        dataLine["LineDesc"], fontTitleFactura));
                    celValCantidad.Colspan = 1;
                    celValCantidad.Padding = 2;
                    celValCantidad.Border = 0;
                    //celValDesc.BorderWidthLeft = 1;
                    celValCantidad.HorizontalAlignment = Element.ALIGN_LEFT;
                    celValCantidad.VerticalAlignment = Element.ALIGN_TOP;
                    table.AddCell(celValCantidad);
                    strError += "Add LineDesc OK";

                    strError += "Add LineDesc";
                    iTextSharp.text.pdf.PdfPCell celValCantidadv = new iTextSharp.text.pdf.PdfPCell(new Phrase("" +
                        dataLine["LineDesc"], fontTitleFactura));
                    celValCantidadv.Colspan = 1;
                    celValCantidadv.Padding = 2;
                    celValCantidadv.Border = 0;
                    //celValDesc.BorderWidthLeft = 1;
                    celValCantidadv.HorizontalAlignment = Element.ALIGN_LEFT;
                    celValCantidadv.VerticalAlignment = Element.ALIGN_TOP;
                    table.AddCell(celValCantidadv);
                    strError += "Add LineDesc OK";
                    return true;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                    return false;
                }
            }

            private bool AddUnidadesYobel(ref PdfPTable table)
            {
                try
                {
                    iTextSharp.text.pdf.PdfPCell celValL = new iTextSharp.text.pdf.PdfPCell(new Phrase(" "));
                    celValL.Colspan = 1;
                    celValL.Padding = 3;
                    celValL.Border = 0;
                    //celTextL.BorderWidthRight = 1;
                    celValL.BorderWidthLeft = 1;
                    celValL.HorizontalAlignment = Element.ALIGN_CENTER;
                    celValL.VerticalAlignment = Element.ALIGN_TOP;

                    table.AddCell(celValL);

                    iTextSharp.text.pdf.PdfPCell celValCodigo = new iTextSharp.text.pdf.PdfPCell(new Phrase(" "));
                    celValCodigo.Colspan = 1;
                    celValCodigo.Padding = 3;
                    celValCodigo.Border = 0;
                    celValCodigo.BorderWidthLeft = 1;
                    //celDescripcion.BorderColorBottom = BaseColor.WHITE;
                    celValCodigo.HorizontalAlignment = Element.ALIGN_CENTER;
                    celValCodigo.VerticalAlignment = Element.ALIGN_TOP;

                    table.AddCell(celValCodigo);

                    iTextSharp.text.pdf.PdfPCell celValCantidad = new iTextSharp.text.pdf.PdfPCell(new Phrase(" "));
                    celValCantidad.Colspan = 1;
                    celValCantidad.Padding = 3;
                    celValCantidad.Border = 0;
                    celValCantidad.BorderWidthLeft = 1;
                    celValCantidad.HorizontalAlignment = Element.ALIGN_CENTER;
                    celValCantidad.VerticalAlignment = Element.ALIGN_TOP;

                    table.AddCell(celValCantidad);

                    iTextSharp.text.pdf.PdfPCell celValUnidad = new iTextSharp.text.pdf.PdfPCell(new Phrase(" "));
                    celValUnidad.Colspan = 1;
                    celValUnidad.Padding = 3;
                    celValUnidad.Border = 0;
                    celValUnidad.BorderWidthLeft = 1;
                    celValUnidad.HorizontalAlignment = Element.ALIGN_CENTER;
                    celValUnidad.VerticalAlignment = Element.ALIGN_TOP;

                    table.AddCell(celValUnidad);

                    iTextSharp.text.pdf.PdfPCell celValDesc = new iTextSharp.text.pdf.PdfPCell(new Phrase(" "));
                    celValDesc.Colspan = 1;
                    celValDesc.Padding = 3;
                    celValDesc.Border = 0;
                    celValDesc.BorderWidthLeft = 1;
                    celValDesc.HorizontalAlignment = Element.ALIGN_CENTER;
                    celValDesc.VerticalAlignment = Element.ALIGN_TOP;

                    table.AddCell(celValDesc);

                    iTextSharp.text.pdf.PdfPCell celValValorUnitario = new iTextSharp.text.pdf.PdfPCell(new Phrase(" "));
                    celValValorUnitario.Colspan = 1;
                    celValValorUnitario.Border = 0;
                    celValValorUnitario.BorderWidthLeft = 1;
                    celValValorUnitario.Padding = 3;
                    celValValorUnitario.HorizontalAlignment = Element.ALIGN_CENTER;
                    celValValorUnitario.VerticalAlignment = Element.ALIGN_TOP;

                    table.AddCell(celValValorUnitario);

                    iTextSharp.text.pdf.PdfPCell celValDescuento = new iTextSharp.text.pdf.PdfPCell(new Phrase(" "));
                    celValDescuento.Colspan = 1;
                    celValDescuento.Padding = 3;
                    celValDescuento.Border = 0;
                    celValDescuento.BorderWidthLeft = 1;
                    celValDescuento.HorizontalAlignment = Element.ALIGN_CENTER;
                    celValDescuento.VerticalAlignment = Element.ALIGN_TOP;

                    table.AddCell(celValDescuento);

                    iTextSharp.text.pdf.PdfPCell celValValorTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase(" "));
                    celValValorTotal.Colspan = 1;
                    celValValorTotal.Padding = 3;
                    celValValorTotal.Border = 0;
                    celValValorTotal.BorderWidthLeft = 1;
                    celValValorTotal.HorizontalAlignment = Element.ALIGN_CENTER;
                    celValValorTotal.VerticalAlignment = Element.ALIGN_TOP;

                    table.AddCell(celValValorTotal);

                    iTextSharp.text.pdf.PdfPCell celValIva = new iTextSharp.text.pdf.PdfPCell(new Phrase(" "));
                    celValIva.Colspan = 1;
                    celValIva.Padding = 3;
                    celValIva.Border = 0;
                    celValIva.BorderWidthLeft = 1;
                    celValIva.BorderWidthRight = 1;
                    celValIva.HorizontalAlignment = Element.ALIGN_CENTER;
                    celValIva.VerticalAlignment = Element.ALIGN_TOP;

                    table.AddCell(celValIva);
                    return true;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                    return false;
                }
            }

            public partial class HelpersEletroHome
            {
                public class FuentesYobel
            {
                    public static iTextSharp.text.Font Plantilla_4_fontTitleAEI
                    {
                        get
                        {
                            return FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                        }
                    }
                }
            }

            public class Yobel
            {
                public class FacturaNacional
                {
                    public static PdfPTable DatosCliente(DataSet ds, string CUFE, System.Drawing.Image image)
                    {
                        Helpers.Plantilla_4.DatosClienteDatos datos = new Helpers.Plantilla_4.DatosClienteDatos(ds);

                        PdfPTable tableFacturar = new PdfPTable(3);
                        //Dimenciones.
                        float[] DimencionFacturar = new float[3];
                        DimencionFacturar[0] = 1.0F;//
                        DimencionFacturar[1] = 1.0F;//
                        DimencionFacturar[2] = 1.0F;//

                        tableFacturar.WidthPercentage = 100;
                        tableFacturar.SetWidths(DimencionFacturar);
                        //----------------------------------------------------------------------------------------------
                        PdfPCell celNofactura = new PdfPCell(new Phrase(
                            $"FACTURA DE VENTA No. {(string)ds.Tables["InvcHead"].Rows[0]["LegalNumber"]}",
                            FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 12, iTextSharp.text.Font.NORMAL)));
                        celNofactura.HorizontalAlignment = Element.ALIGN_RIGHT;
                        celNofactura.Colspan = 3;
                        celNofactura.Padding = 3;
                        celNofactura.BorderWidthLeft = 0;
                        celNofactura.BorderWidthRight = 0;
                        tableFacturar.AddCell(celNofactura);
                        //----------------------------------------------------------------------------------------------
                        PdfPTable tableDatosCliente = new PdfPTable(1);
                        float[] DimencionFacturarA = new float[2];
                        DimencionFacturarA[0] = 1.0F;//
                        DimencionFacturarA[1] = 9.0F;//

                        tableDatosCliente.WidthPercentage = 100;
                        //-------------------------- Señores -------------------------------------------------------------------
                        PdfPCell celNombreCliente = new PdfPCell();
                        //celNombreCliente.AddElement(phrSeñores);
                        celNombreCliente.Border = 0;
                        //celNombreCliente.Padding = 0;
                        Helpers.Plantilla_4.DatosClienteTitulos(ref celNombreCliente, "Señores: ", ds.Tables["Customer"].Rows[0]["Name"].ToString());
                        tableDatosCliente.AddCell(celNombreCliente);


                        //--------------------------- NIT -------------------------------------------------------------------------

                        Chunk TituloNIT = new Chunk("NIT: ", Helpers.Fuentes.Plantilla_4_fontTitle2);
                        Chunk ValorNIT = new Chunk((string)ds.Tables["Customer"].Rows[0]["ResaleID"] + "\r\n", Helpers.Fuentes.Plantilla_4_fontTitleFactura);
                        Phrase phrNIT = new Phrase();
                        phrNIT.Add(TituloNIT);
                        phrNIT.Add(ValorNIT);
                        PdfPCell celNit = new PdfPCell();
                        celNit.AddElement(phrNIT);
                        celNit.Border = 0;
                        tableDatosCliente.AddCell(celNit);
                        //------------------- Teléfono y Fax --------------------------------------------------------------------------------------------------
                        PdfPTable tableTel = new PdfPTable(2);
                        tableTel.WidthPercentage = 100;
                        tableTel.SetWidths(new float[] { 1f, 1f });
                        tableTel.PaddingTop = 0;

                        Chunk TituloTelefono = new Chunk("Teléfono: ", Helpers.Fuentes.Plantilla_4_fontTitle2);
                        Chunk ValorTelefono = new Chunk((string)ds.Tables["Customer"].Rows[0]["PhoneNum"], Helpers.Fuentes.Plantilla_4_fontTitleFactura);
                        Phrase phrTelefono = new Phrase();
                        phrTelefono.Add(TituloTelefono);
                        phrTelefono.Add(ValorTelefono);
                        PdfPCell celValTelefonoCliente = new PdfPCell();
                        celValTelefonoCliente.AddElement(phrTelefono);
                        celValTelefonoCliente.Colspan = 1;
                        //celValTelefonoCliente.Padding = 0;
                        celValTelefonoCliente.Border = 0;
                        celValTelefonoCliente.HorizontalAlignment = Element.ALIGN_LEFT;
                        celValTelefonoCliente.VerticalAlignment = Element.ALIGN_TOP;
                        tableTel.AddCell(celValTelefonoCliente);

                        Chunk TituloFax = new Chunk("Fax: ", Helpers.Fuentes.Plantilla_4_fontTitle2);
                        Chunk ValorFax = new Chunk(ds.Tables["Customer"].Rows[0]["FaxNum"].ToString(), Helpers.Fuentes.Plantilla_4_fontTitleFactura);
                        Phrase phrFax = new Phrase();
                        phrFax.Add(TituloFax);
                        phrFax.Add(ValorFax);
                        PdfPCell celValFaxCliente = new PdfPCell();
                        celValFaxCliente.AddElement(phrFax);
                        celValFaxCliente.Colspan = 1;
                        celValFaxCliente.Padding = 0;
                        celValFaxCliente.Border = 0;
                        celValFaxCliente.HorizontalAlignment = Element.ALIGN_LEFT;
                        celValFaxCliente.VerticalAlignment = Element.ALIGN_TOP;
                        tableTel.AddCell(celValFaxCliente);

                        PdfPCell celTel = new PdfPCell(tableTel);
                        celTel.Border = 0;
                        tableDatosCliente.AddCell(celTel);
                        //--------- Evaluando forma de pago -------
                        if (datos.CelFormaPago != null)
                        {
                            tableDatosCliente.AddCell(datos.CelFormaPago);
                        }
                        //---------------- Domicilio principal -----------------------------------------------------------------------------------
                        Chunk TituloDomicilioPPal = new Chunk("Domicilio Ppal: ", Helpers.Fuentes.Plantilla_4_fontTitle2);
                        Chunk ValorDomicilioPPal = new Chunk(ds.Tables["Customer"].Rows[0]["Address1"].ToString(), Helpers.Fuentes.Plantilla_4_fontTitleFactura);
                        Phrase phrDomicilioPPal = new Phrase();
                        phrDomicilioPPal.Add(TituloDomicilioPPal);
                        phrDomicilioPPal.Add(ValorDomicilioPPal);

                        PdfPCell celDomicilioPpal = new PdfPCell(); //new PdfPCell(tableDomicilioPpal);
                        celDomicilioPpal.AddElement(phrDomicilioPPal);
                        celDomicilioPpal.Border = 0;
                        tableDatosCliente.AddCell(celDomicilioPpal);
                        //--------------------  ------------------------------------------------------------------------------------
                        PdfPCell celDomicilioEmbarque = new PdfPCell(); //new PdfPCell(tableDomicilioEmbarque);
                        Helpers.Plantilla_4.DatosClienteTitulos(ref celDomicilioEmbarque, "Domicilio : ", ds.Tables["InvcHead"].Rows[0]["Character03"].ToString());
                        celDomicilioEmbarque.Border = 0;
                        tableDatosCliente.AddCell(celDomicilioEmbarque);
                        //------------------- Codigo Tienda ------------------------------------
                        string CodigoTienda = string.Empty;
                        if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "CodigoTienda", 0))
                            CodigoTienda = ds.Tables["InvcHead"].Rows[0]["CodigoTienda"].ToString();
                        PdfPCell celCodigoTienda = new PdfPCell();
                        //Helpers.Plantilla_4.DatosClienteTitulos(ref celCodigoTienda, "Código Tienda: ", CodigoTienda);
                        celCodigoTienda.Border = 0;
                        tableDatosCliente.AddCell(celCodigoTienda);
                        //--------------------------------------------------------------------------------------------------------------
                        iTextSharp.text.pdf.PdfPCell celdatosCliente = new iTextSharp.text.pdf.PdfPCell(tableDatosCliente);
                        celdatosCliente.Colspan = 1;
                        tableFacturar.AddCell(celdatosCliente);
                        //--------------------------------------------------------------------------------------------------------------
                        iTextSharp.text.Font fontTitleFecha = FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL);
                        iTextSharp.text.Font fontDMA = FontFactory.GetFont(FontFactory.HELVETICA, 6, BaseColor.GRAY);
                        iTextSharp.text.Font fontDetalleFecha = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 8, iTextSharp.text.Font.NORMAL);
                        //--------------------------------------------------------------------------------------------------------------
                        PdfPTable tableToCel2 = new PdfPTable(2);
                        float[] DimencionDespacharA = new float[2];
                        DimencionDespacharA[0] = 1.0F;//
                        DimencionDespacharA[1] = 1.0F;//

                        tableToCel2.WidthPercentage = 100;
                        tableToCel2.SetWidths(DimencionDespacharA);

                        PdfPTable tableFechaEmision = new PdfPTable(1);

                        iTextSharp.text.pdf.PdfPCell celTextFechaEmision = new iTextSharp.text.pdf.PdfPCell(new Phrase("Fecha Emisión", fontTitleFecha));
                        celTextFechaEmision.Colspan = 1;
                        //celTextFechaEmision.Padding = 3;
                        celTextFechaEmision.Border = 0;
                        celTextFechaEmision.HorizontalAlignment = Element.ALIGN_CENTER;
                        celTextFechaEmision.VerticalAlignment = Element.ALIGN_TOP;
                        tableFechaEmision.AddCell(celTextFechaEmision);

                        iTextSharp.text.pdf.PdfPCell celDMA = new iTextSharp.text.pdf.PdfPCell(new Phrase("DD/MM/AAAA", fontDMA));
                        celDMA.Colspan = 1;
                        //celDMA.Padding = 3;
                        celDMA.Border = 0;
                        celDMA.HorizontalAlignment = Element.ALIGN_CENTER;
                        celDMA.VerticalAlignment = Element.ALIGN_TOP;
                        tableFechaEmision.AddCell(celDMA);

                        //------ Dato fecha y hora ------
                        //iTextSharp.text.pdf.PdfPCell celFechaEmisionVal = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                        //DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).ToString("dd/MM/yyyy"), fontDetalleFecha));
                        iTextSharp.text.pdf.PdfPCell celFechaEmisionVal = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                            datos.FechayHora, fontDetalleFecha));
                        celFechaEmisionVal.Colspan = 1;
                        //celFechaEmisionVal.Padding = 3;
                        celFechaEmisionVal.Border = 0;
                        celFechaEmisionVal.HorizontalAlignment = Element.ALIGN_CENTER;
                        celFechaEmisionVal.VerticalAlignment = Element.ALIGN_TOP;
                        tableFechaEmision.AddCell(celFechaEmisionVal);
                        PdfPCell celFechaEmision = new PdfPCell(tableFechaEmision);
                        tableToCel2.AddCell(celFechaEmision);

                        //---------------------------------------------------------------------------------------------------------------
                        PdfPTable tableFechaDetalleVencimiento = new PdfPTable(1);

                        iTextSharp.text.pdf.PdfPCell celTextDetalleFechaVencimiento = new iTextSharp.text.pdf.PdfPCell(new Phrase("Fecha Vencimiento", fontTitleFecha));
                        celTextDetalleFechaVencimiento.Colspan = 1;
                        //celTextDetalleFechaVencimiento.Padding = 3;
                        celTextDetalleFechaVencimiento.Border = 0;
                        celTextDetalleFechaVencimiento.HorizontalAlignment = Element.ALIGN_CENTER;
                        celTextDetalleFechaVencimiento.VerticalAlignment = Element.ALIGN_TOP;
                        tableFechaDetalleVencimiento.AddCell(celTextDetalleFechaVencimiento);

                        tableFechaDetalleVencimiento.AddCell(celDMA);

                        iTextSharp.text.pdf.PdfPCell celFechaVencimientoVal = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                            DateTime.Parse((string)ds.Tables["InvcHead"].Rows[0]["DueDate"]).ToString("dd/MM/yyyy"), fontDetalleFecha));
                        celFechaVencimientoVal.Colspan = 1;
                    //celFechaVencimientoVal.Padding = 3;NOMBRE C. COMPRADOR
                    celFechaVencimientoVal.Border = 0;
                        celFechaVencimientoVal.HorizontalAlignment = Element.ALIGN_CENTER;
                        celFechaVencimientoVal.VerticalAlignment = Element.ALIGN_TOP;
                        tableFechaDetalleVencimiento.AddCell(celFechaVencimientoVal);

                        PdfPCell celFechaDetalleVencimiento = new PdfPCell(tableFechaDetalleVencimiento);
                        tableToCel2.AddCell(celFechaDetalleVencimiento);
                        //-------------------------------------------------------------------------------------------------------------

                        iTextSharp.text.pdf.PdfPCell celNoPedido = new PdfPCell() { Border=0};
                        Helpers.Plantilla_4.DatosClienteTitulos(ref celNoPedido, "orden de compra: ", ds.Tables["InvcHead"].Rows[0]["OrderNum"].ToString());
                        tableToCel2.AddCell(celNoPedido);
                        //-------------------------------------------------------------------------------------
                        iTextSharp.text.pdf.PdfPCell celOrderCompra = new iTextSharp.text.pdf.PdfPCell() { Border=0};
                        //Helpers.Plantilla_4.DatosClienteTitulos(ref celOrderCompra, "Orden de Compra: ", (string)ds.Tables["InvcHead"].Rows[0]["PONum"]);
                        tableToCel2.AddCell(celOrderCompra);
                        //-------------------------------------------------------------------------------------

                        PdfPCell celVendededor = new PdfPCell();//new PdfPCell(tableVendedor);
                        Helpers.Plantilla_4.DatosClienteTitulos(ref celVendededor, "Vendedor: ", ds.Tables["InvcHead"].Rows[0]["SalesRepName1"].ToString());
                        celVendededor.Colspan = 2;
                        tableToCel2.AddCell(celVendededor);
                        //-------------------------------------------------------------------------------------

                        PdfPCell celCodigoVendededor = new PdfPCell();//new PdfPCell(tableCodigoVendedor);
                        Helpers.Plantilla_4.DatosClienteTitulos(ref celCodigoVendededor, "tipo de servicio: ", ds.Tables["InvcHead"].Rows[0]["SalesRepCode1"].ToString());
                        celCodigoVendededor.Colspan = 2;
                        tableToCel2.AddCell(celCodigoVendededor);
                        //-------------- Nro Aviso -------------------------
                        PdfPCell celnroAviso = new PdfPCell();
                        string NroAviso = string.Empty;
                        if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "NroAviso", 0))
                            NroAviso = ds.Tables["InvcHead"].Rows[0]["NroAviso"].ToString();
                        //Helpers.Plantilla_4.DatosClienteTitulos(ref celnroAviso, "Nro Aviso: ", NroAviso);
                        celnroAviso.Colspan = 2;
                        tableToCel2.AddCell(celnroAviso);
                        //-------------------------------------------------------------------------------------
                        iTextSharp.text.pdf.PdfPCell cel2 = new iTextSharp.text.pdf.PdfPCell(tableToCel2);
                        cel2.Colspan = 1;
                        tableFacturar.AddCell(cel2);


                        /*CELL QR*/
                        Paragraph prgCufe = new Paragraph("CUFE: " + CUFE, Helpers.Fuentes.Plantilla_4_fontTitle2);
                        prgCufe.Alignment = Element.ALIGN_LEFT;

                        iTextSharp.text.Image QRPdf = iTextSharp.text.Image.GetInstance(image, BaseColor.WHITE);
                        QRPdf.ScaleAbsolute(90f, 90f);
                        QRPdf.Alignment = Element.ALIGN_CENTER;
                        //iTextSharp.text.pdf.PdfPCell celImgQR = new iTextSharp.text.pdf.PdfPCell(QRPdf);
                        iTextSharp.text.pdf.PdfPCell celImgQR = new iTextSharp.text.pdf.PdfPCell();
                        celImgQR.AddElement(QRPdf);
                        celImgQR.AddElement(prgCufe);
                        celImgQR.Colspan = 2;
                        celImgQR.Padding = 3;
                        celImgQR.Border = 0;
                        celImgQR.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        celImgQR.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                        tableFacturar.AddCell(celImgQR);
                        //----- Retornando tabla
                        return tableFacturar;
                    }
                }
            }

            #endregion

        }
    }
