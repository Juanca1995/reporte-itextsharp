﻿namespace RulesServicesDIAN2.Adquiriente
{
    using iTextSharp.text;
    using iTextSharp.text.pdf;
    using System;
    //Referenciar y usar.
    using System.Data;
    using System.Drawing;
    using System.IO;
    using System.Net;
    using System.Net.NetworkInformation;
    using System.Net.Sockets;
    using System.Web;
    using System.Windows.Forms;

    public partial class pdfEstandarAR
    {
		#region Formato Factura Ubanas

        #region Urbanas
        public bool FacturaUrbanas(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {
            bool esLocal = Helpers.Compartido.EsLocal;
            string Ruta = string.Empty;
            string RutaImg = string.Empty;
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            if (esLocal)
            {
                Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
                RutaImg = $@"{AppDomain.CurrentDomain.BaseDirectory}\Logo_890200877.png";
            }
            else
            {
                Ruta = HttpContext.Current.Server.MapPath("~/PDF/" + NIT + "/");
                RutaImg = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";
            }
            NomArchivo = string.Empty;
            NomArchivo = NombreInvoice + ".pdf";

            try
            {
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.
                Document document = Helpers.Fedco.Documento;//new Document(iTextSharp.text.PageSize.LETTER);
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;

                //---- capturando las tablas estaticas
                PdfPTable Encabezado = Urbanas.FacturaNacional.Encabezado(DsInvoiceAR, QRInvoice, InvoiceType, RutaImg, CUFE);
                PdfPTable DatosCliente = Urbanas.FacturaNacional.DatosCliente(DsInvoiceAR);
                PdfPTable DatosFactura = Urbanas.FacturaNacional.DatosFactura(DsInvoiceAR);
                PdfPTable DetalleHead = Urbanas.FacturaNacional.DetalleHead(245);
                PdfPTable PiePagina = Urbanas.FacturaNacional.PiePagina(DsInvoiceAR);

                writer.PageEvent = new Urbanas.EventPageUrbanas(Encabezado, DatosCliente, DatosFactura, DetalleHead, PiePagina);

                // step 3: we open the document     
                document.Open();
                document.Add(Urbanas.FacturaNacional.Detalles(DsInvoiceAR));
                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();
                Helpers.Simex.AddPageNumberPagToSimexExp($"{Ruta}{NomArchivo}", dimxSimex, dimYSimex + 10, Helpers.Fuentes.SimexLeyenda);
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return false;
            }
        }
        #endregion

        #region Nota Credito Urbanas
        public bool NotaCreditoUrbanas(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {
            bool esLocal = Helpers.Compartido.EsLocal;
            string Ruta = string.Empty;
            string RutaImg = string.Empty;
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            if (esLocal)
            {
                Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
                RutaImg = $@"{AppDomain.CurrentDomain.BaseDirectory}\Logo_Fedco.png";
            }
            else
            {
                Ruta = HttpContext.Current.Server.MapPath("~/PDF/" + NIT + "/");
                RutaImg = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";
            }
            NomArchivo = string.Empty;
            NomArchivo = NombreInvoice + ".pdf";

            try
            {
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.
                Document document = Helpers.Fedco.Documento;//new Document(iTextSharp.text.PageSize.LETTER);
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;

                //---- capturando las tablas estaticas
                PdfPTable Encabezado = Notaurbanas.Notacionalurbanas.Encabezado(DsInvoiceAR, QRInvoice, InvoiceType, RutaImg);
                PdfPTable DatosCliente = Notaurbanas.Notacionalurbanas.DatosCliente(DsInvoiceAR);
                PdfPTable DatosFactura = Notaurbanas.Notacionalurbanas.DatosFactura(DsInvoiceAR);
                PdfPTable DetalleHead = Notaurbanas.Notacionalurbanas.DetalleHead(245);
                PdfPTable PiePagina = Notaurbanas.Notacionalurbanas.PiePagina(DsInvoiceAR);

                writer.PageEvent = new Helpers.Fedco.EventPageFedco(Encabezado, DatosCliente, DatosFactura, DetalleHead, PiePagina);

                // step 3: we open the document     
                document.Open();
                document.Add(Notaurbanas.Notacionalurbanas.Detalles(DsInvoiceAR));
                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();
                Helpers.Simex.AddPageNumberPagToSimexExp($"{Ruta}{NomArchivo}", dimxSimex, dimYSimex + 10, Helpers.Fuentes.SimexLeyenda);
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return false;
            }
        }
        #endregion

        #endregion

        public class Urbanas
        {
            public class FacturaNacional
            {
                public static PdfPTable Encabezado(DataSet ds, System.Drawing.Image QR, string InvoiceType, string RutaImg, string cufe)
                {
                    //---- Variables -----
                    string InvcHeadCompany = string.Empty;
                    string CompanyName = string.Empty;
                    string CompanyStateTaxID = string.Empty;
                    string CompanyAddress1 = string.Empty;
                    string CompanyAddress2 = string.Empty;
                    string CompanyAddress3 = string.Empty;
                    string CompanyAddress = string.Empty;
                    string CompanyCity = string.Empty;
                    string CompanyState = string.Empty;
                    string CompanyCountry = string.Empty;
                    string CompanyPhoneNum = string.Empty;
                    string InvcHeadNetWeight = string.Empty;
                    string InvcHeadShortChar01 = string.Empty;
                    string InvcHeadShortChar02 = string.Empty;
                    string InvcHeadShortChar03 = string.Empty;
                    string InvcHeadShortChar04 = string.Empty;
                    string InvcHeadShortChar05 = string.Empty;
                    string InvcHeadLegalNumber = string.Empty;

                    //----- Asignacion de variables
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "Company", 0))
                        InvcHeadCompany = ds.Tables["InvcHead"].Rows[0]["Company"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "NetWeight", 0))
                        InvcHeadNetWeight = ds.Tables["InvcHead"].Rows[0]["NetWeight"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "ShortChar01", 0))
                        InvcHeadShortChar01 = ds.Tables["InvcHead"].Rows[0]["ShortChar01"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "ShortChar02", 0))
                        InvcHeadShortChar02 = ds.Tables["InvcHead"].Rows[0]["ShortChar02"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "ShortChar03", 0))
                        InvcHeadShortChar03 = ds.Tables["InvcHead"].Rows[0]["ShortChar03"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "ShortChar04", 0))
                        InvcHeadShortChar04 = ds.Tables["InvcHead"].Rows[0]["ShortChar04"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "ShortChar05", 0))
                        InvcHeadShortChar05 = ds.Tables["InvcHead"].Rows[0]["ShortChar05"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "LegalNumber", 0))
                        InvcHeadLegalNumber = ds.Tables["InvcHead"].Rows[0]["LegalNumber"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "Company", "Name", 0))
                        CompanyName = ds.Tables["Company"].Rows[0]["Name"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "Company", "StateTaxID", 0))
                        CompanyStateTaxID = ds.Tables["Company"].Rows[0]["StateTaxID"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "Company", "Address1", 0))
                        CompanyAddress1 = ds.Tables["Company"].Rows[0]["Address1"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "Company", "Address2", 0))
                        CompanyAddress2 = ds.Tables["Company"].Rows[0]["Address2"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "Company", "Address3", 0))
                        CompanyAddress3 = ds.Tables["Company"].Rows[0]["Address3"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "Company", "City", 0))
                        CompanyCity = ds.Tables["Company"].Rows[0]["City"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "Company", "State", 0))
                        CompanyState = ds.Tables["Company"].Rows[0]["State"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "Company", "Country", 0))
                        CompanyCountry = ds.Tables["Company"].Rows[0]["Country"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "Company", "PhoneNum", 0))
                        CompanyPhoneNum = ds.Tables["Company"].Rows[0]["PhoneNum"].ToString();
                    CompanyAddress = CompanyAddress1;
                    if (!string.IsNullOrEmpty(CompanyAddress2))
                        CompanyAddress += " " + CompanyAddress2;
                    if (!string.IsNullOrEmpty(CompanyAddress3))
                        CompanyAddress += " " +  CompanyAddress3;
                    //------------------------------

                    PdfPTable table = new PdfPTable(5);
                    table.WidthPercentage = 100;
                    table.SetWidths(new float[] { 1.2f, 1.9f, 0.0f, 1.8f, 1.3f });
                    PdfPCell celLogo = new PdfPCell();

                    iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                    iTextSharp.text.Font fontTitle2 = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                    iTextSharp.text.Font fontCustom = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);

                    Paragraph prgTitle = new Paragraph(CompanyName, fontTitle);

                    Paragraph prgTitle2 = new Paragraph(string.Format("NIT. {0}\n{1}\n{2} {3} {4}\n{5}", CompanyStateTaxID,
                        CompanyAddress, CompanyCity,CompanyState, CompanyCountry,CompanyPhoneNum), fontTitle2);
                    prgTitle.Alignment = Element.ALIGN_CENTER;
                    prgTitle2.Alignment = Element.ALIGN_CENTER;
                    celLogo.AddElement(prgTitle);
                    celLogo.AddElement(prgTitle2);



                    //Paragraph infocabezera = new Paragraph(string.Format("NIT.", CustomerPhoneNum, InvcHeadNetWeight), fontTitle2);
                    Paragraph infocabezera2 = new Paragraph(string.Format(InvcHeadShortChar01), fontTitle2);
                    Paragraph infocabezera3 = new Paragraph(string.Format(InvcHeadShortChar02), fontTitle2);
                    Paragraph infocabezera4 = new Paragraph(string.Format(InvcHeadShortChar03), fontTitle2);
                    Paragraph infocabezera5 = new Paragraph(string.Format(InvcHeadShortChar04), fontTitle2);
                    Paragraph infocabezera6 = new Paragraph(string.Format(InvcHeadShortChar05), fontTitle2);

                    PdfPCell conttibuyentes = new PdfPCell(new Phrase("")) { Border = 0, };

                    //conttibuyentes.AddElement(infocabezera);
                    conttibuyentes.AddElement(infocabezera2);
                    conttibuyentes.AddElement(infocabezera3);
                    conttibuyentes.AddElement(infocabezera4);
                    conttibuyentes.AddElement(infocabezera5);
                    conttibuyentes.AddElement(infocabezera6);

                    System.Drawing.Image logo = null;
                    var requestLogo = WebRequest.Create(RutaImg);
                    using (var responseLogo = requestLogo.GetResponse())
                    using (var streamLogo = responseLogo.GetResponseStream())
                    {
                        logo = Bitmap.FromStream(streamLogo);
                    }
                    iTextSharp.text.Image ImgLogo = iTextSharp.text.Image.GetInstance(logo, BaseColor.WHITE);
                    ImgLogo.ScaleAbsolute(120f, 120f);

                    //divLogo.BackgroundImage = ImgLogo;
                    PdfPCell celLogoE4 = new PdfPCell()
                    {

                        Border = 0,
                        //Colspan =2,        
                        VerticalAlignment = Element.ALIGN_TOP,
                    };
                    celLogoE4.AddElement(ImgLogo);
                    table.AddCell(celLogoE4);

                    iTextSharp.text.Font fontTitleFactura = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                    //-----------------------------------------------------------------------------------------------------
                    iTextSharp.text.Image ImgQR = iTextSharp.text.Image.GetInstance(QR, BaseColor.WHITE);
                    ImgQR.ScaleAbsolute(65f, 65f);
                    ImgQR.Alignment = Element.ALIGN_CENTER;
                    PdfPCell celQR = new PdfPCell();
                    Paragraph prgCufe = new Paragraph($"CUFE: {cufe}", fontTitleFactura);
                    Paragraph prgRepresentacion = new Paragraph($"Representación gráfica factura electrónica", fontTitleFactura);
                    celQR.AddElement(ImgQR);
                    celQR.AddElement(prgCufe);
                    celQR.AddElement(prgRepresentacion);
                    //----------------------------------------------------------------------------------------------------

                    PdfPTable tableFactura = new PdfPTable(3);

                    tableFactura.WidthPercentage = 100;
                    tableFactura.SetWidths(new float[] { 1.0f, 4.0f, 0.5f });

                    iTextSharp.text.pdf.PdfPCell celTittle = new iTextSharp.text.pdf.PdfPCell(new Phrase("FACTURA DE VENTA\n\n", fontTitleFactura));
                    celTittle.Colspan = 3;
                    celTittle.Padding = 3;
                    celTittle.HorizontalAlignment = Element.ALIGN_CENTER;
                    celTittle.VerticalAlignment = Element.ALIGN_TOP;
                    celTittle.Border = 0;
                    celTittle.BackgroundColor = BaseColor.LIGHT_GRAY;
                    tableFactura.AddCell(celTittle);

                    iTextSharp.text.pdf.PdfPCell celNo = new iTextSharp.text.pdf.PdfPCell(new Phrase("No. ", fontTitleFactura));
                    celNo.Colspan = 1;
                    celNo.Padding = 5;
                    celNo.HorizontalAlignment = Element.ALIGN_CENTER;
                    celNo.VerticalAlignment = Element.ALIGN_TOP;
                    celNo.Border = 0;
                    celNo.BackgroundColor = BaseColor.LIGHT_GRAY;
                    tableFactura.AddCell(celNo);


                    string NumLegalFactura = string.Empty;
                    if (InvoiceType == "InvoiceType")
                        NumLegalFactura = InvcHeadLegalNumber;
                    else if (InvoiceType == "CreditNoteType")
                        NumLegalFactura = InvcHeadLegalNumber;
                    else
                        NumLegalFactura = InvcHeadLegalNumber;

                    iTextSharp.text.pdf.PdfPCell celNoFactura = new iTextSharp.text.pdf.PdfPCell(new Phrase(NumLegalFactura, fontTitleFactura));
                    celNoFactura.Colspan = 1;
                    celNoFactura.Padding = 5;
                    celNoFactura.HorizontalAlignment = Element.ALIGN_LEFT;
                    celNoFactura.VerticalAlignment = Element.ALIGN_TOP;
                    celNoFactura.Border = 0;
                    celNoFactura.BackgroundColor = BaseColor.WHITE;
                    tableFactura.AddCell(celNoFactura);


                    iTextSharp.text.pdf.PdfPCell celEspacioNoFactura = new iTextSharp.text.pdf.PdfPCell(new Phrase(" ", fontTitleFactura));
                    celEspacioNoFactura.Colspan = 1;
                    //celNo.Padding = 3;
                    celEspacioNoFactura.HorizontalAlignment = Element.ALIGN_CENTER;
                    celEspacioNoFactura.VerticalAlignment = Element.ALIGN_TOP;
                    celEspacioNoFactura.Border = 0;
                    celEspacioNoFactura.BackgroundColor = BaseColor.LIGHT_GRAY;
                    tableFactura.AddCell(celEspacioNoFactura);

                    iTextSharp.text.pdf.PdfPCell celRellenoNoFactura = new iTextSharp.text.pdf.PdfPCell(new Phrase(" ", fontTitleFactura));
                    celRellenoNoFactura.Colspan = 3;
                    celRellenoNoFactura.Padding = 3;
                    celRellenoNoFactura.BackgroundColor = BaseColor.LIGHT_GRAY;
                    celRellenoNoFactura.HorizontalAlignment = Element.ALIGN_RIGHT;
                    celRellenoNoFactura.VerticalAlignment = Element.ALIGN_TOP;
                    celRellenoNoFactura.Border = 0;
                    tableFactura.AddCell(celRellenoNoFactura);

                    iTextSharp.text.pdf.PdfPCell celConsecutivoInterno = new iTextSharp.text.pdf.PdfPCell(new Phrase($"", fontTitleFactura));
                    celConsecutivoInterno.Colspan = 4;
                    //celConsecutivoInterno.Padding = 3;
                    celConsecutivoInterno.BackgroundColor = BaseColor.WHITE;
                    celConsecutivoInterno.HorizontalAlignment = Element.ALIGN_RIGHT;
                    celConsecutivoInterno.VerticalAlignment = Element.ALIGN_TOP;
                    celConsecutivoInterno.Border = 0;
                    tableFactura.AddCell(celConsecutivoInterno);

                    PdfPCell celNumeroFactura = new PdfPCell();

                    celNumeroFactura.AddElement(tableFactura);
                    //-- Agregando todas las celdas a la tabla maestra--
                    celLogo.Border = 0;
                    celLogo.Padding = 0;
                    //celAcercade.Border = 0;
                    //celAcercade.Padding = 0;
                    celQR.Border = 0;
                    celQR.Padding = 0;
                    celNumeroFactura.Border = 0;
                    celNumeroFactura.PaddingLeft = 5;
                    celNumeroFactura.Padding = 0;

                    table.AddCell(celLogo);
                    //table.AddCell(celAcercade);
                    table.AddCell(conttibuyentes);
                    table.AddCell(celQR);

                    table.AddCell(celNumeroFactura);

                    return table;
                }
                public static PdfPTable DatosCliente(DataSet ds)
                {
                    //--- Variables --------
                    string InvcHeadCustomerName = string.Empty;
                    string InvHeadCharacter02 = string.Empty;
                    string InvHeadCharacter03 = string.Empty;
                    string InvHeadCharacter04 = string.Empty;
                    string InvHeadCharacter05 = string.Empty;
                    string InvHeadCharacter06 = string.Empty;
                    string InvHeadCharacter07 = string.Empty;
                    string CustomerResaleID = string.Empty;
                    string CustomerAddress1 = string.Empty;
                    string CustomerPhoneNum = string.Empty;
                    string CustomerCity = string.Empty;
                    string CustomerCountry = string.Empty;
                    //---- Asignacion de variables -------
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "CustomerName", 0))
                        InvcHeadCustomerName = ds.Tables["InvcHead"].Rows[0]["CustomerName"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "Character02", 0))
                        InvHeadCharacter02 = ds.Tables["InvcHead"].Rows[0]["Character02"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "Character03", 0))
                        InvHeadCharacter03 = ds.Tables["InvcHead"].Rows[0]["Character03"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "Character04", 0))
                        InvHeadCharacter04 = ds.Tables["InvcHead"].Rows[0]["Character04"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "Character05", 0))
                        InvHeadCharacter05 = ds.Tables["InvcHead"].Rows[0]["Character05"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "Character06", 0))
                        InvHeadCharacter06 = ds.Tables["InvcHead"].Rows[0]["Character06"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "Character07", 0))
                        InvHeadCharacter07 = ds.Tables["InvcHead"].Rows[0]["Character07"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "Customer", "ResaleID", 0))
                        CustomerResaleID = ds.Tables["Customer"].Rows[0]["ResaleID"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "Customer", "Address1", 0))
                        CustomerAddress1 = ds.Tables["Customer"].Rows[0]["Address1"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "Customer", "PhoneNum", 0))
                        CustomerPhoneNum = ds.Tables["Customer"].Rows[0]["PhoneNum"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "Customer", "City", 0))
                        CustomerCity = ds.Tables["Customer"].Rows[0]["City"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "Customer", "Country", 0))
                        CustomerCountry = ds.Tables["Customer"].Rows[0]["Country"].ToString();
                    //------------------------------------------------------------------------------------------------
                    PdfPTable tableFacturar = new PdfPTable(3);
                    //Dimenciones.
                    float[] DimencionFacturar = new float[3];
                    DimencionFacturar[0] = 1.0F;//
                    DimencionFacturar[1] = 0.01F;//
                    DimencionFacturar[2] = 1.0F;//

                    tableFacturar.WidthPercentage = 100;
                    tableFacturar.SetWidths(DimencionFacturar);
                    //----------------------------------------------------------------------------------------------
                    PdfPTable tableFacturarA = new PdfPTable(2);
                    float[] DimencionFacturarA = new float[2];
                    DimencionFacturarA[0] = 0.8F;//
                    DimencionFacturarA[1] = 2.0F;//

                    tableFacturarA.WidthPercentage = 100;
                    tableFacturarA.SetWidths(DimencionFacturarA);

                    iTextSharp.text.pdf.PdfPCell celDatosFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase("FACTURAR A:\n", Helpers.Fuentes.Plantilla_2_fontTitleFactura));
                    celDatosFacturarA.Colspan = 2;
                    celDatosFacturarA.Padding = 3;
                    celDatosFacturarA.Border = 0;
                    celDatosFacturarA.BorderColorBottom = BaseColor.WHITE;
                    celDatosFacturarA.HorizontalAlignment = Element.ALIGN_CENTER;
                    celDatosFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                    tableFacturarA.AddCell(celDatosFacturarA);

                    Helpers.Plantilla_2.DatosClienteTituloValor(ref tableFacturarA, "CLIENTE: ", InvcHeadCustomerName);
                    Helpers.Plantilla_2.DatosClienteTituloValor(ref tableFacturarA, "NIT: ", CustomerResaleID);
                    Helpers.Plantilla_2.DatosClienteTituloValor(ref tableFacturarA, "DIRECCION: ", CustomerAddress1);
                    Helpers.Plantilla_2.DatosClienteTituloValor(ref tableFacturarA, "TELEFONO: ", CustomerPhoneNum);
                    Helpers.Plantilla_2.DatosClienteTituloValor(ref tableFacturarA, "CIUDAD: ", CustomerCity);
                    Helpers.Plantilla_2.DatosClienteTituloValor(ref tableFacturarA, "PAIS: ", CustomerCountry);

                    iTextSharp.text.pdf.PdfPCell celTittleFacturarA = new iTextSharp.text.pdf.PdfPCell(tableFacturarA);
                    tableFacturar.AddCell(celTittleFacturarA);
                    //----------------------------------------------------------------------------------------------------------------------------

                    iTextSharp.text.pdf.PdfPCell celEspacio2 = new iTextSharp.text.pdf.PdfPCell(new Phrase(" ", Helpers.Fuentes.Plantilla_2_fontTitleFactura));
                    celEspacio2.Border = 0;
                    tableFacturar.AddCell(celEspacio2);

                    //------------------------------------------------------------------------------------------

                    PdfPTable tableDespacharA = new PdfPTable(2);
                    float[] DimencionDespacharA = new float[2];
                    DimencionDespacharA[0] = 0.8F;//
                    DimencionDespacharA[1] = 2.0F;//

                    tableDespacharA.WidthPercentage = 100;
                    tableDespacharA.SetWidths(DimencionDespacharA);

                    iTextSharp.text.pdf.PdfPCell celTittleDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase("INFORMAIÓN OV:\n", Helpers.Fuentes.Plantilla_2_fontTitleFactura));
                    celTittleDespacharA.Colspan = 2;
                    celTittleDespacharA.Padding = 3;
                    celTittleDespacharA.Border = 0;
                    celTittleDespacharA.BorderColorBottom = BaseColor.WHITE;
                    celTittleDespacharA.HorizontalAlignment = Element.ALIGN_CENTER;
                    celTittleDespacharA.VerticalAlignment = Element.ALIGN_TOP;
                    tableDespacharA.AddCell(celTittleDespacharA);

                    Helpers.Plantilla_2.DatosClienteTituloValor(ref tableDespacharA, "CLIENTE: ", InvHeadCharacter07);
                    Helpers.Plantilla_2.DatosClienteTituloValor(ref tableDespacharA, "NIT: ", InvHeadCharacter06);
                    Helpers.Plantilla_2.DatosClienteTituloValor(ref tableDespacharA, "DIRECCION: ", InvHeadCharacter02);
                    Helpers.Plantilla_2.DatosClienteTituloValor(ref tableDespacharA, "TELEFONO: ", InvHeadCharacter03);
                    Helpers.Plantilla_2.DatosClienteTituloValor(ref tableDespacharA, "CIUDAD: ", InvHeadCharacter04);
                    Helpers.Plantilla_2.DatosClienteTituloValor(ref tableDespacharA, "PAIS: ", InvHeadCharacter05);

                    iTextSharp.text.pdf.PdfPCell celDatosDespacharA = new iTextSharp.text.pdf.PdfPCell(tableDespacharA);
                    tableFacturar.AddCell(celDatosDespacharA);
                    return tableFacturar;
                }
                public static PdfPTable DatosFactura(DataSet ds)
                {
                    return Helpers.Plantilla_2.DatosFactura(ds);
                }
                public static PdfPTable DetalleHead(float min)
                {
                    //return Helpers.Plantilla_2.DetalleHead(min);
                    PdfPTable tableTituloUnidades = new PdfPTable(6);

                    tableTituloUnidades.WidthPercentage = 100;
                    tableTituloUnidades.SetWidths(new float[] { 1.0f, 2.5f, 1.0f, 1.0f, 1.0f, 1.5f });

                    Helpers.Plantilla_2.DetalleHeadTitulos(ref tableTituloUnidades, "CÓDIGO", false);
                    Helpers.Plantilla_2.DetalleHeadTitulos(ref tableTituloUnidades, "DESCRIPCIÓN", false);
                    Helpers.Plantilla_2.DetalleHeadTitulos(ref tableTituloUnidades, "CANTIDAD", false);
                    Helpers.Plantilla_2.DetalleHeadTitulos(ref tableTituloUnidades, "UNIDAD", false);
                    Helpers.Plantilla_2.DetalleHeadTitulos(ref tableTituloUnidades, "VALOR UNITARIO", false);
                    Helpers.Plantilla_2.DetalleHeadTitulos(ref tableTituloUnidades, "VALOR TOTAL", true);
                    //--- agregando las celdas vacias ---------------------
                    Helpers.Plantilla_2.DetalleHeadTituloCel(ref tableTituloUnidades, false, min);
                    Helpers.Plantilla_2.DetalleHeadTituloCel(ref tableTituloUnidades, false, min);
                    Helpers.Plantilla_2.DetalleHeadTituloCel(ref tableTituloUnidades, false, min);
                    Helpers.Plantilla_2.DetalleHeadTituloCel(ref tableTituloUnidades, false, min);
                    Helpers.Plantilla_2.DetalleHeadTituloCel(ref tableTituloUnidades, false, min);
                    Helpers.Plantilla_2.DetalleHeadTituloCel(ref tableTituloUnidades, true, min);

                    return tableTituloUnidades;
                }
                public static PdfPTable PiePagina(DataSet ds)
                {
                    //----- Variables ---------
                    string InvcHeadDspDocInvoiceAmt = "0";
                    string InvcHeadDspDocSubTotal = "0";
                    string InvcHeadNumber01 = "0";
                    string InvcHeadDocTaxAmt = "0";
                    string InvcHeadInvoiceComment = string.Empty;
                    string InvcHeadDocWHTaxAmt = "0";
                    string InvcHeadDocDepositCredit = "0";
                    string InvcHeadShortChar01 = string.Empty;
                    string InvcHeadShortChar02 = string.Empty;
                    string InvcHeadShortChar03 = string.Empty;
                    string InvcHeadShortChar04 = string.Empty;
                    string InvcHeadShortChar05 = string.Empty;
                    //---- Asignacion de valores a las variables ----
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "DspDocInvoiceAmt", 0))
                        InvcHeadDspDocInvoiceAmt = ds.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "DspDocSubTotal", 0))
                        InvcHeadDspDocSubTotal = ds.Tables["InvcHead"].Rows[0]["DspDocSubTotal"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "InvoiceComment", 0))
                        InvcHeadInvoiceComment = ds.Tables["InvcHead"].Rows[0]["InvoiceComment"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "Number01", 0))
                        InvcHeadNumber01 = ds.Tables["InvcHead"].Rows[0]["Number01"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "DocTaxAmt", 0))
                        InvcHeadDocTaxAmt = ds.Tables["InvcHead"].Rows[0]["DocTaxAmt"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "DocWHTaxAmt", 0))
                        InvcHeadDocWHTaxAmt = ds.Tables["InvcHead"].Rows[0]["DocWHTaxAmt"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "ShortChar01", 0))
                        InvcHeadShortChar01 = ds.Tables["InvcHead"].Rows[0]["ShortChar01"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "ShortChar02", 0))
                        InvcHeadShortChar02 = ds.Tables["InvcHead"].Rows[0]["ShortChar02"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "ShortChar03", 0))
                        InvcHeadShortChar03 = ds.Tables["InvcHead"].Rows[0]["ShortChar03"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "ShortChar04", 0))
                        InvcHeadShortChar04 = ds.Tables["InvcHead"].Rows[0]["ShortChar04"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "ShortChar05", 0))
                        InvcHeadShortChar05 = ds.Tables["InvcHead"].Rows[0]["ShortChar05"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "DocDepositCredit", 0))
                        InvcHeadDocDepositCredit = ds.Tables["InvcHead"].Rows[0]["DocDepositCredit"].ToString();
                    //return Helpers.Plantilla_2.PiePagina(ds);

                    PdfPTable auto = new PdfPTable(1);
                    auto.WidthPercentage = 100;
                    Paragraph prgShortChar01 = new Paragraph(InvcHeadShortChar01, Helpers.Fuentes.Plantilla_2_fontTitleFactura);
                    Paragraph prgShortChar02 = new Paragraph(InvcHeadShortChar02, Helpers.Fuentes.Plantilla_2_fontTitleFactura);
                    Paragraph prgShortChar03= new Paragraph(InvcHeadShortChar03, Helpers.Fuentes.Plantilla_2_fontTitleFactura);
                    Paragraph prgShortChar04 = new Paragraph(InvcHeadShortChar04, Helpers.Fuentes.Plantilla_2_fontTitleFactura);
                    Paragraph prgShortChar05 = new Paragraph(InvcHeadShortChar05, Helpers.Fuentes.Plantilla_2_fontTitleFactura);

                    PdfPCell autor = new PdfPCell()
                    {
                        MinimumHeight = 30,
                        Border = 0,
                    };
                    autor.AddElement(prgShortChar01);
                    autor.AddElement(prgShortChar02);
                    autor.AddElement(prgShortChar03);
                    autor.AddElement(prgShortChar04);
                    autor.AddElement(prgShortChar05);
                    auto.AddCell(autor);


                    PdfPTable tabla = new PdfPTable(1);
                    tabla.WidthPercentage = 100;
                    tabla.AddCell(auto);
                    //------**********************************************************************************************************
                    iTextSharp.text.pdf.PdfPCell celEspacio2 = new iTextSharp.text.pdf.PdfPCell(new Phrase(" ", Helpers.Fuentes.Plantilla_2_fontTitleFactura));
                    celEspacio2.Border = 0;
                    PdfPCell celEspacio3 = new PdfPCell(new Phrase("  ", Helpers.Fuentes.Plantilla_2_fontEspacio));
                    celEspacio3.Border = 0;
                    PdfPTable tableTotalesObs = new PdfPTable(3);

                    tableTotalesObs.WidthPercentage = 100;
                    tableTotalesObs.SetWidths(new float[] { 2.5f, 0.01f, 1.0f });

                    PdfPTable _tableObs = new PdfPTable(1);
                    _tableObs.WidthPercentage = 100;

                    iTextSharp.text.pdf.PdfPCell celValorLetras = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("VALORES EN LETRA: {0}", Helpers.Compartido.
                        Nroenletras(InvcHeadDspDocInvoiceAmt)), Helpers.Fuentes.Plantilla_2_fontTitleFactura));
                    celValorLetras.Colspan = 1;
                    celValorLetras.Padding = 3;
                    //celValorLetras.Border = 0;
                    celValorLetras.BorderColorBottom = BaseColor.WHITE;
                    celValorLetras.HorizontalAlignment = Element.ALIGN_LEFT;
                    celValorLetras.VerticalAlignment = Element.ALIGN_TOP;

                    iTextSharp.text.pdf.PdfPCell celValorAsegurado = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("VALOR ASEGURADO: {0:C2}",
                       decimal.Parse(InvcHeadDspDocSubTotal)), Helpers.Fuentes.Plantilla_2_fontTitle2));
                    celValorAsegurado.Colspan = 1;
                    celValorAsegurado.Padding = 5;
                    //celValorLetras.Border = 0;
                    celValorAsegurado.BorderColorBottom = BaseColor.WHITE;
                    celValorAsegurado.HorizontalAlignment = Element.ALIGN_LEFT;
                    celValorAsegurado.VerticalAlignment = Element.ALIGN_TOP;

                    iTextSharp.text.pdf.PdfPCell celObservaciones = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("SE REQUIEREN DATOS BANCARIOS:\n{0}",
                        InvcHeadInvoiceComment), Helpers.Fuentes.Plantilla_2_fontTitleFactura));
                    celObservaciones.Colspan = 1;
                    celObservaciones.Padding = 3;
                    //celValorLetras.Border = 0;
                    celObservaciones.BorderColorBottom = BaseColor.WHITE;
                    celObservaciones.HorizontalAlignment = Element.ALIGN_LEFT;
                    celObservaciones.VerticalAlignment = Element.ALIGN_TOP;

                    _tableObs.AddCell(celValorLetras);
                    _tableObs.AddCell(celEspacio3);
                    _tableObs.AddCell(celValorAsegurado);
                    _tableObs.AddCell(celEspacio3);
                    _tableObs.AddCell(celObservaciones);

                    iTextSharp.text.pdf.PdfPCell _celObs = new iTextSharp.text.pdf.PdfPCell(_tableObs);
                    _celObs.Border = 0;
                    tableTotalesObs.AddCell(_celObs);
                    //----------------------------------------------------------------------------------------------------------------------------

                    iTextSharp.text.pdf.PdfPCell _celEspacio2 = new iTextSharp.text.pdf.PdfPCell(new Phrase(" ", Helpers.Fuentes.Plantilla_2_fontTitleFactura));
                    _celEspacio2.Border = 0;
                    tableTotalesObs.AddCell(_celEspacio2);

                    //------------------------------------------------------------------------------------------

                    PdfPTable _tableTotales = new PdfPTable(2);
                    float[] _DimencionTotales = new float[2];
                    _DimencionTotales[0] = 1.5F;//
                    _DimencionTotales[1] = 2.5F;//

                    _tableTotales.WidthPercentage = 100;
                    _tableTotales.SetWidths(_DimencionTotales);

                    iTextSharp.text.pdf.PdfPCell _celTextSubTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase("SUBTOTAL:", Helpers.Fuentes.Plantilla_2_fontTitleFactura));
                    _celTextSubTotal.Colspan = 1;
                    _celTextSubTotal.Padding = 3;
                    _celTextSubTotal.PaddingBottom = 5;
                    _celTextSubTotal.Border = 0;
                    _celTextSubTotal.BorderWidthRight = 1;
                    _celTextSubTotal.BorderColorBottom = BaseColor.WHITE;
                    _celTextSubTotal.HorizontalAlignment = Element.ALIGN_LEFT;
                    _celTextSubTotal.VerticalAlignment = Element.ALIGN_TOP;
                    _tableTotales.AddCell(_celTextSubTotal);

                    iTextSharp.text.pdf.PdfPCell _celSubTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:C2}",decimal.Parse(InvcHeadDspDocSubTotal)), Helpers.Fuentes.Plantilla_2_fontTitle2));
                    _celSubTotal.Colspan = 1;
                    _celSubTotal.Padding = 3;
                    _celSubTotal.PaddingBottom = 5;
                    _celSubTotal.Border = 0;
                    _celSubTotal.BorderColorBottom = BaseColor.WHITE;
                    _celSubTotal.HorizontalAlignment = Element.ALIGN_RIGHT;
                    _celSubTotal.VerticalAlignment = Element.ALIGN_TOP;
                    _tableTotales.AddCell(_celSubTotal);

                    iTextSharp.text.pdf.PdfPCell _celTextDescuentos = new iTextSharp.text.pdf.PdfPCell(new Phrase("DESCUENTOS:", Helpers.Fuentes.Plantilla_2_fontTitleFactura));
                    _celTextDescuentos.Colspan = 1;
                    _celTextDescuentos.Padding = 3;
                    _celTextDescuentos.PaddingBottom = 5;
                    _celTextDescuentos.Border = 0;
                    _celTextDescuentos.BorderWidthRight = 1;
                    _celTextDescuentos.BorderColorBottom = BaseColor.WHITE;
                    _celTextDescuentos.HorizontalAlignment = Element.ALIGN_LEFT;
                    _celTextDescuentos.VerticalAlignment = Element.ALIGN_TOP;
                    _tableTotales.AddCell(_celTextDescuentos);

                    iTextSharp.text.pdf.PdfPCell _celDescuentos = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:C2}", decimal.Parse(InvcHeadNumber01)), Helpers.Fuentes.Plantilla_2_fontTitle2));
                    _celDescuentos.Colspan = 1;
                    _celDescuentos.Padding = 3;
                    _celDescuentos.PaddingBottom = 5;
                    _celDescuentos.Border = 0;
                    _celDescuentos.BorderColorBottom = BaseColor.WHITE;
                    _celDescuentos.HorizontalAlignment = Element.ALIGN_RIGHT;
                    _celDescuentos.VerticalAlignment = Element.ALIGN_TOP;
                    _tableTotales.AddCell(_celDescuentos);

                    iTextSharp.text.pdf.PdfPCell _celTextSubTotal2 = new iTextSharp.text.pdf.PdfPCell(new Phrase("IMPUESTOS:", Helpers.Fuentes.Plantilla_2_fontTitleFactura));
                    _celTextSubTotal2.Colspan = 1;
                    _celTextSubTotal2.Padding = 3;
                    _celTextSubTotal2.PaddingBottom = 5;
                    _celTextSubTotal2.Border = 0;
                    _celTextSubTotal2.BorderWidthRight = 1;
                    _celTextSubTotal2.BorderColorBottom = BaseColor.WHITE;
                    _celTextSubTotal2.HorizontalAlignment = Element.ALIGN_LEFT;
                    _celTextSubTotal2.VerticalAlignment = Element.ALIGN_TOP;
                    _tableTotales.AddCell(_celTextSubTotal2);

                    iTextSharp.text.pdf.PdfPCell _celSubTotal2 = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:C2}",
                        decimal.Parse(InvcHeadDocTaxAmt)), Helpers.Fuentes.Plantilla_2_fontTitle2));
                    _celSubTotal2.Colspan = 1;
                    _celSubTotal2.Padding = 3;
                    _celSubTotal2.PaddingBottom = 5;
                    _celSubTotal2.Border = 0;
                    _celSubTotal2.BorderColorBottom = BaseColor.WHITE;
                    _celSubTotal2.HorizontalAlignment = Element.ALIGN_RIGHT;
                    _celSubTotal2.VerticalAlignment = Element.ALIGN_TOP;
                    _tableTotales.AddCell(_celSubTotal2);

                    iTextSharp.text.pdf.PdfPCell _celTextIva = new iTextSharp.text.pdf.PdfPCell(new Phrase("DEPÓSITOS:", Helpers.Fuentes.Plantilla_2_fontTitleFactura));
                    _celTextIva.Colspan = 1;
                    _celTextIva.Padding = 3;
                    _celTextIva.PaddingBottom = 5;
                    _celTextIva.Border = 0;
                    _celTextIva.BorderWidthRight = 1;
                    //_celTextIva.BorderColorBottom = BaseColor.BLACK;
                    _celTextIva.HorizontalAlignment = Element.ALIGN_LEFT;
                    _celTextIva.VerticalAlignment = Element.ALIGN_TOP;
                    _tableTotales.AddCell(_celTextIva);

                    iTextSharp.text.pdf.PdfPCell _celIva = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:C2}",
                        decimal.Parse(InvcHeadDocDepositCredit)), Helpers.Fuentes.Plantilla_2_fontTitle2));
                    _celIva.Colspan = 1;
                    _celIva.Padding = 3;
                    _celIva.PaddingBottom = 5;
                    _celIva.Border = 0;
                    //_celIva.BorderColorBottom = BaseColor.BLACK;
                    _celIva.HorizontalAlignment = Element.ALIGN_RIGHT;
                    _celIva.VerticalAlignment = Element.ALIGN_TOP;
                    _tableTotales.AddCell(_celIva);

                    iTextSharp.text.pdf.PdfPCell _celTextTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase("TOTAL:", Helpers.Fuentes.Plantilla_2_fontTitleFactura));
                    _celTextTotal.Colspan = 1;
                    _celTextTotal.Padding = 3;
                    _celTextTotal.PaddingBottom = 5;
                    _celTextTotal.Border = 0;
                    _celTextTotal.BorderWidthRight = 1;
                    _celTextTotal.BorderWidthTop = 1;
                    _celTextTotal.HorizontalAlignment = Element.ALIGN_LEFT;
                    _celTextTotal.VerticalAlignment = Element.ALIGN_TOP;
                    _tableTotales.AddCell(_celTextTotal);

                    iTextSharp.text.pdf.PdfPCell _celTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:C2}",
                        decimal.Parse(InvcHeadDspDocInvoiceAmt)), Helpers.Fuentes.Plantilla_2_fontTitleFactura));
                    _celTotal.Colspan = 1;
                    _celTotal.Padding = 3;
                    _celTotal.PaddingBottom = 5;
                    _celTotal.Border = 0;
                    _celTotal.BorderWidthTop = 1;
                    _celTotal.HorizontalAlignment = Element.ALIGN_RIGHT;
                    _celTotal.VerticalAlignment = Element.ALIGN_TOP;
                    _tableTotales.AddCell(_celTotal);

                    iTextSharp.text.pdf.PdfPCell _celTotales = new iTextSharp.text.pdf.PdfPCell(_tableTotales);
                    tableTotalesObs.AddCell(_celTotales);
                    //------------------------------------------------------------------------
                    PdfPTable tableResolucion = new PdfPTable(1);
                    tableResolucion.WidthPercentage = 100;
                    string strResolucion = "";
                    iTextSharp.text.pdf.PdfPCell _celResolucion = new iTextSharp.text.pdf.PdfPCell(new Phrase(strResolucion, Helpers.Fuentes.Plantilla_2_fontTitleFactura));
                    _celResolucion.Colspan = 1;
                    _celResolucion.Padding = 3;
                    //_celResolucion.Border = 0;
                    _celResolucion.HorizontalAlignment = Element.ALIGN_JUSTIFIED_ALL;
                    _celResolucion.VerticalAlignment = Element.ALIGN_TOP;
                    tableResolucion.AddCell(_celResolucion);
                    //----------------------------------------------------------------------
                    PdfPTable tableFirmas = new PdfPTable(5);

                    float[] DimencionFirmas = new float[5];
                    DimencionFirmas[0] = 1.0F;//
                    DimencionFirmas[1] = 0.02F;//
                    DimencionFirmas[2] = 1.0F;//
                    DimencionFirmas[3] = 0.02F;//
                    DimencionFirmas[4] = 1.4F;//

                    tableFirmas.WidthPercentage = 100;
                    tableFirmas.SetWidths(DimencionFirmas);

                    PdfPTable tableDespacahdoPor = new PdfPTable(1);
                    tableDespacahdoPor.WidthPercentage = 100;
                    iTextSharp.text.pdf.PdfPCell celDespachadoPor = new iTextSharp.text.pdf.PdfPCell(new Phrase("", Helpers.Fuentes.Plantilla_2_fontTitleFactura));
                    celDespachadoPor.Colspan = 1;
                    celDespachadoPor.Padding = 3;
                    celDespachadoPor.Border = 0;
                    celDespachadoPor.HorizontalAlignment = Element.ALIGN_CENTER;
                    celDespachadoPor.VerticalAlignment = Element.ALIGN_TOP;
                    tableDespacahdoPor.AddCell(celDespachadoPor);

                    iTextSharp.text.pdf.PdfPCell _celDespachadoPor = new iTextSharp.text.pdf.PdfPCell(tableDespacahdoPor);
                    tableFirmas.AddCell(_celDespachadoPor);
                    //----------------------------------------------------------------------------------------------------------------------------

                    tableFirmas.AddCell(celEspacio2);

                    //------------------------------------------------------------------------------------------

                    PdfPTable tableFirmaConductor = new PdfPTable(1);
                    tableFirmaConductor.WidthPercentage = 100;
                    iTextSharp.text.pdf.PdfPCell celFirmaConductor = new iTextSharp.text.pdf.PdfPCell(new Phrase("", Helpers.Fuentes.Plantilla_2_fontTitleFactura))
                    { Border = 0 };

                    celFirmaConductor.Colspan = 1;
                    celFirmaConductor.Padding = 3;
                    //celFirmaConductor.Border = 0;
                    celFirmaConductor.HorizontalAlignment = Element.ALIGN_CENTER;
                    celFirmaConductor.VerticalAlignment = Element.ALIGN_TOP;
                    tableFirmaConductor.AddCell(celFirmaConductor);

                    iTextSharp.text.pdf.PdfPCell _celFirmaConductor = new iTextSharp.text.pdf.PdfPCell(tableFirmaConductor) { Border = 0 };
                    tableFirmas.AddCell(_celFirmaConductor);

                    //-------------------------------------------------------------------------------------
                    tableFirmas.AddCell(celEspacio2);
                    //-------------------------------------------------------------------------------------

                    PdfPTable tableSelloCliente = new PdfPTable(1);
                    //tableSelloCliente.WidthPercentage = 100;

                    iTextSharp.text.pdf.PdfPCell cel1SelloCliente = new iTextSharp.text.pdf.PdfPCell(new Phrase(" " +
                        "no es endolsable", Helpers.Fuentes.Plantilla_2_fontTitleFactura));
                    cel1SelloCliente.Colspan = 1;
                    cel1SelloCliente.Padding = 5;
                    cel1SelloCliente.Border = 1;
                    cel1SelloCliente.BorderWidthBottom = 1;
                    cel1SelloCliente.HorizontalAlignment = Element.ALIGN_LEFT;
                    cel1SelloCliente.VerticalAlignment = Element.ALIGN_TOP;
                    tableSelloCliente.AddCell(cel1SelloCliente);

                    iTextSharp.text.pdf.PdfPCell cel2SelloFechaRecibido = new iTextSharp.text.pdf.PdfPCell(new Phrase("Fecha de recibido:", Helpers.Fuentes.Plantilla_2_fontTitleFactura));

                    cel2SelloFechaRecibido.Colspan = 1;
                    cel2SelloFechaRecibido.Padding = 3;
                    cel2SelloFechaRecibido.Border = 0;
                    cel2SelloFechaRecibido.HorizontalAlignment = Element.ALIGN_LEFT;
                    cel2SelloFechaRecibido.VerticalAlignment = Element.ALIGN_TOP;
                    tableSelloCliente.AddCell(cel2SelloFechaRecibido);

                    iTextSharp.text.pdf.PdfPCell cel2SelloNombre = new iTextSharp.text.pdf.PdfPCell(new Phrase("Nombre:", Helpers.Fuentes.Plantilla_2_fontTitleFactura));

                    cel2SelloNombre.Colspan = 1;
                    cel2SelloNombre.Padding = 3;
                    cel2SelloNombre.Border = 0;
                    cel2SelloNombre.HorizontalAlignment = Element.ALIGN_LEFT;
                    cel2SelloNombre.VerticalAlignment = Element.ALIGN_TOP;
                    tableSelloCliente.AddCell(cel2SelloNombre);

                    iTextSharp.text.pdf.PdfPCell cel2SelloId = new iTextSharp.text.pdf.PdfPCell(new Phrase("Identificacion:", Helpers.Fuentes.Plantilla_2_fontTitleFactura));

                    cel2SelloId.Colspan = 1;
                    cel2SelloId.Padding = 3;
                    cel2SelloId.Border = 0;
                    cel2SelloId.HorizontalAlignment = Element.ALIGN_LEFT;
                    cel2SelloId.VerticalAlignment = Element.ALIGN_TOP;
                    tableSelloCliente.AddCell(cel2SelloId);

                    iTextSharp.text.pdf.PdfPCell cel2SelloFirma = new iTextSharp.text.pdf.PdfPCell(new Phrase("Firma:\n", Helpers.Fuentes.Plantilla_2_fontTitleFactura));

                    cel2SelloFirma.Colspan = 1;
                    cel2SelloFirma.Padding = 3;
                    cel2SelloFirma.Border = 0;
                    cel2SelloFirma.HorizontalAlignment = Element.ALIGN_LEFT;
                    cel2SelloFirma.VerticalAlignment = Element.ALIGN_TOP;
                    tableSelloCliente.AddCell(cel2SelloFirma);

                    iTextSharp.text.pdf.PdfPCell cel3SelloCliente = new iTextSharp.text.pdf.PdfPCell(new Phrase("______________________________________________________\n\n" +
                        "RECIBIDO CONFORME FIRMA Y SELLO DEL CLIENTE", Helpers.Fuentes.Plantilla_2_fontTitleFactura));

                    cel3SelloCliente.Colspan = 1;
                    cel3SelloCliente.Padding = 3;
                    cel3SelloCliente.Border = 0;
                    cel3SelloCliente.HorizontalAlignment = Element.ALIGN_CENTER;
                    cel3SelloCliente.VerticalAlignment = Element.ALIGN_TOP;
                    tableSelloCliente.AddCell(cel3SelloCliente);

                    iTextSharp.text.pdf.PdfPCell _celSelloCliente = new iTextSharp.text.pdf.PdfPCell(tableSelloCliente);
                    //_celSelloCliente.Border = 0;
                    //_celSelloCliente.BorderColor = BaseColor.WHITE;
                    tableFirmas.AddCell(_celSelloCliente);
                    //-----------------------------------------------------------------------------
                    PdfPCell celTableTotalesObs = new PdfPCell();
                    PdfPCell celResolucion = new PdfPCell();
                    PdfPCell celTableFirmas = new PdfPCell();
                    celTableTotalesObs.Border = 0;
                    celTableTotalesObs.AddElement(tableTotalesObs);
                    celResolucion.Border = 0;
                    celResolucion.AddElement(tableResolucion);
                    celTableFirmas.Border = 0;
                    celTableFirmas.AddElement(tableFirmas);

                    tabla.AddCell(celTableTotalesObs);
                    tabla.AddCell(celResolucion);
                    tabla.AddCell(celTableFirmas);

                    return tabla;
                }
                public static PdfPTable Detalles(DataSet ds)
                {
                    PdfPTable tableUnidades = new PdfPTable(6);
                    tableUnidades.WidthPercentage = 100;
                    tableUnidades.SetWidths(new float[] { 1.0f, 2.5f, 1.0f, 1.0f, 1.0f, 1.5f });

                    foreach (DataRow InvoiceLine in ds.Tables["InvcDtl"].Rows)
                    {
                        //AddUnidadesFRAL(InvoiceLine, ref tableUnidades, Helpers.Fuentes.Plantilla_2_fontCustom);
                        string PartNum = string.Empty;
                        string SalesUM = string.Empty;
                        string SellingShipQty = "0";
                        if (Helpers.Compartido.VerificarExistenciaColumnas(InvoiceLine, "SalesUM"))
                            SalesUM = InvoiceLine["SalesUM"].ToString();
                        if (Helpers.Compartido.VerificarExistenciaColumnas(InvoiceLine, "PartNum"))
                            SalesUM = InvoiceLine["PartNum"].ToString();
                        if (Helpers.Compartido.VerificarExistenciaColumnas(InvoiceLine, "SellingShipQty"))
                            SellingShipQty = InvoiceLine["SellingShipQty"].ToString();

                        Helpers.Plantilla_2.DetallesCeldas(ref tableUnidades, PartNum, Helpers.Fuentes.Plantilla_2_fontCustom);
                        Helpers.Plantilla_2.DetallesCeldas(ref tableUnidades, InvoiceLine["LineDesc"].ToString(), Helpers.Fuentes.Plantilla_2_fontCustom);
                        Helpers.Plantilla_2.DetallesCeldas(ref tableUnidades, string.Format("{0:N0}",  decimal.Parse(SellingShipQty)), Helpers.Fuentes.Plantilla_2_fontCustom);
                        Helpers.Plantilla_2.DetallesCeldas(ref tableUnidades, SalesUM, Helpers.Fuentes.Plantilla_2_fontCustom);
                        Helpers.Plantilla_2.DetallesCeldas(ref tableUnidades, string.Format("{0:C2}", Convert.ToDecimal(InvoiceLine["UnitPrice"].ToString())), Helpers.Fuentes.Plantilla_2_fontCustom);
                        Helpers.Plantilla_2.DetallesCeldas(ref tableUnidades, string.Format("{0:C2}", decimal.Parse(InvoiceLine["DocExtPrice"].ToString())), Helpers.Fuentes.Plantilla_2_fontCustom);
                    }
                    return tableUnidades;
                }
            }
            public class FacturaExportacion
            {
                public static PdfPTable Encabezado(DataSet ds, System.Drawing.Image QR, string InvoiceType)
                {
                    return Helpers.Plantilla_2.Encabezado(ds, QR, InvoiceType);
                }
                public static PdfPTable DatosCliente(DataSet ds)
                {
                    return Helpers.Plantilla_2.DatosCliente(ds);
                }
                public static PdfPTable DatosFactura(DataSet ds)
                {
                    return Helpers.Plantilla_2.DatosFactura(ds);
                }
                public static PdfPTable DetalleHead(DataSet ds)
                {
                    return Helpers.Plantilla_2.DetalleHead(245);
                }
                public static PdfPTable PiePagina(DataSet ds)
                {
                    return Helpers.Plantilla_2.PiePagina(ds);
                }
                public static PdfPTable Detalles(DataSet ds)
                {
                    return Helpers.Plantilla_2.Detalles(ds);
                }
            }
            public class EventPageUrbanas : PdfPageEventHelper
            {
                PdfContentByte cb;

                // we will put the final number of pages in a template
                PdfTemplate headerTemplate, footerTemplate;

                // this is the BaseFont we are going to use for the header / footer
                BaseFont bf = null;

                // This keeps track of the creation time
                DateTime PrintTime = DateTime.Now;

                private string _header;

                public string Header
                {
                    get { return _header; }
                    set { _header = value; }
                }
                PdfPTable _Encabezado;
                PdfPTable _DatosCliente;
                PdfPTable _DetalleHead;
                PdfPTable _PiePagina;
                PdfPTable _DatosFactura;

                public PdfPTable PiePagina
                {
                    get
                    {
                        return _PiePagina;
                    }
                    set
                    {
                        _PiePagina = value;
                    }
                }
                public PdfPTable Encabezado
                {
                    get
                    {
                        return _Encabezado;
                    }
                    set
                    {
                        _Encabezado = value;
                    }
                }
                public PdfPTable DatosCliente
                {
                    get
                    {
                        return _DatosCliente;
                    }
                    set
                    {
                        _DatosCliente = value;
                    }
                }
                public PdfPTable DatosFactura
                {
                    get
                    {
                        return _DatosFactura;
                    }
                    set
                    {
                        _DatosFactura = value;
                    }
                }
                public PdfPTable DetalleHead
                {
                    get
                    {
                        return _DetalleHead;
                    }
                    set
                    {
                        _DetalleHead = value;
                    }
                }

                public EventPageUrbanas(PdfPTable encabezado, PdfPTable DatosCliente, PdfPTable DatosFactura, PdfPTable detalleHead, PdfPTable piepagina)
                {
                    _Encabezado = encabezado;
                    _DatosCliente = DatosCliente;
                    _DatosFactura = DatosFactura;
                    _DetalleHead = detalleHead;
                    _PiePagina = piepagina;
                }

                public override void OnOpenDocument(PdfWriter writer, Document document)
                {
                    try
                    {
                        PrintTime = DateTime.Now;
                        bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                        cb = writer.DirectContent;
                        headerTemplate = cb.CreateTemplate(100, 100);
                        footerTemplate = cb.CreateTemplate(50, 50);
                    }
                    catch (DocumentException de)
                    {
                    }
                    catch (System.IO.IOException ioe)
                    {
                    }
                }

                public override void OnEndPage(iTextSharp.text.pdf.PdfWriter writer, iTextSharp.text.Document document)
                {
                    base.OnEndPage(writer, document);

                    Encabezado.TotalWidth = document.PageSize.Width - 35f;
                    Encabezado.WriteSelectedRows(0, -1, 20, document.PageSize.Height - 30, writer.DirectContentUnder);

                    _DatosCliente.TotalWidth = document.PageSize.Width - 35f;
                    _DatosCliente.WriteSelectedRows(0, -1, 20, document.PageSize.Height - 120, writer.DirectContentUnder);

                    _DatosFactura.TotalWidth = document.PageSize.Width - 35f;
                    _DatosFactura.WriteSelectedRows(0, -1, 20, document.PageSize.Height - 230, writer.DirectContentUnder);

                    _DetalleHead.TotalWidth = document.PageSize.Width - 35f;
                    _DetalleHead.WriteSelectedRows(0, -1, 20, document.PageSize.Height - 275, writer.DirectContentUnder);

                    _PiePagina.TotalWidth = document.PageSize.Width - 35f;
                    _PiePagina.WriteSelectedRows(0, -1, 20, document.PageSize.Height - 540, writer.DirectContentUnder);
                }

                public override void OnCloseDocument(PdfWriter writer, Document document)
                {
                    base.OnCloseDocument(writer, document);

                    headerTemplate.BeginText();
                    headerTemplate.SetFontAndSize(bf, 12);
                    headerTemplate.SetTextMatrix(0, 0);
                    headerTemplate.ShowText((writer.PageNumber - 1).ToString());
                    headerTemplate.EndText();

                    //footerTemplate.BeginText();
                    //footerTemplate.SetFontAndSize(bf, 12);
                    //footerTemplate.SetTextMatrix(0, 0);
                    //footerTemplate.ShowText((writer.PageNumber - 1).ToString());
                    //footerTemplate.EndText();
                }
            }
        }

        public class Notaurbanas
        {
            public class Notacionalurbanas
            {
                public static PdfPTable Encabezado(DataSet ds, System.Drawing.Image QR, string InvoiceType, string RutaImg)
                {
                    PdfPTable table = new PdfPTable(5);
                    table.WidthPercentage = 100;
                    table.SetWidths(new float[] { 1.2f, 1.2f, 1.5f, 0.6f, 1.3f });
                    PdfPCell celLogo = new PdfPCell();

                    iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                    iTextSharp.text.Font fontTitle2 = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                    iTextSharp.text.Font fontCustom = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);

                    Paragraph prgTitle = new Paragraph(ds.Tables["InvcHead"].Rows[0]["Company"].ToString(), fontTitle);

                    Paragraph prgTitle2 = new Paragraph(string.Format("NIT. {0}\n{1}\n{2} {3} {4}\n{5}", ds.Tables["Customer"].Rows[0]["CustID"].ToString(),
                        ds.Tables["Customer"].Rows[0]["Address1"].ToString(), ds.Tables["Customer"].Rows[0]["City"].ToString(),
                        ds.Tables["Customer"].Rows[0]["State"].ToString(), ds.Tables["Customer"].Rows[0]["Country"].ToString(),
                        ds.Tables["Customer"].Rows[0]["PhoneNum"].ToString()), fontTitle2);
                    prgTitle.Alignment = Element.ALIGN_CENTER;
                    prgTitle2.Alignment = Element.ALIGN_CENTER;
                    celLogo.AddElement(prgTitle);
                    celLogo.AddElement(prgTitle2);



                    Paragraph infocabezera = new Paragraph(string.Format("NIT.", ds.Tables["Customer"].Rows[0]["PhoneNum"].ToString(), ds.Tables["InvcHead"].Rows[0]["NetWeight"].ToString()), fontTitle2);
                    Paragraph infocabezera2 = new Paragraph(string.Format(ds.Tables["InvcHead"].Rows[0]["ShortChar01"].ToString()), fontTitle2);
                    Paragraph infocabezera3 = new Paragraph(string.Format(ds.Tables["InvcHead"].Rows[0]["ShortChar02"].ToString()), fontTitle2);
                    Paragraph infocabezera4 = new Paragraph(string.Format(ds.Tables["InvcHead"].Rows[0]["ShortChar03"].ToString()), fontTitle2);
                    Paragraph infocabezera5 = new Paragraph(string.Format(ds.Tables["InvcHead"].Rows[0]["ShortChar04"].ToString()), fontTitle2);
                    Paragraph infocabezera6 = new Paragraph(string.Format(ds.Tables["InvcHead"].Rows[0]["ShortChar05"].ToString()), fontTitle2);

                    PdfPCell conttibuyentes = new PdfPCell(new Phrase("")) { Border = 0, };

                    conttibuyentes.AddElement(infocabezera);
                    conttibuyentes.AddElement(infocabezera2);
                    conttibuyentes.AddElement(infocabezera3);
                    conttibuyentes.AddElement(infocabezera4);
                    conttibuyentes.AddElement(infocabezera5);
                    conttibuyentes.AddElement(infocabezera6);

                    System.Drawing.Image logo = null;
                    var requestLogo = WebRequest.Create(RutaImg);
                    using (var responseLogo = requestLogo.GetResponse())
                    using (var streamLogo = responseLogo.GetResponseStream())
                    {
                        logo = Bitmap.FromStream(streamLogo);
                    }
                    iTextSharp.text.Image ImgLogo = iTextSharp.text.Image.GetInstance(logo, BaseColor.WHITE);
                    ImgLogo.ScaleAbsolute(120f, 120f);

                    //divLogo.BackgroundImage = ImgLogo;
                    PdfPCell celLogoE4 = new PdfPCell()
                    {

                        Border = 0,
                        //Colspan =2,        
                        VerticalAlignment = Element.ALIGN_TOP,
                    };
                    celLogoE4.AddElement(ImgLogo);


                    table.AddCell(celLogoE4);


                    //-----------------------------------------------------------------------------------------------------
                    iTextSharp.text.Image ImgQR = iTextSharp.text.Image.GetInstance(QR, BaseColor.WHITE);
                    ImgQR.ScaleAbsolute(65f, 65f);
                    ImgQR.Alignment = Element.ALIGN_CENTER;
                    PdfPCell celQR = new PdfPCell();
                    celQR.AddElement(ImgQR);
                    //----------------------------------------------------------------------------------------------------
                    iTextSharp.text.Font fontTitleFactura = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);

                    PdfPTable tableFactura = new PdfPTable(3);

                    tableFactura.WidthPercentage = 100;
                    tableFactura.SetWidths(new float[] { 1.0f, 4.0f, 0.5f });

                    iTextSharp.text.pdf.PdfPCell celTittle = new iTextSharp.text.pdf.PdfPCell(new Phrase("NOTA CREDITO\n\n", fontTitleFactura));
                    celTittle.Colspan = 3;
                    celTittle.Padding = 3;
                    celTittle.HorizontalAlignment = Element.ALIGN_CENTER;
                    celTittle.VerticalAlignment = Element.ALIGN_TOP;
                    celTittle.Border = 0;
                    celTittle.BackgroundColor = BaseColor.LIGHT_GRAY;
                    tableFactura.AddCell(celTittle);

                    iTextSharp.text.pdf.PdfPCell celNo = new iTextSharp.text.pdf.PdfPCell(new Phrase("No. ", fontTitleFactura));
                    celNo.Colspan = 1;
                    celNo.Padding = 5;
                    celNo.HorizontalAlignment = Element.ALIGN_CENTER;
                    celNo.VerticalAlignment = Element.ALIGN_TOP;
                    celNo.Border = 0;
                    celNo.BackgroundColor = BaseColor.LIGHT_GRAY;
                    tableFactura.AddCell(celNo);


                    string NumLegalFactura = string.Empty;
                    if (InvoiceType == "InvoiceType")
                        NumLegalFactura = ds.Tables["InvcHead"].Rows[0]["LegalNumber"].ToString();
                    else if (InvoiceType == "CreditNoteType")
                        NumLegalFactura = ds.Tables["InvcHead"].Rows[0]["LegalNumber"].ToString();
                    else
                        NumLegalFactura = ds.Tables["InvcHead"].Rows[0]["LegalNumber"].ToString();

                    iTextSharp.text.pdf.PdfPCell celNoFactura = new iTextSharp.text.pdf.PdfPCell(new Phrase(NumLegalFactura, fontTitleFactura));
                    celNoFactura.Colspan = 1;
                    celNoFactura.Padding = 5;
                    celNoFactura.HorizontalAlignment = Element.ALIGN_LEFT;
                    celNoFactura.VerticalAlignment = Element.ALIGN_TOP;
                    celNoFactura.Border = 0;
                    celNoFactura.BackgroundColor = BaseColor.WHITE;
                    tableFactura.AddCell(celNoFactura);


                    iTextSharp.text.pdf.PdfPCell celEspacioNoFactura = new iTextSharp.text.pdf.PdfPCell(new Phrase(" ", fontTitleFactura));
                    celEspacioNoFactura.Colspan = 1;
                    //celNo.Padding = 3;
                    celEspacioNoFactura.HorizontalAlignment = Element.ALIGN_CENTER;
                    celEspacioNoFactura.VerticalAlignment = Element.ALIGN_TOP;
                    celEspacioNoFactura.Border = 0;
                    celEspacioNoFactura.BackgroundColor = BaseColor.LIGHT_GRAY;
                    tableFactura.AddCell(celEspacioNoFactura);

                    iTextSharp.text.pdf.PdfPCell celRellenoNoFactura = new iTextSharp.text.pdf.PdfPCell(new Phrase(" ", fontTitleFactura));
                    celRellenoNoFactura.Colspan = 3;
                    celRellenoNoFactura.Padding = 3;
                    celRellenoNoFactura.BackgroundColor = BaseColor.LIGHT_GRAY;
                    celRellenoNoFactura.HorizontalAlignment = Element.ALIGN_RIGHT;
                    celRellenoNoFactura.VerticalAlignment = Element.ALIGN_TOP;
                    celRellenoNoFactura.Border = 0;
                    tableFactura.AddCell(celRellenoNoFactura);

                    iTextSharp.text.pdf.PdfPCell celConsecutivoInterno = new iTextSharp.text.pdf.PdfPCell(new Phrase("CUFE:", fontTitleFactura));
                    celConsecutivoInterno.Colspan = 4;
                    //celConsecutivoInterno.Padding = 3;
                    celConsecutivoInterno.BackgroundColor = BaseColor.WHITE;
                    celConsecutivoInterno.HorizontalAlignment = Element.ALIGN_RIGHT;
                    celConsecutivoInterno.VerticalAlignment = Element.ALIGN_TOP;
                    celConsecutivoInterno.Border = 0;
                    tableFactura.AddCell(celConsecutivoInterno);

                    PdfPCell celNumeroFactura = new PdfPCell();

                    celNumeroFactura.AddElement(tableFactura);
                    //-- Agregando todas las celdas a la tabla maestra--
                    celLogo.Border = 0;
                    celLogo.Padding = 0;
                    //celAcercade.Border = 0;
                    //celAcercade.Padding = 0;
                    celQR.Border = 0;
                    celQR.Padding = 0;
                    celNumeroFactura.Border = 0;
                    celNumeroFactura.PaddingLeft = 5;
                    celNumeroFactura.Padding = 0;

                    table.AddCell(celLogo);
                    //table.AddCell(celAcercade);
                    table.AddCell(conttibuyentes);
                    table.AddCell(celQR);

                    table.AddCell(celNumeroFactura);

                    return table;
                }
                public static PdfPTable DatosCliente(DataSet ds)
                {
                    //------------------------------------------------------------------------------------------------
                    PdfPTable tableFacturar = new PdfPTable(3);
                    //Dimenciones.
                    float[] DimencionFacturar = new float[3];
                    DimencionFacturar[0] = 1.0F;//
                    DimencionFacturar[1] = 0.01F;//
                    DimencionFacturar[2] = 1.0F;//

                    tableFacturar.WidthPercentage = 100;
                    tableFacturar.SetWidths(DimencionFacturar);
                    //----------------------------------------------------------------------------------------------
                    PdfPTable tableFacturarA = new PdfPTable(2);
                    float[] DimencionFacturarA = new float[2];
                    DimencionFacturarA[0] = 0.8F;//
                    DimencionFacturarA[1] = 2.0F;//

                    tableFacturarA.WidthPercentage = 100;
                    tableFacturarA.SetWidths(DimencionFacturarA);

                    iTextSharp.text.pdf.PdfPCell celDatosFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase("FACTURAR A:\n", Helpers.Fuentes.Plantilla_2_fontTitleFactura));
                    celDatosFacturarA.Colspan = 2;
                    celDatosFacturarA.Padding = 3;
                    celDatosFacturarA.Border = 0;
                    celDatosFacturarA.BorderColorBottom = BaseColor.WHITE;
                    celDatosFacturarA.HorizontalAlignment = Element.ALIGN_CENTER;
                    celDatosFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                    tableFacturarA.AddCell(celDatosFacturarA);

                    Helpers.Plantilla_2.DatosClienteTituloValor(ref tableFacturarA, "CLIENTE: ", ds.Tables["InvcHead"].Rows[0]["CustomerName"].ToString());
                    Helpers.Plantilla_2.DatosClienteTituloValor(ref tableFacturarA, "NIT: ", ds.Tables["Customer"].Rows[0]["ResaleID"].ToString());
                    Helpers.Plantilla_2.DatosClienteTituloValor(ref tableFacturarA, "DIRECCION: ", ds.Tables["Customer"].Rows[0]["Address1"].ToString());
                    Helpers.Plantilla_2.DatosClienteTituloValor(ref tableFacturarA, "TELEFONO: ", ds.Tables["Customer"].Rows[0]["PhoneNum"].ToString());
                    Helpers.Plantilla_2.DatosClienteTituloValor(ref tableFacturarA, "CIUDAD: ", ds.Tables["Customer"].Rows[0]["City"].ToString());
                    Helpers.Plantilla_2.DatosClienteTituloValor(ref tableFacturarA, "PAIS: ", ds.Tables["Customer"].Rows[0]["Country"].ToString());

                    iTextSharp.text.pdf.PdfPCell celTittleFacturarA = new iTextSharp.text.pdf.PdfPCell(tableFacturarA);
                    tableFacturar.AddCell(celTittleFacturarA);
                    //----------------------------------------------------------------------------------------------------------------------------

                    iTextSharp.text.pdf.PdfPCell celEspacio2 = new iTextSharp.text.pdf.PdfPCell(new Phrase(" ", Helpers.Fuentes.Plantilla_2_fontTitleFactura));
                    celEspacio2.Border = 0;
                    tableFacturar.AddCell(celEspacio2);

                    //------------------------------------------------------------------------------------------

                    PdfPTable tableDespacharA = new PdfPTable(2);
                    float[] DimencionDespacharA = new float[2];
                    DimencionDespacharA[0] = 0.8F;//
                    DimencionDespacharA[1] = 2.0F;//

                    tableDespacharA.WidthPercentage = 100;
                    tableDespacharA.SetWidths(DimencionDespacharA);

                    iTextSharp.text.pdf.PdfPCell celTittleDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase("INFORMAIÓN OV:\n", Helpers.Fuentes.Plantilla_2_fontTitleFactura));
                    celTittleDespacharA.Colspan = 2;
                    celTittleDespacharA.Padding = 3;
                    celTittleDespacharA.Border = 0;
                    celTittleDespacharA.BorderColorBottom = BaseColor.WHITE;
                    celTittleDespacharA.HorizontalAlignment = Element.ALIGN_CENTER;
                    celTittleDespacharA.VerticalAlignment = Element.ALIGN_TOP;
                    tableDespacharA.AddCell(celTittleDespacharA);

                    Helpers.Plantilla_2.DatosClienteTituloValor(ref tableDespacharA, "CLIENTE: ", ds.Tables["InvcHead"].Rows[0]["Character07"].ToString());
                    Helpers.Plantilla_2.DatosClienteTituloValor(ref tableDespacharA, "NIT: ", ds.Tables["InvcHead"].Rows[0]["Character06"].ToString());
                    Helpers.Plantilla_2.DatosClienteTituloValor(ref tableDespacharA, "DIRECCION: ", ds.Tables["InvcHead"].Rows[0]["Character02"].ToString());
                    Helpers.Plantilla_2.DatosClienteTituloValor(ref tableDespacharA, "TELEFONO: ", ds.Tables["InvcHead"].Rows[0]["Character03"].ToString());
                    Helpers.Plantilla_2.DatosClienteTituloValor(ref tableDespacharA, "CIUDAD: ", ds.Tables["InvcHead"].Rows[0]["Character04"].ToString());
                    Helpers.Plantilla_2.DatosClienteTituloValor(ref tableDespacharA, "PAIS: ", ds.Tables["InvcHead"].Rows[0]["Character05"].ToString());

                    iTextSharp.text.pdf.PdfPCell celDatosDespacharA = new iTextSharp.text.pdf.PdfPCell(tableDespacharA);
                    tableFacturar.AddCell(celDatosDespacharA);
                    return tableFacturar;
                }
                public static PdfPTable DatosFactura(DataSet ds)
                {
                    return Helpers.Plantilla_2.DatosFactura(ds);
                }
                public static PdfPTable DetalleHead(float min)
                {
                    return Helpers.Plantilla_2.DetalleHead(min);

                    //PdfPTable tableTituloUnidades = new PdfPTable(6);

                    //tableTituloUnidades.WidthPercentage = 100;
                    //tableTituloUnidades.SetWidths(new float[] { 1.0f, 3.0f, 1.0f, 0.5f, 1.0f, 1.5f });

                    //Helpers.Plantilla_2.DetalleHeadTitulos(ref tableTituloUnidades, "CÓDIGO", false);
                    //Helpers.Plantilla_2.DetalleHeadTitulos(ref tableTituloUnidades, "DESCRIPCIÓN", false);
                    //Helpers.Plantilla_2.DetalleHeadTitulos(ref tableTituloUnidades, "CANTIDAD", false);
                    //Helpers.Plantilla_2.DetalleHeadTitulos(ref tableTituloUnidades, "UNIDAD", false);
                    //Helpers.Plantilla_2.DetalleHeadTitulos(ref tableTituloUnidades, "VALOR UNITARIO", false);
                    //Helpers.Plantilla_2.DetalleHeadTitulos(ref tableTituloUnidades, "VALOR TOTAL", true);
                    ////--- agregando las celdas vacias ---------------------
                    //Helpers.Plantilla_2.DetalleHeadTituloCel(ref tableTituloUnidades, false, MinimumHeight);
                    //Helpers.Plantilla_2.DetalleHeadTituloCel(ref tableTituloUnidades, false, MinimumHeight);
                    //Helpers.Plantilla_2.DetalleHeadTituloCel(ref tableTituloUnidades, false, MinimumHeight);
                    //Helpers.Plantilla_2.DetalleHeadTituloCel(ref tableTituloUnidades, false, MinimumHeight);
                    //Helpers.Plantilla_2.DetalleHeadTituloCel(ref tableTituloUnidades, false, MinimumHeight);
                    //Helpers.Plantilla_2.DetalleHeadTituloCel(ref tableTituloUnidades, true, MinimumHeight);



                    //return tableTituloUnidades;

                }
                public static PdfPTable PiePagina(DataSet ds)
                {
                    //return Helpers.Plantilla_2.PiePagina(ds);

                    PdfPTable auto = new PdfPTable(1);
                    auto.WidthPercentage = 100;

                    PdfPCell autor = new PdfPCell(new Phrase("INFORMACIÓN AUTORRENTA:", Helpers.Fuentes.Plantilla_2_fontTitleFactura)) { MinimumHeight = 30, Border = 0, };
                    auto.AddCell(autor);

                    PdfPTable tabla = new PdfPTable(1);
                    tabla.WidthPercentage = 100;
                    tabla.AddCell(auto);
                    //------**********************************************************************************************************
                    iTextSharp.text.pdf.PdfPCell celEspacio2 = new iTextSharp.text.pdf.PdfPCell(new Phrase(" ", Helpers.Fuentes.Plantilla_2_fontTitleFactura));
                    celEspacio2.Border = 0;
                    PdfPCell celEspacio3 = new PdfPCell(new Phrase("  ", Helpers.Fuentes.Plantilla_2_fontEspacio));
                    celEspacio3.Border = 0;
                    PdfPTable tableTotalesObs = new PdfPTable(3);

                    tableTotalesObs.WidthPercentage = 100;
                    tableTotalesObs.SetWidths(new float[] { 2.5f, 0.01f, 1.0f });

                    PdfPTable _tableObs = new PdfPTable(1);
                    _tableObs.WidthPercentage = 100;

                    iTextSharp.text.pdf.PdfPCell celValorLetras = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("VALORES EN LETRA: {0}", Helpers.Compartido.
                        Nroenletras(ds.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"].ToString())), Helpers.Fuentes.Plantilla_2_fontTitleFactura));
                    celValorLetras.Colspan = 1;
                    celValorLetras.Padding = 3;
                    //celValorLetras.Border = 0;
                    celValorLetras.BorderColorBottom = BaseColor.WHITE;
                    celValorLetras.HorizontalAlignment = Element.ALIGN_LEFT;
                    celValorLetras.VerticalAlignment = Element.ALIGN_TOP;

                    iTextSharp.text.pdf.PdfPCell celValorAsegurado = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("VALOR ASEGURADO: {0:C2}",
                       decimal.Parse(ds.Tables["InvcHead"].Rows[0]["DspDocSubTotal"].ToString())), Helpers.Fuentes.Plantilla_2_fontTitle2));
                    celValorAsegurado.Colspan = 1;
                    celValorAsegurado.Padding = 5;
                    //celValorLetras.Border = 0;
                    celValorAsegurado.BorderColorBottom = BaseColor.WHITE;
                    celValorAsegurado.HorizontalAlignment = Element.ALIGN_LEFT;
                    celValorAsegurado.VerticalAlignment = Element.ALIGN_TOP;

                    iTextSharp.text.pdf.PdfPCell celObservaciones = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("OBSERVACIONES:\n{0}",
                        ds.Tables["InvcHead"].Rows[0]["InvoiceComment"].ToString()), Helpers.Fuentes.Plantilla_2_fontTitleFactura));
                    celObservaciones.Colspan = 1;
                    celObservaciones.Padding = 3;
                    //celValorLetras.Border = 0;
                    celObservaciones.BorderColorBottom = BaseColor.WHITE;
                    celObservaciones.HorizontalAlignment = Element.ALIGN_LEFT;
                    celObservaciones.VerticalAlignment = Element.ALIGN_TOP;

                    _tableObs.AddCell(celValorLetras);
                    _tableObs.AddCell(celEspacio3);
                    _tableObs.AddCell(celValorAsegurado);
                    _tableObs.AddCell(celEspacio3);
                    _tableObs.AddCell(celObservaciones);

                    iTextSharp.text.pdf.PdfPCell _celObs = new iTextSharp.text.pdf.PdfPCell(_tableObs);
                    _celObs.Border = 0;
                    tableTotalesObs.AddCell(_celObs);
                    //----------------------------------------------------------------------------------------------------------------------------

                    iTextSharp.text.pdf.PdfPCell _celEspacio2 = new iTextSharp.text.pdf.PdfPCell(new Phrase(" ", Helpers.Fuentes.Plantilla_2_fontTitleFactura));
                    _celEspacio2.Border = 0;
                    tableTotalesObs.AddCell(_celEspacio2);

                    //------------------------------------------------------------------------------------------

                    PdfPTable _tableTotales = new PdfPTable(2);
                    float[] _DimencionTotales = new float[2];
                    _DimencionTotales[0] = 1.5F;//
                    _DimencionTotales[1] = 2.5F;//

                    _tableTotales.WidthPercentage = 100;
                    _tableTotales.SetWidths(_DimencionTotales);

                    iTextSharp.text.pdf.PdfPCell _celTextSubTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase("SUBTOTAL:", Helpers.Fuentes.Plantilla_2_fontTitleFactura));
                    _celTextSubTotal.Colspan = 1;
                    _celTextSubTotal.Padding = 3;
                    _celTextSubTotal.PaddingBottom = 5;
                    _celTextSubTotal.Border = 0;
                    _celTextSubTotal.BorderWidthRight = 1;
                    _celTextSubTotal.BorderColorBottom = BaseColor.WHITE;
                    _celTextSubTotal.HorizontalAlignment = Element.ALIGN_LEFT;
                    _celTextSubTotal.VerticalAlignment = Element.ALIGN_TOP;
                    _tableTotales.AddCell(_celTextSubTotal);

                    iTextSharp.text.pdf.PdfPCell _celSubTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:C2}",
                        decimal.Parse(ds.Tables["InvcHead"].Rows[0]["DspDocSubTotal"].ToString())), Helpers.Fuentes.Plantilla_2_fontTitle2));
                    _celSubTotal.Colspan = 1;
                    _celSubTotal.Padding = 3;
                    _celSubTotal.PaddingBottom = 5;
                    _celSubTotal.Border = 0;
                    _celSubTotal.BorderColorBottom = BaseColor.WHITE;
                    _celSubTotal.HorizontalAlignment = Element.ALIGN_RIGHT;
                    _celSubTotal.VerticalAlignment = Element.ALIGN_TOP;
                    _tableTotales.AddCell(_celSubTotal);

                    iTextSharp.text.pdf.PdfPCell _celTextDescuentos = new iTextSharp.text.pdf.PdfPCell(new Phrase("DESCUENTOS:", Helpers.Fuentes.Plantilla_2_fontTitleFactura));
                    _celTextDescuentos.Colspan = 1;
                    _celTextDescuentos.Padding = 3;
                    _celTextDescuentos.PaddingBottom = 5;
                    _celTextDescuentos.Border = 0;
                    _celTextDescuentos.BorderWidthRight = 1;
                    _celTextDescuentos.BorderColorBottom = BaseColor.WHITE;
                    _celTextDescuentos.HorizontalAlignment = Element.ALIGN_LEFT;
                    _celTextDescuentos.VerticalAlignment = Element.ALIGN_TOP;
                    _tableTotales.AddCell(_celTextDescuentos);

                    iTextSharp.text.pdf.PdfPCell _celDescuentos = new iTextSharp.text.pdf.PdfPCell(new Phrase("xxxxxxxxxx", Helpers.Fuentes.Plantilla_2_fontTitle2));
                    _celDescuentos.Colspan = 1;
                    _celDescuentos.Padding = 3;
                    _celDescuentos.PaddingBottom = 5;
                    _celDescuentos.Border = 0;
                    _celDescuentos.BorderColorBottom = BaseColor.WHITE;
                    _celDescuentos.HorizontalAlignment = Element.ALIGN_RIGHT;
                    _celDescuentos.VerticalAlignment = Element.ALIGN_TOP;
                    _tableTotales.AddCell(_celDescuentos);

                    iTextSharp.text.pdf.PdfPCell _celTextSubTotal2 = new iTextSharp.text.pdf.PdfPCell(new Phrase("SUBTOTAL2:", Helpers.Fuentes.Plantilla_2_fontTitleFactura));
                    _celTextSubTotal2.Colspan = 1;
                    _celTextSubTotal2.Padding = 3;
                    _celTextSubTotal2.PaddingBottom = 5;
                    _celTextSubTotal2.Border = 0;
                    _celTextSubTotal2.BorderWidthRight = 1;
                    _celTextSubTotal2.BorderColorBottom = BaseColor.WHITE;
                    _celTextSubTotal2.HorizontalAlignment = Element.ALIGN_LEFT;
                    _celTextSubTotal2.VerticalAlignment = Element.ALIGN_TOP;
                    _tableTotales.AddCell(_celTextSubTotal2);

                    iTextSharp.text.pdf.PdfPCell _celSubTotal2 = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:C2}",
                        decimal.Parse(ds.Tables["InvcHead"].Rows[0]["DocTaxAmt"].ToString())), Helpers.Fuentes.Plantilla_2_fontTitle2));
                    _celSubTotal2.Colspan = 1;
                    _celSubTotal2.Padding = 3;
                    _celSubTotal2.PaddingBottom = 5;
                    _celSubTotal2.Border = 0;
                    _celSubTotal2.BorderColorBottom = BaseColor.WHITE;
                    _celSubTotal2.HorizontalAlignment = Element.ALIGN_RIGHT;
                    _celSubTotal2.VerticalAlignment = Element.ALIGN_TOP;
                    _tableTotales.AddCell(_celSubTotal2);

                    iTextSharp.text.pdf.PdfPCell _celTextIva = new iTextSharp.text.pdf.PdfPCell(new Phrase("IVA:", Helpers.Fuentes.Plantilla_2_fontTitleFactura));
                    _celTextIva.Colspan = 1;
                    _celTextIva.Padding = 3;
                    _celTextIva.PaddingBottom = 5;
                    _celTextIva.Border = 0;
                    _celTextIva.BorderWidthRight = 1;
                    //_celTextIva.BorderColorBottom = BaseColor.BLACK;
                    _celTextIva.HorizontalAlignment = Element.ALIGN_LEFT;
                    _celTextIva.VerticalAlignment = Element.ALIGN_TOP;
                    _tableTotales.AddCell(_celTextIva);

                    iTextSharp.text.pdf.PdfPCell _celIva = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:C2}",
                        decimal.Parse(ds.Tables["InvcHead"].Rows[0]["DocWHTaxAmt"].ToString())), Helpers.Fuentes.Plantilla_2_fontTitle2));
                    _celIva.Colspan = 1;
                    _celIva.Padding = 3;
                    _celIva.PaddingBottom = 5;
                    _celIva.Border = 0;
                    //_celIva.BorderColorBottom = BaseColor.BLACK;
                    _celIva.HorizontalAlignment = Element.ALIGN_RIGHT;
                    _celIva.VerticalAlignment = Element.ALIGN_TOP;
                    _tableTotales.AddCell(_celIva);

                    iTextSharp.text.pdf.PdfPCell _celTextTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase("TOTAL:", Helpers.Fuentes.Plantilla_2_fontTitleFactura));
                    _celTextTotal.Colspan = 1;
                    _celTextTotal.Padding = 3;
                    _celTextTotal.PaddingBottom = 5;
                    _celTextTotal.Border = 0;
                    _celTextTotal.BorderWidthRight = 1;
                    _celTextTotal.BorderWidthTop = 1;
                    _celTextTotal.HorizontalAlignment = Element.ALIGN_LEFT;
                    _celTextTotal.VerticalAlignment = Element.ALIGN_TOP;
                    _tableTotales.AddCell(_celTextTotal);

                    iTextSharp.text.pdf.PdfPCell _celTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:C2}",
                        decimal.Parse(ds.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"].ToString())), Helpers.Fuentes.Plantilla_2_fontTitleFactura));
                    _celTotal.Colspan = 1;
                    _celTotal.Padding = 3;
                    _celTotal.PaddingBottom = 5;
                    _celTotal.Border = 0;
                    _celTotal.BorderWidthTop = 1;
                    _celTotal.HorizontalAlignment = Element.ALIGN_RIGHT;
                    _celTotal.VerticalAlignment = Element.ALIGN_TOP;
                    _tableTotales.AddCell(_celTotal);

                    iTextSharp.text.pdf.PdfPCell _celTotales = new iTextSharp.text.pdf.PdfPCell(_tableTotales);
                    tableTotalesObs.AddCell(_celTotales);
                    //------------------------------------------------------------------------
                    PdfPTable tableResolucion = new PdfPTable(1);
                    tableResolucion.WidthPercentage = 100;
                    string strResolucion = "";
                    iTextSharp.text.pdf.PdfPCell _celResolucion = new iTextSharp.text.pdf.PdfPCell(new Phrase(strResolucion, Helpers.Fuentes.Plantilla_2_fontTitleFactura));
                    _celResolucion.Colspan = 1;
                    _celResolucion.Padding = 3;
                    //_celResolucion.Border = 0;
                    _celResolucion.HorizontalAlignment = Element.ALIGN_JUSTIFIED_ALL;
                    _celResolucion.VerticalAlignment = Element.ALIGN_TOP;
                    tableResolucion.AddCell(_celResolucion);
                    //----------------------------------------------------------------------
                    PdfPTable tableFirmas = new PdfPTable(5);

                    float[] DimencionFirmas = new float[5];
                    DimencionFirmas[0] = 1.0F;//
                    DimencionFirmas[1] = 0.02F;//
                    DimencionFirmas[2] = 1.0F;//
                    DimencionFirmas[3] = 0.02F;//
                    DimencionFirmas[4] = 1.4F;//

                    tableFirmas.WidthPercentage = 100;
                    tableFirmas.SetWidths(DimencionFirmas);

                    PdfPTable tableDespacahdoPor = new PdfPTable(1);
                    tableDespacahdoPor.WidthPercentage = 100;
                    iTextSharp.text.pdf.PdfPCell celDespachadoPor = new iTextSharp.text.pdf.PdfPCell(new Phrase("\n\n\n\n\n\n\n\n Urbanas NIT.890.200.877-1 usuario que genera la factura:" +
                        "\n______________________________________\n\nELABOIRADO POR", Helpers.Fuentes.Plantilla_2_fontTitleFactura));
                    celDespachadoPor.Colspan = 1;
                    celDespachadoPor.Padding = 3;
                    celDespachadoPor.HorizontalAlignment = Element.ALIGN_CENTER;
                    celDespachadoPor.VerticalAlignment = Element.ALIGN_TOP;
                    tableDespacahdoPor.AddCell(celDespachadoPor);

                    iTextSharp.text.pdf.PdfPCell _celDespachadoPor = new iTextSharp.text.pdf.PdfPCell(tableDespacahdoPor);
                    tableFirmas.AddCell(_celDespachadoPor);
                    //----------------------------------------------------------------------------------------------------------------------------

                    tableFirmas.AddCell(celEspacio2);

                    //------------------------------------------------------------------------------------------

                    PdfPTable tableFirmaConductor = new PdfPTable(1);
                    tableFirmaConductor.WidthPercentage = 100;
                    iTextSharp.text.pdf.PdfPCell celFirmaConductor = new iTextSharp.text.pdf.PdfPCell(new Phrase("", Helpers.Fuentes.Plantilla_2_fontTitleFactura))
                    { Border = 0 };

                    celFirmaConductor.Colspan = 1;
                    celFirmaConductor.Padding = 3;
                    //celFirmaConductor.Border = 0;
                    celFirmaConductor.HorizontalAlignment = Element.ALIGN_CENTER;
                    celFirmaConductor.VerticalAlignment = Element.ALIGN_TOP;
                    tableFirmaConductor.AddCell(celFirmaConductor);

                    iTextSharp.text.pdf.PdfPCell _celFirmaConductor = new iTextSharp.text.pdf.PdfPCell(tableFirmaConductor) { Border = 0 };
                    tableFirmas.AddCell(_celFirmaConductor);

                    //-------------------------------------------------------------------------------------
                    tableFirmas.AddCell(celEspacio2);
                    //-------------------------------------------------------------------------------------

                    PdfPTable tableSelloCliente = new PdfPTable(1);
                    //tableSelloCliente.WidthPercentage = 100;

                    iTextSharp.text.pdf.PdfPCell cel1SelloCliente = new iTextSharp.text.pdf.PdfPCell(new Phrase(" " +
                        "no es endolsable", Helpers.Fuentes.Plantilla_2_fontTitleFactura));
                    cel1SelloCliente.Colspan = 1;
                    cel1SelloCliente.Padding = 5;
                    cel1SelloCliente.Border = 1;
                    cel1SelloCliente.BorderWidthBottom = 1;
                    cel1SelloCliente.HorizontalAlignment = Element.ALIGN_LEFT;
                    cel1SelloCliente.VerticalAlignment = Element.ALIGN_TOP;
                    tableSelloCliente.AddCell(cel1SelloCliente);

                    iTextSharp.text.pdf.PdfPCell cel2SelloFechaRecibido = new iTextSharp.text.pdf.PdfPCell(new Phrase("Fecha de recibido:", Helpers.Fuentes.Plantilla_2_fontTitleFactura));

                    cel2SelloFechaRecibido.Colspan = 1;
                    cel2SelloFechaRecibido.Padding = 3;
                    cel2SelloFechaRecibido.Border = 0;
                    cel2SelloFechaRecibido.HorizontalAlignment = Element.ALIGN_LEFT;
                    cel2SelloFechaRecibido.VerticalAlignment = Element.ALIGN_TOP;
                    tableSelloCliente.AddCell(cel2SelloFechaRecibido);

                    iTextSharp.text.pdf.PdfPCell cel2SelloNombre = new iTextSharp.text.pdf.PdfPCell(new Phrase("Nombre:", Helpers.Fuentes.Plantilla_2_fontTitleFactura));

                    cel2SelloNombre.Colspan = 1;
                    cel2SelloNombre.Padding = 3;
                    cel2SelloNombre.Border = 0;
                    cel2SelloNombre.HorizontalAlignment = Element.ALIGN_LEFT;
                    cel2SelloNombre.VerticalAlignment = Element.ALIGN_TOP;
                    tableSelloCliente.AddCell(cel2SelloNombre);

                    iTextSharp.text.pdf.PdfPCell cel2SelloId = new iTextSharp.text.pdf.PdfPCell(new Phrase("Identificacion:", Helpers.Fuentes.Plantilla_2_fontTitleFactura));

                    cel2SelloId.Colspan = 1;
                    cel2SelloId.Padding = 3;
                    cel2SelloId.Border = 0;
                    cel2SelloId.HorizontalAlignment = Element.ALIGN_LEFT;
                    cel2SelloId.VerticalAlignment = Element.ALIGN_TOP;
                    tableSelloCliente.AddCell(cel2SelloId);

                    iTextSharp.text.pdf.PdfPCell cel2SelloFirma = new iTextSharp.text.pdf.PdfPCell(new Phrase("Firma:\n", Helpers.Fuentes.Plantilla_2_fontTitleFactura));

                    cel2SelloFirma.Colspan = 1;
                    cel2SelloFirma.Padding = 3;
                    cel2SelloFirma.Border = 0;
                    cel2SelloFirma.HorizontalAlignment = Element.ALIGN_LEFT;
                    cel2SelloFirma.VerticalAlignment = Element.ALIGN_TOP;
                    tableSelloCliente.AddCell(cel2SelloFirma);

                    iTextSharp.text.pdf.PdfPCell cel3SelloCliente = new iTextSharp.text.pdf.PdfPCell(new Phrase("______________________________________________________\n\n" +
                        "RECIBIDO CONFORME FIRMA Y SELLO DEL CLIENTE", Helpers.Fuentes.Plantilla_2_fontTitleFactura));

                    cel3SelloCliente.Colspan = 1;
                    cel3SelloCliente.Padding = 3;
                    cel3SelloCliente.Border = 0;
                    cel3SelloCliente.HorizontalAlignment = Element.ALIGN_CENTER;
                    cel3SelloCliente.VerticalAlignment = Element.ALIGN_TOP;
                    tableSelloCliente.AddCell(cel3SelloCliente);

                    iTextSharp.text.pdf.PdfPCell _celSelloCliente = new iTextSharp.text.pdf.PdfPCell(tableSelloCliente);
                    //_celSelloCliente.Border = 0;
                    //_celSelloCliente.BorderColor = BaseColor.WHITE;
                    tableFirmas.AddCell(_celSelloCliente);
                    //-----------------------------------------------------------------------------
                    PdfPCell celTableTotalesObs = new PdfPCell();
                    PdfPCell celResolucion = new PdfPCell();
                    PdfPCell celTableFirmas = new PdfPCell();
                    celTableTotalesObs.Border = 0;
                    celTableTotalesObs.AddElement(tableTotalesObs);
                    celResolucion.Border = 0;
                    celResolucion.AddElement(tableResolucion);
                    celTableFirmas.Border = 0;
                    celTableFirmas.AddElement(tableFirmas);

                    tabla.AddCell(celTableTotalesObs);
                    tabla.AddCell(celResolucion);
                    tabla.AddCell(celTableFirmas);

                    return tabla;
                }
                public static PdfPTable Detalles(DataSet ds)
                {
                    return Helpers.Plantilla_2.Detalles(ds);

                }
            }
            public class FacturaExportacion
            {
                public static PdfPTable Encabezado(DataSet ds, System.Drawing.Image QR, string InvoiceType)
                {
                    return Helpers.Plantilla_2.Encabezado(ds, QR, InvoiceType);
                }
                public static PdfPTable DatosCliente(DataSet ds)
                {
                    return Helpers.Plantilla_2.DatosCliente(ds);
                }
                public static PdfPTable DatosFactura(DataSet ds)
                {
                    return Helpers.Plantilla_2.DatosFactura(ds);
                }
                public static PdfPTable DetalleHead(DataSet ds)
                {
                    return Helpers.Plantilla_2.DetalleHead(245);
                }
                public static PdfPTable PiePagina(DataSet ds)
                {
                    return Helpers.Plantilla_2.PiePagina(ds);
                }
                public static PdfPTable Detalles(DataSet ds)
                {
                    return Helpers.Plantilla_2.Detalles(ds);
                }
            }
        }
    }
}
