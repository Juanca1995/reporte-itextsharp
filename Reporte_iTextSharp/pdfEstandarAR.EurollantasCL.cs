﻿namespace RulesServicesDIAN2.Adquiriente
{
    using iTextSharp.text;
    using iTextSharp.text.pdf;
    using System;
    //Referenciar y usar.
    using System.Data;
    using System.Drawing;
    using System.IO;
    using System.Net;
    using System.Net.NetworkInformation;
    using System.Net.Sockets;
    using System.Web;
    using System.Windows.Forms;

    public partial class pdfEstandarAR
    {

        #region Formatos Facturacion EUROLLANTAS
        float dimxEuroLlantas = 540f, dimYEuroLlantas = 7f;
        public bool FacturaNacionalEuroLlantas(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {
            RoundRectangle CelEventBorderRound = new RoundRectangle();
            NomArchivo = string.Empty;
            string Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            string RutaImg = $@"{AppDomain.CurrentDomain.BaseDirectory}\Eurollantas.png";
            NomArchivo = NombreInvoice + ".pdf";
            try
            {
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.
                Document document = new Document(iTextSharp.text.PageSize.LETTER, 20f, 15f, 195f, 30f);
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));

                #region ENCABEZADO

                PdfPTable tableEncabezado = new PdfPTable(3);
                tableEncabezado.WidthPercentage = 100;
                tableEncabezado.SetWidths(new float[] { 1.5f, 3.2f, 1.8f });

                PdfPCell celLogo = new PdfPCell();
                iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle2 = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustom = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);

                Paragraph prgTitle2 = new Paragraph(string.Format("{0}\n{1}\n{2}\n PBX: {3}\n",
                        DsInvoiceAR.Tables["Company"].Rows[0]["Address3"].ToString(), DsInvoiceAR.Tables["Company"].Rows[0]["Address2"].ToString(),
                        DsInvoiceAR.Tables["Company"].Rows[0]["Address1"].ToString(), DsInvoiceAR.Tables["Company"].Rows[0]["PhoneNum"].ToString()), fontTitle2);
                prgTitle2.Alignment = Element.ALIGN_CENTER;
                celLogo.AddElement(prgTitle2);
                Paragraph prgTextInf = new Paragraph("www.eurollantas.com.co", fontTitle2);
                prgTextInf.Alignment = Element.ALIGN_CENTER;
                celLogo.AddElement(prgTextInf);
                Paragraph prgTextInf1 = new Paragraph("servicioalcliente@eurollantas.com.co", fontTitle2);
                prgTextInf1.Alignment = Element.ALIGN_CENTER;
                celLogo.AddElement(prgTextInf1);
                Paragraph prgTextInf2 = new Paragraph("Estamos Afiliados a Cifin y Procredito dando cumplimiento a la ley 1266 de 2008", fontTitle2);
                prgTextInf2.Alignment = Element.ALIGN_CENTER;
                celLogo.AddElement(prgTextInf2);
                Paragraph prgTextInf3 = new Paragraph("Actividad Economica 4530", fontTitle2);
                prgTextInf3.Alignment = Element.ALIGN_CENTER;
                celLogo.AddElement(prgTextInf3);
                Paragraph prgTitle3 = new Paragraph("REGIMEN COMUN -NO GRANDES CONTRIBUYENTES- SUJETOS DE RETENCION DE IVA - NO SOMOS AUTORRETENEDORES", fontTitle2);
                prgTitle3.Alignment = Element.ALIGN_CENTER;
                celLogo.AddElement(prgTitle3);
                celLogo.Border = 0;

                System.Drawing.Image logo = null;
                var requestLogo = WebRequest.Create(RutaImg);
                using (var responseLogo = requestLogo.GetResponse())
                using (var streamLogo = responseLogo.GetResponseStream())
                {
                    logo = Bitmap.FromStream(streamLogo);
                }
                iTextSharp.text.Image ImgLogo = iTextSharp.text.Image.GetInstance(logo, BaseColor.WHITE);
                ImgLogo.ScaleAbsolute(120f, 120f);

                //divLogo.BackgroundImage = ImgLogo;
                PdfPCell celLogoE4 = new PdfPCell();
                celLogoE4.Border = 0;
                Paragraph prgInfoLogo = new Paragraph(DsInvoiceAR.Tables["Company"].Rows[0]["Name"].ToString(), fontTitle);
                Paragraph prgNitlogo = new Paragraph("NIT:" + DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"].ToString(), fontTitle2);
                prgInfoLogo.Alignment = Element.ALIGN_CENTER;
                prgNitlogo.Alignment = Element.ALIGN_CENTER;
                celLogoE4.AddElement(ImgLogo);
                celLogoE4.AddElement(prgInfoLogo);
                celLogoE4.AddElement(prgNitlogo);
                tableEncabezado.AddCell(celLogoE4);

                iTextSharp.text.Font fontTitleFactura = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 9, iTextSharp.text.Font.NORMAL);

                PdfPTable tableFactura = new PdfPTable(1);
                tableFactura.WidthPercentage = 100;

                PdfPTable tableFacturaN = new PdfPTable(1);
                tableFactura.WidthPercentage = 100;

                PdfPCell celTittle = new PdfPCell(new Phrase("FACTURA DE VENTA", fontTitleFactura));
                celTittle.Border = 0;
                celTittle.BorderWidthBottom = 1;
                celTittle.HorizontalAlignment = Element.ALIGN_CENTER;
                tableFacturaN.AddCell(celTittle);

                PdfPCell celNo = new PdfPCell(new Phrase("No. " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"].ToString(), fontTitleFactura));
                celNo.Border = 0;
                celNo.HorizontalAlignment = Element.ALIGN_CENTER;
                tableFacturaN.AddCell(celNo);

                PdfPCell celTFac = new PdfPCell(tableFacturaN);
                celTFac.CellEvent = CelEventBorderRound;
                celTFac.Border = 0;

                tableFactura.AddCell(celTFac);

                /*CELL QR*/
                iTextSharp.text.Image QRPdf = iTextSharp.text.Image.GetInstance(QRInvoice, BaseColor.WHITE);
                QRPdf.ScaleAbsolute(70f, 70f);
                iTextSharp.text.pdf.PdfPCell celImgQR = new iTextSharp.text.pdf.PdfPCell(QRPdf);
                celImgQR.HorizontalAlignment = Element.ALIGN_CENTER;
                celImgQR.Border = 0;
                celImgQR.PaddingTop = 5;

                PdfPCell celVacia = new PdfPCell(new Phrase("  ", fontTitleFactura));
                celVacia.Border = 0;
                celVacia.HorizontalAlignment = Element.ALIGN_CENTER;
                tableFactura.AddCell(celImgQR);

                PdfPCell celFactura = new PdfPCell(tableFactura);
                celFactura.Border = 0;

                tableEncabezado.AddCell(celLogo);
                tableEncabezado.AddCell(celFactura);

                //--- Inicia datos del cliente -------------------
                
                PdfPTable tableFacturar = new PdfPTable(3);
                float[] DimencionFacturar = new float[3];
                DimencionFacturar[0] = 1.0F;//
                DimencionFacturar[1] = 0.01F;//
                DimencionFacturar[2] = 1.0F;//

                tableFacturar.WidthPercentage = 100;
                tableFacturar.SetWidths(DimencionFacturar);

                PdfPTable tableFacturarA = new PdfPTable(2);
                float[] DimencionFacturarA = new float[2];
                DimencionFacturarA[0] = 0.8F;//
                DimencionFacturarA[1] = 3.0F;//

                tableFacturarA.WidthPercentage = 100;
                tableFacturarA.SetWidths(DimencionFacturarA);


                DatosClienteTituloValor(ref tableFacturarA, "SEÑORES: ", DsInvoiceAR.Tables["InvcHead"].Rows[0]["CustomerName"].ToString());
                DatosClienteTituloValor(ref tableFacturarA, "NIT: ", DsInvoiceAR.Tables["Customer"].Rows[0]["CustID"].ToString());
                DatosClienteTituloValor(ref tableFacturarA, "DIRECCION: ", DsInvoiceAR.Tables["Customer"].Rows[0]["Address1"].ToString());
                DatosClienteTituloValor(ref tableFacturarA, "TELEFONO: ", DsInvoiceAR.Tables["Customer"].Rows[0]["PhoneNum"].ToString());
                DatosClienteTituloValor(ref tableFacturarA, "EMAIL: ", DsInvoiceAR.Tables["Customer"].Rows[0]["EMailAddress"].ToString());
                DatosClienteTituloValor(ref tableFacturarA, "CUFE: ", CUFE);

                PdfPCell celTittleFacturarA = new PdfPCell(tableFacturarA);
                celTittleFacturarA.CellEvent = CelEventBorderRound;
                celTittleFacturarA.Border = 0;
                tableFacturar.AddCell(celTittleFacturarA);

                PdfPCell celEspacio2 = new PdfPCell(new Phrase(" ", FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL)));
                celEspacio2.Border = 0;
                tableFacturar.AddCell(celEspacio2);

                PdfPTable infoCle = new PdfPTable(1);
                infoCle.WidthPercentage = 100;

                PdfPTable tableDespacharA = new PdfPTable(6);
                float[] DimencionDespacharA = new float[6];
                DimencionDespacharA[0] = 1.0F;//
                DimencionDespacharA[1] = 1.8F;//
                DimencionDespacharA[2] = 1.0F;//
                DimencionDespacharA[3] = 1.8F;//
                DimencionDespacharA[4] = 1.0F;//
                DimencionDespacharA[5] = 1.0F;//
                tableDespacharA.WidthPercentage = 100;
                tableDespacharA.SetWidths(DimencionDespacharA);

                DatosClienteTituloValor(ref tableDespacharA, "PLACA: ", DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar01"].ToString());
                DatosClienteTituloValor(ref tableDespacharA, "SOAT: ", DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar04"].ToString());

                PdfPCell celTitle = new PdfPCell(new Phrase("FECHA: ", FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL)));
                celTitle.Colspan = 1;
                celTitle.Padding = 3;
                celTitle.Border = 0;
                celTitle.BorderWidthLeft = 1;
                celTitle.HorizontalAlignment = Element.ALIGN_LEFT;
                celTitle.VerticalAlignment = Element.ALIGN_TOP;
                tableDespacharA.AddCell(celTitle);
                PdfPCell celValue = new PdfPCell(new Phrase(DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).ToString("dd/MM/yyyy"), FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL)));
                celValue.Colspan = 1;
                celValue.Padding = 3;
                celValue.Border = 0;
                celValue.HorizontalAlignment = Element.ALIGN_LEFT;
                celValue.VerticalAlignment = Element.ALIGN_TOP;
                tableDespacharA.AddCell(celValue);

                DatosClienteTituloValor(ref tableDespacharA, "VEHI: ", DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar02"].ToString());
                DatosClienteTituloValor(ref tableDespacharA, "TECNO: ", DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar05"].ToString());

                PdfPCell celTitle2 = new PdfPCell(new Phrase("ORDEN No: ", FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL)));
                celTitle2.Colspan = 1;
                celTitle2.Padding = 3;
                celTitle2.Border = 0;
                celTitle2.BorderWidthLeft = 1;
                celTitle2.HorizontalAlignment = Element.ALIGN_LEFT;
                celTitle2.VerticalAlignment = Element.ALIGN_TOP;
                tableDespacharA.AddCell(celTitle2);
                PdfPCell celValue2 = new PdfPCell(new Phrase(DsInvoiceAR.Tables["InvcHead"].Rows[0]["OrderNum"].ToString(), FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL)));
                celValue2.Colspan = 1;
                celValue2.Padding = 3;
                celValue2.Border = 0;
                celValue2.HorizontalAlignment = Element.ALIGN_LEFT;
                celValue2.VerticalAlignment = Element.ALIGN_TOP;
                tableDespacharA.AddCell(celValue2);

                DatosClienteTituloValor(ref tableDespacharA, "KMS: ", DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar03"].ToString());
                DatosClienteTituloValor(ref tableDespacharA, "EXTINTOR: ", DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar06"].ToString());

                PdfPCell celTitle3 = new PdfPCell(new Phrase("VEND: ", FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL)));
                celTitle3.Colspan = 1;
                celTitle3.Padding = 3;
                celTitle3.Border = 0;
                celTitle3.BorderWidthLeft = 1;
                celTitle3.HorizontalAlignment = Element.ALIGN_LEFT;
                celTitle3.VerticalAlignment = Element.ALIGN_TOP;
                tableDespacharA.AddCell(celTitle3);
                PdfPCell celValue3 = new PdfPCell(new Phrase(DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar07"].ToString(), FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL)));
                celValue3.Colspan = 1;
                celValue3.Padding = 3;
                celValue3.Border = 0;
                celValue3.HorizontalAlignment = Element.ALIGN_LEFT;
                celValue3.VerticalAlignment = Element.ALIGN_TOP;
                tableDespacharA.AddCell(celValue3);

                PdfPCell celDatosDespacharA = new PdfPCell(tableDespacharA);
                celDatosDespacharA.Border = 0;
                celDatosDespacharA.CellEvent = CelEventBorderRound;
                infoCle.AddCell(celDatosDespacharA);

                PdfPCell celResol = new PdfPCell(new Phrase(DsInvoiceAR.Tables["InvcHead"].Rows[0]["Character01"].ToString(), FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL)));
                celResol.Border = 0;
                celResol.HorizontalAlignment = Element.ALIGN_CENTER;
                infoCle.AddCell(celResol);

                PdfPCell celTablaInfo = new PdfPCell(infoCle);
                celTablaInfo.Border = 0;
                tableFacturar.AddCell(celTablaInfo);

                #endregion

                #region UNIDADES

                PdfPTable tableTituloUnidades = new PdfPTable(9);

                tableTituloUnidades.WidthPercentage = 100;
                tableTituloUnidades.SetWidths(new float[] { 1.8f, 0.7f, 0.7f, 2.8f, 0.7f, 0.7f, 1.5f, 0.7f, 1.5f });

                DetalleHeadTitulos(ref tableTituloUnidades, "CÓDIGO", false);
                DetalleHeadTitulos(ref tableTituloUnidades, "CAI", false);
                DetalleHeadTitulos(ref tableTituloUnidades, "CANT.", false);
                DetalleHeadTitulos(ref tableTituloUnidades, "DESCRIPCIÓN", false);
                DetalleHeadTitulos(ref tableTituloUnidades, "OP", false);
                DetalleHeadTitulos(ref tableTituloUnidades, "%", false);
                DetalleHeadTitulos(ref tableTituloUnidades, "VR.UNITARIO", false);
                DetalleHeadTitulos(ref tableTituloUnidades, "IVA", false);
                DetalleHeadTitulos(ref tableTituloUnidades, "VR.TOTAL", true);

                foreach (DataRow InvoiceLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
                {
                    if (!AddUnidadesEuroLlantas(tableTituloUnidades, InvoiceLine, DsInvoiceAR))
                        return false;
                }
                #endregion

                #region pie de pagina

                PdfPTable tabla = new PdfPTable(1);
                tabla.WidthPercentage = 100;
                PdfPCell celEspacio3 = new PdfPCell(new Phrase("  ", FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 0.3F, iTextSharp.text.Font.NORMAL)));
                celEspacio3.Border = 0;

                PdfPTable tableTotalesObs = new PdfPTable(3);
                tableTotalesObs.WidthPercentage = 100;
                tableTotalesObs.SetWidths(new float[] { 2.5f, 0.01f, 1.0f });

                PdfPTable _tableObs = new PdfPTable(1);
                _tableObs.WidthPercentage = 100;

                PdfPCell celValorLetras = new PdfPCell(new Phrase(string.Format("SON: {0}", Helpers.Compartido.
                    Nroenletras(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"].ToString())), FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL)));
                celValorLetras.Colspan = 1;
                celValorLetras.Padding = 5;
                celValorLetras.Border = 0;
                celValorLetras.BorderColorBottom = BaseColor.WHITE;
                celValorLetras.HorizontalAlignment = Element.ALIGN_LEFT;
                celValorLetras.VerticalAlignment = Element.ALIGN_TOP;

                PdfPCell celValorAsegurado = new PdfPCell(new Phrase(string.Format("OBSERVACIONES: {0}",
                   DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceComment"].ToString()), Helpers.Fuentes.Plantilla_2_fontTitle2));
                celValorAsegurado.Colspan = 1;
                celValorAsegurado.Padding = 5;
                celValorAsegurado.BorderWidthTop = 1;
                celValorAsegurado.BorderColorBottom = BaseColor.WHITE;
                celValorAsegurado.HorizontalAlignment = Element.ALIGN_LEFT;
                celValorAsegurado.VerticalAlignment = Element.ALIGN_TOP;

                PdfPTable tableContado = new PdfPTable(3);
                tableContado.WidthPercentage = 100;
                PdfPCell celEspacioTable1 = new PdfPCell(new Phrase("  ", FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL)));
                tableContado.AddCell(celEspacioTable1);
                PdfPCell celEspacioTable2 = new PdfPCell(new Phrase("  ", FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL)));
                tableContado.AddCell(celEspacioTable2);
                PdfPCell celEspacioTable3 = new PdfPCell(new Phrase("  ", FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL)));
                tableContado.AddCell(celEspacioTable3);
                PdfPCell celEspacioTable4 = new PdfPCell(new Phrase("  ", FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL)));
                celEspacioTable4.Border = 0;
                tableContado.AddCell(celEspacioTable4);
                PdfPCell celEspacioTable5 = new PdfPCell(new Phrase("  ", FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL)));
                tableContado.AddCell(celEspacioTable5);
                PdfPCell celEspacioTable6 = new PdfPCell(new Phrase("CONTADO\n" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"].ToString(), FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL)));
                celEspacioTable6.Border = 0;
                celEspacioTable6.HorizontalAlignment = Element.ALIGN_CENTER;
                tableContado.AddCell(celEspacioTable6);

                PdfPCell celObservaciones = new PdfPCell(tableContado);
                celObservaciones.Border = 0;

                _tableObs.AddCell(celValorLetras);
                _tableObs.AddCell(celEspacio3);
                _tableObs.AddCell(celValorAsegurado);
                _tableObs.AddCell(celEspacio3);
                _tableObs.AddCell(celObservaciones);

                PdfPCell _celObs = new PdfPCell(_tableObs);
                _celObs.Border = 0;
                _celObs.CellEvent = CelEventBorderRound;
                tableTotalesObs.AddCell(_celObs);

                PdfPCell _celEspacio2 = new PdfPCell(new Phrase(" ", FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL)));
                _celEspacio2.Border = 0;
                tableTotalesObs.AddCell(_celEspacio2);

                PdfPTable _tableTotales = new PdfPTable(2);
                float[] _DimencionTotales = new float[2];
                _DimencionTotales[0] = 1.5F;//
                _DimencionTotales[1] = 2.5F;//

                _tableTotales.WidthPercentage = 100;
                _tableTotales.SetWidths(_DimencionTotales);

                PdfPCell _celTextSubTotal = new PdfPCell(new Phrase("SUBTOTAL:", FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL)));
                _celTextSubTotal.Colspan = 1;
                _celTextSubTotal.Padding = 3;
                _celTextSubTotal.PaddingBottom = 5;
                _celTextSubTotal.Border = 0;
                _celTextSubTotal.BorderWidthRight = 1;
                _celTextSubTotal.BorderColorBottom = BaseColor.WHITE;
                _celTextSubTotal.HorizontalAlignment = Element.ALIGN_LEFT;
                _celTextSubTotal.VerticalAlignment = Element.ALIGN_TOP;
                _tableTotales.AddCell(_celTextSubTotal);

                PdfPCell _celSubTotal = new PdfPCell(new Phrase(string.Format("{0}", decimal.Parse(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubTotal"].ToString()).ToString("N2")), FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 8, iTextSharp.text.Font.NORMAL)));
                _celSubTotal.Colspan = 1;
                _celSubTotal.Padding = 3;
                _celSubTotal.PaddingBottom = 5;
                _celSubTotal.Border = 0;
                _celSubTotal.BorderColorBottom = BaseColor.WHITE;
                _celSubTotal.HorizontalAlignment = Element.ALIGN_RIGHT;
                _celSubTotal.VerticalAlignment = Element.ALIGN_TOP;
                _tableTotales.AddCell(_celSubTotal);

                PdfPCell _celTextDescuentos = new PdfPCell(new Phrase("RETEFUENTE:", FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL)));
                _celTextDescuentos.Colspan = 1;
                _celTextDescuentos.Padding = 3;
                _celTextDescuentos.PaddingBottom = 5;
                _celTextDescuentos.Border = 0;
                _celTextDescuentos.BorderWidthRight = 1;
                _celTextDescuentos.BorderColorBottom = BaseColor.WHITE;
                _celTextDescuentos.HorizontalAlignment = Element.ALIGN_LEFT;
                _celTextDescuentos.VerticalAlignment = Element.ALIGN_TOP;
                _tableTotales.AddCell(_celTextDescuentos);

                PdfPCell _celDescuentos = new PdfPCell(new Phrase(string.Format("{0}", GetValImpuestoByID("0A", DsInvoiceAR).ToString("N2")), FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 8, iTextSharp.text.Font.NORMAL)));
                _celDescuentos.Colspan = 1;
                _celDescuentos.Padding = 3;
                _celDescuentos.PaddingBottom = 5;
                _celDescuentos.Border = 0;
                _celDescuentos.BorderColorBottom = BaseColor.WHITE;
                _celDescuentos.HorizontalAlignment = Element.ALIGN_RIGHT;
                _celDescuentos.VerticalAlignment = Element.ALIGN_TOP;
                _tableTotales.AddCell(_celDescuentos);

                PdfPCell _celTextSubTotal2 = new PdfPCell(new Phrase("RETEIVA:", FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL)));
                _celTextSubTotal2.Colspan = 1;
                _celTextSubTotal2.Padding = 3;
                _celTextSubTotal2.PaddingBottom = 5;
                _celTextSubTotal2.Border = 0;
                _celTextSubTotal2.BorderWidthRight = 1;
                _celTextSubTotal2.BorderColorBottom = BaseColor.WHITE;
                _celTextSubTotal2.HorizontalAlignment = Element.ALIGN_LEFT;
                _celTextSubTotal2.VerticalAlignment = Element.ALIGN_TOP;
                _tableTotales.AddCell(_celTextSubTotal2);

                PdfPCell _celSubTotal2 = new PdfPCell(new Phrase(string.Format("{0}",
                    GetValImpuestoByID("0B", DsInvoiceAR).ToString("N2")), FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL)));
                _celSubTotal2.Colspan = 1;
                _celSubTotal2.Padding = 3;
                _celSubTotal2.PaddingBottom = 5;
                _celSubTotal2.Border = 0;
                _celSubTotal2.BorderColorBottom = BaseColor.WHITE;
                _celSubTotal2.HorizontalAlignment = Element.ALIGN_RIGHT;
                _celSubTotal2.VerticalAlignment = Element.ALIGN_TOP;
                _tableTotales.AddCell(_celSubTotal2);

                PdfPCell _celTextIva = new PdfPCell(new Phrase("IVA:", FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL)));
                _celTextIva.Colspan = 1;
                _celTextIva.Padding = 3;
                _celTextIva.PaddingBottom = 5;
                _celTextIva.Border = 0;
                _celTextIva.BorderWidthRight = 1;
                //_celTextIva.BorderColorBottom = BaseColor.BLACK;
                _celTextIva.HorizontalAlignment = Element.ALIGN_LEFT;
                _celTextIva.VerticalAlignment = Element.ALIGN_TOP;
                _tableTotales.AddCell(_celTextIva);

                PdfPCell _celIva = new PdfPCell(new Phrase(string.Format("{0}",
                    GetValImpuestoByID("01", DsInvoiceAR).ToString("N2")), FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL)));
                _celIva.Colspan = 1;
                _celIva.Padding = 3;
                _celIva.PaddingBottom = 5;
                _celIva.Border = 0;
                //_celIva.BorderColorBottom = BaseColor.BLACK;
                _celIva.HorizontalAlignment = Element.ALIGN_RIGHT;
                _celIva.VerticalAlignment = Element.ALIGN_TOP;
                _tableTotales.AddCell(_celIva);

                PdfPCell _celTextTotal = new PdfPCell(new Phrase("VALOR TOTAL:", FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL)));
                _celTextTotal.Colspan = 1;
                _celTextTotal.Padding = 3;
                _celTextTotal.PaddingBottom = 5;
                _celTextTotal.Border = 0;
                _celTextTotal.BorderWidthRight = 1;
                _celTextTotal.HorizontalAlignment = Element.ALIGN_LEFT;
                _celTextTotal.VerticalAlignment = Element.ALIGN_TOP;
                _tableTotales.AddCell(_celTextTotal);

                PdfPCell _celTotal = new PdfPCell(new Phrase(string.Format("{0}",
                    decimal.Parse(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"].ToString()).ToString("N2")), FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL)));
                _celTotal.Colspan = 1;
                _celTotal.Padding = 3;
                _celTotal.PaddingBottom = 5;
                _celTotal.Border = 0;
                _celTotal.HorizontalAlignment = Element.ALIGN_RIGHT;
                _celTotal.VerticalAlignment = Element.ALIGN_TOP;
                _tableTotales.AddCell(_celTotal);

                PdfPCell _celTotales = new PdfPCell(_tableTotales);
                _celTotales.Border = 0;
                _celTotales.CellEvent = CelEventBorderRound;
                tableTotalesObs.AddCell(_celTotales);

                PdfPCell celTableTotalesObs = new PdfPCell(tableTotalesObs);
                celTableTotalesObs.Border = 0;

                tabla.AddCell(celTableTotalesObs);

                //----- Pie de pagina 2 ---------

                PdfPTable tablaPie = new PdfPTable(1);
                tablaPie.WidthPercentage = 100;

                PdfPCell consigText = new PdfPCell(new Phrase(string.Format("FAVOR CONSIGNAR A NOMBRE DE CONCRELLANTAS SAS CTA CORRIENTE BANCOLOMBIA:02118182304",
                    ""), FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 9, iTextSharp.text.Font.NORMAL)));
                consigText.Border = 0;
                consigText.HorizontalAlignment = Element.ALIGN_CENTER;
                tablaPie.AddCell(consigText);

                PdfPCell celEspacio = new PdfPCell(new Phrase(" ", FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL)));
                celEspacio.Border = 0;
                tablaPie.AddCell(celEspacio);

                PdfPCell autorizacionText = new PdfPCell(new Phrase("Autorizo a Concrellantas SAS con nit 900.686.590-2 o a quien represente sus derechos u ostente en el futuro la calidad de acreedor a consultar, reportar, procesar, solicitar o divulgar a cualquier entidad que maneje o administre base de datos con la información referente a mi comportamiento comercial, crediticio y financiero dando cumplimiento a la Ley 1266 de 2008.\n\n" +
                    "En cumplimiento con lo establecido por el Decreto 1377 de 2013, por el cual se reglamenta la Ley 1581 de 2012, usted esta autorizado a CONCRE LLANTAS SAS, de manera libre, voluntaria, previa e inequívoca, la continuación del tratamiento de sus datos personales para las finalidades previstas en su política de protección de datos personales; la cual podrá consultar en la página web, ww.eurollantas.com.co.\n\n" +
                    "Esta factura de venta es titulo valor en virtud de la ley 1231 del 17 / 07 / 2008 en caso de mora se causarán intereses a la tasa máxima permitida, por la superintendencia financiera.", FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL)));
                autorizacionText.CellEvent = CelEventBorderRound;
                autorizacionText.Border = 0;
                autorizacionText.Padding = 8;
                autorizacionText.BackgroundColor = BaseColor.LIGHT_GRAY;
                tablaPie.AddCell(autorizacionText);

                PdfPCell celEspac1 = new PdfPCell(new Phrase(" ", FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL)));
                celEspac1.Border = 0;
                tablaPie.AddCell(celEspac1);

                PdfPCell infoTitle = new PdfPCell(new Phrase("INFORMACION DE QUIEN RECIBE:", FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL)));
                infoTitle.Border = 0;
                infoTitle.HorizontalAlignment = Element.ALIGN_CENTER;
                tablaPie.AddCell(infoTitle);

                PdfPCell celEspacio4 = new PdfPCell(new Phrase(" ", FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL)));
                celEspacio4.Border = 0;
                tablaPie.AddCell(celEspacio4);

                //Tabla informacion quien recive
                PdfPTable tablaInfo = new PdfPTable(3);
                tablaInfo.WidthPercentage = 100;

                PdfPTable tablaExpedidaPor = new PdfPTable(1);
                tablaExpedidaPor.WidthPercentage = 100;
                PdfPCell celCliente = new PdfPCell(new Phrase("Expedida Por:\n\n _______________________________________\n\n", FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL)));
                celCliente.Border = 0;
                celCliente.Padding = 12;
                tablaExpedidaPor.AddCell(celCliente);
                PdfPCell celTablaExpP = new PdfPCell(tablaExpedidaPor);
                celTablaExpP.Border = 0;
                celTablaExpP.CellEvent = CelEventBorderRound;

                PdfPTable tablaInfoClien = new PdfPTable(2);
                tablaInfoClien.WidthPercentage = 100;
                PdfPCell celClienteInf1 = new PdfPCell(new Phrase("Nombre\n\n", FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL)));
                celClienteInf1.Border = 0;
                celClienteInf1.BorderWidthRight = 1;
                celClienteInf1.BorderWidthBottom = 1;
                tablaInfoClien.AddCell(celClienteInf1);
                PdfPCell celClienteInf2 = new PdfPCell(new Phrase("Cedula\n\n", FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL)));
                celClienteInf2.Border = 0;
                celClienteInf2.BorderWidthBottom = 1;
                tablaInfoClien.AddCell(celClienteInf2);
                PdfPCell celClienteInf3 = new PdfPCell(new Phrase("Firma\n\n", FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL)));
                celClienteInf3.Border = 0;
                celClienteInf3.BorderWidthRight = 1;
                tablaInfoClien.AddCell(celClienteInf3);
                PdfPCell celClienteInf4 = new PdfPCell(new Phrase("Fecha\n\n", FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL)));
                celClienteInf4.Border = 0;
                tablaInfoClien.AddCell(celClienteInf4);
                PdfPCell celTablaC = new PdfPCell(tablaInfoClien);
                celTablaC.Border = 0;
                celTablaC.CellEvent = CelEventBorderRound;

                PdfPTable tablaFirma = new PdfPTable(1);
                tablaFirma.WidthPercentage = 100;
                PdfPCell firmaLegal = new PdfPCell(new Phrase("Firma Representante Legal - Cliente:\n\n _______________________________________\n\n", FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL)));
                firmaLegal.Border = 0;
                firmaLegal.Padding = 12;
                tablaFirma.AddCell(firmaLegal);
                PdfPCell celTablaFirma = new PdfPCell(tablaFirma);
                celTablaFirma.Border = 0;
                celTablaFirma.CellEvent = CelEventBorderRound;

                tablaInfo.AddCell(celTablaExpP);
                tablaInfo.AddCell(celTablaC);
                tablaInfo.AddCell(celTablaFirma);
                PdfPCell celTablaInfo2 = new PdfPCell(tablaInfo);
                celTablaInfo2.Border = 0;
                tablaPie.AddCell(celTablaInfo2);


                #endregion

                writer.PageEvent = new EventPageEuroLlantas(tableEncabezado, tableFacturar, tableTituloUnidades, tabla, tablaPie);
                document.Open();

                /*PIE DE PAGINA*/
                PdfContentByte pCb = writer.DirectContent;
                pCb.BeginText();
                pCb.SetFontAndSize(BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.WINANSI, false), 8);
                pCb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "IMPRESO POR CONCRE LLANTAS S.A.S NIT: " + NIT + ".      ORIGINAL    ATENDIDO POR: " +
                    DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar09"].ToString(), 300, 15, 0);
                pCb.EndText();

                writer.Flush();
                document.Close();
                RutaPdf = NomArchivo;
                AddPageNumberPagTo($"{Ruta}{NomArchivo}", dimxEuroLlantas, dimYEuroLlantas + 10, fontTitle);
                AddPageContinueLeonisa($"{Ruta}{NomArchivo}", dimxEuroLlantas, dimYEuroLlantas, fontTitle);
                return true;
            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.Message;
                return false;
            }
        }
        #endregion

        #region Metodos 
        public static void DatosClienteTituloValor(ref PdfPTable table, string title, string value)
        {
            PdfPCell celTitle = new PdfPCell(new Phrase(title, FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL)));
            celTitle.Colspan = 1;
            celTitle.Padding = 3;
            celTitle.Border = 0;
            celTitle.HorizontalAlignment = Element.ALIGN_LEFT;
            celTitle.VerticalAlignment = Element.ALIGN_TOP;
            table.AddCell(celTitle);

            PdfPCell celValue = new PdfPCell(new Phrase(value, FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL)));
            celValue.Colspan = 1;
            celValue.Padding = 3;
            celValue.Border = 0;
            celValue.HorizontalAlignment = Element.ALIGN_LEFT;
            celValue.VerticalAlignment = Element.ALIGN_TOP;
            table.AddCell(celValue);
        }

        public static void DetalleHeadTitulos(ref PdfPTable table, string Titulo, bool BordeDerecho)
        {
            PdfPCell celda = new PdfPCell(new Phrase(Titulo, FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL)));
            celda.Colspan = 1;
            celda.Padding = 3;
            if (!BordeDerecho)
                celda.BorderWidthRight = 0;
            celda.HorizontalAlignment = Element.ALIGN_CENTER;
            celda.VerticalAlignment = Element.ALIGN_TOP;
            celda.BackgroundColor = BaseColor.LIGHT_GRAY;
            table.AddCell(celda);
        }

        public static void DetallesCeldas(ref PdfPTable table, string data, iTextSharp.text.Font font)
        {
            PdfPCell celda = new PdfPCell(new Phrase(data, font));
            celda.Colspan = 1;
            celda.Padding = 3;
            celda.HorizontalAlignment = Element.ALIGN_CENTER;
            celda.VerticalAlignment = Element.ALIGN_TOP;
            table.AddCell(celda);
        }

        private bool AddUnidadesEuroLlantas(PdfPTable dataTable, DataRow InvoiceLine, DataSet DsInvoiceAR)
        {
            try
            {
                DetallesCeldas(ref dataTable, (string)InvoiceLine["PartNum"], FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL));
                DetallesCeldas(ref dataTable, (string)InvoiceLine["ShortChar01"], FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL));
                DetallesCeldas(ref dataTable, (string)InvoiceLine["SellingShipQty"], FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL));
                DetallesCeldas(ref dataTable, (string)InvoiceLine["LineDesc"], FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL));
                DetallesCeldas(ref dataTable, (string)InvoiceLine["ShortChar02"], FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL));
                DetallesCeldas(ref dataTable, decimal.Parse((string)InvoiceLine["DiscountPercent"]).ToString("N2"), FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL));
                DetallesCeldas(ref dataTable, decimal.Parse((string)InvoiceLine["DocUnitPrice"]).ToString("N2"), FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL));
                DetallesCeldas(ref dataTable, decimal.Parse(GetPercentIdImpDIAN((string)InvoiceLine["InvoiceNum"], (string)InvoiceLine["InvoiceLine"], DsInvoiceAR.Tables["InvcTax"])).ToString("N2"), FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL));
                DetallesCeldas(ref dataTable, decimal.Parse((string)InvoiceLine["DspDocExtPrice"]).ToString("N2"), FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL));
                return true;
            }
            catch (Exception ex)
            {
                strError += "Error agregando items de la factura " + ex.ToString();
                return false;
            }
        }

        public class EventPageEuroLlantas : PdfPageEventHelper
        {
            PdfContentByte cb;

            // we will put the final number of pages in a template
            PdfTemplate headerTemplate, footerTemplate;

            // this is the BaseFont we are going to use for the header / footer
            BaseFont bf = null;

            // This keeps track of the creation time
            DateTime PrintTime = DateTime.Now;

            private string _header;

            public string Header
            {
                get { return _header; }
                set { _header = value; }
            }
            PdfPTable _Encabezado;
            PdfPTable _DatosCliente;
            PdfPTable _DetalleHead;
            PdfPTable _PiePagina;
            PdfPTable _PiePagina2;

            public PdfPTable PiePagina
            {
                get
                {
                    return _PiePagina;
                }
                set
                {
                    _PiePagina = value;
                }
            }
            public PdfPTable PiePagina2
            {
                get
                {
                    return _PiePagina2;
                }
                set
                {
                    _PiePagina2 = value;
                }
            }
            public PdfPTable Encabezado
            {
                get
                {
                    return _Encabezado;
                }
                set
                {
                    _Encabezado = value;
                }
            }
            public PdfPTable DatosCliente
            {
                get
                {
                    return _DatosCliente;
                }
                set
                {
                    _DatosCliente = value;
                }
            }
            public PdfPTable DetalleHead
            {
                get
                {
                    return _DetalleHead;
                }
                set
                {
                    _DetalleHead = value;
                }
            }

            public EventPageEuroLlantas(PdfPTable encabezado, PdfPTable DatosCliente, PdfPTable detalleHead, PdfPTable piepagina, PdfPTable piepagina2)
            {
                _Encabezado = encabezado;
                _DatosCliente = DatosCliente;
                _DetalleHead = detalleHead;
                _PiePagina = piepagina;
                _PiePagina2 = piepagina2;
            }

            public override void OnOpenDocument(PdfWriter writer, Document document)
            {
                try
                {
                    PrintTime = DateTime.Now;
                    bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                    cb = writer.DirectContent;
                    headerTemplate = cb.CreateTemplate(100, 100);
                    footerTemplate = cb.CreateTemplate(50, 50);
                }
                catch (DocumentException de)
                {
                }
                catch (System.IO.IOException ioe)
                {
                }
            }

            public override void OnEndPage(PdfWriter writer, Document document)
            {
                base.OnEndPage(writer, document);

                Encabezado.TotalWidth = document.PageSize.Width - 35f;
                Encabezado.WriteSelectedRows(0, -1, 20, document.PageSize.Height - 10, writer.DirectContentUnder);

                _DatosCliente.TotalWidth = document.PageSize.Width - 35f;
                _DatosCliente.WriteSelectedRows(0, -1, 20, document.PageSize.Height - 120, writer.DirectContentUnder);

                _DetalleHead.TotalWidth = document.PageSize.Width - 35f;
                _DetalleHead.WriteSelectedRows(0, -1, 20, document.PageSize.Height - 220, writer.DirectContentUnder);

                _PiePagina.TotalWidth = document.PageSize.Width - 35f;
                _PiePagina.WriteSelectedRows(0, -1, 20, document.PageSize.Height - 467, writer.DirectContentUnder);

                _PiePagina2.TotalWidth = document.PageSize.Width - 35f;
                _PiePagina2.WriteSelectedRows(0, -1, 20, document.PageSize.Height - 560, writer.DirectContentUnder);
            }

            public override void OnCloseDocument(PdfWriter writer, Document document)
            {
                base.OnCloseDocument(writer, document);

                headerTemplate.BeginText();
                headerTemplate.SetFontAndSize(bf, 12);
                headerTemplate.SetTextMatrix(0, 0);
                headerTemplate.ShowText((writer.PageNumber - 1).ToString());
                headerTemplate.EndText();
            }
        }
        #endregion
    }
}
