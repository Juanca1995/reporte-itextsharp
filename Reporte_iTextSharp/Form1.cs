﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
//using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Web;
using System.Xml.Linq;
using System.Xml;
using System.Net;
using RulesServicesDIAN2.Adquiriente;
using System.Drawing;
using System.Diagnostics;

namespace Reporte_iTextSharp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            XmlInvoice.Load(RutaXmlInvoice);
            ds = GenerarDataSet(RutaXmlInvoice);
        }

        pdfEstandarAR pdfEstandar = new pdfEstandarAR();


        //string RutaXmlInvoice = AppDomain.CurrentDomain.BaseDirectory.ToString() + "XmlInvoiceNum245808.xml";
        //string RutaXmlInvoice = AppDomain.CurrentDomain.BaseDirectory.ToString() + "Simex37358.xml";
        //string RutaXmlInvoice = AppDomain.CurrentDomain.BaseDirectory.ToString() + "Aldor Ingles.Xml";
        //string RutaXmlInvoice = AppDomain.CurrentDomain.BaseDirectory.ToString() + "interdoor.Xml";
        //string RutaXmlInvoice = AppDomain.CurrentDomain.BaseDirectory.ToString() + "XMLDunnington.Xml";
        //string RutaXmlInvoice = AppDomain.CurrentDomain.BaseDirectory.ToString() + "ppycorrugados.Xml";
        //string RutaXmlInvoice = AppDomain.CurrentDomain.BaseDirectory.ToString() + "DORI.Xml";
        //string RutaXmlInvoice = AppDomain.CurrentDomain.BaseDirectory.ToString() + "Xenco.Xml";
        //string RutaXmlInvoice = AppDomain.CurrentDomain.BaseDirectory.ToString() + "hotel.Xml";
        //string RutaXmlInvoice = AppDomain.CurrentDomain.BaseDirectory.ToString() + "inversiones-dinastía.Xml";
        //string RutaXmlInvoice = AppDomain.CurrentDomain.BaseDirectory.ToString() + "XmlInvoiceNum66194.Xml";
        string RutaXmlInvoice = AppDomain.CurrentDomain.BaseDirectory.ToString() + "XmlInvoiceNum245808.Xml";
        //string RutaXmlInvoice = AppDomain.CurrentDomain.BaseDirectory.ToString() + "Factura_Procar.Xml";
        //string RutaXmlInvoice = AppDomain.CurrentDomain.BaseDirectory.ToString() + "flores.Xml";
        //string RutaXmlInvoice = AppDomain.CurrentDomain.BaseDirectory.ToString() + "LABORALESMEDELLIN.Xml"; 
        //string RutaXmlInvoice = AppDomain.CurrentDomain.BaseDirectory.ToString() + "Atmopel _250025.Xml";
        //string RutaXmlInvoice = AppDomain.CurrentDomain.BaseDirectory.ToString() + "Abracol_02.Xml";
        //string RutaXmlInvoice = AppDomain.CurrentDomain.BaseDirectory.ToString() + "Platinovo_Nota_credito_6776.Xml";
        //string RutaXmlInvoice = AppDomain.CurrentDomain.BaseDirectory.ToString() + "RetycolFacturaDolares.Xml";
        //string RutaXmlInvoice = AppDomain.CurrentDomain.BaseDirectory.ToString() + "XmlInvoiceNum36284.xml"; //formato para Simex
        XmlDocument XmlInvoice = new XmlDocument();

        //Convertir XML a Dataset.
        private DataSet GenerarDataSet(string xmlfile)
        {
            try
            {
                DataSet dataSet = new DataSet();
                StringReader xmlSR = new StringReader(xmlfile);

                System.IO.FileStream fsReadXml = new System.IO.FileStream
                (xmlfile, System.IO.FileMode.Open);
                ///Cargamos el DataSet..
                dataSet.ReadXml(fsReadXml);
                return dataSet;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Estatus:Error en GenerarDataSet: " + ex.Message + " \n");
                //PeticionWCF.Tracer += "Hora:" + DateTime.Now.ToString() + " \n\n";
                return null;
            }
        }
        
        DataSet ds = new DataSet();
        private void btnImportar_Click(object sender, EventArgs e)
        {
            //XmlInvoice.Load(RutaXmlInvoice);
            //ds=GenerarDataSet(RutaXmlInvoice);

            //MessageBox.Show(dt.Rows[0]["Company"].ToString());
        }

        private void btnGenerarPdf_Click(object sender, EventArgs e)
        {
            System.Drawing.Image logo = null;
            //Banner
            try
            {
                //var requestBanner = WebRequest.Create($@"C:\Users\key94\source\repos\Reporte_iTextSharp\Reporte_iTextSharp\bin\Debug\imgQR.png");
                var requestBanner = WebRequest.Create($@"{AppDomain.CurrentDomain.BaseDirectory}\imgQR.png");
                using (var responseBanner = requestBanner.GetResponse())
                using (var streamBanner = responseBanner.GetResponseStream())
                {
                    logo = Bitmap.FromStream(streamBanner);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Error al cargar Logo Banner, not foud\n");
            }

            DataTable dt = ds.Tables["InvcHead"];
            DataRow row = ds.Tables["InvcHead"].Rows[0];
            var ruta = string.Empty;

            //pdfEstandar.CreatePDF();
            //pdfEstandar.FacturaNacionalLeonisa1("900665411",
            //    "FacturaNacionalLeonisa1", ref ruta, ds, logo, dt.Rows[0]["InvoiceType"].ToString(),"cufe");
            //pdfEstandar.FacturaLocal_AGP("900665411", "Factura_AGP1", ref ruta, ds, logo, dt.Rows[0]["InvoiceType"].ToString(), "cufe");
            //pdfEstandar.FormatoDataCredito("900665411", "FormatoDataCredito", ref ruta, ds, logo, dt.Rows[0]["InvoiceType"].ToString(), "cufe");
            //pdfEstandar.FacturaNacionalSimex("900665411", "FacturaNacionalSimex", ref ruta, ds, logo, dt.Rows[0]["InvoiceType"].ToString(), "cufe");
            //pdfEstandar.FacturaNacionalPlastinovo("900665411", "FacturaNacionalPlastinovo", ref ruta, ds, logo, dt.Rows[0]["InvoiceType"].ToString(), "cufe");
            //pdfEstandar.FacturaExportacionPlastinovo("900665411", "FacturaExportacionPlastinovo", ref ruta, ds, logo, dt.Rows[0]["InvoiceType"].ToString(), "cufe");
            //pdfEstandar.NotaCreditoPlastinovo("900665411", "Plastinovo Nota credito", ref ruta, ds, logo, dt.Rows[0]["InvoiceType"].ToString(), "cufe");
            //pdfEstandar.FacturaNacionalSoinco("900665411", "FacturaNacionalSoinco", ref ruta, ds, logo, dt.Rows[0]["InvoiceType"].ToString(), "cufe");
            //pdfEstandar.FacturaNacionalABRACOL("900665411", "Factura_Abracol", ref ruta, ds, logo, dt.Rows[0]["InvoiceType"].ToString(), "cufe");
            //pdfEstandar.FacturaNotaCreditoABRACOL("900665411", "NotaCredito_Abracol", ref ruta, ds, logo, dt.Rows[0]["InvoiceType"].ToString(), "cufe");
            //pdfEstandar.NotaCreditoAtmopel("900665411", "Atmopel Nota Crédito", ref ruta, ds, logo, dt.Rows[0]["InvoiceType"].ToString(), "cufe");
            //pdfEstandar.FacturaNacionalAtmopel("900665411", "Atmopel Factura Nacional", ref ruta, ds, logo, dt.Rows[0]["InvoiceType"].ToString(), "cufe");
            //pdfEstandar.FacturaExportacionSoinco("900665411", "FacturaExportacionSoinco", ref ruta, ds, logo, dt.Rows[0]["InvoiceType"].ToString(), "cufe");
            //pdfEstandar.FacturaNacionalRetycol("900665411", "RetycolFactura", ref ruta, ds, logo, dt.Rows[0]["InvoiceType"].ToString(), "cufe");
            //pdfEstandar.NotaCreditoAtmopel("900665411", "Atmopel Nota credito", ref ruta, ds, logo, dt.Rows[0]["InvoiceType"].ToString(), "cufe");
            //pdfEstandar.NotaDebitoAtmopel("900665411", "Atmopel Nota debito", ref ruta, ds, logo, dt.Rows[0]["InvoiceType"].ToString(), "cufe");
            //pdfEstandar.FacturaNacionalFedco("900665411", "Fedco", ref ruta, ds, logo, dt.Rows[0]["InvoiceType"].ToString(), "cufe");
            //pdfEstandar.FacturaNacionalE4("900665411", "E_Quatro", ref ruta, ds, logo, row["InvoiceType"].ToString(), "d58a0e5cbc4125bf62c39077c7622946bf7cd530");
            //pdfEstandar.FacturaUrbanas("900665411", "Urbanas", ref ruta, ds, logo, row["InvoiceType"].ToString(), "cufe");
            //pdfEstandar.FacturaEng1ALDOR("900665411", "Aldor Inglés", ref ruta, ds, logo, row["InvoiceType"].ToString(), "cufe");
            //pdfEstandar.FacturaVenezuelaALDOR("900665411", "Aldor Venezuela", ref ruta, ds, logo, row["InvoiceType"].ToString(), "cufe");
            //pdfEstandar.FacturaNotaVentalALDOR("900665411", "Aldor Nota venta", ref ruta, ds, logo, row["InvoiceType"].ToString(), "cufe");
            //pdfEstandar.FacturaNotaVentaVenzuelaALDOR("900665411", "Aldor Nota venta venezuela", ref ruta, ds, logo, row["InvoiceType"].ToString(), "cufe");
            pdfEstandar.FacturaNacionalCentrodeOrtopediayTraumatologia("900665411", "FacturaNacionalCentrodeOrtopediayTraumatologia", ref ruta, ds, logo, row["InvoiceType"].ToString(), "cufe");




            System.Diagnostics.Process.Start($@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\{ruta}");
            Application.Exit();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            btnGenerarPdf_Click(sender, e);
            //btnGenerarPdf_Click(sender, e);
        }
    }
}
