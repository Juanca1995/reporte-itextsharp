﻿namespace RulesServicesDIAN2.Adquiriente
{
    using iTextSharp.text;
    using iTextSharp.text.pdf;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Diagnostics;
    using System.Drawing;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.NetworkInformation;
    using System.Text;
    using System.Threading.Tasks;
    using System.Web;
    using System.Windows.Forms;

    public partial class pdfEstandarAR
    {
        #region Formatos Facturacion BELFORT

        private decimal GetValTaxableAmt(string InvoiceNum, string InvoiceLine, string IdImPDIAN, DataSet DsInvcHed)
        {
            decimal ValImp = 0;

            try
            {
                //Trasa += "Valor Impuesto ID" + IdImPDIAN + "\n";
                //Recorremos los Impuestos y acumulamos por IdImpDIAN.                
                foreach (DataRow RowTax in DsInvcHed.Tables["InvcTax"].Rows)
                {
                    //Buscamos el IdImpDIAN por el RateCode del InvcTax.      
                    DataRow[] RowImpDIAN = DsInvcHed.Tables["SalesTRC"].Select("RateCode='" + RowTax["RateCode"].ToString() + "'");
                    if (RowImpDIAN != null && RowImpDIAN.GetLength(0) > 0 &&
                        RowImpDIAN[0]["IdImpDIAN_c"].ToString() == IdImPDIAN &&
                        (string)RowTax["InvoiceNum"] == InvoiceNum &&
                        (string)RowTax["InvoiceLine"] == InvoiceLine)
                    {
                        //Trasa += "RateCode =" + RowTax["RateCode"].ToString() + "IdImpDIAN =" + RowImpDIAN[0]["IdImpDIAN_c"].ToString() + " Valor" + Convert.ToDecimal(RowTax["DocTaxAmt"]) + "\n";                        
                        ValImp += Convert.ToDecimal(RowTax["DocTaxableAmt"]);
                    }
                }

                return ValImp;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public bool FacturaNotaDebitoBelfort(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {
            NomArchivo = string.Empty;
            string Ruta = HttpContext.Current.Server.MapPath("~/PDF/" + NIT + "/");
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            string RutaImg = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";
            NomArchivo = NombreInvoice + ".pdf";

            try
            {
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.

                Document document = new Document(iTextSharp.text.PageSize.LETTER);

                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;

                // step 3: we open the document     
                document.Open();

                PdfDiv divEspacio = new PdfDiv();
                divEspacio.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio.Height = 10;
                divEspacio.Width = 130;

                PdfDiv divEspacio2 = new PdfDiv();
                divEspacio2.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio2.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                divEspacio2.Height = 2;
                divEspacio2.Width = 130;

                //FUENTES----------------------------------------------------------------------------------------------------------
                iTextSharp.text.Font fontTitleBold = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 8, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustom = FontFactory.GetFont(FontFactory.HELVETICA, 6f, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustomI = FontFactory.GetFont(FontFactory.HELVETICA_OBLIQUE, 6f, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustomBold = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 6, iTextSharp.text.Font.NORMAL);
                //FUENTES---------------------------------------------------------------------------------------------------------
                RoundRectangle CelEventBorderRound = new RoundRectangle();

                #region ENCABEZADO
                PdfPTable tableEncabezado = new PdfPTable(1);
                tableEncabezado.WidthPercentage = 100;
                PdfPCell celEncabezado = new PdfPCell()
                {
                    Border = PdfPCell.NO_BORDER,
                    BorderWidthTop = 1.01f,
                    PaddingBottom = 30
                };

                tableEncabezado.AddCell(celEncabezado);
                #endregion

                #region Datos del Cliente
                PdfPTable tableInfoCliente = new PdfPTable(new float[3] { 3, 0.18f, 2.5f });
                tableInfoCliente.WidthPercentage = 85;
                tableInfoCliente.HorizontalAlignment = Element.ALIGN_LEFT;

                Paragraph prgNombreEmpresa = new Paragraph("DANN CARLTON MEDELLIN",
                    FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 14, iTextSharp.text.Font.NORMAL));

                Paragraph prgDatosEmpresa = new Paragraph($"\n\n\n\n\nTeléfono {3664908}" +
                    $"\nFax 3664908 \n" +
                    $"____________________________________________________\n" +
                    $"NIF", fontTitle);
                PdfPCell celInfoEmpresa = new PdfPCell()
                {

                };
                celInfoEmpresa.AddElement(prgNombreEmpresa);
                celInfoEmpresa.AddElement(prgDatosEmpresa);
                tableInfoCliente.AddCell(celInfoEmpresa);

                PdfPCell celEspacioInfoCliente = new PdfPCell();
                celEspacioInfoCliente.Border = 0;
                tableInfoCliente.AddCell(celEspacioInfoCliente);

                PdfPTable tableDatosCliente = new PdfPTable(1);

                Paragraph prgDatosCliente = new Paragraph($"Exmo(s) Sr.(s)\n{ (string)DsInvoiceAR.Tables["Customer"].Rows[0]["Name"] }\n" +
                    $"{(string)DsInvoiceAR.Tables["Customer"].Rows[0]["Address1"]}\n\n\n" +
                    $"{(string)DsInvoiceAR.Tables["Customer"].Rows[0]["City"]}                                  xxxxxxx",
                    fontTitle);

                PdfPCell celDatosCliente1 = new PdfPCell()
                {
                    VerticalAlignment = Element.ALIGN_TOP,
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    //Border=PdfPCell.NO_BORDER
                };
                celDatosCliente1.AddElement(prgDatosCliente);
                tableDatosCliente.AddCell(celDatosCliente1);

                PdfPCell celDatosCliente2 = new PdfPCell(new Phrase($"NIF  {(string)DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"]}\n", fontTitle))
                {
                    VerticalAlignment = Element.ALIGN_TOP,
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    Padding = 3
                };
                tableDatosCliente.AddCell(celDatosCliente2);

                Paragraph prgTituloNumFactura = new Paragraph("NOTA DEBITO",
                    FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 14, iTextSharp.text.Font.NORMAL));
                prgTituloNumFactura.Alignment = Element.ALIGN_CENTER;

                Paragraph prgValNumFactura = new Paragraph($"\n{(string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"]} / ND",
                    fontTitle);
                prgValNumFactura.Alignment = Element.ALIGN_CENTER;

                PdfPCell celNumFactura = new PdfPCell()
                {
                    VerticalAlignment = Element.ALIGN_TOP,
                    Border = PdfPCell.NO_BORDER,
                    Padding = 3
                };
                celNumFactura.AddElement(prgTituloNumFactura);
                celNumFactura.AddElement(prgValNumFactura);
                tableDatosCliente.AddCell(celNumFactura);

                PdfPCell celDatosCliente = new PdfPCell(tableDatosCliente)
                {
                    Border = PdfPCell.NO_BORDER
                };
                tableInfoCliente.AddCell(celDatosCliente);
                #endregion

                #region UNIDADES
                PdfPTable tableEncabezadoMoneda = new PdfPTable(new float[] { 1, 4, 1, 1 });
                tableEncabezadoMoneda.WidthPercentage = 85;
                tableEncabezadoMoneda.HorizontalAlignment = Element.ALIGN_LEFT;

                //PdfPTable tableContenidoEncabezado = new PdfPTable(4);
                //tableContenidoEncabezado.WidthPercentage = 100;

                PdfPCell celTextFechaMoneda = new PdfPCell()
                {
                    //Border = PdfPCell.NO_BORDER,
                    VerticalAlignment = Element.ALIGN_CENTER,
                    Phrase = new Phrase("Fecha", fontTitleBold),
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    BorderWidthRight = PdfPCell.NO_BORDER,
                    BorderWidthBottom = PdfPCell.NO_BORDER,
                };
                tableEncabezadoMoneda.AddCell(celTextFechaMoneda);

                PdfPCell celTextDescMoneda = new PdfPCell()
                {
                    VerticalAlignment = Element.ALIGN_CENTER,
                    Phrase = new Phrase("Descripción", fontTitleBold),
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    BorderWidthRight = PdfPCell.NO_BORDER,
                    BorderWidthLeft = PdfPCell.NO_BORDER,
                    BorderWidthBottom = PdfPCell.NO_BORDER,
                };
                tableEncabezadoMoneda.AddCell(celTextDescMoneda);

                PdfPCell celTextMoneda = new PdfPCell()
                {
                    //Border = PdfPCell.NO_BORDER,
                    VerticalAlignment = Element.ALIGN_CENTER,
                    Phrase = new Phrase("Moneda", fontTitleBold),
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    BorderWidthRight = PdfPCell.NO_BORDER,
                    BorderWidthLeft = PdfPCell.NO_BORDER,
                    BorderWidthBottom = PdfPCell.NO_BORDER,
                };
                tableEncabezadoMoneda.AddCell(celTextMoneda);

                PdfPCell celTextCambio = new PdfPCell()
                {
                    //Border = PdfPCell.NO_BORDER,
                    VerticalAlignment = Element.ALIGN_CENTER,
                    Phrase = new Phrase("Cambio", fontTitleBold),
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    BorderWidthLeft = PdfPCell.NO_BORDER,
                    BorderWidthBottom = PdfPCell.NO_BORDER,
                };
                tableEncabezadoMoneda.AddCell(celTextCambio);
                //--------------------------------------------------------------------------------------
                PdfPCell celValFechaMoneda = new PdfPCell()
                {
                    //Border = PdfPCell.NO_BORDER,
                    VerticalAlignment = Element.ALIGN_CENTER,
                    Phrase = new Phrase("********", fontTitle),
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    BorderWidthRight = PdfPCell.NO_BORDER,
                    BorderWidthTop = PdfPCell.NO_BORDER,
                    Padding = 5,
                };
                tableEncabezadoMoneda.AddCell(celValFechaMoneda);

                PdfPCell celValDescMoneda = new PdfPCell()
                {
                    VerticalAlignment = Element.ALIGN_CENTER,
                    Phrase = new Phrase("*********", fontTitle),
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    BorderWidthRight = PdfPCell.NO_BORDER,
                    BorderWidthLeft = PdfPCell.NO_BORDER,
                    BorderWidthTop = PdfPCell.NO_BORDER,
                    Padding = 5,
                };
                tableEncabezadoMoneda.AddCell(celValDescMoneda);

                PdfPCell celValMoneda = new PdfPCell()
                {
                    //Border = PdfPCell.NO_BORDER,
                    VerticalAlignment = Element.ALIGN_CENTER,
                    Phrase = new Phrase($"{(string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["CurrencyCode"]}", fontTitle),
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    BorderWidthRight = PdfPCell.NO_BORDER,
                    BorderWidthLeft = PdfPCell.NO_BORDER,
                    BorderWidthTop = PdfPCell.NO_BORDER,
                    Padding = 5,
                };
                tableEncabezadoMoneda.AddCell(celValMoneda);

                PdfPCell celValCambio = new PdfPCell()
                {
                    //Border = PdfPCell.NO_BORDER,
                    VerticalAlignment = Element.ALIGN_CENTER,
                    Phrase = new Phrase("******", fontTitle),
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    BorderWidthLeft = PdfPCell.NO_BORDER,
                    BorderWidthTop = PdfPCell.NO_BORDER,
                    Padding = 5,
                };
                tableEncabezadoMoneda.AddCell(celValCambio);
                //--------------------------------------------------------------------------------------
                PdfPTable tableEncabezadoUnidades = new PdfPTable(new float[] { 1, 2.1f, 2.1f, 1 });
                tableEncabezadoUnidades.WidthPercentage = 85;
                tableEncabezadoUnidades.HorizontalAlignment = Element.ALIGN_LEFT;

                PdfPCell celTextFechaUnidades = new PdfPCell(new Phrase(
                    "Fecha", fontTitleBold))
                {
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    Padding = 3,
                };
                tableEncabezadoUnidades.AddCell(celTextFechaUnidades);

                PdfPCell celTextServicios = new PdfPCell(new Phrase(
                    "Servicios", fontTitleBold))
                {
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    Padding = 3,
                };
                tableEncabezadoUnidades.AddCell(celTextServicios);

                PdfPCell celTextDescripcion = new PdfPCell(new Phrase(
                    "Descripción", fontTitleBold))
                {
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    Padding = 3,
                };
                tableEncabezadoUnidades.AddCell(celTextDescripcion);

                PdfPCell celTextValor = new PdfPCell(new Phrase(
                    "Valor", fontTitleBold))
                {
                    HorizontalAlignment = Element.ALIGN_RIGHT,
                    Padding = 3,
                };
                tableEncabezadoUnidades.AddCell(celTextDescripcion);
                //--------------------------------------------------------------------------------------
                PdfPCell celValFechaUnidades = new PdfPCell(new Phrase(
                    " ", fontTitleBold))
                {
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    Padding = 3,
                };
                tableEncabezadoUnidades.AddCell(celValFechaUnidades);

                PdfPCell celValServicios = new PdfPCell(new Phrase(
                    " ", fontTitleBold))
                {
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    Padding = 3,
                };
                tableEncabezadoUnidades.AddCell(celValServicios);

                PdfPCell celValDescripcion = new PdfPCell(new Phrase(
                    " ", fontTitleBold))
                {
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    Padding = 3,
                };
                tableEncabezadoUnidades.AddCell(celValDescripcion);

                PdfPCell celValValor = new PdfPCell(new Phrase(
                    " ", fontTitleBold))
                {
                    HorizontalAlignment = Element.ALIGN_RIGHT,
                    Padding = 3,
                };
                tableEncabezadoUnidades.AddCell(celValValor);
                //--------------------------------------------------------------------------------------
                PdfPCell celVacioUnidades = new PdfPCell(new Phrase(
                    " ", fontTitleBold))
                {
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    Padding = 3,
                    Colspan = 2,
                    Border = PdfPCell.NO_BORDER,
                };
                tableEncabezadoUnidades.AddCell(celVacioUnidades);

                PdfPCell celTextTotal = new PdfPCell(new Phrase(
                    "Total", fontTitleBold))
                {
                    HorizontalAlignment = Element.ALIGN_RIGHT,
                    Padding = 3,
                };
                tableEncabezadoUnidades.AddCell(celTextTotal);

                PdfPCell celValTotal = new PdfPCell(new Phrase(
                    $"{decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"]).ToString("N2")}",
                    FontFactory.GetFont(FontFactory.TIMES_ROMAN, 8, iTextSharp.text.Font.BOLD)))
                {
                    HorizontalAlignment = Element.ALIGN_RIGHT,
                    Padding = 3,
                };
                tableEncabezadoUnidades.AddCell(celValTotal);
                //------------------------------------------------------------------------------
                PdfPCell celTextTasa = new PdfPCell(new Phrase("Tasa",
                    FontFactory.GetFont(FontFactory.TIMES_BOLD, 8, iTextSharp.text.Font.UNDERLINE)))
                {
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    Padding = 3,
                    BorderWidthRight = PdfPCell.NO_BORDER,
                    BorderWidthBottom = PdfPCell.NO_BORDER,
                };
                tableEncabezadoUnidades.AddCell(celTextTasa);

                PdfPCell celTextImpuesto = new PdfPCell(new Phrase("Valor Impuesto",
                    FontFactory.GetFont(FontFactory.TIMES_BOLD, 8, iTextSharp.text.Font.UNDERLINE)))
                {
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    Padding = 3,
                    BorderWidthLeft = PdfPCell.NO_BORDER,
                    BorderWidthBottom = PdfPCell.NO_BORDER,
                };
                tableEncabezadoUnidades.AddCell(celTextImpuesto);
                tableEncabezadoUnidades.AddCell(celVacioUnidades);
                //--------------------------------------------------------------------------
                PdfPCell celValTasa = new PdfPCell(new Phrase("******",
                    FontFactory.GetFont(FontFactory.TIMES_BOLD, 8, iTextSharp.text.Font.UNDERLINE)))
                {
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    Padding = 3,
                    BorderWidthRight = PdfPCell.NO_BORDER,
                    BorderWidthTop = PdfPCell.NO_BORDER,
                };
                tableEncabezadoUnidades.AddCell(celValTasa);

                PdfPCell celValImpuesto = new PdfPCell(new Phrase("********",
                    FontFactory.GetFont(FontFactory.TIMES_BOLD, 8, iTextSharp.text.Font.UNDERLINE)))
                {
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    Padding = 3,
                    BorderWidthLeft = PdfPCell.NO_BORDER,
                    BorderWidthTop = PdfPCell.NO_BORDER,
                };
                tableEncabezadoUnidades.AddCell(celValImpuesto);
                tableEncabezadoUnidades.AddCell(celVacioUnidades);
                //PdfPTable tableValUnidades = new PdfPTable(dimTableEncabezadoUnidades);
                //tableValUnidades.WidthPercentage = 100;
                //decimal totalDescuento = 0, totalSubtotal = 0;
                //foreach (DataRow InvoiceLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
                //{
                //    if (!AddUnidadesDoricolor(InvoiceLine, ref tableValUnidades, fontCustom, DsInvoiceAR))
                //        return false;


                //    totalDescuento += decimal.Parse((string)InvoiceLine["DocDiscount"]);
                //    totalSubtotal += decimal.Parse((string)InvoiceLine["DspDocExtPrice"]);
                //}

                //int numCol = 20 - DsInvoiceAR.Tables["InvcDtl"].Rows.Count;
                //PdfPCell celEspacioToUnidades = new PdfPCell(new Phrase(" "));
                //celEspacioToUnidades.Border = PdfPCell.NO_BORDER;
                //celEspacioToUnidades.Colspan = 5;
                //for (int i = 0; i < numCol; i++)
                //    tableValUnidades.AddCell(celEspacioToUnidades);

                #endregion

                #region Pie
                PdfPTable tablePie = new PdfPTable(new float[] { 1, 1.5f, 1, 2, 2 });
                tablePie.WidthPercentage = 100;

                iTextSharp.text.Image QRPdf = iTextSharp.text.Image.GetInstance(QRInvoice, BaseColor.WHITE);

                PdfDiv divQR = new PdfDiv()
                {
                    Float = iTextSharp.text.pdf.PdfDiv.FloatType.RIGHT,
                    Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE,
                    BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID,
                    Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK,
                    Height = 85,
                    Width = 85,
                };
                divQR.AddElement(new Paragraph($"Cufe: {CUFE}", fontTitle) { Alignment = Element.ALIGN_RIGHT, });
                divQR.AddElement(QRPdf);

                PdfPCell celImgQR = new PdfPCell()
                {
                    Padding = 3,
                    PaddingLeft = 15,
                    HorizontalAlignment = iTextSharp.text.Element.ALIGN_RIGHT,
                    VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP,
                    Colspan = 5,
                    Border = PdfPCell.NO_BORDER,
                };
                celImgQR.AddElement(divQR);
                celImgQR.AddElement(new Paragraph("Representación gráfica factura electrónica", fontTitle) { Alignment = Element.ALIGN_CENTER, });
                tablePie.AddCell(celImgQR);

                PdfPCell celTextFac = new PdfPCell(new Phrase(
                    "", fontTitle))
                {
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    Padding = 3,
                    PaddingTop = 15,
                    Border = PdfPCell.NO_BORDER,
                    BorderWidthTop = 1,
                };
                tablePie.AddCell(celTextFac);

                PdfPCell celFechaCreacion = new PdfPCell(new Phrase(
                    DateTime.Now.ToString(), fontTitle))
                {
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    Padding = 3,
                    PaddingTop = 15,
                    Border = PdfPCell.NO_BORDER,
                    BorderWidthTop = 1,
                };
                tablePie.AddCell(celFechaCreacion);

                PdfPCell celCreador = new PdfPCell(new Phrase(
                   "", fontTitle))
                {
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    Padding = 3,
                    PaddingTop = 15,
                    Border = PdfPCell.NO_BORDER,
                    BorderWidthTop = 1,
                };
                tablePie.AddCell(celCreador);

                PdfPCell celNumPagina = new PdfPCell(new Phrase(
                    " ", fontTitle))
                {
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    Padding = 3,
                    PaddingTop = 15,
                    Border = PdfPCell.NO_BORDER,
                    BorderWidthTop = 1,
                };
                tablePie.AddCell(celNumPagina);

                System.Drawing.Image logoNewH = null;
                var requestLogoNewH = WebRequest.Create(RutaImg);

                using (var responseLogoNewH = requestLogoNewH.GetResponse())
                using (var streamLogoNewH = responseLogoNewH.GetResponseStream())
                {
                    logoNewH = Bitmap.FromStream(streamLogoNewH);
                }

                PdfDiv divLogoNewH = new PdfDiv();
                divLogoNewH.Float = iTextSharp.text.pdf.PdfDiv.FloatType.LEFT;
                divLogoNewH.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divLogoNewH.BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID;
                divLogoNewH.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                divLogoNewH.Height = 27;
                divLogoNewH.Width = 80;
                //divLogoNewH.BackgroundColor = BaseColor.LIGHT_GRAY;
                iTextSharp.text.Image ImgLogoNewH = iTextSharp.text.Image.GetInstance(logoNewH, BaseColor.WHITE);
                divLogoNewH.BackgroundImage = ImgLogoNewH;

                PdfPCell celLogoNewHotel = new PdfPCell()
                {
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    Padding = 3,
                    //PaddingTop = 15,
                    Border = PdfPCell.NO_BORDER,
                    BorderWidthTop = 1,
                };
                celLogoNewHotel.AddElement(divLogoNewH);
                tablePie.AddCell(celLogoNewHotel);
                #endregion

                document.Add(tableEncabezado);
                for (int i = 0; i < 6; i++)
                    document.Add(divEspacio);

                document.Add(tableInfoCliente);
                document.Add(divEspacio);
                document.Add(divEspacio);
                document.Add(tableEncabezadoMoneda);
                document.Add(divEspacio);
                document.Add(divEspacio);
                document.Add(divEspacio);
                document.Add(tableEncabezadoUnidades);
                document.Add(divEspacio);
                int cant = Math.Abs(20 - DsInvoiceAR.Tables["InvcDtl"].Rows.Count);
                for (int i = 0; i < cant; i++)
                    document.Add(divEspacio);

                document.Add(tablePie);

                //document.Add(tableTotales);

                /*PIE DE PAGINA*/
                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();
                RutaPdf = NomArchivo;
                return true;
            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.Message;
                return false;
            }
        }

        public bool FacturaNacionalBelfort(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {
            //NomArchivo = string.Empty;
            //string Ruta = HttpContext.Current.Server.MapPath("~/PDF/" + NIT + "/");
            //string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            //string RutaImg = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";
            //NomArchivo = NombreInvoice + ".pdf";

            NomArchivo = string.Empty;
            string Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            string RutaImg = $@"{AppDomain.CurrentDomain.BaseDirectory}\LOGO_Freddy .png";
            NomArchivo = NombreInvoice + ".pdf";

            try
            {
                #region head
                strError += "1\n";
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.

                Document document = new Document(iTextSharp.text.PageSize.LETTER);

                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;

                // step 3: we open the document     
                document.Open();

                PdfDiv divEspacio = new PdfDiv();
                divEspacio.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio.Height = 10;
                divEspacio.Width = 130;

                PdfDiv divEspacio2 = new PdfDiv();
                divEspacio2.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio2.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                divEspacio2.Height = 2;
                divEspacio2.Width = 130;

                //FUENTES----------------------------------------------------------------------------------------------------------
                iTextSharp.text.Font fontTitleBold = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 8, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustom = FontFactory.GetFont(FontFactory.HELVETICA, 6f, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustomI = FontFactory.GetFont(FontFactory.HELVETICA_OBLIQUE, 6f, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustomBold = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 6, iTextSharp.text.Font.NORMAL);
                //FUENTES---------------------------------------------------------------------------------------------------------
                RoundRectangle CelEventBorderRound = new RoundRectangle();

                strError += "2\n";

                #endregion
                strError += "2\n";
                #region "ENCABEZADO"

                System.Drawing.Image logo = null;
                var requestLogo = WebRequest.Create(RutaImg);

                PdfPTable tableEncabezado = new PdfPTable(new float[] { 1.5f, 3, 2.8f });
                tableEncabezado.WidthPercentage = 100;

                using (var responseLogo = requestLogo.GetResponse())
                using (var streamLogo = responseLogo.GetResponseStream())
                {
                    logo = Bitmap.FromStream(streamLogo);
                }

                PdfPCell celLogoEncabezado = new PdfPCell()
                {
                    Border = PdfPCell.NO_BORDER
                };

                PdfDiv divLogo = new PdfDiv();
                divLogo.Float = iTextSharp.text.pdf.PdfDiv.FloatType.LEFT;
                divLogo.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divLogo.BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID;
                divLogo.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                divLogo.Height = 90;
                divLogo.Width = 90;
                //divLogo.BackgroundColor = BaseColor.LIGHT_GRAY;
                iTextSharp.text.Image ImgLogo = iTextSharp.text.Image.GetInstance(logo, BaseColor.WHITE);
                divLogo.BackgroundImage = ImgLogo;
                celLogoEncabezado.AddElement(divLogo);
                tableEncabezado.AddCell(celLogoEncabezado);
                //---------------------------------------------------------------------------------------------------------
                PdfDiv divAcercade = new PdfDiv();
                divAcercade.Float = iTextSharp.text.pdf.PdfDiv.FloatType.RIGHT;
                divAcercade.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divAcercade.BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID;
                divAcercade.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divAcercade.BackgroundColor = BaseColor.LIGHT_GRAY;
                divAcercade.Width = 310;

                iTextSharp.text.Font fontAcercadeBold = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 8f, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontAcercade = FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL);
                //{(string)DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"]}
                Paragraph prgTituloAcercade1 = new Paragraph("ADMINISTRADORA HOTELERA MEDELLIN S.A",
                    FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 14f, iTextSharp.text.Font.NORMAL));
                prgTituloAcercade1.Alignment = Element.ALIGN_CENTER;

                Paragraph prgTituloAcercade2 = new Paragraph($"NIT " +
                    $"{(string)DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"]}-" +
                    $"{CalcularDigitoVerificacion((string)DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"])}\n" +
                    $"{(string)DsInvoiceAR.Tables["Company"].Rows[0]["Address1"]}" +
                    $"TEL {(string)DsInvoiceAR.Tables["Company"].Rows[0]["PhoneNum"]} -Fax " +
                    $"{(string)DsInvoiceAR.Tables["Company"].Rows[0]["FaxNum"]}\n" +
                    $"{(string)DsInvoiceAR.Tables["Company"].Rows[0]["City"]} - COLOMBIA\n" +
                    $"IVA - REGIMEN COMUN\n" +
                    $"OBRANDO POR CUENTA DEL PATRIMONIO AUTONOMO\n" +
                    $"FIDEICOMISO HOTEL BELFORT No.9 FIDUCOLOMBIA S.A.\n" +
                    $"FAVOR ABSTENERSE DE PRACTICAR RETENCIONES EN LA FUENTE\n" +
                    $"DE ACUERDO A LO SEŃALADO EN EL ART.102 DEL ESTATUTO TRIBUTARIO\n" +
                    $"FACTURA POR COMPUTADOR IMPRESA POR NEW HOTEL SOFTWARE NIT 830.087.331-8",
                    fontCustomBold);
                prgTituloAcercade2.Alignment = Element.ALIGN_CENTER;
                PdfPCell celInfoEnc1 = new PdfPCell()
                {
                    Border = 0,
                    Padding = 0,
                    HorizontalAlignment = Element.ALIGN_CENTER
                };
                celInfoEnc1.AddElement(prgTituloAcercade1);
                celInfoEnc1.AddElement(prgTituloAcercade2);
                tableEncabezado.AddCell(celInfoEnc1);
                //---------------------------------------------------------------------------------------------------------------------
                Paragraph prgNoFactura = new Paragraph($"FACTURA DE VENTA FE {(string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"]}",
                    FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 10f, iTextSharp.text.Font.NORMAL));
                prgNoFactura.Alignment = Element.ALIGN_CENTER;


                //$"Resolucion Dian No 18762009443410\n" +
                //    $"Autorizada de FE A 240001 al A 285000\n" +
                //    $"Fecha julio 30/2018 Vigencia 18 meses\n" +


                Paragraph prgNumeracionFactura = new Paragraph((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Character02"],
                    FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 6f, iTextSharp.text.Font.NORMAL));
                prgNumeracionFactura.Alignment = Element.ALIGN_CENTER;

                Paragraph prgResolucion = new Paragraph($"\nResolucion DIAN 18762009925253 DEL 30/08/2018\n" +
                                                        $"Numeración Autorizada A 121805 al A 150000\n", fontTitle);
                prgResolucion.Alignment = Element.ALIGN_CENTER;

                Paragraph prgFechaFactura = new Paragraph($"\nFecha Factura: " +
                    $"{DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).ToString("dd/MM/yyyy")}", fontTitle);
                prgFechaFactura.Alignment = Element.ALIGN_RIGHT;
                Paragraph prgFechaVencimientoFactura = new Paragraph($"Fecha Vencimiento: " +
                    $"{DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"]).ToString("dd/MM/yyyy")}",
                    fontTitle);
                prgFechaVencimientoFactura.Alignment = Element.ALIGN_RIGHT;

                Paragraph prgHoraFactura = new Paragraph($"Hora: " +
                    $"{DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).ToString("h:mm:ss:tt")}", fontTitle);
                prgHoraFactura.Alignment = Element.ALIGN_RIGHT;

                PdfPCell celNoFactura = new PdfPCell()
                {
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    VerticalAlignment = Element.ALIGN_TOP,
                    Padding = 0,
                    Border = PdfPCell.NO_BORDER
                };
                celNoFactura.AddElement(prgNoFactura);
                celNoFactura.AddElement(prgResolucion);
                //celNoFactura.AddElement(prgNumeracionFactura);
                celNoFactura.AddElement(prgFechaFactura);
                celNoFactura.AddElement(prgFechaVencimientoFactura);
                celNoFactura.AddElement(prgHoraFactura);
                tableEncabezado.AddCell(celNoFactura);
                #endregion
                strError += "3\n";
                #region Datos del Cliente
                PdfPTable tableInfoCliente = new PdfPTable(new float[6] { 1.65f, 0.5f, 0.5f, 0.5f, 0.3f, 0.6f });
                tableInfoCliente.WidthPercentage = 100;

                tableInfoCliente.AddCell(new PdfPCell(new Phrase($"Cufe: {CUFE}", fontTitle))
                {
                    Colspan = 3,
                    Border = PdfPCell.NO_BORDER,
                });

                tableInfoCliente.AddCell(new PdfPCell(new Phrase("Representación gráfica factura electrónica",
                    fontTitle))
                {
                    Colspan = 3,
                    Border = PdfPCell.NO_BORDER,
                    HorizontalAlignment = Element.ALIGN_RIGHT,
                });

                PdfPCell celCompania = new PdfPCell(new Phrase($"Compañia:\n" +
                    $"{(string)DsInvoiceAR.Tables["Customer"].Rows[0]["Name"]}", fontTitleBold))
                {
                    Padding = 2,
                    PaddingBottom = 5
                };
                tableInfoCliente.AddCell(celCompania);

                PdfPCell celNit = new PdfPCell(new Phrase($"C.C/Nit\n" +
                    $"{(string)DsInvoiceAR.Tables["Customer"].Rows[0]["ResaleID"]}", fontTitleBold))
                {
                    Colspan = 2,
                    Padding = 2,
                    PaddingBottom = 5
                };
                tableInfoCliente.AddCell(celNit);

                PdfPCell celNoReserva = new PdfPCell(new Phrase(
                    $"Reserva No.\n{(string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar02"]}", fontTitleBold))
                {
                    Padding = 2,
                    PaddingBottom = 5
                };
                tableInfoCliente.AddCell(celNoReserva);

                //----------------------------------------------------------------------------------------
                PdfPTable tableHabitacionNoches = new PdfPTable(new float[] { 2, 1 });

                PdfPCell celHabitacion = new PdfPCell(new Phrase(
                    $"Habitación/Salón\n{(string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar03"]}", fontTitleBold))
                {
                    Padding = 2,
                    PaddingBottom = 5
                };
                tableHabitacionNoches.AddCell(celHabitacion);

                PdfPCell celNoches = new PdfPCell(new Phrase(
                    $"Noches:\n{(string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number01"]}", fontTitleBold))
                {
                    Padding = 2,
                    PaddingBottom = 5,
                    HorizontalAlignment = Element.ALIGN_RIGHT
                };
                tableHabitacionNoches.AddCell(celNoches);

                PdfPCell celHabitacionNoche = new PdfPCell(tableHabitacionNoches) { Colspan = 2 };
                tableInfoCliente.AddCell(celHabitacionNoche);
                //-------------------------------------------------------------------------------------
                PdfPCell celDireccion = new PdfPCell(new Phrase("Dirección:\n" +
                    $"{(string)DsInvoiceAR.Tables["Customer"].Rows[0]["Address1"]}", fontTitleBold))
                {
                    Padding = 2,
                    PaddingBottom = 5
                };
                tableInfoCliente.AddCell(celDireccion);

                PdfPCell celCiudad = new PdfPCell(new Phrase("Ciudad:\n" +
                    $"{(string)DsInvoiceAR.Tables["Customer"].Rows[0]["City"]}", fontTitleBold))
                {
                    Padding = 2,
                    PaddingBottom = 5
                };
                tableInfoCliente.AddCell(celCiudad);

                var fechaLlegada = string.IsNullOrEmpty((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Date01"]) ? string.Empty :
                    DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Date01"]).ToString("dd/MM/yyyy");

                PdfPCell celFechaLlegada = new PdfPCell(new Phrase(
                    $"Fecha Llegada:\n" +
                    $"{fechaLlegada}",
                    fontTitleBold))
                {
                    Padding = 2,
                    PaddingBottom = 5
                };
                tableInfoCliente.AddCell(celFechaLlegada);

                var fechaSalida = string.IsNullOrEmpty((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Date02"]) ? string.Empty :
                    DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Date02"]).ToString("dd/MM/yyyy");

                PdfPCell celFechaSalida = new PdfPCell(new Phrase(
                    $"Fecha Salida:\n" +
                    $"{fechaSalida}", fontTitleBold))
                {
                    Padding = 2,
                    PaddingBottom = 5
                };
                tableInfoCliente.AddCell(celFechaSalida);

                PdfPCell celUsuario = new PdfPCell(new Phrase($"Usuario:\n" +
                    $"{(string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar04"]}", fontTitleBold))
                {
                    Padding = 2,
                    PaddingBottom = 5
                };
                tableInfoCliente.AddCell(celUsuario);

                PdfPCell celCentroCosto = new PdfPCell(new Phrase("Centro de Costo:\n" +
                    $"{(string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar05"]}", fontTitleBold))
                {
                    Padding = 2,
                    PaddingBottom = 5
                };
                tableInfoCliente.AddCell(celCentroCosto);
                //----------------------------------------------------------------------------------------
                PdfPCell celHuesped = new PdfPCell(new Phrase($"Huesped:\n" +
                    $"{(string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar06"]}", fontTitleBold))
                {
                    Padding = 2,
                    PaddingBottom = 5
                };
                tableInfoCliente.AddCell(celHuesped);

                PdfPCell celTelefono = new PdfPCell(new Phrase("Teléfono:\n" +
                $"{(string)DsInvoiceAR.Tables["Customer"].Rows[0]["PhoneNum"]}", fontTitleBold))
                {
                    Padding = 2,
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    PaddingBottom = 5,
                    Colspan = 3
                };
                tableInfoCliente.AddCell(celTelefono);

                PdfPTable tableNumPersonas = new PdfPTable(2);

                PdfPCell celTextNumPersonas = new PdfPCell(new Phrase("Número de Personas:", fontTitleBold))
                {
                    Padding = 0,
                    Border = 0,
                    Colspan = 2,
                };
                tableNumPersonas.AddCell(celTextNumPersonas);

                PdfPCell celNumAdultos = new PdfPCell(new Phrase($"Adultos: " +
                    $"{(string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number02"]}", fontTitleBold))
                {
                    Padding = 2,
                    Border = 0,
                };
                tableNumPersonas.AddCell(celNumAdultos);

                PdfPCell celTextNumNinos = new PdfPCell(new Phrase($"Niños: " +
                    $"{(string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number03"]}", fontTitleBold))
                {
                    Padding = 2,
                    Border = 0,
                };
                tableNumPersonas.AddCell(celTextNumNinos);

                PdfPCell celNumPersonas = new PdfPCell(tableNumPersonas)
                {
                    Padding = 2,
                    Colspan = 2
                };
                tableInfoCliente.AddCell(celNumPersonas);

                PdfPCell celEspacioInfoCliente = new PdfPCell()
                {
                    Colspan = 6
                };

                string InvcHeadDate03 = "0";
                if (Helpers.Compartido.VerificarExistenciaColumnas(DsInvoiceAR, "InvcHead", "Date03", 0))
                    InvcHeadDate03 = DsInvoiceAR.Tables["InvcHead"].Rows[0]["Date03"].ToString();

                string CustomerNumber01 = "0";
                if (Helpers.Compartido.VerificarExistenciaColumnas(DsInvoiceAR, "Customer", "Number01", 0))
                    CustomerNumber01 = DsInvoiceAR.Tables["Customer"].Rows[0]["Number01"].ToString();


                PdfPTable adisional = new PdfPTable(2);
                adisional.WidthPercentage = 100;
                PdfPCell ingreso = new PdfPCell(new Phrase("Fecha de Ingreso: " + InvcHeadDate03, fontTitleBold));
                adisional.AddCell(ingreso);
                PdfPCell n_documento = new PdfPCell(new Phrase("Numero de Documento: " + CustomerNumber01, fontTitleBold));
                adisional.AddCell(n_documento);
                celEspacioInfoCliente.AddElement(adisional);


                //PdfPTable adisional = new PdfPTable(2);
                //adisional.WidthPercentage = 100;
                //PdfPCell ingreso = new PdfPCell(new Phrase("Fecha de Ingreso: " + " no agregado", fontTitleBold));
                //adisional.AddCell(ingreso);
                //PdfPCell n_documento = new PdfPCell(new Phrase("Numero de Documento: " + " no agregado", fontTitleBold));
                //adisional.AddCell(n_documento);
                //celEspacioInfoCliente.AddElement(adisional);

                tableInfoCliente.AddCell(celEspacioInfoCliente);

                #endregion
                strError += "4\n";
                #region UNIDADES

                PdfPTable tableEncabezadoUnidades = new PdfPTable(new float[] { 2.5f, 1.2f, 1, 1, 1.3f, 0.8f, 1, 1 });
                tableEncabezadoUnidades.WidthPercentage = 100;
                var fontEncabezado = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 6, BaseColor.WHITE);

                PdfPCell celTextConcepto = new PdfPCell()
                {
                    //Border = PdfPCell.NO_BORDER,
                    VerticalAlignment = Element.ALIGN_CENTER,
                    Phrase = new Phrase("Concepto", fontEncabezado),
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    BorderWidthRight = PdfPCell.NO_BORDER,
                    BackgroundColor = BaseColor.LIGHT_GRAY,
                };
                tableEncabezadoUnidades.AddCell(celTextConcepto);

                PdfPCell celTextDetalle = new PdfPCell()
                {
                    //Border = PdfPCell.NO_BORDER,
                    VerticalAlignment = Element.ALIGN_CENTER,
                    Phrase = new Phrase("Detalle", fontEncabezado),
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    BorderWidthRight = PdfPCell.NO_BORDER,
                    BorderWidthLeft = PdfPCell.NO_BORDER,
                    BackgroundColor = BaseColor.LIGHT_GRAY,
                };
                tableEncabezadoUnidades.AddCell(celTextDetalle);

                PdfPCell celTextValor = new PdfPCell()
                {
                    //Border = PdfPCell.NO_BORDER,
                    VerticalAlignment = Element.ALIGN_CENTER,
                    Phrase = new Phrase("Valor", fontEncabezado),
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    BorderWidthRight = PdfPCell.NO_BORDER,
                    BorderWidthLeft = PdfPCell.NO_BORDER,
                    BackgroundColor = BaseColor.LIGHT_GRAY,
                };
                tableEncabezadoUnidades.AddCell(celTextValor);

                PdfPCell celTextImpuestos = new PdfPCell()
                {
                    //Border = PdfPCell.NO_BORDER,
                    VerticalAlignment = Element.ALIGN_CENTER,
                    Phrase = new Phrase("Impuestos", fontEncabezado),
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    BorderWidthRight = PdfPCell.NO_BORDER,
                    BorderWidthLeft = PdfPCell.NO_BORDER,
                    BackgroundColor = BaseColor.LIGHT_GRAY,
                };
                tableEncabezadoUnidades.AddCell(celTextImpuestos);

                PdfPCell celTextTotal = new PdfPCell()
                {
                    //Border = PdfPCell.NO_BORDER,
                    VerticalAlignment = Element.ALIGN_CENTER,
                    Phrase = new Phrase("Total", fontEncabezado),
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    BorderWidthRight = PdfPCell.NO_BORDER,
                    BorderWidthLeft = PdfPCell.NO_BORDER,
                    BackgroundColor = BaseColor.LIGHT_GRAY,
                };
                tableEncabezadoUnidades.AddCell(celTextTotal);

                PdfPCell celTextDescuento = new PdfPCell()
                {
                    //Border = PdfPCell.NO_BORDER,
                    VerticalAlignment = Element.ALIGN_CENTER,
                    Phrase = new Phrase("Descuento", fontEncabezado),
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    BorderWidthRight = PdfPCell.NO_BORDER,
                    BorderWidthLeft = PdfPCell.NO_BORDER,
                    BackgroundColor = BaseColor.LIGHT_GRAY,
                };
                tableEncabezadoUnidades.AddCell(celTextDescuento);

                PdfPCell celTextAbonos = new PdfPCell()
                {
                    //Border = PdfPCell.NO_BORDER,
                    VerticalAlignment = Element.ALIGN_CENTER,
                    Phrase = new Phrase("Abonos", fontEncabezado),
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    BorderWidthRight = PdfPCell.NO_BORDER,
                    BorderWidthLeft = PdfPCell.NO_BORDER,
                    BackgroundColor = BaseColor.LIGHT_GRAY,
                };
                tableEncabezadoUnidades.AddCell(celTextAbonos);

                PdfPCell celTextSaldo = new PdfPCell()
                {
                    //Border = PdfPCell.NO_BORDER,
                    VerticalAlignment = Element.ALIGN_CENTER,
                    Phrase = new Phrase("Saldo", fontEncabezado),
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    //BorderWidthRight = PdfPCell.NO_BORDER,
                    BorderWidthLeft = PdfPCell.NO_BORDER,
                    BackgroundColor = BaseColor.LIGHT_GRAY,
                };
                tableEncabezadoUnidades.AddCell(celTextSaldo);
                //--------------------------------------------------------------------------------------
                PdfPTable tableValUnidades = new PdfPTable(new float[] { 2.5f, 1.2f, 1, 1, 1.3f, 0.8f, 1, 1 });
                tableValUnidades.WidthPercentage = 100;
                decimal subTotalValor = 0, subTotalImpuesto = 0, subTotalTotal = 0, subTotalDescuento = 0, subTotalAbono = 0, subTotalSaldo = 0, totalIva16 = 0, totalIva19 = 0;

                strError += "4.1\n";

                foreach (DataRow InvoiceLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
                {
                    strError += "A\n";
                    var porcentageIva = GetPercentIdImpDIAN((string)InvoiceLine["InvoiceNum"], (string)InvoiceLine["InvoiceLine"], DsInvoiceAR.Tables["InvcTax"]).Trim(',', '.');

                    strError += porcentageIva.ToString() + "% B\n";
                    var impuesto =
                        GetValImpuestoByID((string)InvoiceLine["InvoiceNum"], (string)InvoiceLine["InvoiceLine"], "01", DsInvoiceAR) +
                        GetValImpuestoByID((string)InvoiceLine["InvoiceNum"], (string)InvoiceLine["InvoiceLine"], "02", DsInvoiceAR) +
                        GetValImpuestoByID((string)InvoiceLine["InvoiceNum"], (string)InvoiceLine["InvoiceLine"], "04", DsInvoiceAR);

                    var taxableAmt = GetValTaxableAmt((string)InvoiceLine["InvoiceNum"], (string)InvoiceLine["InvoiceLine"], "01", DsInvoiceAR) +
                            GetValTaxableAmt((string)InvoiceLine["InvoiceNum"], (string)InvoiceLine["InvoiceLine"], "02", DsInvoiceAR) +
                            GetValTaxableAmt((string)InvoiceLine["InvoiceNum"], (string)InvoiceLine["InvoiceLine"], "03", DsInvoiceAR) +
                            GetValTaxableAmt((string)InvoiceLine["InvoiceNum"], (string)InvoiceLine["InvoiceLine"], "04", DsInvoiceAR);

                    var total = taxableAmt + impuesto;
                    strError += "C\n";

                    strError += "D\n";
                    if (!AddUnidadesBelfort(InvoiceLine, ref tableValUnidades, fontCustom, DsInvoiceAR, impuesto))
                        return false;


                    strError += "E\n";
                    subTotalValor += taxableAmt;
                    subTotalImpuesto += impuesto;
                    subTotalAbono += decimal.Parse((string)InvoiceLine["ShortChar01"]);
                    subTotalTotal += total /*decimal.Parse((string)InvoiceLine["DspDocExtPrice"])*/;
                    subTotalDescuento += decimal.Parse((string)InvoiceLine["DocDiscount"]);


                    //subTotalSaldo += (string.IsNullOrEmpty((string)InvoiceLine["ShortChar02"])) ? 0 : decimal.Parse((string)InvoiceLine["ShortChar02"]);
                    strError += "F\n";
                }

                //if (((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar08"]).ToUpper()=="CREDITO")
                subTotalSaldo =
                    decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"]);
                //else
                //    subTotalSaldo = 0;

                strError += "4.2\n";

                PdfPCell celTextSubtotales = new PdfPCell(new Phrase("Sub Totales", fontTitleBold))
                {
                    Colspan = 2,
                    BorderWidthRight = PdfPCell.NO_BORDER,
                    PaddingLeft = 25,
                };
                tableValUnidades.AddCell(celTextSubtotales);

                PdfPCell celSubtotalValor = new PdfPCell(new Phrase(subTotalValor.ToString("N0"), fontTitleBold))
                {
                    BorderWidthRight = PdfPCell.NO_BORDER,
                    BorderWidthLeft = PdfPCell.NO_BORDER,
                    HorizontalAlignment = Element.ALIGN_RIGHT,
                };
                tableValUnidades.AddCell(celSubtotalValor);

                PdfPCell celSubtotalImpuesto = new PdfPCell(new Phrase(subTotalImpuesto.ToString("N0"), fontTitleBold))
                {
                    BorderWidthRight = PdfPCell.NO_BORDER,
                    BorderWidthLeft = PdfPCell.NO_BORDER,
                    HorizontalAlignment = Element.ALIGN_RIGHT,
                };
                tableValUnidades.AddCell(celSubtotalImpuesto);

                PdfPCell celSubtotalTotal = new PdfPCell(new Phrase((subTotalTotal).ToString("N0"), fontTitleBold))
                {
                    BorderWidthRight = PdfPCell.NO_BORDER,
                    BorderWidthLeft = PdfPCell.NO_BORDER,
                    HorizontalAlignment = Element.ALIGN_RIGHT,
                };
                tableValUnidades.AddCell(celSubtotalTotal);

                PdfPCell celSubtotalDescuento = new PdfPCell(new Phrase(subTotalDescuento.ToString("N0"), fontTitleBold))
                {
                    BorderWidthRight = PdfPCell.NO_BORDER,
                    BorderWidthLeft = PdfPCell.NO_BORDER,
                    HorizontalAlignment = Element.ALIGN_RIGHT,
                };
                tableValUnidades.AddCell(celSubtotalDescuento);

                PdfPCell celSubtotalAbono = new PdfPCell(new Phrase(subTotalAbono.ToString("N0"), fontTitleBold))
                {
                    BorderWidthRight = PdfPCell.NO_BORDER,
                    BorderWidthLeft = PdfPCell.NO_BORDER,
                    HorizontalAlignment = Element.ALIGN_RIGHT,
                };
                tableValUnidades.AddCell(celSubtotalAbono);

                PdfPCell celSubtotal = new PdfPCell(new Phrase(subTotalSaldo.ToString("N2"), fontTitleBold))
                {
                    BorderWidthLeft = PdfPCell.NO_BORDER,
                    HorizontalAlignment = Element.ALIGN_RIGHT,
                };
                tableValUnidades.AddCell(celSubtotal);
                //----------------------------------------------------------------------------------

                PdfPCell celRCargos = new PdfPCell()
                {
                    //Border = PdfPCell.NO_BORDER,
                    VerticalAlignment = Element.ALIGN_CENTER,
                    Phrase = new Phrase("Resumen Cargos", fontEncabezado),
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    BorderWidthRight = PdfPCell.NO_BORDER,
                    BackgroundColor = BaseColor.LIGHT_GRAY,
                    Colspan = 4,
                };
                tableValUnidades.AddCell(celRCargos);

                PdfPCell celRImpuestos = new PdfPCell()
                {
                    //Border = PdfPCell.NO_BORDER,
                    VerticalAlignment = Element.ALIGN_CENTER,
                    Phrase = new Phrase("Resumen Impuestos", fontEncabezado),
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    BorderWidthLeft = PdfPCell.NO_BORDER,
                    BackgroundColor = BaseColor.LIGHT_GRAY,
                    Colspan = 4,
                };
                tableValUnidades.AddCell(celRImpuestos);
                //------------------------------------------------------------------------------------
                Paragraph prgNoCargosG = new Paragraph("Total Cargos No Gravados", fontTitle);
                prgNoCargosG.Alignment = Element.ALIGN_RIGHT;

                Paragraph prgCargosG = new Paragraph("Total Cargos Gravados:", fontTitle);
                prgCargosG.Alignment = Element.ALIGN_RIGHT;

                Paragraph prgCargosG8 = new Paragraph("Total Cargos Gravados 8%:", fontTitle);
                prgCargosG8.Alignment = Element.ALIGN_RIGHT;

                Paragraph prgCargosG16 = new Paragraph(" ", fontTitle);
                prgCargosG16.Alignment = Element.ALIGN_RIGHT;

                Paragraph prgCargosG19 = new Paragraph("Total Cargos Gravados 19%:", fontTitle);
                prgCargosG19.Alignment = Element.ALIGN_RIGHT;

                Paragraph prgTotalImpuestoConsumo = new Paragraph("Total Imp. Consumo 8%:", fontTitle);
                prgTotalImpuestoConsumo.Alignment = Element.ALIGN_RIGHT;

                Paragraph prgIva16 = new Paragraph(" ", fontTitle);
                prgIva16.Alignment = Element.ALIGN_RIGHT;

                Paragraph prgIva19 = new Paragraph("Total IVA 19%:", fontTitle);
                prgIva19.Alignment = Element.ALIGN_RIGHT;

                Paragraph prgTotalIva = new Paragraph("Total IVA:", fontTitle);
                prgTotalIva.Alignment = Element.ALIGN_RIGHT;

                Paragraph prgTotalAbonosPagos = new Paragraph("Total Abonos y Pagos:", fontTitle);
                prgTotalAbonosPagos.Alignment = Element.ALIGN_RIGHT;

                //var textCxC = totalCxC > 0 ? "CxC" : string.Empty;

                Paragraph prgTotalCxC = new Paragraph($"Total:", fontTitle);
                prgTotalCxC.Alignment = Element.ALIGN_RIGHT;

                PdfPCell celValRCargos = new PdfPCell()
                {
                    //Border = PdfPCell.NO_BORDER,
                    VerticalAlignment = Element.ALIGN_CENTER,
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    BorderWidthRight = PdfPCell.NO_BORDER,
                    BorderWidthBottom = PdfPCell.NO_BORDER,
                    Colspan = 4,
                };
                celValRCargos.AddElement(prgNoCargosG);
                celValRCargos.AddElement(prgCargosG);
                celValRCargos.AddElement(prgCargosG8);
                celValRCargos.AddElement(prgCargosG16);
                celValRCargos.AddElement(prgCargosG19);
                celValRCargos.AddElement(prgTotalImpuestoConsumo);
                celValRCargos.AddElement(prgIva16);
                celValRCargos.AddElement(prgIva19);
                celValRCargos.AddElement(prgTotalIva);
                celValRCargos.AddElement(prgTotalAbonosPagos);
                celValRCargos.AddElement(prgTotalCxC);
                tableValUnidades.AddCell(celValRCargos);
                //------------------------------------------------------------------------------------
                Paragraph prgValNoCargosG = new Paragraph(
                    decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number04"]).ToString("N0")
                    , fontTitle);
                prgValNoCargosG.Alignment = Element.ALIGN_RIGHT;

                Paragraph prgValCargosG = new Paragraph(
                    decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number05"]).ToString("N0"), fontTitle);
                prgValCargosG.Alignment = Element.ALIGN_RIGHT;

                Paragraph prgValCargosG8 = new Paragraph(
                    decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number06"]).ToString("N0"), fontTitle);
                prgValCargosG8.Alignment = Element.ALIGN_RIGHT;

                Paragraph prgValCargosG16 = new Paragraph(" ", fontTitle);
                prgValCargosG16.Alignment = Element.ALIGN_RIGHT;

                Paragraph prgValCargosG19 = new Paragraph(
                    decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number07"]).ToString("N0"), fontTitle);
                prgValCargosG19.Alignment = Element.ALIGN_RIGHT;

                Paragraph prgValTotalImpuestoConsumo = new Paragraph((GetValImpuestoByID("02", DsInvoiceAR) +
                        GetValImpuestoByID("04", DsInvoiceAR)).ToString("N0"), fontTitle);
                prgValTotalImpuestoConsumo.Alignment = Element.ALIGN_RIGHT;

                Paragraph prgValIva16 = new Paragraph(" ", fontTitle);
                prgValIva16.Alignment = Element.ALIGN_RIGHT;

                Paragraph prgValIva19 = new Paragraph(GetValImpuestoByID("01", DsInvoiceAR).ToString("N0"), fontTitle);
                prgValIva19.Alignment = Element.ALIGN_RIGHT;

                Paragraph prgValTotalIva = new Paragraph(GetValImpuestoByID("01", DsInvoiceAR).ToString("N0"), fontTitle);
                prgValTotalIva.Alignment = Element.ALIGN_RIGHT;

                decimal totalAbonosPagos = 0;
                totalAbonosPagos =
                    decimal.TryParse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number10"], out totalAbonosPagos) ?
                    totalAbonosPagos : 0;

                if (totalAbonosPagos < 0)
                    totalAbonosPagos = 0;

                Paragraph prgValTotalAbonosPagos = new Paragraph(
                    totalAbonosPagos.ToString("N0"), fontTitle);
                prgValTotalAbonosPagos.Alignment = Element.ALIGN_RIGHT;

                decimal totalCxC = 0;
                totalCxC = Helpers.Compartido.VerificarExistenciaColumnas(DsInvoiceAR, "InvcHead", "Number14") ?
                    decimal.TryParse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number14"], out totalCxC) ? totalCxC : 0 : 0;

                Paragraph cell_total = new Paragraph(new Phrase("" + $"{decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"]).ToString("N2")}", fontTitle));
                cell_total.Alignment = Element.ALIGN_RIGHT;
                //else
                //    prgValTotalCxC.Add(new Phrase("",fontTitle));

                PdfPCell celValRImpuestos = new PdfPCell()
                {
                    //Border = PdfPCell.NO_BORDER,
                    VerticalAlignment = Element.ALIGN_CENTER,

                    HorizontalAlignment = Element.ALIGN_LEFT,
                    BorderWidthLeft = PdfPCell.NO_BORDER,
                    BorderWidthBottom = PdfPCell.NO_BORDER,
                    Colspan = 4,
                };
                celValRImpuestos.AddElement(prgValNoCargosG);
                celValRImpuestos.AddElement(prgValCargosG);
                celValRImpuestos.AddElement(prgValCargosG8);
                celValRImpuestos.AddElement(prgValCargosG16);
                celValRImpuestos.AddElement(prgValCargosG19);
                celValRImpuestos.AddElement(prgValTotalImpuestoConsumo);
                celValRImpuestos.AddElement(prgValIva16);
                celValRImpuestos.AddElement(prgValIva19);
                celValRImpuestos.AddElement(prgValTotalIva);
                celValRImpuestos.AddElement(prgValTotalAbonosPagos);
                celValRImpuestos.AddElement(cell_total);
                celValRImpuestos.PaddingRight = 80;
                tableValUnidades.AddCell(celValRImpuestos);

                PdfPCell celVacioValUnidades = new PdfPCell(new Phrase(" "))
                {
                    Colspan = 8,
                    BorderWidthBottom = PdfPCell.NO_BORDER,
                    BorderWidthTop = PdfPCell.NO_BORDER,
                };
                var cant = 5 - DsInvoiceAR.Tables["InvcDtl"].Rows.Count;
                for (int i = 0; i < cant; i++)
                    tableValUnidades.AddCell(celVacioValUnidades);
                //-----------------------------------------------------------------------------------
                PdfPTable tableEmitidaPor = new PdfPTable(new float[] { 2, 1, 1.3f, 1.1f, 1 });
                tableEmitidaPor.WidthPercentage = 100;

                iTextSharp.text.Image QRPdf = iTextSharp.text.Image.GetInstance(QRInvoice, BaseColor.WHITE);

                PdfDiv divQR = new PdfDiv()
                {
                    Float = iTextSharp.text.pdf.PdfDiv.FloatType.RIGHT,
                    Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE,
                    BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID,
                    Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK,
                    Height = 85,
                    Width = 85,
                };
                divQR.AddElement(QRPdf);

                PdfPCell celImgQR = new PdfPCell()
                {
                    Padding = 3,
                    PaddingLeft = 15,
                    HorizontalAlignment = iTextSharp.text.Element.ALIGN_RIGHT,
                    VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP,
                    Colspan = 5,
                    Border = PdfPCell.LEFT_BORDER | PdfPCell.RIGHT_BORDER | PdfPCell.BOTTOM_BORDER,
                };
                celImgQR.AddElement(divQR);
                tableEmitidaPor.AddCell(celImgQR);

                PdfPCell celTextEmitidaPor = new PdfPCell(new Phrase($"Emitida Por " +
                    $"{(string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["SalesRepName1"]}\n", fontTitle))
                {
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    VerticalAlignment = Element.ALIGN_TOP,
                    PaddingBottom = 10,
                };
                //celTextEmitidaPor.AddElement(prgSubRallado);
                tableEmitidaPor.AddCell(celTextEmitidaPor);

                PdfPCell celTextTotalCargos = new PdfPCell(new Phrase($"Total Cargos: " +
                    $"{decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number11"]).ToString("N2")}", fontCustomBold));
                tableEmitidaPor.AddCell(celTextTotalCargos);

                PdfPCell celTextTotalIva = new PdfPCell(new Phrase($"Total Iva: " + $"{decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DocTaxAmt"]).ToString("N2")}", fontCustomBold));
                tableEmitidaPor.AddCell(celTextTotalIva);

                PdfPCell celTextTotalAbonos = new PdfPCell(new Phrase($"Total Abonos: {subTotalAbono.ToString("N2")}", fontCustomBold));
                tableEmitidaPor.AddCell(celTextTotalAbonos);

                PdfPCell celTextSaldo2 = new PdfPCell(new Phrase($"Saldo: " +
                    $"{subTotalSaldo.ToString("N2")}",
                    fontCustomBold));
                tableEmitidaPor.AddCell(celTextSaldo2);
                #endregion
                strError += "5\n";
                #region Pie
                PdfPTable tableTraspasoParticulares = new PdfPTable(2);
                tableTraspasoParticulares.WidthPercentage = 100;

                Paragraph prgTraspasoParticulares = new Paragraph();

                PdfPCell celTextTParticulares = new PdfPCell(new Phrase(
                    "TRASPASO A PARTICULARES", fontEncabezado))
                {
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    Padding = 0,
                    PaddingBottom = 3,
                    Border = PdfPCell.NO_BORDER,
                    BackgroundColor = BaseColor.GRAY,
                };
                tableTraspasoParticulares.AddCell(celTextTParticulares);

                PdfPCell celVacio = new PdfPCell(new Phrase(" ", fontCustom))
                {
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    Padding = 0,
                    BorderWidthBottom = PdfPCell.NO_BORDER,
                    //BorderWidthTop=1,
                };
                tableTraspasoParticulares.AddCell(celVacio);
                //-------------------------------------------------------------------
                PdfPTable tableFirma = new PdfPTable(new float[] { 1.5f, 1 });
                tableFirma.WidthPercentage = 100;

                PdfPCell celTextPagare = new PdfPCell(new Phrase(
                   "Pagare incondicionalemente a la vista a la orden de" +
                   "Administradora Hotelera Medellin S.A.en la ciudad de" +
                   "Medellin la suma a cancelar aqui especificada", fontCustom))
                {
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    Padding = 3,
                    Border = PdfPCell.NO_BORDER,
                };
                tableFirma.AddCell(celTextPagare);

                PdfPCell celTextFirma = new PdfPCell(new Phrase(
                    "Firma:\n______________________________", fontCustom))
                {
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    Padding = 0,
                    VerticalAlignment = Element.ALIGN_BOTTOM,
                    Border = PdfPCell.NO_BORDER,
                };
                tableFirma.AddCell(celTextFirma);

                PdfPCell celTextValorPagare = new PdfPCell(new Phrase(
                   "El Valor de este pagare junto con los intereses a la tasa" +
                   "maxima permitida por la ley", fontCustom))
                {
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    Padding = 3,
                    Border = PdfPCell.NO_BORDER,
                };
                tableFirma.AddCell(celTextValorPagare);

                PdfPCell celTextCC = new PdfPCell(new Phrase(
                    "C.C", fontCustom))
                {
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    Padding = 3,
                    Border = PdfPCell.NO_BORDER,
                };
                tableFirma.AddCell(celTextCC);

                PdfPCell celFirma = new PdfPCell(tableFirma);
                tableTraspasoParticulares.AddCell(celFirma);
                //-------------------------------------------------------------------
                PdfPTable tableAceptada = new PdfPTable(1);
                tableAceptada.WidthPercentage = 100;

                PdfPCell celTextAceptada = new PdfPCell(new Phrase(
                    "ACEPTADA", FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 12, iTextSharp.text.Font.NORMAL)))
                {
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    Padding = 3,
                    BorderWidthTop = PdfPCell.NO_BORDER,
                };
                tableAceptada.AddCell(celTextAceptada);

                PdfPCell celVacioAceptada = new PdfPCell(new Phrase(
                     " ", FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 12, iTextSharp.text.Font.NORMAL)))
                {
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    Padding = 10,
                };
                tableAceptada.AddCell(celVacioAceptada);
                //tableAceptada.AddCell(celVacioAceptada);

                PdfPCell celAceptada = new PdfPCell(tableAceptada);
                celAceptada.BorderWidthTop = PdfPCell.NO_BORDER;
                tableTraspasoParticulares.AddCell(celAceptada);

                PdfPCell lastCelText = new PdfPCell(new Phrase(
                   $"Esta factura de venta se asimilara en sus efectos legales a una letra de cambio, segun ley 1231 de julio de 2008. Condiciones de Pago: Contado \n" +
                   $"Esta factura es de pago inmediato de lo contrario causaria interes de mora a la tasa vigente \n" +
                   $"Consignar a la cuenta corriente Bancolombia 005234333 06 Convenio 26794 a nombre de Administradora Hotelera Medellin S.A  \n",
                   fontCustomBold))
                {
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    Padding = 0,
                    PaddingBottom = 2,
                    Colspan = 2,
                    //Border = PdfPCell.NO_BORDER,
                };
                tableTraspasoParticulares.AddCell(lastCelText);
                #endregion
                strError += "6\n";
                #region Exit 

                document.Add(tableEncabezado);
                document.Add(divEspacio);
                document.Add(tableInfoCliente);
                document.Add(tableEncabezadoUnidades);
                document.Add(tableValUnidades);
                document.Add(tableEmitidaPor);
                document.Add(tableTraspasoParticulares);
                //document.Add(divEspacio);
                //document.Add(tableEncabezadoUnidades);
                //document.Add(divEspacio);

                /*PIE DE PAGINA*/
                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();

                RutaPdf = NomArchivo;
                return true;
                #endregion
            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.Message;
                return false;
            }
        }

        public bool FacturaNotaCreditoBelfort(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {
            NomArchivo = string.Empty;
            string Ruta = HttpContext.Current.Server.MapPath("~/PDF/" + NIT + "/");
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            string RutaImg = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";
            NomArchivo = NombreInvoice + ".pdf";

            try
            {
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.

                Document document = new Document(iTextSharp.text.PageSize.LETTER);

                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;

                // step 3: we open the document     
                document.Open();

                PdfDiv divEspacio = new PdfDiv();
                divEspacio.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio.Height = 10;
                divEspacio.Width = 130;

                PdfDiv divEspacio2 = new PdfDiv();
                divEspacio2.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio2.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                divEspacio2.Height = 2;
                divEspacio2.Width = 130;

                //FUENTES----------------------------------------------------------------------------------------------------------
                iTextSharp.text.Font fontTitleBold = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 8, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustom = FontFactory.GetFont(FontFactory.HELVETICA, 6f, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustomI = FontFactory.GetFont(FontFactory.HELVETICA_OBLIQUE, 6f, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustomBold = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 6, iTextSharp.text.Font.NORMAL);
                //FUENTES---------------------------------------------------------------------------------------------------------
                DottedCell CelEventBorderDotted = new DottedCell(PdfPCell.BOX);

                #region ENCABEZADO
                System.Drawing.Image logo = null;
                var requestLogo = WebRequest.Create(RutaImg);

                PdfPTable tableEncabezado = new PdfPTable(new float[] { 3f, 0.047f, 2.36f });
                tableEncabezado.WidthPercentage = 100;

                PdfPTable tableLogo = new PdfPTable(new float[] { 0.76f, 1.5f });
                tableLogo.WidthPercentage = 100;

                using (var responseLogo = requestLogo.GetResponse())
                using (var streamLogo = responseLogo.GetResponseStream())
                {
                    logo = Bitmap.FromStream(streamLogo);
                }

                PdfPCell celLogoEncabezado = new PdfPCell()
                {
                    Border = PdfPCell.NO_BORDER,
                };

                PdfDiv divLogo = new PdfDiv();
                divLogo.Float = iTextSharp.text.pdf.PdfDiv.FloatType.LEFT;
                divLogo.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divLogo.BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID;
                divLogo.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                divLogo.Height = 85;
                divLogo.Width = 85;
                //divLogo.BackgroundColor = BaseColor.LIGHT_GRAY;
                iTextSharp.text.Image ImgLogo = iTextSharp.text.Image.GetInstance(logo, BaseColor.WHITE);
                divLogo.BackgroundImage = ImgLogo;
                celLogoEncabezado.AddElement(divLogo);
                tableLogo.AddCell(celLogoEncabezado);

                Paragraph prgNombreEmpresa = new Paragraph((string)DsInvoiceAR.Tables["Company"].Rows[0]["Name"],
                    FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 14, iTextSharp.text.Font.NORMAL));
                prgNombreEmpresa.Alignment = Element.ALIGN_CENTER;

                Paragraph prgNitEmpresaEncabezado = new Paragraph($"Nit: {(string)DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"]}", fontTitleBold);
                prgNitEmpresaEncabezado.Alignment = Element.ALIGN_CENTER;

                PdfPCell celTituloEmpresaNit = new PdfPCell()
                {
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    Border = PdfPCell.NO_BORDER,
                };
                celTituloEmpresaNit.AddElement(prgNombreEmpresa);
                celTituloEmpresaNit.AddElement(prgNitEmpresaEncabezado);
                tableLogo.AddCell(celTituloEmpresaNit);

                PdfPCell celLogo = new PdfPCell(tableLogo)
                {
                    Border = PdfPCell.NO_BORDER,
                    CellEvent = CelEventBorderDotted,
                };
                tableEncabezado.AddCell(celLogo);
                //---------------------------------------------------------------------------------------------------------
                PdfPCell celEspacioEncabezado = new PdfPCell()
                {
                    Border = PdfPCell.NO_BORDER,
                };
                tableEncabezado.AddCell(celEspacioEncabezado);
                //---------------------------------------------------------------------------------------------------------
                Paragraph prgDatosCliente = new Paragraph($"Exmo(s) Sr.(s)\n{ (string)DsInvoiceAR.Tables["Customer"].Rows[0]["Name"] }\n" +
                    $"{(string)DsInvoiceAR.Tables["Customer"].Rows[0]["Address1"]}\n\n\n" +
                    $"xxxxx             {(string)DsInvoiceAR.Tables["Customer"].Rows[0]["City"]}\n" +
                    $"COLOMBIA\n" +
                    $"N° Fiscal             {(string)DsInvoiceAR.Tables["Customer"].Rows[0]["ResaleID"]}",
                    FontFactory.GetFont(FontFactory.TIMES, 8, iTextSharp.text.Font.NORMAL));

                PdfPCell celDatosCliente = new PdfPCell()
                {
                    CellEvent = CelEventBorderDotted,
                    VerticalAlignment = Element.ALIGN_TOP,
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    Border = PdfPCell.NO_BORDER
                };
                celDatosCliente.AddElement(prgDatosCliente);
                tableEncabezado.AddCell(celDatosCliente);
                #endregion

                #region Datos del Cliente
                PdfPTable tableInfoCliente = new PdfPTable(new float[6] { 1, 1.8f, 1, 1, 1, 1 });
                tableInfoCliente.WidthPercentage = 100;

                PdfPCell celTextNotaCredito = new PdfPCell(new Phrase("NOTA CREDITO",
                    FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 18, iTextSharp.text.Font.NORMAL)))
                {
                    Colspan = 6,
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    Border = PdfPCell.NO_BORDER,
                    PaddingBottom = 20,
                };
                tableInfoCliente.AddCell(celTextNotaCredito);

                PdfPCell celTextRecibo = new PdfPCell(new Phrase("Recibo", fontTitle))
                {
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    Border = PdfPCell.NO_BORDER,
                };
                tableInfoCliente.AddCell(celTextRecibo);

                PdfPCell celValRecibo = new PdfPCell(new Phrase(" ", fontTitleBold))
                {
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    Border = PdfPCell.NO_BORDER,
                };
                tableInfoCliente.AddCell(celValRecibo);

                PdfPCell celTextFechaEmision = new PdfPCell(new Phrase("Fecha Emision", fontTitle))
                {
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    Border = PdfPCell.NO_BORDER,
                };
                tableInfoCliente.AddCell(celTextFechaEmision);

                PdfPCell celValFechaEmision = new PdfPCell(new Phrase(
                    DateTime.Now.ToString(),
                    fontTitleBold))
                {
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    Border = PdfPCell.NO_BORDER,
                };
                tableInfoCliente.AddCell(celValFechaEmision);


                PdfPCell celTextValor = new PdfPCell(new Phrase("Valor", fontTitle))
                {
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    Border = PdfPCell.NO_BORDER,
                };
                tableInfoCliente.AddCell(celTextValor);


                PdfPCell celValValor = new PdfPCell(new Phrase(
                    decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"]).ToString("N2"),
                    fontTitleBold))
                {
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    Border = PdfPCell.NO_BORDER,
                };
                tableInfoCliente.AddCell(celValValor);
                //-------------------------------------------------------------------------------------------
                PdfPCell celTextRecibimosDe = new PdfPCell(new Phrase("Recibimos de", fontTitle))
                {
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    Padding = 3,
                    //BorderWidthRight = PdfPCell.NO_BORDER,
                    //Border = PdfPCell.NO_BORDER,
                    Colspan = 1,
                };
                tableInfoCliente.AddCell(celTextRecibimosDe);

                PdfPCell celValRecibimosDe = new PdfPCell(new Phrase("{}", fontTitle))
                {
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    Padding = 3,
                    //BorderWidthLeft=PdfPCell.NO_BORDER,
                    //Border = PdfPCell.NO_BORDER,
                    Colspan = 5,
                };
                tableInfoCliente.AddCell(celValRecibimosDe);

                PdfPCell celTextTipoMv = new PdfPCell(new Phrase("Tipo Mv.", fontTitle))
                {
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    Padding = 3,
                    //BorderWidthRight = PdfPCell.NO_BORDER,
                    Border = PdfPCell.NO_BORDER,
                    BorderWidthLeft = 0.1f,
                    Colspan = 1,
                };
                tableInfoCliente.AddCell(celTextTipoMv);

                PdfPCell celValTipoMv = new PdfPCell(new Phrase("NOTA CREDITO", fontTitle))
                {
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    Padding = 3,
                    //BorderWidthRight = PdfPCell.NO_BORDER,
                    Border = PdfPCell.NO_BORDER,
                    Colspan = 1,
                };
                tableInfoCliente.AddCell(celValTipoMv);

                PdfPCell celTextCantDe = new PdfPCell(new Phrase("Cantidad de", fontTitle))
                {
                    HorizontalAlignment = Element.ALIGN_RIGHT,
                    Padding = 3,
                    //BorderWidthRight = PdfPCell.NO_BORDER,
                    Border = PdfPCell.NO_BORDER,
                    Colspan = 1,
                };
                tableInfoCliente.AddCell(celTextCantDe);

                PdfPCell celValCantDe = new PdfPCell(new Phrase(" ", fontTitle))
                {
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    Padding = 3,
                    //BorderWidthRight = PdfPCell.NO_BORDER,
                    Border = PdfPCell.NO_BORDER,
                    Colspan = 1,
                };
                tableInfoCliente.AddCell(celValCantDe);

                PdfPCell celTextCambio = new PdfPCell(new Phrase("Cambio", fontTitle))
                {
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    Padding = 3,
                    //BorderWidthRight = PdfPCell.NO_BORDER,
                    Border = PdfPCell.NO_BORDER,
                    Colspan = 1,
                };
                tableInfoCliente.AddCell(celTextCambio);

                PdfPCell celValCambio = new PdfPCell(new Phrase(" ", fontTitle))
                {
                    HorizontalAlignment = Element.ALIGN_RIGHT,
                    Padding = 3,
                    //BorderWidthRight = PdfPCell.NO_BORDER,
                    Border = PdfPCell.NO_BORDER,
                    BorderWidthRight = 0.1f,
                    Colspan = 1,
                };
                tableInfoCliente.AddCell(celValCambio);

                PdfPCell celVacioInfoCliente = new PdfPCell(new Phrase(" "))
                {
                    HorizontalAlignment = Element.ALIGN_RIGHT,
                    Padding = 3,
                    //BorderWidthRight = PdfPCell.NO_BORDER,
                    Border = PdfPCell.NO_BORDER,
                    Colspan = 2,
                };
                celVacioInfoCliente.BorderWidthLeft = 0.1f;
                tableInfoCliente.AddCell(celVacioInfoCliente);
                celVacioInfoCliente.Border = PdfPCell.NO_BORDER;

                PdfPCell celTextPendiente = new PdfPCell(new Phrase("Pendiente", fontTitle))
                {
                    HorizontalAlignment = Element.ALIGN_RIGHT,
                    Padding = 0,
                    Border = PdfPCell.NO_BORDER,

                };
                tableInfoCliente.AddCell(celTextPendiente);

                PdfPCell celValPendiente = new PdfPCell(new Phrase(" ", fontTitle))
                {
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    Padding = 3,
                    //BorderWidthRight = PdfPCell.NO_BORDER,
                    Border = PdfPCell.NO_BORDER,
                };
                tableInfoCliente.AddCell(celValPendiente);
                celVacioInfoCliente.BorderWidthRight = 0.1f;
                tableInfoCliente.AddCell(celVacioInfoCliente);
                celVacioInfoCliente.Border = PdfPCell.NO_BORDER;

                PdfPCell celTextDescripcionIbfoCliente = new PdfPCell(new Phrase("Descripción", fontTitle))
                {
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    Padding = 3.5f,
                    //BorderWidthRight = PdfPCell.NO_BORDER,
                    Border = PdfPCell.NO_BORDER,
                    BorderWidthLeft = 0.1f,
                };
                tableInfoCliente.AddCell(celTextDescripcionIbfoCliente);

                PdfPCell celValDescripcionIbfoCliente = new PdfPCell(new Phrase(" ", fontTitle))
                {
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    Padding = 3.5f,
                    //BorderWidthRight = PdfPCell.NO_BORDER,
                    Border = PdfPCell.NO_BORDER,
                };
                tableInfoCliente.AddCell(celValDescripcionIbfoCliente);
                tableInfoCliente.AddCell(celVacioInfoCliente);
                celVacioInfoCliente.Border = PdfPCell.RIGHT_BORDER;
                tableInfoCliente.AddCell(celVacioInfoCliente);
                celVacioInfoCliente.Border = PdfPCell.NO_BORDER;
                //--------------------------------------------------------------------
                celVacioInfoCliente.Border = PdfPCell.TOP_BORDER;
                tableInfoCliente.AddCell(celVacioInfoCliente);
                tableInfoCliente.AddCell(celVacioInfoCliente);
                celVacioInfoCliente.Border = PdfPCell.NO_BORDER;

                PdfPTable tableDescPagoValorCont = new PdfPTable(new float[] { 2, 1.5f });
                PdfPCell celTextDescPagoValorCont = new PdfPCell(new Phrase("Descripcion del pago",
                    FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 6, iTextSharp.text.Font.UNDERLINE)))
                {
                    Border = PdfPCell.TOP_BORDER | PdfPCell.LEFT_BORDER,
                    Padding = 2,
                    //BorderWidthRight=PdfPCell.NO_BORDER,
                };
                tableDescPagoValorCont.AddCell(celTextDescPagoValorCont);

                PdfPCell celTextDescPagoValor = new PdfPCell(new Phrase("Valor",
                    FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 6, iTextSharp.text.Font.UNDERLINE)))
                {
                    Border = PdfPCell.TOP_BORDER | PdfPCell.RIGHT_BORDER,
                    //BorderWidthLeft=PdfPCell.NO_BORDER,
                    Padding = 2,
                    HorizontalAlignment = Element.ALIGN_CENTER,
                };
                tableDescPagoValorCont.AddCell(celTextDescPagoValor);

                PdfPCell celValDescPagoValorCont = new PdfPCell(new Phrase(" ",
                    fontCustom))
                {
                    Border = PdfPCell.LEFT_BORDER,
                    Padding = 2,
                    //BorderWidthRight = PdfPCell.NO_BORDER,
                };
                tableDescPagoValorCont.AddCell(celValDescPagoValorCont);

                PdfPCell celValtDescPagoValor = new PdfPCell(new Phrase(
                    decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"]).ToString("N2"),
                    fontCustom))
                {
                    Border = PdfPCell.RIGHT_BORDER,
                    //BorderWidthLeft = PdfPCell.NO_BORDER,
                    Padding = 2,
                    HorizontalAlignment = Element.ALIGN_RIGHT,
                };
                tableDescPagoValorCont.AddCell(celValtDescPagoValor);

                PdfPCell celValtDescPagoValorTotal = new PdfPCell(new Phrase(
                    decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"]).ToString("N2"),
                    fontCustomBold))
                {
                    Border = PdfPCell.BOTTOM_BORDER | PdfPCell.RIGHT_BORDER | PdfPCell.LEFT_BORDER,
                    //BorderWidthLeft = PdfPCell.NO_BORDER,
                    Padding = 2,
                    Colspan = 2,
                    HorizontalAlignment = Element.ALIGN_RIGHT,
                };
                tableDescPagoValorCont.AddCell(celValtDescPagoValorTotal);

                PdfPTable tableDescPagoValor = new PdfPTable(1);

                PdfPCell celDescPagoValorCont = new PdfPCell(tableDescPagoValorCont)
                {
                    Padding = 2
                };
                tableDescPagoValor.AddCell(celDescPagoValorCont);

                PdfPCell celDescPagoValor = new PdfPCell(tableDescPagoValor)
                {
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    Padding = 2,
                    //BorderWidthRight = PdfPCell.NO_BORDER,
                    //Border = iTextSharp.text.Rectangle.TOP_BORDER | iTextSharp.text.Rectangle.BOTTOM_BORDER,
                    Border = PdfPCell.TOP_BORDER,
                    Colspan = 2
                };
                tableInfoCliente.AddCell(celDescPagoValor);
                #endregion

                #region UNIDADES
                PdfPTable tableEncabezadoUnidades = new PdfPTable(new float[] { 1, 0.72f, 1.75f, 1, 1, 1, 1, 1 });
                tableEncabezadoUnidades.WidthPercentage = 100;

                var fontEncabezadoUnidades = FontFactory.GetFont(FontFactory.TIMES_ROMAN, 7.5f, iTextSharp.text.Font.UNDERLINE);

                PdfPCell celTextFecha = new PdfPCell(new Phrase("Fecha", fontEncabezadoUnidades))
                {
                    Border = 0
                };
                tableEncabezadoUnidades.AddCell(celTextFecha);

                PdfPCell celTextTipo = new PdfPCell(new Phrase("Tipo", fontEncabezadoUnidades))
                {
                    Border = 0
                };
                tableEncabezadoUnidades.AddCell(celTextTipo);

                PdfPCell celTextDescripcion = new PdfPCell(new Phrase("Descripción", fontEncabezadoUnidades))
                {
                    Border = 0
                };
                tableEncabezadoUnidades.AddCell(celTextDescripcion);

                PdfPCell celTextDocumento = new PdfPCell(new Phrase("Documento", fontEncabezadoUnidades))
                {
                    Border = 0
                };
                tableEncabezadoUnidades.AddCell(celTextDocumento);

                PdfPCell celTextValOriginal = new PdfPCell(new Phrase("Valor Original", fontEncabezadoUnidades))
                {
                    Border = 0
                };
                tableEncabezadoUnidades.AddCell(celTextValOriginal);

                PdfPCell celTextCambioEncabezado = new PdfPCell(new Phrase("Cambio", fontEncabezadoUnidades))
                {
                    Border = 0
                };
                tableEncabezadoUnidades.AddCell(celTextCambioEncabezado);

                PdfPCell celTextValPagado = new PdfPCell(new Phrase("Valor pagado", fontEncabezadoUnidades))
                {
                    Border = 0
                };
                tableEncabezadoUnidades.AddCell(celTextValPagado);

                PdfPCell celTextValPendiente = new PdfPCell(new Phrase("Valor Pendiente", fontEncabezadoUnidades))
                {
                    Border = 0
                };
                tableEncabezadoUnidades.AddCell(celTextValPendiente);
                //---------------------------------------------------------------------------------------------
                PdfPTable tableValUnidades = new PdfPTable(new float[] { 1, 0.72f, 1.75f, 1, 1, 1, 1, 1 });
                tableValUnidades.WidthPercentage = 100;
                foreach (DataRow InvoiceLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
                {
                    if (!AddUnidadesNCBelfort(InvoiceLine, ref tableValUnidades, fontCustom, DsInvoiceAR))
                        return false;
                }

                PdfPCell celVacioValUnidades = new PdfPCell(new Phrase(" "))
                {
                    Border = 0,
                    Colspan = 6
                };
                tableValUnidades.AddCell(celVacioValUnidades);

                PdfPCell celValValorPagado = new PdfPCell(new Phrase(" ", fontCustomBold))
                {
                    Border = 0
                };
                tableValUnidades.AddCell(celValValorPagado);
                celVacioValUnidades.Colspan = 1;
                tableValUnidades.AddCell(celVacioValUnidades);
                #endregion

                #region FIRMA Y PIE
                iTextSharp.text.Font fontFirma = FontFactory.GetFont(FontFactory.TIMES_ROMAN, 8, iTextSharp.text.Font.NORMAL);
                PdfPTable tableFirma = new PdfPTable(1);
                tableFirma.WidthPercentage = 63;
                tableFirma.HorizontalAlignment = Element.ALIGN_RIGHT;

                PdfPCell celFirma = new PdfPCell(new Phrase("Firma", fontFirma))
                {
                    Border = PdfPCell.BOTTOM_BORDER,
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    Padding = 2,
                };
                tableFirma.AddCell(celFirma);

                PdfPCell celFirmaSub = new PdfPCell(new Phrase($"BELFORT MEDELLIN\nFecha de Impresión {DateTime.Now} Procesado por computadora", fontFirma))
                {
                    Border = PdfPCell.NO_BORDER,
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    Padding = 2,
                };
                tableFirma.AddCell(celFirmaSub);
                //-------PIE----------------------------------------------------------
                PdfPTable tablePie = new PdfPTable(new float[] { 1, 1.5f, 1, 2, 2 });
                tablePie.WidthPercentage = 100;

                iTextSharp.text.Image QRPdf = iTextSharp.text.Image.GetInstance(QRInvoice, BaseColor.WHITE);

                PdfDiv divQR = new PdfDiv()
                {
                    Float = iTextSharp.text.pdf.PdfDiv.FloatType.RIGHT,
                    Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE,
                    BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID,
                    Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK,
                    Height = 85,
                    Width = 85,
                };
                divQR.AddElement(QRPdf);

                PdfPCell celImgQR = new PdfPCell()
                {
                    Padding = 3,
                    PaddingLeft = 15,
                    HorizontalAlignment = iTextSharp.text.Element.ALIGN_RIGHT,
                    VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP,
                    Colspan = 5,
                    Border = PdfPCell.NO_BORDER,
                };
                celImgQR.AddElement(new Paragraph($"Cufe: {CUFE}", fontTitle) { Alignment = Element.ALIGN_RIGHT, });
                celImgQR.AddElement(divQR);
                celImgQR.AddElement(new Paragraph("Representación gráfica factura electrónica", fontTitle) { Alignment = Element.ALIGN_CENTER, });
                tablePie.AddCell(celImgQR);

                PdfPCell celTextFac = new PdfPCell(new Phrase(
                    "", fontTitle))
                {
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    Padding = 3,
                    PaddingTop = 15,
                    Border = PdfPCell.NO_BORDER,
                    BorderWidthTop = 1,
                };
                tablePie.AddCell(celTextFac);

                PdfPCell celFechaCreacion = new PdfPCell(new Phrase(
                    DateTime.Now.ToString(), fontTitle))
                {
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    Padding = 3,
                    PaddingTop = 15,
                    Border = PdfPCell.NO_BORDER,
                    BorderWidthTop = 1,
                };
                tablePie.AddCell(celFechaCreacion);

                PdfPCell celCreador = new PdfPCell(new Phrase(
                   "", fontTitle))
                {
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    Padding = 3,
                    PaddingTop = 15,
                    Border = PdfPCell.NO_BORDER,
                    BorderWidthTop = 1,
                };
                tablePie.AddCell(celCreador);

                PdfPCell celNumPagina = new PdfPCell(new Phrase(
                    " ", fontTitle))
                {
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    Padding = 3,
                    PaddingTop = 15,
                    Border = PdfPCell.NO_BORDER,
                    BorderWidthTop = 1,
                };
                tablePie.AddCell(celNumPagina);

                System.Drawing.Image logoNewH = null;
                var requestLogoNewH = WebRequest.Create(RutaImg);

                using (var responseLogoNewH = requestLogoNewH.GetResponse())
                using (var streamLogoNewH = responseLogoNewH.GetResponseStream())
                {
                    logoNewH = Bitmap.FromStream(streamLogoNewH);
                }

                PdfDiv divLogoNewH = new PdfDiv();
                divLogoNewH.Float = iTextSharp.text.pdf.PdfDiv.FloatType.LEFT;
                divLogoNewH.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divLogoNewH.BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID;
                divLogoNewH.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                divLogoNewH.Height = 27;
                divLogoNewH.Width = 80;
                //divLogoNewH.BackgroundColor = BaseColor.LIGHT_GRAY;
                iTextSharp.text.Image ImgLogoNewH = iTextSharp.text.Image.GetInstance(logoNewH, BaseColor.WHITE);
                divLogoNewH.BackgroundImage = ImgLogoNewH;

                PdfPCell celLogoNewHotel = new PdfPCell()
                {
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    Padding = 3,
                    //PaddingTop = 15,
                    Border = PdfPCell.NO_BORDER,
                    BorderWidthTop = 1,
                };
                celLogoNewHotel.AddElement(divLogoNewH);
                tablePie.AddCell(celLogoNewHotel);
                #endregion

                document.Add(tableEncabezado);
                document.Add(divEspacio);
                document.Add(tableInfoCliente);
                document.Add(divEspacio);
                document.Add(tableEncabezadoUnidades);
                document.Add(tableValUnidades);
                document.Add(divEspacio);
                document.Add(divEspacio);
                document.Add(divEspacio);
                document.Add(divEspacio);
                document.Add(tableFirma);
                int cant = 18 - DsInvoiceAR.Tables["InvcDtl"].Rows.Count;
                for (int i = 0; i < cant; i++)
                    document.Add(divEspacio);
                document.Add(tablePie);

                /*PIE DE PAGINA*/
                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();
                RutaPdf = NomArchivo;
                return true;
            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.Message;
                return false;
            }
        }

        private bool AddUnidadesBelfort(DataRow dataLine, ref PdfPTable table, iTextSharp.text.Font fontCustom, DataSet dataSet, decimal impuesto)
        {
            try
            {
                DateTime fechaConcepto = new DateTime();
                string textFechaConcepto = string.Empty;

                if (Helpers.Compartido.VerificarExistenciaColumnas(dataLine, "Date01"))
                {
                    if ((string)dataLine["Date01"] != null)
                        textFechaConcepto =
                            DateTime.TryParse((string)dataLine["Date01"], out fechaConcepto) ?
                            fechaConcepto.ToString("dd/MM/yy") : string.Empty;
                }


                //(string)dataLine["SellingShipQty"], fontCustom)
                PdfPCell celTextConcepto = new PdfPCell()
                {
                    //Border = PdfPCell.NO_BORDER,
                    VerticalAlignment = Element.ALIGN_CENTER,
                    Phrase = new Phrase($"{textFechaConcepto}  {(string)dataLine["LineDesc"]}", fontCustom),
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    BorderWidthBottom = PdfPCell.NO_BORDER,
                    BorderWidthTop = PdfPCell.NO_BORDER,
                    //BorderWidthRight = PdfPCell.NO_BORDER,
                };
                table.AddCell(celTextConcepto);

                PdfPCell celTextDetalle = new PdfPCell()
                {
                    //Border = PdfPCell.NO_BORDER,
                    VerticalAlignment = Element.ALIGN_CENTER,
                    Phrase = new Phrase(" ", fontCustom),
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    BorderWidthRight = PdfPCell.NO_BORDER,
                    BorderWidthBottom = PdfPCell.NO_BORDER,
                    BorderWidthTop = PdfPCell.NO_BORDER,

                    //BorderWidthLeft = PdfPCell.NO_BORDER,
                };
                table.AddCell(celTextDetalle);

                var taxableAmt = GetValTaxableAmt((string)dataLine["InvoiceNum"], (string)dataLine["InvoiceLine"], "01", dataSet) +
                    GetValTaxableAmt((string)dataLine["InvoiceNum"], (string)dataLine["InvoiceLine"], "02", dataSet) +
                    GetValTaxableAmt((string)dataLine["InvoiceNum"], (string)dataLine["InvoiceLine"], "03", dataSet) +
                    GetValTaxableAmt((string)dataLine["InvoiceNum"], (string)dataLine["InvoiceLine"], "04", dataSet);

                PdfPCell celTextValor = new PdfPCell()
                {
                    //Border = PdfPCell.NO_BORDER,
                    VerticalAlignment = Element.ALIGN_CENTER,
                    //Phrase = new Phrase(decimal.Parse((string)dataLine["DocUnitPrice"]).ToString("N0"), fontCustom),
                    Phrase = new Phrase(taxableAmt.ToString("N0"), fontCustom),
                    HorizontalAlignment = Element.ALIGN_RIGHT,
                    BorderWidthRight = PdfPCell.NO_BORDER,
                    BorderWidthBottom = PdfPCell.NO_BORDER,
                    BorderWidthTop = PdfPCell.NO_BORDER,
                };
                table.AddCell(celTextValor);

                //var impuesto = (decimal.Parse((string)dataLine["DocUnitPrice"])*
                //    decimal.Parse(GetPercentIdImpDIAN((string)dataLine["InvoiceNum"], (string)dataLine["InvoiceLine"], dataSet.Tables["InvcTax"]).Trim(',','0','.')))/100;
                PdfPCell celTextImpuestos = new PdfPCell()
                {
                    //Border = PdfPCell.NO_BORDER,
                    VerticalAlignment = Element.ALIGN_CENTER,
                    Phrase = new Phrase(impuesto.ToString("N0"), fontCustom),
                    HorizontalAlignment = Element.ALIGN_RIGHT,
                    BorderWidthRight = PdfPCell.NO_BORDER,
                    BorderWidthBottom = PdfPCell.NO_BORDER,
                    BorderWidthTop = PdfPCell.NO_BORDER,
                };
                table.AddCell(celTextImpuestos);

                var total = taxableAmt + impuesto;

                PdfPCell celTextTotal = new PdfPCell()
                {
                    //Border = PdfPCell.NO_BORDER,
                    VerticalAlignment = Element.ALIGN_CENTER,
                    Phrase = new Phrase(total.ToString("N0"), fontCustom),
                    HorizontalAlignment = Element.ALIGN_RIGHT,
                    BorderWidthRight = PdfPCell.NO_BORDER,
                    BorderWidthBottom = PdfPCell.NO_BORDER,
                    BorderWidthTop = PdfPCell.NO_BORDER,
                };
                table.AddCell(celTextTotal);

                PdfPCell celTextDescuento = new PdfPCell()
                {
                    //Border = PdfPCell.NO_BORDER,
                    VerticalAlignment = Element.ALIGN_CENTER,
                    Phrase = new Phrase(decimal.Parse((string)dataLine["DocDiscount"]).ToString("N0"), fontCustom),
                    HorizontalAlignment = Element.ALIGN_RIGHT,
                    BorderWidthRight = PdfPCell.NO_BORDER,
                    BorderWidthBottom = PdfPCell.NO_BORDER,
                    BorderWidthTop = PdfPCell.NO_BORDER,
                };
                table.AddCell(celTextDescuento);

                PdfPCell celTextAbonos = new PdfPCell()
                {
                    //Border = PdfPCell.NO_BORDER,
                    VerticalAlignment = Element.ALIGN_CENTER,
                    Phrase = new Phrase((string)dataLine["ShortChar01"], fontCustom),
                    HorizontalAlignment = Element.ALIGN_RIGHT,
                    BorderWidthRight = PdfPCell.NO_BORDER,
                    BorderWidthBottom = PdfPCell.NO_BORDER,
                    BorderWidthTop = PdfPCell.NO_BORDER,
                };
                table.AddCell(celTextAbonos);

                PdfPCell celTextSaldo = new PdfPCell()
                {
                    //Border = PdfPCell.NO_BORDER,
                    VerticalAlignment = Element.ALIGN_CENTER,
                    Phrase = new Phrase((string)dataLine["ShortChar02"], fontCustom),
                    HorizontalAlignment = Element.ALIGN_RIGHT,
                    BorderWidthBottom = PdfPCell.NO_BORDER,
                    BorderWidthTop = PdfPCell.NO_BORDER,
                    //BorderWidthRight = PdfPCell.NO_BORDER,
                    //BorderWidthLeft = PdfPCell.NO_BORDER,
                };
                table.AddCell(celTextSaldo);
                return true;
            }
            catch (Exception ex)
            {
                strError += "Error al crear columnas de tabla Items: " + ex.Message;
                return false;
            }
        }

        private bool AddUnidadesNCBelfort(DataRow dataLine, ref PdfPTable table, iTextSharp.text.Font fontCustom, DataSet dataSet)
        {
            try
            {
                PdfPCell celTextFecha = new PdfPCell(new Phrase(DateTime.Parse((string)dataSet.Tables["InvcHead"].Rows[0]["InvoiceDate"]).ToString("dd/MM/yyyy"),
                    fontCustom))
                {
                    Border = 0
                };
                table.AddCell(celTextFecha);

                PdfPCell celTextTipo = new PdfPCell(new Phrase(" ", fontCustom))
                {
                    Border = 0
                };
                table.AddCell(celTextTipo);

                PdfPCell celTextDescripcion = new PdfPCell(new Phrase((string)dataLine["LineDesc"], fontCustom))
                {
                    Border = 0
                };
                table.AddCell(celTextDescripcion);

                PdfPCell celTextDocumento = new PdfPCell(new Phrase(" ", fontCustom))
                {
                    Border = 0
                };
                table.AddCell(celTextDocumento);

                PdfPCell celTextValOriginal = new PdfPCell(new Phrase(decimal.Parse((string)dataLine["DocUnitPrice"]).ToString("N2"), fontCustom))
                {
                    Border = 0
                };
                table.AddCell(celTextValOriginal);

                PdfPCell celTextCambioEncabezado = new PdfPCell(new Phrase(" ", fontCustom))
                {
                    Border = 0
                };
                table.AddCell(celTextCambioEncabezado);

                PdfPCell celTextValPagado = new PdfPCell(new Phrase(" ", fontCustom))
                {
                    Border = 0
                };
                table.AddCell(celTextValPagado);

                PdfPCell celTextValPendiente = new PdfPCell(new Phrase(" ", fontCustom))
                {
                    Border = 0
                };
                table.AddCell(celTextValPendiente);
                return true;
            }
            catch (Exception ex)
            {
                strError += "Error al crear columnas de tabla Items: " + ex.Message;
                return false;
            }
        }

        #endregion
    }
}