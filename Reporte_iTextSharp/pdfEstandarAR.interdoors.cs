﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
//Referenciar y usar.
using System.Data;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Web;
using System.Windows.Forms;

namespace RulesServicesDIAN2.Adquiriente
{
    public partial class pdfEstandarAR
    {
        #region Lines de Detalle Factura

        private bool AddUnidadesDoors(DataRow dataLine, ref PdfPTable table, iTextSharp.text.Font fontTitleFactura, DataSet dataSet)
        {
            try
            {
                //CODIGO
                strError += "Add PartNum";
                iTextSharp.text.pdf.PdfPCell celValCodigo = new iTextSharp.text.pdf.PdfPCell(new Phrase((string)dataLine["PartNum"], fontTitleFactura));
                celValCodigo.Colspan = 1;
                celValCodigo.Padding = 2;
                celValCodigo.Border = PdfPCell.LEFT_BORDER | PdfPCell.RIGHT_BORDER;
                celValCodigo.HorizontalAlignment = Element.ALIGN_CENTER;
                celValCodigo.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValCodigo);
                strError += "Add PartNum OK";

                //DESCRIPCIÓN ARTÍCULO
                strError += "Add LineDesc";
                iTextSharp.text.pdf.PdfPCell celValDescripcion = new iTextSharp.text.pdf.PdfPCell(new Phrase((string)dataLine["LineDesc"], fontTitleFactura));
                celValDescripcion.Colspan = 1;
                celValDescripcion.Padding = 2;
                celValDescripcion.Border = PdfPCell.RIGHT_BORDER;
                celValDescripcion.HorizontalAlignment = Element.ALIGN_LEFT;
                celValDescripcion.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValDescripcion);
                strError += "Add LineDesc OK";

                //CANTIDAD
                strError += "Add SellingShipQty";
                iTextSharp.text.pdf.PdfPCell celValCantidad = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    string.Format("{0:C2}", Convert.ToDecimal((string)dataLine["SellingShipQty"])), fontTitleFactura));
                celValCantidad.Colspan = 1;
                celValCantidad.Padding = 2;
                celValCantidad.Border = PdfPCell.RIGHT_BORDER;
                //celValDesc.BorderWidthLeft = 1;
                celValCantidad.HorizontalAlignment = Element.ALIGN_CENTER;
                celValCantidad.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValCantidad);
                strError += "Add SellingShipQty OK";

                //VR. UNITARIO
                strError += "Add UnitPrice";
                iTextSharp.text.pdf.PdfPCell celValValorUnitario = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    string.Format("{0:C2}", Convert.ToDecimal((string)dataLine["UnitPrice"].ToString())), fontTitleFactura));
                celValValorUnitario.Colspan = 1;
                celValValorUnitario.Padding = 2;
                celValValorUnitario.Border = PdfPCell.RIGHT_BORDER;
                celValValorUnitario.HorizontalAlignment = Element.ALIGN_CENTER;
                celValValorUnitario.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValValorUnitario);
                strError += "Add UnitPrice OK";

                //% DSCTO
                strError += "Add DiscountPercent";
                iTextSharp.text.pdf.PdfPCell celValDescuento = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    decimal.Parse((string)dataLine["DiscountPercent"]).ToString("N0"), fontTitleFactura));
                celValDescuento.Colspan = 1;
                celValDescuento.Padding = 2;
                celValDescuento.Border = PdfPCell.RIGHT_BORDER;
                celValDescuento.HorizontalAlignment = Element.ALIGN_CENTER;
                celValDescuento.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValDescuento);
                strError += "Add DiscountPercent OK";

                //VR. PARCIAL
                strError += "Add DocExtPrice";
                //iTextSharp.text.pdf.PdfPCell celValLote = new iTextSharp.text.pdf.PdfPCell(new Phrase(decimal.Parse((string)dataLine["DocExtPrice"]).ToString("N0"), fontTitleFactura));
                iTextSharp.text.pdf.PdfPCell celValLote = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    string.Format("{0:C2}", Convert.ToDecimal((string)dataLine["DocExtPrice"])), fontTitleFactura));
                celValLote.Colspan = 1;
                celValLote.Border = PdfPCell.RIGHT_BORDER;
                celValLote.Padding = 2;
                celValLote.HorizontalAlignment = Element.ALIGN_CENTER;
                celValLote.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValLote);
                strError += "Add DocExtPrice OK";
                return true;
            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.Message;
                return false;
            }
        }
        #endregion

        #region Linea de Detalle Nota Credito

        private bool AddUnidadesDoorsNotaCredito(DataRow dataLine, ref PdfPTable table, iTextSharp.text.Font fontTitleFactura, DataSet dataSet)
        {
            try
            {
                //ARTÍCULO
                strError += "Add PartNum LineDesc";
                iTextSharp.text.pdf.PdfPCell celValArticulo = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0} - {1}",
                    (string)dataLine["PartNum"], (string)dataLine["LineDesc"]), fontTitleFactura));
                celValArticulo.Colspan = 1;
                celValArticulo.Padding = 2;
                celValArticulo.Border = PdfPCell.RIGHT_BORDER | PdfPCell.LEFT_BORDER;
                celValArticulo.HorizontalAlignment = Element.ALIGN_LEFT;
                celValArticulo.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValArticulo);
                strError += "Add PartNum LineDesc OK";

                //MOT
                strError += "Add MOT";
                iTextSharp.text.pdf.PdfPCell celValMot = new iTextSharp.text.pdf.PdfPCell(new Phrase((string)dataLine["ShortChar01"], fontTitleFactura));
                celValMot.Colspan = 1;
                celValMot.Padding = 2;
                celValMot.Border = PdfPCell.RIGHT_BORDER;
                celValMot.HorizontalAlignment = Element.ALIGN_CENTER;
                celValMot.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValMot);
                strError += "Add MOT OK";

                //UNIDAD
                strError += "Add SalesUM";
                iTextSharp.text.pdf.PdfPCell celValSalesUM = new iTextSharp.text.pdf.PdfPCell(new Phrase((string)dataLine["SalesUM"], fontTitleFactura));
                celValSalesUM.Colspan = 1;
                celValSalesUM.Padding = 2;
                celValSalesUM.Border = PdfPCell.RIGHT_BORDER;
                celValSalesUM.HorizontalAlignment = Element.ALIGN_CENTER;
                celValSalesUM.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValSalesUM);
                strError += "Add SalesUM OK";

                //CANTIDAD
                strError += "Add SellingShipQty";
                iTextSharp.text.pdf.PdfPCell celValCantidad = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    string.Format("{0:C2}", Convert.ToDecimal((string)dataLine["SellingShipQty"])), fontTitleFactura));
                celValCantidad.Colspan = 1;
                celValCantidad.Padding = 2;
                celValCantidad.Border = PdfPCell.RIGHT_BORDER;
                //celValDesc.BorderWidthLeft = 1;
                celValCantidad.HorizontalAlignment = Element.ALIGN_CENTER;
                celValCantidad.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValCantidad);
                strError += "Add SellingShipQty OK";

                //VR. UNITARIO
                strError += "Add UnitPrice";
                iTextSharp.text.pdf.PdfPCell celValValorUnitario = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    string.Format("{0:C2}", Convert.ToDecimal((string)dataLine["UnitPrice"].ToString())), fontTitleFactura));
                celValValorUnitario.Colspan = 1;
                celValValorUnitario.Padding = 2;
                celValValorUnitario.Border = PdfPCell.RIGHT_BORDER;
                celValValorUnitario.HorizontalAlignment = Element.ALIGN_CENTER;
                celValValorUnitario.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValValorUnitario);
                strError += "Add UnitPrice OK";

                //% DSCTO
                strError += "Add DiscountPercent";
                iTextSharp.text.pdf.PdfPCell celValDescuento = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    decimal.Parse((string)dataLine["DiscountPercent"]).ToString("N0"), fontTitleFactura));
                celValDescuento.Colspan = 1;
                celValDescuento.Padding = 2;
                celValDescuento.Border = PdfPCell.RIGHT_BORDER;
                celValDescuento.HorizontalAlignment = Element.ALIGN_CENTER;
                celValDescuento.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValDescuento);
                strError += "Add DiscountPercent OK";

                //VR. PARCIAL
                strError += "Add DocExtPrice";
                //iTextSharp.text.pdf.PdfPCell celValLote = new iTextSharp.text.pdf.PdfPCell(new Phrase(decimal.Parse((string)dataLine["DocExtPrice"]).ToString("N0"), fontTitleFactura));
                iTextSharp.text.pdf.PdfPCell celValLote = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    string.Format("{0:C2}", Convert.ToDecimal((string)dataLine["DocExtPrice"])), fontTitleFactura));
                celValLote.Colspan = 1;
                celValLote.Border = PdfPCell.RIGHT_BORDER;
                celValLote.Padding = 2;
                celValLote.HorizontalAlignment = Element.ALIGN_CENTER;
                celValLote.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValLote);
                strError += "Add DocExtPrice OK";

                //% IVA
                strError += "Add PORCENTAJE_IVA";
                string porcentajeIva = GetPercentIdImpDIAN((string)dataLine["InvoiceNum"], (string)dataLine["InvoiceLine"], dataSet.Tables["InvcTax"]).ToString();
                iTextSharp.text.pdf.PdfPCell celValIva = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:N2}",
                    Convert.ToDecimal(porcentajeIva)), fontTitleFactura));
                celValDescuento.Colspan = 1;
                celValDescuento.Padding = 2;
                celValDescuento.Border = PdfPCell.RIGHT_BORDER;
                celValDescuento.HorizontalAlignment = Element.ALIGN_CENTER;
                celValDescuento.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValDescuento);
                strError += "Add PORCENTAJE_IVA OK";

                return true;
            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.Message;
                return false;
            }
        }
        #endregion

        #region FACTURA DE VENTAS

        public bool FacturaNacionalInterDoors(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {
            //agregamos las imagenes fuentes abrimos y cerramos el documento
            #region Head
            bool esLocal = Helpers.Compartido.EsLocal;
            string Ruta = string.Empty;
            string RutaImg = string.Empty;
            string Rutasello = string.Empty;
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            if (esLocal)
            {
                Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
                RutaImg = $@"{AppDomain.CurrentDomain.BaseDirectory}\Logo_Interdoors.png";
            }
            else
            {
                Ruta = HttpContext.Current.Server.MapPath("~/PDF/" + NIT + "/");
                RutaImg = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";
            }
            NomArchivo = string.Empty;
            NomArchivo = NombreInvoice + ".pdf";

            try
            {
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.
                Document document = new Document(iTextSharp.text.PageSize.LETTER, 20f, 10f, 30f, 30f);
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;

                // step 3: we open the document     
                document.Open();

                PdfDiv divEspacio = new PdfDiv();
                divEspacio.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio.Height = 10;
                divEspacio.Width = 130;

                PdfDiv divEspacio2 = new PdfDiv();
                divEspacio2.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio2.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio2.Height = 2;
                divEspacio2.Width = 130;

                //FUENTES----------------------------------------------------------------------------------------------------------
                iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle2 = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustom = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitleFecha = FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontDMA = FontFactory.GetFont(FontFactory.HELVETICA, 6, BaseColor.GRAY);
                iTextSharp.text.Font fontDetalleFecha = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 8, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitleFactura = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                //FUENTES---------------------------------------------------------------------------------------------------------

                /////saltos de trabla 
                PdfPTable salto = new PdfPTable(1);
                salto.WidthPercentage = 100;
                PdfPCell vacio = new PdfPCell() { MinimumHeight = 2, Border = 0, };
                salto.AddCell(vacio);
                ////-------------------------------------

                //LOGO-------------------------------------------------------------------------------------------------------------
                System.Drawing.Image logo = null;
                var requestLogo = WebRequest.Create(RutaImg);

                using (var responseLogo = requestLogo.GetResponse())
                using (var streamLogo = responseLogo.GetResponseStream())
                {
                    logo = Bitmap.FromStream(streamLogo);
                }

                PdfDiv divLogo = new PdfDiv();
                divLogo.Float = iTextSharp.text.pdf.PdfDiv.FloatType.LEFT;
                divLogo.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divLogo.BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID;
                divLogo.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                divLogo.Height = 60;
                divLogo.Width = 145;
                //divLogo.BackgroundColor = BaseColor.LIGHT_GRAY;
                iTextSharp.text.Image ImgLogo = iTextSharp.text.Image.GetInstance(logo, BaseColor.WHITE);
                divLogo.BackgroundImage = ImgLogo;
                //LOGO-------------------------------------------------------------------------------------------------------------

                //---------------------------------------------------------------------------------------------------------
                PdfDiv divAcercade = new PdfDiv();
                divAcercade.Float = iTextSharp.text.pdf.PdfDiv.FloatType.LEFT;
                divAcercade.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divAcercade.BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID;
                divAcercade.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divAcercade.BackgroundColor = BaseColor.LIGHT_GRAY;
                //divAcercade.PaddingLeft = 5;
                //divAcercade.PaddingRight = 10;
                divAcercade.Width = 140;

                #endregion

                #region Header

                PdfPTable Header = new PdfPTable(new float[] { 2.5f, 2.5f, 2.5f, 2.5f });
                Header.WidthPercentage = 100;

                PdfPCell cell_logo = new PdfPCell() { Border = 0, };
                cell_logo.AddElement(ImgLogo);

                PdfPCell cell_info = new PdfPCell() { Border = 0, };
                //agregamos el numnre de la empresa
                PdfPTable nombre_empresa = new PdfPTable(1);
                nombre_empresa.WidthPercentage = 100;
                PdfPCell empresa = new PdfPCell(new Phrase(DsInvoiceAR.Tables["InvcHead"].Rows[0]["Company"].ToString(), fontTitle)) { Border = 0, };
                nombre_empresa.AddCell(empresa);
                cell_info.AddElement(nombre_empresa);
                //agregamos el nit de la empresa
                PdfPTable niut_empresa = new PdfPTable(1);
                niut_empresa.WidthPercentage = 100;
                PdfPCell nit = new PdfPCell(new Phrase(DsInvoiceAR.Tables["Company"].Rows[0]["Name"].ToString(), fontTitle)) { Border = 0, };
                niut_empresa.AddCell(nit);
                cell_info.AddElement(niut_empresa);
                //agregamos la direccion de la empresa 
                PdfPTable direc_empresa = new PdfPTable(1);
                direc_empresa.WidthPercentage = 100;
                PdfPCell direc = new PdfPCell(new Phrase(DsInvoiceAR.Tables["Company"].Rows[0]["Address1"].ToString(), fontTitle)) { Border = 0, };
                direc_empresa.AddCell(direc);
                cell_info.AddElement(direc_empresa);
                //agregamos la ciudad de la empresa
                PdfPTable ciudad_empresa = new PdfPTable(1);
                ciudad_empresa.WidthPercentage = 100;
                PdfPCell ciudad = new PdfPCell(new Phrase(DsInvoiceAR.Tables["Company"].Rows[0]["City"].ToString(), fontTitle)) { Border = 0, };
                ciudad_empresa.AddCell(ciudad);
                cell_info.AddElement(ciudad_empresa);
                //agregamos la pbx de la empresa
                PdfPTable pbx_empresa = new PdfPTable(1);
                pbx_empresa.WidthPercentage = 100;
                PdfPCell pbx = new PdfPCell(new Phrase(string.Format("PBX: {0}", DsInvoiceAR.Tables["Company"].Rows[0]["PhoneNum"].ToString()), fontTitle)) { Border = 0, };
                pbx_empresa.AddCell(pbx);
                cell_info.AddElement(pbx_empresa);
                //agregamos el numero y fax de la empresa 
                PdfPTable fax_empresa = new PdfPTable(1);
                fax_empresa.WidthPercentage = 100;
                PdfPCell fax = new PdfPCell(new Phrase(string.Format("FAX: {0}", DsInvoiceAR.Tables["Company"].Rows[0]["FaxNum"].ToString()), fontTitle)) { Border = 0, };
                fax_empresa.AddCell(fax);
                cell_info.AddElement(fax_empresa);
                //agregamos el email de la empresa
                PdfPTable email_empresa = new PdfPTable(1);
                email_empresa.WidthPercentage = 100;
                PdfPCell email = new PdfPCell(new Phrase(DsInvoiceAR.Tables["Company"].Rows[0]["ShortChar02"].ToString(), fontTitle)) { Border = 0, };
                email_empresa.AddCell(email);
                cell_info.AddElement(email_empresa);

                PdfPCell cell_qr = new PdfPCell() { Border = 0, };
                iTextSharp.text.Image QRPdf = iTextSharp.text.Image.GetInstance(QRInvoice, BaseColor.WHITE);
                QRPdf.ScaleAbsolute(60, 60);
                QRPdf.Border = 0;
                cell_qr.AddElement(QRPdf);

                PdfPCell cell_factura = new PdfPCell() { Border = 0, };

                //agregamos el numero de la factura
                PdfPTable numero_factura = new PdfPTable(1);
                numero_factura.WidthPercentage = 100;
                PdfPCell numerof = new PdfPCell(new Phrase("FACTURA DE VENTAS")) { Border = 0, };
                numero_factura.AddCell(numerof);
                cell_factura.AddElement(numero_factura);
                //agregamos el regimen de la empresa 
                PdfPTable regimen = new PdfPTable(1);
                regimen.WidthPercentage = 100;
                PdfPCell regimenn = new PdfPCell(new Phrase(DsInvoiceAR.Tables["Company"].Rows[0]["ShortChar01"].ToString(), fontTitleFactura)) { Border = 0, };
                regimen.AddCell(regimenn);
                cell_factura.AddElement(regimen);

                Header.AddCell(cell_logo);
                Header.AddCell(cell_info);
                Header.AddCell(cell_qr);
                Header.AddCell(cell_factura);

                PdfPTable Header_baja = new PdfPTable(new float[] { 0.3f, 0.7f });
                Header_baja.WidthPercentage = 100;

                PdfPCell header_bajovacio = new PdfPCell() { Border = 0, };

                PdfPCell header_bajocontenmido = new PdfPCell(new Phrase("Resol No. 18762007665311 Facturación del FV -00030854 al FV -00034999 Fecha 06/04/2018 Fecha vencimiento06 / 10 / 2019 " + " CUFE: " + CUFE, fontTitleFactura)) { Border = 0, };

                Header_baja.AddCell(header_bajovacio);
                Header_baja.AddCell(header_bajocontenmido);

                #endregion

                #region Body

                PdfPTable Body = new PdfPTable(new float[] { 0.7f, 0.1f, 0.3f });
                Body.WidthPercentage = 100;

                PdfPCell señores = new PdfPCell();
                //SEÑORES
                PdfPTable cliente = new PdfPTable(1);
                cliente.WidthPercentage = 100;
                PdfPCell cliente_señores = new PdfPCell(new Phrase("Señores", fontTitle)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, };
                cliente.AddCell(cliente_señores);
                señores.AddElement(cliente);
                //agregamos el nombre del cleinte
                PdfPTable nombre_cliente = new PdfPTable(1);
                nombre_cliente.WidthPercentage = 100;
                PdfPCell nombre_client = new PdfPCell(new Phrase(DsInvoiceAR.Tables["Customer"].Rows[0]["Name"].ToString(), fontTitle)) { Border = 0, };
                nombre_cliente.AddCell(nombre_client);
                señores.AddElement(nombre_cliente);
                //agregamos el nit del cliente
                PdfPTable nit_cliente = new PdfPTable(1);
                nit_cliente.WidthPercentage = 100;
                PdfPCell nit_client = new PdfPCell(new Phrase(DsInvoiceAR.Tables["Customer"].Rows[0]["Company"].ToString(), fontTitle)) { Border = 0, };
                nit_cliente.AddCell(nit_client);
                señores.AddElement(nit_cliente);
                //agregamos la direccioin del clientre
                PdfPTable direccion_cliente = new PdfPTable(1);
                direccion_cliente.WidthPercentage = 100;
                PdfPCell direccion_client = new PdfPCell(new Phrase(DsInvoiceAR.Tables["Customer"].Rows[0]["Address1"].ToString(), fontTitle)) { Border = 0, };
                direccion_cliente.AddCell(direccion_client);
                señores.AddElement(direccion_cliente);
                //agregamos la ciudad cliente
                PdfPTable ciudad_cliente = new PdfPTable(1);
                ciudad_cliente.WidthPercentage = 100;
                PdfPCell ciudad_client = new PdfPCell(new Phrase(DsInvoiceAR.Tables["Customer"].Rows[0]["City"].ToString(), fontTitle)) { Border = 0, };
                ciudad_cliente.AddCell(ciudad_client);
                señores.AddElement(ciudad_cliente);
                //agregamos el numero y fax de la empresa 
                PdfPTable departamento_cliente = new PdfPTable(1);
                departamento_cliente.WidthPercentage = 100;
                PdfPCell fdepart_clienteax = new PdfPCell(new Phrase(DsInvoiceAR.Tables["Customer"].Rows[0]["State"].ToString(), fontTitle)) { Border = 0, };
                departamento_cliente.AddCell(fdepart_clienteax);
                señores.AddElement(departamento_cliente);

                PdfPCell body_vacio = new PdfPCell() { Border = 0, };
                PdfPCell body_fechas = new PdfPCell() { Border = 0, };

                PdfPTable fechas_final = new PdfPTable(2);
                fechas_final.WidthPercentage = 100;

                PdfPCell cont_fechas1 = new PdfPCell() { Border = 0, Padding = 0, };

                //agregamos fecha de entrega
                PdfPTable f_entrega = new PdfPTable(1);
                f_entrega.WidthPercentage = 100;
                PdfPCell f_entre = new PdfPCell(new Phrase("Fecha de entrega", fontTitle)) { HorizontalAlignment = Element.ALIGN_CENTER, Padding = 0, MinimumHeight = 12, };
                f_entrega.AddCell(f_entre);
                cont_fechas1.AddElement(f_entrega);
                //agregamos formato
                PdfPTable fomato = new PdfPTable(1);
                fomato.WidthPercentage = 100;
                PdfPCell fomato1 = new PdfPCell(new Phrase("DIA/MES/AÑO", fontTitle)) { HorizontalAlignment = Element.ALIGN_CENTER, Padding = 0, MinimumHeight = 12, };
                fomato.AddCell(fomato1);
                cont_fechas1.AddElement(fomato);
                //agregamos la fecha
                PdfPTable fecha = new PdfPTable(1);
                fecha.WidthPercentage = 100;
                PdfPCell fecha_ = new PdfPCell(new Phrase(string.Format("{0:MM/dd/yyyy}", DateTime.Parse(DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"].ToString())), fontTitle)) { HorizontalAlignment = Element.ALIGN_CENTER, Padding = 0, MinimumHeight = 15, };
                fecha.AddCell(fecha_);
                cont_fechas1.AddElement(fecha);
                //agregamos la remicion
                PdfPTable remicion = new PdfPTable(1);
                remicion.WidthPercentage = 100;
                PdfPCell remicion_n = new PdfPCell(new Phrase("REMISION No.  ", fontTitle)) { HorizontalAlignment = Element.ALIGN_CENTER, Padding = 0, MinimumHeight = 12, };
                remicion.AddCell(remicion_n);
                cont_fechas1.AddElement(remicion);
                //agregamos el valor de remicion
                PdfPTable v_remicion = new PdfPTable(1);
                v_remicion.WidthPercentage = 100;
                PdfPCell v_remicion1 = new PdfPCell(new Phrase(DsInvoiceAR.Tables["InvcHead"].Rows[0]["OrderNum"].ToString(), fontTitle)) { HorizontalAlignment = Element.ALIGN_CENTER, Padding = 0, MinimumHeight = 15, };
                v_remicion.AddCell(v_remicion1);
                cont_fechas1.AddElement(v_remicion);

                PdfPCell cont_fechas2 = new PdfPCell() { Border = 0, Padding = 0, };

                //agregamos vensimiento
                PdfPTable vensimiento = new PdfPTable(1);
                vensimiento.WidthPercentage = 100;
                PdfPCell vensimiento_t = new PdfPCell(new Phrase(" VENCIMIENTO  ", fontTitle)) { HorizontalAlignment = Element.ALIGN_CENTER, Padding = 0, MinimumHeight = 12, };
                vensimiento.AddCell(vensimiento_t);
                cont_fechas2.AddElement(vensimiento);
                //agregamos formato
                PdfPTable fomato_v = new PdfPTable(1);
                fomato_v.WidthPercentage = 100;
                PdfPCell fomato_v1 = new PdfPCell(new Phrase("DIA/MES/AÑO", fontTitle)) { HorizontalAlignment = Element.ALIGN_CENTER, Padding = 0, MinimumHeight = 12, };
                fomato_v.AddCell(fomato_v1);
                cont_fechas2.AddElement(fomato_v);
                //agregamos la fecha
                PdfPTable fomato_v2 = new PdfPTable(1);
                fomato_v2.WidthPercentage = 100;
                PdfPCell fomato_v2_ = new PdfPCell(new Phrase(string.Format("{0:MM/dd/yyyy}", DateTime.Parse(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"].ToString())), fontTitle)) { HorizontalAlignment = Element.ALIGN_CENTER, Padding = 0, MinimumHeight = 15, };
                fomato_v2.AddCell(fomato_v2_);
                cont_fechas2.AddElement(fomato_v2);
                //agregamos la VENDEDOR
                PdfPTable VENDEDOR = new PdfPTable(1);
                VENDEDOR.WidthPercentage = 100;
                PdfPCell VENDEDOR_v = new PdfPCell(new Phrase("VENDEDOR_no ", fontTitle)) { HorizontalAlignment = Element.ALIGN_CENTER, Padding = 0, MinimumHeight = 12, };
                VENDEDOR.AddCell(VENDEDOR_v);
                cont_fechas2.AddElement(VENDEDOR);
                //agregamos VENDEDOR numero
                PdfPTable VENDEDOR_n = new PdfPTable(1);
                VENDEDOR_n.WidthPercentage = 100;
                PdfPCell VENDEDOR_no = new PdfPCell(new Phrase(DsInvoiceAR.Tables["InvcHead"].Rows[0]["SalesRepName1"].ToString(), fontTitle)) { HorizontalAlignment = Element.ALIGN_CENTER, Padding = 0, MinimumHeight = 15, };
                VENDEDOR_n.AddCell(VENDEDOR_no);
                cont_fechas2.AddElement(VENDEDOR_n);

                fechas_final.AddCell(cont_fechas1);
                fechas_final.AddCell(cont_fechas2);
                body_fechas.AddElement(fechas_final);

                Body.AddCell(señores);
                Body.AddCell(body_vacio);
                Body.AddCell(body_fechas);


                PdfPTable Titulos_detalles = new PdfPTable(new float[] { 0.1f, 0.54f, 0.08f, 0.1f, 0.08f, 0.1f });
                Titulos_detalles.WidthPercentage = 100;

                PdfPCell CODIGO = new PdfPCell(new Phrase("CODIGO", fontTitle)) { HorizontalAlignment = Element.ALIGN_CENTER, };
                PdfPCell DESCRIPCIÓN = new PdfPCell(new Phrase("DESCRIPCIÓN ARTÍCULO", fontTitle)) { HorizontalAlignment = Element.ALIGN_CENTER, };
                PdfPCell CANTIDAD = new PdfPCell(new Phrase("CANTIDAD ", fontTitle)) { HorizontalAlignment = Element.ALIGN_CENTER, };
                PdfPCell UNITARIO = new PdfPCell(new Phrase("VR. UNITARIO", fontTitle)) { HorizontalAlignment = Element.ALIGN_CENTER, };
                PdfPCell DSCTO = new PdfPCell(new Phrase("% DSCTO", fontTitle)) { HorizontalAlignment = Element.ALIGN_CENTER, };
                PdfPCell PARCIAL = new PdfPCell(new Phrase("VR.PARCIAL", fontTitle)) { HorizontalAlignment = Element.ALIGN_CENTER, };

                Titulos_detalles.AddCell(CODIGO);
                Titulos_detalles.AddCell(DESCRIPCIÓN);
                Titulos_detalles.AddCell(CANTIDAD);
                Titulos_detalles.AddCell(UNITARIO);
                Titulos_detalles.AddCell(DSCTO);
                Titulos_detalles.AddCell(PARCIAL);

                #endregion

                #region Unidades

                PdfPTable tableTituloUnidades = new PdfPTable(6);
                //Dimenciones.
                float[] DimencionUnidades = new float[6];
                DimencionUnidades[0] = 0.1f;//L
                DimencionUnidades[1] = 0.54f;//Codigo
                DimencionUnidades[2] = 0.08f;//Descripcion
                DimencionUnidades[3] = 0.1f;//Unidad
                DimencionUnidades[4] = 0.08f;//Cantidad
                DimencionUnidades[5] = 0.1f;//lote

                PdfPTable tableUnidades = new PdfPTable(6);
                tableUnidades.WidthPercentage = 100;
                tableUnidades.SetWidths(DimencionUnidades);
                decimal totalDescuento = 0;
                foreach (DataRow InvoiceLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
                {
                    ////confirmar del nodo InvcDtl
                    //if (bool.Parse((string)InvoiceLine["CheckBox01"]) == true)
                    //{

                    if (!AddUnidadesDoors(InvoiceLine, ref tableUnidades, fontCustom, DsInvoiceAR))
                        return false;

                    totalDescuento += decimal.Parse((string)InvoiceLine["InvoiceLine"]);
                    //}
                }

                int numAdLineas = 10 - DsInvoiceAR.Tables["InvcDtl"].Rows.Count;
                PdfPTable tableEspacioUnidades = new PdfPTable(6);
                tableEspacioUnidades.WidthPercentage = 100;
                tableEspacioUnidades.SetWidths(DimencionUnidades);

                PdfPTable tableLineaFinal = new PdfPTable(6);
                tableLineaFinal.WidthPercentage = 100;
                tableLineaFinal.SetWidths(DimencionUnidades);
                iTextSharp.text.pdf.PdfPCell LineaFinal = new iTextSharp.text.pdf.PdfPCell();
                LineaFinal.Colspan = 6;
                LineaFinal.Padding = 3;
                LineaFinal.BorderColorBottom = BaseColor.WHITE;
                LineaFinal.HorizontalAlignment = Element.ALIGN_CENTER;
                LineaFinal.VerticalAlignment = Element.ALIGN_TOP;
                tableLineaFinal.AddCell(LineaFinal);

                #endregion

                #region Footer

                PdfPTable Footer = new PdfPTable(new float[] { 0.75f, 0.25f });
                Footer.WidthPercentage = 100;

                PdfPCell pie_info = new PdfPCell() { Border = 0, Padding = 0, };

                PdfPTable info_de_pie = new PdfPTable(new float[] { 2.0f, 2.0f, 6.0f });
                info_de_pie.WidthPercentage = 100;

                PdfPCell pague = new PdfPCell(new Phrase("PAGUESE ANTES DE", fontTitle)) { };

                PdfPCell poague_fechas = new PdfPCell(new Phrase("")) { Padding = 0, };

                //agregamos la celda de la fecha de limite de pago
                PdfPTable pqague_formato = new PdfPTable(1);
                pqague_formato.WidthPercentage = 100;

                PdfPTable todas_fechas_pie = new PdfPTable(3);
                todas_fechas_pie.WidthPercentage = 100;

                //Fecha Vencimiento - Tabla con Valores

                DateTime fechaVencimiento = DateTime.Parse(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"].ToString());
                PdfPCell pqague_valdfia1 = new PdfPCell(new Phrase(string.Format("{0:MM}", fechaVencimiento), fontTitle)) { HorizontalAlignment = Element.ALIGN_CENTER, };
                todas_fechas_pie.AddCell(pqague_valdfia1);
                //pqague_dfia1.Border = PdfPCell.RIGHT_BORDER;

                PdfPCell pqague_valmes1 = new PdfPCell(new Phrase(string.Format("{0:dd}", fechaVencimiento), fontTitle)) { HorizontalAlignment = Element.ALIGN_CENTER, };
                todas_fechas_pie.AddCell(pqague_valmes1);
                //pqague_mes1.Border = PdfPCell.RIGHT_BORDER;

                PdfPCell pqague_valaño1 = new PdfPCell(new Phrase(string.Format("{0:yyyy}", fechaVencimiento), fontTitle)) { HorizontalAlignment = Element.ALIGN_CENTER, };
                todas_fechas_pie.AddCell(pqague_valaño1);

                //agregamos el formato de pago dia
                PdfPCell pqague_dfia1 = new PdfPCell(new Phrase(" DÍA ", fontTitle)) { HorizontalAlignment = Element.ALIGN_CENTER, };
                todas_fechas_pie.AddCell(pqague_dfia1);
                pqague_dfia1.Border = PdfPCell.RIGHT_BORDER;

                //agregamos el formato de pago mes
                PdfPCell pqague_mes1 = new PdfPCell(new Phrase(" MES ", fontTitle)) { HorizontalAlignment = Element.ALIGN_CENTER, };
                todas_fechas_pie.AddCell(pqague_mes1);
                pqague_mes1.Border = PdfPCell.RIGHT_BORDER;

                //agregamos el formato de pago AÑO
                PdfPCell pqague_año1 = new PdfPCell(new Phrase(" AÑO ", fontTitle)) { HorizontalAlignment = Element.ALIGN_CENTER, };
                todas_fechas_pie.AddCell(pqague_año1);

                poague_fechas.AddElement(todas_fechas_pie);

                PdfPTable tabledescuento = new PdfPTable(1);
                tabledescuento.WidthPercentage = 100;

                //string valor_descuento = DsInvoiceAR.Tables["InvcHead"].Rows[0][""].ToString();
                PdfPCell cel_descuento_oc = new PdfPCell(new Phrase("OBTENGA UN DESCUENTO DE:", fontTitle)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT };
                PdfPCell cel_descuento_val = new PdfPCell(new Phrase(string.Format("{0:C2}", Convert.ToDecimal(DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number01"].ToString())), fontTitle)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER };
                tabledescuento.AddCell(cel_descuento_oc);
                tabledescuento.AddCell(cel_descuento_val);

                PdfPCell _celdescuento = new PdfPCell(tabledescuento);

                info_de_pie.AddCell(pague);
                info_de_pie.AddCell(poague_fechas);
                info_de_pie.AddCell(_celdescuento);
                pie_info.AddElement(info_de_pie);

                PdfPTable observaciones = new PdfPTable(1);
                observaciones.WidthPercentage = 100;
                PdfPCell observa = new PdfPCell(new Phrase("OBSERVACIONES:", fontTitle)) { BorderWidthBottom = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP, };
                observaciones.AddCell(observa);
                PdfPCell observa_valor = new PdfPCell(new Phrase(DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceComment"].ToString(), fontTitle)) { BorderWidthTop = 0, MinimumHeight = 35, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP, };
                observaciones.AddCell(observa_valor);
                pie_info.AddElement(observaciones);

                PdfPTable firma = new PdfPTable(new float[] { 0.45f, 0.55f, });
                firma.WidthPercentage = 100;

                PdfPCell firma_n = new PdfPCell(new Phrase(""));

                PdfPTable sello = new PdfPTable(1);
                sello.WidthPercentage = 100;
                //agregamos firma y sello
                PdfPCell firma_sello = new PdfPCell(new Phrase("RECIBI (FIRMA, NOMBRE Y SELLO)", fontTitleFactura)) { Border = 0, MinimumHeight = 40, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP };
                sello.AddCell(firma_sello);
                firma_n.AddElement(sello);

                PdfPTable sello_fin = new PdfPTable(new float[] { 0.8f, 0.2f, 0.2f });
                sello_fin.WidthPercentage = 100;
                //agregamos firma y sello
                PdfPCell sello_fin1 = new PdfPCell(new Phrase("NIT/C.C.", fontTitleFactura)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_BOTTOM };
                PdfPCell sello_fin1_DIA = new PdfPCell(new Phrase("DÍA", fontTitle)) { HorizontalAlignment = Element.ALIGN_CENTER, };
                PdfPCell sello_fin1_MES = new PdfPCell(new Phrase("MES", fontTitle)) { HorizontalAlignment = Element.ALIGN_CENTER, };
                sello_fin.AddCell(sello_fin1);
                sello_fin.AddCell(sello_fin1_DIA);
                sello_fin.AddCell(sello_fin1_MES);
                firma_n.AddElement(sello_fin);


                PdfPCell firma_s = new PdfPCell(new Phrase("Todo reclamo se debe efectuar antes de la firma de la remision de transporte o" +
                                                                    "factura.Debe ser cancelada dentro del plazo estipulado, pasado este," +
                                                                    "cobraremos interés de mora a la tasa máxima legal autorizada por la ley." +
                                                                    "Páguese únicamente con cheque cruzado a favor de" +
                                                                    "INTERDOORS S.A.S Factura asimilada a letra de cambio art. 774 Codigo de" +
                                                                    "Comercio" +
                                                                     "Aceptada factura de venta, cliente autoriza consulta y reporte a las centrales deriesgo:", fontTitle))
                { MinimumHeight = 50, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP, };

                firma.AddCell(firma_n);
                firma.AddCell(firma_s);
                pie_info.AddElement(firma);


                PdfPTable totales = new PdfPTable(2);
                totales.WidthPercentage = 100;

                PdfPCell bruto = new PdfPCell(new Phrase("TOTAL BRUTO", fontTitle)) { HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_BOTTOM, Border = 0, MinimumHeight = 22, BorderWidthRight = 1 };
                PdfPCell bruto_V = new PdfPCell(new Phrase(string.Format("{0:C2}", Convert.ToDecimal(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubTotal"].ToString())), fontTitle)) { HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_BOTTOM, Border = 0, MinimumHeight = 22, };
                PdfPCell descuento = new PdfPCell(new Phrase("TOTAL DSCTO", fontTitle)) { HorizontalAlignment = Element.ALIGN_LEFT, Border = 0, BorderWidthRight = 1 };
                PdfPCell descuento_v = new PdfPCell(new Phrase(string.Format("{0:C2}", Convert.ToDecimal(DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number01"].ToString())), fontTitle)) { HorizontalAlignment = Element.ALIGN_RIGHT, Border = 0, };
                PdfPCell subtotal = new PdfPCell(new Phrase("SUB-TOTAL", fontTitle)) { HorizontalAlignment = Element.ALIGN_LEFT, Border = 0, BorderWidthRight = 1 };
                PdfPCell subtotal_v = new PdfPCell(new Phrase(string.Format("{0:C2}", Convert.ToDecimal(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubTotal"].ToString())), fontTitle)) { HorizontalAlignment = Element.ALIGN_RIGHT, Border = 0, };
                PdfPCell IVA = new PdfPCell(new Phrase("I.V.A", fontTitle)) { HorizontalAlignment = Element.ALIGN_LEFT, Border = 0, BorderWidthRight = 1 };
                PdfPCell IVA_v = new PdfPCell(new Phrase(string.Format("{0:C2}", Convert.ToDecimal(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DocTaxAmt"].ToString())), fontTitle)) { HorizontalAlignment = Element.ALIGN_RIGHT, Border = 0, };
                PdfPCell retefuente = new PdfPCell(new Phrase("RETEFUENTE", fontTitle)) { HorizontalAlignment = Element.ALIGN_LEFT, Border = 0, BorderWidthRight = 1 };
                PdfPCell retefuente_v = new PdfPCell(new Phrase(string.Format("{0:C2}", Convert.ToDecimal(GetValImpuestoByID("0B", DsInvoiceAR).ToString())), fontTitle)) { HorizontalAlignment = Element.ALIGN_RIGHT, Border = 0, };
                PdfPCell reteiva = new PdfPCell(new Phrase("RETEIVA", fontTitle)) { HorizontalAlignment = Element.ALIGN_LEFT, Border = 0, BorderWidthRight = 1 };
                PdfPCell reteiva_v = new PdfPCell(new Phrase(string.Format("{0:C2}", Convert.ToDecimal(GetValImpuestoByID("0C", DsInvoiceAR).ToString())), fontTitle)) { HorizontalAlignment = Element.ALIGN_RIGHT, Border = 0, };
                PdfPCell neto = new PdfPCell(new Phrase("VR. NETO", fontTitle)) { HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_BOTTOM, Border = 0, MinimumHeight = 31, BorderWidthRight = 1 };
                PdfPCell neto_v = new PdfPCell(new Phrase(string.Format("{0:C2}", Convert.ToDecimal(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"].ToString())), fontTitle)) { HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_BOTTOM, Border = 0, MinimumHeight = 31, };

                totales.AddCell(bruto);
                totales.AddCell(bruto_V);
                totales.AddCell(descuento);
                totales.AddCell(descuento_v);
                totales.AddCell(subtotal);
                totales.AddCell(subtotal_v);
                totales.AddCell(IVA);
                totales.AddCell(IVA_v);
                totales.AddCell(retefuente);
                totales.AddCell(retefuente_v);
                totales.AddCell(reteiva);
                totales.AddCell(reteiva_v);
                totales.AddCell(neto);
                totales.AddCell(neto_v);

                PdfPCell pie_total = new PdfPCell(totales);

                Footer.AddCell(pie_info);
                Footer.AddCell(pie_total);

                PdfPTable leyendas = new PdfPTable(1);
                leyendas.WidthPercentage = 100;
                PdfPCell leyenda = new PdfPCell(new Phrase("INTERDOORS S.A.S CALLE 7 SUR N° 42 - 70 OF 505, PBX 520 65 - 20", fontTitle)) { HorizontalAlignment = Element.ALIGN_CENTER, Border = 0, };
                leyendas.AddCell(leyenda);

                PdfPTable original = new PdfPTable(1);
                original.WidthPercentage = 100;
                PdfPCell ori = new PdfPCell(new Phrase("- ORIGINAL CLIENTE -", fontTitle)) { HorizontalAlignment = Element.ALIGN_CENTER, Border = 0, };
                original.AddCell(ori);

                #endregion

                #region Exit

                document.Add(Header);
                document.Add(salto);
                document.Add(Header_baja);
                document.Add(salto);
                document.Add(Body);
                document.Add(salto);
                document.Add(Titulos_detalles);
                document.Add(tableTituloUnidades);
                document.Add(tableUnidades);
                document.Add(tableEspacioUnidades);
                document.Add(tableLineaFinal);
                document.Add(Footer);
                document.Add(leyendas);
                document.Add(original);

                //document.Add(divTextBancoCheque);
                /*PIE DE PAGINA*/
                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();
                RutaPdf = NomArchivo;
                return true;
            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.Message;
                return false;
            }


            #endregion
        }
        #endregion

        #region NOTA CREDITO

        public bool NotaCreditolInterDoors(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {
            //agregamos las imagenes fuentes abrimos y cerramos el documento
            #region Head
            bool esLocal = Helpers.Compartido.EsLocal;
            string Ruta = string.Empty;
            string RutaImg = string.Empty;
            string Rutasello = string.Empty;
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            if (esLocal)
            {
                Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
                RutaImg = $@"{AppDomain.CurrentDomain.BaseDirectory}\Logo_Interdoors.png";
            }
            else
            {
                Ruta = HttpContext.Current.Server.MapPath("~/PDF/" + NIT + "/");
                RutaImg = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";
            }
            NomArchivo = string.Empty;
            NomArchivo = NombreInvoice + ".pdf";

            try
            {
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.
                Document document = new Document(iTextSharp.text.PageSize.LETTER, 20f, 10f, 30f, 30f);
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;

                // step 3: we open the document     
                document.Open();

                PdfDiv divEspacio = new PdfDiv();
                divEspacio.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio.Height = 10;
                divEspacio.Width = 130;

                PdfDiv divEspacio2 = new PdfDiv();
                divEspacio2.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio2.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio2.Height = 2;
                divEspacio2.Width = 130;

                //FUENTES----------------------------------------------------------------------------------------------------------
                iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle2 = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustom = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitleFecha = FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontDMA = FontFactory.GetFont(FontFactory.HELVETICA, 6, BaseColor.GRAY);
                iTextSharp.text.Font fontDetalleFecha = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 8, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitleFactura = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                //FUENTES---------------------------------------------------------------------------------------------------------

                /////saltos de trabla 
                PdfPTable salto = new PdfPTable(1);
                salto.WidthPercentage = 100;
                PdfPCell vacio = new PdfPCell() { MinimumHeight = 2, Border = 0, };
                salto.AddCell(vacio);
                ////-------------------------------------

                //LOGO-------------------------------------------------------------------------------------------------------------
                System.Drawing.Image logo = null;
                var requestLogo = WebRequest.Create(RutaImg);

                using (var responseLogo = requestLogo.GetResponse())
                using (var streamLogo = responseLogo.GetResponseStream())
                {
                    logo = Bitmap.FromStream(streamLogo);
                }

                PdfDiv divLogo = new PdfDiv();
                divLogo.Float = iTextSharp.text.pdf.PdfDiv.FloatType.LEFT;
                divLogo.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divLogo.BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID;
                divLogo.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                divLogo.Height = 60;
                divLogo.Width = 145;
                //divLogo.BackgroundColor = BaseColor.LIGHT_GRAY;
                iTextSharp.text.Image ImgLogo = iTextSharp.text.Image.GetInstance(logo, BaseColor.WHITE);
                divLogo.BackgroundImage = ImgLogo;
                //LOGO-------------------------------------------------------------------------------------------------------------

                //---------------------------------------------------------------------------------------------------------
                PdfDiv divAcercade = new PdfDiv();
                divAcercade.Float = iTextSharp.text.pdf.PdfDiv.FloatType.LEFT;
                divAcercade.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divAcercade.BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID;
                divAcercade.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divAcercade.BackgroundColor = BaseColor.LIGHT_GRAY;
                //divAcercade.PaddingLeft = 5;
                //divAcercade.PaddingRight = 10;
                divAcercade.Width = 140;

                #endregion

                #region Header

                PdfPTable Header = new PdfPTable(new float[] { 4.0f, 1.0f });
                Header.WidthPercentage = 100;

                PdfPTable tablaEmpresa = new PdfPTable(1);
                tablaEmpresa.WidthPercentage = 100;
                PdfPCell _datosEmpresa1 = new PdfPCell(new Phrase(string.Format("{0}\n{1}",
                    DsInvoiceAR.Tables["Company"].Rows[0]["Name"],
                    DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"]), fontTitle))
                { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER };
                PdfPCell _datosEmpresa2 = new PdfPCell(new Phrase(string.Format("{0}\n{1}\nTel: {2} Fax: {3}",
                    DsInvoiceAR.Tables["Company"].Rows[0]["Address1"],
                    DsInvoiceAR.Tables["Company"].Rows[0]["City"],
                    DsInvoiceAR.Tables["Company"].Rows[0]["PhoneNum"],
                    DsInvoiceAR.Tables["Company"].Rows[0]["FaxNum"]), fontCustom))
                { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER };
                tablaEmpresa.AddCell(_datosEmpresa1);
                tablaEmpresa.AddCell(_datosEmpresa2);

                PdfPTable tablaNotaCredito = new PdfPTable(1);
                tablaNotaCredito.WidthPercentage = 100;
                PdfPCell cellTituloNotasCredito = new PdfPCell(new Phrase("NOTA CREDITO", fontTitle)) { HorizontalAlignment = Element.ALIGN_CENTER };

                PdfPTable tablaDatosNotaCredito = new PdfPTable(new float[] { 0.5f, 1.0f });
                tablaDatosNotaCredito.WidthPercentage = 100;

                PdfPCell celTextNotaNumero = new PdfPCell(new Phrase("Numero:", fontTitle)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER };
                PdfPCell celValNotaNumero = new PdfPCell(new Phrase(DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceNum"].ToString(), fontTitle)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER };
                PdfPCell celTextNotaFecha = new PdfPCell(new Phrase("Fecha:", fontTitle)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER };
                PdfPCell celValNotaFecha = new PdfPCell(new Phrase(string.Format("{0:MM/dd/yyyy}", DateTime.Parse(DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"].ToString())), fontTitle)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER };
                PdfPCell celTextNotaPagina = new PdfPCell(new Phrase("Pagina:", fontTitle)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER };
                PdfPCell celValNotaPagina = new PdfPCell(new Phrase((document.PageNumber + 1).ToString(), fontTitle)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER };

                tablaDatosNotaCredito.AddCell(celTextNotaNumero);
                tablaDatosNotaCredito.AddCell(celValNotaNumero);
                tablaDatosNotaCredito.AddCell(celTextNotaFecha);
                tablaDatosNotaCredito.AddCell(celValNotaFecha);
                tablaDatosNotaCredito.AddCell(celTextNotaPagina);
                tablaDatosNotaCredito.AddCell(celValNotaPagina);

                PdfPCell cellDatosNotaCredito = new PdfPCell(tablaDatosNotaCredito);
                tablaNotaCredito.AddCell(cellTituloNotasCredito);
                tablaNotaCredito.AddCell(cellDatosNotaCredito);

                PdfPCell cellEmpresa = new PdfPCell(tablaEmpresa) { Border = 0 };
                PdfPCell cellNotaCredito = new PdfPCell(tablaNotaCredito) { Border = 0 };

                PdfPCell espacioHeader = new PdfPCell(new Phrase(" ", fontCustom)) { Border = 0, Colspan = 2 };

                Header.AddCell(cellEmpresa);
                Header.AddCell(cellNotaCredito);
                Header.AddCell(espacioHeader);



                #endregion

                #region Body

                PdfPTable Body = new PdfPTable(new float[] { 2.0f, 2.0f });
                Body.WidthPercentage = 100;

                PdfPTable tablaCliente = new PdfPTable(4);
                tablaCliente.WidthPercentage = 100;

                PdfPCell cellTextCliente = new PdfPCell(new Phrase("Cliente:", fontTitle)) { Border = 0, Colspan = 1 };
                PdfPCell cellValCliente = new PdfPCell(new Phrase(DsInvoiceAR.Tables["Customer"].Rows[0]["Name"].ToString(), fontTitle2)) { Border = 0, Colspan = 3 };
                PdfPCell cellTextContacto = new PdfPCell(new Phrase("Contacto:", fontTitle)) { Border = 0, Colspan = 1 };
                PdfPCell cellValContacto = new PdfPCell(new Phrase(DsInvoiceAR.Tables["Customer"].Rows[0]["EmailAddress"].ToString(), fontTitle2)) { Border = 0, Colspan = 3 };
                PdfPCell cellTextNit = new PdfPCell(new Phrase("Nit o C.C.:", fontTitle)) { Border = 0, Colspan = 1 };
                PdfPCell cellValNit = new PdfPCell(new Phrase(DsInvoiceAR.Tables["Customer"].Rows[0]["Company"].ToString(), fontTitle2)) { Border = 0, Colspan = 1 };
                PdfPCell cellTextCodigo = new PdfPCell(new Phrase("Código:", fontTitle)) { Border = 0, Colspan = 1 };
                PdfPCell cellValCodigo = new PdfPCell(new Phrase(DsInvoiceAR.Tables["Customer"].Rows[0]["CustNum"].ToString(), fontTitle2)) { Border = 0, Colspan = 1 };
                PdfPCell cellTextDireccion = new PdfPCell(new Phrase("Dirección:", fontTitle)) { Border = 0, Colspan = 1 };
                PdfPCell cellValDireccion = new PdfPCell(new Phrase(DsInvoiceAR.Tables["Customer"].Rows[0]["Address1"].ToString(), fontTitle2)) { Border = 0, Colspan = 3 };
                PdfPCell cellTextCiudad = new PdfPCell(new Phrase("Ciudad:", fontTitle)) { Border = 0, Colspan = 1 };
                PdfPCell cellValCiudad = new PdfPCell(new Phrase(DsInvoiceAR.Tables["Customer"].Rows[0]["City"].ToString(), fontTitle2)) { Border = 0, Colspan = 3 };
                PdfPCell cellTextTelefono = new PdfPCell(new Phrase("Teléfono:", fontTitle)) { Border = 0, Colspan = 1 };
                PdfPCell cellValTelefono = new PdfPCell(new Phrase(DsInvoiceAR.Tables["Customer"].Rows[0]["PhoneNum"].ToString(), fontTitle2)) { Border = 0, Colspan = 3 };
                PdfPCell cellTextFax = new PdfPCell(new Phrase("Fax:", fontTitle)) { Border = 0, Colspan = 1 };
                PdfPCell cellValFax = new PdfPCell(new Phrase(DsInvoiceAR.Tables["Customer"].Rows[0]["FaxNum"].ToString(), fontTitle2)) { Border = 0, Colspan = 3 };

                tablaCliente.AddCell(cellTextCliente);
                tablaCliente.AddCell(cellValCliente);
                tablaCliente.AddCell(cellTextContacto);
                tablaCliente.AddCell(cellValContacto);
                tablaCliente.AddCell(cellTextNit);
                tablaCliente.AddCell(cellValNit);
                tablaCliente.AddCell(cellTextCodigo);
                tablaCliente.AddCell(cellValCodigo);
                tablaCliente.AddCell(cellTextDireccion);
                tablaCliente.AddCell(cellValDireccion);
                tablaCliente.AddCell(cellTextCiudad);
                tablaCliente.AddCell(cellValCiudad);
                tablaCliente.AddCell(cellTextTelefono);
                tablaCliente.AddCell(cellValTelefono);
                tablaCliente.AddCell(cellTextFax);
                tablaCliente.AddCell(cellValFax);

                PdfPTable tablaDatosFactura = new PdfPTable(1);
                tablaDatosFactura.WidthPercentage = 100;

                PdfPTable tablaDatosFact1 = new PdfPTable(new float[] { 3.0f, 1.0f });
                PdfPCell cellTextFormaPago = new PdfPCell(new Phrase("Forma de Pago", fontTitle));
                PdfPCell cellTextFechaVencimiento = new PdfPCell(new Phrase("Fecha Vcto.", fontTitle));
                PdfPCell cellValFormaPago = new PdfPCell(new Phrase(DsInvoiceAR.Tables["Customer"].Rows[0]["TermsDescription"].ToString(), fontTitle2));
                PdfPCell cellValFechaVencimiento = new PdfPCell(new Phrase(string.Format("{0:MM/dd/yyyy}", DateTime.Parse(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"].ToString())), fontTitle2));
                tablaDatosFact1.AddCell(cellTextFormaPago);
                tablaDatosFact1.AddCell(cellTextFechaVencimiento);
                tablaDatosFact1.AddCell(cellValFormaPago);
                tablaDatosFact1.AddCell(cellValFechaVencimiento);

                PdfPTable tablaDatosFact2 = new PdfPTable(1);
                PdfPCell cellTextVendedor = new PdfPCell(new Phrase("Vendedor:", fontTitle));
                PdfPCell cellValVendedor = new PdfPCell(new Phrase(DsInvoiceAR.Tables["InvcHead"].Rows[0]["SalesRepName1"].ToString(), fontTitle2));
                tablaDatosFact2.AddCell(cellTextVendedor);
                tablaDatosFact2.AddCell(cellValVendedor);

                PdfPTable tablaDatosFact3 = new PdfPTable(new float[] { 1.0f, 1.0f, 1.0f, 1.5f });
                PdfPCell cellTextOrden = new PdfPCell(new Phrase("OC. Nro", fontTitle));
                PdfPCell cellTextDocumentoAlt = new PdfPCell(new Phrase("Docto.Alt.", fontTitle));
                PdfPCell cellTextFacturaBase = new PdfPCell(new Phrase("Factura Base", fontTitle));
                PdfPCell cellTextMoneda = new PdfPCell(new Phrase("Moneda", fontTitle));
                PdfPCell cellValOrden = new PdfPCell(new Phrase(DsInvoiceAR.Tables["InvcHead"].Rows[0]["OrderNum"].ToString(), fontTitle2));
                PdfPCell cellValDocumentoAlt = new PdfPCell(new Phrase(DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceRef"].ToString(), fontTitle2));
                PdfPCell cellValFacturaBase = new PdfPCell(new Phrase(DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceRef"].ToString(), fontTitle2));
                PdfPCell cellValMoneda = new PdfPCell(new Phrase(DsInvoiceAR.Tables["InvcHead"].Rows[0]["CurrencyCode"].ToString(), fontTitle2));
                tablaDatosFact3.AddCell(cellTextOrden);
                tablaDatosFact3.AddCell(cellTextDocumentoAlt);
                tablaDatosFact3.AddCell(cellTextFacturaBase);
                tablaDatosFact3.AddCell(cellTextMoneda);
                tablaDatosFact3.AddCell(cellValOrden);
                tablaDatosFact3.AddCell(cellValDocumentoAlt);
                tablaDatosFact3.AddCell(cellValFacturaBase);
                tablaDatosFact3.AddCell(cellValMoneda);

                PdfPCell cellDatosFact1 = new PdfPCell(tablaDatosFact1) { Border = 0 };
                PdfPCell cellDatosFact2 = new PdfPCell(tablaDatosFact2) { Border = 0 };
                PdfPCell cellDatosFact3 = new PdfPCell(tablaDatosFact3) { Border = 0 };
                tablaDatosFactura.AddCell(cellDatosFact1);
                tablaDatosFactura.AddCell(cellDatosFact2);
                tablaDatosFactura.AddCell(cellDatosFact3);

                PdfPCell cellCliente = new PdfPCell(tablaCliente);
                PdfPCell cellDatosFactura = new PdfPCell(tablaDatosFactura) { Border = 0 };

                Body.AddCell(cellCliente);
                Body.AddCell(cellDatosFactura);

                #endregion

                #region Unidades

                //Dimenciones.
                float[] DimencionUnidades = new float[8];
                DimencionUnidades[0] = 0.54f;//ARTICULO
                DimencionUnidades[1] = 0.05f;//MOT
                DimencionUnidades[2] = 0.05f;//UM
                DimencionUnidades[3] = 0.1f;//CODIGO
                DimencionUnidades[4] = 0.1f;//PRECIOUNT
                DimencionUnidades[5] = 0.1f;//DSCTO
                DimencionUnidades[6] = 0.1f;//VALORTOTAL
                DimencionUnidades[7] = 0.05f;//PORCENTAJEIVA

                PdfPTable Titulos_detalles = new PdfPTable(8);
                Titulos_detalles.WidthPercentage = 100;
                Titulos_detalles.SetWidths(DimencionUnidades);

                PdfPCell ARTICULO = new PdfPCell(new Phrase("Item", fontTitle)) { HorizontalAlignment = Element.ALIGN_LEFT, };
                PdfPCell MOT = new PdfPCell(new Phrase("Mot", fontTitle)) { HorizontalAlignment = Element.ALIGN_CENTER, };
                PdfPCell UM = new PdfPCell(new Phrase("U.M. ", fontTitle)) { HorizontalAlignment = Element.ALIGN_CENTER, };
                PdfPCell CANTIDAD = new PdfPCell(new Phrase("Cantidad", fontTitle)) { HorizontalAlignment = Element.ALIGN_CENTER, };
                PdfPCell PRECIOUNT = new PdfPCell(new Phrase("Precio Unit.", fontTitle)) { HorizontalAlignment = Element.ALIGN_CENTER, };
                PdfPCell DSCTO = new PdfPCell(new Phrase("Dscto.", fontTitle)) { HorizontalAlignment = Element.ALIGN_CENTER, };
                PdfPCell VALORTOTAL = new PdfPCell(new Phrase("Valor Total", fontTitle)) { HorizontalAlignment = Element.ALIGN_CENTER, };
                PdfPCell PORCENTAJEIVA = new PdfPCell(new Phrase("IVA %", fontTitle)) { HorizontalAlignment = Element.ALIGN_CENTER, };

                Titulos_detalles.AddCell(ARTICULO);
                Titulos_detalles.AddCell(MOT);
                Titulos_detalles.AddCell(UM);
                Titulos_detalles.AddCell(CANTIDAD);
                Titulos_detalles.AddCell(PRECIOUNT);
                Titulos_detalles.AddCell(DSCTO);
                Titulos_detalles.AddCell(VALORTOTAL);
                Titulos_detalles.AddCell(PORCENTAJEIVA);

                PdfPTable tableUnidades = new PdfPTable(8);
                tableUnidades.WidthPercentage = 100;
                tableUnidades.SetWidths(DimencionUnidades);
                decimal totalDescuento = 0;
                foreach (DataRow InvoiceLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
                {
                    ////confirmar del nodo InvcDtl
                    //if (bool.Parse((string)InvoiceLine["CheckBox01"]) == true)
                    //{

                    if (!AddUnidadesDoorsNotaCredito(InvoiceLine, ref tableUnidades, fontCustom, DsInvoiceAR))
                        return false;

                    totalDescuento += decimal.Parse((string)InvoiceLine["InvoiceLine"]);
                    //}
                }

                int numAdLineas = 10 - DsInvoiceAR.Tables["InvcDtl"].Rows.Count;
                PdfPTable tableEspacioUnidades = new PdfPTable(8);
                tableEspacioUnidades.WidthPercentage = 100;
                tableEspacioUnidades.SetWidths(DimencionUnidades);

                PdfPTable tableLineaFinal = new PdfPTable(8);
                tableLineaFinal.WidthPercentage = 100;
                tableLineaFinal.SetWidths(DimencionUnidades);
                iTextSharp.text.pdf.PdfPCell LineaFinal = new iTextSharp.text.pdf.PdfPCell();
                LineaFinal.Colspan = 6;
                LineaFinal.Padding = 3;
                LineaFinal.BorderColorBottom = BaseColor.WHITE;
                LineaFinal.HorizontalAlignment = Element.ALIGN_CENTER;
                LineaFinal.VerticalAlignment = Element.ALIGN_TOP;
                tableLineaFinal.AddCell(LineaFinal);

                #endregion

                #region Footer

                PdfPTable Footer = new PdfPTable(1);
                Footer.WidthPercentage = 100;

                PdfPTable tablaTotales = new PdfPTable(6);
                tablaTotales.WidthPercentage = 100;

                PdfPCell cellTextTotalBruto = new PdfPCell(new Phrase("Total Bruto", fontTitle)) { BorderWidthBottom = 0, HorizontalAlignment = Element.ALIGN_RIGHT };
                PdfPCell cellTextDescuento = new PdfPCell(new Phrase("Descuentos", fontTitle)) { BorderWidthBottom = 0, HorizontalAlignment = Element.ALIGN_RIGHT };
                PdfPCell cellTextSubtotal = new PdfPCell(new Phrase("Sub Total", fontTitle)) { BorderWidthBottom = 0, HorizontalAlignment = Element.ALIGN_RIGHT };
                PdfPCell cellTextValorIva = new PdfPCell(new Phrase("Vlr Impuestos", fontTitle)) { BorderWidthBottom = 0, HorizontalAlignment = Element.ALIGN_RIGHT };
                PdfPCell cellTextValorRefuente = new PdfPCell(new Phrase("Vlr Retención", fontTitle)) { BorderWidthBottom = 0, HorizontalAlignment = Element.ALIGN_RIGHT };
                PdfPCell cellTextValorTotal = new PdfPCell(new Phrase("Total", fontTitle)) { BorderWidthBottom = 0, HorizontalAlignment = Element.ALIGN_RIGHT };
                PdfPCell cellValTotalBruto = new PdfPCell(new Phrase(string.Format("{0:C2}", Convert.ToDecimal(DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number02"].ToString())), fontTitle2)) { BorderWidthTop = 0, HorizontalAlignment = Element.ALIGN_RIGHT };
                PdfPCell cellValDescuento = new PdfPCell(new Phrase(string.Format("{0:C2}", Convert.ToDecimal(DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number01"].ToString())), fontTitle2)) { BorderWidthTop = 0, HorizontalAlignment = Element.ALIGN_RIGHT };
                PdfPCell cellValSubtotal = new PdfPCell(new Phrase(string.Format("{0:C2}", Convert.ToDecimal(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubTotal"].ToString())), fontTitle2)) { BorderWidthTop = 0, HorizontalAlignment = Element.ALIGN_RIGHT };
                PdfPCell cellValValorIva = new PdfPCell(new Phrase(string.Format("{0:C2}", Convert.ToDecimal(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DocTaxAmt"].ToString())), fontTitle2)) { BorderWidthTop = 0, HorizontalAlignment = Element.ALIGN_RIGHT };
                PdfPCell cellValValorRefuente = new PdfPCell(new Phrase(string.Format("{0:C2}", Convert.ToDecimal(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DocWHTaxAmt"].ToString())), fontTitle2)) { BorderWidthTop = 0, HorizontalAlignment = Element.ALIGN_RIGHT };
                PdfPCell cellValValorTotal = new PdfPCell(new Phrase(string.Format("{0:C2}", Convert.ToDecimal(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"].ToString())), fontTitle2)) { BorderWidthTop = 0, HorizontalAlignment = Element.ALIGN_RIGHT };

                tablaTotales.AddCell(cellTextTotalBruto);
                tablaTotales.AddCell(cellTextDescuento);
                tablaTotales.AddCell(cellTextSubtotal);
                tablaTotales.AddCell(cellTextValorIva);
                tablaTotales.AddCell(cellTextValorRefuente);
                tablaTotales.AddCell(cellTextValorTotal);
                tablaTotales.AddCell(cellValTotalBruto);
                tablaTotales.AddCell(cellValDescuento);
                tablaTotales.AddCell(cellValSubtotal);
                tablaTotales.AddCell(cellValValorIva);
                tablaTotales.AddCell(cellValValorRefuente);
                tablaTotales.AddCell(cellValValorTotal);

                PdfPCell cellTotales = new PdfPCell(tablaTotales) { Border = 0 };

                PdfPTable tablaRetenciones = new PdfPTable(5);

                //Titulos
                PdfPCell cellTextRetenciones = new PdfPCell(new Phrase("Retención:", fontTitle)) { Border = 0, Colspan = 5 };
                PdfPCell cellTextIva = new PdfPCell(new Phrase("IVA", fontTitle2)) { Border = 0, Colspan = 1, HorizontalAlignment = Element.ALIGN_CENTER };
                PdfPCell cellTextReteIva = new PdfPCell(new Phrase("RETE IVA COMPRAS 15%", fontTitle2)) { Border = 0, Colspan = 1 };
                PdfPCell cellTextRenta = new PdfPCell(new Phrase("RENTA", fontTitle2)) { Border = 0, Colspan = 1, HorizontalAlignment = Element.ALIGN_CENTER };
                PdfPCell cellTextRetencionVenta = new PdfPCell(new Phrase("VENTAS 2.5%", fontTitle2)) { Border = 0, Colspan = 1 };
                //Valores
                PdfPCell cellValIva = new PdfPCell(new Phrase(string.Format("{0:C2}", Convert.ToDecimal(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DocTaxAmt"].ToString())), fontTitle2)) { Border = 0, Colspan = 1, HorizontalAlignment = Element.ALIGN_CENTER };
                PdfPCell cellValPorcentajeReteIva = new PdfPCell(new Phrase(string.Format("{0:N2}", Convert.ToDecimal(DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number03"].ToString())), fontTitle2)) { Border = 0, Colspan = 1, HorizontalAlignment = Element.ALIGN_CENTER };
                PdfPCell cellValTotalReteIva = new PdfPCell(new Phrase(string.Format("{0:C2}", Convert.ToDecimal(DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number04"].ToString())), fontTitle2)) { Border = 0, Colspan = 1, HorizontalAlignment = Element.ALIGN_CENTER };
                PdfPCell cellValRenta = new PdfPCell(new Phrase(string.Format("{0:C2}", Convert.ToDecimal(DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number05"].ToString())), fontTitle2)) { Border = 0, Colspan = 1, HorizontalAlignment = Element.ALIGN_CENTER };
                PdfPCell cellValPorcentajeRetencionVentas = new PdfPCell(new Phrase(string.Format("{0:N2}", Convert.ToDecimal(DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number06"].ToString())), fontTitle2)) { Border = 0, Colspan = 1, HorizontalAlignment = Element.ALIGN_CENTER };
                PdfPCell cellValTotalRetencionVentas = new PdfPCell(new Phrase(string.Format("{0:C2}", Convert.ToDecimal(DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number07"].ToString())), fontTitle2)) { Border = 0, Colspan = 1, HorizontalAlignment = Element.ALIGN_CENTER };

                //PdfPCell cellValIva = new PdfPCell(new Phrase("XxxxxX", fontTitle2)) { Border = 0, Colspan = 1, HorizontalAlignment = Element.ALIGN_CENTER };
                //PdfPCell cellValPorcentajeReteIva = new PdfPCell(new Phrase("XxxxxX", fontTitle2)) { Border = 0, Colspan = 1, HorizontalAlignment = Element.ALIGN_CENTER };
                //PdfPCell cellValTotalReteIva = new PdfPCell(new Phrase("XxxxxX", fontTitle2)) { Border = 0, Colspan = 1, HorizontalAlignment = Element.ALIGN_CENTER };
                //PdfPCell cellValRenta = new PdfPCell(new Phrase("XxxxxX", fontTitle2)) { Border = 0, Colspan = 1, HorizontalAlignment = Element.ALIGN_CENTER };
                //PdfPCell cellValPorcentajeRetencionVentas = new PdfPCell(new Phrase("XxxxxX", fontTitle2)) { Border = 0, Colspan = 1, HorizontalAlignment = Element.ALIGN_CENTER };
                //PdfPCell cellValTotalRetencionVentas = new PdfPCell(new Phrase("XxxxxX", fontTitle2)) { Border = 0, Colspan = 1, HorizontalAlignment = Element.ALIGN_CENTER };

                tablaRetenciones.AddCell(cellTextRetenciones);
                tablaRetenciones.AddCell(cellTextIva);
                tablaRetenciones.AddCell(cellTextReteIva);
                tablaRetenciones.AddCell(cellValIva);
                tablaRetenciones.AddCell(cellValPorcentajeReteIva);
                tablaRetenciones.AddCell(cellValTotalReteIva);
                tablaRetenciones.AddCell(cellTextRenta);
                tablaRetenciones.AddCell(cellTextRetencionVenta);
                tablaRetenciones.AddCell(cellValRenta);
                tablaRetenciones.AddCell(cellValPorcentajeRetencionVentas);
                tablaRetenciones.AddCell(cellValTotalRetencionVentas);

                PdfPCell cellRetenciones = new PdfPCell(tablaRetenciones);

                PdfPCell cellTextValorLetras = new PdfPCell(new Phrase("Valor letras: ", fontTitle)) { Border = 0 };

                PdfPCell cellValValorLetras = new PdfPCell(new Phrase(string.Format("{0}",
                Nroenletras(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"].ToString())), fontTitle2));
                cellValValorLetras.Border = 0;
                cellValValorLetras.HorizontalAlignment = Element.ALIGN_JUSTIFIED_ALL;

                PdfPCell cellObservqaciones = new PdfPCell(new Phrase(DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceComment"].ToString(), fontTitle2)) { Border = 0 };

                PdfPTable tablePie = new PdfPTable(7);

                PdfPCell cellTextResolucion = new PdfPCell(new Phrase("Resolución No.", fontTitle)) { Border = 0 };
                PdfPCell cellValResolucion = new PdfPCell(new Phrase(DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar01"].ToString(), fontTitle2)) { Border = 0 };
                PdfPCell cellTextFecha = new PdfPCell(new Phrase("Fecha", fontTitle)) { Border = 0 };
                PdfPCell cellValFecha = new PdfPCell(new Phrase(string.Format("{0:MM/dd/yyyy}", DateTime.Parse(DsInvoiceAR.Tables["InvcHead"].Rows[0]["Date01"].ToString())), fontTitle2)) { Border = 0 };
                PdfPCell cellTextRango = new PdfPCell(new Phrase("Facturar:", fontTitle)) { Border = 0 };
                PdfPCell cellValRango = new PdfPCell(new Phrase(string.Format("{0} al {1}",
                    DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar02"].ToString(), DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar03"].ToString()), fontTitle2))
                { Border = 0 };

                PdfPTable tableFirma = new PdfPTable(1);
                PdfPCell cellValFirma = new PdfPCell(new Phrase(" ", fontTitle2)) { Border = 0, BorderWidthBottom = 1 };
                PdfPCell cellTextFirma = new PdfPCell(new Phrase("(Firma y Sello)", fontTitle2)) { Border = 0 };
                tableFirma.AddCell(cellValFirma);
                tableFirma.AddCell(cellTextFirma);

                PdfPCell cellFirmas = new PdfPCell(tableFirma) { Border = 0 };

                tablePie.AddCell(cellTextResolucion);
                tablePie.AddCell(cellValResolucion);
                tablePie.AddCell(cellTextFecha);
                tablePie.AddCell(cellValFecha);
                tablePie.AddCell(cellTextRango);
                tablePie.AddCell(cellValRango);
                tablePie.AddCell(cellFirmas);

                PdfPCell cellPie = new PdfPCell(tablePie) { Border = 0 };


                PdfPCell cellEspacio = new PdfPCell(new Phrase(" ", fontCustom)) { Border = 0 };

                Footer.AddCell(cellTotales);
                Footer.AddCell(cellEspacio);
                Footer.AddCell(cellEspacio);
                Footer.AddCell(cellRetenciones);
                Footer.AddCell(cellEspacio);
                Footer.AddCell(cellTextValorLetras);
                Footer.AddCell(cellValValorLetras);
                Footer.AddCell(cellEspacio);
                Footer.AddCell(cellObservqaciones);
                Footer.AddCell(cellEspacio);
                Footer.AddCell(cellPie);

                #endregion

                #region Exit

                document.Add(Header);
                document.Add(salto);
                document.Add(Body);
                document.Add(salto);
                document.Add(salto);
                document.Add(Titulos_detalles);
                document.Add(tableUnidades);
                document.Add(tableEspacioUnidades);
                document.Add(tableLineaFinal);
                document.Add(Footer);

                //document.Add(divTextBancoCheque);
                /*PIE DE PAGINA*/
                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();
                RutaPdf = NomArchivo;
                return true;
            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.Message;
                return false;
            }


            #endregion
        }
        #endregion

    }
}