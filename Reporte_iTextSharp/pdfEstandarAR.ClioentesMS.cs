﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
//Referenciar y usar.
using System.Data;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Web;
using System.Windows.Forms;

namespace RulesServicesDIAN2.Adquiriente

{
    public partial class pdfEstandarAR
    {

        #region  lineas de unidfades 

        private bool AddUnidadesCMS(DataRow dataLine, ref PdfPTable table, iTextSharp.text.Font fontTitleFactura, DataSet dataSet)
        {
            try
            {
                //CODIGO
                strError += "Add InvoiceLine";
                iTextSharp.text.pdf.PdfPCell celValL = new iTextSharp.text.pdf.PdfPCell(new Phrase((string)dataLine["PartNumPartDescription"], fontTitleFactura));
                celValL.Colspan = 1;
                celValL.Padding = 2;
                celValL.Border = 0;
                //celTextL.BorderWidthRight = 1;
                //celValL.BorderWidthLeft = 1;
                celValL.HorizontalAlignment = Element.ALIGN_CENTER;
                celValL.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValL);
                strError += "InvoiceLine OK";

                //DESCRIPCION
                strError += "Add PartNum";
                iTextSharp.text.pdf.PdfPCell celValCodigo = new iTextSharp.text.pdf.PdfPCell(new Phrase((string)dataLine["PartNumPartDescription"], fontTitleFactura));
                celValCodigo.Colspan = 1;
                celValCodigo.Padding = 2;
                celValCodigo.Border = 0;
                //celValCodigo.BorderWidthLeft = 1;
                //celDescripcion.BorderColorBottom = BaseColor.WHITE;
                celValCodigo.HorizontalAlignment = Element.ALIGN_LEFT;
                celValCodigo.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValCodigo);
                strError += "Add PartNum OK";

                //UNIDADES
                strError += "Add SellingShipQty";
                iTextSharp.text.pdf.PdfPCell celValDescripcion = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    decimal.Parse((string)dataLine["SellingShipQty"]).ToString("N0"), fontTitleFactura));
                celValDescripcion.Colspan = 1;
                celValDescripcion.Padding = 2;
                celValDescripcion.Border = 0;
                //celValCantidad.BorderWidthLeft = 1;
                celValDescripcion.HorizontalAlignment = Element.ALIGN_CENTER;
                celValDescripcion.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValDescripcion);
                strError += "Add SellingShipQty OK";

                //PRECIO UNIDAD
                strError += "Add SalesUM";
                iTextSharp.text.pdf.PdfPCell celValUnidad = new iTextSharp.text.pdf.PdfPCell(new Phrase("$ " +
                    decimal.Parse((string)dataLine["DocUnitPrice"]).ToString("N3"), fontTitleFactura));
                celValUnidad.Colspan = 1;
                celValUnidad.Padding = 2;
                celValUnidad.Border = 0;
                //celValUnidad.BorderWidthLeft = 1;
                celValUnidad.HorizontalAlignment = Element.ALIGN_CENTER;
                celValUnidad.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValUnidad);
                strError += "Add SalesUM OK";

                //SUB TOTAL
                strError += "Add LineDesc";
                iTextSharp.text.pdf.PdfPCell celValCantidad = new iTextSharp.text.pdf.PdfPCell(new Phrase("$ " +
                    decimal.Parse((string)dataLine["DspDocExtPrice"]).ToString("N3"), fontTitleFactura));
                celValCantidad.Colspan = 1;
                celValCantidad.Padding = 2;
                celValCantidad.Border = 0;
                //celValDesc.BorderWidthLeft = 1;
                celValCantidad.HorizontalAlignment = Element.ALIGN_CENTER;
                celValCantidad.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValCantidad);
                strError += "Add LineDesc OK";
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return false;
            }
        }

        #endregion

        #region  Factura para clientes de mega sistemas 

        public bool FormatoCMS(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {
            NomArchivo = string.Empty;
            string Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            string RutaImg = $@"{AppDomain.CurrentDomain.BaseDirectory}\calidad3.png";
            NomArchivo = NombreInvoice + ".pdf";
            try
            {
                #region Head
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.
                Document document = new Document(iTextSharp.text.PageSize.LETTER);
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;

                // step 3: we open the document     
                document.Open();

                PdfDiv divEspacio = new PdfDiv();
                divEspacio.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio.Height = 10;
                divEspacio.Width = 130;

                PdfDiv divEspacio2 = new PdfDiv();
                divEspacio2.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio2.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio2.Height = 2;
                divEspacio2.Width = 130;

                //FUENTES----------------------------------------------------------------------------------------------------------
                iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle2 = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustom = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);
                //FUENTES---------------------------------------------------------------------------------------------------------

                ///objeto para el borde 
                RoundRectangle CelEventBorderRound = new RoundRectangle();


                System.Drawing.Image Logo2 = null;
                var request2 = WebRequest.Create(RutaImg);
                using (var response2 = request2.GetResponse())
                using (var stream2 = response2.GetResponseStream())
                {
                    Logo2 = Bitmap.FromStream(stream2);
                }
                //Logos--------------------------------
                iTextSharp.text.Image LogoPdf3 = iTextSharp.text.Image.GetInstance(Logo2, BaseColor.WHITE);
                LogoPdf3.ScaleAbsolute(90f, 90f);
                LogoPdf3.Border = 0;

                iTextSharp.text.Image QRPdf = iTextSharp.text.Image.GetInstance(QRInvoice, BaseColor.WHITE);
                QRPdf.ScaleAbsolute(60f, 60f);
                QRPdf.Border = 0;

                #endregion

                PdfPTable vacio = new PdfPTable(1);
                vacio.WidthPercentage = 100;
                PdfPCell vacio1 = new PdfPCell()
                {
                    MinimumHeight = 5,
                    Border = 0
                };
                vacio.AddCell(vacio1);

                #region Header

                PdfPTable Header = new PdfPTable(new float[] { 0.8f, 0.12f });
                Header.WidthPercentage = 100;

                PdfPCell cel_logo = new PdfPCell() { Border = 0 };
                //cel_logo.AddElement(LogoPdf3);
                Header.AddCell(cel_logo);
                PdfPCell cell_Qr = new PdfPCell() { Border = 0, };
                cell_Qr.AddElement(QRPdf);
                Header.AddCell(cell_Qr);

                PdfPTable Header_Company = new PdfPTable(3);
                Header_Company.WidthPercentage = 100;

                PdfPCell cell_1 = new PdfPCell() { Border = 0 };
                Header_Company.AddCell(cell_1);
                PdfPCell cell_info = new PdfPCell() { Border = 0 };
                Paragraph prgTitle = new Paragraph(string.Format("{0}{1}", DsInvoiceAR.Tables["Company"].Rows[0]["Name"] + "\n",
                $"Nit " + DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"]), fontCustom);
                Paragraph prgTitle2 = new Paragraph(string.Format("{0}\n{1}\nFRONTERADISTRIBUCIONES@GMAIL.COM\n{2}\n{3}\n",
                    DsInvoiceAR.Tables["Company"].Rows[0]["Address1"].ToString(),
                  $"" + DsInvoiceAR.Tables["Company"].Rows[0]["PhoneNum"].ToString() + $"  CEl:   " + DsInvoiceAR.Tables["Company"].Rows[0]["State"].ToString(),
                    DsInvoiceAR.Tables["Company"].Rows[0]["State"].ToString() + " - " + DsInvoiceAR.Tables["Company"].Rows[0]["Country"].ToString(),
                  $"ACTIVIDAD: 4631 \n"),
                    fontCustom);
                prgTitle.Alignment = Element.ALIGN_CENTER;
                prgTitle2.Alignment = Element.ALIGN_CENTER;
                cell_info.AddElement(prgTitle);
                cell_info.AddElement(prgTitle2);
                Header_Company.AddCell(cell_info);

                PdfPCell cell_factura = new PdfPCell() { Border = 0 };

                PdfPTable n_factura = new PdfPTable(1);
                n_factura.WidthPercentage = 100;
                PdfPCell cell_n_factura = new PdfPCell() { Border = 0, CellEvent = CelEventBorderRound, };
                Paragraph prgTitl3 = new Paragraph(string.Format("FACTURA DE VENTA No.", fontCustom));
                Paragraph prgTitle4 = new Paragraph(string.Format("" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"].ToString()), fontCustom);
                prgTitl3.Alignment = Element.ALIGN_CENTER;
                prgTitle4.Alignment = Element.ALIGN_CENTER;
                cell_n_factura.AddElement(prgTitl3);
                cell_n_factura.AddElement(prgTitle4);
                n_factura.AddCell(cell_n_factura);
                cell_factura.AddElement(n_factura);

                PdfPTable leyenda_1 = new PdfPTable(1);
                leyenda_1.WidthPercentage = 100;
                PdfPCell cell_leyenda_1 = new PdfPCell(new Phrase("I.V.A REGIMEN COMUN \n" +
                                                       " NO SOMOS GRANDES CONTRIBUYENTES \n" +
                                                       " NO SOMOS RETENEDORES DE I.V.A \n", fontCustom))
                { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, };
                leyenda_1.AddCell(cell_leyenda_1);
                cell_factura.AddElement(leyenda_1);

                Header_Company.AddCell(cell_factura);

                #endregion

                #region Body

                PdfPTable Body = new PdfPTable(new float[] { 4.5f, 0.05f, 4.5f, });
                Body.WidthPercentage = 100;

                PdfPCell cell_cliente_info = new PdfPCell() { Border = 0, CellEvent = CelEventBorderRound };

                Paragraph prgTitle5 = new Paragraph(string.Format(
                "\n{0}\n{1}\n{2}\n{3}\n{4}\n\n",
                $"SEÑORES: " + DsInvoiceAR.Tables["Customer"].Rows[0]["Name"].ToString() + $"                                     NIT.  " + DsInvoiceAR.Tables["Customer"].Rows[0]["CustID"].ToString(),
                $"CONTACTO:: " + DsInvoiceAR.Tables["Customer"].Rows[0]["ShortChar01"].ToString(),
                $"DIRECCIÓN: " + DsInvoiceAR.Tables["Customer"].Rows[0]["Address1"].ToString(),
                $"TELÉFONO: " + DsInvoiceAR.Tables["Customer"].Rows[0]["PhoneNum"].ToString(),
                $"CIUDAD: " + DsInvoiceAR.Tables["Customer"].Rows[0]["City"].ToString()),
                fontCustom);
                prgTitle5.Alignment = Element.ALIGN_LEFT;
                cell_cliente_info.AddElement(prgTitle5);
                Body.AddCell(cell_cliente_info);

                PdfPCell cell_line = new PdfPCell() { Border = 0, };
                Body.AddCell(cell_line);

                PdfPCell cell_fecha = new PdfPCell() { Border = 0, };

                PdfPTable f_x = new PdfPTable(3);
                f_x.WidthPercentage = 100;

                PdfPCell cell_f_x = new PdfPCell() { Border = 0, CellEvent = CelEventBorderRound };
                Paragraph prgTitle6 = new Paragraph(string.Format(
                "FECHA DE EXPEDICION\n{0}\nMES  DIA  AÑO\n",
                $"" + DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).ToString("dd  mm  yyyy")),
                fontCustom);
                prgTitle6.Alignment = Element.ALIGN_CENTER;
                cell_f_x.AddElement(prgTitle6);
                f_x.AddCell(cell_f_x);

                PdfPCell cell_f_v = new PdfPCell() { Border = 0, CellEvent = CelEventBorderRound };
                Paragraph prgTitle7 = new Paragraph(string.Format(
                "VENCIMIENTO\n{0}\nMES  DIA  AÑO\n",
                $"" + DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"]).ToString("dd  mm  yyyy")),
                fontCustom);
                prgTitle7.Alignment = Element.ALIGN_CENTER;
                cell_f_v.AddElement(prgTitle7);
                f_x.AddCell(cell_f_v);

                PdfPCell cell_f_p = new PdfPCell() { Border = 0, CellEvent = CelEventBorderRound };
                Paragraph prgTitle8 = new Paragraph(string.Format(
                "PEDIDO\n{0}\n",
                $"" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["OrderNum"]),
                fontCustom);
                prgTitle8.Alignment = Element.ALIGN_CENTER;
                cell_f_p.AddElement(prgTitle8);
                f_x.AddCell(cell_f_p);
                cell_fecha.AddElement(f_x);

                PdfPTable f_x2 = new PdfPTable(3);
                f_x2.WidthPercentage = 100;

                PdfPCell cell_f_x2 = new PdfPCell() { Border = 0, CellEvent = CelEventBorderRound, };
                Paragraph prgTitle62 = new Paragraph(string.Format("CENTRO DE COSTO\n{0}\n\n", $"" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar02"]), fontCustom);
                prgTitle62.Alignment = Element.ALIGN_CENTER;
                cell_f_x2.AddElement(prgTitle62);
                f_x2.AddCell(cell_f_x2);

                PdfPCell cell_f_v2 = new PdfPCell() { Border = 0, CellEvent = CelEventBorderRound };
                Paragraph prgTitle72 = new Paragraph(string.Format("REMISION\n{0}\n\n", $"" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar03"]), fontCustom);
                prgTitle72.Alignment = Element.ALIGN_CENTER;
                cell_f_v2.AddElement(prgTitle72);
                f_x2.AddCell(cell_f_v2);

                PdfPCell cell_f_p2 = new PdfPCell() { Border = 0, CellEvent = CelEventBorderRound };
                Paragraph prgTitle82 = new Paragraph(string.Format("COND. DE PAGO\n{0}\n", $"" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar04"]), fontCustom);
                prgTitle82.Alignment = Element.ALIGN_CENTER;
                cell_f_p2.AddElement(prgTitle82);
                f_x2.AddCell(cell_f_p2);

                cell_fecha.AddElement(f_x2);

                Body.AddCell(cell_fecha);


                PdfPTable Body_info = new PdfPTable(1);
                Body_info.WidthPercentage = 100;

                PdfPCell cell_Bosy_info = new PdfPCell() { Border = 0, CellEvent = CelEventBorderRound, MinimumHeight = 15, };

                PdfPTable VD = new PdfPTable(3);
                VD.WidthPercentage = 100;
                PdfPCell cell_vendedor = new PdfPCell(new Phrase("VENDEDOR: " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["SalesRepName1"], fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT };
                VD.AddCell(cell_vendedor);
                PdfPCell cell_zona = new PdfPCell(new Phrase("ZONA: " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar01"], fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT };
                VD.AddCell(cell_zona);
                PdfPCell cell_tranportador = new PdfPCell(new Phrase("TRANSPORTADOR: " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar05"], fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT };
                VD.AddCell(cell_tranportador);
                cell_Bosy_info.AddElement(VD);

                PdfPTable DE = new PdfPTable(3);
                DE.WidthPercentage = 100;
                PdfPCell cell_venvioio = new PdfPCell(new Phrase("DIRECCION ENVIO: " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar06"], fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT };
                DE.AddCell(cell_venvioio);
                PdfPCell cell_DETALLE = new PdfPCell(new Phrase("DETALLE: " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["Character01"], fontCustom)) {Colspan=2, Border = 0, HorizontalAlignment = Element.ALIGN_LEFT };
                DE.AddCell(cell_DETALLE);
                cell_Bosy_info.AddElement(DE);

                Body_info.AddCell(cell_Bosy_info);

                PdfPTable resolucion = new PdfPTable(2);
                resolucion.WidthPercentage = 100;

                PdfPCell cell_resolucion = new PdfPCell(new Phrase("RESOLUCION DE FACTURACION No.: 984375934DS de 2012/02/15", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT };
                resolucion.AddCell(cell_resolucion);

                PdfPCell r_factura = new PdfPCell(new Phrase("RANGO DE FACTURACION : 00000000000001 - 00000000009000", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT };
                resolucion.AddCell(r_factura);

                #endregion

                #region Unidades

                PdfPTable titulos_unidades = new PdfPTable(new float[] { 0.15f, 0.45f, 0.10f, 0.15f, 0.15f, });
                titulos_unidades.WidthPercentage = 100;

                PdfPCell REFERENCIA = new PdfPCell(new Phrase("REFERENCIA", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, MinimumHeight = 15, };
                titulos_unidades.AddCell(REFERENCIA);
                PdfPCell DESCRIPCION = new PdfPCell(new Phrase("DESCRIPCION", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP };
                titulos_unidades.AddCell(DESCRIPCION);
                PdfPCell CANTIDAD = new PdfPCell(new Phrase("CANTIDAD", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP };
                titulos_unidades.AddCell(CANTIDAD);
                PdfPCell VALORUNITARIO = new PdfPCell(new Phrase("VALOR UNITARIO", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP };
                titulos_unidades.AddCell(VALORUNITARIO);
                PdfPCell VALORTOTAL = new PdfPCell(new Phrase("VALOR TOTAL", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP };
                titulos_unidades.AddCell(VALORTOTAL);

                PdfPTable tableTituloUnidades = new PdfPTable(5);
                //Dimenciones.
                float[] DimencionUnidades = new float[5];
                DimencionUnidades[0] = 0.15f;//CÓDIGO
                DimencionUnidades[1] = 0.45f;//DESCRIPCION
                DimencionUnidades[2] = 0.10f;//CANTIDAD
                DimencionUnidades[3] = 0.15f;//DTO
                DimencionUnidades[4] = 0.15f;//PRECIO


                PdfPTable tableUnidades = new PdfPTable(5);
                tableUnidades.WidthPercentage = 100;
                tableUnidades.SetWidths(DimencionUnidades);

                decimal unidades = 0;

                foreach (DataRow InvoiceLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
                {
                    if (!AddUnidadesCMS(InvoiceLine, ref tableUnidades, fontCustom, DsInvoiceAR))
                        return false;

                    unidades += decimal.Parse((string)InvoiceLine["SellingShipQty"]);
                }

                iTextSharp.text.pdf.PdfPCell LineaFinal = new iTextSharp.text.pdf.PdfPCell();
                LineaFinal.Colspan = 5;
                LineaFinal.Padding = 3;
                //LineaFinal.Border = 1;
                LineaFinal.BorderColorBottom = BaseColor.WHITE;
                LineaFinal.HorizontalAlignment = Element.ALIGN_CENTER;
                LineaFinal.VerticalAlignment = Element.ALIGN_TOP;
                tableUnidades.AddCell(LineaFinal);


                #endregion

                #region Footer

                PdfPTable Footer = new PdfPTable(new float[] { 0.35f, 0.3f, 0.35f });
                Footer.WidthPercentage = 100;

                PdfPCell ob = new PdfPCell() { MinimumHeight = 50, };

                PdfPTable obs = new PdfPTable(1);
                obs.WidthPercentage =100;
                PdfPCell cell_obs = new PdfPCell(new Phrase("OBSERVACIONES:\n"
                    + DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceComment"].ToString()
                    , fontCustom))
                { Border=0,HorizontalAlignment=Element.ALIGN_LEFT,VerticalAlignment=Element.ALIGN_TOP,MinimumHeight=70,};
                obs.AddCell(cell_obs);
                ob.AddElement(obs);

                PdfPTable son = new PdfPTable(1);
                son.WidthPercentage = 100;
                PdfPCell cell_son = new PdfPCell(new Phrase(
                    $"Son: **{Nroenletras((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"])}**"
                    , fontCustom))
                { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP, MinimumHeight = 30, };
                son.AddCell(cell_son);
                ob.AddElement(son);

                Footer.AddCell(ob);

                PdfPCell footer_fecha = new PdfPCell();

                PdfPTable table_f_medio = new PdfPTable(1);
                table_f_medio.WidthPercentage = 100;
                PdfPCell leyenda = new PdfPCell(new Phrase(
                    "DAMOS POR ACEPTADA LA FACTURA EN EL \n" +
                    "MOMENTO EN QUE UNA PERSONA DE LA \n" +
                    "EMPRESA FIRME \n\n\n" +
                    "FIRMA______________________________________\n\n" +
                    "NIT O C.C.:________________________________\n\n"
                    , fontCustom))
                {
                    Border=0,

                };
                table_f_medio.AddCell(leyenda);
                footer_fecha.AddElement(table_f_medio);

                PdfPTable F = new PdfPTable(6);
                F.WidthPercentage = 100;
                PdfPCell cell_F0 = new PdfPCell(new Phrase("FECHA: ", fontCustom)) { Border = 0,  Colspan=2, MinimumHeight=25,HorizontalAlignment=Element.ALIGN_LEFT,VerticalAlignment=Element.ALIGN_TOP};
                PdfPCell cell_F1 = new PdfPCell(new Phrase("MES ", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_BOTTOM };
                PdfPCell cell_F2 = new PdfPCell(new Phrase("DÍA ", fontCustom)) { HorizontalAlignment=Element.ALIGN_CENTER,VerticalAlignment=Element.ALIGN_BOTTOM };
                PdfPCell cell_F3 = new PdfPCell(new Phrase("AÑO ", fontCustom)) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_BOTTOM };
                PdfPCell cell_F4 = new PdfPCell(new Phrase(" ", fontCustom)) { Border = 0, };
                F.AddCell(cell_F0);
                F.AddCell(cell_F1);
                F.AddCell(cell_F2);
                F.AddCell(cell_F3);
                F.AddCell(cell_F4);
                footer_fecha.AddElement(F);

                PdfPTable Ff = new PdfPTable(1);
                Ff.WidthPercentage = 100;
                PdfPCell cell_Ff = new PdfPCell(new Phrase("RECIBI CONFORME - FIRMA, FECHA Y SELLO ", fontCustom)) { Border = 0, MinimumHeight = 15, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP };
                Ff.AddCell(cell_Ff);
                footer_fecha.AddElement(Ff);

                Footer.AddCell(footer_fecha);

                PdfPCell totales = new PdfPCell();

                PdfPTable SUBTOTAL = new PdfPTable(2);
                SUBTOTAL.WidthPercentage = 100;
                PdfPCell cell_SUBTOTAL = new PdfPCell(new Phrase("SUBTOTAL",fontCustom)) { HorizontalAlignment=Element.ALIGN_LEFT };
                PdfPCell cell_SUBTOTALv = new PdfPCell(new Phrase(string.Format("{0:C2}", Convert.ToDecimal(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubTotal"].ToString())), fontCustom)) { HorizontalAlignment = Element.ALIGN_RIGHT, MinimumHeight = 13, Padding = 0, };
                SUBTOTAL.AddCell(cell_SUBTOTAL);
                SUBTOTAL.AddCell(cell_SUBTOTALv);
                totales.AddElement(SUBTOTAL);

                PdfPTable DESCUENTO = new PdfPTable(2);
                DESCUENTO.WidthPercentage = 100;
                PdfPCell cell_DESCUENTO = new PdfPCell(new Phrase("DESCUENTO", fontCustom)) { HorizontalAlignment = Element.ALIGN_LEFT };
                PdfPCell cell_DESCUENTOLv = new PdfPCell(new Phrase("$ "+Decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number01"]).ToString("N0"), fontCustom)) { HorizontalAlignment = Element.ALIGN_RIGHT, MinimumHeight = 13, Padding = 0, };
                DESCUENTO.AddCell(cell_DESCUENTO);
                DESCUENTO.AddCell(cell_DESCUENTOLv);
                totales.AddElement(DESCUENTO);

                PdfPTable DCTO = new PdfPTable(2);
                DCTO.WidthPercentage = 100;
                PdfPCell cell_DCTO = new PdfPCell(new Phrase("SUBTOTAL-DCTO", fontCustom)) { HorizontalAlignment = Element.ALIGN_LEFT };
                PdfPCell cell_DCTOv = new PdfPCell(new Phrase("$ " + Decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"]).ToString("N0"), fontCustom)) { HorizontalAlignment = Element.ALIGN_RIGHT, MinimumHeight = 13, Padding = 0, };
                DCTO.AddCell(cell_DCTO);
                DCTO.AddCell(cell_DCTOv);
                totales.AddElement(DCTO);

                PdfPTable IVA = new PdfPTable(2);
                IVA.WidthPercentage = 100;
                PdfPCell cell_IVA = new PdfPCell(new Phrase("I.V.A", fontCustom)) { HorizontalAlignment = Element.ALIGN_LEFT };
                PdfPCell cell_IVAv = new PdfPCell(new Phrase("$ " + Decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DocTaxAmt"]).ToString("N0"), fontCustom)) { HorizontalAlignment = Element.ALIGN_RIGHT, MinimumHeight = 13, Padding = 0, };
                IVA.AddCell(cell_IVA);
                IVA.AddCell(cell_IVAv);
                totales.AddElement(IVA);

                PdfPTable TOTAL = new PdfPTable(2);
                TOTAL.WidthPercentage = 100;
                PdfPCell cell_TOTAL = new PdfPCell(new Phrase("TOTAL", fontCustom)) { HorizontalAlignment = Element.ALIGN_LEFT };
                PdfPCell cell_TOTALv = new PdfPCell(new Phrase("$ " + Decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"]).ToString("N0"), fontCustom)) { HorizontalAlignment = Element.ALIGN_RIGHT, MinimumHeight = 13, Padding = 0, };
                TOTAL.AddCell(cell_TOTAL);
                TOTAL.AddCell(cell_TOTALv);
                totales.AddElement(TOTAL);

                PdfPTable RTE = new PdfPTable(2);
                RTE.WidthPercentage = 100;
                PdfPCell cell_RTE = new PdfPCell(new Phrase("RTE.FTE", fontCustom)) { HorizontalAlignment = Element.ALIGN_LEFT };
                PdfPCell cell_RTEv = new PdfPCell(new Phrase(string.Format("{0:C2}", Convert.ToDecimal(GetValImpuestoByID("0B", DsInvoiceAR).ToString())), fontCustom)) { HorizontalAlignment = Element.ALIGN_RIGHT, MinimumHeight = 13, Padding = 0, };
                RTE.AddCell(cell_RTE);
                RTE.AddCell(cell_RTEv);
                totales.AddElement(RTE);

                PdfPTable RTRTEIVAE = new PdfPTable(2);
                RTRTEIVAE.WidthPercentage = 100;
                PdfPCell cell_RTRTEIVAE = new PdfPCell(new Phrase("RTE.IVA", fontCustom)) { HorizontalAlignment = Element.ALIGN_LEFT };
                PdfPCell cell_RTRTEIVAEv = new PdfPCell(new Phrase(string.Format("{0:C2}", Convert.ToDecimal(GetValImpuestoByID("0C", DsInvoiceAR).ToString())), fontCustom)) { HorizontalAlignment = Element.ALIGN_RIGHT, MinimumHeight = 13, Padding = 0, };
                RTRTEIVAE.AddCell(cell_RTRTEIVAE);
                RTRTEIVAE.AddCell(cell_RTRTEIVAEv);
                totales.AddElement(RTRTEIVAE);

                PdfPTable RICA = new PdfPTable(2);
                RICA.WidthPercentage = 100;
                decimal RFuenteIca = GetValImpuestoByID("0C", DsInvoiceAR);
                PdfPCell cell_RICA = new PdfPCell(new Phrase("RTE.ICA", fontCustom)) { HorizontalAlignment = Element.ALIGN_LEFT };
                PdfPCell cell_RICAv = new PdfPCell(new Phrase("$ "+RFuenteIca.ToString("N2"), fontCustom)) { HorizontalAlignment = Element.ALIGN_RIGHT, MinimumHeight = 13, Padding = 0, };
                RICA.AddCell(cell_RICA);
                RICA.AddCell(cell_RICAv);
                totales.AddElement(RICA);

                PdfPTable NETO = new PdfPTable(2);
                NETO.WidthPercentage = 100;
                PdfPCell cell_NETO = new PdfPCell(new Phrase("NETO", fontCustom)) { HorizontalAlignment = Element.ALIGN_LEFT };
                PdfPCell cell_NETOv = new PdfPCell(new Phrase("$ " + Decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"]).ToString("N0"), fontCustom)) { HorizontalAlignment = Element.ALIGN_RIGHT, MinimumHeight= 13, Padding=0, };
                NETO.AddCell(cell_NETO);
                NETO.AddCell(cell_NETOv);
                totales.AddElement(NETO);


                Footer.AddCell(totales);


                PdfPTable fin = new PdfPTable(1);
                fin.WidthPercentage = 100;
                PdfPCell cell_fin = new PdfPCell(new Phrase("Factura impresa por computador por: Megasistemas S.A.S. - Nit: 800009741-2", fontCustom)) { Border=0,HorizontalAlignment=Element.ALIGN_CENTER};
                fin.AddCell(cell_fin);

                #endregion

                #region Exit
                document.Add(Header);
                document.Add(Header_Company);
                document.Add(Body);
                document.Add(vacio);
                document.Add(Body_info);
                document.Add(resolucion);
                document.Add(cell_resolucion);
                document.Add(titulos_unidades);
                document.Add(tableTituloUnidades);
                document.Add(tableUnidades);
                document.Add(vacio);
                document.Add(vacio);
                document.Add(vacio);
                document.Add(vacio);
                document.Add(Footer);
                document.Add(fin);

                /*PIE DE PAGINA*/
                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();
                return true;
                #endregion
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return false;
            }
        }

        #endregion


    }
}
