﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
//Referenciar y usar.
using System.Data;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Web;
using System.Windows.Forms;

namespace RulesServicesDIAN2.Adquiriente
{
    public partial class pdfEstandarAR
    {
        #region Formatos Inovak

        private bool AddUnidadesinnovak(DataRow dataLine, ref PdfPTable table, iTextSharp.text.Font fontTitleFactura, DataSet dataSet)
        {
            try
            {
                //CODIGO
                strError += "Add InvoiceLine";
                iTextSharp.text.pdf.PdfPCell celValL = new iTextSharp.text.pdf.PdfPCell(new Phrase((string)dataLine["PartNum"], fontTitleFactura));
                celValL.Colspan = 1;
                celValL.Padding = 2;
                celValL.Border = 0;
                celValL.Border = PdfPCell.RIGHT_BORDER | PdfPCell.LEFT_BORDER;
                //celTextL.BorderWidthRight = 1;
                //celValL.BorderWidthLeft = 1;
                celValL.HorizontalAlignment = Element.ALIGN_CENTER;
                celValL.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValL);
                strError += "InvoiceLine OK";

                //DESCRIPCION
                strError += "Add PartNum";
                iTextSharp.text.pdf.PdfPCell celValCodigo = new iTextSharp.text.pdf.PdfPCell(new Phrase((string)dataLine["PartNumPartDescription"], fontTitleFactura));
                celValCodigo.Colspan = 1;
                celValCodigo.Padding = 2;
                celValCodigo.Border = 0;
                celValCodigo.Border = PdfPCell.RIGHT_BORDER;
                //celValCodigo.BorderWidthLeft = 1;
                //celDescripcion.BorderColorBottom = BaseColor.WHITE;
                celValCodigo.HorizontalAlignment = Element.ALIGN_CENTER;
                celValCodigo.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValCodigo);
                strError += "Add PartNum OK";

                //UNIDADES
                strError += "Add SellingShipQty";
                iTextSharp.text.pdf.PdfPCell celValDescripcion = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    decimal.Parse((string)dataLine["InvoiceLine"]).ToString("N0"), fontTitleFactura));
                celValDescripcion.Colspan = 1;
                celValDescripcion.Padding = 2;
                celValDescripcion.Border = 0;
                celValDescripcion.Border = PdfPCell.RIGHT_BORDER;
                //celValCantidad.BorderWidthLeft = 1;
                celValDescripcion.HorizontalAlignment = Element.ALIGN_CENTER;
                celValDescripcion.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValDescripcion);
                strError += "Add SellingShipQty OK";

                //PRECIO UNIDAD
                strError += "Add SalesUM";
                iTextSharp.text.pdf.PdfPCell celValUnidad = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    dataLine["SalesUM"].ToString(), fontTitleFactura));
                celValUnidad.Colspan = 1;
                celValUnidad.Padding = 2;
                celValUnidad.Border = 0;
                celValUnidad.Border = PdfPCell.RIGHT_BORDER;
                //celValUnidad.BorderWidthLeft = 1;
                celValUnidad.HorizontalAlignment = Element.ALIGN_CENTER;
                celValUnidad.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValUnidad);
                strError += "Add SalesUM OK";

                //SUB TOTAL
                strError += "Add LineDesc";
                iTextSharp.text.pdf.PdfPCell celValCantidad = new iTextSharp.text.pdf.PdfPCell(new Phrase("$ " +
                    decimal.Parse((string)dataLine["DspDocExtPrice"]).ToString("N0"), fontTitleFactura));
                celValCantidad.Colspan = 1;
                celValCantidad.Padding = 2;
                celValCantidad.Border = 0;
                celValCantidad.Border = PdfPCell.RIGHT_BORDER;
                //celValDesc.BorderWidthLeft = 1;
                celValCantidad.HorizontalAlignment = Element.ALIGN_CENTER;
                celValCantidad.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValCantidad);
                strError += "Add LineDesc OK";

                //SUB TOTAL
                strError += "Add LineDesc";
                iTextSharp.text.pdf.PdfPCell celValCantidad2 = new iTextSharp.text.pdf.PdfPCell(new Phrase("$ " +
                    decimal.Parse((string)dataLine["DspDocExtPrice"]).ToString("N0"), fontTitleFactura));
                celValCantidad2.Colspan = 1;
                celValCantidad2.Padding = 2;
                celValCantidad2.Border = 0;
                celValCantidad2.Border = PdfPCell.RIGHT_BORDER;
                //celValDesc.BorderWidthLeft = 1;
                celValCantidad2.HorizontalAlignment = Element.ALIGN_CENTER;
                celValCantidad2.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValCantidad2);
                strError += "Add LineDesc OK";

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return false;
            }
        }

        public bool NotaCreditoinnovak(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {
            NomArchivo = string.Empty;
            string Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            string RutaImg = $@"{AppDomain.CurrentDomain.BaseDirectory}\LOGO_innovak.png";
            NomArchivo = NombreInvoice + ".pdf";
            try
            {
                #region Head
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.
                Document document = new Document(iTextSharp.text.PageSize.LETTER);
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;

                // step 3: we open the document     
                document.Open();

                PdfDiv divEspacio = new PdfDiv();
                divEspacio.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio.Height = 10;
                divEspacio.Width = 130;

                PdfDiv divEspacio2 = new PdfDiv();
                divEspacio2.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio2.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio2.Height = 2;
                divEspacio2.Width = 130;

                //FUENTES----------------------------------------------------------------------------------------------------------
                iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle2 = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle3 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 9, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustom = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitleFactura = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitleFactura2 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 9, iTextSharp.text.Font.NORMAL);
                //FUENTES---------------------------------------------------------------------------------------------------------

                System.Drawing.Image logo = null;
                var requestLogo = WebRequest.Create(RutaImg);

                using (var responseLogo = requestLogo.GetResponse())
                using (var streamLogo = responseLogo.GetResponseStream())
                {
                    logo = Bitmap.FromStream(streamLogo);
                }

                System.Drawing.Image logo2 = null;
                var requestLogo2 = WebRequest.Create(RutaImg);

                using (var responseLogo2 = requestLogo2.GetResponse())
                using (var streamLogo2 = responseLogo2.GetResponseStream())
                {
                    logo2 = Bitmap.FromStream(streamLogo2);
                }

                #endregion

                #region ENCABEZADO

                PdfPTable Header = new PdfPTable(4);
                Header.WidthPercentage = 100;

                //logo
                PdfPCell cell_logo = new PdfPCell() { Border = 0, };
                iTextSharp.text.Image ImgLogo2 = iTextSharp.text.Image.GetInstance(logo2, BaseColor.WHITE);
                ImgLogo2.ScaleAbsolute(100, 100);
                cell_logo.AddElement(ImgLogo2);
                Header.AddCell(cell_logo);

                //informacion de la empresa
                PdfPCell cell_info = new PdfPCell() { Border = 0, };
                Paragraph prgTitle = new Paragraph(string.Format("{0}\n{1}", DsInvoiceAR.Tables["Company"].Rows[0]["Name"],
               $"NIT. " + DsInvoiceAR.Tables["Company"].Rows[0]["Name"]), fontTitle);
                Paragraph prgTitle2 = new Paragraph(string.Format("{0}\n{1}\n{2} - {3}",
                    DsInvoiceAR.Tables["Company"].Rows[0]["Address1"].ToString(),
                    DsInvoiceAR.Tables["Company"].Rows[0]["PhoneNum"].ToString(),
                    DsInvoiceAR.Tables["Company"].Rows[0]["City"].ToString(),
                    DsInvoiceAR.Tables["Company"].Rows[0]["State"].ToString()),
                    fontTitle);
                prgTitle.Alignment = Element.ALIGN_LEFT;
                prgTitle2.Alignment = Element.ALIGN_LEFT;
                cell_info.AddElement(prgTitle);
                cell_info.AddElement(prgTitle2);
                Header.AddCell(cell_info);
                //Qr
                PdfPCell cell_Qr = new PdfPCell() { Border = 0, };
                iTextSharp.text.Image ImgQR = iTextSharp.text.Image.GetInstance(QRInvoice, BaseColor.WHITE);
                ImgQR.ScaleAbsolute(70, 70);
                cell_Qr.AddElement(ImgQR);
                Header.AddCell(cell_Qr);
                //tipo de de documento
                PdfPCell cell_factura = new PdfPCell() { Border = 0, };
                PdfPTable tableFactura = new PdfPTable(3);
                //Dimenciones.
                float[] DimencionFactura = new float[3];
                DimencionFactura[0] = 1.0F;//
                DimencionFactura[1] = 4.0F;//
                DimencionFactura[2] = 0.5F;//

                tableFactura.WidthPercentage = 100;
                tableFactura.SetWidths(DimencionFactura);

                iTextSharp.text.pdf.PdfPCell celTittle = new iTextSharp.text.pdf.PdfPCell(new Phrase("NOTA CREDITO\n\n", fontTitleFactura));
                celTittle.Colspan = 3;
                celTittle.Padding = 3;
                celTittle.HorizontalAlignment = Element.ALIGN_CENTER;
                celTittle.VerticalAlignment = Element.ALIGN_TOP;
                celTittle.Border = 0;
                celTittle.BorderWidthTop = 1;
                celTittle.BorderWidthLeft = 1;
                celTittle.BorderWidthRight = 1;
                celTittle.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableFactura.AddCell(celTittle);

                iTextSharp.text.pdf.PdfPCell celNo = new iTextSharp.text.pdf.PdfPCell(new Phrase("No. ", fontTitleFactura));
                celNo.Colspan = 1;
                celNo.Padding = 5;
                celNo.HorizontalAlignment = Element.ALIGN_CENTER;
                celNo.VerticalAlignment = Element.ALIGN_TOP;
                celNo.Border = 0;
                celNo.BorderWidthLeft = 1;
                celNo.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableFactura.AddCell(celNo);

                string NumLegalFactura = string.Empty;
                if (InvoiceType == "InvoiceType")
                    NumLegalFactura = DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceRef"].ToString();
                else if (InvoiceType == "CreditNoteType")
                    NumLegalFactura = DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"].ToString();
                else
                    NumLegalFactura = DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"].ToString();

                iTextSharp.text.pdf.PdfPCell celNoFactura = new iTextSharp.text.pdf.PdfPCell(new Phrase(NumLegalFactura, fontTitleFactura));
                celNoFactura.Colspan = 1;
                celNoFactura.Padding = 5;
                celNoFactura.HorizontalAlignment = Element.ALIGN_LEFT;
                celNoFactura.VerticalAlignment = Element.ALIGN_TOP;
                //celNoFactura.Border = 1;
                celNoFactura.BackgroundColor = BaseColor.WHITE;
                tableFactura.AddCell(celNoFactura);


                iTextSharp.text.pdf.PdfPCell celEspacioNoFactura = new iTextSharp.text.pdf.PdfPCell(new Phrase(" ", fontTitleFactura));
                celEspacioNoFactura.Colspan = 1;
                celEspacioNoFactura.Border = 0;
                //celNo.Padding = 3;
                celEspacioNoFactura.BorderWidthRight = 1;
                celEspacioNoFactura.HorizontalAlignment = Element.ALIGN_CENTER;
                celEspacioNoFactura.VerticalAlignment = Element.ALIGN_TOP;
                celEspacioNoFactura.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableFactura.AddCell(celEspacioNoFactura);

                iTextSharp.text.pdf.PdfPCell celRellenoNoFactura = new iTextSharp.text.pdf.PdfPCell(new Phrase(" ", fontTitleFactura));
                celRellenoNoFactura.Colspan = 3;
                celRellenoNoFactura.Padding = 3;
                celRellenoNoFactura.Border = 0;
                celRellenoNoFactura.BorderWidthLeft = 1;
                celRellenoNoFactura.BorderWidthRight = 1;
                celRellenoNoFactura.BorderWidthBottom = 1;
                celRellenoNoFactura.BackgroundColor = BaseColor.LIGHT_GRAY;
                celRellenoNoFactura.HorizontalAlignment = Element.ALIGN_RIGHT;
                celRellenoNoFactura.VerticalAlignment = Element.ALIGN_TOP;
                tableFactura.AddCell(celRellenoNoFactura);

                iTextSharp.text.pdf.PdfPCell celConsecutivoInterno = new iTextSharp.text.pdf.PdfPCell(new Phrase("xxxxxxxxxx", fontCustom));
                celConsecutivoInterno.Colspan = 4;
                celConsecutivoInterno.BackgroundColor = BaseColor.WHITE;
                celConsecutivoInterno.HorizontalAlignment = Element.ALIGN_RIGHT;
                celConsecutivoInterno.VerticalAlignment = Element.ALIGN_TOP;
                celConsecutivoInterno.Border = 0;
                tableFactura.AddCell(celConsecutivoInterno);

                PdfDiv divNumeroFactura = new PdfDiv();
                divNumeroFactura.Float = iTextSharp.text.pdf.PdfDiv.FloatType.LEFT;
                divNumeroFactura.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divNumeroFactura.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                divNumeroFactura.PaddingLeft = 5;
                divNumeroFactura.Width = 140;
                divNumeroFactura.AddElement(tableFactura);
                cell_factura.AddElement(tableFactura);
                Header.AddCell(cell_factura);


                #endregion

                #region FACTURAR Y DESPACHAR A
                //------------------------------------------------------------------------------------------------
                PdfPTable tableFacturar = new PdfPTable(3);
                //Dimenciones.
                float[] DimencionFacturar = new float[3];
                DimencionFacturar[0] = 1.0F;//
                DimencionFacturar[1] = 0.01F;//
                DimencionFacturar[2] = 1.0F;//

                tableFacturar.WidthPercentage = 100;
                tableFacturar.SetWidths(DimencionFacturar);
                //----------------------------------------------------------------------------------------------
                PdfPTable tableFacturarA = new PdfPTable(2);
                float[] DimencionFacturarA = new float[2];
                DimencionFacturarA[0] = 0.8F;//
                DimencionFacturarA[1] = 2.0F;//

                tableFacturarA.WidthPercentage = 100;
                tableFacturarA.SetWidths(DimencionFacturarA);

                iTextSharp.text.pdf.PdfPCell celDatosFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase("FACTURAR A:\n", fontTitleFactura));
                celDatosFacturarA.Colspan = 2;
                celDatosFacturarA.Padding = 3;
                celDatosFacturarA.Border = 0;
                celDatosFacturarA.BorderColorBottom = BaseColor.WHITE;
                celDatosFacturarA.HorizontalAlignment = Element.ALIGN_CENTER;
                celDatosFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celDatosFacturarA);

                iTextSharp.text.pdf.PdfPCell celTextClienteFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase("CLIENTE:", fontTitleFactura));
                celTextClienteFacturarA.Colspan = 1;
                celTextClienteFacturarA.Padding = 3;
                celTextClienteFacturarA.Border = 0;
                celTextClienteFacturarA.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextClienteFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celTextClienteFacturarA);

                iTextSharp.text.pdf.PdfPCell celClienteFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase(DsInvoiceAR.Tables["InvcHead"].Rows[0]["CustomerName"].ToString(),
                    fontTitle2));
                celClienteFacturarA.Colspan = 1;
                celClienteFacturarA.Padding = 3;
                celClienteFacturarA.Border = 0;
                celClienteFacturarA.HorizontalAlignment = Element.ALIGN_LEFT;
                celClienteFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celClienteFacturarA);

                iTextSharp.text.pdf.PdfPCell celTextNitFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase("NIT:", fontTitleFactura));
                celTextNitFacturarA.Colspan = 1;
                celTextNitFacturarA.Padding = 3;
                celTextNitFacturarA.Border = 0;
                celTextNitFacturarA.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextNitFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celTextNitFacturarA);

                iTextSharp.text.pdf.PdfPCell celNitFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase("xxxxxxxxx", fontTitle2));
                celNitFacturarA.Colspan = 1;
                celNitFacturarA.Padding = 3;
                celNitFacturarA.Border = 0;
                celNitFacturarA.HorizontalAlignment = Element.ALIGN_LEFT;
                celNitFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celNitFacturarA);

                iTextSharp.text.pdf.PdfPCell celTextDireccionFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase("DIRECCION:", fontTitleFactura));
                celTextDireccionFacturarA.Colspan = 1;
                celTextDireccionFacturarA.Padding = 3;
                celTextDireccionFacturarA.Border = 0;
                celTextDireccionFacturarA.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextDireccionFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celTextDireccionFacturarA);

                iTextSharp.text.pdf.PdfPCell celDireccionFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase("xxxxxxxxxx", fontTitle2));
                celDireccionFacturarA.Colspan = 1;
                celDireccionFacturarA.Padding = 3;
                celDireccionFacturarA.Border = 0;
                celDireccionFacturarA.HorizontalAlignment = Element.ALIGN_LEFT;
                celDireccionFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celDireccionFacturarA);

                iTextSharp.text.pdf.PdfPCell celTextTelFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase("TELEFONO:", fontTitleFactura));
                celTextTelFacturarA.Colspan = 1;
                celTextTelFacturarA.Padding = 3;
                celTextTelFacturarA.Border = 0;
                celTextTelFacturarA.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextTelFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celTextTelFacturarA);

                iTextSharp.text.pdf.PdfPCell celTelFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase("xxxxxxxxxxxx", fontTitle2));
                celTelFacturarA.Colspan = 1;
                celTelFacturarA.Padding = 3;
                celTelFacturarA.Border = 0;
                celTelFacturarA.HorizontalAlignment = Element.ALIGN_LEFT;
                celTelFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celTelFacturarA);

                iTextSharp.text.pdf.PdfPCell celTextCiudadFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase("CIUDAD:", fontTitleFactura));
                celTextCiudadFacturarA.Colspan = 1;
                celTextCiudadFacturarA.Padding = 3;
                celTextCiudadFacturarA.Border = 0;
                celTextCiudadFacturarA.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextCiudadFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celTextCiudadFacturarA);

                iTextSharp.text.pdf.PdfPCell celCiudadFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase("xxxxxxxxxx", fontTitle2));
                celCiudadFacturarA.Colspan = 1;
                celCiudadFacturarA.Padding = 3;
                celCiudadFacturarA.Border = 0;
                celCiudadFacturarA.HorizontalAlignment = Element.ALIGN_LEFT;
                celCiudadFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celCiudadFacturarA);

                iTextSharp.text.pdf.PdfPCell celTextPaisFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase("PAIS:", fontTitleFactura));
                celTextPaisFacturarA.Colspan = 1;
                celTextPaisFacturarA.Padding = 3;
                celTextPaisFacturarA.Border = 0;
                celTextPaisFacturarA.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextPaisFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celTextPaisFacturarA);

                iTextSharp.text.pdf.PdfPCell celPaisFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase("xxxxxxxxxxxxxx", fontTitle2));
                celPaisFacturarA.Colspan = 1;
                celPaisFacturarA.Padding = 3;
                celPaisFacturarA.Border = 0;
                celPaisFacturarA.HorizontalAlignment = Element.ALIGN_LEFT;
                celPaisFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celPaisFacturarA);

                iTextSharp.text.pdf.PdfPCell celTittleFacturarA = new iTextSharp.text.pdf.PdfPCell(tableFacturarA);
                tableFacturar.AddCell(celTittleFacturarA);
                //----------------------------------------------------------------------------------------------------------------------------

                iTextSharp.text.pdf.PdfPCell celEspacio2 = new iTextSharp.text.pdf.PdfPCell(new Phrase(" ", fontTitleFactura));
                celEspacio2.Border = 0;
                tableFacturar.AddCell(celEspacio2);

                //------------------------------------------------------------------------------------------

                PdfPTable tableDespacharA = new PdfPTable(2);
                float[] DimencionDespacharA = new float[2];
                DimencionDespacharA[0] = 0.8F;//
                DimencionDespacharA[1] = 2.0F;//

                tableDespacharA.WidthPercentage = 100;
                tableDespacharA.SetWidths(DimencionDespacharA);

                iTextSharp.text.pdf.PdfPCell celTittleDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase("DESPACHAR A:\n", fontTitleFactura));
                celTittleDespacharA.Colspan = 2;
                celTittleDespacharA.Padding = 3;
                celTittleDespacharA.Border = 0;
                celTittleDespacharA.BorderColorBottom = BaseColor.WHITE;
                celTittleDespacharA.HorizontalAlignment = Element.ALIGN_CENTER;
                celTittleDespacharA.VerticalAlignment = Element.ALIGN_TOP;
                tableDespacharA.AddCell(celTittleDespacharA);

                iTextSharp.text.pdf.PdfPCell celTextClienteDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase("CLIENTE:", fontTitleFactura));
                celTextClienteDespacharA.Colspan = 1;
                celTextClienteDespacharA.Padding = 3;
                celTextClienteDespacharA.Border = 0;
                celTextClienteDespacharA.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextClienteDespacharA.VerticalAlignment = Element.ALIGN_TOP;
                tableDespacharA.AddCell(celTextClienteDespacharA);

                iTextSharp.text.pdf.PdfPCell celClienteDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase("xxxxxxxxxxxxx", fontTitle2));
                celClienteDespacharA.Colspan = 1;
                celClienteDespacharA.Padding = 3;
                celClienteDespacharA.Border = 0;
                celClienteDespacharA.HorizontalAlignment = Element.ALIGN_LEFT;
                celClienteDespacharA.VerticalAlignment = Element.ALIGN_TOP;
                tableDespacharA.AddCell(celClienteDespacharA);

                iTextSharp.text.pdf.PdfPCell celTextNitDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase("NIT:", fontTitleFactura));
                celTextNitDespacharA.Colspan = 1;
                celTextNitDespacharA.Padding = 3;
                celTextNitDespacharA.Border = 0;
                celTextNitDespacharA.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextNitDespacharA.VerticalAlignment = Element.ALIGN_TOP;
                tableDespacharA.AddCell(celTextNitDespacharA);

                iTextSharp.text.pdf.PdfPCell celNitDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase("xxxxxxxxxx", fontTitle2));
                celNitDespacharA.Colspan = 1;
                celNitDespacharA.Padding = 3;
                celNitDespacharA.Border = 0;
                celNitDespacharA.HorizontalAlignment = Element.ALIGN_LEFT;
                celNitDespacharA.VerticalAlignment = Element.ALIGN_TOP;
                tableDespacharA.AddCell(celNitDespacharA);

                iTextSharp.text.pdf.PdfPCell celTextDireccionDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase("DIRECCION:", fontTitleFactura));
                celTextDireccionDespacharA.Colspan = 1;
                celTextDireccionDespacharA.Padding = 3;
                celTextDireccionDespacharA.Border = 0;
                celTextDireccionDespacharA.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextDireccionDespacharA.VerticalAlignment = Element.ALIGN_TOP;
                tableDespacharA.AddCell(celTextDireccionDespacharA);

                iTextSharp.text.pdf.PdfPCell celDireccionDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase("xxxxxxxxxxx", fontTitle2));
                celDireccionDespacharA.Colspan = 1;
                celDireccionDespacharA.Padding = 3;
                celDireccionDespacharA.Border = 0;
                celDireccionDespacharA.HorizontalAlignment = Element.ALIGN_LEFT;
                celDireccionDespacharA.VerticalAlignment = Element.ALIGN_TOP;
                tableDespacharA.AddCell(celDireccionDespacharA);

                iTextSharp.text.pdf.PdfPCell celTextTelDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase("TELEFONO:", fontTitleFactura));
                celTextTelDespacharA.Colspan = 1;
                celTextTelDespacharA.Padding = 3;
                celTextTelDespacharA.Border = 0;
                celTextTelDespacharA.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextTelDespacharA.VerticalAlignment = Element.ALIGN_TOP;
                tableDespacharA.AddCell(celTextTelDespacharA);

                iTextSharp.text.pdf.PdfPCell celTelDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase("xxxxxxxxxx", fontTitle2));
                celTelDespacharA.Colspan = 1;
                celTelDespacharA.Padding = 3;
                celTelDespacharA.Border = 0;
                celTelDespacharA.HorizontalAlignment = Element.ALIGN_LEFT;
                celTelDespacharA.VerticalAlignment = Element.ALIGN_TOP;
                tableDespacharA.AddCell(celTelDespacharA);

                iTextSharp.text.pdf.PdfPCell celTextCiudadDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase("CIUDAD:", fontTitleFactura));
                celTextCiudadDespacharA.Colspan = 1;
                celTextCiudadDespacharA.Padding = 3;
                celTextCiudadDespacharA.Border = 0;
                celTextCiudadDespacharA.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextCiudadDespacharA.VerticalAlignment = Element.ALIGN_TOP;
                tableDespacharA.AddCell(celTextCiudadDespacharA);

                iTextSharp.text.pdf.PdfPCell celCiudadDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase("xxxxxxxxxxxxx", fontTitle2));
                celCiudadDespacharA.Colspan = 1;
                celCiudadDespacharA.Padding = 3;
                celCiudadDespacharA.Border = 0;
                celCiudadDespacharA.HorizontalAlignment = Element.ALIGN_LEFT;
                celCiudadDespacharA.VerticalAlignment = Element.ALIGN_TOP;
                tableDespacharA.AddCell(celCiudadDespacharA);

                iTextSharp.text.pdf.PdfPCell celTextPaisDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase("PAIS:", fontTitleFactura));
                celTextPaisDespacharA.Colspan = 1;
                celTextPaisDespacharA.Padding = 3;
                celTextPaisDespacharA.Border = 0;
                celTextPaisDespacharA.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextPaisDespacharA.VerticalAlignment = Element.ALIGN_TOP;
                tableDespacharA.AddCell(celTextPaisDespacharA);

                iTextSharp.text.pdf.PdfPCell celPaisDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase("xxxxxxxxxxxxx", fontTitle2));
                celPaisDespacharA.Colspan = 1;
                celPaisDespacharA.Padding = 3;
                celPaisDespacharA.Border = 0;
                celPaisDespacharA.HorizontalAlignment = Element.ALIGN_LEFT;
                celPaisDespacharA.VerticalAlignment = Element.ALIGN_TOP;
                tableDespacharA.AddCell(celPaisDespacharA);

                iTextSharp.text.pdf.PdfPCell celDatosDespacharA = new iTextSharp.text.pdf.PdfPCell(tableDespacharA);
                tableFacturar.AddCell(celDatosDespacharA);
                //-------------------------------------------------------------------------------------- 
                #endregion

                #region Tabla de Detalles
                //------------------------------------------------------------------------------------------------
                PdfPTable tableDetalles2 = new PdfPTable(4);
                tableDetalles2.WidthPercentage = 100;

                PdfPTable tableFechaFactura = new PdfPTable(2);
                float[] dimecionesTablaFecha = new float[2];
                dimecionesTablaFecha[0] = 1.3F;
                dimecionesTablaFecha[1] = 0.9F;
                tableFechaFactura.SetWidths(dimecionesTablaFecha);

                iTextSharp.text.pdf.PdfPCell celTextFechaFactura = new iTextSharp.text.pdf.PdfPCell(new Phrase("FECHA FACTURA: ",
                    fontTitle));
                celTextFechaFactura.Colspan = 1;
                celTextFechaFactura.Padding = 7;
                celTextFechaFactura.Border = 0;
                celTextFechaFactura.BorderColorBottom = BaseColor.WHITE;
                celTextFechaFactura.HorizontalAlignment = Element.ALIGN_CENTER;
                celTextFechaFactura.VerticalAlignment = Element.ALIGN_CENTER;
                tableFechaFactura.AddCell(celTextFechaFactura);

                iTextSharp.text.pdf.PdfPCell celFechaFactura = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:MM/dd/yyyy}", DateTime.Parse(DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"].ToString())),
                fontCustom));
                celFechaFactura.Colspan = 1;
                celFechaFactura.Padding = 7;
                //celFechaFactura.PaddingTop = 4;
                celFechaFactura.Border = 0;
                celFechaFactura.BorderColorBottom = BaseColor.WHITE;
                celFechaFactura.HorizontalAlignment = Element.ALIGN_LEFT;
                celFechaFactura.VerticalAlignment = Element.ALIGN_CENTER;
                tableFechaFactura.AddCell(celFechaFactura);

                PdfPCell _celFechaFactura = new PdfPCell(tableFechaFactura);
                tableDetalles2.AddCell(_celFechaFactura);


                PdfPTable tableFechaVencimiento = new PdfPTable(2);
                float[] dmTablaFechaVencimiento = new float[2];
                dmTablaFechaVencimiento[0] = 1.4F;
                dmTablaFechaVencimiento[1] = 0.8F;
                tableFechaVencimiento.SetWidths(dmTablaFechaVencimiento);

                iTextSharp.text.pdf.PdfPCell celTextFechaVencimiento = new iTextSharp.text.pdf.PdfPCell(new Phrase("FECHA VENCIMIENTO: ",
                    fontTitle));
                celTextFechaVencimiento.Colspan = 1;
                celTextFechaVencimiento.PaddingTop = 7;
                celTextFechaVencimiento.Border = 0;
                celTextFechaVencimiento.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextFechaVencimiento.VerticalAlignment = Element.ALIGN_TOP;
                tableFechaVencimiento.AddCell(celTextFechaVencimiento);

                iTextSharp.text.pdf.PdfPCell celFechaVencimiento = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:MM/dd/yyyy}",
                   DateTime.Parse(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"].ToString())),
                fontCustom));
                celFechaVencimiento.Colspan = 1;
                celFechaVencimiento.Padding = 7;
                celFechaVencimiento.Border = 0;
                celFechaVencimiento.BorderColorBottom = BaseColor.WHITE;
                celFechaVencimiento.HorizontalAlignment = Element.ALIGN_LEFT;
                celFechaVencimiento.VerticalAlignment = Element.ALIGN_TOP;
                tableFechaVencimiento.AddCell(celFechaVencimiento);

                PdfPCell _celFechaFacturaVencimiento = new PdfPCell(tableFechaVencimiento);
                tableDetalles2.AddCell(_celFechaFacturaVencimiento);

                PdfPTable tableMoneda = new PdfPTable(2);
                float[] dimecionesTablaMoneda = new float[2];
                dimecionesTablaMoneda[0] = 0.6F;
                dimecionesTablaMoneda[1] = 1.0F;
                tableMoneda.SetWidths(dimecionesTablaMoneda);

                iTextSharp.text.pdf.PdfPCell celTextMoneda = new iTextSharp.text.pdf.PdfPCell(new Phrase("MONEDA: ",
                    fontTitle));
                celTextMoneda.Colspan = 1;
                celTextMoneda.Padding = 7;
                celTextMoneda.Border = 0;
                celTextMoneda.BorderColorBottom = BaseColor.WHITE;
                celTextMoneda.HorizontalAlignment = Element.ALIGN_RIGHT;
                celTextMoneda.VerticalAlignment = Element.ALIGN_TOP;
                tableMoneda.AddCell(celTextMoneda);

                iTextSharp.text.pdf.PdfPCell celMoneda = new iTextSharp.text.pdf.PdfPCell(new Phrase(DsInvoiceAR.Tables["InvcHead"].Rows[0]["CurrencyCode"].ToString(),
                fontCustom));
                celMoneda.Colspan = 1;
                celMoneda.Padding = 7;
                celMoneda.Border = 0;
                celMoneda.BorderColorBottom = BaseColor.WHITE;
                celMoneda.HorizontalAlignment = Element.ALIGN_LEFT;
                celMoneda.VerticalAlignment = Element.ALIGN_TOP;
                tableMoneda.AddCell(celMoneda);

                PdfPCell _celMoneda = new PdfPCell(tableMoneda);
                tableDetalles2.AddCell(_celMoneda);

                PdfPTable tableFormaPago = new PdfPTable(2);
                float[] dimecionesTablaFormaPago = new float[2];
                dimecionesTablaFormaPago[0] = 0.4F;
                dimecionesTablaFormaPago[1] = 1.0F;
                tableFormaPago.SetWidths(dimecionesTablaFormaPago);

                iTextSharp.text.pdf.PdfPCell celTextFormaPago = new iTextSharp.text.pdf.PdfPCell(new Phrase("FORMA DE PAGO: ",
                    fontTitle));
                celTextFormaPago.Colspan = 2;
                celTextFormaPago.Padding = 7;
                celTextFormaPago.Border = 0;
                celTextFormaPago.BorderColorBottom = BaseColor.WHITE;
                celTextFormaPago.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextFormaPago.VerticalAlignment = Element.ALIGN_TOP;
                tableFormaPago.AddCell(celTextFormaPago);


                PdfPCell _celFormaPago = new PdfPCell(tableFormaPago);
                tableDetalles2.AddCell(_celFormaPago);

                //-----------------------------------------------------------------------------------------
                PdfPTable tableDetalles = new PdfPTable(5);
                tableDetalles.PaddingTop = 20;
                //Dimenciones.
                float[] DimencionDetalles = new float[5];
                DimencionDetalles[0] = 2.2F;//
                DimencionDetalles[1] = 1.0F;//
                DimencionDetalles[2] = 1.0F;//
                DimencionDetalles[3] = 1.3F;//
                DimencionDetalles[4] = 1.0F;//

                tableDetalles.WidthPercentage = 100;
                tableDetalles.SetWidths(DimencionDetalles);

                iTextSharp.text.pdf.PdfPCell celVendedor = new iTextSharp.text.pdf.PdfPCell(new Phrase("VENDEDOR", fontTitleFactura));
                celVendedor.Colspan = 1;
                celVendedor.Padding = 3;
                //celVendedor.Border = 0;
                celVendedor.BorderColorBottom = BaseColor.WHITE;
                celVendedor.HorizontalAlignment = Element.ALIGN_CENTER;
                celVendedor.VerticalAlignment = Element.ALIGN_TOP;
                celVendedor.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableDetalles.AddCell(celVendedor);

                iTextSharp.text.pdf.PdfPCell celOC_Cliente = new iTextSharp.text.pdf.PdfPCell(new Phrase("O.C.", fontTitleFactura));
                celOC_Cliente.Colspan = 1;
                celOC_Cliente.Padding = 3;
                //celVendedor.Border = 0;
                celOC_Cliente.BorderColorBottom = BaseColor.WHITE;
                celOC_Cliente.HorizontalAlignment = Element.ALIGN_CENTER;
                celOC_Cliente.VerticalAlignment = Element.ALIGN_TOP;
                celOC_Cliente.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableDetalles.AddCell(celOC_Cliente);

                iTextSharp.text.pdf.PdfPCell celOrdenVenta = new iTextSharp.text.pdf.PdfPCell(new Phrase("ORDEN DE VENTA", fontTitleFactura));
                celOrdenVenta.Colspan = 1;
                celOrdenVenta.Padding = 3;
                //celVendedor.Border = 0;
                celOrdenVenta.BorderColorBottom = BaseColor.WHITE;
                celOrdenVenta.HorizontalAlignment = Element.ALIGN_CENTER;
                celOrdenVenta.VerticalAlignment = Element.ALIGN_TOP;
                celOrdenVenta.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableDetalles.AddCell(celOrdenVenta);

                iTextSharp.text.pdf.PdfPCell celLocalizacion = new iTextSharp.text.pdf.PdfPCell(new Phrase("LOCALIZACIÓN", fontTitleFactura));
                celLocalizacion.Colspan = 1;
                celLocalizacion.Padding = 3;
                //celVendedor.Border = 0;
                celLocalizacion.BackgroundColor = BaseColor.LIGHT_GRAY;
                celLocalizacion.BorderColorBottom = BaseColor.WHITE;
                celLocalizacion.HorizontalAlignment = Element.ALIGN_CENTER;
                celLocalizacion.VerticalAlignment = Element.ALIGN_TOP;
                tableDetalles.AddCell(celLocalizacion);

                iTextSharp.text.pdf.PdfPCell celRemision = new iTextSharp.text.pdf.PdfPCell(new Phrase("REMISIÓN", fontTitleFactura));
                celRemision.Colspan = 1;
                celRemision.Padding = 3;
                celRemision.BackgroundColor = BaseColor.LIGHT_GRAY;
                //celVendedor.Border = 0;
                celRemision.BorderColorBottom = BaseColor.WHITE;
                celRemision.HorizontalAlignment = Element.ALIGN_CENTER;
                celRemision.VerticalAlignment = Element.ALIGN_TOP;
                tableDetalles.AddCell(celRemision);

                iTextSharp.text.pdf.PdfPCell celDataVendedor = new iTextSharp.text.pdf.PdfPCell(new Phrase("xxxxxxxxxxx", fontCustom));
                celDataVendedor.Colspan = 1;
                celDataVendedor.Padding = 3;
                //celVendedor.Border = 0;
                celDataVendedor.BorderColorBottom = BaseColor.WHITE;
                celDataVendedor.HorizontalAlignment = Element.ALIGN_CENTER;
                celDataVendedor.VerticalAlignment = Element.ALIGN_TOP;
                tableDetalles.AddCell(celDataVendedor);

                iTextSharp.text.pdf.PdfPCell celDataOC_Cliente = new iTextSharp.text.pdf.PdfPCell(new Phrase("xxxxxxxxxx", fontCustom));
                celDataOC_Cliente.Colspan = 1;
                celDataOC_Cliente.Padding = 3;
                //celVendedor.Border = 0;
                celDataOC_Cliente.BorderColorBottom = BaseColor.WHITE;
                celDataOC_Cliente.HorizontalAlignment = Element.ALIGN_CENTER;
                celDataOC_Cliente.VerticalAlignment = Element.ALIGN_TOP;
                tableDetalles.AddCell(celDataOC_Cliente);

                iTextSharp.text.pdf.PdfPCell celDataOrdenVenta = new iTextSharp.text.pdf.PdfPCell(new Phrase(DsInvoiceAR.Tables["InvcHead"].Rows[0]["OrderNum"].ToString(),
                    fontCustom));
                celDataOrdenVenta.Colspan = 1;
                celDataOrdenVenta.Padding = 3;
                //celVendedor.Border = 0;
                celDataOrdenVenta.BorderColorBottom = BaseColor.WHITE;
                celDataOrdenVenta.HorizontalAlignment = Element.ALIGN_CENTER;
                celDataOrdenVenta.VerticalAlignment = Element.ALIGN_TOP;
                tableDetalles.AddCell(celDataOrdenVenta);

                iTextSharp.text.pdf.PdfPCell celDataCondicionEntrega = new iTextSharp.text.pdf.PdfPCell(new Phrase("xxxxxxxxxxxxxxxx", fontCustom));
                celDataCondicionEntrega.Colspan = 1;
                celDataCondicionEntrega.Padding = 3;
                //celVendedor.Border = 0;
                celDataCondicionEntrega.BorderColorBottom = BaseColor.WHITE;
                celDataCondicionEntrega.HorizontalAlignment = Element.ALIGN_CENTER;
                celDataCondicionEntrega.VerticalAlignment = Element.ALIGN_TOP;
                tableDetalles.AddCell(celDataCondicionEntrega);

                iTextSharp.text.pdf.PdfPCell celDataTerminosPago = new iTextSharp.text.pdf.PdfPCell(new Phrase("xxxxxxxxxxxx", fontCustom));
                celDataTerminosPago.Colspan = 1;
                celDataTerminosPago.Padding = 3;
                //celVendedor.Border = 0;
                celDataTerminosPago.BorderColorBottom = BaseColor.WHITE;
                celDataTerminosPago.HorizontalAlignment = Element.ALIGN_CENTER;
                celDataTerminosPago.VerticalAlignment = Element.ALIGN_TOP;
                tableDetalles.AddCell(celDataTerminosPago);
                #endregion

                #region Tabla Unidades
                PdfPTable tableTituloUnidades = new PdfPTable(8);
                //Dimenciones.
                float[] DimencionUnidades = new float[8];
                DimencionUnidades[0] = 0.5F;//codigo
                DimencionUnidades[1] = 2.0F;//descripcion
                DimencionUnidades[2] = 0.8F;//cantidad
                DimencionUnidades[3] = 0.3F;//um
                DimencionUnidades[4] = 1.0F;//valor unitario
                DimencionUnidades[5] = 1.2F;//descuentos
                DimencionUnidades[6] = 1.2F;//
                DimencionUnidades[7] = 0.3F;//

                tableTituloUnidades.WidthPercentage = 100;
                tableTituloUnidades.SetWidths(DimencionUnidades);

                iTextSharp.text.pdf.PdfPCell celCodigo = new iTextSharp.text.pdf.PdfPCell(new Phrase("CÓDIGO", fontTitleFactura));
                celCodigo.Colspan = 1;
                celCodigo.PaddingTop = 6;
                //celCodigo.Border = 1;
                celCodigo.BorderWidthRight = 0;
                celCodigo.BackgroundColor = BaseColor.LIGHT_GRAY;
                celCodigo.HorizontalAlignment = Element.ALIGN_CENTER;
                celCodigo.VerticalAlignment = Element.ALIGN_TOP;

                tableTituloUnidades.AddCell(celCodigo);

                iTextSharp.text.pdf.PdfPCell celDescripcion = new iTextSharp.text.pdf.PdfPCell(new Phrase("DESCRIPCIÓN", fontTitleFactura));
                celDescripcion.Colspan = 1;
                celDescripcion.PaddingTop = 6;
                //celDescripcion.Border = 1;
                celDescripcion.BorderWidthRight = 0;
                celDescripcion.BackgroundColor = BaseColor.LIGHT_GRAY;
                celDescripcion.HorizontalAlignment = Element.ALIGN_CENTER;
                celDescripcion.VerticalAlignment = Element.ALIGN_TOP;

                tableTituloUnidades.AddCell(celDescripcion);

                iTextSharp.text.pdf.PdfPCell celCantidad = new iTextSharp.text.pdf.PdfPCell(new Phrase("CANT.", fontTitleFactura));
                celCantidad.Colspan = 1;
                celCantidad.PaddingTop = 6;
                //celVendedor.Border = 0;
                celCantidad.BorderWidthRight = 0;
                celCantidad.BackgroundColor = BaseColor.LIGHT_GRAY;
                celCantidad.HorizontalAlignment = Element.ALIGN_CENTER;
                celCantidad.VerticalAlignment = Element.ALIGN_TOP;
                tableTituloUnidades.AddCell(celCantidad);

                iTextSharp.text.pdf.PdfPCell celUM = new iTextSharp.text.pdf.PdfPCell(new Phrase("U.M.", fontTitleFactura));
                celUM.Colspan = 1;
                celUM.PaddingTop = 6;
                //celVendedor.Border = 0;
                celUM.BorderWidthRight = 0;
                celUM.BackgroundColor = BaseColor.LIGHT_GRAY;
                celUM.HorizontalAlignment = Element.ALIGN_CENTER;
                celUM.VerticalAlignment = Element.ALIGN_TOP;

                tableTituloUnidades.AddCell(celUM);

                iTextSharp.text.pdf.PdfPCell celValorUnit = new iTextSharp.text.pdf.PdfPCell(new Phrase("VR. UNIT.", fontTitleFactura));
                celValorUnit.Colspan = 1;
                celValorUnit.PaddingTop = 6;
                //celVendedor.Border = 0;
                celValorUnit.BorderWidthRight = 0;
                celValorUnit.BackgroundColor = BaseColor.LIGHT_GRAY;
                celValorUnit.HorizontalAlignment = Element.ALIGN_CENTER;
                celValorUnit.VerticalAlignment = Element.ALIGN_TOP;
                tableTituloUnidades.AddCell(celValorUnit);

                iTextSharp.text.pdf.PdfPTable tableDescuentos = new PdfPTable(2);

                iTextSharp.text.pdf.PdfPCell celDescuentos = new iTextSharp.text.pdf.PdfPCell(new Phrase("DESCUENTO", fontTitleFactura));
                celDescuentos.Colspan = 2;
                celDescuentos.HorizontalAlignment = Element.ALIGN_CENTER;
                celDescuentos.VerticalAlignment = Element.ALIGN_TOP;
                celDescuentos.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableDescuentos.AddCell(celDescuentos);

                iTextSharp.text.pdf.PdfPCell celDesc1 = new iTextSharp.text.pdf.PdfPCell(new Phrase("DCTO%", fontTitleFactura));
                celDesc1.Colspan = 1;
                celDesc1.HorizontalAlignment = Element.ALIGN_CENTER;
                celDesc1.VerticalAlignment = Element.ALIGN_TOP;
                celDesc1.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableDescuentos.AddCell(celDesc1);

                iTextSharp.text.pdf.PdfPCell celDesc2 = new iTextSharp.text.pdf.PdfPCell(new Phrase("DCTO. AB.", fontTitleFactura));
                celDesc2.Colspan = 1;
                celDesc2.HorizontalAlignment = Element.ALIGN_CENTER;
                celDesc2.VerticalAlignment = Element.ALIGN_TOP;
                celDesc2.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableDescuentos.AddCell(celDesc2);

                iTextSharp.text.pdf.PdfPCell _celDescuentos12 = new iTextSharp.text.pdf.PdfPCell(tableDescuentos);
                tableTituloUnidades.AddCell(_celDescuentos12);

                iTextSharp.text.pdf.PdfPCell celValorTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase("VALOR TOTAL", fontTitleFactura));
                celValorTotal.Colspan = 1;
                celValorTotal.PaddingTop = 6;
                celValorTotal.HorizontalAlignment = Element.ALIGN_CENTER;
                celValorTotal.VerticalAlignment = Element.ALIGN_CENTER;
                celValorTotal.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableTituloUnidades.AddCell(celValorTotal);

                iTextSharp.text.pdf.PdfPCell celIva = new iTextSharp.text.pdf.PdfPCell(new Phrase("IVA", fontTitleFactura));
                celIva.Colspan = 1;
                celIva.PaddingTop = 6;
                celIva.HorizontalAlignment = Element.ALIGN_CENTER;
                celIva.VerticalAlignment = Element.ALIGN_TOP;
                celIva.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableTituloUnidades.AddCell(celIva);

                PdfPTable tableUnidades = new PdfPTable(8);
                tableUnidades.WidthPercentage = 100;
                tableUnidades.SetWidths(DimencionUnidades);

                foreach (DataRow InvoiceLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
                {
                    if (!AddUnidadesALDOR(InvoiceLine, ref tableUnidades, fontCustom, DsInvoiceAR))
                        return false;
                }

                iTextSharp.text.pdf.PdfPCell LineaFinal = new iTextSharp.text.pdf.PdfPCell();
                LineaFinal.Colspan = 8;
                LineaFinal.Padding = 3;
                LineaFinal.Border = 1;
                LineaFinal.BorderColorBottom = BaseColor.WHITE;
                LineaFinal.HorizontalAlignment = Element.ALIGN_CENTER;
                LineaFinal.VerticalAlignment = Element.ALIGN_TOP;
                tableUnidades.AddCell(LineaFinal);

                //-----------------------------------------------------------------------------------------
                PdfPTable tableTotalUnidades = new PdfPTable(8);
                tableTotalUnidades.PaddingTop = 20;
                //Dimenciones.
                float[] DimencionTotalUnidades = new float[8];
                DimencionTotalUnidades[0] = 1.0F;//
                DimencionTotalUnidades[1] = 1.0F;//
                DimencionTotalUnidades[2] = 1.0F;//
                DimencionTotalUnidades[3] = 1.0F;//
                DimencionTotalUnidades[4] = 1.0F;//
                DimencionTotalUnidades[5] = 1.0F;//
                DimencionTotalUnidades[6] = 1.0F;//
                DimencionTotalUnidades[7] = 1.0F;//

                tableTotalUnidades.WidthPercentage = 100;
                tableTotalUnidades.SetWidths(DimencionTotalUnidades);

                iTextSharp.text.pdf.PdfPCell celTotalUnidades = new iTextSharp.text.pdf.PdfPCell(new Phrase("TOTAL UNIDADES", fontTitleFactura));
                celTotalUnidades.Colspan = 1;
                celTotalUnidades.Padding = 3;
                //celVendedor.Border = 0;
                celTotalUnidades.BorderColorBottom = BaseColor.WHITE;
                celTotalUnidades.HorizontalAlignment = Element.ALIGN_CENTER;
                celTotalUnidades.VerticalAlignment = Element.ALIGN_TOP;
                celTotalUnidades.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableTotalUnidades.AddCell(celTotalUnidades);

                iTextSharp.text.pdf.PdfPCell celPesoNeto = new iTextSharp.text.pdf.PdfPCell(new Phrase("PESO NETO", fontTitleFactura));
                celPesoNeto.Colspan = 1;
                celPesoNeto.Padding = 3;
                //celVendedor.Border = 0;
                celPesoNeto.BorderColorBottom = BaseColor.WHITE;
                celPesoNeto.HorizontalAlignment = Element.ALIGN_CENTER;
                celPesoNeto.VerticalAlignment = Element.ALIGN_TOP;
                celPesoNeto.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableTotalUnidades.AddCell(celPesoNeto);

                iTextSharp.text.pdf.PdfPCell celPesoBruto = new iTextSharp.text.pdf.PdfPCell(new Phrase("PESO BRUTO", fontTitleFactura));
                celPesoBruto.Colspan = 1;
                celPesoBruto.Padding = 3;
                //celVendedor.Border = 0;
                celPesoBruto.BorderColorBottom = BaseColor.WHITE;
                celPesoBruto.HorizontalAlignment = Element.ALIGN_CENTER;
                celPesoBruto.VerticalAlignment = Element.ALIGN_TOP;
                celPesoBruto.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableTotalUnidades.AddCell(celPesoBruto);

                iTextSharp.text.pdf.PdfPCell celSubTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase("SUBTOTAL", fontTitleFactura));
                celSubTotal.Colspan = 1;
                celSubTotal.Padding = 3;
                //celVendedor.Border = 0;
                celSubTotal.BackgroundColor = BaseColor.LIGHT_GRAY;
                celSubTotal.BorderColorBottom = BaseColor.WHITE;
                celSubTotal.HorizontalAlignment = Element.ALIGN_CENTER;
                celSubTotal.VerticalAlignment = Element.ALIGN_TOP;
                tableTotalUnidades.AddCell(celSubTotal);

                iTextSharp.text.pdf.PdfPCell celValorretIva = new iTextSharp.text.pdf.PdfPCell(new Phrase("RETE IVA", fontTitleFactura));
                celValorretIva.Colspan = 1;
                celValorretIva.Padding = 3;
                celValorretIva.BackgroundColor = BaseColor.LIGHT_GRAY;
                //celVendedor.Border = 0;
                celValorretIva.BorderColorBottom = BaseColor.WHITE;
                celValorretIva.HorizontalAlignment = Element.ALIGN_CENTER;
                celValorretIva.VerticalAlignment = Element.ALIGN_TOP;
                tableTotalUnidades.AddCell(celValorretIva);


                iTextSharp.text.pdf.PdfPCell celValorIva = new iTextSharp.text.pdf.PdfPCell(new Phrase("VALOR IVA", fontTitleFactura));
                celValorIva.Colspan = 1;
                celValorIva.Padding = 3;
                celValorIva.BackgroundColor = BaseColor.LIGHT_GRAY;
                //celVendedor.Border = 0;
                celValorIva.BorderColorBottom = BaseColor.WHITE;
                celValorIva.HorizontalAlignment = Element.ALIGN_CENTER;
                celValorIva.VerticalAlignment = Element.ALIGN_TOP;
                tableTotalUnidades.AddCell(celValorIva);

                iTextSharp.text.pdf.PdfPCell celTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase("TOTAL", fontTitleFactura));
                celTotal.Colspan = 1;
                celTotal.Padding = 3;
                celTotal.BackgroundColor = BaseColor.LIGHT_GRAY;
                //celVendedor.Border = 0;
                celTotal.BorderColorBottom = BaseColor.WHITE;
                celTotal.HorizontalAlignment = Element.ALIGN_CENTER;
                celTotal.VerticalAlignment = Element.ALIGN_TOP;
                tableTotalUnidades.AddCell(celTotal);


                iTextSharp.text.pdf.PdfPCell celretecion = new iTextSharp.text.pdf.PdfPCell(new Phrase("RETEFUENTE", fontTitleFactura));
                celretecion.Colspan = 1;
                celretecion.Padding = 3;
                celretecion.BackgroundColor = BaseColor.LIGHT_GRAY;
                //celVendedor.Border = 0;
                celretecion.BorderColorBottom = BaseColor.WHITE;
                celretecion.HorizontalAlignment = Element.ALIGN_CENTER;
                celretecion.VerticalAlignment = Element.ALIGN_TOP;
                tableTotalUnidades.AddCell(celretecion);

                iTextSharp.text.pdf.PdfPCell _celTotalUnidades = new iTextSharp.text.pdf.PdfPCell(new Phrase("xxx", fontTitleFactura));
                _celTotalUnidades.Colspan = 1;
                _celTotalUnidades.Padding = 3;
                //celVendedor.Border = 0;
                _celTotalUnidades.HorizontalAlignment = Element.ALIGN_RIGHT;
                _celTotalUnidades.VerticalAlignment = Element.ALIGN_TOP;
                tableTotalUnidades.AddCell(_celTotalUnidades);

                iTextSharp.text.pdf.PdfPCell _celPesoNeto = new iTextSharp.text.pdf.PdfPCell(new Phrase("xxx", fontTitleFactura));
                _celPesoNeto.Colspan = 1;
                _celPesoNeto.Padding = 3;
                //celVendedor.Border = 0;
                _celPesoNeto.HorizontalAlignment = Element.ALIGN_RIGHT;
                _celPesoNeto.VerticalAlignment = Element.ALIGN_TOP;
                tableTotalUnidades.AddCell(_celPesoNeto);

                iTextSharp.text.pdf.PdfPCell _celPesoBruto = new iTextSharp.text.pdf.PdfPCell(new Phrase("xxx", fontTitleFactura));
                _celPesoBruto.Colspan = 1;
                _celPesoBruto.Padding = 3;
                //celVendedor.Border = 0;
                _celPesoBruto.HorizontalAlignment = Element.ALIGN_RIGHT;
                _celPesoBruto.VerticalAlignment = Element.ALIGN_TOP;
                tableTotalUnidades.AddCell(_celPesoBruto);

                iTextSharp.text.pdf.PdfPCell _celTotalUnidadesSubTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase("xxx", fontTitleFactura));
                _celTotalUnidadesSubTotal.Colspan = 1;
                _celTotalUnidadesSubTotal.Padding = 3;
                //celVendedor.Border = 0;
                _celTotalUnidadesSubTotal.HorizontalAlignment = Element.ALIGN_RIGHT;
                _celTotalUnidadesSubTotal.VerticalAlignment = Element.ALIGN_TOP;
                tableTotalUnidades.AddCell(_celTotalUnidadesSubTotal);


                iTextSharp.text.pdf.PdfPCell _celValorreteIva = new iTextSharp.text.pdf.PdfPCell(new Phrase("xxx", fontTitleFactura));
                _celValorreteIva.Colspan = 1;
                _celValorreteIva.Padding = 3;
                //celVendedor.Border = 0;
                _celValorreteIva.HorizontalAlignment = Element.ALIGN_RIGHT;
                _celValorreteIva.VerticalAlignment = Element.ALIGN_TOP;
                tableTotalUnidades.AddCell(_celValorreteIva);

                iTextSharp.text.pdf.PdfPCell _celValorIva = new iTextSharp.text.pdf.PdfPCell(new Phrase("xxx", fontTitleFactura));
                _celValorIva.Colspan = 1;
                _celValorIva.Padding = 3;
                //celVendedor.Border = 0;
                _celValorIva.HorizontalAlignment = Element.ALIGN_RIGHT;
                _celValorIva.VerticalAlignment = Element.ALIGN_TOP;
                tableTotalUnidades.AddCell(_celValorIva);

                iTextSharp.text.pdf.PdfPCell _celTotalUnidadesTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase("xxx", fontTitleFactura));
                _celTotalUnidadesTotal.Colspan = 1;
                _celTotalUnidadesTotal.Padding = 3;
                //celVendedor.Border = 0;
                _celTotalUnidadesTotal.HorizontalAlignment = Element.ALIGN_RIGHT;
                _celTotalUnidadesTotal.VerticalAlignment = Element.ALIGN_TOP;
                tableTotalUnidades.AddCell(_celTotalUnidadesTotal);

                //------------------------------------------------------------------------------------------------
                PdfPTable tableValorLetras = new PdfPTable(2);

                float[] DimencionValorLetras = new float[2];
                DimencionValorLetras[0] = 1.0F;//
                DimencionValorLetras[1] = 5.0F;//

                tableValorLetras.WidthPercentage = 100;
                tableValorLetras.SetWidths(DimencionValorLetras);

                iTextSharp.text.pdf.PdfPCell celTextValorLetras = new iTextSharp.text.pdf.PdfPCell(new Phrase("VALORES EN LETRA: ", fontTitleFactura));
                celTextValorLetras.Colspan = 1;
                celTextValorLetras.Padding = 3;
                celTextValorLetras.BackgroundColor = BaseColor.LIGHT_GRAY;
                celTextValorLetras.BorderWidthRight = 0;
                celTextValorLetras.BorderWidthBottom = 0;
                celTextValorLetras.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextValorLetras.VerticalAlignment = Element.ALIGN_TOP;
                tableValorLetras.AddCell(celTextValorLetras);

                iTextSharp.text.pdf.PdfPCell celValorLetras = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0}",
                Nroenletras(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"].ToString())), fontTitle2));
                celValorLetras.Colspan = 1;
                celValorLetras.Padding = 3;
                celValorLetras.BorderWidthLeft = 0;
                celValorLetras.BorderWidthBottom = 0;
                celValorLetras.HorizontalAlignment = Element.ALIGN_LEFT;
                celValorLetras.VerticalAlignment = Element.ALIGN_TOP;
                tableValorLetras.AddCell(celValorLetras);

                iTextSharp.text.pdf.PdfPCell celEspacioValorLetras = new iTextSharp.text.pdf.PdfPCell(new Phrase(" "));
                celEspacioValorLetras.Colspan = 2;
                celEspacioValorLetras.Padding = 3;
                celEspacioValorLetras.BorderWidthTop = 0;
                celEspacioValorLetras.HorizontalAlignment = Element.ALIGN_LEFT;
                celEspacioValorLetras.VerticalAlignment = Element.ALIGN_TOP;
                tableValorLetras.AddCell(celEspacioValorLetras);
                //------------------------------------------------------------------------------------------------
                PdfPTable tableObservaciones = new PdfPTable(2);

                float[] DimencionObservaciones = new float[2];
                DimencionObservaciones[0] = 1.0F;//
                DimencionObservaciones[1] = 5.0F;//

                tableObservaciones.WidthPercentage = 100;
                tableObservaciones.SetWidths(DimencionObservaciones);

                iTextSharp.text.pdf.PdfPCell celTextObservaciones = new iTextSharp.text.pdf.PdfPCell(new Phrase("OBSERVACIONES: ", fontTitleFactura));
                celTextObservaciones.Colspan = 1;
                celTextObservaciones.Padding = 3;
                celTextObservaciones.BackgroundColor = BaseColor.LIGHT_GRAY;
                celTextObservaciones.BorderWidthRight = 0;
                celTextObservaciones.BorderWidthBottom = 0;
                celTextObservaciones.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextObservaciones.VerticalAlignment = Element.ALIGN_TOP;
                tableObservaciones.AddCell(celTextObservaciones);

                iTextSharp.text.pdf.PdfPCell celObservaciones = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("\n{0}",
                    DsInvoiceAR.Tables["InvcHead"].Rows[0]["ObservacionCliente_c"].ToString()), fontTitle2));
                celObservaciones.Colspan = 1;
                celObservaciones.Padding = 3;
                celObservaciones.BorderWidthLeft = 0;
                celObservaciones.BorderWidthBottom = 0;
                celObservaciones.HorizontalAlignment = Element.ALIGN_LEFT;
                celObservaciones.VerticalAlignment = Element.ALIGN_TOP;
                tableObservaciones.AddCell(celObservaciones);

                iTextSharp.text.pdf.PdfPCell celEspacioObservaciones = new iTextSharp.text.pdf.PdfPCell(new Phrase("\n\n\n\n"));
                celEspacioObservaciones.Colspan = 2;
                celEspacioObservaciones.Padding = 3;
                celEspacioObservaciones.BorderWidthTop = 0;
                celEspacioObservaciones.HorizontalAlignment = Element.ALIGN_LEFT;
                celEspacioObservaciones.VerticalAlignment = Element.ALIGN_TOP;
                tableObservaciones.AddCell(celEspacioObservaciones);
                //----------------------------------------------------------------------------------------------
                PdfPTable tableResolucion = new PdfPTable(1);
                tableResolucion.WidthPercentage = 100;
                string strResolucion = "xxx";
                iTextSharp.text.pdf.PdfPCell _celResolucion = new iTextSharp.text.pdf.PdfPCell(new Phrase(strResolucion, fontTitleFactura));
                _celResolucion.Colspan = 1;
                _celResolucion.Padding = 3;
                //_celResolucion.Border = 0;
                _celResolucion.HorizontalAlignment = Element.ALIGN_JUSTIFIED_ALL;
                _celResolucion.VerticalAlignment = Element.ALIGN_TOP;
                tableResolucion.AddCell(_celResolucion);
                //------------------------------------------------------------------------
                PdfPTable tableTrasnportador = new PdfPTable(4);

                float[] DimencionTransportador = new float[4];
                DimencionTransportador[0] = 1.0F;//
                DimencionTransportador[1] = 1.0F;//
                DimencionTransportador[2] = 1.3F;//
                DimencionTransportador[3] = 1.0F;//


                tableTrasnportador.WidthPercentage = 100;
                tableTrasnportador.SetWidths(DimencionTransportador);

                iTextSharp.text.pdf.PdfPCell celTransportador = new iTextSharp.text.pdf.PdfPCell(new Phrase("TRANSPORTADOR", fontTitleFactura));
                celTransportador.Colspan = 1;
                celTransportador.Padding = 3;
                celTransportador.BorderWidthBottom = 0;
                celTransportador.BackgroundColor = BaseColor.LIGHT_GRAY;
                celTransportador.HorizontalAlignment = Element.ALIGN_CENTER;
                celTransportador.VerticalAlignment = Element.ALIGN_TOP;
                tableTrasnportador.AddCell(celTransportador);

                iTextSharp.text.pdf.PdfPCell celConductor = new iTextSharp.text.pdf.PdfPCell(new Phrase("NOMBRE  DEL CONDUCTOR", fontTitleFactura));
                celConductor.Colspan = 1;
                celConductor.Padding = 3;
                celConductor.BorderWidthBottom = 0;
                celConductor.BackgroundColor = BaseColor.LIGHT_GRAY;
                celConductor.HorizontalAlignment = Element.ALIGN_CENTER;
                celConductor.VerticalAlignment = Element.ALIGN_TOP;
                tableTrasnportador.AddCell(celConductor);

                iTextSharp.text.pdf.PdfPCell celCC = new iTextSharp.text.pdf.PdfPCell(new Phrase("CEDULA DE CIUDADANIA", fontTitleFactura));
                celCC.Colspan = 1;
                celCC.Padding = 3;
                celCC.BorderWidthBottom = 0;
                celCC.BackgroundColor = BaseColor.LIGHT_GRAY;
                celCC.HorizontalAlignment = Element.ALIGN_CENTER;
                celCC.VerticalAlignment = Element.ALIGN_TOP;
                tableTrasnportador.AddCell(celCC);

                iTextSharp.text.pdf.PdfPCell celPlacaVehiculo = new iTextSharp.text.pdf.PdfPCell(new Phrase("PLACA VEHICULO", fontTitleFactura));
                celPlacaVehiculo.Colspan = 1;
                celPlacaVehiculo.Padding = 3;
                celPlacaVehiculo.BorderWidthBottom = 0;
                celPlacaVehiculo.BackgroundColor = BaseColor.LIGHT_GRAY;
                celPlacaVehiculo.HorizontalAlignment = Element.ALIGN_CENTER;
                celPlacaVehiculo.VerticalAlignment = Element.ALIGN_TOP;
                tableTrasnportador.AddCell(celPlacaVehiculo);

                //---------------------CAMPOS DE TRANSPORTADOR----------------------------
                iTextSharp.text.pdf.PdfPCell _celTransportador = new iTextSharp.text.pdf.PdfPCell(new Phrase(" ", fontTitleFactura));
                _celTransportador.Colspan = 1;
                _celTransportador.Padding = 3;
                _celTransportador.BorderWidthTop = 0;
                _celTransportador.HorizontalAlignment = Element.ALIGN_CENTER;
                _celTransportador.VerticalAlignment = Element.ALIGN_TOP;
                tableTrasnportador.AddCell(_celTransportador);

                iTextSharp.text.pdf.PdfPCell _celConductor = new iTextSharp.text.pdf.PdfPCell(new Phrase(" ", fontTitleFactura));
                _celConductor.Colspan = 1;
                _celConductor.Padding = 3;
                _celConductor.BorderWidthTop = 0;
                _celConductor.HorizontalAlignment = Element.ALIGN_CENTER;
                _celConductor.VerticalAlignment = Element.ALIGN_TOP;
                tableTrasnportador.AddCell(_celConductor);

                iTextSharp.text.pdf.PdfPCell _celCC = new iTextSharp.text.pdf.PdfPCell(new Phrase(" ", fontTitleFactura));
                _celCC.Colspan = 1;
                _celCC.Padding = 3;
                _celCC.BorderWidthTop = 0;
                _celCC.HorizontalAlignment = Element.ALIGN_CENTER;
                _celCC.VerticalAlignment = Element.ALIGN_TOP;
                tableTrasnportador.AddCell(_celCC);

                iTextSharp.text.pdf.PdfPCell _celPlacaVehiculo = new iTextSharp.text.pdf.PdfPCell(new Phrase(" ", fontTitleFactura));
                _celPlacaVehiculo.Colspan = 1;
                _celPlacaVehiculo.Padding = 3;
                _celPlacaVehiculo.BorderWidthTop = 0;
                _celPlacaVehiculo.HorizontalAlignment = Element.ALIGN_CENTER;
                _celPlacaVehiculo.VerticalAlignment = Element.ALIGN_TOP;
                tableTrasnportador.AddCell(_celPlacaVehiculo);

                //------------------------------------------------------------------------
                PdfPTable tableFirmas = new PdfPTable(5);

                float[] DimencionFirmas = new float[5];
                DimencionFirmas[0] = 1.0F;//
                DimencionFirmas[1] = 0.02F;//
                DimencionFirmas[2] = 1.0F;//
                DimencionFirmas[3] = 0.02F;//
                DimencionFirmas[4] = 1.4F;//

                tableFirmas.WidthPercentage = 100;
                tableFirmas.SetWidths(DimencionFirmas);

                PdfPTable tableDespacahdoPor = new PdfPTable(1);
                //tableDespacahdoPor.WidthPercentage = 100;

                iTextSharp.text.pdf.PdfPCell celDespachadoPor = new iTextSharp.text.pdf.PdfPCell(new Phrase("\n\n\n\n\n" +
                    "\n______________________________________\n\nDESPACHADO POR", fontTitleFactura));
                celDespachadoPor.Colspan = 1;
                celDespachadoPor.Padding = 3;
                celDespachadoPor.HorizontalAlignment = Element.ALIGN_CENTER;
                celDespachadoPor.VerticalAlignment = Element.ALIGN_TOP;
                tableDespacahdoPor.AddCell(celDespachadoPor);

                iTextSharp.text.pdf.PdfPCell celEspacioDespachadoPor = new iTextSharp.text.pdf.PdfPCell(new Phrase("\n\n\n", fontTitleFactura));
                celEspacioDespachadoPor.Colspan = 1;
                celEspacioDespachadoPor.Padding = 3;
                celEspacioDespachadoPor.Border = 0;
                celEspacioDespachadoPor.HorizontalAlignment = Element.ALIGN_CENTER;
                celEspacioDespachadoPor.VerticalAlignment = Element.ALIGN_TOP;
                tableDespacahdoPor.AddCell(celEspacioDespachadoPor);

                iTextSharp.text.pdf.PdfPCell _celDespachadoPor = new iTextSharp.text.pdf.PdfPCell(tableDespacahdoPor);
                _celDespachadoPor.Border = 0;
                tableFirmas.AddCell(_celDespachadoPor);
                //------------------------------------------------------------------------------------------
                tableFirmas.AddCell(celEspacio2);
                //------------------------------------------------------------------------------------------
                PdfPTable tableFirmaConductor = new PdfPTable(1);
                tableFirmaConductor.WidthPercentage = 100;
                iTextSharp.text.pdf.PdfPCell celFirmaConductor = new iTextSharp.text.pdf.PdfPCell(new Phrase("\n\n\n\n\n\n" +
                    "______________________________________" +
                    "\n\nFIRMA DEL CONDUCTOR", fontTitleFactura));
                celFirmaConductor.Colspan = 1;
                celFirmaConductor.Padding = 3;
                //celFirmaConductor.Border = 0;
                celFirmaConductor.HorizontalAlignment = Element.ALIGN_CENTER;
                celFirmaConductor.VerticalAlignment = Element.ALIGN_TOP;
                tableFirmaConductor.AddCell(celFirmaConductor);

                iTextSharp.text.pdf.PdfPCell celEspacioFirmaConductor = new iTextSharp.text.pdf.PdfPCell(new Phrase("\n\n\n", fontTitleFactura));
                celEspacioFirmaConductor.Colspan = 1;
                celEspacioFirmaConductor.Padding = 3;
                celEspacioFirmaConductor.Border = 0;
                celEspacioFirmaConductor.HorizontalAlignment = Element.ALIGN_CENTER;
                celEspacioFirmaConductor.VerticalAlignment = Element.ALIGN_TOP;
                tableFirmaConductor.AddCell(celEspacioFirmaConductor);

                iTextSharp.text.pdf.PdfPCell _celFirmaConductor = new iTextSharp.text.pdf.PdfPCell(tableFirmaConductor);
                _celFirmaConductor.Border = 0;
                tableFirmas.AddCell(_celFirmaConductor);
                //-------------------------------------------------------------------------------------
                tableFirmas.AddCell(celEspacio2);
                //-------------------------------------------------------------------------------------

                PdfPTable tableSelloCliente = new PdfPTable(1);
                //tableSelloCliente.WidthPercentage = 100;

                iTextSharp.text.pdf.PdfPCell cel1SelloCliente = new iTextSharp.text.pdf.PdfPCell(new Phrase("Apreciado cliente este documento " +
                    "no es endolsable", fontTitleFactura));
                cel1SelloCliente.Colspan = 1;
                cel1SelloCliente.Padding = 5;
                cel1SelloCliente.Border = 1;
                cel1SelloCliente.BorderWidthBottom = 1;
                cel1SelloCliente.HorizontalAlignment = Element.ALIGN_LEFT;
                cel1SelloCliente.VerticalAlignment = Element.ALIGN_TOP;
                tableSelloCliente.AddCell(cel1SelloCliente);

                iTextSharp.text.pdf.PdfPCell cel2SelloFechaRecibido = new iTextSharp.text.pdf.PdfPCell(new Phrase("Fecha de recibido:", fontTitleFactura));

                cel2SelloFechaRecibido.Colspan = 1;
                cel2SelloFechaRecibido.Padding = 3;
                cel2SelloFechaRecibido.Border = 0;
                cel2SelloFechaRecibido.HorizontalAlignment = Element.ALIGN_LEFT;
                cel2SelloFechaRecibido.VerticalAlignment = Element.ALIGN_TOP;
                tableSelloCliente.AddCell(cel2SelloFechaRecibido);

                iTextSharp.text.pdf.PdfPCell cel2SelloNombre = new iTextSharp.text.pdf.PdfPCell(new Phrase("Nombre:", fontTitleFactura));

                cel2SelloNombre.Colspan = 1;
                cel2SelloNombre.Padding = 3;
                cel2SelloNombre.Border = 0;
                cel2SelloNombre.HorizontalAlignment = Element.ALIGN_LEFT;
                cel2SelloNombre.VerticalAlignment = Element.ALIGN_TOP;
                tableSelloCliente.AddCell(cel2SelloNombre);

                iTextSharp.text.pdf.PdfPCell cel2SelloId = new iTextSharp.text.pdf.PdfPCell(new Phrase("Identificacion:", fontTitleFactura));

                cel2SelloId.Colspan = 1;
                cel2SelloId.Padding = 3;
                cel2SelloId.Border = 0;
                cel2SelloId.HorizontalAlignment = Element.ALIGN_LEFT;
                cel2SelloId.VerticalAlignment = Element.ALIGN_TOP;
                tableSelloCliente.AddCell(cel2SelloId);

                iTextSharp.text.pdf.PdfPCell cel2SelloFirma = new iTextSharp.text.pdf.PdfPCell(new Phrase("Firma:\n", fontTitleFactura));

                cel2SelloFirma.Colspan = 1;
                cel2SelloFirma.Padding = 3;
                cel2SelloFirma.Border = 0;
                cel2SelloFirma.HorizontalAlignment = Element.ALIGN_LEFT;
                cel2SelloFirma.VerticalAlignment = Element.ALIGN_TOP;
                tableSelloCliente.AddCell(cel2SelloFirma);

                iTextSharp.text.pdf.PdfPCell cel3SelloCliente = new iTextSharp.text.pdf.PdfPCell(new Phrase("______________________________________________________\n\n" +
                    "RECIBIDO CONFORME FIRMA Y SELLO DEL CLIENTE", fontTitleFactura));

                cel3SelloCliente.Colspan = 1;
                cel3SelloCliente.Padding = 3;
                cel3SelloCliente.Border = 0;
                cel3SelloCliente.HorizontalAlignment = Element.ALIGN_CENTER;
                cel3SelloCliente.VerticalAlignment = Element.ALIGN_TOP;
                tableSelloCliente.AddCell(cel3SelloCliente);

                iTextSharp.text.pdf.PdfPCell _celSelloCliente = new iTextSharp.text.pdf.PdfPCell(tableSelloCliente);
                tableFirmas.AddCell(_celSelloCliente);
                #endregion

                #region Exit
                document.Add(Header);
                //document.Add(divLogo);
                //document.Add(divAcercade);
                //document.Add(divLogo2);
                //document.Add(divQR);
                //document.Add(divNumeroFactura);
                document.Add(divEspacio);
                document.Add(tableFacturar);
                document.Add(divEspacio);
                document.Add(tableDetalles2);
                document.Add(divEspacio2);
                document.Add(divEspacio2);
                document.Add(tableDetalles);
                document.Add(divEspacio2);
                document.Add(tableTituloUnidades);
                document.Add(tableUnidades);
                document.Add(divEspacio2);
                document.Add(divEspacio2);
                document.Add(tableTotalUnidades);
                document.Add(divEspacio2);
                document.Add(divEspacio2);
                document.Add(tableValorLetras);
                document.Add(divEspacio2);
                document.Add(divEspacio2);
                document.Add(tableObservaciones);
                document.Add(divEspacio2);
                document.Add(divEspacio2);
                document.Add(tableResolucion);
                document.Add(divEspacio2);
                document.Add(tableTrasnportador);
                document.Add(divEspacio2);
                document.Add(divEspacio2);
                document.Add(tableFirmas);

                /*PIE DE PAGINA*/
                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();
                return true;
                #endregion
            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.Message;
                return false;
            }  
        }
      
        private void AddUnidadesinnovak(DataRow dataLine, ref PdfPTable pdfPTable, iTextSharp.text.Font font)
        {
            iTextSharp.text.pdf.PdfPCell celCodigo = new iTextSharp.text.pdf.PdfPCell(new Phrase("xxxx", font));
            celCodigo.Colspan = 1;
            celCodigo.Padding = 3;
            //celCodigo.Border = 0;
            celCodigo.BorderWidthBottom = 0;
            celCodigo.BorderWidthTop = 0;
            celCodigo.BorderWidthRight = 0;
            celCodigo.BorderColorBottom = BaseColor.WHITE;
            celCodigo.HorizontalAlignment = Element.ALIGN_CENTER;
            celCodigo.VerticalAlignment = Element.ALIGN_TOP;
            pdfPTable.AddCell(celCodigo);

            iTextSharp.text.pdf.PdfPCell celDescripcion = new iTextSharp.text.pdf.PdfPCell(new Phrase(dataLine["LineDesc"].ToString(), font));
            celDescripcion.Colspan = 1;
            celDescripcion.Padding = 3;
            //celDescripcion.Border = 0;
            celDescripcion.BorderWidthBottom = 0;
            celDescripcion.BorderWidthTop = 0;
            celDescripcion.BorderWidthRight = 0;
            celDescripcion.BorderColorBottom = BaseColor.WHITE;
            celDescripcion.HorizontalAlignment = Element.ALIGN_CENTER;
            celDescripcion.VerticalAlignment = Element.ALIGN_TOP;
            pdfPTable.AddCell(celDescripcion);

            iTextSharp.text.pdf.PdfPCell celCantidad = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:N0}", dataLine["SellingShipQty"].ToString()), font));
            celCantidad.Colspan = 1;
            celCantidad.Padding = 3;
            //celCantidad.Border = 0;
            celCantidad.BorderWidthBottom = 0;
            celCantidad.BorderWidthTop = 0;
            celCantidad.BorderWidthRight = 0;
            celCantidad.BorderColorBottom = BaseColor.WHITE;
            celCantidad.HorizontalAlignment = Element.ALIGN_CENTER;
            celCantidad.VerticalAlignment = Element.ALIGN_TOP;
            pdfPTable.AddCell(celCantidad);

            iTextSharp.text.pdf.PdfPCell celUnidad = new iTextSharp.text.pdf.PdfPCell(new Phrase("xxxxx", font));
            celUnidad.Colspan = 1;
            celUnidad.Padding = 3;
            //celUnidad.Border = 0;
            celUnidad.BorderWidthBottom = 0;
            celUnidad.BorderWidthTop = 0;
            celUnidad.BorderWidthRight = 0;
            celUnidad.BorderColorBottom = BaseColor.WHITE;
            celUnidad.HorizontalAlignment = Element.ALIGN_CENTER;
            celUnidad.VerticalAlignment = Element.ALIGN_TOP;
            pdfPTable.AddCell(celUnidad);

            iTextSharp.text.pdf.PdfPCell celValorUnitario = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:C2}",
                Convert.ToDecimal(dataLine["UnitPrice"].ToString())), font));
            celValorUnitario.Colspan = 1;
            celValorUnitario.Padding = 3;
            //celValorUnitario.Border = 0;
            celValorUnitario.BorderWidthBottom = 0;
            celValorUnitario.BorderWidthTop = 0;
            celValorUnitario.BorderWidthRight = 0;
            celValorUnitario.BorderColorBottom = BaseColor.WHITE;
            celValorUnitario.HorizontalAlignment = Element.ALIGN_CENTER;
            celValorUnitario.VerticalAlignment = Element.ALIGN_TOP;
            pdfPTable.AddCell(celValorUnitario);

            //decimal cantidad = 0, unitPrice = 0, Total = 0;
            //cantidad = decimal.Parse(dataLine["SellingShipQty"].ToString());
            //unitPrice = decimal.Parse(dataLine["DocExtPrice"].ToString());
            //Total = cantidad * unitPrice;
            iTextSharp.text.pdf.PdfPCell celValorTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:C2}",
                decimal.Parse(dataLine["DocExtPrice"].ToString())), font));
            celValorTotal.Colspan = 1;
            celValorTotal.Padding = 3;
            //celValorTotal.Border = 0;
            celValorTotal.BorderWidthBottom = 0;
            celValorTotal.BorderWidthTop = 0;
            celValorTotal.BorderColorBottom = BaseColor.WHITE;
            celValorTotal.HorizontalAlignment = Element.ALIGN_CENTER;
            celValorTotal.VerticalAlignment = Element.ALIGN_TOP;
            pdfPTable.AddCell(celValorTotal);
        }

        public bool FacturaNacionalinnovak(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {        
            NomArchivo = string.Empty;
            string Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            string RutaImg = $@"{AppDomain.CurrentDomain.BaseDirectory}\LOGO_innovak.png";
            string RutaMarcaAgua = $@"{AppDomain.CurrentDomain.BaseDirectory}\marca_agua.png";
            NomArchivo = NombreInvoice + ".pdf";
            try
            {
                #region Head
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.
                Document document = new Document(iTextSharp.text.PageSize.LETTER);
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;

                // step 3: we open the document     
                document.Open();

                PdfDiv divEspacio = new PdfDiv();
                divEspacio.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio.Height = 10;
                divEspacio.Width = 130;

                PdfDiv divEspacio2 = new PdfDiv();
                divEspacio2.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio2.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio2.Height = 2;
                divEspacio2.Width = 130;

                //FUENTES----------------------------------------------------------------------------------------------------------
                iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle3 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 9, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle2 = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustom = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitleFactura = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                //FUENTES---------------------------------------------------------------------------------------------------------

                System.Drawing.Image logo = null;
                var requestLogo = WebRequest.Create(RutaImg);

                using (var responseLogo = requestLogo.GetResponse())
                using (var streamLogo = responseLogo.GetResponseStream())
                {
                    logo = Bitmap.FromStream(streamLogo);
                }

                System.Drawing.Image logo2 = null;
                var requestLogo2 = WebRequest.Create(RutaImg);

                using (var responseLogo2 = requestLogo2.GetResponse())
                using (var streamLogo2 = responseLogo2.GetResponseStream())
                {
                    logo2 = Bitmap.FromStream(streamLogo2);
                }
                #endregion

                #region ENCABEZADO
                PdfPTable Header = new PdfPTable(4);
                Header.WidthPercentage = 100;
                //informacion de la empresa
                PdfPCell cell_info = new PdfPCell() { Border = 0, };
                Paragraph prgTitle = new Paragraph(string.Format("{0}\n{1}", DsInvoiceAR.Tables["Company"].Rows[0]["Name"],
                $"NIT. " + DsInvoiceAR.Tables["Company"].Rows[0]["Name"]), fontTitle);
                Paragraph prgTitle2 = new Paragraph(string.Format("{0}\n{1}\n{2} - {3}",
                    DsInvoiceAR.Tables["Company"].Rows[0]["Address1"].ToString(),
                    DsInvoiceAR.Tables["Company"].Rows[0]["PhoneNum"].ToString(),
                    DsInvoiceAR.Tables["Company"].Rows[0]["City"].ToString(),
                    DsInvoiceAR.Tables["Company"].Rows[0]["State"].ToString()),
                    fontTitle);
                prgTitle.Alignment = Element.ALIGN_LEFT;
                prgTitle2.Alignment = Element.ALIGN_LEFT;
                cell_info.AddElement(prgTitle);
                cell_info.AddElement(prgTitle2);
                Header.AddCell(cell_info);
                //logo
                PdfPCell cell_logo = new PdfPCell() { Border = 0, };
                iTextSharp.text.Image ImgLogo2 = iTextSharp.text.Image.GetInstance(logo2, BaseColor.WHITE);
                ImgLogo2.ScaleAbsolute(100, 100);
                cell_logo.AddElement(ImgLogo2);
                Header.AddCell(cell_logo);
                //Qr
                PdfPCell cell_Qr = new PdfPCell() { Border = 0, };
                iTextSharp.text.Image ImgQR = iTextSharp.text.Image.GetInstance(QRInvoice, BaseColor.WHITE);
                ImgQR.ScaleAbsolute(70, 70);
                cell_Qr.AddElement(ImgQR);
                Header.AddCell(cell_Qr);
                //tipo de de documento
                PdfPCell cell_factura = new PdfPCell() { Border = 0, };
                PdfPTable tableFactura = new PdfPTable(3);
                //Dimenciones.
                float[] DimencionFactura = new float[3];
                DimencionFactura[0] = 1.0F;//
                DimencionFactura[1] = 4.0F;//
                DimencionFactura[2] = 0.5F;//

                tableFactura.WidthPercentage = 100;
                tableFactura.SetWidths(DimencionFactura);

                iTextSharp.text.pdf.PdfPCell celTittle = new iTextSharp.text.pdf.PdfPCell(new Phrase("FACTURA DE VENTAS\n\n", fontTitleFactura));
                celTittle.Colspan = 3;
                celTittle.Padding = 3;
                celTittle.HorizontalAlignment = Element.ALIGN_CENTER;
                celTittle.VerticalAlignment = Element.ALIGN_TOP;
                celTittle.Border = 0;
                celTittle.BorderWidthTop = 1;
                celTittle.BorderWidthLeft = 1;
                celTittle.BorderWidthRight = 1;
                celTittle.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableFactura.AddCell(celTittle);

                iTextSharp.text.pdf.PdfPCell celNo = new iTextSharp.text.pdf.PdfPCell(new Phrase("No. ", fontTitleFactura));
                celNo.Colspan = 1;
                celNo.Padding = 5;
                celNo.HorizontalAlignment = Element.ALIGN_CENTER;
                celNo.VerticalAlignment = Element.ALIGN_TOP;
                celNo.Border = 0;
                celNo.BorderWidthLeft = 1;
                celNo.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableFactura.AddCell(celNo);

                string NumLegalFactura = string.Empty;
                if (InvoiceType == "InvoiceType")
                    NumLegalFactura = DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceRef"].ToString();
                else if (InvoiceType == "CreditNoteType")
                    NumLegalFactura = DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"].ToString();
                else
                    NumLegalFactura = DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"].ToString();

                iTextSharp.text.pdf.PdfPCell celNoFactura = new iTextSharp.text.pdf.PdfPCell(new Phrase(NumLegalFactura, fontTitleFactura));
                celNoFactura.Colspan = 1;
                celNoFactura.Padding = 5;
                celNoFactura.HorizontalAlignment = Element.ALIGN_LEFT;
                celNoFactura.VerticalAlignment = Element.ALIGN_TOP;
                //celNoFactura.Border = 1;
                celNoFactura.BackgroundColor = BaseColor.WHITE;
                tableFactura.AddCell(celNoFactura);


                iTextSharp.text.pdf.PdfPCell celEspacioNoFactura = new iTextSharp.text.pdf.PdfPCell(new Phrase(" ", fontTitleFactura));
                celEspacioNoFactura.Colspan = 1;
                celEspacioNoFactura.Border = 0;
                //celNo.Padding = 3;
                celEspacioNoFactura.BorderWidthRight = 1;
                celEspacioNoFactura.HorizontalAlignment = Element.ALIGN_CENTER;
                celEspacioNoFactura.VerticalAlignment = Element.ALIGN_TOP;
                celEspacioNoFactura.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableFactura.AddCell(celEspacioNoFactura);

                iTextSharp.text.pdf.PdfPCell celRellenoNoFactura = new iTextSharp.text.pdf.PdfPCell(new Phrase(" ", fontTitleFactura));
                celRellenoNoFactura.Colspan = 3;
                celRellenoNoFactura.Padding = 3;
                celRellenoNoFactura.Border = 0;
                celRellenoNoFactura.BorderWidthLeft = 1;
                celRellenoNoFactura.BorderWidthRight = 1;
                celRellenoNoFactura.BorderWidthBottom = 1;
                celRellenoNoFactura.BackgroundColor = BaseColor.LIGHT_GRAY;
                celRellenoNoFactura.HorizontalAlignment = Element.ALIGN_RIGHT;
                celRellenoNoFactura.VerticalAlignment = Element.ALIGN_TOP;
                tableFactura.AddCell(celRellenoNoFactura);

                iTextSharp.text.pdf.PdfPCell celConsecutivoInterno = new iTextSharp.text.pdf.PdfPCell(new Phrase("CUFE:"+CUFE, fontCustom));
                celConsecutivoInterno.Colspan = 4;
                celConsecutivoInterno.BackgroundColor = BaseColor.WHITE;
                celConsecutivoInterno.HorizontalAlignment = Element.ALIGN_RIGHT;
                celConsecutivoInterno.VerticalAlignment = Element.ALIGN_TOP;
                celConsecutivoInterno.Border = 0;
                tableFactura.AddCell(celConsecutivoInterno);

                PdfDiv divNumeroFactura = new PdfDiv();
                divNumeroFactura.Float = iTextSharp.text.pdf.PdfDiv.FloatType.LEFT;
                divNumeroFactura.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divNumeroFactura.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                divNumeroFactura.PaddingLeft = 5;
                divNumeroFactura.Width = 140;
                divNumeroFactura.AddElement(tableFactura);
                cell_factura.AddElement(tableFactura);
                Header.AddCell(cell_factura);


                #endregion

                #region FACTURAR Y DESPACHAR A
                //------------------------------------------------------------------------------------------------
                PdfPTable tableFacturar = new PdfPTable(3);
                //Dimenciones.
                float[] DimencionFacturar = new float[3];
                DimencionFacturar[0] = 1.0F;//
                DimencionFacturar[1] = 0.01F;//
                DimencionFacturar[2] = 1.0F;//

                tableFacturar.WidthPercentage = 100;
                tableFacturar.SetWidths(DimencionFacturar);
                //----------------------------------------------------------------------------------------------
                PdfPTable tableFacturarA = new PdfPTable(2);
                float[] DimencionFacturarA = new float[2];
                DimencionFacturarA[0] = 0.8F;//
                DimencionFacturarA[1] = 2.0F;//

                tableFacturarA.WidthPercentage = 100;
                tableFacturarA.SetWidths(DimencionFacturarA);

                iTextSharp.text.pdf.PdfPCell celDatosFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase("FACTURAR A:\n", fontTitleFactura)) { Border = 0, };
                celDatosFacturarA.Colspan = 2;
                celDatosFacturarA.Padding = 3;
                celDatosFacturarA.Border = 0;
                celDatosFacturarA.BorderColorBottom = BaseColor.WHITE;
                celDatosFacturarA.HorizontalAlignment = Element.ALIGN_CENTER;
                celDatosFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celDatosFacturarA);

                iTextSharp.text.pdf.PdfPCell celTextClienteDespacharA2 = new iTextSharp.text.pdf.PdfPCell(new Phrase("CLIENTE:", fontTitleFactura));
                celTextClienteDespacharA2.Colspan = 1;
                celTextClienteDespacharA2.Padding = 3;
                celTextClienteDespacharA2.Border = 0;
                celTextClienteDespacharA2.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextClienteDespacharA2.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celTextClienteDespacharA2);

                iTextSharp.text.pdf.PdfPCell celClienteDespacharA2 = new iTextSharp.text.pdf.PdfPCell(new Phrase("" + DsInvoiceAR.Tables["Customer"].Rows[0]["Name"].ToString(), fontTitle2));
                celClienteDespacharA2.Colspan = 1;
                celClienteDespacharA2.Padding = 3;
                celClienteDespacharA2.Border = 0;
                celClienteDespacharA2.HorizontalAlignment = Element.ALIGN_LEFT;
                celClienteDespacharA2.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celClienteDespacharA2);

                iTextSharp.text.pdf.PdfPCell celTextNitDespacharA2 = new iTextSharp.text.pdf.PdfPCell(new Phrase("NIT:", fontTitleFactura));
                celTextNitDespacharA2.Colspan = 1;
                celTextNitDespacharA2.Padding = 3;
                celTextNitDespacharA2.Border = 0;
                celTextNitDespacharA2.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextNitDespacharA2.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celTextNitDespacharA2);

                iTextSharp.text.pdf.PdfPCell celNitDespacharA2 = new iTextSharp.text.pdf.PdfPCell(new Phrase("" + DsInvoiceAR.Tables["Customer"].Rows[0]["CustID"].ToString(), fontTitle2));
                celNitDespacharA2.Colspan = 1;
                celNitDespacharA2.Padding = 3;
                celNitDespacharA2.Border = 0;
                celNitDespacharA2.HorizontalAlignment = Element.ALIGN_LEFT;
                celNitDespacharA2.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celNitDespacharA2);

                iTextSharp.text.pdf.PdfPCell celTextDireccionDespacharA2 = new iTextSharp.text.pdf.PdfPCell(new Phrase("DIRECCION:", fontTitleFactura));
                celTextDireccionDespacharA2.Colspan = 1;
                celTextDireccionDespacharA2.Padding = 3;
                celTextDireccionDespacharA2.Border = 0;
                celTextDireccionDespacharA2.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextDireccionDespacharA2.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celTextDireccionDespacharA2);

                iTextSharp.text.pdf.PdfPCell celDireccionDespacharA2 = new iTextSharp.text.pdf.PdfPCell(new Phrase("" + DsInvoiceAR.Tables["Customer"].Rows[0]["Address1"].ToString(), fontTitle2));
                celDireccionDespacharA2.Colspan = 1;
                celDireccionDespacharA2.Padding = 3;
                celDireccionDespacharA2.Border = 0;
                celDireccionDespacharA2.HorizontalAlignment = Element.ALIGN_LEFT;
                celDireccionDespacharA2.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celDireccionDespacharA2);

                iTextSharp.text.pdf.PdfPCell celTextTelDespacharA2 = new iTextSharp.text.pdf.PdfPCell(new Phrase("TELEFONO:", fontTitleFactura));
                celTextTelDespacharA2.Colspan = 1;
                celTextTelDespacharA2.Padding = 3;
                celTextTelDespacharA2.Border = 0;
                celTextTelDespacharA2.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextTelDespacharA2.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celTextTelDespacharA2);

                iTextSharp.text.pdf.PdfPCell celTelDespacharA2 = new iTextSharp.text.pdf.PdfPCell(new Phrase("" + DsInvoiceAR.Tables["Customer"].Rows[0]["PhoneNum"].ToString(), fontTitle2));
                celTelDespacharA2.Colspan = 1;
                celTelDespacharA2.Padding = 3;
                celTelDespacharA2.Border = 0;
                celTelDespacharA2.HorizontalAlignment = Element.ALIGN_LEFT;
                celTelDespacharA2.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celTelDespacharA2);

                iTextSharp.text.pdf.PdfPCell celTextCiudadDespacharA2 = new iTextSharp.text.pdf.PdfPCell(new Phrase("CIUDAD:", fontTitleFactura));
                celTextCiudadDespacharA2.Colspan = 1;
                celTextCiudadDespacharA2.Padding = 3;
                celTextCiudadDespacharA2.Border = 0;
                celTextCiudadDespacharA2.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextCiudadDespacharA2.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celTextCiudadDespacharA2);

                iTextSharp.text.pdf.PdfPCell celCiudadDespacharA2 = new iTextSharp.text.pdf.PdfPCell(new Phrase("" + DsInvoiceAR.Tables["Customer"].Rows[0]["City"].ToString(), fontTitle2));
                celCiudadDespacharA2.Colspan = 1;
                celCiudadDespacharA2.Padding = 3;
                celCiudadDespacharA2.Border = 0;
                celCiudadDespacharA2.HorizontalAlignment = Element.ALIGN_LEFT;
                celCiudadDespacharA2.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celCiudadDespacharA2);

                iTextSharp.text.pdf.PdfPCell celTextPaisDespacharA2 = new iTextSharp.text.pdf.PdfPCell(new Phrase("PAIS:", fontTitleFactura));
                celTextPaisDespacharA2.Colspan = 1;
                celTextPaisDespacharA2.Padding = 3;
                celTextPaisDespacharA2.Border = 0;
                celTextPaisDespacharA2.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextPaisDespacharA2.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celTextPaisDespacharA2);

                iTextSharp.text.pdf.PdfPCell celPaisDespacharA2 = new iTextSharp.text.pdf.PdfPCell(new Phrase("" + DsInvoiceAR.Tables["Customer"].Rows[0]["ShortChar01"].ToString(), fontTitle2));
                celPaisDespacharA2.Colspan = 1;
                celPaisDespacharA2.Padding = 3;
                celPaisDespacharA2.Border = 0;
                celPaisDespacharA2.HorizontalAlignment = Element.ALIGN_LEFT;
                celPaisDespacharA2.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celPaisDespacharA2);

                iTextSharp.text.pdf.PdfPCell celDatosDespacharA2 = new iTextSharp.text.pdf.PdfPCell(tableFacturarA);
                tableFacturar.AddCell(celDatosDespacharA2);



                //----------------------------------------------------------------------------------------------------------------------------

                iTextSharp.text.pdf.PdfPCell celEspacio2 = new iTextSharp.text.pdf.PdfPCell(new Phrase(" ", fontTitleFactura));
                celEspacio2.Border = 0;
                tableFacturar.AddCell(celEspacio2);

                //------------------------------------------------------------------------------------------

                PdfPTable tableDespacharA = new PdfPTable(2);
                float[] DimencionDespacharA = new float[2];
                DimencionDespacharA[0] = 0.8F;//
                DimencionDespacharA[1] = 2.0F;//

                tableDespacharA.WidthPercentage = 100;
                tableDespacharA.SetWidths(DimencionDespacharA);

                iTextSharp.text.pdf.PdfPCell celTittleDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase("INFORMACIÓN OV:\n", fontTitleFactura));
                celTittleDespacharA.Colspan = 2;
                celTittleDespacharA.Padding = 3;
                celTittleDespacharA.Border = 0;
                celTittleDespacharA.BorderColorBottom = BaseColor.WHITE;
                celTittleDespacharA.HorizontalAlignment = Element.ALIGN_CENTER;
                celTittleDespacharA.VerticalAlignment = Element.ALIGN_TOP;
                tableDespacharA.AddCell(celTittleDespacharA);


                iTextSharp.text.pdf.PdfPCell celTextClienteDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase("CLIENTE:", fontTitleFactura));
                celTextClienteDespacharA.Colspan = 1;
                celTextClienteDespacharA.Padding = 3;
                celTextClienteDespacharA.Border = 0;
                celTextClienteDespacharA.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextClienteDespacharA.VerticalAlignment = Element.ALIGN_TOP;
                tableDespacharA.AddCell(celTextClienteDespacharA);

                iTextSharp.text.pdf.PdfPCell celClienteDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase("" + DsInvoiceAR.Tables["Customer"].Rows[0]["ShipToName"].ToString(), fontTitle2));
                celClienteDespacharA.Colspan = 1;
                celClienteDespacharA.Padding = 3;
                celClienteDespacharA.Border = 0;
                celClienteDespacharA.HorizontalAlignment = Element.ALIGN_LEFT;
                celClienteDespacharA.VerticalAlignment = Element.ALIGN_TOP;
                tableDespacharA.AddCell(celClienteDespacharA);

                iTextSharp.text.pdf.PdfPCell celTextNitDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase("NIT:", fontTitleFactura));
                celTextNitDespacharA.Colspan = 1;
                celTextNitDespacharA.Padding = 3;
                celTextNitDespacharA.Border = 0;
                celTextNitDespacharA.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextNitDespacharA.VerticalAlignment = Element.ALIGN_TOP;
                tableDespacharA.AddCell(celTextNitDespacharA);

                iTextSharp.text.pdf.PdfPCell celNitDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase("" + DsInvoiceAR.Tables["Customer"].Rows[0]["ShipToNit"].ToString(), fontTitle2));
                celNitDespacharA.Colspan = 1;
                celNitDespacharA.Padding = 3;
                celNitDespacharA.Border = 0;
                celNitDespacharA.HorizontalAlignment = Element.ALIGN_LEFT;
                celNitDespacharA.VerticalAlignment = Element.ALIGN_TOP;
                tableDespacharA.AddCell(celNitDespacharA);

                iTextSharp.text.pdf.PdfPCell celTextDireccionDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase("DIRECCION:", fontTitleFactura));
                celTextDireccionDespacharA.Colspan = 1;
                celTextDireccionDespacharA.Padding = 3;
                celTextDireccionDespacharA.Border = 0;
                celTextDireccionDespacharA.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextDireccionDespacharA.VerticalAlignment = Element.ALIGN_TOP;
                tableDespacharA.AddCell(celTextDireccionDespacharA);

                iTextSharp.text.pdf.PdfPCell celDireccionDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase("" + DsInvoiceAR.Tables["Customer"].Rows[0]["ShipToAddress"].ToString(), fontTitle2));
                celDireccionDespacharA.Colspan = 1;
                celDireccionDespacharA.Padding = 3;
                celDireccionDespacharA.Border = 0;
                celDireccionDespacharA.HorizontalAlignment = Element.ALIGN_LEFT;
                celDireccionDespacharA.VerticalAlignment = Element.ALIGN_TOP;
                tableDespacharA.AddCell(celDireccionDespacharA);

                iTextSharp.text.pdf.PdfPCell celTextTelDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase("TELEFONO:", fontTitleFactura));
                celTextTelDespacharA.Colspan = 1;
                celTextTelDespacharA.Padding = 3;
                celTextTelDespacharA.Border = 0;
                celTextTelDespacharA.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextTelDespacharA.VerticalAlignment = Element.ALIGN_TOP;
                tableDespacharA.AddCell(celTextTelDespacharA);

                iTextSharp.text.pdf.PdfPCell celTelDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase("" + DsInvoiceAR.Tables["Customer"].Rows[0]["ShipToPhone"].ToString(), fontTitle2));
                celTelDespacharA.Colspan = 1;
                celTelDespacharA.Padding = 3;
                celTelDespacharA.Border = 0;
                celTelDespacharA.HorizontalAlignment = Element.ALIGN_LEFT;
                celTelDespacharA.VerticalAlignment = Element.ALIGN_TOP;
                tableDespacharA.AddCell(celTelDespacharA);

                iTextSharp.text.pdf.PdfPCell celTextCiudadDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase("CIUDAD:", fontTitleFactura));
                celTextCiudadDespacharA.Colspan = 1;
                celTextCiudadDespacharA.Padding = 3;
                celTextCiudadDespacharA.Border = 0;
                celTextCiudadDespacharA.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextCiudadDespacharA.VerticalAlignment = Element.ALIGN_TOP;
                tableDespacharA.AddCell(celTextCiudadDespacharA);

                iTextSharp.text.pdf.PdfPCell celCiudadDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase("" + DsInvoiceAR.Tables["Customer"].Rows[0]["ShipToCity"].ToString(), fontTitle2));
                celCiudadDespacharA.Colspan = 1;
                celCiudadDespacharA.Padding = 3;
                celCiudadDespacharA.Border = 0;
                celCiudadDespacharA.HorizontalAlignment = Element.ALIGN_LEFT;
                celCiudadDespacharA.VerticalAlignment = Element.ALIGN_TOP;
                tableDespacharA.AddCell(celCiudadDespacharA);

                iTextSharp.text.pdf.PdfPCell celTextPaisDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase("PAIS:", fontTitleFactura));
                celTextPaisDespacharA.Colspan = 1;
                celTextPaisDespacharA.Padding = 3;
                celTextPaisDespacharA.Border = 0;
                celTextPaisDespacharA.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextPaisDespacharA.VerticalAlignment = Element.ALIGN_TOP;
                tableDespacharA.AddCell(celTextPaisDespacharA);

                iTextSharp.text.pdf.PdfPCell celPaisDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase("" + DsInvoiceAR.Tables["Customer"].Rows[0]["ShipToCountry"].ToString(), fontTitle2));
                celPaisDespacharA.Colspan = 1;
                celPaisDespacharA.Padding = 3;
                celPaisDespacharA.Border = 0;
                celPaisDespacharA.HorizontalAlignment = Element.ALIGN_LEFT;
                celPaisDespacharA.VerticalAlignment = Element.ALIGN_TOP;
                tableDespacharA.AddCell(celPaisDespacharA);

                iTextSharp.text.pdf.PdfPCell celDatosDespacharA = new iTextSharp.text.pdf.PdfPCell(tableDespacharA);
                tableFacturar.AddCell(celDatosDespacharA);
                //-------------------------------------------------------------------------------------- 
                #endregion

                #region Tabla de Detalles
                //------------------------------------------------------------------------------------------------
                PdfPTable tableDetalles = new PdfPTable(6);
                tableDetalles.PaddingTop = 20;
                //Dimenciones.
                float[] DimencionDetalles = new float[6];
                DimencionDetalles[0] = 1.5F;//
                DimencionDetalles[1] = 1.0F;//
                DimencionDetalles[2] = 1.0F;//
                DimencionDetalles[3] = 1.0F;//
                DimencionDetalles[4] = 1.0F;//
                DimencionDetalles[5] = 0.8F;//

                tableDetalles.WidthPercentage = 100;
                tableDetalles.SetWidths(DimencionDetalles);

                iTextSharp.text.pdf.PdfPCell celVendedor = new iTextSharp.text.pdf.PdfPCell(new Phrase("VENDEDOR", fontTitleFactura));
                celVendedor.Colspan = 1;
                celVendedor.Padding = 3;
                //celVendedor.Border = 0;
                celVendedor.BorderColorBottom = BaseColor.WHITE;
                celVendedor.HorizontalAlignment = Element.ALIGN_CENTER;
                celVendedor.VerticalAlignment = Element.ALIGN_TOP;
                tableDetalles.AddCell(celVendedor);

                iTextSharp.text.pdf.PdfPCell celOC_Cliente = new iTextSharp.text.pdf.PdfPCell(new Phrase("O.C. CLIENTE", fontTitleFactura));
                celOC_Cliente.Colspan = 1;
                celOC_Cliente.Padding = 3;
                //celVendedor.Border = 0;
                celOC_Cliente.BorderColorBottom = BaseColor.WHITE;
                celOC_Cliente.HorizontalAlignment = Element.ALIGN_CENTER;
                celOC_Cliente.VerticalAlignment = Element.ALIGN_TOP;
                tableDetalles.AddCell(celOC_Cliente);

                iTextSharp.text.pdf.PdfPCell celOrdenVenta = new iTextSharp.text.pdf.PdfPCell(new Phrase("PEDIDO", fontTitleFactura));
                celOrdenVenta.Colspan = 1;
                celOrdenVenta.Padding = 3;
                //celVendedor.Border = 0;
                celOrdenVenta.BorderColorBottom = BaseColor.WHITE;
                celOrdenVenta.HorizontalAlignment = Element.ALIGN_CENTER;
                celOrdenVenta.VerticalAlignment = Element.ALIGN_TOP;
                tableDetalles.AddCell(celOrdenVenta);

                iTextSharp.text.pdf.PdfPCell celTerminoPago = new iTextSharp.text.pdf.PdfPCell(new Phrase("TÉRMINO DE PAGO", fontTitleFactura));
                celTerminoPago.Colspan = 1;
                celTerminoPago.Padding = 3;
                //celVendedor.Border = 0;
                celTerminoPago.BorderColorBottom = BaseColor.WHITE;
                celTerminoPago.HorizontalAlignment = Element.ALIGN_CENTER;
                celTerminoPago.VerticalAlignment = Element.ALIGN_TOP;
                tableDetalles.AddCell(celTerminoPago);

                iTextSharp.text.pdf.PdfPCell celorden = new iTextSharp.text.pdf.PdfPCell(new Phrase("ORDEN DE COMBRA", fontTitleFactura));
                celorden.Colspan = 1;
                celorden.Padding = 3;
                //celVendedor.Border = 0;
                celorden.BorderColorBottom = BaseColor.WHITE;
                celorden.HorizontalAlignment = Element.ALIGN_CENTER;
                celorden.VerticalAlignment = Element.ALIGN_TOP;
                tableDetalles.AddCell(celorden);

                iTextSharp.text.pdf.PdfPCell celRemision = new iTextSharp.text.pdf.PdfPCell(new Phrase("REMICION", fontTitleFactura));
                celRemision.Colspan = 1;
                celRemision.Padding = 3;
                //celVendedor.Border = 0;
                celRemision.BorderColorBottom = BaseColor.WHITE;
                celRemision.HorizontalAlignment = Element.ALIGN_CENTER;
                celRemision.VerticalAlignment = Element.ALIGN_TOP;
                tableDetalles.AddCell(celRemision);

                iTextSharp.text.pdf.PdfPCell celDataVendedor = new iTextSharp.text.pdf.PdfPCell(new Phrase("" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["SalesRepName1"].ToString(), fontCustom));
                celDataVendedor.Colspan = 1;
                celDataVendedor.Padding = 3;
                //celVendedor.Border = 0;
                celDataVendedor.BorderColorBottom = BaseColor.WHITE;
                celDataVendedor.HorizontalAlignment = Element.ALIGN_CENTER;
                celDataVendedor.VerticalAlignment = Element.ALIGN_TOP;
                tableDetalles.AddCell(celDataVendedor);

                iTextSharp.text.pdf.PdfPCell celDataOC_Cliente = new iTextSharp.text.pdf.PdfPCell(new Phrase("" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["PONum"].ToString(), fontCustom));
                celDataOC_Cliente.Colspan = 1;
                celDataOC_Cliente.Padding = 3;
                //celVendedor.Border = 0;
                celDataOC_Cliente.BorderColorBottom = BaseColor.WHITE;
                celDataOC_Cliente.HorizontalAlignment = Element.ALIGN_CENTER;
                celDataOC_Cliente.VerticalAlignment = Element.ALIGN_TOP;
                tableDetalles.AddCell(celDataOC_Cliente);

                iTextSharp.text.pdf.PdfPCell celDataOrdenVenta = new iTextSharp.text.pdf.PdfPCell(new Phrase(DsInvoiceAR.Tables["InvcHead"].Rows[0]["OrderNum"].ToString(),
                    fontCustom));
                celDataOrdenVenta.Colspan = 1;
                celDataOrdenVenta.Padding = 3;
                //celVendedor.Border = 0;
                celDataOrdenVenta.BorderColorBottom = BaseColor.WHITE;
                celDataOrdenVenta.HorizontalAlignment = Element.ALIGN_CENTER;
                celDataOrdenVenta.VerticalAlignment = Element.ALIGN_TOP;
                 tableDetalles.AddCell(celDataOrdenVenta);

                iTextSharp.text.pdf.PdfPCell celDataTerminoPago = new iTextSharp.text.pdf.PdfPCell(new Phrase("" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["OrderNum"].ToString(), fontCustom));
                celDataTerminoPago.Colspan = 1;
                celDataTerminoPago.Padding = 3;
                //celVendedor.Border = 0;
                celDataTerminoPago.BorderColorBottom = BaseColor.WHITE;
                celDataTerminoPago.HorizontalAlignment = Element.ALIGN_CENTER;
                celDataTerminoPago.VerticalAlignment = Element.ALIGN_TOP;
                tableDetalles.AddCell(celDataTerminoPago);

                iTextSharp.text.pdf.PdfPCell celDataorden = new iTextSharp.text.pdf.PdfPCell(new Phrase("" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["OrderNum"].ToString(), fontCustom));
                celDataorden.Colspan = 1;
                celDataorden.Padding = 3;
                //celVendedor.Border = 0;
                celDataorden.BorderColorBottom = BaseColor.WHITE;
                celDataorden.HorizontalAlignment = Element.ALIGN_CENTER;
                celDataorden.VerticalAlignment = Element.ALIGN_TOP;
                tableDetalles.AddCell(celDataorden);


                iTextSharp.text.pdf.PdfPCell celDataRemision = new iTextSharp.text.pdf.PdfPCell(new Phrase("" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["OrderNum"].ToString(), fontCustom));
                celDataRemision.Colspan = 1;
                celDataRemision.Padding = 3;
                //celVendedor.Border = 0;
                celDataRemision.BorderColorBottom = BaseColor.WHITE;
                celDataRemision.HorizontalAlignment = Element.ALIGN_CENTER;
                celDataRemision.VerticalAlignment = Element.ALIGN_TOP;
                tableDetalles.AddCell(celDataRemision);

                //-------------------------------------------------------------------

                PdfPTable tableDetalles2 = new PdfPTable(3);
                tableDetalles2.WidthPercentage = 100;

                PdfPTable tableFechaFactura = new PdfPTable(2);
                float[] dimecionesTablaFecha = new float[2];
                dimecionesTablaFecha[0] = 0.7F;
                dimecionesTablaFecha[1] = 1.0F;
                tableFechaFactura.SetWidths(dimecionesTablaFecha);

                iTextSharp.text.pdf.PdfPCell celTextFechaFactura = new iTextSharp.text.pdf.PdfPCell(new Phrase("FECHA FACTURA: ",
                    fontTitle));
                celTextFechaFactura.Colspan = 1;
                celTextFechaFactura.Padding = 3;
                celTextFechaFactura.Border = 0;
                celTextFechaFactura.BorderColorBottom = BaseColor.WHITE;
                celTextFechaFactura.HorizontalAlignment = Element.ALIGN_CENTER;
                celTextFechaFactura.VerticalAlignment = Element.ALIGN_CENTER;
                tableFechaFactura.AddCell(celTextFechaFactura);

                iTextSharp.text.pdf.PdfPCell celFechaFactura = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:MM/dd/yyyy}", DateTime.Parse(DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"].ToString())),
                fontCustom));
                celFechaFactura.Colspan = 1;
                celFechaFactura.Padding = 3;
                celFechaFactura.PaddingTop = 4;
                celFechaFactura.Border = 0;
                celFechaFactura.BorderColorBottom = BaseColor.WHITE;
                celFechaFactura.HorizontalAlignment = Element.ALIGN_LEFT;
                celFechaFactura.VerticalAlignment = Element.ALIGN_CENTER;
                tableFechaFactura.AddCell(celFechaFactura);

                PdfPCell _celFechaFactura = new PdfPCell(tableFechaFactura);
                tableDetalles2.AddCell(_celFechaFactura);


                PdfPTable tableFechaVencimiento = new PdfPTable(2);
                float[] dmTablaFechaVencimiento = new float[2];
                dmTablaFechaVencimiento[0] = 1.0F;
                dmTablaFechaVencimiento[1] = 1.0F;
                tableFechaVencimiento.SetWidths(dmTablaFechaVencimiento);

                iTextSharp.text.pdf.PdfPCell celTextFechaVencimiento = new iTextSharp.text.pdf.PdfPCell(new Phrase("FECHA VENCIMIENTO: ",
                    fontTitle));
                celTextFechaVencimiento.Colspan = 1;
                celTextFechaVencimiento.Padding = 3;
                celTextFechaVencimiento.Border = 0;
                celTextFechaVencimiento.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextFechaVencimiento.VerticalAlignment = Element.ALIGN_TOP;
                tableFechaVencimiento.AddCell(celTextFechaVencimiento);

                iTextSharp.text.pdf.PdfPCell celFechaVencimiento = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:MM/dd/yyyy}",
                   DateTime.Parse(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"].ToString())),
                fontCustom));
                celFechaVencimiento.Colspan = 1;
                celFechaVencimiento.Padding = 4;
                celFechaVencimiento.Border = 0;
                celFechaVencimiento.BorderColorBottom = BaseColor.WHITE;
                celFechaVencimiento.HorizontalAlignment = Element.ALIGN_LEFT;
                celFechaVencimiento.VerticalAlignment = Element.ALIGN_TOP;
                tableFechaVencimiento.AddCell(celFechaVencimiento);

                PdfPCell _celFechaFacturaVencimiento = new PdfPCell(tableFechaVencimiento);
                tableDetalles2.AddCell(_celFechaFacturaVencimiento);

                PdfPTable tableMoneda = new PdfPTable(2);
                float[] dimecionesTablaMoneda = new float[2];
                dimecionesTablaMoneda[0] = 0.4F;
                dimecionesTablaMoneda[1] = 1.0F;
                tableMoneda.SetWidths(dimecionesTablaMoneda);

                iTextSharp.text.pdf.PdfPCell celTextMoneda = new iTextSharp.text.pdf.PdfPCell(new Phrase("MONEDA: ",
                    fontTitle));
                celTextMoneda.Colspan = 1;
                celTextMoneda.Padding = 3;
                celTextMoneda.Border = 0;
                celTextMoneda.BorderColorBottom = BaseColor.WHITE;
                celTextMoneda.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextMoneda.VerticalAlignment = Element.ALIGN_TOP;
                tableMoneda.AddCell(celTextMoneda);

                iTextSharp.text.pdf.PdfPCell celMoneda = new iTextSharp.text.pdf.PdfPCell(new Phrase(DsInvoiceAR.Tables["InvcHead"].Rows[0]["CurrencyCode"].ToString(),
                fontCustom));
                celMoneda.Colspan = 1;
                celMoneda.Padding = 4;
                celMoneda.Border = 0;
                celMoneda.BorderColorBottom = BaseColor.WHITE;
                celMoneda.HorizontalAlignment = Element.ALIGN_LEFT;
                celMoneda.VerticalAlignment = Element.ALIGN_TOP;
                tableMoneda.AddCell(celMoneda);

                PdfPCell _celMoneda = new PdfPCell(tableMoneda);
                tableDetalles2.AddCell(_celMoneda);

                #endregion

                #region Tabla Unidades
                PdfPTable tableTituloUnidades = new PdfPTable(6);
                //Dimenciones.
                float[] DimencionUnidades = new float[6];
                DimencionUnidades[0] = 1.0F;//
                DimencionUnidades[1] = 3.0F;//
                DimencionUnidades[2] = 1.0F;//
                DimencionUnidades[3] = 0.5F;//
                DimencionUnidades[4] = 1.0F;//
                DimencionUnidades[5] = 1.5F;//

                tableTituloUnidades.WidthPercentage = 100;
                tableTituloUnidades.SetWidths(DimencionUnidades);

                iTextSharp.text.pdf.PdfPCell celCodigo = new iTextSharp.text.pdf.PdfPCell(new Phrase("CÓDIGO", fontTitleFactura));
                celCodigo.Colspan = 1;
                celCodigo.Padding = 3;
                //celCodigo.Border = 1;
                celCodigo.BorderWidthRight = 0;
                //celCodigo.BorderColorBottom = BaseColor.WHITE;
                celCodigo.HorizontalAlignment = Element.ALIGN_CENTER;
                celCodigo.VerticalAlignment = Element.ALIGN_TOP;
                celCodigo.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableTituloUnidades.AddCell(celCodigo);

                iTextSharp.text.pdf.PdfPCell celDescripcion = new iTextSharp.text.pdf.PdfPCell(new Phrase("DESCRIPCIÓN", fontTitleFactura));
                celDescripcion.Colspan = 1;
                celDescripcion.Padding = 3;
                //celDescripcion.Border = 1;
                celDescripcion.BorderWidthRight = 0;
                //celDescripcion.BorderColorBottom = BaseColor.WHITE;
                celDescripcion.HorizontalAlignment = Element.ALIGN_CENTER;
                celDescripcion.VerticalAlignment = Element.ALIGN_TOP;
                celDescripcion.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableTituloUnidades.AddCell(celDescripcion);

                iTextSharp.text.pdf.PdfPCell celCantidad = new iTextSharp.text.pdf.PdfPCell(new Phrase("CANTIDAD", fontTitleFactura));
                celCantidad.Colspan = 1;
                celCantidad.Padding = 3;
                //celVendedor.Border = 0;
                celCantidad.BorderWidthRight = 0;
                //celCantidad.BorderColorBottom = BaseColor.WHITE;
                celCantidad.HorizontalAlignment = Element.ALIGN_CENTER;
                celCantidad.VerticalAlignment = Element.ALIGN_TOP;
                celCantidad.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableTituloUnidades.AddCell(celCantidad);

                iTextSharp.text.pdf.PdfPCell celUnidad = new iTextSharp.text.pdf.PdfPCell(new Phrase("UNIDAD", fontTitleFactura));
                celUnidad.Colspan = 1;
                celUnidad.Padding = 3;
                //celVendedor.Border = 0;
                celUnidad.BorderWidthRight = 0;
                //celUnidad.BorderColorBottom = BaseColor.WHITE;
                celUnidad.HorizontalAlignment = Element.ALIGN_CENTER;
                celUnidad.VerticalAlignment = Element.ALIGN_TOP;
                celUnidad.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableTituloUnidades.AddCell(celUnidad);

                iTextSharp.text.pdf.PdfPCell celValorUnitario = new iTextSharp.text.pdf.PdfPCell(new Phrase("VALOR UNITARIO", fontTitleFactura));
                celValorUnitario.Colspan = 1;
                celValorUnitario.Padding = 3;
                //celVendedor.Border = 0;
                celValorUnitario.BorderWidthRight = 0;
                //celValorUnitario.BorderColorBottom = BaseColor.WHITE;
                celValorUnitario.HorizontalAlignment = Element.ALIGN_CENTER;
                celValorUnitario.VerticalAlignment = Element.ALIGN_TOP;
                celValorUnitario.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableTituloUnidades.AddCell(celValorUnitario);

                iTextSharp.text.pdf.PdfPCell celValorTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase("VALOR TOTAL", fontTitleFactura));
                celValorTotal.Colspan = 1;
                celValorTotal.Padding = 3;
                //celVendedor.Border = 0;
                //celValorTotal.BorderColorBottom = BaseColor.WHITE;
                celValorTotal.HorizontalAlignment = Element.ALIGN_CENTER;
                celValorTotal.VerticalAlignment = Element.ALIGN_TOP;
                celValorTotal.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableTituloUnidades.AddCell(celValorTotal);

                PdfPTable tableUnidades = new PdfPTable(6);
                tableUnidades.WidthPercentage = 100;
                tableUnidades.SetWidths(DimencionUnidades);

                foreach (DataRow InvoiceLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
                {
                    if (!AddUnidadesinnovak(InvoiceLine, ref tableUnidades, fontCustom, DsInvoiceAR))
                        return false;
                }

                iTextSharp.text.pdf.PdfPCell LineaFinal = new iTextSharp.text.pdf.PdfPCell();
                LineaFinal.Colspan = 6;
                LineaFinal.Padding = 3;
                LineaFinal.Border = 1;
                LineaFinal.BorderColorBottom = BaseColor.WHITE;
                LineaFinal.HorizontalAlignment = Element.ALIGN_CENTER;
                LineaFinal.VerticalAlignment = Element.ALIGN_TOP;
                tableUnidades.AddCell(LineaFinal);

                //------------------------------------------------------------------------------------------------
                iTextSharp.text.Font fontEspacio = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 0.3F, iTextSharp.text.Font.NORMAL);
                PdfPCell celEspacio3 = new PdfPCell(new Phrase("  ", fontEspacio));
                celEspacio3.Border = 0;

                PdfPTable tableTotalesObs = new PdfPTable(3);

                float[] DimencionTotalesObs = new float[3];
                DimencionTotalesObs[0] = 2.5F;//
                DimencionTotalesObs[1] = 0.01F;//
                DimencionTotalesObs[2] = 1.0F;//

                tableTotalesObs.WidthPercentage = 100;
                tableTotalesObs.SetWidths(DimencionTotalesObs);

                PdfPTable _tableObs = new PdfPTable(1);
                _tableObs.WidthPercentage = 100;

                iTextSharp.text.pdf.PdfPCell celValorLetras = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("VALORES EN LETRA: {0}",
                    Nroenletras(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"].ToString())), fontTitleFactura));
                celValorLetras.Colspan = 1;
                celValorLetras.Padding = 3;
                //celValorLetras.Border = 0;
                celValorLetras.BorderColorBottom = BaseColor.WHITE;
                celValorLetras.HorizontalAlignment = Element.ALIGN_LEFT;
                celValorLetras.VerticalAlignment = Element.ALIGN_TOP;

                iTextSharp.text.pdf.PdfPCell celValorAsegurado = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format(" {0:C2}",
                   decimal.Parse(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubTotal"].ToString())), fontTitle2));
                celValorAsegurado.Colspan = 1;
                celValorAsegurado.Padding = 5;
                //celValorLetras.Border = 0;
                celValorAsegurado.BorderColorBottom = BaseColor.WHITE;
                celValorAsegurado.HorizontalAlignment = Element.ALIGN_LEFT;
                celValorAsegurado.VerticalAlignment = Element.ALIGN_TOP;

                iTextSharp.text.pdf.PdfPCell celObservaciones = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("OBSERVACIONES:\n{0}",
                    DsInvoiceAR.Tables["InvcHead"].Rows[0]["ObservacionCliente_c"].ToString()), fontTitleFactura));
                celObservaciones.Colspan = 1;
                celObservaciones.Padding = 3;
                //celValorLetras.Border = 0;
                celObservaciones.BorderColorBottom = BaseColor.WHITE;
                celObservaciones.HorizontalAlignment = Element.ALIGN_LEFT;
                celObservaciones.VerticalAlignment = Element.ALIGN_TOP;

                _tableObs.AddCell(celValorLetras);
                _tableObs.AddCell(celEspacio3);
                _tableObs.AddCell(celValorAsegurado);
                _tableObs.AddCell(celEspacio3);
                _tableObs.AddCell(celObservaciones);

                iTextSharp.text.pdf.PdfPCell _celObs = new iTextSharp.text.pdf.PdfPCell(_tableObs);
                _celObs.Border = 0;
                tableTotalesObs.AddCell(_celObs);
                //----------------------------------------------------------------------------------------------------------------------------

                iTextSharp.text.pdf.PdfPCell _celEspacio2 = new iTextSharp.text.pdf.PdfPCell(new Phrase(" ", fontTitleFactura));
                _celEspacio2.Border = 0;
                tableTotalesObs.AddCell(_celEspacio2);

                //------------------------------------------------------------------------------------------

                PdfPTable _tableTotales = new PdfPTable(2);
                float[] _DimencionTotales = new float[2];
                _DimencionTotales[0] = 1.5F;//
                _DimencionTotales[1] = 2.5F;//

                _tableTotales.WidthPercentage = 100;
                _tableTotales.SetWidths(_DimencionTotales);



                iTextSharp.text.pdf.PdfPCell _celTextDescuentos = new iTextSharp.text.pdf.PdfPCell(new Phrase("", fontTitleFactura));
                _celTextDescuentos.Colspan = 1;
                _celTextDescuentos.Padding = 3;
                _celTextDescuentos.PaddingBottom = 5;
                _celTextDescuentos.Border = 0;
                _celTextDescuentos.BorderWidthRight = 1;
                _celTextDescuentos.BorderColorBottom = BaseColor.WHITE;
                _celTextDescuentos.HorizontalAlignment = Element.ALIGN_LEFT;
                _celTextDescuentos.VerticalAlignment = Element.ALIGN_TOP;
                _tableTotales.AddCell(_celTextDescuentos);

                iTextSharp.text.pdf.PdfPCell _celDescuentos = new iTextSharp.text.pdf.PdfPCell(new Phrase("", fontTitle2));
                _celDescuentos.Colspan = 1;
                _celDescuentos.Padding = 3;
                _celDescuentos.PaddingBottom = 5;
                _celDescuentos.Border = 0;
                _celDescuentos.BorderColorBottom = BaseColor.WHITE;
                _celDescuentos.HorizontalAlignment = Element.ALIGN_RIGHT;
                _celDescuentos.VerticalAlignment = Element.ALIGN_TOP;
                _tableTotales.AddCell(_celDescuentos);


                iTextSharp.text.pdf.PdfPCell _celTextSubTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase("SUBTOTAL", fontTitleFactura));
                _celTextSubTotal.Colspan = 1;
                _celTextSubTotal.Padding = 3;
                _celTextSubTotal.PaddingBottom = 5;
                //_celTextSubTotal.Border = 0;
                _celTextSubTotal.BorderWidthRight = 1;
                _celTextSubTotal.BorderColorBottom = BaseColor.WHITE;
                _celTextSubTotal.HorizontalAlignment = Element.ALIGN_LEFT;
                _celTextSubTotal.VerticalAlignment = Element.ALIGN_TOP;
                _tableTotales.AddCell(_celTextSubTotal);

                iTextSharp.text.pdf.PdfPCell _celSubTotal2 = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:C2}",
                    decimal.Parse(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DocTaxAmt"].ToString())), fontTitle2));
                _celSubTotal2.Colspan = 1;
                _celSubTotal2.Padding = 3;
                _celSubTotal2.PaddingBottom = 5;
                //_celSubTotal2.Border = 0;
                _celSubTotal2.BorderColorBottom = BaseColor.WHITE;
                _celSubTotal2.HorizontalAlignment = Element.ALIGN_RIGHT;
                _celSubTotal2.VerticalAlignment = Element.ALIGN_TOP;
                _tableTotales.AddCell(_celSubTotal2);

                iTextSharp.text.pdf.PdfPCell _celTextSubTotal2v = new iTextSharp.text.pdf.PdfPCell(new Phrase("SUBTOTAL2", fontTitleFactura));
                _celTextSubTotal2v.Colspan = 1;
                _celTextSubTotal2v.Padding = 3;
                _celTextSubTotal2v.PaddingBottom = 5;
                //_celTextSubTotal.Border = 0;
                _celTextSubTotal2v.BorderWidthRight = 1;
                _celTextSubTotal2v.BorderColorBottom = BaseColor.WHITE;
                _celTextSubTotal2v.HorizontalAlignment = Element.ALIGN_LEFT;
                _celTextSubTotal2v.VerticalAlignment = Element.ALIGN_TOP;
                _tableTotales.AddCell(_celTextSubTotal2v);

                iTextSharp.text.pdf.PdfPCell _celSubTotal22 = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:C2}",
                    decimal.Parse(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DocTaxAmt"].ToString())), fontTitle2));
                _celSubTotal22.Colspan = 1;
                _celSubTotal22.Padding = 3;
                _celSubTotal22.PaddingBottom = 5;
                //_celSubTotal2.Border = 0;
                _celSubTotal22.BorderColorBottom = BaseColor.WHITE;
                _celSubTotal22.HorizontalAlignment = Element.ALIGN_RIGHT;
                _celSubTotal22.VerticalAlignment = Element.ALIGN_TOP;
                _tableTotales.AddCell(_celSubTotal22);



                iTextSharp.text.pdf.PdfPCell _celTextSubTotal2 = new iTextSharp.text.pdf.PdfPCell(new Phrase("RETENCIONES", fontTitleFactura));
                _celTextSubTotal2.Colspan = 1;
                _celTextSubTotal2.Padding = 3;
                _celTextSubTotal2.PaddingBottom = 5;
                //_celTextSubTotal2.Border = 0;
                _celTextSubTotal2.BorderWidthRight = 1;
                _celTextSubTotal2.BorderColorBottom = BaseColor.WHITE;
                _celTextSubTotal2.HorizontalAlignment = Element.ALIGN_LEFT;
                _celTextSubTotal2.VerticalAlignment = Element.ALIGN_TOP;
                _tableTotales.AddCell(_celTextSubTotal2);



                iTextSharp.text.pdf.PdfPCell _celSubTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:C2}",
                    decimal.Parse(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubTotal"].ToString())), fontTitle2));
                _celSubTotal.Colspan = 1;
                _celSubTotal.Padding = 3;
                _celSubTotal.PaddingBottom = 5;
                //_celSubTotal.Border = 0;
                _celSubTotal.BorderColorBottom = BaseColor.WHITE;
                _celSubTotal.HorizontalAlignment = Element.ALIGN_RIGHT;
                _celSubTotal.VerticalAlignment = Element.ALIGN_TOP;
                _tableTotales.AddCell(_celSubTotal);

                iTextSharp.text.pdf.PdfPCell _celTextIva = new iTextSharp.text.pdf.PdfPCell(new Phrase("IVA", fontTitleFactura));
                _celTextIva.Colspan = 1;
                _celTextIva.Padding = 3;
                _celTextIva.PaddingBottom = 5;
                //_celTextIva.Border = 0;
                _celTextIva.BorderWidthRight = 1;
                //_celTextIva.BorderColorBottom = BaseColor.BLACK;
                _celTextIva.HorizontalAlignment = Element.ALIGN_LEFT;
                _celTextIva.VerticalAlignment = Element.ALIGN_TOP;
                _tableTotales.AddCell(_celTextIva);


                iTextSharp.text.pdf.PdfPCell _celIva = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:C2}",
                    decimal.Parse(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DocWHTaxAmt"].ToString())), fontTitle2));
                _celIva.Colspan = 1;
                _celIva.Padding = 3;
                _celIva.PaddingBottom = 5;
                _celIva.Border = 0;
                //_celIva.BorderColorBottom = BaseColor.BLACK;
                _celIva.HorizontalAlignment = Element.ALIGN_RIGHT;
                _celIva.VerticalAlignment = Element.ALIGN_TOP;
                _tableTotales.AddCell(_celIva);

                iTextSharp.text.pdf.PdfPCell _celTextTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase("TOTAL", fontTitleFactura));
                _celTextTotal.Colspan = 1;
                _celTextTotal.Padding = 3;
                _celTextTotal.PaddingBottom = 5;
                //_celTextTotal.Border = 0;
                _celTextTotal.BorderWidthRight = 1;
                _celTextTotal.BorderWidthTop = 1;
                _celTextTotal.HorizontalAlignment = Element.ALIGN_LEFT;
                _celTextTotal.VerticalAlignment = Element.ALIGN_TOP;
                _tableTotales.AddCell(_celTextTotal);

                iTextSharp.text.pdf.PdfPCell _celTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:C2}",
                    decimal.Parse(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"].ToString())), fontTitleFactura));
                _celTotal.Colspan = 1;
                _celTotal.Padding = 3;
                _celTotal.PaddingBottom = 5;
                //_celTotal.Border = 0;
                _celTotal.BorderWidthTop = 1;
                _celTotal.HorizontalAlignment = Element.ALIGN_RIGHT;
                _celTotal.VerticalAlignment = Element.ALIGN_TOP;
                _tableTotales.AddCell(_celTotal);

                iTextSharp.text.pdf.PdfPCell _celTotales = new iTextSharp.text.pdf.PdfPCell(_tableTotales);
                tableTotalesObs.AddCell(_celTotales);

                //------------------------------------------------------------------------
                PdfPTable tableResolucion = new PdfPTable(1);
                tableResolucion.WidthPercentage = 100;
                string strResolucion = "";
                iTextSharp.text.pdf.PdfPCell _celResolucion = new iTextSharp.text.pdf.PdfPCell(new Phrase(strResolucion, fontTitleFactura));
                _celResolucion.Colspan = 1;
                _celResolucion.Padding = 3;
                //_celResolucion.Border = 0;
                _celResolucion.HorizontalAlignment = Element.ALIGN_JUSTIFIED_ALL;
                _celResolucion.VerticalAlignment = Element.ALIGN_TOP;
                tableResolucion.AddCell(_celResolucion);

                //----------------------------------------------------------------------
                PdfPTable tableFirmas = new PdfPTable(5);

                float[] DimencionFirmas = new float[5];
                DimencionFirmas[0] = 1.0F;//
                DimencionFirmas[1] = 0.02F;//
                DimencionFirmas[2] = 1.0F;//
                DimencionFirmas[3] = 0.02F;//
                DimencionFirmas[4] = 1.4F;//

                tableFirmas.WidthPercentage = 100;
                tableFirmas.SetWidths(DimencionFirmas);

                PdfPTable tableDespacahdoPor = new PdfPTable(1);
                tableDespacahdoPor.WidthPercentage = 100;
                iTextSharp.text.pdf.PdfPCell celDespachadoPor = new iTextSharp.text.pdf.PdfPCell(new Phrase("______________________________________" +
                                                                                                            "Sede Guayabal; " +
                                                                                                            "Carrera 52 # 7 Sur – 124 " +
                                                                                                            "444 45 54 " +
                                                                                                            " Medellin ", fontTitleFactura))
                {
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    VerticalAlignment = Element.ALIGN_BOTTOM,

                };
                celDespachadoPor.Colspan = 1;
                celDespachadoPor.Padding = 3;
                celDespachadoPor.Border = 0;
                celDespachadoPor.HorizontalAlignment = Element.ALIGN_CENTER;
                //celDespachadoPor.VerticalAlignment = Element.ALIGN_TOP;
                tableDespacahdoPor.AddCell(celDespachadoPor);

                iTextSharp.text.pdf.PdfPCell _celDespachadoPor = new iTextSharp.text.pdf.PdfPCell(tableDespacahdoPor);
                tableFirmas.AddCell(_celDespachadoPor);
                //----------------------------------------------------------------------------------------------------------------------------

                tableFirmas.AddCell(celEspacio2);

                //------------------------------------------------------------------------------------------

                PdfPTable tableFirmaConductor = new PdfPTable(1);
                tableFirmaConductor.WidthPercentage = 100;
                iTextSharp.text.pdf.PdfPCell celFirmaConductor = new iTextSharp.text.pdf.PdfPCell(new Phrase("______________________________________" +
                                                                                                            "Sede Palace; " +
                                                                                                            " Carrera 50 # 41 – 97 " +
                                                                                                            " 444 45 54 " +
                                                                                                            "Medellin ", fontTitleFactura))
                {
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    VerticalAlignment = Element.ALIGN_BOTTOM,
                };
                celFirmaConductor.Colspan = 1;
                celFirmaConductor.Padding = 3;
                celFirmaConductor.Border = 0;
                celFirmaConductor.HorizontalAlignment = Element.ALIGN_CENTER;
                //celFirmaConductor.VerticalAlignment = Element.ALIGN_TOP;
                tableFirmaConductor.AddCell(celFirmaConductor);

                iTextSharp.text.pdf.PdfPCell _celFirmaConductor = new iTextSharp.text.pdf.PdfPCell(tableFirmaConductor);
                tableFirmas.AddCell(_celFirmaConductor);

                //-------------------------------------------------------------------------------------
                tableFirmas.AddCell(celEspacio2);
                //-------------------------------------------------------------------------------------

                PdfPTable tableSelloCliente = new PdfPTable(1);
                //tableSelloCliente.WidthPercentage = 100;

                iTextSharp.text.pdf.PdfPCell cel1SelloCliente = new iTextSharp.text.pdf.PdfPCell(new Phrase("Apreciado cliente este documento " +
                    "no es endolsable", fontTitleFactura));
                cel1SelloCliente.Colspan = 1;
                cel1SelloCliente.Padding = 5;
                cel1SelloCliente.Border = 1;
                cel1SelloCliente.BorderWidthBottom = 1;
                cel1SelloCliente.HorizontalAlignment = Element.ALIGN_LEFT;
                cel1SelloCliente.VerticalAlignment = Element.ALIGN_TOP;
                tableSelloCliente.AddCell(cel1SelloCliente);

                iTextSharp.text.pdf.PdfPCell cel2SelloFechaRecibido = new iTextSharp.text.pdf.PdfPCell(new Phrase("Fecha de recibido:", fontTitleFactura));

                cel2SelloFechaRecibido.Colspan = 1;
                cel2SelloFechaRecibido.Padding = 3;
                cel2SelloFechaRecibido.Border = 0;
                cel2SelloFechaRecibido.HorizontalAlignment = Element.ALIGN_LEFT;
                cel2SelloFechaRecibido.VerticalAlignment = Element.ALIGN_TOP;
                tableSelloCliente.AddCell(cel2SelloFechaRecibido);

                iTextSharp.text.pdf.PdfPCell cel2SelloNombre = new iTextSharp.text.pdf.PdfPCell(new Phrase("Nombre:", fontTitleFactura));

                cel2SelloNombre.Colspan = 1;
                cel2SelloNombre.Padding = 3;
                cel2SelloNombre.Border = 0;
                cel2SelloNombre.HorizontalAlignment = Element.ALIGN_LEFT;
                cel2SelloNombre.VerticalAlignment = Element.ALIGN_TOP;
                tableSelloCliente.AddCell(cel2SelloNombre);

                iTextSharp.text.pdf.PdfPCell cel2SelloId = new iTextSharp.text.pdf.PdfPCell(new Phrase("Identificacion:", fontTitleFactura));

                cel2SelloId.Colspan = 1;
                cel2SelloId.Padding = 3;
                cel2SelloId.Border = 0;
                cel2SelloId.HorizontalAlignment = Element.ALIGN_LEFT;
                cel2SelloId.VerticalAlignment = Element.ALIGN_TOP;
                tableSelloCliente.AddCell(cel2SelloId);

                iTextSharp.text.pdf.PdfPCell cel2SelloFirma = new iTextSharp.text.pdf.PdfPCell(new Phrase("Firma:\n", fontTitleFactura));

                cel2SelloFirma.Colspan = 1;
                cel2SelloFirma.Padding = 3;
                cel2SelloFirma.Border = 0;
                cel2SelloFirma.HorizontalAlignment = Element.ALIGN_LEFT;
                cel2SelloFirma.VerticalAlignment = Element.ALIGN_TOP;
                tableSelloCliente.AddCell(cel2SelloFirma);

                iTextSharp.text.pdf.PdfPCell cel3SelloCliente = new iTextSharp.text.pdf.PdfPCell(new Phrase("______________________________________________________\n\n" +
                    "RECIBIDO CONFORME FIRMA Y SELLO DEL CLIENTE", fontTitleFactura));

                cel3SelloCliente.Colspan = 1;
                cel3SelloCliente.Padding = 3;
                cel3SelloCliente.Border = 0;
                cel3SelloCliente.HorizontalAlignment = Element.ALIGN_CENTER;
                cel3SelloCliente.VerticalAlignment = Element.ALIGN_TOP;
                tableSelloCliente.AddCell(cel3SelloCliente);

                iTextSharp.text.pdf.PdfPCell _celSelloCliente = new iTextSharp.text.pdf.PdfPCell(tableSelloCliente);
                //_celSelloCliente.Border = 0;
                //_celSelloCliente.BorderColor = BaseColor.WHITE;
                tableFirmas.AddCell(_celSelloCliente);

                PdfPTable origin = new PdfPTable(1);
                origin.WidthPercentage = 100;
                PdfPCell origuinal = new PdfPCell(new Phrase("")) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER };
                origin.AddCell(origuinal);
                #endregion

                #region Exit

                document.Add(Header);
                document.Add(divEspacio);
                document.Add(tableFacturar);
                document.Add(divEspacio);
                document.Add(tableDetalles);
                document.Add(tableDetalles2);
                document.Add(divEspacio2);
                document.Add(tableTituloUnidades);
                document.Add(tableUnidades);
                document.Add(divEspacio2);
                document.Add(divEspacio2);
                document.Add(tableTotalesObs);
                document.Add(divEspacio2);
                document.Add(divEspacio2);
                document.Add(tableResolucion);
                document.Add(divEspacio2);
                document.Add(divEspacio2);
                document.Add(tableFirmas);
                document.Add(divEspacio2);
                document.Add(origin);

                //document.Add(divTextBancoCheque);
                /*PIE DE PAGINA*/
                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();
                RutaPdf = NomArchivo;
                return true;
                #endregion
            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.Message;
                return false;
            }
        }

        #endregion

    }

}
