﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//Referenciar y usar.
using System.Data;
using System.IO;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Windows.Forms;
using System.Web;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Net;
using System.Drawing;
using System.Globalization;

namespace RulesServicesDIAN2.Adquiriente
{
    public partial class pdfEstandarAR
    {
        #region FormatosFacturacion PROCAR

        public bool FacturaNacionalProcar(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {
            NomArchivo = string.Empty;
            string Ruta = HttpContext.Current.Server.MapPath("~/PDF/" + NIT + "/");
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            string RutaImg = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";

            string RutaImgLogoISO = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + "_ISO.png";

            NomArchivo = NombreInvoice + ".pdf";

            try
            {
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.
                Document document = new Document(iTextSharp.text.PageSize.LETTER);
                document.SetMargins(20f, 15f, 280f, 315f);
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;

                // step 3: we open the document     

                PdfDiv divEspacio = new PdfDiv();
                divEspacio.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio.Height = 10;
                divEspacio.Width = 130;

                PdfDiv divEspacio2 = new PdfDiv();
                divEspacio2.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio2.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio2.Height = 2;
                divEspacio2.Width = 130;

                iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle2 = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle2Bold = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.BOLD);
                iTextSharp.text.Font fontCustom = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);

                PdfPTable tableEncabezadoPag = new PdfPTable(1) { WidthPercentage = 100 };

                PdfPTable tableContEncabezadoPag = new PdfPTable(1) { WidthPercentage = 100, };

                #region Encabezado
                PdfPTable tableEncabezado = new PdfPTable(new float[] { 0.75f, 2.3f, 1, 1.5f, })
                {
                    WidthPercentage = 100,
                };

                System.Drawing.Image logo = null;
                var requestLogo = WebRequest.Create(RutaImg);

                using (var responseLogo = requestLogo.GetResponse())
                using (var streamLogo = responseLogo.GetResponseStream())
                {
                    logo = Bitmap.FromStream(streamLogo);
                }

                iTextSharp.text.Image LogoImg = iTextSharp.text.Image.GetInstance(logo, BaseColor.WHITE);

                PdfDiv divLogo = new PdfDiv()
                {
                    Float = iTextSharp.text.pdf.PdfDiv.FloatType.LEFT,
                    Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE,
                    BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID,
                    Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK,
                    Width = 60,
                    Height = 110,
                    //BackgroundColor = BaseColor.LIGHT_GRAY,
                };
                divLogo.BackgroundImage = LogoImg;

                PdfPCell celImgLogo = new PdfPCell()
                {
                    Border = PdfPCell.NO_BORDER,
                };
                celImgLogo.AddElement(divLogo);

                tableEncabezado.AddCell(celImgLogo); ;
                //---------------------------------------------------------------------------------------------------------

                Paragraph prgTitle = new Paragraph(DsInvoiceAR.Tables["Company"].Rows[0]["Name"].ToString(), fontTitle);

                Paragraph prgTitle2 = new Paragraph($"NIT. " +
                    $"{(string)DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"]}-" +
                    $"{CalcularDigitoVerificacion((string)DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"])}" +
                    $" {(string)DsInvoiceAR.Tables["Company"].Rows[0]["MapURL"]}\n" +
                    //$"{(string)DsInvoiceAR.Tables["Company"].Rows[0]["Address1"]}\n" +
                    //$"{(string)DsInvoiceAR.Tables["Company"].Rows[0]["City"]} " +
                    //$"{(string)DsInvoiceAR.Tables["Company"].Rows[0]["State"]} COLOMBIA\n" +
                    $"{(string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Character08"]}\n" +
                    $"{(string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Character10"]}\n" +
                    $"{(string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Character11"]}"
                    /*, DsInvoiceAR.Tables["Company"].Rows[0]["PhoneNum"].ToString()*/, fontTitle2);
                prgTitle.Alignment = Element.ALIGN_CENTER;
                prgTitle2.Alignment = Element.ALIGN_CENTER;

                //PdfDiv divAcercade = new PdfDiv() {
                //    Float = iTextSharp.text.pdf.PdfDiv.FloatType.LEFT,
                //    Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE,
                //    BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID,
                //    Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK,
                //    BackgroundColor = BaseColor.LIGHT_GRAY,
                //    PaddingRight = 10,
                //    Width = 140,
                //};

                PdfPCell celAcercade = new PdfPCell()
                {
                    Border = PdfPCell.NO_BORDER,
                    Padding = 0,
                };
                celAcercade.AddElement(prgTitle);
                celAcercade.AddElement(prgTitle2);

                tableEncabezado.AddCell(celAcercade);

                //-----------------------------------------------------------------------------------------------------
                PdfDiv divQR = new PdfDiv();
                divQR.Float = iTextSharp.text.pdf.PdfDiv.FloatType.LEFT;
                divQR.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divQR.BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID;
                divQR.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                divQR.Height = 80;
                divQR.Width = 80;
                //divQR.PaddingRight = 30;
                //divQR.BackgroundColor = BaseColor.LIGHT_GRAY;
                iTextSharp.text.Image ImgQR = iTextSharp.text.Image.GetInstance(QRInvoice, BaseColor.WHITE);
                //ImgQR.Alignment = Element.ALIGN_RIGHT;
                divQR.BackgroundImage = ImgQR;

                PdfPCell celQR = new PdfPCell()
                {
                    Border = PdfPCell.NO_BORDER,
                };

                celQR.AddElement(divQR);
                tableEncabezado.AddCell(celQR);
                //----------------------------------------------------------------------------------------------------
                iTextSharp.text.Font fontTitleFactura = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7.3f, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitleFacturaNormal = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);

                PdfPTable tableFactura = new PdfPTable(3);

                //Dimenciones.
                float[] DimencionFactura = new float[3];
                DimencionFactura[0] = 1.0F;//
                DimencionFactura[1] = 4.0F;//
                DimencionFactura[2] = 0.5F;//

                tableFactura.WidthPercentage = 100;
                tableFactura.SetWidths(DimencionFactura);

                string textTipoFactura = "FACTURA DE VENTA";
                if (InvoiceType == "InvoiceType")
                    textTipoFactura = "FACTURA DE VENTA";
                else if (InvoiceType == "CreditNoteType")
                    textTipoFactura = "NOTA CREDITO";
                else if (InvoiceType == "DebitNoteType")
                    textTipoFactura = "NOTA DEBITO";

                iTextSharp.text.pdf.PdfPCell celTittle = new iTextSharp.text.pdf.PdfPCell(new Phrase($"{textTipoFactura}\n\n", fontTitleFactura));
                celTittle.Colspan = 3;
                celTittle.Padding = 3;
                celTittle.HorizontalAlignment = Element.ALIGN_CENTER;
                celTittle.VerticalAlignment = Element.ALIGN_TOP;
                celTittle.Border = 0;
                celTittle.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableFactura.AddCell(celTittle);

                iTextSharp.text.pdf.PdfPCell celNo = new iTextSharp.text.pdf.PdfPCell(new Phrase("No. ", fontTitleFactura));
                celNo.Colspan = 1;
                celNo.Padding = 5;
                celNo.HorizontalAlignment = Element.ALIGN_CENTER;
                celNo.VerticalAlignment = Element.ALIGN_TOP;
                celNo.Border = 0;
                celNo.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableFactura.AddCell(celNo);

                string NumLegalFactura = DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"].ToString(); ;
                //if (InvoiceType == "InvoiceType")
                //    NumLegalFactura = DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiseRef"].ToString();
                //else if (InvoiceType == "CreditNoteType")
                //    NumLegalFactura = DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"].ToString();
                //else
                //    NumLegalFactura = DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"].ToString();

                iTextSharp.text.pdf.PdfPCell celNoFactura = new iTextSharp.text.pdf.PdfPCell(new Phrase(NumLegalFactura, fontTitleFactura));
                celNoFactura.Colspan = 1;
                celNoFactura.Padding = 5;
                celNoFactura.HorizontalAlignment = Element.ALIGN_LEFT;
                celNoFactura.VerticalAlignment = Element.ALIGN_TOP;
                celNoFactura.Border = 0;
                celNoFactura.BackgroundColor = BaseColor.WHITE;
                tableFactura.AddCell(celNoFactura);


                iTextSharp.text.pdf.PdfPCell celEspacioNoFactura = new iTextSharp.text.pdf.PdfPCell(new Phrase(" ", fontTitleFactura));
                celEspacioNoFactura.Colspan = 1;
                //celNo.Padding = 3;
                celEspacioNoFactura.HorizontalAlignment = Element.ALIGN_CENTER;
                celEspacioNoFactura.VerticalAlignment = Element.ALIGN_TOP;
                celEspacioNoFactura.Border = 0;
                celEspacioNoFactura.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableFactura.AddCell(celEspacioNoFactura);

                iTextSharp.text.pdf.PdfPCell celRellenoNoFactura = new iTextSharp.text.pdf.PdfPCell(new Phrase(" ", fontTitleFactura));
                celRellenoNoFactura.Colspan = 3;
                celRellenoNoFactura.Padding = 3;
                celRellenoNoFactura.BackgroundColor = BaseColor.LIGHT_GRAY;
                celRellenoNoFactura.HorizontalAlignment = Element.ALIGN_RIGHT;
                celRellenoNoFactura.VerticalAlignment = Element.ALIGN_TOP;
                celRellenoNoFactura.Border = 0;
                tableFactura.AddCell(celRellenoNoFactura);

                iTextSharp.text.pdf.PdfPCell celConsecutivoInterno = new iTextSharp.text.pdf.PdfPCell(
                    new Phrase((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceNum"], fontCustom));
                celConsecutivoInterno.Colspan = 4;
                //celConsecutivoInterno.Padding = 3;
                celConsecutivoInterno.BackgroundColor = BaseColor.WHITE;
                celConsecutivoInterno.HorizontalAlignment = Element.ALIGN_RIGHT;
                celConsecutivoInterno.VerticalAlignment = Element.ALIGN_TOP;
                celConsecutivoInterno.Border = 0;
                tableFactura.AddCell(celConsecutivoInterno);

                string autorizaFE =
                    (string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Character09"] == "2" ? "X" : string.Empty;

                var prgAutorizaFE = new Phrase();
                Chunk textImpr = new Chunk("Impr. ", fontCustom);
                Chunk chunk = new Chunk(autorizaFE, fontTitle);

                prgAutorizaFE.Add(textImpr);
                prgAutorizaFE.Add(chunk);

                tableFactura.AddCell(new PdfPCell(prgAutorizaFE)
                {
                    Colspan = 3,
                    HorizontalAlignment = Element.ALIGN_RIGHT,
                    VerticalAlignment = Element.ALIGN_CENTER,
                    Border = PdfPCell.NO_BORDER,
                });

                //PdfDiv divNumeroFactura = new PdfDiv();
                //divNumeroFactura.Float = iTextSharp.text.pdf.PdfDiv.FloatType.RIGHT;
                //divNumeroFactura.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                //divNumeroFactura.BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID;
                //divNumeroFactura.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                ////divNumeroFactura.BackgroundColor = BaseColor.LIGHT_GRAY;
                //divNumeroFactura.PaddingLeft = 5;
                //divNumeroFactura.Width = 140;
                //divNumeroFactura.AddElement(tableFactura);

                tableEncabezado.AddCell(new PdfPCell(tableFactura)
                {
                    Border = PdfPCell.NO_BORDER,
                });

                #endregion

                tableEncabezadoPag.AddCell(new PdfPCell(tableEncabezado)
                {
                    Border = PdfPCell.NO_BORDER,
                    Padding = 0,
                });

                #region FACTURAR Y DESPACHAR A
                //------------------------------------------------------------------------------------------------
                PdfPTable tableFacturar = new PdfPTable(3);
                //Dimenciones.
                float[] DimencionFacturar = new float[3];
                DimencionFacturar[0] = 1.0F;//
                DimencionFacturar[1] = 0.01F;//
                DimencionFacturar[2] = 1.0F;//

                tableFacturar.WidthPercentage = 100;
                tableFacturar.SetWidths(DimencionFacturar);

                tableFacturar.AddCell(new PdfPCell(new Phrase($"CUFE: {CUFE}", fontTitle2))
                {
                    Border = PdfPCell.NO_BORDER,
                    Colspan = 2,
                });

                tableFacturar.AddCell(new PdfPCell(new Phrase("Representación gráfica factura electrónica", fontTitle2))
                {
                    Border = PdfPCell.NO_BORDER,
                    Colspan = 2,
                });
                //----------------------------------------------------------------------------------------------
                PdfPTable tableFacturarA = new PdfPTable(2);
                float[] DimencionFacturarA = new float[2];
                DimencionFacturarA[0] = 0.47F;//
                DimencionFacturarA[1] = 2.0F;//

                tableFacturarA.WidthPercentage = 100;
                tableFacturarA.SetWidths(DimencionFacturarA);

                iTextSharp.text.pdf.PdfPCell celDatosFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase("FACTURAR A:\n", fontTitleFactura));
                celDatosFacturarA.Colspan = 2;
                celDatosFacturarA.Padding = 3;
                celDatosFacturarA.Border = 0;
                celDatosFacturarA.BorderColorBottom = BaseColor.WHITE;
                celDatosFacturarA.HorizontalAlignment = Element.ALIGN_CENTER;
                celDatosFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celDatosFacturarA);

                iTextSharp.text.pdf.PdfPCell celTextClienteFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase("CLIENTE:", fontTitleFactura));
                celTextClienteFacturarA.Colspan = 1;
                celTextClienteFacturarA.Padding = 3;
                celTextClienteFacturarA.Border = 0;
                celTextClienteFacturarA.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextClienteFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celTextClienteFacturarA);

                iTextSharp.text.pdf.PdfPCell celClienteFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase((string)DsInvoiceAR.Tables["Customer"].Rows[0]["Name"],
                    fontTitle2));
                celClienteFacturarA.Colspan = 1;
                celClienteFacturarA.Padding = 3;
                celClienteFacturarA.Border = 0;
                celClienteFacturarA.HorizontalAlignment = Element.ALIGN_LEFT;
                celClienteFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celClienteFacturarA);

                iTextSharp.text.pdf.PdfPCell celTextNitFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase("NIT:", fontTitleFactura));
                celTextNitFacturarA.Colspan = 1;
                celTextNitFacturarA.Padding = 3;
                celTextNitFacturarA.Border = 0;
                celTextNitFacturarA.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextNitFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celTextNitFacturarA);

                Phrase prgInfoNitZona = new Phrase();

                prgInfoNitZona.Add(new Chunk((string)DsInvoiceAR.Tables["Customer"].Rows[0]["ResaleID"],
                    fontTitle2));

                prgInfoNitZona.Add(new Chunk("      Zona: ",
                    FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.BOLD)));

                prgInfoNitZona.Add(new Chunk((string)DsInvoiceAR.Tables["Customer"].Rows[0]["TerritoryID"],
                    FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL)));

                prgInfoNitZona.Add(new Chunk("      MAYORISTA: ",
                    FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.BOLD)));

                prgInfoNitZona.Add(new Chunk($"{(string)DsInvoiceAR.Tables["Customer"].Rows[0]["Character01"]}",
                    FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL)));


                iTextSharp.text.pdf.PdfPCell celNitFacturarA = new iTextSharp.text.pdf.PdfPCell(prgInfoNitZona);
                celNitFacturarA.Colspan = 1;
                celNitFacturarA.Padding = 3;
                celNitFacturarA.Border = 0;
                celNitFacturarA.HorizontalAlignment = Element.ALIGN_LEFT;
                celNitFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celNitFacturarA);

                iTextSharp.text.pdf.PdfPCell celTextDireccionFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase("DIRECCION:", fontTitleFactura));
                celTextDireccionFacturarA.Colspan = 1;
                celTextDireccionFacturarA.Padding = 3;
                celTextDireccionFacturarA.Border = 0;
                celTextDireccionFacturarA.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextDireccionFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                tableFacturarA.AddCell(celTextDireccionFacturarA);

                //PdfPTable tableValDir_Mayorista = new PdfPTable(new float[] { 2,1.5f }) { WidthPercentage=100, };

                iTextSharp.text.pdf.PdfPCell celDireccionFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    $"{((string)DsInvoiceAR.Tables["Customer"].Rows[0]["Address1"]).TrimEnd(' ')} {(string)DsInvoiceAR.Tables["Customer"].Rows[0]["City"]}", fontTitle2));
                celDireccionFacturarA.Colspan = 2;
                //celDireccionFacturarA.Padding = 3;
                celDireccionFacturarA.Border = 0;
                celDireccionFacturarA.HorizontalAlignment = Element.ALIGN_LEFT;
                celDireccionFacturarA.VerticalAlignment = Element.ALIGN_BOTTOM;
                tableFacturarA.AddCell(celDireccionFacturarA);

                if (InvoiceType == "InvoiceType")
                {
                    iTextSharp.text.pdf.PdfPCell celTextTelFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase("TELEFONO:", fontTitleFactura))
                    {
                        Colspan = 1,
                        Padding = 3,
                        Border = 0,
                        HorizontalAlignment = Element.ALIGN_LEFT,
                        VerticalAlignment = Element.ALIGN_TOP,
                    };
                    tableFacturarA.AddCell(celTextTelFacturarA);

                    iTextSharp.text.pdf.PdfPCell celTelFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    (string)DsInvoiceAR.Tables["Customer"].Rows[0]["PhoneNum"], fontTitle2))
                    {
                        Colspan = 1,
                        Padding = 3,
                        Border = 0,
                        HorizontalAlignment = Element.ALIGN_LEFT,
                        VerticalAlignment = Element.ALIGN_TOP,
                    };
                    tableFacturarA.AddCell(celTelFacturarA);
                }
                else if (InvoiceType == "CreditNoteType" || InvoiceType == "DebitNoteType")
                {
                    iTextSharp.text.pdf.PdfPCell celTextTelFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase("CIUDAD:", fontTitleFactura))
                    {
                        Colspan = 1,
                        Padding = 3,
                        Border = 0,
                        HorizontalAlignment = Element.ALIGN_LEFT,
                        VerticalAlignment = Element.ALIGN_TOP,
                    };
                    tableFacturarA.AddCell(celTextTelFacturarA);

                    iTextSharp.text.pdf.PdfPCell celTelFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    (string)DsInvoiceAR.Tables["Customer"].Rows[0]["City"], fontTitle2))
                    {
                        Colspan = 1,
                        Padding = 3,
                        Border = 0,
                        HorizontalAlignment = Element.ALIGN_LEFT,
                        VerticalAlignment = Element.ALIGN_TOP,
                    };
                    tableFacturarA.AddCell(celTelFacturarA);
                }

                iTextSharp.text.pdf.PdfPCell celTittleFacturarA = new iTextSharp.text.pdf.PdfPCell(tableFacturarA);
                tableFacturar.AddCell(celTittleFacturarA);
                //----------------------------------------------------------------------------------------------------------------------------

                iTextSharp.text.pdf.PdfPCell celEspacio2 = new iTextSharp.text.pdf.PdfPCell(new Phrase(" ", fontTitleFactura));
                celEspacio2.Border = 0;
                tableFacturar.AddCell(celEspacio2);

                //------------------------------------------------------------------------------------------

                PdfPTable tableDespacharA = new PdfPTable(2);
                float[] DimencionDespacharA = new float[2];
                DimencionDespacharA[0] = 0.8F;//
                DimencionDespacharA[1] = 2.0F;//

                tableDespacharA.WidthPercentage = 100;
                tableDespacharA.SetWidths(DimencionDespacharA);

                if (InvoiceType == "InvoiceType")
                {
                    iTextSharp.text.pdf.PdfPCell celTittleDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase("DESPACHAR A:\n", fontTitleFactura))
                    {
                        Colspan = 2,
                        Padding = 3,
                        Border = 0,
                        BorderColorBottom = BaseColor.WHITE,
                        HorizontalAlignment = Element.ALIGN_CENTER,
                        VerticalAlignment = Element.ALIGN_TOP,

                    };
                    tableDespacharA.AddCell(celTittleDespacharA);

                    iTextSharp.text.pdf.PdfPCell celTextClienteDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase("AUTORIZADO:", fontTitleFactura))
                    {
                        Colspan = 1,
                        Padding = 3,
                        Border = 0,
                        HorizontalAlignment = Element.ALIGN_LEFT,
                        VerticalAlignment = Element.ALIGN_TOP,
                    };
                    tableDespacharA.AddCell(celTextClienteDespacharA);

                    iTextSharp.text.pdf.PdfPCell celClienteDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                        $"{(string)DsInvoiceAR.Tables["Customer"].Rows[0]["Character02"]}", fontTitle2))
                    {
                        Colspan = 1,
                        Padding = 3,
                        Border = 0,
                        HorizontalAlignment = Element.ALIGN_LEFT,
                        VerticalAlignment = Element.ALIGN_TOP,
                    };
                    tableDespacharA.AddCell(celClienteDespacharA);

                    iTextSharp.text.pdf.PdfPCell celTextNitDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase(" ", fontTitleFactura))
                    {
                        Colspan = 1,
                        Padding = 3,
                        Border = 0,
                        HorizontalAlignment = Element.ALIGN_LEFT,
                        VerticalAlignment = Element.ALIGN_TOP,
                    };
                    tableDespacharA.AddCell(celTextNitDespacharA);

                    iTextSharp.text.pdf.PdfPCell celNitDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                        /*(string)DsInvoiceAR.Tables["Customer"].Rows[0]["ResaleID"]*/" ", fontTitle2))
                    {
                        Colspan = 1,
                        Padding = 3,
                        Border = 0,
                        HorizontalAlignment = Element.ALIGN_LEFT,
                        VerticalAlignment = Element.ALIGN_TOP,
                    };
                    tableDespacharA.AddCell(celNitDespacharA);

                    iTextSharp.text.pdf.PdfPCell celTextDireccionDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase("DIR. ENVIO:", fontTitleFactura))
                    {
                        Colspan = 1,
                        Padding = 3,
                        Border = 0,
                        HorizontalAlignment = Element.ALIGN_LEFT,
                        VerticalAlignment = Element.ALIGN_TOP,
                    };
                    tableDespacharA.AddCell(celTextDireccionDespacharA);

                    iTextSharp.text.pdf.PdfPCell celDireccionDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                        $"{(string)DsInvoiceAR.Tables["Customer"].Rows[0]["ShipToAddress"]}", fontTitle2))
                    {
                        Colspan = 1,
                        Padding = 3,
                        Border = 0,
                        HorizontalAlignment = Element.ALIGN_LEFT,
                        VerticalAlignment = Element.ALIGN_TOP,
                    };
                    tableDespacharA.AddCell(celDireccionDespacharA);

                    iTextSharp.text.pdf.PdfPCell celTextTelDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase("TELEFONO:", fontTitleFactura))
                    {
                        Colspan = 1,
                        Padding = 3,
                        Border = 0,
                        HorizontalAlignment = Element.ALIGN_LEFT,
                        VerticalAlignment = Element.ALIGN_TOP,
                    };
                    tableDespacharA.AddCell(celTextTelDespacharA);

                    iTextSharp.text.pdf.PdfPCell celTelDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                        (string)DsInvoiceAR.Tables["Customer"].Rows[0]["PhoneNum"], fontTitle2))
                    {
                        Colspan = 1,
                        Padding = 3,
                        Border = 0,
                        HorizontalAlignment = Element.ALIGN_LEFT,
                        VerticalAlignment = Element.ALIGN_TOP,
                    };
                    tableDespacharA.AddCell(celTelDespacharA);
                }
                else if (InvoiceType == "CreditNoteType" || InvoiceType == "DebitNoteType")
                {
                    iTextSharp.text.pdf.PdfPCell celTittleDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase(" \n", fontTitleFactura))
                    {
                        Colspan = 2,
                        Padding = 3,
                        Border = 0,
                        BorderColorBottom = BaseColor.WHITE,
                        HorizontalAlignment = Element.ALIGN_CENTER,
                        VerticalAlignment = Element.ALIGN_TOP,

                    };
                    tableDespacharA.AddCell(celTittleDespacharA);

                    iTextSharp.text.pdf.PdfPCell celTextClienteDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase(" ", fontTitleFactura))
                    {
                        Colspan = 1,
                        Padding = 3,
                        Border = 0,
                        HorizontalAlignment = Element.ALIGN_LEFT,
                        VerticalAlignment = Element.ALIGN_TOP,
                    };
                    tableDespacharA.AddCell(celTextClienteDespacharA);

                    iTextSharp.text.pdf.PdfPCell celClienteDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                        $"{""/*(string)DsInvoiceAR.Tables["Customer"].Rows[0]["Name"]*/}", fontTitle2))
                    {
                        Colspan = 1,
                        Padding = 3,
                        Border = 0,
                        HorizontalAlignment = Element.ALIGN_LEFT,
                        VerticalAlignment = Element.ALIGN_TOP,
                    };
                    tableDespacharA.AddCell(celClienteDespacharA);

                    iTextSharp.text.pdf.PdfPCell celTextNitDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase(" ", fontTitleFactura))
                    {
                        Colspan = 1,
                        Padding = 3,
                        Border = 0,
                        HorizontalAlignment = Element.ALIGN_LEFT,
                        VerticalAlignment = Element.ALIGN_TOP,
                    };
                    tableDespacharA.AddCell(celTextNitDespacharA);

                    iTextSharp.text.pdf.PdfPCell celNitDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                        /*(string)DsInvoiceAR.Tables["Customer"].Rows[0]["ResaleID"]*/" ", fontTitle2))
                    {
                        Colspan = 1,
                        Padding = 3,
                        Border = 0,
                        HorizontalAlignment = Element.ALIGN_LEFT,
                        VerticalAlignment = Element.ALIGN_TOP,
                    };
                    tableDespacharA.AddCell(celNitDespacharA);

                    iTextSharp.text.pdf.PdfPCell celTextDireccionDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase("", fontTitleFactura))
                    {
                        Colspan = 1,
                        Padding = 3,
                        Border = 0,
                        HorizontalAlignment = Element.ALIGN_LEFT,
                        VerticalAlignment = Element.ALIGN_TOP,
                    };
                    tableDespacharA.AddCell(celTextDireccionDespacharA);

                    iTextSharp.text.pdf.PdfPCell celDireccionDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                        $"{""/*(string)DsInvoiceAR.Tables["Customer"].Rows[0]["ShipToAddress"]*/}", fontTitle2))
                    {
                        Colspan = 1,
                        Padding = 3,
                        Border = 0,
                        HorizontalAlignment = Element.ALIGN_LEFT,
                        VerticalAlignment = Element.ALIGN_TOP,
                    };
                    tableDespacharA.AddCell(celDireccionDespacharA);

                    iTextSharp.text.pdf.PdfPCell celTextTelDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase("", fontTitleFactura))
                    {
                        Colspan = 1,
                        Padding = 3,
                        Border = 0,
                        HorizontalAlignment = Element.ALIGN_LEFT,
                        VerticalAlignment = Element.ALIGN_TOP,
                    };
                    tableDespacharA.AddCell(celTextTelDespacharA);

                    iTextSharp.text.pdf.PdfPCell celTelDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                        /*(string)DsInvoiceAR.Tables["Customer"].Rows[0]["PhoneNum"]*/"", fontTitle2))
                    {
                        Colspan = 1,
                        Padding = 3,
                        Border = 0,
                        HorizontalAlignment = Element.ALIGN_LEFT,
                        VerticalAlignment = Element.ALIGN_TOP,
                    };
                    tableDespacharA.AddCell(celTelDespacharA);
                }


                iTextSharp.text.pdf.PdfPCell celDatosDespacharA = new iTextSharp.text.pdf.PdfPCell(tableDespacharA);
                tableFacturar.AddCell(celDatosDespacharA);
                //-------------------------------------------------------------------------------------- 
                #endregion

                tableEncabezadoPag.AddCell(new PdfPCell(tableFacturar)
                {
                    Border = PdfPCell.NO_BORDER,
                    Padding = 0,
                    PaddingBottom = 5,
                });

                #region Tabla de informacion de Factura
                //------------------------------------------------------------------------------------------------
                //PdfPTable tableAutorizaFE = new PdfPTable(new float[] { 2f, 0.4f })
                //{
                //    WidthPercentage = 100,
                //};

                //tableAutorizaFE.AddCell(new PdfPCell(new Phrase("Impr.", fontTitle)));
                //if ((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Character09"] == "2")
                //    tableAutorizaFE.AddCell(new PdfPCell(new Phrase("X", fontTitle)));
                //else
                //    tableAutorizaFE.AddCell(new PdfPCell(new Phrase(" ", fontTitle)));

                PdfPTable tableInfoFactura = new PdfPTable(1) { WidthPercentage = 100, };

                PdfPTable tableDetalles = new PdfPTable(5);
                tableDetalles.PaddingTop = 20;
                //Dimenciones.
                float[] DimencionDetalles = new float[5];
                DimencionDetalles[0] = 1.2F;//
                DimencionDetalles[1] = 2.3F;//
                DimencionDetalles[2] = 1.0F;//
                DimencionDetalles[3] = 1.0F;//
                DimencionDetalles[4] = 0.8F;//

                tableDetalles.WidthPercentage = 100;
                tableDetalles.SetWidths(DimencionDetalles);

                iTextSharp.text.pdf.PdfPCell celOC_Cliente = new iTextSharp.text.pdf.PdfPCell(new Phrase("COD. VENDEDOR", fontTitleFactura));
                celOC_Cliente.Colspan = 1;
                celOC_Cliente.Padding = 3;
                //celVendedor.Border = 0;
                celOC_Cliente.BorderColorBottom = BaseColor.WHITE;
                celOC_Cliente.HorizontalAlignment = Element.ALIGN_CENTER;
                celOC_Cliente.VerticalAlignment = Element.ALIGN_TOP;
                tableDetalles.AddCell(celOC_Cliente);

                if (InvoiceType == "InvoiceType")
                {
                    PdfPTable tableFacturo_Placa = new PdfPTable(2) { WidthPercentage = 100, };

                    iTextSharp.text.pdf.PdfPCell celVendedor = new iTextSharp.text.pdf.PdfPCell(new Phrase("FACTURÓ", fontTitleFactura));
                    celVendedor.Colspan = 1;
                    celVendedor.Padding = 3;
                    //celVendedor.Border = 0;
                    celVendedor.BorderColorBottom = BaseColor.WHITE;
                    celVendedor.HorizontalAlignment = Element.ALIGN_CENTER;
                    celVendedor.VerticalAlignment = Element.ALIGN_TOP;
                    tableFacturo_Placa.AddCell(celVendedor);

                    iTextSharp.text.pdf.PdfPCell celPlaca = new iTextSharp.text.pdf.PdfPCell(new Phrase("PLACA VEHICULO", fontTitleFactura));
                    celPlaca.Colspan = 1;
                    celPlaca.Padding = 3;
                    //celPlaca.Border = 0;
                    celPlaca.BorderColorBottom = BaseColor.WHITE;
                    celPlaca.HorizontalAlignment = Element.ALIGN_CENTER;
                    celPlaca.VerticalAlignment = Element.ALIGN_TOP;
                    tableFacturo_Placa.AddCell(celPlaca);

                    tableDetalles.AddCell(new PdfPCell(tableFacturo_Placa) { Border = PdfPCell.NO_BORDER, });
                }
                else if (InvoiceType == "CreditNoteType" || InvoiceType == "DebitNoteType")
                {
                    iTextSharp.text.pdf.PdfPCell celVendedor = new iTextSharp.text.pdf.PdfPCell(new Phrase("APLICADO A FACTURAS", fontTitleFactura));
                    celVendedor.Colspan = 1;
                    celVendedor.Padding = 3;
                    //celVendedor.Border = 0;
                    celVendedor.BorderColorBottom = BaseColor.WHITE;
                    celVendedor.HorizontalAlignment = Element.ALIGN_CENTER;
                    celVendedor.VerticalAlignment = Element.ALIGN_TOP;
                    tableDetalles.AddCell(celVendedor);
                }

                if (InvoiceType == "InvoiceType")
                {
                    iTextSharp.text.pdf.PdfPCell celOrdenVenta = new iTextSharp.text.pdf.PdfPCell(new Phrase("PEDIDO", fontTitleFactura));
                    celOrdenVenta.Colspan = 1;
                    celOrdenVenta.Padding = 3;
                    //celVendedor.Border = 0;
                    celOrdenVenta.BorderColorBottom = BaseColor.WHITE;
                    celOrdenVenta.HorizontalAlignment = Element.ALIGN_CENTER;
                    celOrdenVenta.VerticalAlignment = Element.ALIGN_TOP;
                    tableDetalles.AddCell(celOrdenVenta);
                }
                else if (InvoiceType == "CreditNoteType" || InvoiceType == "DebitNoteType")
                {
                    iTextSharp.text.pdf.PdfPCell celOrdenVenta = new iTextSharp.text.pdf.PdfPCell(new Phrase("ELABORÓ", fontTitleFactura));
                    celOrdenVenta.Colspan = 1;
                    celOrdenVenta.Padding = 3;
                    //celVendedor.Border = 0;
                    celOrdenVenta.BorderColorBottom = BaseColor.WHITE;
                    celOrdenVenta.HorizontalAlignment = Element.ALIGN_CENTER;
                    celOrdenVenta.VerticalAlignment = Element.ALIGN_TOP;
                    tableDetalles.AddCell(celOrdenVenta);
                }

                iTextSharp.text.pdf.PdfPCell celTerminoPago = new iTextSharp.text.pdf.PdfPCell(new Phrase("TÉRMINO DE PAGO", fontTitleFactura));
                celTerminoPago.Colspan = 1;
                celTerminoPago.Padding = 3;
                //celVendedor.Border = 0;
                celTerminoPago.BorderColorBottom = BaseColor.WHITE;
                celTerminoPago.HorizontalAlignment = Element.ALIGN_CENTER;
                celTerminoPago.VerticalAlignment = Element.ALIGN_TOP;
                tableDetalles.AddCell(celTerminoPago);

                if (InvoiceType == "InvoiceType")
                {
                    iTextSharp.text.pdf.PdfPCell celRemision = new iTextSharp.text.pdf.PdfPCell(new Phrase("CUOTA(S)", fontTitleFactura));
                    celRemision.Colspan = 1;
                    celRemision.Padding = 3;
                    //celVendedor.Border = 0;
                    celRemision.BorderColorBottom = BaseColor.WHITE;
                    celRemision.HorizontalAlignment = Element.ALIGN_CENTER;
                    celRemision.VerticalAlignment = Element.ALIGN_TOP;
                    tableDetalles.AddCell(celRemision);
                }
                else if (InvoiceType == "CreditNoteType" || InvoiceType == "DebitNoteType")
                {
                    iTextSharp.text.pdf.PdfPCell celRemision = new iTextSharp.text.pdf.PdfPCell(
                        new Phrase("DCTO TOTAL", fontTitleFactura));
                    celRemision.Colspan = 1;
                    celRemision.Padding = 3;
                    //celVendedor.Border = 0;
                    celRemision.BorderColorBottom = BaseColor.WHITE;
                    celRemision.HorizontalAlignment = Element.ALIGN_CENTER;
                    celRemision.VerticalAlignment = Element.ALIGN_TOP;
                    tableDetalles.AddCell(celRemision);
                }
                //--------------------VALORES----------------------------------------------------------------
                iTextSharp.text.pdf.PdfPCell celDataOC_Cliente = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    (string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["SalesRepCode"], fontCustom));
                celDataOC_Cliente.Colspan = 1;
                celDataOC_Cliente.Padding = 3;
                //celVendedor.Border = 0;
                celDataOC_Cliente.BorderColorBottom = BaseColor.WHITE;
                celDataOC_Cliente.HorizontalAlignment = Element.ALIGN_CENTER;
                celDataOC_Cliente.VerticalAlignment = Element.ALIGN_TOP;
                tableDetalles.AddCell(celDataOC_Cliente);

                if (InvoiceType == "InvoiceType")
                {
                    var tableValFacturo_Placa = new PdfPTable(2) { WidthPercentage = 100, };

                    iTextSharp.text.pdf.PdfPCell celDataVendedor = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                        (string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["EntryPerson"], fontCustom));
                    celDataVendedor.Colspan = 1;
                    celDataVendedor.Padding = 3;
                    //celVendedor.Border = 0;
                    celDataVendedor.BorderColorBottom = BaseColor.WHITE;
                    celDataVendedor.HorizontalAlignment = Element.ALIGN_CENTER;
                    celDataVendedor.VerticalAlignment = Element.ALIGN_TOP;
                    tableValFacturo_Placa.AddCell(celDataVendedor);

                    iTextSharp.text.pdf.PdfPCell celDataPlaca = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    (string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Character01"], fontCustom));
                    celDataPlaca.Colspan = 1;
                    celDataPlaca.Padding = 3;
                    //celVendedor.Border = 0;
                    celDataPlaca.BorderColorBottom = BaseColor.WHITE;
                    celDataPlaca.HorizontalAlignment = Element.ALIGN_CENTER;
                    celDataPlaca.VerticalAlignment = Element.ALIGN_TOP;
                    tableValFacturo_Placa.AddCell(celDataPlaca);

                    tableDetalles.AddCell(new PdfPCell(tableValFacturo_Placa) { Border = PdfPCell.NO_BORDER, });
                }
                else if (InvoiceType == "CreditNoteType" || InvoiceType == "DebitNoteType")
                {
                    iTextSharp.text.pdf.PdfPCell celDataVendedor = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    (string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceRef"], fontCustom));
                    celDataVendedor.Colspan = 1;
                    celDataVendedor.Padding = 3;
                    //celVendedor.Border = 0;
                    celDataVendedor.BorderColorBottom = BaseColor.WHITE;
                    celDataVendedor.HorizontalAlignment = Element.ALIGN_CENTER;
                    celDataVendedor.VerticalAlignment = Element.ALIGN_TOP;
                    tableDetalles.AddCell(celDataVendedor);
                }

                if (InvoiceType == "InvoiceType")
                {
                    iTextSharp.text.pdf.PdfPCell celDataOrdenVenta = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                        (string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["OrderNum"],
                        fontCustom));
                    celDataOrdenVenta.Colspan = 1;
                    celDataOrdenVenta.Padding = 3;
                    //celVendedor.Border = 0;
                    celDataOrdenVenta.BorderColorBottom = BaseColor.WHITE;
                    celDataOrdenVenta.HorizontalAlignment = Element.ALIGN_CENTER;
                    celDataOrdenVenta.VerticalAlignment = Element.ALIGN_TOP;
                    tableDetalles.AddCell(celDataOrdenVenta);
                }
                else if (InvoiceType == "CreditNoteType" || InvoiceType == "DebitNoteType")
                {
                    iTextSharp.text.pdf.PdfPCell celDataOrdenVenta = new iTextSharp.text.pdf.PdfPCell(
                        new Phrase((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["EntryPerson"], fontCustom));
                    celDataOrdenVenta.Colspan = 1;
                    celDataOrdenVenta.Padding = 3;
                    //celVendedor.Border = 0;
                    celDataOrdenVenta.BorderColorBottom = BaseColor.WHITE;
                    celDataOrdenVenta.HorizontalAlignment = Element.ALIGN_CENTER;
                    celDataOrdenVenta.VerticalAlignment = Element.ALIGN_TOP;
                    tableDetalles.AddCell(celDataOrdenVenta);
                }

                iTextSharp.text.pdf.PdfPCell celDataTerminoPago = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    (string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["TermsCodeDescription"], fontCustom));
                celDataTerminoPago.Colspan = 1;
                celDataTerminoPago.Padding = 3;
                //celVendedor.Border = 0;
                celDataTerminoPago.BorderColorBottom = BaseColor.WHITE;
                celDataTerminoPago.HorizontalAlignment = Element.ALIGN_CENTER;
                celDataTerminoPago.VerticalAlignment = Element.ALIGN_TOP;
                tableDetalles.AddCell(celDataTerminoPago);

                if (InvoiceType == "InvoiceType")
                {
                    iTextSharp.text.pdf.PdfPCell celDataRemision = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                        $"{Decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number01"]).ToString("N0")} de " +
                        $"{Decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number02"]).ToString("N2")}", fontCustom));
                    celDataRemision.Colspan = 1;
                    celDataRemision.Padding = 3;
                    //celVendedor.Border = 0;
                    celDataRemision.BorderColorBottom = BaseColor.WHITE;
                    celDataRemision.HorizontalAlignment = Element.ALIGN_CENTER;
                    celDataRemision.VerticalAlignment = Element.ALIGN_TOP;
                    tableDetalles.AddCell(celDataRemision);
                }
                else if (InvoiceType == "CreditNoteType" || InvoiceType == "DebitNoteType")
                {
                    iTextSharp.text.pdf.PdfPCell celDataRemision = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    Decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DocDiscount"]).ToString("N2"), fontCustom));
                    celDataRemision.Colspan = 1;
                    celDataRemision.Padding = 3;
                    //celVendedor.Border = 0;
                    celDataRemision.BorderColorBottom = BaseColor.WHITE;
                    celDataRemision.HorizontalAlignment = Element.ALIGN_CENTER;
                    celDataRemision.VerticalAlignment = Element.ALIGN_TOP;
                    tableDetalles.AddCell(celDataRemision);
                }

                tableInfoFactura.AddCell(new PdfPCell(tableDetalles) { Border = PdfPCell.NO_BORDER, Padding = 0, });
                //-------------------------------------------------------------------

                PdfPTable tableDetalles2 = new PdfPTable(3);
                tableDetalles2.WidthPercentage = 100;

                PdfPTable tableFechaFactura = new PdfPTable(2);
                float[] dimecionesTablaFecha = new float[2];
                dimecionesTablaFecha[0] = 0.7F;
                dimecionesTablaFecha[1] = 1.0F;
                tableFechaFactura.SetWidths(dimecionesTablaFecha);

                iTextSharp.text.pdf.PdfPCell celTextFechaFactura = new iTextSharp.text.pdf.PdfPCell(new Phrase("FECHA FACTURA: ",
                    fontTitle));
                celTextFechaFactura.Colspan = 1;
                celTextFechaFactura.Padding = 3;
                celTextFechaFactura.Border = 0;
                celTextFechaFactura.BorderColorBottom = BaseColor.WHITE;
                celTextFechaFactura.HorizontalAlignment = Element.ALIGN_CENTER;
                celTextFechaFactura.VerticalAlignment = Element.ALIGN_CENTER;
                tableFechaFactura.AddCell(celTextFechaFactura);

                iTextSharp.text.pdf.PdfPCell celFechaFactura = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    string.Format("{0:dd/MM/yyyy}", DateTime.Parse(DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"].ToString())),
                fontCustom));
                celFechaFactura.Colspan = 1;
                celFechaFactura.Padding = 3;
                celFechaFactura.PaddingTop = 4;
                celFechaFactura.Border = 0;
                celFechaFactura.BorderColorBottom = BaseColor.WHITE;
                celFechaFactura.HorizontalAlignment = Element.ALIGN_LEFT;
                celFechaFactura.VerticalAlignment = Element.ALIGN_CENTER;
                tableFechaFactura.AddCell(celFechaFactura);

                PdfPCell _celFechaFactura = new PdfPCell(tableFechaFactura);
                tableDetalles2.AddCell(_celFechaFactura);


                PdfPTable tableFechaVencimiento = new PdfPTable(2);
                float[] dmTablaFechaVencimiento = new float[2];
                dmTablaFechaVencimiento[0] = 1.0F;
                dmTablaFechaVencimiento[1] = 1.0F;
                tableFechaVencimiento.SetWidths(dmTablaFechaVencimiento);

                iTextSharp.text.pdf.PdfPCell celTextFechaVencimiento = new iTextSharp.text.pdf.PdfPCell(new Phrase("FECHA VENCIMIENTO: ",
                    fontTitle));
                celTextFechaVencimiento.Colspan = 1;
                celTextFechaVencimiento.Padding = 3;
                celTextFechaVencimiento.Border = 0;
                celTextFechaVencimiento.HorizontalAlignment = Element.ALIGN_LEFT;
                celTextFechaVencimiento.VerticalAlignment = Element.ALIGN_TOP;
                tableFechaVencimiento.AddCell(celTextFechaVencimiento);

                iTextSharp.text.pdf.PdfPCell celFechaVencimiento = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    string.Format("{0:dd/MM/yyyy}", DateTime.Parse(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"].ToString())),
                fontCustom));
                celFechaVencimiento.Colspan = 1;
                celFechaVencimiento.Padding = 4;
                celFechaVencimiento.Border = 0;
                celFechaVencimiento.BorderColorBottom = BaseColor.WHITE;
                celFechaVencimiento.HorizontalAlignment = Element.ALIGN_LEFT;
                celFechaVencimiento.VerticalAlignment = Element.ALIGN_TOP;
                tableFechaVencimiento.AddCell(celFechaVencimiento);

                PdfPCell _celFechaFacturaVencimiento = new PdfPCell(tableFechaVencimiento);
                tableDetalles2.AddCell(_celFechaFacturaVencimiento);
                //------------------------------------------------------------------------------------------------------
                PdfPTable tableMoneda_Desc = new PdfPTable(new float[] { 1, 1.6f })
                {
                    WidthPercentage = 100,
                };

                Phrase prgInfoMoneda = new Phrase();
                prgInfoMoneda.Add(new Chunk("MONEDA: ", fontTitle));
                prgInfoMoneda.Add(new Chunk((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["CurrencyCode"], fontCustom));

                tableMoneda_Desc.AddCell(new PdfPCell(prgInfoMoneda));

                Phrase prgPorcDesc = new Phrase();
                prgPorcDesc.Add(new Chunk("%Descuento: ", fontTitle2Bold));
                prgPorcDesc.Add(new Chunk((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DiscountPercent"], fontTitle2));

                tableMoneda_Desc.AddCell(new PdfPCell(prgPorcDesc));
                tableDetalles2.AddCell(new PdfPCell(tableMoneda_Desc));

                tableInfoFactura.AddCell(new PdfPCell(tableDetalles2) { Border = PdfPCell.NO_BORDER, Padding = 0, });
                #endregion

                tableEncabezadoPag.AddCell(new PdfPCell(tableInfoFactura)
                {
                    Border = PdfPCell.NO_BORDER,
                    Padding = 0,
                    PaddingBottom = 5,
                });

                #region Tabla Unidades
                PdfPTable tableTituloUnidades = new PdfPTable(6);
                //Dimenciones.
                float[] DimencionUnidades = new float[6];
                if (InvoiceType == "InvoiceType")
                {
                    DimencionUnidades[0] = 1.0F;//
                    DimencionUnidades[1] = 3.5F;//
                    DimencionUnidades[2] = 0.5F;//
                    DimencionUnidades[3] = 1.0F;//
                    DimencionUnidades[4] = 1.0F;//
                    DimencionUnidades[5] = 1.0F;//
                }
                else if (InvoiceType == "CreditNoteType" || InvoiceType == "DebitNoteType")
                {
                    DimencionUnidades[0] = 1.0F;//
                    DimencionUnidades[1] = 3.5F;//
                    DimencionUnidades[2] = 0.5F;//
                    DimencionUnidades[3] = 1.0F;//
                    DimencionUnidades[4] = 1.0F;//
                    DimencionUnidades[5] = 1.0F;//
                }

                tableTituloUnidades.WidthPercentage = 100;
                tableTituloUnidades.SetWidths(DimencionUnidades);

                iTextSharp.text.pdf.PdfPCell celCodigo = new iTextSharp.text.pdf.PdfPCell(new Phrase("CÓDIGO",
                    FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 8.3f, iTextSharp.text.Font.NORMAL)));
                celCodigo.Colspan = 1;
                celCodigo.Padding = 3;
                //celCodigo.Border = 1;
                celCodigo.BorderWidthRight = 0;
                //celCodigo.BorderColorBottom = BaseColor.WHITE;
                celCodigo.HorizontalAlignment = Element.ALIGN_CENTER;
                celCodigo.VerticalAlignment = Element.ALIGN_TOP;
                celCodigo.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableTituloUnidades.AddCell(celCodigo);

                iTextSharp.text.pdf.PdfPCell celDescripcion = new iTextSharp.text.pdf.PdfPCell(new Phrase("DESCRIPCIÓN",
                    FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 8.3f, iTextSharp.text.Font.NORMAL)));
                celDescripcion.Colspan = 1;
                celDescripcion.Padding = 3;
                //celDescripcion.Border = 1;
                celDescripcion.BorderWidthRight = 0;
                //celDescripcion.BorderColorBottom = BaseColor.WHITE;
                celDescripcion.HorizontalAlignment = Element.ALIGN_CENTER;
                celDescripcion.VerticalAlignment = Element.ALIGN_TOP;
                celDescripcion.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableTituloUnidades.AddCell(celDescripcion);

                iTextSharp.text.pdf.PdfPCell celCantidad = new iTextSharp.text.pdf.PdfPCell(new Phrase("CANT",
                    FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 8.3f, iTextSharp.text.Font.NORMAL)));
                celCantidad.Colspan = 1;
                celCantidad.Padding = 3;
                //celVendedor.Border = 0;
                celCantidad.BorderWidthRight = 0;
                //celCantidad.BorderColorBottom = BaseColor.WHITE;
                celCantidad.HorizontalAlignment = Element.ALIGN_CENTER;
                celCantidad.VerticalAlignment = Element.ALIGN_TOP;
                celCantidad.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableTituloUnidades.AddCell(celCantidad);

                if (InvoiceType == "InvoiceType")
                {
                    PdfPTable tableTextDctos = new PdfPTable(2) { WidthPercentage = 100, };

                    iTextSharp.text.pdf.PdfPCell celUnidad = new iTextSharp.text.pdf.PdfPCell(new Phrase("%DCTO",
                        FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 8.3f, iTextSharp.text.Font.NORMAL)));
                    celUnidad.Colspan = 1;
                    celUnidad.Padding = 0;
                    celUnidad.PaddingBottom = 3;
                    celUnidad.PaddingTop = 3;
                    //celVendedor.Border = 0;
                    celUnidad.BorderWidthRight = 0;
                    //celUnidad.BorderColorBottom = BaseColor.WHITE;
                    celUnidad.HorizontalAlignment = Element.ALIGN_CENTER;
                    celUnidad.VerticalAlignment = Element.ALIGN_BOTTOM;
                    celUnidad.BackgroundColor = BaseColor.LIGHT_GRAY;
                    tableTextDctos.AddCell(celUnidad);
                    tableTextDctos.AddCell(celUnidad);

                    tableTituloUnidades.AddCell(new PdfPCell(tableTextDctos) { Border = PdfPCell.NO_BORDER, });
                }
                else if (InvoiceType == "CreditNoteType" || InvoiceType == "DebitNoteType")
                {
                    iTextSharp.text.pdf.PdfPCell celUnidad = new iTextSharp.text.pdf.PdfPCell(new Phrase("UNIDAD",
                        FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 8.3f, iTextSharp.text.Font.NORMAL)));
                    celUnidad.Colspan = 1;
                    celUnidad.Padding = 0;
                    celUnidad.PaddingBottom = 3;
                    celUnidad.PaddingTop = 3;
                    //celVendedor.Border = 0;
                    celUnidad.BorderWidthRight = 0;
                    //celUnidad.BorderColorBottom = BaseColor.WHITE;
                    celUnidad.HorizontalAlignment = Element.ALIGN_CENTER;
                    celUnidad.VerticalAlignment = Element.ALIGN_BOTTOM;
                    celUnidad.BackgroundColor = BaseColor.LIGHT_GRAY;
                    tableTituloUnidades.AddCell(celUnidad);
                }


                iTextSharp.text.pdf.PdfPCell celValorUnitario = new iTextSharp.text.pdf.PdfPCell(new Phrase("VLR.UNITARIO",
                    FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 8.3f, iTextSharp.text.Font.NORMAL)));
                celValorUnitario.Colspan = 1;
                celValorUnitario.Padding = 3;
                //celVendedor.Border = 0;
                celValorUnitario.BorderWidthRight = 0;
                //celValorUnitario.BorderColorBottom = BaseColor.WHITE;
                celValorUnitario.HorizontalAlignment = Element.ALIGN_CENTER;
                celValorUnitario.VerticalAlignment = Element.ALIGN_TOP;
                celValorUnitario.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableTituloUnidades.AddCell(celValorUnitario);

                iTextSharp.text.pdf.PdfPCell celValorTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase("VALOR TOTAL",
                    FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 8.3f, iTextSharp.text.Font.NORMAL)));
                celValorTotal.Colspan = 1;
                celValorTotal.Padding = 3;
                //celVendedor.Border = 0;
                //celValorTotal.BorderColorBottom = BaseColor.WHITE;
                celValorTotal.HorizontalAlignment = Element.ALIGN_CENTER;
                celValorTotal.VerticalAlignment = Element.ALIGN_TOP;
                celValorTotal.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableTituloUnidades.AddCell(celValorTotal);

                PdfPTable tableUnidades = new PdfPTable(6);
                tableUnidades.WidthPercentage = 100;
                tableUnidades.ExtendLastRow = true;
                tableUnidades.SetWidths(DimencionUnidades);

                decimal totalCantidad = 0;
                foreach (DataRow InvoiceLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
                {
                    //if (!AddRowInvoiceRimax(InvoiceLine, ref tableItems, fuenteDatos, DsInvoiceAR.Tables["InvcTax"]))
                    //    return false;

                    totalCantidad += decimal.TryParse((string)InvoiceLine["SellingShipQty"], out totalCantidad) ? totalCantidad : 0;
                    if (InvoiceType == "InvoiceType")
                    {
                        //for (int i = 0; i < 10; i++)
                        AddUnidadesProcar(InvoiceLine, ref tableUnidades,
                            FontFactory.GetFont(FontFactory.HELVETICA, 7.8f, iTextSharp.text.Font.NORMAL));
                    }
                    else if (InvoiceType == "CreditNoteType" || InvoiceType == "DebitNoteType")
                    {
                        AddUnidadesProcarNC(InvoiceLine, ref tableUnidades,
                            FontFactory.GetFont(FontFactory.HELVETICA, 7.5f, iTextSharp.text.Font.NORMAL));
                    }
                }

                iTextSharp.text.pdf.PdfPCell LineaFinal = new iTextSharp.text.pdf.PdfPCell();
                LineaFinal.Colspan = 6;
                //LineaFinal.Padding = 3;
                LineaFinal.Border = 1;
                LineaFinal.BorderColorBottom = BaseColor.WHITE;
                LineaFinal.HorizontalAlignment = Element.ALIGN_CENTER;
                LineaFinal.VerticalAlignment = Element.ALIGN_TOP;
                //tableUnidades.AddCell(LineaFinal);

                //------------------------------------------------------------------------------------------------
                iTextSharp.text.Font fontEspacio = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 0.3F, iTextSharp.text.Font.NORMAL);
                PdfPCell celEspacio3 = new PdfPCell(new Phrase("  ", fontEspacio));
                celEspacio3.Border = 0;

                PdfPTable tableTotalesObs = new PdfPTable(3);

                float[] DimencionTotalesObs = new float[3];
                DimencionTotalesObs[0] = 2.5F;//
                DimencionTotalesObs[1] = 0.01F;//
                DimencionTotalesObs[2] = 1.0F;//

                tableTotalesObs.WidthPercentage = 100;
                tableTotalesObs.SetWidths(DimencionTotalesObs);

                PdfPTable _tableObs = new PdfPTable(1);
                _tableObs.WidthPercentage = 100;

                PdfPCell celValorLetras = new PdfPCell()
                {
                    Colspan = 1,
                    Padding = 3,
                    //Border = 0,
                    BorderColorBottom = BaseColor.WHITE,
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    VerticalAlignment = Element.ALIGN_TOP,
                };

                if (InvoiceType == "InvoiceType")
                {
                    celValorLetras.Phrase = new Phrase("ESTIMADO CLIENTE: " +
                    "SE HACE ENTREGA MANUAL DE USO DE LLANTAS", fontTitleFactura);
                }
                else if (InvoiceType == "CreditNoteType" || InvoiceType == "DebitNoteType")
                {
                    celValorLetras.Phrase = new Phrase($"VALORES EN LETRA: " +
                        $"{Nroenletras((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"])}", fontTitleFactura);
                }

                iTextSharp.text.pdf.PdfPCell celValorAsegurado = new iTextSharp.text.pdf.PdfPCell(
                    new Phrase(string.Format("SU SALDO EN CARTERA ES DE: {0:C2} al {1:dd/MM/yyyy}\n{2} " +
                    "Y/O FAVOR GIRAR CHEQUES UNICAMENTE A NOMBRE DE PROCAR INVERSIONES",
                decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number03"]),
                DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]),
                (string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Character02"]), fontTitle2));

                if (InvoiceType == "InvoiceType")
                {
                    celValorAsegurado.Colspan = 1;
                    celValorAsegurado.Padding = 5;
                    //celValorLetras.Border = 0;
                    celValorAsegurado.BorderColorBottom = BaseColor.WHITE;
                    celValorAsegurado.HorizontalAlignment = Element.ALIGN_LEFT;
                    celValorAsegurado.VerticalAlignment = Element.ALIGN_TOP;
                    celValorAsegurado.MinimumHeight = 12;
                }
                else if (InvoiceType == "CreditNoteType" || InvoiceType == "DebitNoteType")
                {
                    celValorAsegurado = new iTextSharp.text.pdf.PdfPCell(
                        new Phrase($"CONCEPTO: {(string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Character07"]}", fontTitle2Bold));
                    celValorAsegurado.Colspan = 1;
                    celValorAsegurado.Padding = 5;
                    //celValorLetras.Border = 0;
                    celValorAsegurado.BorderColorBottom = BaseColor.WHITE;
                    celValorAsegurado.HorizontalAlignment = Element.ALIGN_LEFT;
                    celValorAsegurado.VerticalAlignment = Element.ALIGN_TOP;
                    celValorAsegurado.MinimumHeight = 12;
                }

                PdfPTable tableInfoTributaria = new PdfPTable(1)
                {
                    WidthPercentage = 100,
                };

                if (InvoiceType == "InvoiceType")
                {
                    tableInfoTributaria.AddCell(new PdfPCell(new Phrase("INFORMACION TRIBUTARIA", fontTitleFacturaNormal))
                    {
                        Border = PdfPCell.BOTTOM_BORDER,
                        HorizontalAlignment = Element.ALIGN_CENTER,
                        VerticalAlignment = Element.ALIGN_CENTER,
                    });

                    tableInfoTributaria.AddCell(new PdfPCell(
                        new Phrase((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Character03"], fontTitleFactura))
                    {
                        Border = PdfPCell.NO_BORDER,
                    });
                }
                else if (InvoiceType == "CreditNoteType" || InvoiceType == "DebitNoteType")
                {
                    tableInfoTributaria.AddCell(new PdfPCell(new Phrase(
                        $"OBSERVACIONES: {(string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceComment"]}", fontTitleFactura))
                    {
                        Border = PdfPCell.NO_BORDER,
                    });
                }

                iTextSharp.text.pdf.PdfPCell celObservaciones = new iTextSharp.text.pdf.PdfPCell(tableInfoTributaria);
                celObservaciones.Colspan = 1;
                celObservaciones.Padding = 3;
                celObservaciones.MinimumHeight = 12;
                //celValorLetras.Border = 0;
                celObservaciones.BorderColorBottom = BaseColor.WHITE;
                celObservaciones.HorizontalAlignment = Element.ALIGN_LEFT;
                celObservaciones.VerticalAlignment = Element.ALIGN_TOP;

                _tableObs.AddCell(celValorLetras);
                _tableObs.AddCell(celEspacio3);
                _tableObs.AddCell(celValorAsegurado);
                _tableObs.AddCell(celEspacio3);
                _tableObs.AddCell(celObservaciones);

                iTextSharp.text.pdf.PdfPCell _celObs = new iTextSharp.text.pdf.PdfPCell(_tableObs);
                _celObs.Border = 0;
                tableTotalesObs.AddCell(_celObs);
                //----------------------------------------------------------------------------------------------------------------------------

                iTextSharp.text.pdf.PdfPCell _celEspacio2 = new iTextSharp.text.pdf.PdfPCell(new Phrase(" ", fontTitleFactura));
                _celEspacio2.Border = 0;
                tableTotalesObs.AddCell(_celEspacio2);

                //------------------------------------------------------------------------------------------

                PdfPTable _tableTotales = new PdfPTable(2);
                float[] _DimencionTotales = new float[2];
                _DimencionTotales[0] = 1.5F;//
                _DimencionTotales[1] = 2.5F;//

                _tableTotales.WidthPercentage = 100;
                _tableTotales.SetWidths(_DimencionTotales);

                iTextSharp.text.pdf.PdfPCell _celTextSubTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase("SUBTOTAL:", fontTitleFactura));
                _celTextSubTotal.Colspan = 1;
                _celTextSubTotal.Padding = 3;
                _celTextSubTotal.PaddingBottom = 5;
                _celTextSubTotal.Border = PdfPCell.LEFT_BORDER | PdfPCell.TOP_BORDER;
                _celTextSubTotal.BorderWidthRight = 1;
                _celTextSubTotal.BorderColorBottom = BaseColor.WHITE;
                _celTextSubTotal.HorizontalAlignment = Element.ALIGN_LEFT;
                _celTextSubTotal.VerticalAlignment = Element.ALIGN_TOP;
                _tableTotales.AddCell(_celTextSubTotal);

                iTextSharp.text.pdf.PdfPCell _celSubTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:C2}",
                    decimal.Parse(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubTotal"].ToString())), fontTitle2));
                _celSubTotal.Colspan = 1;
                _celSubTotal.Padding = 3;
                _celSubTotal.PaddingBottom = 5;
                _celSubTotal.Border = PdfPCell.RIGHT_BORDER | PdfPCell.TOP_BORDER;
                _celSubTotal.BorderColorBottom = BaseColor.WHITE;
                _celSubTotal.HorizontalAlignment = Element.ALIGN_RIGHT;
                _celSubTotal.VerticalAlignment = Element.ALIGN_TOP;
                _tableTotales.AddCell(_celSubTotal);

                iTextSharp.text.pdf.PdfPCell _celTextDescuentos = new iTextSharp.text.pdf.PdfPCell(new Phrase(" "/*"DESCUENTO:"*/, fontTitleFactura));
                _celTextDescuentos.Colspan = 1;
                _celTextDescuentos.Padding = 3;
                _celTextDescuentos.PaddingBottom = 5;
                _celTextDescuentos.Border = PdfPCell.LEFT_BORDER;
                _celTextDescuentos.BorderWidthRight = 1;
                _celTextDescuentos.BorderColorBottom = BaseColor.WHITE;
                _celTextDescuentos.HorizontalAlignment = Element.ALIGN_LEFT;
                _celTextDescuentos.VerticalAlignment = Element.ALIGN_TOP;
                //_tableTotales.AddCell(_celTextDescuentos);

                iTextSharp.text.pdf.PdfPCell _celDescuentos = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    " "/*Decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DocDiscount"]).ToString("C2")*/, fontTitle2));
                _celDescuentos.Colspan = 1;
                _celDescuentos.Padding = 3;
                _celDescuentos.PaddingBottom = 5;
                _celDescuentos.Border = PdfPCell.RIGHT_BORDER;
                _celDescuentos.BorderColorBottom = BaseColor.WHITE;
                _celDescuentos.HorizontalAlignment = Element.ALIGN_RIGHT;
                _celDescuentos.VerticalAlignment = Element.ALIGN_TOP;
                //_tableTotales.AddCell(_celDescuentos);

                //iTextSharp.text.pdf.PdfPCell _celTextSubTotal2 = new iTextSharp.text.pdf.PdfPCell(new Phrase("TOT. CANT", fontTitleFactura));
                //_celTextSubTotal2.Colspan = 1;
                //_celTextSubTotal2.Padding = 3;
                //_celTextSubTotal2.PaddingBottom = 5;
                //_celTextSubTotal2.Border = PdfPCell.LEFT_BORDER;
                //_celTextSubTotal2.BorderWidthRight = 1;
                //_celTextSubTotal2.BorderColorBottom = BaseColor.WHITE;
                //_celTextSubTotal2.HorizontalAlignment = Element.ALIGN_LEFT;
                //_celTextSubTotal2.VerticalAlignment = Element.ALIGN_TOP;
                //_tableTotales.AddCell(_celTextSubTotal2);

                //iTextSharp.text.pdf.PdfPCell _celSubTotal2 = new iTextSharp.text.pdf.PdfPCell(new Phrase(totalCantidad.ToString("N0"), fontTitle2));
                //_celSubTotal2.Colspan = 1;
                //_celSubTotal2.Padding = 3;
                //_celSubTotal2.PaddingBottom = 5;
                //_celSubTotal2.Border = PdfPCell.RIGHT_BORDER;
                //_celSubTotal2.BorderColorBottom = BaseColor.WHITE;
                //_celSubTotal2.HorizontalAlignment = Element.ALIGN_RIGHT;
                //_celSubTotal2.VerticalAlignment = Element.ALIGN_TOP;
                //_tableTotales.AddCell(_celSubTotal2);

                iTextSharp.text.pdf.PdfPCell _celTextIva = new iTextSharp.text.pdf.PdfPCell(new Phrase("IVA:", fontTitleFactura));
                _celTextIva.Colspan = 1;
                _celTextIva.Padding = 3;
                _celTextIva.PaddingBottom = 5;
                _celTextIva.Border = PdfPCell.LEFT_BORDER;
                _celTextIva.BorderWidthRight = 1;
                //_celTextIva.BorderColorBottom = BaseColor.BLACK;
                _celTextIva.HorizontalAlignment = Element.ALIGN_LEFT;
                _celTextIva.VerticalAlignment = Element.ALIGN_TOP;
                _tableTotales.AddCell(_celTextIva);

                iTextSharp.text.pdf.PdfPCell _celIva = new iTextSharp.text.pdf.PdfPCell(new Phrase(GetValImpuestoByID("01", DsInvoiceAR).ToString("C"), fontTitle2));
                _celIva.Colspan = 1;
                _celIva.Padding = 3;
                _celIva.PaddingBottom = 5;
                _celIva.Border = PdfPCell.RIGHT_BORDER;
                //_celIva.BorderColorBottom = BaseColor.BLACK;
                _celIva.HorizontalAlignment = Element.ALIGN_RIGHT;
                _celIva.VerticalAlignment = Element.ALIGN_TOP;
                _tableTotales.AddCell(_celIva);

                iTextSharp.text.pdf.PdfPCell _celTextTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase("TOTAL:", fontTitleFactura));
                _celTextTotal.Colspan = 1;
                _celTextTotal.Padding = 3;
                _celTextTotal.PaddingBottom = 5;
                _celTextTotal.Border = PdfPCell.LEFT_BORDER;
                _celTextTotal.BorderWidthRight = 1;
                _celTextTotal.BorderWidthTop = 1;
                _celTextTotal.HorizontalAlignment = Element.ALIGN_LEFT;
                _celTextTotal.VerticalAlignment = Element.ALIGN_TOP;
                _tableTotales.AddCell(_celTextTotal);

                iTextSharp.text.pdf.PdfPCell _celTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:C2}",
                    decimal.Parse(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"].ToString())), fontTitleFactura));
                _celTotal.Colspan = 1;
                _celTotal.Padding = 3;
                _celTotal.PaddingBottom = 5;
                _celTotal.Border = PdfPCell.RIGHT_BORDER;
                _celTotal.BorderWidthTop = 1;
                _celTotal.HorizontalAlignment = Element.ALIGN_RIGHT;
                _celTotal.VerticalAlignment = Element.ALIGN_TOP;
                _tableTotales.AddCell(_celTotal);

                _tableTotales.AddCell(new PdfPCell()
                {
                    FixedHeight = 5,
                    Colspan = 2,
                    Border = PdfPCell.TOP_BORDER,
                });

                _tableTotales.AddCell(new PdfPCell(new Phrase("TOT. CANT", fontTitleFactura))
                {
                    Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER | PdfPCell.LEFT_BORDER,
                });

                _tableTotales.AddCell(new PdfPCell(new Phrase(totalCantidad.ToString("N0"), fontTitle2))
                {
                    HorizontalAlignment = Element.ALIGN_RIGHT,
                    Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER | PdfPCell.RIGHT_BORDER,
                });

                _tableTotales.AddCell(new PdfPCell()
                {
                    Colspan = 2,
                    Border = PdfPCell.NO_BORDER,
                });

                iTextSharp.text.pdf.PdfPCell _celTotales = new iTextSharp.text.pdf.PdfPCell(_tableTotales)
                {
                    Border = PdfPCell.NO_BORDER,
                };
                tableTotalesObs.AddCell(_celTotales);

                //------------------------------------------------------------------------
                //Aqui estaban las observaciones
                PdfPTable tableResolucion = new PdfPTable(1);
                tableResolucion.WidthPercentage = 100;
                string strResolucion = $"{(string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Character05"]} " +
                    $"{(string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Character06"]}";
                iTextSharp.text.pdf.PdfPCell _celResolucion = new iTextSharp.text.pdf.PdfPCell(new Phrase(strResolucion, fontTitleFactura));
                _celResolucion.Colspan = 1;
                _celResolucion.Padding = 3;
                //_celResolucion.Border = 0;
                _celResolucion.HorizontalAlignment = Element.ALIGN_JUSTIFIED;
                _celResolucion.VerticalAlignment = Element.ALIGN_TOP;
                tableResolucion.AddCell(_celResolucion);

                //----------------------------------------------------------------------
                PdfPTable tableFirmas = new PdfPTable(5);

                float[] DimencionFirmas = new float[5];
                DimencionFirmas[0] = 1.0F;//
                DimencionFirmas[1] = 0.02F;//
                DimencionFirmas[2] = 1.0F;//
                DimencionFirmas[3] = 0.02F;//
                DimencionFirmas[4] = 1.4F;//

                tableFirmas.WidthPercentage = 100;
                tableFirmas.SetWidths(DimencionFirmas);

                PdfPTable tableDespacahdoPor = new PdfPTable(1);
                tableDespacahdoPor.WidthPercentage = 100;

                if (InvoiceType == "InvoiceType")
                {
                    iTextSharp.text.pdf.PdfPCell celDespachadoPor = new iTextSharp.text.pdf.PdfPCell(new Phrase("\n\n\n\n\n\n\n\n usuario que genera la factura:" +
                        "\n______________________________________\n\nDESPACHADO POR", fontTitleFactura));
                    celDespachadoPor.Colspan = 1;
                    celDespachadoPor.Padding = 3;
                    celDespachadoPor.FixedHeight = 90;
                    celDespachadoPor.HorizontalAlignment = Element.ALIGN_CENTER;
                    celDespachadoPor.VerticalAlignment = Element.ALIGN_TOP;
                    tableDespacahdoPor.AddCell(celDespachadoPor);
                }
                else if (InvoiceType == "CreditNoteType" || InvoiceType == "DebitNoteType")
                {
                    iTextSharp.text.pdf.PdfPCell celDespachadoPor = new iTextSharp.text.pdf.PdfPCell(new Phrase("\n\n\n\n\n\n\n\n usuario que genera la factura:" +
                    "\n______________________________________\n\nELABORADO", fontTitleFactura));
                    celDespachadoPor.Colspan = 1;
                    celDespachadoPor.Padding = 3;
                    celDespachadoPor.FixedHeight = 90;
                    celDespachadoPor.HorizontalAlignment = Element.ALIGN_CENTER;
                    celDespachadoPor.VerticalAlignment = Element.ALIGN_TOP;
                    tableDespacahdoPor.AddCell(celDespachadoPor);
                }

                tableDespacahdoPor.AddCell(new PdfPCell()
                {
                    Border = PdfPCell.TOP_BORDER,
                });

                tableFirmas.AddCell(new PdfPCell(tableDespacahdoPor)
                {
                    Border = PdfPCell.NO_BORDER,
                });
                //----------------------------------------------------------------------------------------------------------------------------

                tableFirmas.AddCell(celEspacio2);

                //------------------------------------------------------------------------------------------

                PdfPTable tableFirmaConductor = new PdfPTable(1);
                tableFirmaConductor.WidthPercentage = 100;

                if (InvoiceType == "InvoiceType")
                {
                    iTextSharp.text.pdf.PdfPCell celFirmaConductor = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    $"OBSERVACIONES:\n{(string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceComment"]}\n" +
                    $"{(string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Character04"]}", fontTitleFacturaNormal));
                    celFirmaConductor.Colspan = 1;
                    celFirmaConductor.Padding = 3;
                    //celFirmaConductor.Border = 0;
                    celFirmaConductor.HorizontalAlignment = Element.ALIGN_JUSTIFIED;
                    celFirmaConductor.VerticalAlignment = Element.ALIGN_TOP;
                    tableFirmaConductor.AddCell(celFirmaConductor);

                    tableFirmas.AddCell(new PdfPCell(tableFirmaConductor)
                    {
                        Border = PdfPCell.NO_BORDER,
                    });
                }
                else if (InvoiceType == "CreditNoteType" || InvoiceType == "DebitNoteType")
                {
                    iTextSharp.text.pdf.PdfPCell celFirmaConductor = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    $"______________________________________\n\nFIRMA DEL CONDUCTOR", fontTitleFactura));
                    celFirmaConductor.Colspan = 1;
                    celFirmaConductor.Padding = 3;
                    //celFirmaConductor.Border = 0;
                    celFirmaConductor.HorizontalAlignment = Element.ALIGN_CENTER;
                    celFirmaConductor.VerticalAlignment = Element.ALIGN_BOTTOM;
                    tableFirmaConductor.AddCell(celFirmaConductor);

                    tableFirmas.AddCell(new PdfPCell(tableFirmaConductor)
                    {
                        Border = PdfPCell.NO_BORDER,
                    });
                }

                //-------------------------------------------------------------------------------------
                tableFirmas.AddCell(celEspacio2);
                //-------------------------------------------------------------------------------------

                PdfPTable tableSelloCliente = new PdfPTable(1);
                //tableSelloCliente.WidthPercentage = 100;

                iTextSharp.text.pdf.PdfPCell cel1SelloCliente = new iTextSharp.text.pdf.PdfPCell(new Phrase("Apreciado cliente este documento " +
                    "no es endolsable", fontTitleFactura));
                cel1SelloCliente.Colspan = 1;
                cel1SelloCliente.Padding = 5;
                cel1SelloCliente.Border = 1;
                cel1SelloCliente.BorderWidthBottom = 1;
                cel1SelloCliente.HorizontalAlignment = Element.ALIGN_LEFT;
                cel1SelloCliente.VerticalAlignment = Element.ALIGN_TOP;
                tableSelloCliente.AddCell(cel1SelloCliente);

                iTextSharp.text.pdf.PdfPCell cel2SelloFechaRecibido = new iTextSharp.text.pdf.PdfPCell(new Phrase("Fecha de recibido:", fontTitleFactura));

                cel2SelloFechaRecibido.Colspan = 1;
                cel2SelloFechaRecibido.Padding = 3;
                cel2SelloFechaRecibido.Border = 0;
                cel2SelloFechaRecibido.HorizontalAlignment = Element.ALIGN_LEFT;
                cel2SelloFechaRecibido.VerticalAlignment = Element.ALIGN_TOP;
                tableSelloCliente.AddCell(cel2SelloFechaRecibido);

                iTextSharp.text.pdf.PdfPCell cel2SelloNombre = new iTextSharp.text.pdf.PdfPCell(new Phrase("Nombre:", fontTitleFactura));

                cel2SelloNombre.Colspan = 1;
                cel2SelloNombre.Padding = 3;
                cel2SelloNombre.Border = 0;
                cel2SelloNombre.HorizontalAlignment = Element.ALIGN_LEFT;
                cel2SelloNombre.VerticalAlignment = Element.ALIGN_TOP;
                tableSelloCliente.AddCell(cel2SelloNombre);

                iTextSharp.text.pdf.PdfPCell cel2SelloId = new iTextSharp.text.pdf.PdfPCell(new Phrase("Identificacion:", fontTitleFactura));

                cel2SelloId.Colspan = 1;
                cel2SelloId.Padding = 3;
                cel2SelloId.Border = 0;
                cel2SelloId.HorizontalAlignment = Element.ALIGN_LEFT;
                cel2SelloId.VerticalAlignment = Element.ALIGN_TOP;
                tableSelloCliente.AddCell(cel2SelloId);

                iTextSharp.text.pdf.PdfPCell cel2SelloFirma = new iTextSharp.text.pdf.PdfPCell(new Phrase("Firma:\n", fontTitleFactura));

                cel2SelloFirma.Colspan = 1;
                cel2SelloFirma.Padding = 3;
                cel2SelloFirma.Border = 0;
                cel2SelloFirma.HorizontalAlignment = Element.ALIGN_LEFT;
                cel2SelloFirma.VerticalAlignment = Element.ALIGN_TOP;
                tableSelloCliente.AddCell(cel2SelloFirma);

                iTextSharp.text.pdf.PdfPCell cel3SelloCliente = new iTextSharp.text.pdf.PdfPCell(new Phrase("______________________________________________________\n\n" +
                    "RECIBIDO CONFORME FIRMA Y SELLO DEL CLIENTE", fontTitleFactura));

                cel3SelloCliente.Colspan = 1;
                cel3SelloCliente.Padding = 3;
                cel3SelloCliente.Border = 0;
                cel3SelloCliente.HorizontalAlignment = Element.ALIGN_CENTER;
                cel3SelloCliente.VerticalAlignment = Element.ALIGN_BOTTOM;
                tableSelloCliente.AddCell(cel3SelloCliente);

                //_celSelloCliente.Border = 0;
                //_celSelloCliente.BorderColor = BaseColor.WHITE;
                tableFirmas.AddCell(new PdfPCell(tableSelloCliente));

                tableFirmas.AddCell(new PdfPCell(new Phrase("PASADOS DIEZ (10) DIAS HABILES DE FECHA DE FACTURA" +
                    " NO SE ACEPTAN DEVOLUCIONES DE MERCANCIA NI CAMBIO DE FACTURA",
                    FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL)))
                {
                    Border = PdfPCell.NO_BORDER,
                    Colspan = 5,
                    HorizontalAlignment = Element.ALIGN_CENTER,
                });

                tableFirmas.AddCell(new PdfPCell(new Phrase($"Fecha de impresion: {DateTime.Now.ToString("dd/MM/yyyy HH:mm")}",
                    FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL)))
                {
                    Border = PdfPCell.NO_BORDER,
                    Colspan = 5,
                    HorizontalAlignment = Element.ALIGN_RIGHT,
                });
                #endregion

                tableEncabezadoPag.AddCell(new PdfPCell(tableTituloUnidades)
                {
                    Border = PdfPCell.NO_BORDER,
                    VerticalAlignment = Element.ALIGN_BOTTOM,
                    Padding = 0,
                });

                tableContEncabezadoPag.AddCell(new PdfPCell(tableEncabezadoPag)
                {
                    Border = PdfPCell.NO_BORDER,
                    FixedHeight = 280,
                    VerticalAlignment = Element.ALIGN_BOTTOM,
                    Padding = 0,
                });

                //writer.PageEvent = new EventPageEncabezado(tableContEncabezadoPag, 0/*28f*/);
                //--------------------------------------------------------------------------------------------------
                PdfPTable tablePiePag = new PdfPTable(1) { WidthPercentage = 100, };

                tablePiePag.AddCell(new PdfPCell()
                {
                    Border = PdfPCell.TOP_BORDER,
                    FixedHeight = 5,
                    VerticalAlignment = Element.ALIGN_TOP,
                    Padding = 0,
                    //PaddingBottom = 5,
                });

                tablePiePag.AddCell(new PdfPCell(tableTotalesObs)
                {
                    Border = PdfPCell.NO_BORDER,
                    Padding = 0,
                    PaddingBottom = 5,
                });

                tablePiePag.AddCell(new PdfPCell(tableResolucion)
                {
                    Border = PdfPCell.NO_BORDER,
                    Padding = 0,
                    PaddingBottom = 5,
                });

                tablePiePag.AddCell(new PdfPCell(tableFirmas)
                {
                    Border = PdfPCell.NO_BORDER,
                    Padding = 0,
                    //PaddingBottom = 5,
                });

                writer.PageEvent = new EventPagePiePag(tablePiePag, 315f);

                document.Open();

                //document.Add(divLogo);
                //document.Add(divAcercade);
                //document.Add(divQR);
                //document.Add(divNumeroFactura);
                //document.Add(tableEncabezadoPag);
                //document.Add(divEspacio);
                //document.Add(tableFacturar);
                //document.Add(divEspacio);
                //document.Add(tableDetalles);
                //document.Add(tableDetalles2);
                //document.Add(divEspacio2);
                //document.Add(tableTituloUnidades);
                document.Add(tableUnidades);
                //document.Add(divEspacio2);
                //document.Add(divEspacio2);
                //document.Add(tableTotalesObs);
                //document.Add(divEspacio2);
                //document.Add(divEspacio2);
                //document.Add(tableResolucion);
                //document.Add(divEspacio2);
                //document.Add(divEspacio2);
                //document.Add(tableFirmas);

                /*PIE DE PAGINA*/
                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();
                RutaPdf = NomArchivo;
                //AddPageNumberPagTo($"{Ruta}{NomArchivo}", 460f, 697f, fontTitle, "Pag.  ", "/", string.Empty);
                return true;
            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.Message;
                return false;
            }
        }

        private void AddUnidadesProcar(DataRow dataLine, ref PdfPTable pdfPTable, iTextSharp.text.Font font)
        {
            try
            {
                iTextSharp.text.pdf.PdfPCell celCodigo = new iTextSharp.text.pdf.PdfPCell(new Phrase((string)dataLine["PartNum"], font));
                celCodigo.Colspan = 1;
                celCodigo.Padding = 3;
                //celCodigo.Border = 0;
                celCodigo.BorderWidthBottom = 0;
                celCodigo.BorderWidthTop = 0;
                celCodigo.BorderWidthRight = 0;
                celCodigo.BorderColorBottom = BaseColor.WHITE;
                celCodigo.HorizontalAlignment = Element.ALIGN_CENTER;
                celCodigo.VerticalAlignment = Element.ALIGN_TOP;
                pdfPTable.AddCell(celCodigo);

                iTextSharp.text.pdf.PdfPCell celDescripcion = new iTextSharp.text.pdf.PdfPCell(new Phrase(dataLine["LineDesc"].ToString(), font));
                celDescripcion.Colspan = 1;
                celDescripcion.Padding = 3;
                //celDescripcion.Border = 0;
                celDescripcion.BorderWidthBottom = 0;
                celDescripcion.BorderWidthTop = 0;
                celDescripcion.BorderWidthRight = 0;
                celDescripcion.BorderColorBottom = BaseColor.WHITE;
                celDescripcion.HorizontalAlignment = Element.ALIGN_CENTER;
                celDescripcion.VerticalAlignment = Element.ALIGN_TOP;
                pdfPTable.AddCell(celDescripcion);

                iTextSharp.text.pdf.PdfPCell celCantidad = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    decimal.Parse((string)dataLine["SellingShipQty"]).ToString("N0"), font));
                celCantidad.Colspan = 1;
                celCantidad.Padding = 3;
                //celCantidad.Border = 0;
                celCantidad.BorderWidthBottom = 0;
                celCantidad.BorderWidthTop = 0;
                celCantidad.BorderWidthRight = 0;
                celCantidad.BorderColorBottom = BaseColor.WHITE;
                celCantidad.HorizontalAlignment = Element.ALIGN_CENTER;
                celCantidad.VerticalAlignment = Element.ALIGN_TOP;
                pdfPTable.AddCell(celCantidad);

                PdfPTable tablePorcDescuentos = new PdfPTable(2);

                tablePorcDescuentos.AddCell(new PdfPCell(new Phrase($"%{decimal.Parse((string)dataLine["DiscountPercent"]).ToString("N2")}", font))
                {
                    Border = PdfPCell.RIGHT_BORDER,
                    HorizontalAlignment = Element.ALIGN_CENTER,
                });

                tablePorcDescuentos.AddCell(new PdfPCell(new Phrase($"%{decimal.Parse((string)dataLine["Number01"]).ToString("N2")}", font))
                {
                    Border = PdfPCell.NO_BORDER,
                    HorizontalAlignment = Element.ALIGN_CENTER,
                });

                iTextSharp.text.pdf.PdfPCell celUnidad = new iTextSharp.text.pdf.PdfPCell(tablePorcDescuentos);
                celUnidad.Colspan = 1;
                celUnidad.Padding = 0;
                //celUnidad.Border = 0;
                celUnidad.BorderWidthBottom = 0;
                celUnidad.BorderWidthTop = 0;
                celUnidad.BorderWidthRight = 0;
                celUnidad.BorderColorBottom = BaseColor.WHITE;
                celUnidad.HorizontalAlignment = Element.ALIGN_CENTER;
                celUnidad.VerticalAlignment = Element.ALIGN_TOP;
                pdfPTable.AddCell(celUnidad);

                iTextSharp.text.pdf.PdfPCell celValorUnitario = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:C2}",
                    Convert.ToDecimal(dataLine["UnitPrice"].ToString())), font));
                celValorUnitario.Colspan = 1;
                celValorUnitario.Padding = 3;
                //celValorUnitario.Border = 0;
                celValorUnitario.BorderWidthBottom = 0;
                celValorUnitario.BorderWidthTop = 0;
                celValorUnitario.BorderWidthRight = 0;
                celValorUnitario.BorderColorBottom = BaseColor.WHITE;
                celValorUnitario.HorizontalAlignment = Element.ALIGN_CENTER;
                celValorUnitario.VerticalAlignment = Element.ALIGN_TOP;
                pdfPTable.AddCell(celValorUnitario);

                //decimal cantidad = 0, unitPrice = 0, Total = 0;
                //cantidad = decimal.Parse(dataLine["SellingShipQty"].ToString());
                //unitPrice = decimal.Parse(dataLine["DocExtPrice"].ToString());
                //Total = cantidad * unitPrice;
                //SubTotalDtl

                decimal SubTotalDtl = 0;

                if (!decimal.TryParse((string)dataLine["SubTotalDtl"], out SubTotalDtl))
                    SubTotalDtl = 0;

                iTextSharp.text.pdf.PdfPCell celValorTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase(SubTotalDtl.ToString("C2"), font));
                celValorTotal.Colspan = 1;
                celValorTotal.Padding = 3;
                //celValorTotal.Border = 0;
                celValorTotal.BorderWidthBottom = 0;
                celValorTotal.BorderWidthTop = 0;
                celValorTotal.BorderColorBottom = BaseColor.WHITE;
                celValorTotal.HorizontalAlignment = Element.ALIGN_CENTER;
                celValorTotal.VerticalAlignment = Element.ALIGN_TOP;
                pdfPTable.AddCell(celValorTotal);

                //iTextSharp.text.pdf.PdfPCell celValorTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:C2}",
                //    decimal.Parse(dataLine["DocExtPrice"].ToString())), font));
                //celValorTotal.Colspan = 1;
                //celValorTotal.Padding = 3;
                ////celValorTotal.Border = 0;
                //celValorTotal.BorderWidthBottom = 0;
                //celValorTotal.BorderWidthTop = 0;
                //celValorTotal.BorderColorBottom = BaseColor.WHITE;
                //celValorTotal.HorizontalAlignment = Element.ALIGN_CENTER;
                //celValorTotal.VerticalAlignment = Element.ALIGN_TOP;
                //pdfPTable.AddCell(celValorTotal);
            }
            catch (Exception ex)
            {
                strError += "Error addLineInvoice: " + ex.Message;
            }
        }

        private void AddUnidadesProcarNC(DataRow dataLine, ref PdfPTable pdfPTable, iTextSharp.text.Font font)
        {
            try
            {
                iTextSharp.text.pdf.PdfPCell celCodigo = new iTextSharp.text.pdf.PdfPCell(new Phrase((string)dataLine["PartNum"], font));
                celCodigo.Colspan = 1;
                celCodigo.Padding = 3;
                //celCodigo.Border = 0;
                celCodigo.BorderWidthBottom = 0;
                celCodigo.BorderWidthTop = 0;
                celCodigo.BorderWidthRight = 0;
                celCodigo.BorderColorBottom = BaseColor.WHITE;
                celCodigo.HorizontalAlignment = Element.ALIGN_CENTER;
                celCodigo.VerticalAlignment = Element.ALIGN_TOP;
                pdfPTable.AddCell(celCodigo);

                iTextSharp.text.pdf.PdfPCell celDescripcion = new iTextSharp.text.pdf.PdfPCell(new Phrase(dataLine["LineDesc"].ToString(), font));
                celDescripcion.Colspan = 1;
                celDescripcion.Padding = 3;
                //celDescripcion.Border = 0;
                celDescripcion.BorderWidthBottom = 0;
                celDescripcion.BorderWidthTop = 0;
                celDescripcion.BorderWidthRight = 0;
                celDescripcion.BorderColorBottom = BaseColor.WHITE;
                celDescripcion.HorizontalAlignment = Element.ALIGN_CENTER;
                celDescripcion.VerticalAlignment = Element.ALIGN_TOP;
                pdfPTable.AddCell(celDescripcion);

                iTextSharp.text.pdf.PdfPCell celCantidad = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    decimal.Parse((string)dataLine["SellingShipQty"]).ToString("N0"), font));
                celCantidad.Colspan = 1;
                celCantidad.Padding = 3;
                //celCantidad.Border = 0;
                celCantidad.BorderWidthBottom = 0;
                celCantidad.BorderWidthTop = 0;
                celCantidad.BorderWidthRight = 0;
                celCantidad.BorderColorBottom = BaseColor.WHITE;
                celCantidad.HorizontalAlignment = Element.ALIGN_CENTER;
                celCantidad.VerticalAlignment = Element.ALIGN_TOP;
                pdfPTable.AddCell(celCantidad);

                iTextSharp.text.pdf.PdfPCell celUnidad = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    (string)dataLine["SalesUm"], font));
                celUnidad.Colspan = 1;
                celUnidad.Padding = 3;
                //celCantidad.Border = 0;
                celUnidad.BorderWidthBottom = 0;
                celUnidad.BorderWidthTop = 0;
                celUnidad.BorderWidthRight = 0;
                celUnidad.BorderColorBottom = BaseColor.WHITE;
                celUnidad.HorizontalAlignment = Element.ALIGN_CENTER;
                celUnidad.VerticalAlignment = Element.ALIGN_TOP;
                pdfPTable.AddCell(celUnidad);

                iTextSharp.text.pdf.PdfPCell celValorUnitario = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:C2}",
                    Convert.ToDecimal(dataLine["UnitPrice"].ToString())), font));
                celValorUnitario.Colspan = 1;
                celValorUnitario.Padding = 3;
                //celValorUnitario.Border = 0;
                celValorUnitario.BorderWidthBottom = 0;
                celValorUnitario.BorderWidthTop = 0;
                celValorUnitario.BorderWidthRight = 0;
                celValorUnitario.BorderColorBottom = BaseColor.WHITE;
                celValorUnitario.HorizontalAlignment = Element.ALIGN_CENTER;
                celValorUnitario.VerticalAlignment = Element.ALIGN_TOP;
                pdfPTable.AddCell(celValorUnitario);

                //decimal cantidad = 0, unitPrice = 0, Total = 0;
                //cantidad = decimal.Parse(dataLine["SellingShipQty"].ToString());
                //unitPrice = decimal.Parse(dataLine["DocExtPrice"].ToString());
                //Total = cantidad * unitPrice;

                decimal SubTotalDtl = 0;

                if (!decimal.TryParse((string)dataLine["SubTotalDtl"], out SubTotalDtl))
                    SubTotalDtl = 0;

                iTextSharp.text.pdf.PdfPCell celValorTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase(SubTotalDtl.ToString("C2"), font));
                celValorTotal.Colspan = 1;
                celValorTotal.Padding = 3;
                //celValorTotal.Border = 0;
                celValorTotal.BorderWidthBottom = 0;
                celValorTotal.BorderWidthTop = 0;
                celValorTotal.BorderColorBottom = BaseColor.WHITE;
                celValorTotal.HorizontalAlignment = Element.ALIGN_CENTER;
                celValorTotal.VerticalAlignment = Element.ALIGN_TOP;
                pdfPTable.AddCell(celValorTotal);

                //iTextSharp.text.pdf.PdfPCell celValorTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:C2}",
                //    decimal.Parse(dataLine["DocExtPrice"].ToString())), font));
                //celValorTotal.Colspan = 1;
                //celValorTotal.Padding = 3;
                ////celValorTotal.Border = 0;
                //celValorTotal.BorderWidthBottom = 0;
                //celValorTotal.BorderWidthTop = 0;
                //celValorTotal.BorderColorBottom = BaseColor.WHITE;
                //celValorTotal.HorizontalAlignment = Element.ALIGN_CENTER;
                //celValorTotal.VerticalAlignment = Element.ALIGN_TOP;
                //pdfPTable.AddCell(celValorTotal);
            }
            catch (Exception ex)
            {
                strError += "Error addLineInvoice: " + ex.Message;
            }
        }

        #endregion
    }
}
