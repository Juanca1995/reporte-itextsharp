﻿namespace RulesServicesDIAN2.Adquiriente
{
    using iTextSharp.text;
    using iTextSharp.text.pdf;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Diagnostics;
    using System.Drawing;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.NetworkInformation;
    using System.Text;
    using System.Threading.Tasks;
    using System.Web;
    using System.Windows.Forms;

    public partial class pdfEstandarAR
    {
        public bool FacturaNacionalFedco(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {
            bool esLocal = Helpers.Compartido.EsLocal;
            string Ruta = string.Empty;
            string RutaImg = string.Empty;
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            if (esLocal)
            {
                Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
                RutaImg = $@"{AppDomain.CurrentDomain.BaseDirectory}\Logo_Fedco.png";
            }
            else
            {
                Ruta = HttpContext.Current.Server.MapPath("~/PDF/" + NIT + "/");
                RutaImg = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";
            }
            NomArchivo = string.Empty;
            NomArchivo = NombreInvoice + ".pdf";

            try
            {
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.
                //Document document = Helpers.Fedco.Documento;//new Document(iTextSharp.text.PageSize.LETTER);
                Document document = Helpers.E4.Documento;//new Document(iTextSharp.text.PageSize.LETTER);
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;

                //---- capturando las tablas estaticas
                PdfPTable Encabezado = Fedco.FacturaNacional.Encabezado(DsInvoiceAR, QRInvoice, InvoiceType, RutaImg, CUFE);
                //PdfPTable Encabezado = E4.FacturaNacional.Encabezado(DsInvoiceAR, QRInvoice, InvoiceType, RutaImg, CUFE);
                PdfPTable DatosCliente = Fedco.FacturaNacional.DatosCliente(DsInvoiceAR);
                //PdfPTable DatosCliente = E4.FacturaNacional.DatosCliente(DsInvoiceAR);
                PdfPTable DatosFactura = Fedco.FacturaNacional.DatosFactura(DsInvoiceAR);
                //PdfPTable DatosFactura = E4.FacturaNacional.DatosFactura(DsInvoiceAR);
                PdfPTable DetalleHead = Fedco.FacturaNacional.DetalleHead(235);
                //PdfPTable DetalleHead = E4.FacturaNacional.DetalleHead(235);
                PdfPTable PiePagina = Fedco.FacturaNacional.PiePagina(DsInvoiceAR);
                //PdfPTable PiePagina = E4.FacturaNacional.PiePagina(DsInvoiceAR);

                //writer.PageEvent = new Helpers.Fedco.EventPageFedco(Encabezado, DatosCliente, DatosFactura, DetalleHead, PiePagina);
                writer.PageEvent = new Helpers.Fedco.EventPageFedco(Encabezado, DatosCliente, DatosFactura, DetalleHead, PiePagina);

                // step 3: we open the document     
                document.Open();
                document.Add(Fedco.FacturaNacional.Detalles(DsInvoiceAR));
                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();
                Helpers.Simex.AddPageNumberPagToSimexExp($"{Ruta}{NomArchivo}", dimxSimex, dimYSimex + 10, Helpers.Fuentes.SimexLeyenda);
                RutaPdf = NomArchivo;
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return false;
            }
        }
        public class Fedco
        {
            public class FacturaNacional
            {
                public static PdfPTable Encabezado(DataSet ds, System.Drawing.Image QR, string InvoiceType, string RutaImg, string CUFE)
                {
                    //Variables ------
                    string InvoiceNum = string.Empty;
                    string InvcHeadCompany = string.Empty;
                    string InvcHeadInvoiceRef = string.Empty;
                    string InvcHeadLegalNumber = string.Empty;
                    string CompanyName = string.Empty;
                    string CompanyState = string.Empty;
                    string CompanyCity = string.Empty;
                    string CompanyCountry = string.Empty;
                    string CompanyAddress1 = string.Empty;
                    string CompanyStateTaxID = string.Empty;
                    string CompanyPhoneNum = string.Empty;
                    //Asignacion de valores
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "InvoiceNum", 0))
                        InvoiceNum = ds.Tables["InvcHead"].Rows[0]["InvoiceNum"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "Company", 0))
                        InvcHeadCompany = ds.Tables["InvcHead"].Rows[0]["Company"].ToString();

                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "InvoiceRef", 0))
                        InvcHeadInvoiceRef = ds.Tables["InvcHead"].Rows[0]["InvoiceRef"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "LegalNumber", 0))
                        InvcHeadLegalNumber = ds.Tables["InvcHead"].Rows[0]["LegalNumber"].ToString();

                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "Company", "Name", 0))
                        CompanyName = ds.Tables["Company"].Rows[0]["Name"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "Company", "State", 0))
                        CompanyState = ds.Tables["Company"].Rows[0]["State"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "Company", "City", 0))
                        CompanyCity = ds.Tables["Company"].Rows[0]["City"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "Company", "Address1", 0))
                        CompanyAddress1 = ds.Tables["Company"].Rows[0]["Address1"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "Company", "StateTaxID", 0))
                        CompanyStateTaxID = ds.Tables["Company"].Rows[0]["StateTaxID"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "Company", "Country", 0))
                        CompanyCountry = ds.Tables["Company"].Rows[0]["Country"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "Company", "PhoneNum", 0))
                        CompanyPhoneNum = ds.Tables["Company"].Rows[0]["PhoneNum"].ToString();

                    PdfPTable table = new PdfPTable(4);
                    table.WidthPercentage = 100;
                    table.SetWidths(new float[] { 0.9f, 2.3f, 0.9f, 1.5f });
                    PdfPCell celLogo = new PdfPCell();

                    iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                    iTextSharp.text.Font fontTitle2 = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                    iTextSharp.text.Font fontCustom = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);

                    Paragraph prgTitle = new Paragraph("", fontTitle);

                    Paragraph prgTitle2 = new Paragraph(string.Format("FEDCO S.A. EN REORGANIZACION\nNIT. {0}\n{1}\n{2}\n{3} \n{4}\n{5}", "890109640-3",
                        "CALLE 79B No. 75-460", "TEL.: 3532218", "BARRANQUILLA - COLOMBIA", "", ""), fontTitle2);
                    prgTitle.Alignment = Element.ALIGN_CENTER;
                    prgTitle2.Alignment = Element.ALIGN_CENTER;
                    celLogo.AddElement(prgTitle);
                    celLogo.AddElement(prgTitle2);

                    //PdfDiv divLogo = new PdfDiv();
                    //divLogo.Float = iTextSharp.text.pdf.PdfDiv.FloatType.LEFT;
                    //divLogo.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                    //divLogo.BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID;
                    //divLogo.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                    //divLogo.Height = 20;
                    //divLogo.Width = 30;

                    System.Drawing.Image logo = null;
                    var requestLogo = WebRequest.Create(RutaImg);
                    using (var responseLogo = requestLogo.GetResponse())
                    using (var streamLogo = responseLogo.GetResponseStream())
                    {
                        logo = Bitmap.FromStream(streamLogo);
                    }
                    iTextSharp.text.Image ImgLogo = iTextSharp.text.Image.GetInstance(logo, BaseColor.WHITE);
                    ImgLogo.ScaleAbsolute(80f, 80f);

                    //divLogo.BackgroundImage = ImgLogo;
                    PdfPCell celLogoE4 = new PdfPCell()
                    {

                        Border = 0,
                        //Colspan =2,        
                        VerticalAlignment = Element.ALIGN_TOP,
                    };
                    celLogoE4.AddElement(ImgLogo);

                    table.AddCell(celLogoE4);


                    //-----------------------------------------------------------------------------------------------------
                    iTextSharp.text.Image ImgQR = iTextSharp.text.Image.GetInstance(QR, BaseColor.WHITE);
                    ImgQR.ScaleAbsolute(60f, 60f);
                    ImgQR.Alignment = Element.ALIGN_CENTER;
                    PdfPCell celQR = new PdfPCell();
                    celQR.AddElement(ImgQR);
                    Paragraph prgCUFE = new Paragraph(CUFE, Helpers.Fuentes.Arial8Normal);
                    prgCUFE.Alignment = Element.ALIGN_CENTER;
                    celQR.AddElement(prgCUFE);
                    //----------------------------------------------------------------------------------------------------
                    iTextSharp.text.Font fontTitleFactura = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);

                    PdfPTable tableFactura = new PdfPTable(3);

                    tableFactura.WidthPercentage = 100;
                    tableFactura.SetWidths(new float[] { 1.0f, 4.0f, 0.5f });

                    iTextSharp.text.pdf.PdfPCell celTittle = new iTextSharp.text.pdf.PdfPCell(new Phrase("FACTURA DE VENTA\n\n", fontTitleFactura));
                    celTittle.Colspan = 3;
                    celTittle.Padding = 3;
                    celTittle.HorizontalAlignment = Element.ALIGN_CENTER;
                    celTittle.VerticalAlignment = Element.ALIGN_TOP;
                    celTittle.Border = 0;
                    celTittle.BackgroundColor = BaseColor.LIGHT_GRAY;
                    tableFactura.AddCell(celTittle);

                    iTextSharp.text.pdf.PdfPCell celNo = new iTextSharp.text.pdf.PdfPCell(new Phrase("No. ", fontTitleFactura));
                    celNo.Colspan = 1;
                    celNo.Padding = 5;
                    celNo.HorizontalAlignment = Element.ALIGN_CENTER;
                    celNo.VerticalAlignment = Element.ALIGN_TOP;
                    celNo.Border = 0;
                    celNo.BackgroundColor = BaseColor.LIGHT_GRAY;
                    tableFactura.AddCell(celNo);

                    string NumLegalFactura = string.Empty;
                    if (InvoiceType == "InvoiceType")
                        NumLegalFactura = InvcHeadLegalNumber;
                    else if (InvoiceType == "CreditNoteType")
                        NumLegalFactura = InvcHeadLegalNumber;
                    else
                        NumLegalFactura = InvcHeadLegalNumber;

                    iTextSharp.text.pdf.PdfPCell celNoFactura = new iTextSharp.text.pdf.PdfPCell(new Phrase(NumLegalFactura, fontTitleFactura));
                    celNoFactura.Colspan = 1;
                    celNoFactura.Padding = 5;
                    celNoFactura.HorizontalAlignment = Element.ALIGN_LEFT;
                    celNoFactura.VerticalAlignment = Element.ALIGN_TOP;
                    celNoFactura.Border = 0;
                    celNoFactura.BackgroundColor = BaseColor.WHITE;
                    tableFactura.AddCell(celNoFactura);


                    iTextSharp.text.pdf.PdfPCell celEspacioNoFactura = new iTextSharp.text.pdf.PdfPCell(new Phrase(" ", fontTitleFactura));
                    celEspacioNoFactura.Colspan = 1;
                    //celNo.Padding = 3;
                    celEspacioNoFactura.HorizontalAlignment = Element.ALIGN_CENTER;
                    celEspacioNoFactura.VerticalAlignment = Element.ALIGN_TOP;
                    celEspacioNoFactura.Border = 0;
                    celEspacioNoFactura.BackgroundColor = BaseColor.LIGHT_GRAY;
                    tableFactura.AddCell(celEspacioNoFactura);

                    iTextSharp.text.pdf.PdfPCell celRellenoNoFactura = new iTextSharp.text.pdf.PdfPCell(new Phrase(" ", fontTitleFactura));
                    celRellenoNoFactura.Colspan = 3;
                    celRellenoNoFactura.Padding = 3;
                    celRellenoNoFactura.BackgroundColor = BaseColor.LIGHT_GRAY;
                    celRellenoNoFactura.HorizontalAlignment = Element.ALIGN_RIGHT;
                    celRellenoNoFactura.VerticalAlignment = Element.ALIGN_TOP;
                    celRellenoNoFactura.Border = 0;
                    tableFactura.AddCell(celRellenoNoFactura);

                    iTextSharp.text.pdf.PdfPCell celConsecutivoInterno = new iTextSharp.text.pdf.PdfPCell(new Phrase(InvoiceNum, fontCustom));
                    celConsecutivoInterno.Colspan = 4;
                    //celConsecutivoInterno.Padding = 3;
                    celConsecutivoInterno.BackgroundColor = BaseColor.WHITE;
                    celConsecutivoInterno.HorizontalAlignment = Element.ALIGN_RIGHT;
                    celConsecutivoInterno.VerticalAlignment = Element.ALIGN_TOP;
                    celConsecutivoInterno.Border = 0;
                    tableFactura.AddCell(celConsecutivoInterno);

                    PdfPCell celNumeroFactura = new PdfPCell();

                    celNumeroFactura.AddElement(tableFactura);
                    //-- Agregando todas las celdas a la tabla maestra--
                    celLogo.Border = 0;
                    celLogo.Padding = 0;
                    //celAcercade.Border = 0;
                    //celAcercade.Padding = 0;
                    celQR.Border = 0;
                    celQR.Padding = 0;
                    celNumeroFactura.Border = 0;
                    celNumeroFactura.PaddingLeft = 5;
                    celNumeroFactura.Padding = 0;

                    table.AddCell(celLogo);
                    //table.AddCell(celAcercade);
                    table.AddCell(celQR);
                    table.AddCell(celNumeroFactura);
                    return table;
                }
                public static PdfPTable DatosCliente(DataSet ds)
                {
                    //--------  Variables
                    string InvcHeadCustomerName = string.Empty;
                    string CustomerResaleID = string.Empty;
                    string CustomerAddress1 = string.Empty;
                    string CustomerPhoneNum = string.Empty;
                    string CustomerCity = string.Empty;
                    string CustomerCountry = string.Empty;
                    string InvcHeadCharacter01 = string.Empty;
                    string InvcHeadCharacter02 = string.Empty;
                    string InvcHeadCharacter03 = string.Empty;
                    string InvcHeadInvoiceRef = string.Empty;
                    string InvcDtlDiscountPercent = string.Empty;
                    //Asignacion de valores a variables
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "CustomerName", 0))
                        InvcHeadCustomerName = ds.Tables["InvcHead"].Rows[0]["CustomerName"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "Customer", "ResaleID", 0))
                        CustomerResaleID = ds.Tables["Customer"].Rows[0]["ResaleID"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "Customer", "Address1", 0))
                        CustomerAddress1 = ds.Tables["Customer"].Rows[0]["Address1"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "Customer", "PhoneNum", 0))
                        CustomerPhoneNum = ds.Tables["Customer"].Rows[0]["PhoneNum"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "Customer", "City", 0))
                        CustomerCity = ds.Tables["Customer"].Rows[0]["City"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "Customer", "Country", 0))
                        CustomerCountry = ds.Tables["Customer"].Rows[0]["Country"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "Character01", 0))
                        InvcHeadCharacter01 = ds.Tables["InvcHead"].Rows[0]["Character01"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "Character02", 0))
                        InvcHeadCharacter02 = ds.Tables["InvcHead"].Rows[0]["Character02"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "Character03", 0))
                        InvcHeadCharacter03 = ds.Tables["InvcHead"].Rows[0]["Character03"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "InvoiceRef", 0))
                        InvcHeadInvoiceRef = ds.Tables["InvcHead"].Rows[0]["InvoiceRef"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcDtl", "DiscountPercent", 0))
                        InvcDtlDiscountPercent = ds.Tables["InvcDtl"].Rows[0]["DiscountPercent"].ToString();

                    //Fuentes----------------------------------------------------------------------------------------------------------
                    iTextSharp.text.Font fontTitleBold = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 8.5f, iTextSharp.text.Font.NORMAL);
                    iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA, 6f, iTextSharp.text.Font.NORMAL);
                    iTextSharp.text.Font fontCustom = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);
                    iTextSharp.text.Font resumenes = FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL);
                    iTextSharp.text.Font fontCustomBold = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 6, iTextSharp.text.Font.NORMAL);


                    //return Helpers.Plantilla_2.DatosCliente(ds);
                    #region FACTURAR
                    //------------------------------------------------------------------------------------------------
                    PdfPTable tableFacturar = new PdfPTable(3);
                    //Dimenciones.
                    float[] DimencionFacturar = new float[3];
                    DimencionFacturar[0] = 1.0F;//
                    DimencionFacturar[1] = 0.01F;//
                    DimencionFacturar[2] = 1.0F;//

                    tableFacturar.WidthPercentage = 100;
                    tableFacturar.SetWidths(DimencionFacturar);
                    //----------------------------------------------------------------------------------------------
                    PdfPTable tableFacturarA = new PdfPTable(2);
                    float[] DimencionFacturarA = new float[2];
                    DimencionFacturarA[0] = 0.8F;//
                    DimencionFacturarA[1] = 2.0F;//

                    tableFacturarA.WidthPercentage = 100;
                    tableFacturarA.SetWidths(DimencionFacturarA);

                    iTextSharp.text.pdf.PdfPCell celDatosFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase("FACTURAR A:\n", fontCustom));
                    celDatosFacturarA.Colspan = 2;
                    celDatosFacturarA.Padding = 3;
                    celDatosFacturarA.Border = 0;
                    celDatosFacturarA.BorderColorBottom = BaseColor.WHITE;
                    celDatosFacturarA.HorizontalAlignment = Element.ALIGN_CENTER;
                    celDatosFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                    tableFacturarA.AddCell(celDatosFacturarA);

                    iTextSharp.text.pdf.PdfPCell celTextClienteFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase("CLIENTE:", fontCustom));
                    celTextClienteFacturarA.Colspan = 1;
                    celTextClienteFacturarA.Padding = 3;
                    celTextClienteFacturarA.Border = 0;
                    celTextClienteFacturarA.HorizontalAlignment = Element.ALIGN_LEFT;
                    celTextClienteFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                    tableFacturarA.AddCell(celTextClienteFacturarA);

                    iTextSharp.text.pdf.PdfPCell celClienteFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase((string)ds.Tables["InvcHead"].Rows[0]["ContactName"],
                        fontCustom));
                    celClienteFacturarA.Colspan = 1;
                    celClienteFacturarA.Padding = 3;
                    celClienteFacturarA.Border = 0;
                    celClienteFacturarA.HorizontalAlignment = Element.ALIGN_LEFT;
                    celClienteFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                    tableFacturarA.AddCell(celClienteFacturarA);

                    iTextSharp.text.pdf.PdfPCell celTextNitFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase("NIT:", fontCustom));
                    celTextNitFacturarA.Colspan = 1;
                    celTextNitFacturarA.Padding = 3;
                    celTextNitFacturarA.Border = 0;
                    celTextNitFacturarA.HorizontalAlignment = Element.ALIGN_LEFT;
                    celTextNitFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                    tableFacturarA.AddCell(celTextNitFacturarA);

                    iTextSharp.text.pdf.PdfPCell celNitFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase((string)ds.Tables["Customer"].Rows[0]["CustID"],
                        fontCustom));
                    celNitFacturarA.Colspan = 1;
                    celNitFacturarA.Padding = 3;
                    celNitFacturarA.Border = 0;
                    celNitFacturarA.HorizontalAlignment = Element.ALIGN_LEFT;
                    celNitFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                    tableFacturarA.AddCell(celNitFacturarA);

                    iTextSharp.text.pdf.PdfPCell celTextDireccionFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase("DIRECCION:", fontCustom));
                    celTextDireccionFacturarA.Colspan = 1;
                    celTextDireccionFacturarA.Padding = 3;
                    celTextDireccionFacturarA.Border = 0;
                    celTextDireccionFacturarA.HorizontalAlignment = Element.ALIGN_LEFT;
                    celTextDireccionFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                    tableFacturarA.AddCell(celTextDireccionFacturarA);

                    iTextSharp.text.pdf.PdfPCell celDireccionFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                        CustomerAddress1, fontCustom));
                    celDireccionFacturarA.Colspan = 1;
                    celDireccionFacturarA.Padding = 3;
                    celDireccionFacturarA.Border = 0;
                    celDireccionFacturarA.HorizontalAlignment = Element.ALIGN_LEFT;
                    celDireccionFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                    tableFacturarA.AddCell(celDireccionFacturarA);

                    iTextSharp.text.pdf.PdfPCell celTextTelFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase("TELEFONO:", fontCustom));
                    celTextTelFacturarA.Colspan = 1;
                    celTextTelFacturarA.Padding = 3;
                    celTextTelFacturarA.Border = 0;
                    celTextTelFacturarA.HorizontalAlignment = Element.ALIGN_LEFT;
                    celTextTelFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                    tableFacturarA.AddCell(celTextTelFacturarA);

                    iTextSharp.text.pdf.PdfPCell celTelFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                        CustomerPhoneNum, fontCustom));
                    celTelFacturarA.Colspan = 1;
                    celTelFacturarA.Padding = 3;
                    celTelFacturarA.Border = 0;
                    celTelFacturarA.HorizontalAlignment = Element.ALIGN_LEFT;
                    celTelFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                    tableFacturarA.AddCell(celTelFacturarA);

                    iTextSharp.text.pdf.PdfPCell celTextCiudadFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase("CIUDAD:", fontCustom));
                    celTextCiudadFacturarA.Colspan = 1;
                    celTextCiudadFacturarA.Padding = 3;
                    celTextCiudadFacturarA.Border = 0;
                    celTextCiudadFacturarA.HorizontalAlignment = Element.ALIGN_LEFT;
                    celTextCiudadFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                    tableFacturarA.AddCell(celTextCiudadFacturarA);

                    iTextSharp.text.pdf.PdfPCell celCiudadFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                        CustomerCity, fontCustom));
                    celCiudadFacturarA.Colspan = 1;
                    celCiudadFacturarA.Padding = 3;
                    celCiudadFacturarA.Border = 0;
                    celCiudadFacturarA.HorizontalAlignment = Element.ALIGN_LEFT;
                    celCiudadFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                    tableFacturarA.AddCell(celCiudadFacturarA);

                    iTextSharp.text.pdf.PdfPCell celTextPaisFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase("SUCURSAL:", fontCustom));
                    celTextPaisFacturarA.Colspan = 1;
                    celTextPaisFacturarA.Padding = 3;
                    celTextPaisFacturarA.Border = 0;
                    celTextPaisFacturarA.HorizontalAlignment = Element.ALIGN_LEFT;
                    celTextPaisFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                    tableFacturarA.AddCell(celTextPaisFacturarA);

                    iTextSharp.text.pdf.PdfPCell celPaisFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                        CustomerCountry, fontCustom));
                    celPaisFacturarA.Colspan = 1;
                    celPaisFacturarA.Padding = 3;
                    celPaisFacturarA.Border = 0;
                    celPaisFacturarA.HorizontalAlignment = Element.ALIGN_LEFT;
                    celPaisFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                    tableFacturarA.AddCell(celPaisFacturarA);

                    iTextSharp.text.pdf.PdfPCell celTittleFacturarA = new iTextSharp.text.pdf.PdfPCell(tableFacturarA);
                    tableFacturar.AddCell(celTittleFacturarA);
                    //----------------------------------------------------------------------------------------------------------------------------

                    iTextSharp.text.pdf.PdfPCell celEspacio2 = new iTextSharp.text.pdf.PdfPCell(new Phrase(" ", fontCustom));
                    celEspacio2.Border = 0;
                    tableFacturar.AddCell(celEspacio2);

                    //------------------------------------------------------------------------------------------

                    PdfPTable tableDespacharA = new PdfPTable(2);
                    float[] DimencionDespacharA = new float[2];
                    DimencionDespacharA[0] = 0.8F;//
                    DimencionDespacharA[1] = 2.0F;//

                    tableDespacharA.WidthPercentage = 100;
                    tableDespacharA.SetWidths(DimencionDespacharA);

                    iTextSharp.text.pdf.PdfPCell celTittleDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase("  .   ", fontCustom));
                    celTittleDespacharA.Colspan = 2;
                    celTittleDespacharA.Padding = 3;
                    celTittleDespacharA.Border = 0;
                    celTittleDespacharA.BorderColorBottom = BaseColor.WHITE;
                    celTittleDespacharA.HorizontalAlignment = Element.ALIGN_CENTER;
                    celTittleDespacharA.VerticalAlignment = Element.ALIGN_TOP;
                    tableDespacharA.AddCell(celTittleDespacharA);

                    iTextSharp.text.pdf.PdfPCell celTextClienteDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase("ZONA:", fontCustom));
                    celTextClienteDespacharA.Colspan = 1;
                    celTextClienteDespacharA.Padding = 3;
                    celTextClienteDespacharA.Border = 0;
                    celTextClienteDespacharA.HorizontalAlignment = Element.ALIGN_LEFT;
                    celTextClienteDespacharA.VerticalAlignment = Element.ALIGN_TOP;
                    tableDespacharA.AddCell(celTextClienteDespacharA);

                    iTextSharp.text.pdf.PdfPCell celClienteDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                        InvcHeadCharacter01, fontCustom));
                    celClienteDespacharA.Colspan = 1;
                    celClienteDespacharA.Padding = 3;
                    celClienteDespacharA.Border = 0;
                    celClienteDespacharA.HorizontalAlignment = Element.ALIGN_LEFT;
                    celClienteDespacharA.VerticalAlignment = Element.ALIGN_TOP;
                    tableDespacharA.AddCell(celClienteDespacharA);

                    iTextSharp.text.pdf.PdfPCell celTextNitDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase("BODEGA:", fontCustom));
                    celTextNitDespacharA.Colspan = 1;
                    celTextNitDespacharA.Padding = 3;
                    celTextNitDespacharA.Border = 0;
                    celTextNitDespacharA.HorizontalAlignment = Element.ALIGN_LEFT;
                    celTextNitDespacharA.VerticalAlignment = Element.ALIGN_TOP;
                    tableDespacharA.AddCell(celTextNitDespacharA);

                    iTextSharp.text.pdf.PdfPCell celNitDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                        InvcHeadCharacter02, fontCustom));
                    celNitDespacharA.Colspan = 1;
                    celNitDespacharA.Padding = 3;
                    celNitDespacharA.Border = 0;
                    celNitDespacharA.HorizontalAlignment = Element.ALIGN_LEFT;
                    celNitDespacharA.VerticalAlignment = Element.ALIGN_TOP;
                    tableDespacharA.AddCell(celNitDespacharA);

                    iTextSharp.text.pdf.PdfPCell celTextDireccionDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase("PLUS:", fontCustom));
                    celTextDireccionDespacharA.Colspan = 1;
                    celTextDireccionDespacharA.Padding = 3;
                    celTextDireccionDespacharA.Border = 0;
                    celTextDireccionDespacharA.HorizontalAlignment = Element.ALIGN_LEFT;
                    celTextDireccionDespacharA.VerticalAlignment = Element.ALIGN_TOP;
                    tableDespacharA.AddCell(celTextDireccionDespacharA);

                    iTextSharp.text.pdf.PdfPCell celDireccionDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                        InvcHeadCharacter03, fontCustom));
                    celDireccionDespacharA.Colspan = 1;
                    celDireccionDespacharA.Padding = 3;
                    celDireccionDespacharA.Border = 0;
                    celDireccionDespacharA.HorizontalAlignment = Element.ALIGN_LEFT;
                    celDireccionDespacharA.VerticalAlignment = Element.ALIGN_TOP;
                    tableDespacharA.AddCell(celDireccionDespacharA);

                    iTextSharp.text.pdf.PdfPCell celTextTelDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase("DESCUENTO:", fontCustom));
                    celTextTelDespacharA.Colspan = 1;
                    celTextTelDespacharA.Padding = 3;
                    celTextTelDespacharA.Border = 0;
                    celTextTelDespacharA.HorizontalAlignment = Element.ALIGN_LEFT;
                    celTextTelDespacharA.VerticalAlignment = Element.ALIGN_TOP;
                    tableDespacharA.AddCell(celTextTelDespacharA);

                    iTextSharp.text.pdf.PdfPCell celTelDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                        InvcDtlDiscountPercent, fontCustom));
                    celTelDespacharA.Colspan = 1;
                    celTelDespacharA.Padding = 3;
                    celTelDespacharA.Border = 0;
                    celTelDespacharA.HorizontalAlignment = Element.ALIGN_LEFT;
                    celTelDespacharA.VerticalAlignment = Element.ALIGN_TOP;
                    tableDespacharA.AddCell(celTelDespacharA);

                    //iTextSharp.text.pdf.PdfPCell celTextCiudadDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase("IVA:", fontCustom));
                    //celTextCiudadDespacharA.Colspan = 1;
                    //celTextCiudadDespacharA.Padding = 3;
                    //celTextCiudadDespacharA.Border = 0;
                    //celTextCiudadDespacharA.HorizontalAlignment = Element.ALIGN_LEFT;
                    //celTextCiudadDespacharA.VerticalAlignment = Element.ALIGN_TOP;
                    //tableDespacharA.AddCell(celTextCiudadDespacharA);

                    //iTextSharp.text.pdf.PdfPCell celCiudadDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    //    (String)ds.Tables["SalesTRC"].Rows[0]["RateCode"], fontCustom));
                    //celCiudadDespacharA.Colspan = 1;
                    //celCiudadDespacharA.Padding = 3;
                    //celCiudadDespacharA.Border = 0;
                    //celCiudadDespacharA.HorizontalAlignment = Element.ALIGN_LEFT;
                    //celCiudadDespacharA.VerticalAlignment = Element.ALIGN_TOP;
                    //tableDespacharA.AddCell(celCiudadDespacharA);

                    iTextSharp.text.pdf.PdfPCell celTextPaisDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase(" "));
                    celTextPaisDespacharA.Colspan = 1;
                    celTextPaisDespacharA.Padding = 3;
                    celTextPaisDespacharA.Border = 0;
                    celTextPaisDespacharA.HorizontalAlignment = Element.ALIGN_LEFT;
                    celTextPaisDespacharA.VerticalAlignment = Element.ALIGN_TOP;
                    tableDespacharA.AddCell(celTextPaisDespacharA);

                    iTextSharp.text.pdf.PdfPCell celPaisDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase(" ", fontCustom));
                    celPaisDespacharA.Colspan = 1;
                    celPaisDespacharA.Padding = 3;
                    celPaisDespacharA.Border = 0;
                    celPaisDespacharA.HorizontalAlignment = Element.ALIGN_LEFT;
                    celPaisDespacharA.VerticalAlignment = Element.ALIGN_TOP;
                    tableDespacharA.AddCell(celPaisDespacharA);

                    iTextSharp.text.pdf.PdfPCell celDatosDespacharA = new iTextSharp.text.pdf.PdfPCell(tableDespacharA);
                    tableFacturar.AddCell(celDatosDespacharA);
                    //-------------------------------------------------------------------------------------- 

                    return tableFacturar;
                    #endregion
                }
                public static PdfPTable DatosFactura(DataSet ds)
                {
                    //--------- variables ---------
                    string InvcHeadSalesRepName1 = string.Empty;
                    string InvcHeadPONum = string.Empty;
                    string InvcHeadOrderNum = string.Empty;
                    string InvcHeadTermsCodeDescription = string.Empty;
                    string InvcHeadInvoiceDate = string.Empty;
                    string InvcHeadDueDate = string.Empty;
                    string InvcHeadCurrencyCode = string.Empty;
                    //---- Asignar valores a las variables ------
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "SalesRepName1", 0))
                        InvcHeadSalesRepName1 = ds.Tables["InvcHead"].Rows[0]["SalesRepName1"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "PONum", 0))
                        InvcHeadPONum = ds.Tables["InvcHead"].Rows[0]["PONum"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "OrderNum", 0))
                        InvcHeadOrderNum = ds.Tables["InvcHead"].Rows[0]["OrderNum"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "TermsCodeDescription", 0))
                        InvcHeadTermsCodeDescription = ds.Tables["InvcHead"].Rows[0]["TermsCodeDescription"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "InvoiceDate", 0))
                        InvcHeadInvoiceDate = ds.Tables["InvcHead"].Rows[0]["InvoiceDate"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "DueDate", 0))
                        InvcHeadDueDate = ds.Tables["InvcHead"].Rows[0]["DueDate"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "CurrencyCode", 0))
                        InvcHeadCurrencyCode = ds.Tables["InvcHead"].Rows[0]["CurrencyCode"].ToString();
                    //-------------------------------------

                    PdfPTable table = new PdfPTable(1);
                    table.WidthPercentage = 100;
                    #region Tabla de Detalles
                    //------------------------------------------------------------------------------------------------
                    PdfPTable tableDetalles = new PdfPTable(5);
                    tableDetalles.PaddingTop = 20;
                    //Dimenciones.
                    float[] DimencionDetalles = new float[5];
                    DimencionDetalles[0] = 2.5F;//
                    DimencionDetalles[1] = 1.0F;//
                    DimencionDetalles[2] = 1.0F;//
                    DimencionDetalles[3] = 1.0F;//
                    DimencionDetalles[4] = 0.8F;//

                    tableDetalles.WidthPercentage = 100;
                    tableDetalles.SetWidths(DimencionDetalles);

                    iTextSharp.text.pdf.PdfPCell celVendedor = new iTextSharp.text.pdf.PdfPCell(new Phrase("VENDEDOR", Helpers.Fuentes.Plantilla_2_fontTitleFactura));
                    celVendedor.Colspan = 1;
                    celVendedor.Padding = 3;
                    //celVendedor.Border = 0;
                    celVendedor.BorderColorBottom = BaseColor.WHITE;
                    celVendedor.HorizontalAlignment = Element.ALIGN_CENTER;
                    celVendedor.VerticalAlignment = Element.ALIGN_TOP;
                    tableDetalles.AddCell(celVendedor);

                    iTextSharp.text.pdf.PdfPCell celOC_Cliente = new iTextSharp.text.pdf.PdfPCell(new Phrase("O.C. CLIENTE", Helpers.Fuentes.Plantilla_2_fontTitleFactura));
                    celOC_Cliente.Colspan = 1;
                    celOC_Cliente.Padding = 3;
                    //celVendedor.Border = 0;
                    celOC_Cliente.BorderColorBottom = BaseColor.WHITE;
                    celOC_Cliente.HorizontalAlignment = Element.ALIGN_CENTER;
                    celOC_Cliente.VerticalAlignment = Element.ALIGN_TOP;
                    tableDetalles.AddCell(celOC_Cliente);

                    iTextSharp.text.pdf.PdfPCell celOrdenVenta = new iTextSharp.text.pdf.PdfPCell(new Phrase("AVISO DE RECIBO", Helpers.Fuentes.Plantilla_2_fontTitleFactura));
                    celOrdenVenta.Colspan = 1;
                    celOrdenVenta.Padding = 3;
                    //celVendedor.Border = 0;
                    celOrdenVenta.BorderColorBottom = BaseColor.WHITE;
                    celOrdenVenta.HorizontalAlignment = Element.ALIGN_CENTER;
                    celOrdenVenta.VerticalAlignment = Element.ALIGN_TOP;
                    tableDetalles.AddCell(celOrdenVenta);

                    iTextSharp.text.pdf.PdfPCell celTerminoPago = new iTextSharp.text.pdf.PdfPCell(new Phrase("TÉRMINO DE PAGO", Helpers.Fuentes.Plantilla_2_fontTitleFactura));
                    celTerminoPago.Colspan = 1;
                    celTerminoPago.Padding = 3;
                    //celVendedor.Border = 0;
                    celTerminoPago.BorderColorBottom = BaseColor.WHITE;
                    celTerminoPago.HorizontalAlignment = Element.ALIGN_CENTER;
                    celTerminoPago.VerticalAlignment = Element.ALIGN_TOP;
                    tableDetalles.AddCell(celTerminoPago);

                    iTextSharp.text.pdf.PdfPCell celRemision = new iTextSharp.text.pdf.PdfPCell(new Phrase("REMISIÓN", Helpers.Fuentes.Plantilla_2_fontTitleFactura));
                    celRemision.Colspan = 1;
                    celRemision.Padding = 3;
                    //celVendedor.Border = 0;
                    celRemision.BorderColorBottom = BaseColor.WHITE;
                    celRemision.HorizontalAlignment = Element.ALIGN_CENTER;
                    celRemision.VerticalAlignment = Element.ALIGN_TOP;
                    tableDetalles.AddCell(celRemision);

                    iTextSharp.text.pdf.PdfPCell celDataVendedor = new iTextSharp.text.pdf.PdfPCell(new Phrase(InvcHeadSalesRepName1, Helpers.Fuentes.Plantilla_2_fontCustom));
                    celDataVendedor.Colspan = 1;
                    celDataVendedor.Padding = 3;
                    //celVendedor.Border = 0;
                    celDataVendedor.BorderColorBottom = BaseColor.WHITE;
                    celDataVendedor.HorizontalAlignment = Element.ALIGN_CENTER;
                    celDataVendedor.VerticalAlignment = Element.ALIGN_TOP;
                    tableDetalles.AddCell(celDataVendedor);

                    iTextSharp.text.pdf.PdfPCell celDataOC_Cliente = new iTextSharp.text.pdf.PdfPCell(new Phrase(InvcHeadPONum, Helpers.Fuentes.Plantilla_2_fontCustom));
                    celDataOC_Cliente.Colspan = 1;
                    celDataOC_Cliente.Padding = 3;
                    //celVendedor.Border = 0;
                    celDataOC_Cliente.BorderColorBottom = BaseColor.WHITE;
                    celDataOC_Cliente.HorizontalAlignment = Element.ALIGN_CENTER;
                    celDataOC_Cliente.VerticalAlignment = Element.ALIGN_TOP;
                    tableDetalles.AddCell(celDataOC_Cliente);

                    iTextSharp.text.pdf.PdfPCell celDataOrdenVenta = new iTextSharp.text.pdf.PdfPCell(new Phrase(InvcHeadOrderNum, Helpers.Fuentes.Plantilla_2_fontCustom));
                    celDataOrdenVenta.Colspan = 1;
                    celDataOrdenVenta.Padding = 3;
                    //celVendedor.Border = 0;
                    celDataOrdenVenta.BorderColorBottom = BaseColor.WHITE;
                    celDataOrdenVenta.HorizontalAlignment = Element.ALIGN_CENTER;
                    celDataOrdenVenta.VerticalAlignment = Element.ALIGN_TOP;
                    tableDetalles.AddCell(celDataOrdenVenta);

                    iTextSharp.text.pdf.PdfPCell celDataTerminoPago = new iTextSharp.text.pdf.PdfPCell(new Phrase(InvcHeadTermsCodeDescription, Helpers.Fuentes.Plantilla_2_fontCustom));
                    celDataTerminoPago.Colspan = 1;
                    celDataTerminoPago.Padding = 3;
                    //celVendedor.Border = 0;
                    celDataTerminoPago.BorderColorBottom = BaseColor.WHITE;
                    celDataTerminoPago.HorizontalAlignment = Element.ALIGN_CENTER;
                    celDataTerminoPago.VerticalAlignment = Element.ALIGN_TOP;
                    tableDetalles.AddCell(celDataTerminoPago);

                    iTextSharp.text.pdf.PdfPCell celDataRemision = new iTextSharp.text.pdf.PdfPCell(new Phrase("0", Helpers.Fuentes.Plantilla_2_fontCustom));
                    celDataRemision.Colspan = 1;
                    celDataRemision.Padding = 3;
                    //celVendedor.Border = 0;
                    celDataRemision.BorderColorBottom = BaseColor.WHITE;
                    celDataRemision.HorizontalAlignment = Element.ALIGN_CENTER;
                    celDataRemision.VerticalAlignment = Element.ALIGN_TOP;
                    tableDetalles.AddCell(celDataRemision);

                    //-------------------------------------------------------------------

                    PdfPTable tableDetalles2 = new PdfPTable(3);
                    tableDetalles2.WidthPercentage = 100;

                    PdfPTable tableFechaFactura = new PdfPTable(2);
                    float[] dimecionesTablaFecha = new float[2];
                    dimecionesTablaFecha[0] = 0.7F;
                    dimecionesTablaFecha[1] = 1.0F;
                    tableFechaFactura.SetWidths(dimecionesTablaFecha);

                    iTextSharp.text.pdf.PdfPCell celTextFechaFactura = new iTextSharp.text.pdf.PdfPCell(new Phrase("FECHA FACTURA: ", Helpers.Fuentes.Plantilla_2_fontTitle));
                    celTextFechaFactura.Colspan = 1;
                    celTextFechaFactura.Padding = 3;
                    celTextFechaFactura.Border = 0;
                    celTextFechaFactura.BorderColorBottom = BaseColor.WHITE;
                    celTextFechaFactura.HorizontalAlignment = Element.ALIGN_CENTER;
                    celTextFechaFactura.VerticalAlignment = Element.ALIGN_CENTER;
                    tableFechaFactura.AddCell(celTextFechaFactura);

                    iTextSharp.text.pdf.PdfPCell celFechaFactura = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:dd/MM/yyyy}", DateTime.Parse(InvcHeadInvoiceDate)),
                    Helpers.Fuentes.Plantilla_2_fontCustom));
                    celFechaFactura.Colspan = 1;
                    celFechaFactura.Padding = 3;
                    celFechaFactura.PaddingTop = 4;
                    celFechaFactura.Border = 0;
                    celFechaFactura.BorderColorBottom = BaseColor.WHITE;
                    celFechaFactura.HorizontalAlignment = Element.ALIGN_LEFT;
                    celFechaFactura.VerticalAlignment = Element.ALIGN_CENTER;
                    tableFechaFactura.AddCell(celFechaFactura);

                    PdfPCell _celFechaFactura = new PdfPCell(tableFechaFactura);
                    tableDetalles2.AddCell(_celFechaFactura);


                    PdfPTable tableFechaVencimiento = new PdfPTable(2);
                    float[] dmTablaFechaVencimiento = new float[2];
                    dmTablaFechaVencimiento[0] = 1.0F;
                    dmTablaFechaVencimiento[1] = 1.0F;
                    tableFechaVencimiento.SetWidths(dmTablaFechaVencimiento);

                    iTextSharp.text.pdf.PdfPCell celTextFechaVencimiento = new iTextSharp.text.pdf.PdfPCell(new Phrase("FECHA VENCIMIENTO: ", Helpers.Fuentes.Plantilla_2_fontTitle));
                    celTextFechaVencimiento.Colspan = 1;
                    celTextFechaVencimiento.Padding = 3;
                    celTextFechaVencimiento.Border = 0;
                    celTextFechaVencimiento.HorizontalAlignment = Element.ALIGN_LEFT;
                    celTextFechaVencimiento.VerticalAlignment = Element.ALIGN_TOP;
                    tableFechaVencimiento.AddCell(celTextFechaVencimiento);

                    iTextSharp.text.pdf.PdfPCell celFechaVencimiento = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:dd/MM/yyyy}",  DateTime.Parse(InvcHeadDueDate)),
                    Helpers.Fuentes.Plantilla_2_fontCustom));
                    celFechaVencimiento.Colspan = 1;
                    celFechaVencimiento.Padding = 4;
                    celFechaVencimiento.Border = 0;
                    celFechaVencimiento.BorderColorBottom = BaseColor.WHITE;
                    celFechaVencimiento.HorizontalAlignment = Element.ALIGN_LEFT;
                    celFechaVencimiento.VerticalAlignment = Element.ALIGN_TOP;
                    tableFechaVencimiento.AddCell(celFechaVencimiento);

                    PdfPCell _celFechaFacturaVencimiento = new PdfPCell(tableFechaVencimiento);
                    tableDetalles2.AddCell(_celFechaFacturaVencimiento);

                    PdfPTable tableMoneda = new PdfPTable(2);
                    float[] dimecionesTablaMoneda = new float[2];
                    dimecionesTablaMoneda[0] = 0.4F;
                    dimecionesTablaMoneda[1] = 1.0F;
                    tableMoneda.SetWidths(dimecionesTablaMoneda);

                    iTextSharp.text.pdf.PdfPCell celTextMoneda = new iTextSharp.text.pdf.PdfPCell(new Phrase("MONEDA: ", Helpers.Fuentes.Plantilla_2_fontTitle));
                    celTextMoneda.Colspan = 1;
                    celTextMoneda.Padding = 3;
                    celTextMoneda.Border = 0;
                    celTextMoneda.BorderColorBottom = BaseColor.WHITE;
                    celTextMoneda.HorizontalAlignment = Element.ALIGN_LEFT;
                    celTextMoneda.VerticalAlignment = Element.ALIGN_TOP;
                    tableMoneda.AddCell(celTextMoneda);

                    iTextSharp.text.pdf.PdfPCell celMoneda = new iTextSharp.text.pdf.PdfPCell(new Phrase(InvcHeadCurrencyCode,
                    Helpers.Fuentes.Plantilla_2_fontCustom));
                    celMoneda.Colspan = 1;
                    celMoneda.Padding = 4;
                    celMoneda.Border = 0;
                    celMoneda.BorderColorBottom = BaseColor.WHITE;
                    celMoneda.HorizontalAlignment = Element.ALIGN_LEFT;
                    celMoneda.VerticalAlignment = Element.ALIGN_TOP;
                    tableMoneda.AddCell(celMoneda);

                    PdfPCell _celMoneda = new PdfPCell(tableMoneda);
                    tableDetalles2.AddCell(_celMoneda);

                    #endregion
                    PdfPCell celDetalles = new PdfPCell();
                    celDetalles.Border = 0;
                    celDetalles.Padding = 0;
                    celDetalles.AddElement(tableDetalles);
                    PdfPCell celDetalles2 = new PdfPCell();
                    celDetalles2.Border = 0;
                    celDetalles2.Padding = 0;
                    celDetalles2.AddElement(tableDetalles2);

                    table.AddCell(celDetalles);
                    table.AddCell(celDetalles2);
                    return table;
                }
                public static PdfPTable DetalleHead(float min)
                {
                    PdfPTable tableTituloUnidades = new PdfPTable(8);

                    tableTituloUnidades.WidthPercentage = 100;
                    tableTituloUnidades.SetWidths(new float[] { 1.0f, 1f, 3.0f, 1.0f, 1f, 1.0f, 1.5f, 1f });

                    Helpers.Plantilla_2.DetalleHeadTitulos(ref tableTituloUnidades, "CÓD BARRAS", false);
                    Helpers.Plantilla_2.DetalleHeadTitulos(ref tableTituloUnidades, "PLU", false);
                    Helpers.Plantilla_2.DetalleHeadTitulos(ref tableTituloUnidades, "DESCRIPCIÓN", false);
                    Helpers.Plantilla_2.DetalleHeadTitulos(ref tableTituloUnidades, "CANTIDAD", false);
                    Helpers.Plantilla_2.DetalleHeadTitulos(ref tableTituloUnidades, "VALOR", false);
                    Helpers.Plantilla_2.DetalleHeadTitulos(ref tableTituloUnidades, "DESCUENTO", false);
                    Helpers.Plantilla_2.DetalleHeadTitulos(ref tableTituloUnidades, "VALOR TOTAL", true);
                    Helpers.Plantilla_2.DetalleHeadTitulos(ref tableTituloUnidades, "IVA", false);
                    //--- agregando las celdas vacias ---------------------
                    Helpers.Plantilla_2.DetalleHeadTituloCel(ref tableTituloUnidades, false, min);
                    Helpers.Plantilla_2.DetalleHeadTituloCel(ref tableTituloUnidades, false, min);
                    Helpers.Plantilla_2.DetalleHeadTituloCel(ref tableTituloUnidades, false, min);
                    Helpers.Plantilla_2.DetalleHeadTituloCel(ref tableTituloUnidades, false, min);
                    Helpers.Plantilla_2.DetalleHeadTituloCel(ref tableTituloUnidades, false, min);
                    Helpers.Plantilla_2.DetalleHeadTituloCel(ref tableTituloUnidades, false, min);
                    Helpers.Plantilla_2.DetalleHeadTituloCel(ref tableTituloUnidades, false, min);
                    Helpers.Plantilla_2.DetalleHeadTituloCel(ref tableTituloUnidades, true, min);

                    return tableTituloUnidades;
                }
                public static PdfPTable PiePagina(DataSet ds)
                {
                    string InvcHeadNumber01 = "0";
                    string InvcHeadNumber02 = "0";
                    string InvcHeadNumber03 = "0";
                    string InvcHeadCharacter04 = string.Empty;
                    string ObservacionCliente = string.Empty;
                    string InvcHeadDocTaxAmt = "0";
                    string InvcHeadLeyenda = "**NO SE ACEPTAN DEVOLUCIONES DESPUES DE 48 HORAS DE RECIBIDO * *";

                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "Number01", 0))
                        InvcHeadNumber01 = ds.Tables["InvcHead"].Rows[0]["Number01"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "Character04", 0))
                        InvcHeadCharacter04 = ds.Tables["InvcHead"].Rows[0]["Character04"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "ObservacionCliente_c", 0))
                        ObservacionCliente = ds.Tables["InvcHead"].Rows[0]["ObservacionCliente_c"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "Number02", 0))
                        InvcHeadNumber02 = ds.Tables["InvcHead"].Rows[0]["Number02"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "Number03", 0))
                        InvcHeadNumber03 = ds.Tables["InvcHead"].Rows[0]["Number03"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "DocTaxAmt", 0))
                        InvcHeadDocTaxAmt = ds.Tables["InvcHead"].Rows[0]["DocTaxAmt"].ToString();

                    PdfPTable tabla = new PdfPTable(1);
                    tabla.WidthPercentage = 100;
                    //------**********************************************************************************************************
                    iTextSharp.text.pdf.PdfPCell celEspacio2 = new iTextSharp.text.pdf.PdfPCell(new Phrase(" ", Helpers.Fuentes.Plantilla_2_fontTitleFactura));
                    celEspacio2.Border = 0;
                    PdfPCell celEspacio3 = new PdfPCell(new Phrase("  ", Helpers.Fuentes.Plantilla_2_fontEspacio));
                    celEspacio3.Border = 0;
                    PdfPTable tableTotalesObs = new PdfPTable(3);

                    tableTotalesObs.WidthPercentage = 100;
                    tableTotalesObs.SetWidths(new float[] { 2.5f, 0.01f, 1.0f });

                    PdfPTable _tableObs = new PdfPTable(1);
                    _tableObs.WidthPercentage = 100;

                    iTextSharp.text.pdf.PdfPCell celValorLetras = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("VALORES EN LETRA: {0}", Helpers.Compartido.
                        Nroenletras(ds.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"].ToString())), Helpers.Fuentes.Plantilla_2_fontTitleFactura));
                    celValorLetras.Colspan = 1;
                    celValorLetras.Padding = 3;
                    //celValorLetras.Border = 0;
                    celValorLetras.BorderColorBottom = BaseColor.WHITE;
                    celValorLetras.HorizontalAlignment = Element.ALIGN_LEFT;
                    celValorLetras.VerticalAlignment = Element.ALIGN_TOP;

                    //iTextSharp.text.pdf.PdfPCell celValorAsegurado = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("VALOR ASEGURADO: {0:C2}",
                    //   decimal.Parse(ds.Tables["InvcHead"].Rows[0]["DspDocSubTotal"].ToString())), Helpers.Fuentes.Plantilla_2_fontTitle2));
                    //celValorAsegurado.Colspan = 1;
                    //celValorAsegurado.Padding = 5;
                    ////celValorLetras.Border = 0;
                    //celValorAsegurado.BorderColorBottom = BaseColor.WHITE;
                    //celValorAsegurado.HorizontalAlignment = Element.ALIGN_LEFT;
                    //celValorAsegurado.VerticalAlignment = Element.ALIGN_TOP;

                    Chunk chkObservaciones = new Chunk(string.Format("OBSERVACIONES:\n{0}", ObservacionCliente), Helpers.Fuentes.Plantilla_2_fontTitleFactura);
                    Chunk chkDevolucion = new Chunk("\n" + InvcHeadLeyenda, Helpers.Fuentes.Arial7Negrilla);
                    //Chunk chkResolucion = new Chunk(InvcHeadCharacter04, Helpers.Fuentes.Arial7Negrilla);

                    Paragraph prgObservaciones = new Paragraph();
                    prgObservaciones.Add(chkObservaciones);
                    prgObservaciones.Add(chkDevolucion);
                    //prgObservaciones.Add(chkResponsabilidadTributaria);
                    //prgObservaciones.Add(chkResolucion);

                    //iTextSharp.text.pdf.PdfPCell celObservaciones = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("OBSERVACIONES:\n{0}",
                    //    ObservacionCliente), Helpers.Fuentes.Plantilla_2_fontTitleFactura));
                    PdfPCell celObservaciones = new PdfPCell();
                    celObservaciones.AddElement(prgObservaciones);
                    celObservaciones.Colspan = 1;
                    celObservaciones.Padding = 3;
                    //celValorLetras.Border = 0;
                    celObservaciones.BorderColorBottom = BaseColor.WHITE;
                    celObservaciones.HorizontalAlignment = Element.ALIGN_LEFT;
                    celObservaciones.VerticalAlignment = Element.ALIGN_TOP;

                    _tableObs.AddCell(celValorLetras);
                    _tableObs.AddCell(celEspacio3);
                    //_tableObs.AddCell(celValorAsegurado);
                    _tableObs.AddCell(celEspacio3);
                    _tableObs.AddCell(celObservaciones);

                    iTextSharp.text.pdf.PdfPCell _celObs = new iTextSharp.text.pdf.PdfPCell(_tableObs);
                    _celObs.Border = 0;
                    tableTotalesObs.AddCell(_celObs);
                    //----------------------------------------------------------------------------------------------------------------------------

                    iTextSharp.text.pdf.PdfPCell _celEspacio2 = new iTextSharp.text.pdf.PdfPCell(new Phrase(" ", Helpers.Fuentes.Plantilla_2_fontTitleFactura));
                    _celEspacio2.Border = 0;
                    tableTotalesObs.AddCell(_celEspacio2);

                    //------------------------------------------------------------------------------------------

                    PdfPTable _tableTotales = new PdfPTable(2);
                    float[] _DimencionTotales = new float[2];
                    _DimencionTotales[0] = 1.9F;//
                    _DimencionTotales[1] = 2.1F;//

                    _tableTotales.WidthPercentage = 100;
                    _tableTotales.SetWidths(_DimencionTotales);

                    iTextSharp.text.pdf.PdfPCell _celTextSubTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase("SUBTOTAL:", Helpers.Fuentes.Plantilla_2_fontTitleFactura));
                    _celTextSubTotal.Colspan = 1;
                    _celTextSubTotal.Padding = 3;
                    _celTextSubTotal.PaddingBottom = 5;
                    _celTextSubTotal.Border = 0;
                    _celTextSubTotal.BorderWidthRight = 1;
                    _celTextSubTotal.BorderColorBottom = BaseColor.WHITE;
                    _celTextSubTotal.HorizontalAlignment = Element.ALIGN_LEFT;
                    _celTextSubTotal.VerticalAlignment = Element.ALIGN_TOP;
                    _tableTotales.AddCell(_celTextSubTotal);

                    iTextSharp.text.pdf.PdfPCell _celSubTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:C2}",
                        decimal.Parse(ds.Tables["InvcHead"].Rows[0]["DspDocSubTotal"].ToString())), Helpers.Fuentes.Plantilla_2_fontTitle2));
                    _celSubTotal.Colspan = 1;
                    _celSubTotal.Padding = 3;
                    _celSubTotal.PaddingBottom = 5;
                    _celSubTotal.Border = 0;
                    _celSubTotal.BorderColorBottom = BaseColor.WHITE;
                    _celSubTotal.HorizontalAlignment = Element.ALIGN_RIGHT;
                    _celSubTotal.VerticalAlignment = Element.ALIGN_TOP;
                    _tableTotales.AddCell(_celSubTotal);

                    iTextSharp.text.pdf.PdfPCell _celTextDescuentos = new iTextSharp.text.pdf.PdfPCell(new Phrase("DESCUENTOS:", Helpers.Fuentes.Plantilla_2_fontTitleFactura));
                    _celTextDescuentos.Colspan = 1;
                    _celTextDescuentos.Padding = 3;
                    _celTextDescuentos.PaddingBottom = 5;
                    _celTextDescuentos.Border = 0;
                    _celTextDescuentos.BorderWidthRight = 1;
                    _celTextDescuentos.BorderColorBottom = BaseColor.WHITE;
                    _celTextDescuentos.HorizontalAlignment = Element.ALIGN_LEFT;
                    _celTextDescuentos.VerticalAlignment = Element.ALIGN_TOP;
                    _tableTotales.AddCell(_celTextDescuentos);

                    iTextSharp.text.pdf.PdfPCell _celDescuentos = new iTextSharp.text.pdf.PdfPCell(new Phrase(InvcHeadNumber01, Helpers.Fuentes.Plantilla_2_fontTitle2));
                    _celDescuentos.Colspan = 1;
                    _celDescuentos.Padding = 3;
                    _celDescuentos.PaddingBottom = 5;
                    _celDescuentos.Border = 0;
                    _celDescuentos.BorderColorBottom = BaseColor.WHITE;
                    _celDescuentos.HorizontalAlignment = Element.ALIGN_RIGHT;
                    _celDescuentos.VerticalAlignment = Element.ALIGN_TOP;
                    _tableTotales.AddCell(_celDescuentos);

                    iTextSharp.text.pdf.PdfPCell _celTextAntesIVA = new iTextSharp.text.pdf.PdfPCell(new Phrase("VALOR ANTES IVA:", Helpers.Fuentes.Plantilla_2_fontTitleFactura));
                    _celTextAntesIVA.Colspan = 1;
                    _celTextAntesIVA.Padding = 3;
                    _celTextAntesIVA.PaddingBottom = 5;
                    _celTextAntesIVA.Border = 0;
                    _celTextAntesIVA.BorderWidthRight = 1;
                    _celTextAntesIVA.BorderColorBottom = BaseColor.WHITE;
                    _celTextAntesIVA.HorizontalAlignment = Element.ALIGN_LEFT;
                    _celTextAntesIVA.VerticalAlignment = Element.ALIGN_TOP;
                    _tableTotales.AddCell(_celTextAntesIVA);

                    iTextSharp.text.pdf.PdfPCell _celAntesIVA = new iTextSharp.text.pdf.PdfPCell(new Phrase(decimal.Parse(InvcHeadNumber02).ToString("C2"), Helpers.Fuentes.Plantilla_2_fontTitle2));
                    _celAntesIVA.Colspan = 1;
                    _celAntesIVA.Padding = 3;
                    _celAntesIVA.PaddingBottom = 5;
                    _celAntesIVA.Border = 0;
                    _celAntesIVA.BorderColorBottom = BaseColor.WHITE;
                    _celAntesIVA.HorizontalAlignment = Element.ALIGN_RIGHT;
                    _celAntesIVA.VerticalAlignment = Element.ALIGN_TOP;
                    _tableTotales.AddCell(_celAntesIVA);

                    iTextSharp.text.pdf.PdfPCell _celTextIva = new iTextSharp.text.pdf.PdfPCell(new Phrase("IVA:", Helpers.Fuentes.Plantilla_2_fontTitleFactura));
                    _celTextIva.Colspan = 1;
                    _celTextIva.Padding = 3;
                    _celTextIva.PaddingBottom = 5;
                    _celTextIva.Border = 0;
                    _celTextIva.BorderWidthRight = 1;
                    //_celTextIva.BorderColorBottom = BaseColor.BLACK;
                    _celTextIva.HorizontalAlignment = Element.ALIGN_LEFT;
                    _celTextIva.VerticalAlignment = Element.ALIGN_TOP;
                    _tableTotales.AddCell(_celTextIva);

                    iTextSharp.text.pdf.PdfPCell _celIva = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:C2}",
                        decimal.Parse(InvcHeadDocTaxAmt)), Helpers.Fuentes.Plantilla_2_fontTitle2));
                    _celIva.Colspan = 1;
                    _celIva.Padding = 3;
                    _celIva.PaddingBottom = 5;
                    _celIva.Border = 0;
                    //_celIva.BorderColorBottom = BaseColor.BLACK;
                    _celIva.HorizontalAlignment = Element.ALIGN_RIGHT;
                    _celIva.VerticalAlignment = Element.ALIGN_TOP;
                    _tableTotales.AddCell(_celIva);

                    iTextSharp.text.pdf.PdfPCell _celTextRetefuente = new iTextSharp.text.pdf.PdfPCell(new Phrase("RETEFUENTE:", Helpers.Fuentes.Plantilla_2_fontTitleFactura));
                    _celTextRetefuente.Colspan = 1;
                    _celTextRetefuente.Padding = 3;
                    _celTextRetefuente.PaddingBottom = 5;
                    _celTextRetefuente.Border = 0;
                    _celTextRetefuente.BorderWidthRight = 1;
                    _celTextRetefuente.BorderColorBottom = BaseColor.WHITE;
                    _celTextRetefuente.HorizontalAlignment = Element.ALIGN_LEFT;
                    _celTextRetefuente.VerticalAlignment = Element.ALIGN_TOP;
                    _tableTotales.AddCell(_celTextRetefuente);

                    iTextSharp.text.pdf.PdfPCell _celRetefuente = new iTextSharp.text.pdf.PdfPCell(new Phrase(decimal.Parse(InvcHeadNumber03).ToString("C2"), Helpers.Fuentes.Plantilla_2_fontTitle2));
                    _celRetefuente.Colspan = 1;
                    _celRetefuente.Padding = 3;
                    _celRetefuente.PaddingBottom = 5;
                    _celRetefuente.Border = 0;
                    _celRetefuente.BorderColorBottom = BaseColor.WHITE;
                    _celRetefuente.HorizontalAlignment = Element.ALIGN_RIGHT;
                    _celRetefuente.VerticalAlignment = Element.ALIGN_TOP;
                    _tableTotales.AddCell(_celRetefuente);


                    iTextSharp.text.pdf.PdfPCell _celTextTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase("TOTAL:", Helpers.Fuentes.Plantilla_2_fontTitleFactura));
                    _celTextTotal.Colspan = 1;
                    _celTextTotal.Padding = 3;
                    _celTextTotal.PaddingBottom = 5;
                    _celTextTotal.Border = 0;
                    _celTextTotal.BorderWidthRight = 1;
                    _celTextTotal.BorderWidthTop = 1;
                    _celTextTotal.HorizontalAlignment = Element.ALIGN_LEFT;
                    _celTextTotal.VerticalAlignment = Element.ALIGN_TOP;
                    _tableTotales.AddCell(_celTextTotal);

                    iTextSharp.text.pdf.PdfPCell _celTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:C2}",
                        decimal.Parse(ds.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"].ToString())), Helpers.Fuentes.Plantilla_2_fontTitleFactura));
                    _celTotal.Colspan = 1;
                    _celTotal.Padding = 3;
                    _celTotal.PaddingBottom = 5;
                    _celTotal.Border = 0;
                    _celTotal.BorderWidthTop = 1;
                    _celTotal.HorizontalAlignment = Element.ALIGN_RIGHT;
                    _celTotal.VerticalAlignment = Element.ALIGN_TOP;
                    _tableTotales.AddCell(_celTotal);

                    iTextSharp.text.pdf.PdfPCell _celTotales = new iTextSharp.text.pdf.PdfPCell(_tableTotales);
                    tableTotalesObs.AddCell(_celTotales);
                    //------------------------------------------------------------------------
                    PdfPTable tableResolucion = new PdfPTable(1);
                    tableResolucion.WidthPercentage = 100;
                    string strResolucion = InvcHeadCharacter04;

                    Chunk chkResponsabilidadTributaria = new Chunk(@"REGIMEN COMUN
GRANDES CONTRIBUYENTES SEGÚN RES. N° 000076 DEL 1 de diciembre de 2016
ACTIVIDAD ECONOMICA 4773/ TARIFA ICA 10 X MIL", Helpers.Fuentes.Arial7Negrilla);
                    Chunk chkResolucion = new Chunk(InvcHeadCharacter04, Helpers.Fuentes.Arial7Negrilla);


                    iTextSharp.text.pdf.PdfPCell _celResolucion = new iTextSharp.text.pdf.PdfPCell();
                    _celResolucion.AddElement(chkResponsabilidadTributaria);
                    _celResolucion.AddElement(chkResolucion);
                    _celResolucion.Colspan = 1;
                    _celResolucion.Padding = 3;
                    //_celResolucion.Border = 0;
                    _celResolucion.HorizontalAlignment = Element.ALIGN_JUSTIFIED_ALL;
                    _celResolucion.VerticalAlignment = Element.ALIGN_TOP;
                    tableResolucion.AddCell(_celResolucion);
                    //----------------------------------------------------------------------
                    PdfPTable tableFirmas = new PdfPTable(5);

                    float[] DimencionFirmas = new float[5];
                    DimencionFirmas[0] = 1.0F;//
                    DimencionFirmas[1] = 0.02F;//
                    DimencionFirmas[2] = 1.0F;//
                    DimencionFirmas[3] = 0.02F;//
                    DimencionFirmas[4] = 1.4F;//

                    tableFirmas.WidthPercentage = 100;
                    tableFirmas.SetWidths(DimencionFirmas);

                    PdfPTable tableDespacahdoPor = new PdfPTable(1);
                    tableDespacahdoPor.WidthPercentage = 100;
                    iTextSharp.text.pdf.PdfPCell celDespachadoPor = new iTextSharp.text.pdf.PdfPCell(new Phrase("\n\n\n\n\n\n\n\n usuario que genera la factura:" +
                        "\n______________________________________\n\nDESPACHADO POR", Helpers.Fuentes.Plantilla_2_fontTitleFactura));
                    celDespachadoPor.Colspan = 1;
                    celDespachadoPor.Padding = 3;
                    celDespachadoPor.HorizontalAlignment = Element.ALIGN_CENTER;
                    celDespachadoPor.VerticalAlignment = Element.ALIGN_TOP;
                    tableDespacahdoPor.AddCell(celDespachadoPor);

                    iTextSharp.text.pdf.PdfPCell _celDespachadoPor = new iTextSharp.text.pdf.PdfPCell(tableDespacahdoPor);
                    tableFirmas.AddCell(_celDespachadoPor);
                    //----------------------------------------------------------------------------------------------------------------------------

                    tableFirmas.AddCell(celEspacio2);

                    //------------------------------------------------------------------------------------------

                    PdfPTable tableFirmaConductor = new PdfPTable(1);
                    tableFirmaConductor.WidthPercentage = 100;
                    iTextSharp.text.pdf.PdfPCell celFirmaConductor = new iTextSharp.text.pdf.PdfPCell(new Phrase("\n\n\n\n\n\n\n\n\n " +
                        "______________________________________" +
                        "\n\nFIRMA DEL CONDUCTOR", Helpers.Fuentes.Plantilla_2_fontTitleFactura));
                    celFirmaConductor.Colspan = 1;
                    celFirmaConductor.Padding = 3;
                    //celFirmaConductor.Border = 0;
                    celFirmaConductor.HorizontalAlignment = Element.ALIGN_CENTER;
                    celFirmaConductor.VerticalAlignment = Element.ALIGN_TOP;
                    tableFirmaConductor.AddCell(celFirmaConductor);

                    iTextSharp.text.pdf.PdfPCell _celFirmaConductor = new iTextSharp.text.pdf.PdfPCell(tableFirmaConductor);
                    tableFirmas.AddCell(_celFirmaConductor);

                    //-------------------------------------------------------------------------------------
                    tableFirmas.AddCell(celEspacio2);
                    //-------------------------------------------------------------------------------------

                    PdfPTable tableSelloCliente = new PdfPTable(1);
                    //tableSelloCliente.WidthPercentage = 100;

                    iTextSharp.text.pdf.PdfPCell cel1SelloCliente = new iTextSharp.text.pdf.PdfPCell(new Phrase("Apreciado cliente este documento " +
                        "no es endolsable", Helpers.Fuentes.Plantilla_2_fontTitleFactura));
                    cel1SelloCliente.Colspan = 1;
                    cel1SelloCliente.Padding = 5;
                    cel1SelloCliente.Border = 1;
                    cel1SelloCliente.BorderWidthBottom = 1;
                    cel1SelloCliente.HorizontalAlignment = Element.ALIGN_LEFT;
                    cel1SelloCliente.VerticalAlignment = Element.ALIGN_TOP;
                    tableSelloCliente.AddCell(cel1SelloCliente);

                    iTextSharp.text.pdf.PdfPCell cel2SelloFechaRecibido = new iTextSharp.text.pdf.PdfPCell(new Phrase("Fecha de recibido:", Helpers.Fuentes.Plantilla_2_fontTitleFactura));

                    cel2SelloFechaRecibido.Colspan = 1;
                    cel2SelloFechaRecibido.Padding = 3;
                    cel2SelloFechaRecibido.Border = 0;
                    cel2SelloFechaRecibido.HorizontalAlignment = Element.ALIGN_LEFT;
                    cel2SelloFechaRecibido.VerticalAlignment = Element.ALIGN_TOP;
                    tableSelloCliente.AddCell(cel2SelloFechaRecibido);

                    iTextSharp.text.pdf.PdfPCell cel2SelloNombre = new iTextSharp.text.pdf.PdfPCell(new Phrase("Nombre:", Helpers.Fuentes.Plantilla_2_fontTitleFactura));

                    cel2SelloNombre.Colspan = 1;
                    cel2SelloNombre.Padding = 3;
                    cel2SelloNombre.Border = 0;
                    cel2SelloNombre.HorizontalAlignment = Element.ALIGN_LEFT;
                    cel2SelloNombre.VerticalAlignment = Element.ALIGN_TOP;
                    tableSelloCliente.AddCell(cel2SelloNombre);

                    iTextSharp.text.pdf.PdfPCell cel2SelloId = new iTextSharp.text.pdf.PdfPCell(new Phrase("Identificacion:", Helpers.Fuentes.Plantilla_2_fontTitleFactura));

                    cel2SelloId.Colspan = 1;
                    cel2SelloId.Padding = 3;
                    cel2SelloId.Border = 0;
                    cel2SelloId.HorizontalAlignment = Element.ALIGN_LEFT;
                    cel2SelloId.VerticalAlignment = Element.ALIGN_TOP;
                    tableSelloCliente.AddCell(cel2SelloId);

                    iTextSharp.text.pdf.PdfPCell cel2SelloFirma = new iTextSharp.text.pdf.PdfPCell(new Phrase("Firma:\n", Helpers.Fuentes.Plantilla_2_fontTitleFactura));

                    cel2SelloFirma.Colspan = 1;
                    cel2SelloFirma.Padding = 3;
                    cel2SelloFirma.Border = 0;
                    cel2SelloFirma.HorizontalAlignment = Element.ALIGN_LEFT;
                    cel2SelloFirma.VerticalAlignment = Element.ALIGN_TOP;
                    tableSelloCliente.AddCell(cel2SelloFirma);

                    iTextSharp.text.pdf.PdfPCell cel3SelloCliente = new iTextSharp.text.pdf.PdfPCell(new Phrase("______________________________________________________\n\n" +
                        "RECIBIDO CONFORME FIRMA Y SELLO DEL CLIENTE", Helpers.Fuentes.Plantilla_2_fontTitleFactura));

                    cel3SelloCliente.Colspan = 1;
                    cel3SelloCliente.Padding = 3;
                    cel3SelloCliente.Border = 0;
                    cel3SelloCliente.HorizontalAlignment = Element.ALIGN_CENTER;
                    cel3SelloCliente.VerticalAlignment = Element.ALIGN_TOP;
                    tableSelloCliente.AddCell(cel3SelloCliente);

                    iTextSharp.text.pdf.PdfPCell _celSelloCliente = new iTextSharp.text.pdf.PdfPCell(tableSelloCliente);
                    //_celSelloCliente.Border = 0;
                    //_celSelloCliente.BorderColor = BaseColor.WHITE;
                    tableFirmas.AddCell(_celSelloCliente);
                    //-----------------------------------------------------------------------------
                    PdfPCell celTableTotalesObs = new PdfPCell();
                    PdfPCell celResolucion = new PdfPCell();

                    PdfPCell celTableFirmas = new PdfPCell();

                    celTableTotalesObs.Border = 0;
                    celTableTotalesObs.AddElement(tableTotalesObs);
                    celResolucion.Border = 0;
                    celResolucion.AddElement(tableResolucion);
                    celTableFirmas.Border = 0;
                    celTableFirmas.AddElement(tableFirmas);

                    tabla.AddCell(celTableTotalesObs);
                    tabla.AddCell(celResolucion);
                    tabla.AddCell(celTableFirmas);

                    return tabla;
                }
                public static PdfPTable Detalles(DataSet ds)
                {
                    PdfPTable tableUnidades = new PdfPTable(8);
                    tableUnidades.WidthPercentage = 100;
                    tableUnidades.SetWidths(new float[] { 1.0f, 1f, 3.0f, 1.0f, 1f, 1.0f, 1.5f, 1f });

                    foreach (DataRow InvoiceLine in ds.Tables["InvcDtl"].Rows)
                    {
                        string PartNum = string.Empty;
                        string SalesUM = string.Empty;
                        string Character01 = string.Empty;
                        string Character02 = string.Empty;
                        string LineDesc = string.Empty;
                        string SellingShipQty = "0";
                        string UnitPrice = "0";
                        string DiscountPercent = "0";
                        string DocExtPrice = "0";
                        string Number01 = "0";

                        if (Helpers.Compartido.VerificarExistenciaColumnas(InvoiceLine, "SalesUM"))
                            SalesUM = InvoiceLine["SalesUM"].ToString();
                        if (Helpers.Compartido.VerificarExistenciaColumnas(InvoiceLine, "PartNum"))
                            PartNum = InvoiceLine["PartNum"].ToString();
                        if (Helpers.Compartido.VerificarExistenciaColumnas(InvoiceLine, "Character01"))
                            Character01 = InvoiceLine["Character01"].ToString();
                        if (Helpers.Compartido.VerificarExistenciaColumnas(InvoiceLine, "Character02"))
                            Character02 = InvoiceLine["Character02"].ToString();
                        if (Helpers.Compartido.VerificarExistenciaColumnas(InvoiceLine, "LineDesc"))
                            LineDesc = InvoiceLine["LineDesc"].ToString();
                        if (Helpers.Compartido.VerificarExistenciaColumnas(InvoiceLine, "SellingShipQty"))
                            SellingShipQty = InvoiceLine["SellingShipQty"].ToString();
                        if (Helpers.Compartido.VerificarExistenciaColumnas(InvoiceLine, "UnitPrice"))
                            UnitPrice = InvoiceLine["UnitPrice"].ToString();
                        if (Helpers.Compartido.VerificarExistenciaColumnas(InvoiceLine, "DiscountPercent"))
                            DiscountPercent = InvoiceLine["DiscountPercent"].ToString();
                        if (Helpers.Compartido.VerificarExistenciaColumnas(InvoiceLine, "DocExtPrice"))
                            DocExtPrice = InvoiceLine["DocExtPrice"].ToString();
                        if (Helpers.Compartido.VerificarExistenciaColumnas(InvoiceLine, "Number01"))
                            Number01 = InvoiceLine["Number01"].ToString();

                        Helpers.Plantilla_2.DetallesCeldas(ref tableUnidades, PartNum, Helpers.Fuentes.Plantilla_2_fontCustom, Element.ALIGN_RIGHT);
                        Helpers.Plantilla_2.DetallesCeldas(ref tableUnidades, Character01, Helpers.Fuentes.Plantilla_2_fontCustom, Element.ALIGN_RIGHT);
                        Helpers.Plantilla_2.DetallesCeldas(ref tableUnidades, LineDesc, Helpers.Fuentes.Plantilla_2_fontCustom, Element.ALIGN_RIGHT);
                        Helpers.Plantilla_2.DetallesCeldas(ref tableUnidades, string.Format("{0:N0}", SellingShipQty), Helpers.Fuentes.Plantilla_2_fontCustom, Element.ALIGN_RIGHT);
                        Helpers.Plantilla_2.DetallesCeldas(ref tableUnidades, string.Format("{0:C2}", Convert.ToDecimal(UnitPrice)), Helpers.Fuentes.Plantilla_2_fontCustom, Element.ALIGN_RIGHT);
                        Helpers.Plantilla_2.DetallesCeldas(ref tableUnidades, Character02, Helpers.Fuentes.Plantilla_2_fontCustom, Element.ALIGN_RIGHT);
                        Helpers.Plantilla_2.DetallesCeldas(ref tableUnidades, string.Format("{0:C2}", Convert.ToDecimal(DocExtPrice)), Helpers.Fuentes.Plantilla_2_fontCustom, Element.ALIGN_RIGHT);
                        Helpers.Plantilla_2.DetallesCeldas(ref tableUnidades, string.Format("{0:N0} %", Convert.ToDecimal(Number01)), Helpers.Fuentes.Plantilla_2_fontCustom, Element.ALIGN_RIGHT);
                    }
                    return tableUnidades;
                }
            }
        }
        public partial class Helpers
        {
            public class Fedco
            {
                public static Document Documento
                {
                    get
                    {
                        return new Document(iTextSharp.text.PageSize.LETTER, 20f, 15f, 295f, 255f);
                    }
                }
                public static void DetalleHeadTitulos(ref PdfPTable table, string Titulo, bool BordeDerecho)
                {
                    iTextSharp.text.pdf.PdfPCell celda = new iTextSharp.text.pdf.PdfPCell(new Phrase(Titulo, Helpers.Fuentes.Plantilla_2_fontTitleFactura));
                    celda.Colspan = 1;
                    celda.Padding = 3;
                    if (!BordeDerecho)
                        celda.BorderWidthRight = 0;
                    celda.HorizontalAlignment = Element.ALIGN_CENTER;
                    celda.VerticalAlignment = Element.ALIGN_TOP;
                    celda.BackgroundColor = BaseColor.LIGHT_GRAY;
                    table.AddCell(celda);
                }
                public static void AddUnidades(DataRow dataLine, ref PdfPTable pdfPTable, iTextSharp.text.Font font)
                {
                    //-----------------------------------------
                    string InvcDtlPartNum = string.Empty;
                    string InvcDtlLineDesc = string.Empty;
                    string InvcDtlSellingShipQty = "0";
                    string InvcDtlUnitPrice = "0";
                    string InvcDtlCharacter01 = string.Empty;
                    string InvcDtlDocDiscount = "0";
                    string InvcDtlDocLineTax = "0";
                    string InvcDtlDocExtPrice = "0";
                    //---Asignacion de valores
                    if (Helpers.Compartido.VerificarExistenciaColumnas(dataLine, "PartNum"))
                        InvcDtlPartNum = dataLine["PartNum"].ToString();
                    if(Helpers.Compartido.VerificarExistenciaColumnas(dataLine, "LineDesc"))
                        InvcDtlLineDesc = dataLine["LineDesc"].ToString();
                    if(Helpers.Compartido.VerificarExistenciaColumnas(dataLine, "SellingShipQty"))
                        InvcDtlSellingShipQty = dataLine["SellingShipQty"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(dataLine, "UnitPrice"))
                        InvcDtlUnitPrice = dataLine["UnitPrice"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(dataLine, "Character01"))
                        InvcDtlCharacter01 = dataLine["Character01"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(dataLine, "DocDiscount"))
                        InvcDtlDocDiscount = dataLine["DocDiscount"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(dataLine, "DocLineTax"))
                        InvcDtlDocLineTax = dataLine["DocLineTax"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(dataLine, "DocExtPrice"))
                        InvcDtlDocExtPrice = dataLine["DocExtPrice"].ToString();

                    //-----------------------------------------
                    Helpers.Plantilla_2.DetallesCeldas(ref pdfPTable, InvcDtlPartNum, font); //partnum
                    Helpers.Plantilla_2.DetallesCeldas(ref pdfPTable, InvcDtlLineDesc, font); //descripcion
                    Helpers.Plantilla_2.DetallesCeldas(ref pdfPTable, string.Format("{0:N0}", InvcDtlSellingShipQty), font); //cantidad
                    //Helpers.Plantilla_2.DetallesCeldas(ref pdfPTable, dataLine["SalesUM"].ToString(), font); //unidad
                    Helpers.Plantilla_2.DetallesCeldas(ref pdfPTable, string.Format("{0:C2}", Convert.ToDecimal(InvcDtlUnitPrice)), font); //valor unitario
                    Helpers.Plantilla_2.DetallesCeldas(ref pdfPTable, InvcDtlCharacter01, font); //PLU
                    Helpers.Plantilla_2.DetallesCeldas(ref pdfPTable, string.Format("{0:C2}", decimal.Parse(InvcDtlDocDiscount)), font); //descuento
                    Helpers.Plantilla_2.DetallesCeldas(ref pdfPTable, string.Format("{0:C2}", decimal.Parse(InvcDtlDocLineTax)), font); //IVA
                    Helpers.Plantilla_2.DetallesCeldas(ref pdfPTable, string.Format("{0:C2}", decimal.Parse(InvcDtlDocExtPrice)), font); //Totallinea
                }
                public class EventPageFedco : PdfPageEventHelper
                {
                    PdfContentByte cb;

                    // we will put the final number of pages in a template
                    PdfTemplate headerTemplate, footerTemplate;

                    // this is the BaseFont we are going to use for the header / footer
                    BaseFont bf = null;

                    // This keeps track of the creation time
                    DateTime PrintTime = DateTime.Now;

                    private string _header;

                    public string Header
                    {
                        get { return _header; }
                        set { _header = value; }
                    }
                    PdfPTable _Encabezado;
                    PdfPTable _DatosCliente;
                    PdfPTable _DetalleHead;
                    PdfPTable _PiePagina;
                    PdfPTable _DatosFactura;

                    public PdfPTable PiePagina
                    {
                        get
                        {
                            return _PiePagina;
                        }
                        set
                        {
                            _PiePagina = value;
                        }
                    }
                    public PdfPTable Encabezado
                    {
                        get
                        {
                            return _Encabezado;
                        }
                        set
                        {
                            _Encabezado = value;
                        }
                    }
                    public PdfPTable DatosCliente
                    {
                        get
                        {
                            return _DatosCliente;
                        }
                        set
                        {
                            _DatosCliente = value;
                        }
                    }
                    public PdfPTable DatosFactura
                    {
                        get
                        {
                            return _DatosFactura;
                        }
                        set
                        {
                            _DatosFactura = value;
                        }
                    }
                    public PdfPTable DetalleHead
                    {
                        get
                        {
                            return _DetalleHead;
                        }
                        set
                        {
                            _DetalleHead = value;
                        }
                    }

                    public EventPageFedco(PdfPTable encabezado, PdfPTable DatosCliente, PdfPTable DatosFactura, PdfPTable detalleHead, PdfPTable piepagina)
                    {
                        _Encabezado = encabezado;
                        _DatosCliente = DatosCliente;
                        _DatosFactura = DatosFactura;
                        _DetalleHead = detalleHead;
                        _PiePagina = piepagina;
                    }

                    public override void OnOpenDocument(PdfWriter writer, Document document)
                    {
                        try
                        {
                            PrintTime = DateTime.Now;
                            bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                            cb = writer.DirectContent;
                            headerTemplate = cb.CreateTemplate(100, 100);
                            footerTemplate = cb.CreateTemplate(50, 50);
                        }
                        catch (DocumentException de)
                        {
                        }
                        catch (System.IO.IOException ioe)
                        {
                        }
                    }

                    public override void OnEndPage(iTextSharp.text.pdf.PdfWriter writer, iTextSharp.text.Document document)
                    {
                        base.OnEndPage(writer, document);

                        Encabezado.TotalWidth = document.PageSize.Width - 35f;
                        Encabezado.WriteSelectedRows(0, -1, 20, document.PageSize.Height - 30, writer.DirectContentUnder);

                        _DatosCliente.TotalWidth = document.PageSize.Width - 35f;
                        _DatosCliente.WriteSelectedRows(0, -1, 20, document.PageSize.Height - 120, writer.DirectContentUnder);

                        _DatosFactura.TotalWidth = document.PageSize.Width - 35f;
                        _DatosFactura.WriteSelectedRows(0, -1, 20, document.PageSize.Height - 210, writer.DirectContentUnder);

                        _DetalleHead.TotalWidth = document.PageSize.Width - 35f;
                        _DetalleHead.WriteSelectedRows(0, -1, 20, document.PageSize.Height - 255, writer.DirectContentUnder);

                        _PiePagina.TotalWidth = document.PageSize.Width - 35f;
                        _PiePagina.WriteSelectedRows(0, -1, 20, document.PageSize.Height - 510, writer.DirectContentUnder);
                    }

                    public override void OnCloseDocument(PdfWriter writer, Document document)
                    {
                        base.OnCloseDocument(writer, document);

                        headerTemplate.BeginText();
                        headerTemplate.SetFontAndSize(bf, 12);
                        headerTemplate.SetTextMatrix(0, 0);
                        headerTemplate.ShowText((writer.PageNumber - 1).ToString());
                        headerTemplate.EndText();
                    }
                }
            }
            public class Plantilla_2
            {
                private static float _MinimumHeight = 265;
                public static Document Documento
                {
                    get
                    {
                        return new Document(iTextSharp.text.PageSize.LETTER, 20f, 15f, 280f, 190f);
                    }
                }
                //20f, 15f, 280f, 190f
                public static float MarginLeft = 20f;
                public static void DatosClienteTituloValor(ref PdfPTable table, string title, string value)
                {
                    iTextSharp.text.pdf.PdfPCell celTitle = new iTextSharp.text.pdf.PdfPCell(new Phrase(title, Helpers.Fuentes.Plantilla_2_fontTitleFactura));
                    celTitle.Colspan = 1;
                    celTitle.Padding = 3;
                    celTitle.Border = 0;
                    celTitle.HorizontalAlignment = Element.ALIGN_LEFT;
                    celTitle.VerticalAlignment = Element.ALIGN_TOP;
                    table.AddCell(celTitle);

                    iTextSharp.text.pdf.PdfPCell celValue = new iTextSharp.text.pdf.PdfPCell(new Phrase(value,
                        Helpers.Fuentes.Plantilla_2_fontTitle2));
                    celValue.Colspan = 1;
                    celValue.Padding = 3;
                    celValue.Border = 0;
                    celValue.HorizontalAlignment = Element.ALIGN_LEFT;
                    celValue.VerticalAlignment = Element.ALIGN_TOP;
                    table.AddCell(celValue);
                }
                public static void DetalleHeadTitulos(ref PdfPTable table, string Titulo, bool BordeDerecho)
                {
                    iTextSharp.text.pdf.PdfPCell celda = new iTextSharp.text.pdf.PdfPCell(new Phrase(Titulo, Helpers.Fuentes.Plantilla_2_fontTitleFactura));
                    celda.Colspan = 1;
                    celda.Padding = 3;
                    if(!BordeDerecho)
                        celda.BorderWidthRight = 0;
                    celda.HorizontalAlignment = Element.ALIGN_CENTER;
                    celda.VerticalAlignment = Element.ALIGN_TOP;
                    celda.BackgroundColor = BaseColor.LIGHT_GRAY;
                    table.AddCell(celda);
                }
                public static void DetalleHeadTituloCel(ref PdfPTable table, bool BordeDerecho, float MinimumHeight)
                {
                    iTextSharp.text.pdf.PdfPCell celda = new iTextSharp.text.pdf.PdfPCell();
                    celda.Colspan = 1;
                    celda.HorizontalAlignment = Element.ALIGN_CENTER;
                    celda.VerticalAlignment = Element.ALIGN_TOP;
                    celda.MinimumHeight = MinimumHeight;
                    table.AddCell(celda);
                }
                public static void DetallesCeldas(ref PdfPTable table, string data, iTextSharp.text.Font font)
                {
                    iTextSharp.text.pdf.PdfPCell celda = new iTextSharp.text.pdf.PdfPCell(new Phrase(data, font));
                    celda.Colspan = 1;
                    celda.Padding = 3;
                    celda.Border = 0;
                    celda.BorderColorBottom = BaseColor.WHITE;
                    celda.HorizontalAlignment = Element.ALIGN_CENTER;
                    celda.VerticalAlignment = Element.ALIGN_TOP;
                    table.AddCell(celda);
                }
                public static void DetallesCeldas(ref PdfPTable table, string data, iTextSharp.text.Font font, int Aligment)
                {
                    iTextSharp.text.pdf.PdfPCell celda = new iTextSharp.text.pdf.PdfPCell(new Phrase(data, font));
                    celda.Colspan = 1;
                    celda.Padding = 3;
                    celda.Border = 0;
                    celda.BorderColorBottom = BaseColor.WHITE;
                    celda.HorizontalAlignment = Aligment;
                    celda.VerticalAlignment = Element.ALIGN_TOP;
                    table.AddCell(celda);
                }
                public static void AddUnidadesFRAL(DataRow dataLine, ref PdfPTable pdfPTable, iTextSharp.text.Font font)
                {
                    string PartNum = string.Empty;
                    string SalesUM = string.Empty;
                    if (Helpers.Compartido.VerificarExistenciaColumnas(dataLine, "SalesUM"))
                        SalesUM = dataLine["SalesUM"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(dataLine, "PartNum"))
                        SalesUM = dataLine["PartNum"].ToString();

                    DetallesCeldas(ref pdfPTable, PartNum, font);
                    DetallesCeldas(ref pdfPTable, dataLine["LineDesc"].ToString(), font);
                    DetallesCeldas(ref pdfPTable, string.Format("{0:N0}", dataLine["SellingShipQty"].ToString()), font);
                    DetallesCeldas(ref pdfPTable, SalesUM, font);
                    DetallesCeldas(ref pdfPTable, string.Format("{0:C2}", Convert.ToDecimal(dataLine["UnitPrice"].ToString())), font);
                    DetallesCeldas(ref pdfPTable, string.Format("{0:C2}", decimal.Parse(dataLine["DocExtPrice"].ToString())), font);
                }
                public static void PiePagina_Totales(ref PdfPTable table, string Titulo, string data)
                {
                    iTextSharp.text.pdf.PdfPCell celtitulo = new iTextSharp.text.pdf.PdfPCell(new Phrase(Titulo, Helpers.Fuentes.Plantilla_2_fontTitleFactura));
                    celtitulo.Colspan = 1;
                    celtitulo.Padding = 3;
                    celtitulo.PaddingBottom = 5;
                    celtitulo.Border = 0;
                    celtitulo.BorderWidthRight = 1;
                    celtitulo.BorderColorBottom = BaseColor.WHITE;
                    celtitulo.HorizontalAlignment = Element.ALIGN_LEFT;
                    celtitulo.VerticalAlignment = Element.ALIGN_TOP;
                    table.AddCell(celtitulo);

                    iTextSharp.text.pdf.PdfPCell celdata = new iTextSharp.text.pdf.PdfPCell(new Phrase(data, Helpers.Fuentes.Plantilla_2_fontTitle2));
                    celdata.Colspan = 1;
                    celdata.Padding = 3;
                    celdata.PaddingBottom = 5;
                    celdata.Border = 0;
                    celdata.BorderColorBottom = BaseColor.WHITE;
                    celdata.HorizontalAlignment = Element.ALIGN_RIGHT;
                    celdata.VerticalAlignment = Element.ALIGN_TOP;
                    table.AddCell(celdata);
                }
                //--------------- Datos de la factura -------------------
                public static PdfPTable Encabezado(DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType)
                {
                    PdfPTable table = new PdfPTable(4);
                    table.WidthPercentage = 100;
                    table.SetWidths(new float[] {1.2f, 2f, 0.9f, 1.5f });
                    PdfPCell celLogo = new PdfPCell();

                    iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                    iTextSharp.text.Font fontTitle2 = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                    iTextSharp.text.Font fontCustom = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);

                    Paragraph prgTitle = new Paragraph(DsInvoiceAR.Tables["InvcHead"].Rows[0]["Company"].ToString(), fontTitle);

                    Paragraph prgTitle2 = new Paragraph(string.Format("NIT. {0}\n{1}\n{2} {3} {4}\n{5}", DsInvoiceAR.Tables["Customer"].Rows[0]["CustID"].ToString(),
                        DsInvoiceAR.Tables["Customer"].Rows[0]["Address1"].ToString(), DsInvoiceAR.Tables["Customer"].Rows[0]["City"].ToString(),
                        DsInvoiceAR.Tables["Customer"].Rows[0]["State"].ToString(), DsInvoiceAR.Tables["Customer"].Rows[0]["Country"].ToString(),
                        DsInvoiceAR.Tables["Customer"].Rows[0]["PhoneNum"].ToString()), fontTitle2);
                    prgTitle.Alignment = Element.ALIGN_CENTER;
                    prgTitle2.Alignment = Element.ALIGN_CENTER;
                    celLogo.AddElement(prgTitle);
                    celLogo.AddElement(prgTitle2);

                    PdfPCell celAcercade = new PdfPCell();
                    
                    iTextSharp.text.Font fontAcercade = FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL);
                    Paragraph prgAcercade = new Paragraph("XXXXXXXXXX XXXXXXXXXXXXXXXXX XXXXXXXXXXXXXXXXXXXXXXXXXX XXXXXXXXXXX" +
                        "XXXXX XXXXXXXXX XXXXXXXXXX XXXXXXXXXXXXXXX XXXXXXXX.", fontAcercade);
                    prgAcercade.Alignment = Element.ALIGN_CENTER;

                    //divAcercade.AddElement(prgAcercade);
                    celAcercade.AddElement(prgAcercade);
                    //-----------------------------------------------------------------------------------------------------
                    iTextSharp.text.Image ImgQR = iTextSharp.text.Image.GetInstance(QRInvoice, BaseColor.WHITE);
                    ImgQR.ScaleAbsolute(60f, 60f);
                    ImgQR.Alignment = Element.ALIGN_CENTER;
                    PdfPCell celQR = new PdfPCell();
                    celQR.AddElement(ImgQR);
                    //----------------------------------------------------------------------------------------------------
                    iTextSharp.text.Font fontTitleFactura = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);

                    PdfPTable tableFactura = new PdfPTable(3);

                    tableFactura.WidthPercentage = 100;
                    tableFactura.SetWidths(new float[] { 1.0f, 4.0f, 0.5f });

                    iTextSharp.text.pdf.PdfPCell celTittle = new iTextSharp.text.pdf.PdfPCell(new Phrase("FACTURA DE VENTA\n\n", fontTitleFactura));
                    celTittle.Colspan = 3;
                    celTittle.Padding = 3;
                    celTittle.HorizontalAlignment = Element.ALIGN_CENTER;
                    celTittle.VerticalAlignment = Element.ALIGN_TOP;
                    celTittle.Border = 0;
                    celTittle.BackgroundColor = BaseColor.LIGHT_GRAY;
                    tableFactura.AddCell(celTittle);

                    iTextSharp.text.pdf.PdfPCell celNo = new iTextSharp.text.pdf.PdfPCell(new Phrase("No. ", fontTitleFactura));
                    celNo.Colspan = 1;
                    celNo.Padding = 5;
                    celNo.HorizontalAlignment = Element.ALIGN_CENTER;
                    celNo.VerticalAlignment = Element.ALIGN_TOP;
                    celNo.Border = 0;
                    celNo.BackgroundColor = BaseColor.LIGHT_GRAY;
                    tableFactura.AddCell(celNo);

                    string NumLegalFactura = string.Empty;
                    if (InvoiceType == "InvoiceType")
                        NumLegalFactura = DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiseRef"].ToString();
                    else if (InvoiceType == "CreditNoteType")
                        NumLegalFactura = DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"].ToString();
                    else
                        NumLegalFactura = DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"].ToString();

                    iTextSharp.text.pdf.PdfPCell celNoFactura = new iTextSharp.text.pdf.PdfPCell(new Phrase(NumLegalFactura, fontTitleFactura));
                    celNoFactura.Colspan = 1;
                    celNoFactura.Padding = 5;
                    celNoFactura.HorizontalAlignment = Element.ALIGN_LEFT;
                    celNoFactura.VerticalAlignment = Element.ALIGN_TOP;
                    celNoFactura.Border = 0;
                    celNoFactura.BackgroundColor = BaseColor.WHITE;
                    tableFactura.AddCell(celNoFactura);


                    iTextSharp.text.pdf.PdfPCell celEspacioNoFactura = new iTextSharp.text.pdf.PdfPCell(new Phrase(" ", fontTitleFactura));
                    celEspacioNoFactura.Colspan = 1;
                    //celNo.Padding = 3;
                    celEspacioNoFactura.HorizontalAlignment = Element.ALIGN_CENTER;
                    celEspacioNoFactura.VerticalAlignment = Element.ALIGN_TOP;
                    celEspacioNoFactura.Border = 0;
                    celEspacioNoFactura.BackgroundColor = BaseColor.LIGHT_GRAY;
                    tableFactura.AddCell(celEspacioNoFactura);

                    iTextSharp.text.pdf.PdfPCell celRellenoNoFactura = new iTextSharp.text.pdf.PdfPCell(new Phrase(" ", fontTitleFactura));
                    celRellenoNoFactura.Colspan = 3;
                    celRellenoNoFactura.Padding = 3;
                    celRellenoNoFactura.BackgroundColor = BaseColor.LIGHT_GRAY;
                    celRellenoNoFactura.HorizontalAlignment = Element.ALIGN_RIGHT;
                    celRellenoNoFactura.VerticalAlignment = Element.ALIGN_TOP;
                    celRellenoNoFactura.Border = 0;
                    tableFactura.AddCell(celRellenoNoFactura);

                    iTextSharp.text.pdf.PdfPCell celConsecutivoInterno = new iTextSharp.text.pdf.PdfPCell(new Phrase("xxxxxxxxxx", fontCustom));
                    celConsecutivoInterno.Colspan = 4;
                    //celConsecutivoInterno.Padding = 3;
                    celConsecutivoInterno.BackgroundColor = BaseColor.WHITE;
                    celConsecutivoInterno.HorizontalAlignment = Element.ALIGN_RIGHT;
                    celConsecutivoInterno.VerticalAlignment = Element.ALIGN_TOP;
                    celConsecutivoInterno.Border = 0;
                    tableFactura.AddCell(celConsecutivoInterno);

                    PdfPCell celNumeroFactura = new PdfPCell();
                    
                    celNumeroFactura.AddElement(tableFactura);
                    //-- Agregando todas las celdas a la tabla maestra--
                    celLogo.Border = 0;
                    celLogo.Padding = 0;
                    celAcercade.Border = 0;
                    celAcercade.Padding = 0;
                    celQR.Border = 0;
                    celQR.Padding = 0;
                    celNumeroFactura.Border = 0;
                    celNumeroFactura.PaddingLeft = 5;
                    celNumeroFactura.Padding = 0;

                    table.AddCell(celLogo);
                    table.AddCell(celAcercade);
                    table.AddCell(celQR);
                    table.AddCell(celNumeroFactura);
                    return table;
                }
                public static PdfPTable DatosCliente(DataSet DsInvoiceAR)
                {
                    //------------------------------------------------------------------------------------------------
                    PdfPTable tableFacturar = new PdfPTable(3);
                    //Dimenciones.
                    float[] DimencionFacturar = new float[3];
                    DimencionFacturar[0] = 1.0F;//
                    DimencionFacturar[1] = 0.01F;//
                    DimencionFacturar[2] = 1.0F;//

                    tableFacturar.WidthPercentage = 100;
                    tableFacturar.SetWidths(DimencionFacturar);
                    //----------------------------------------------------------------------------------------------
                    PdfPTable tableFacturarA = new PdfPTable(2);
                    float[] DimencionFacturarA = new float[2];
                    DimencionFacturarA[0] = 0.8F;//
                    DimencionFacturarA[1] = 2.0F;//

                    tableFacturarA.WidthPercentage = 100;
                    tableFacturarA.SetWidths(DimencionFacturarA);

                    iTextSharp.text.pdf.PdfPCell celDatosFacturarA = new iTextSharp.text.pdf.PdfPCell(new Phrase("FACTURAR A:\n", Helpers.Fuentes.Plantilla_2_fontTitleFactura));
                    celDatosFacturarA.Colspan = 2;
                    celDatosFacturarA.Padding = 3;
                    celDatosFacturarA.Border = 0;
                    celDatosFacturarA.BorderColorBottom = BaseColor.WHITE;
                    celDatosFacturarA.HorizontalAlignment = Element.ALIGN_CENTER;
                    celDatosFacturarA.VerticalAlignment = Element.ALIGN_TOP;
                    tableFacturarA.AddCell(celDatosFacturarA);

                    DatosClienteTituloValor(ref tableFacturarA, "CLIENTE: ", DsInvoiceAR.Tables["InvcHead"].Rows[0]["CustomerName"].ToString());
                    DatosClienteTituloValor(ref tableFacturarA, "NIT: ", DsInvoiceAR.Tables["Customer"].Rows[0]["ResaleID"].ToString());
                    DatosClienteTituloValor(ref tableFacturarA, "DIRECCION: ", DsInvoiceAR.Tables["Customer"].Rows[0]["Address1"].ToString());
                    DatosClienteTituloValor(ref tableFacturarA, "TELEFONO: ", DsInvoiceAR.Tables["Customer"].Rows[0]["PhoneNum"].ToString());
                    DatosClienteTituloValor(ref tableFacturarA, "CIUDAD: ", DsInvoiceAR.Tables["Customer"].Rows[0]["City"].ToString());
                    DatosClienteTituloValor(ref tableFacturarA, "PAIS: ", DsInvoiceAR.Tables["Customer"].Rows[0]["Country"].ToString());

                    iTextSharp.text.pdf.PdfPCell celTittleFacturarA = new iTextSharp.text.pdf.PdfPCell(tableFacturarA);
                    tableFacturar.AddCell(celTittleFacturarA);
                    //----------------------------------------------------------------------------------------------------------------------------

                    iTextSharp.text.pdf.PdfPCell celEspacio2 = new iTextSharp.text.pdf.PdfPCell(new Phrase(" ", Helpers.Fuentes.Plantilla_2_fontTitleFactura));
                    celEspacio2.Border = 0;
                    tableFacturar.AddCell(celEspacio2);

                    //------------------------------------------------------------------------------------------

                    PdfPTable tableDespacharA = new PdfPTable(2);
                    float[] DimencionDespacharA = new float[2];
                    DimencionDespacharA[0] = 0.8F;//
                    DimencionDespacharA[1] = 2.0F;//

                    tableDespacharA.WidthPercentage = 100;
                    tableDespacharA.SetWidths(DimencionDespacharA);

                    iTextSharp.text.pdf.PdfPCell celTittleDespacharA = new iTextSharp.text.pdf.PdfPCell(new Phrase("DESPACHAR A:\n", Helpers.Fuentes.Plantilla_2_fontTitleFactura));
                    celTittleDespacharA.Colspan = 2;
                    celTittleDespacharA.Padding = 3;
                    celTittleDespacharA.Border = 0;
                    celTittleDespacharA.BorderColorBottom = BaseColor.WHITE;
                    celTittleDespacharA.HorizontalAlignment = Element.ALIGN_CENTER;
                    celTittleDespacharA.VerticalAlignment = Element.ALIGN_TOP;
                    tableDespacharA.AddCell(celTittleDespacharA);

                    DatosClienteTituloValor(ref tableDespacharA, "CLIENTE: ", DsInvoiceAR.Tables["Customer"].Rows[0]["Name"].ToString());
                    DatosClienteTituloValor(ref tableDespacharA, "NIT: ", DsInvoiceAR.Tables["Customer"].Rows[0]["ResaleID"].ToString());
                    DatosClienteTituloValor(ref tableDespacharA, "DIRECCION: ", DsInvoiceAR.Tables["Customer"].Rows[0]["Address1"].ToString());
                    DatosClienteTituloValor(ref tableDespacharA, "TELEFONO: ", DsInvoiceAR.Tables["Customer"].Rows[0]["PhoneNum"].ToString());
                    DatosClienteTituloValor(ref tableDespacharA, "CIUDAD: ", DsInvoiceAR.Tables["Customer"].Rows[0]["City"].ToString());
                    DatosClienteTituloValor(ref tableDespacharA, "PAIS: ", DsInvoiceAR.Tables["Customer"].Rows[0]["Country"].ToString());

                    iTextSharp.text.pdf.PdfPCell celDatosDespacharA = new iTextSharp.text.pdf.PdfPCell(tableDespacharA);
                    tableFacturar.AddCell(celDatosDespacharA);
                    return tableFacturar;
                }
                public static PdfPTable DatosFactura(DataSet DsInvoiceAR)
                {
                    //--------- variables ---------
                    string InvcHeadSalesRepName1 = string.Empty;
                    string InvcHeadPONum = string.Empty;
                    string InvcHeadOrderNum = string.Empty;
                    string InvcHeadTermsCodeDescription = string.Empty;
                    string InvcHeadInvoiceDate = string.Empty;
                    string InvcHeadDueDate = string.Empty;
                    string InvcHeadCurrencyCode = string.Empty;
                    //---- Asignar valores a las variables ------
                    if (Helpers.Compartido.VerificarExistenciaColumnas(DsInvoiceAR, "InvcHead", "SalesRepName1", 0))
                        InvcHeadSalesRepName1 = DsInvoiceAR.Tables["InvcHead"].Rows[0]["SalesRepName1"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(DsInvoiceAR, "InvcHead", "PONum", 0))
                        InvcHeadPONum = DsInvoiceAR.Tables["InvcHead"].Rows[0]["PONum"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(DsInvoiceAR, "InvcHead", "OrderNum", 0))
                        InvcHeadOrderNum = DsInvoiceAR.Tables["InvcHead"].Rows[0]["OrderNum"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(DsInvoiceAR, "InvcHead", "TermsCodeDescription", 0))
                        InvcHeadTermsCodeDescription = DsInvoiceAR.Tables["InvcHead"].Rows[0]["TermsCodeDescription"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(DsInvoiceAR, "InvcHead", "InvoiceDate", 0))
                        InvcHeadInvoiceDate = DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(DsInvoiceAR, "InvcHead", "DueDate", 0))
                        InvcHeadDueDate = DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(DsInvoiceAR, "InvcHead", "CurrencyCode", 0))
                        InvcHeadCurrencyCode = DsInvoiceAR.Tables["InvcHead"].Rows[0]["CurrencyCode"].ToString();
                    //-------------------------------------

                    PdfPTable table = new PdfPTable(1);
                    table.WidthPercentage = 100;
                    #region Tabla de Detalles
                    //------------------------------------------------------------------------------------------------
                    PdfPTable tableDetalles = new PdfPTable(5);
                    tableDetalles.PaddingTop = 20;
                    //Dimenciones.
                    float[] DimencionDetalles = new float[5];
                    DimencionDetalles[0] = 2.5F;//
                    DimencionDetalles[1] = 1.0F;//
                    DimencionDetalles[2] = 1.0F;//
                    DimencionDetalles[3] = 1.0F;//
                    DimencionDetalles[4] = 0.8F;//

                    tableDetalles.WidthPercentage = 100;
                    tableDetalles.SetWidths(DimencionDetalles);

                    iTextSharp.text.pdf.PdfPCell celVendedor = new iTextSharp.text.pdf.PdfPCell(new Phrase("VENDEDOR", Helpers.Fuentes.Plantilla_2_fontTitleFactura));
                    celVendedor.Colspan = 1;
                    celVendedor.Padding = 3;
                    //celVendedor.Border = 0;
                    celVendedor.BorderColorBottom = BaseColor.WHITE;
                    celVendedor.HorizontalAlignment = Element.ALIGN_CENTER;
                    celVendedor.VerticalAlignment = Element.ALIGN_TOP;
                    tableDetalles.AddCell(celVendedor);

                    iTextSharp.text.pdf.PdfPCell celOC_Cliente = new iTextSharp.text.pdf.PdfPCell(new Phrase("O.C. CLIENTE", Helpers.Fuentes.Plantilla_2_fontTitleFactura));
                    celOC_Cliente.Colspan = 1;
                    celOC_Cliente.Padding = 3;
                    //celVendedor.Border = 0;
                    celOC_Cliente.BorderColorBottom = BaseColor.WHITE;
                    celOC_Cliente.HorizontalAlignment = Element.ALIGN_CENTER;
                    celOC_Cliente.VerticalAlignment = Element.ALIGN_TOP;
                    tableDetalles.AddCell(celOC_Cliente);

                    iTextSharp.text.pdf.PdfPCell celOrdenVenta = new iTextSharp.text.pdf.PdfPCell(new Phrase("ORDEN DE VENTA", Helpers.Fuentes.Plantilla_2_fontTitleFactura));
                    celOrdenVenta.Colspan = 1;
                    celOrdenVenta.Padding = 3;
                    //celVendedor.Border = 0;
                    celOrdenVenta.BorderColorBottom = BaseColor.WHITE;
                    celOrdenVenta.HorizontalAlignment = Element.ALIGN_CENTER;
                    celOrdenVenta.VerticalAlignment = Element.ALIGN_TOP;
                    tableDetalles.AddCell(celOrdenVenta);

                    iTextSharp.text.pdf.PdfPCell celTerminoPago = new iTextSharp.text.pdf.PdfPCell(new Phrase("TÉRMINO DE PAGO", Helpers.Fuentes.Plantilla_2_fontTitleFactura));
                    celTerminoPago.Colspan = 1;
                    celTerminoPago.Padding = 3;
                    //celVendedor.Border = 0;
                    celTerminoPago.BorderColorBottom = BaseColor.WHITE;
                    celTerminoPago.HorizontalAlignment = Element.ALIGN_CENTER;
                    celTerminoPago.VerticalAlignment = Element.ALIGN_TOP;
                    tableDetalles.AddCell(celTerminoPago);

                    iTextSharp.text.pdf.PdfPCell celRemision = new iTextSharp.text.pdf.PdfPCell(new Phrase("REMISIÓN", Helpers.Fuentes.Plantilla_2_fontTitleFactura));
                    celRemision.Colspan = 1;
                    celRemision.Padding = 3;
                    //celVendedor.Border = 0;
                    celRemision.BorderColorBottom = BaseColor.WHITE;
                    celRemision.HorizontalAlignment = Element.ALIGN_CENTER;
                    celRemision.VerticalAlignment = Element.ALIGN_TOP;
                    tableDetalles.AddCell(celRemision);

                    iTextSharp.text.pdf.PdfPCell celDataVendedor = new iTextSharp.text.pdf.PdfPCell(new Phrase(InvcHeadSalesRepName1, Helpers.Fuentes.Plantilla_2_fontCustom));
                    celDataVendedor.Colspan = 1;
                    celDataVendedor.Padding = 3;
                    //celVendedor.Border = 0;
                    celDataVendedor.BorderColorBottom = BaseColor.WHITE;
                    celDataVendedor.HorizontalAlignment = Element.ALIGN_CENTER;
                    celDataVendedor.VerticalAlignment = Element.ALIGN_TOP;
                    tableDetalles.AddCell(celDataVendedor);

                    iTextSharp.text.pdf.PdfPCell celDataOC_Cliente = new iTextSharp.text.pdf.PdfPCell(new Phrase(InvcHeadPONum, Helpers.Fuentes.Plantilla_2_fontCustom));
                    celDataOC_Cliente.Colspan = 1;
                    celDataOC_Cliente.Padding = 3;
                    //celVendedor.Border = 0;
                    celDataOC_Cliente.BorderColorBottom = BaseColor.WHITE;
                    celDataOC_Cliente.HorizontalAlignment = Element.ALIGN_CENTER;
                    celDataOC_Cliente.VerticalAlignment = Element.ALIGN_TOP;
                    tableDetalles.AddCell(celDataOC_Cliente);

                    iTextSharp.text.pdf.PdfPCell celDataOrdenVenta = new iTextSharp.text.pdf.PdfPCell(new Phrase(InvcHeadOrderNum,
                        Helpers.Fuentes.Plantilla_2_fontCustom));
                    celDataOrdenVenta.Colspan = 1;
                    celDataOrdenVenta.Padding = 3;
                    //celVendedor.Border = 0;
                    celDataOrdenVenta.BorderColorBottom = BaseColor.WHITE;
                    celDataOrdenVenta.HorizontalAlignment = Element.ALIGN_CENTER;
                    celDataOrdenVenta.VerticalAlignment = Element.ALIGN_TOP;
                    tableDetalles.AddCell(celDataOrdenVenta);

                    iTextSharp.text.pdf.PdfPCell celDataTerminoPago = new iTextSharp.text.pdf.PdfPCell(new Phrase(InvcHeadTermsCodeDescription, Helpers.Fuentes.Plantilla_2_fontCustom));
                    celDataTerminoPago.Colspan = 1;
                    celDataTerminoPago.Padding = 3;
                    //celVendedor.Border = 0;
                    celDataTerminoPago.BorderColorBottom = BaseColor.WHITE;
                    celDataTerminoPago.HorizontalAlignment = Element.ALIGN_CENTER;
                    celDataTerminoPago.VerticalAlignment = Element.ALIGN_TOP;
                    tableDetalles.AddCell(celDataTerminoPago);

                    iTextSharp.text.pdf.PdfPCell celDataRemision = new iTextSharp.text.pdf.PdfPCell(new Phrase("0", Helpers.Fuentes.Plantilla_2_fontCustom));
                    celDataRemision.Colspan = 1;
                    celDataRemision.Padding = 3;
                    //celVendedor.Border = 0;
                    celDataRemision.BorderColorBottom = BaseColor.WHITE;
                    celDataRemision.HorizontalAlignment = Element.ALIGN_CENTER;
                    celDataRemision.VerticalAlignment = Element.ALIGN_TOP;
                    tableDetalles.AddCell(celDataRemision);

                    //-------------------------------------------------------------------

                    PdfPTable tableDetalles2 = new PdfPTable(3);
                    tableDetalles2.WidthPercentage = 100;

                    PdfPTable tableFechaFactura = new PdfPTable(2);
                    float[] dimecionesTablaFecha = new float[2];
                    dimecionesTablaFecha[0] = 0.7F;
                    dimecionesTablaFecha[1] = 1.0F;
                    tableFechaFactura.SetWidths(dimecionesTablaFecha);

                    iTextSharp.text.pdf.PdfPCell celTextFechaFactura = new iTextSharp.text.pdf.PdfPCell(new Phrase("FECHA FACTURA: ",
                        Helpers.Fuentes.Plantilla_2_fontTitle));
                    celTextFechaFactura.Colspan = 1;
                    celTextFechaFactura.Padding = 3;
                    celTextFechaFactura.Border = 0;
                    celTextFechaFactura.BorderColorBottom = BaseColor.WHITE;
                    celTextFechaFactura.HorizontalAlignment = Element.ALIGN_CENTER;
                    celTextFechaFactura.VerticalAlignment = Element.ALIGN_CENTER;
                    tableFechaFactura.AddCell(celTextFechaFactura);

                    iTextSharp.text.pdf.PdfPCell celFechaFactura = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:MM/dd/yyyy}", DateTime.Parse(InvcHeadInvoiceDate)),
                    Helpers.Fuentes.Plantilla_2_fontCustom));
                    celFechaFactura.Colspan = 1;
                    celFechaFactura.Padding = 3;
                    celFechaFactura.PaddingTop = 4;
                    celFechaFactura.Border = 0;
                    celFechaFactura.BorderColorBottom = BaseColor.WHITE;
                    celFechaFactura.HorizontalAlignment = Element.ALIGN_LEFT;
                    celFechaFactura.VerticalAlignment = Element.ALIGN_CENTER;
                    tableFechaFactura.AddCell(celFechaFactura);

                    PdfPCell _celFechaFactura = new PdfPCell(tableFechaFactura);
                    tableDetalles2.AddCell(_celFechaFactura);


                    PdfPTable tableFechaVencimiento = new PdfPTable(2);
                    float[] dmTablaFechaVencimiento = new float[2];
                    dmTablaFechaVencimiento[0] = 1.0F;
                    dmTablaFechaVencimiento[1] = 1.0F;
                    tableFechaVencimiento.SetWidths(dmTablaFechaVencimiento);

                    iTextSharp.text.pdf.PdfPCell celTextFechaVencimiento = new iTextSharp.text.pdf.PdfPCell(new Phrase("FECHA VENCIMIENTO: ",
                        Helpers.Fuentes.Plantilla_2_fontTitle));
                    celTextFechaVencimiento.Colspan = 1;
                    celTextFechaVencimiento.Padding = 3;
                    celTextFechaVencimiento.Border = 0;
                    celTextFechaVencimiento.HorizontalAlignment = Element.ALIGN_LEFT;
                    celTextFechaVencimiento.VerticalAlignment = Element.ALIGN_TOP;
                    tableFechaVencimiento.AddCell(celTextFechaVencimiento);

                    iTextSharp.text.pdf.PdfPCell celFechaVencimiento = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:MM/dd/yyyy}",
                       DateTime.Parse(InvcHeadDueDate)),
                    Helpers.Fuentes.Plantilla_2_fontCustom));
                    celFechaVencimiento.Colspan = 1;
                    celFechaVencimiento.Padding = 4;
                    celFechaVencimiento.Border = 0;
                    celFechaVencimiento.BorderColorBottom = BaseColor.WHITE;
                    celFechaVencimiento.HorizontalAlignment = Element.ALIGN_LEFT;
                    celFechaVencimiento.VerticalAlignment = Element.ALIGN_TOP;
                    tableFechaVencimiento.AddCell(celFechaVencimiento);

                    PdfPCell _celFechaFacturaVencimiento = new PdfPCell(tableFechaVencimiento);
                    tableDetalles2.AddCell(_celFechaFacturaVencimiento);

                    PdfPTable tableMoneda = new PdfPTable(2);
                    float[] dimecionesTablaMoneda = new float[2];
                    dimecionesTablaMoneda[0] = 0.4F;
                    dimecionesTablaMoneda[1] = 1.0F;
                    tableMoneda.SetWidths(dimecionesTablaMoneda);

                    iTextSharp.text.pdf.PdfPCell celTextMoneda = new iTextSharp.text.pdf.PdfPCell(new Phrase("MONEDA: ", Helpers.Fuentes.Plantilla_2_fontTitle));
                    celTextMoneda.Colspan = 1;
                    celTextMoneda.Padding = 3;
                    celTextMoneda.Border = 0;
                    celTextMoneda.BorderColorBottom = BaseColor.WHITE;
                    celTextMoneda.HorizontalAlignment = Element.ALIGN_LEFT;
                    celTextMoneda.VerticalAlignment = Element.ALIGN_TOP;
                    tableMoneda.AddCell(celTextMoneda);

                    iTextSharp.text.pdf.PdfPCell celMoneda = new iTextSharp.text.pdf.PdfPCell(new Phrase(InvcHeadCurrencyCode,
                    Helpers.Fuentes.Plantilla_2_fontCustom));
                    celMoneda.Colspan = 1;
                    celMoneda.Padding = 4;
                    celMoneda.Border = 0;
                    celMoneda.BorderColorBottom = BaseColor.WHITE;
                    celMoneda.HorizontalAlignment = Element.ALIGN_LEFT;
                    celMoneda.VerticalAlignment = Element.ALIGN_TOP;
                    tableMoneda.AddCell(celMoneda);

                    PdfPCell _celMoneda = new PdfPCell(tableMoneda);
                    tableDetalles2.AddCell(_celMoneda);

                    #endregion
                    PdfPCell celDetalles = new PdfPCell();
                    celDetalles.Border = 0;
                    celDetalles.Padding = 0;
                    celDetalles.AddElement(tableDetalles);
                    PdfPCell celDetalles2 = new PdfPCell();
                    celDetalles2.Border = 0;
                    celDetalles2.Padding = 0;
                    celDetalles2.AddElement(tableDetalles2);

                    table.AddCell(celDetalles);
                    table.AddCell(celDetalles2);
                    return table;
                }
                public static PdfPTable DetalleHead(float MinimumHeight)
                {
                    PdfPTable tableTituloUnidades = new PdfPTable(6);

                    tableTituloUnidades.WidthPercentage = 100;
                    tableTituloUnidades.SetWidths(new float[] {1.0f, 3.0f, 1.0f, 0.5f, 1.0f, 1.5f });

                    DetalleHeadTitulos(ref tableTituloUnidades, "CÓDIGO", false);
                    DetalleHeadTitulos(ref tableTituloUnidades, "DESCRIPCIÓN",false);
                    DetalleHeadTitulos(ref tableTituloUnidades, "CANTIDAD", false);
                    DetalleHeadTitulos(ref tableTituloUnidades, "UNIDAD", false);
                    DetalleHeadTitulos(ref tableTituloUnidades, "VALOR UNITARIO", false);
                    DetalleHeadTitulos(ref tableTituloUnidades, "VALOR TOTAL", true);
                    //--- agregando las celdas vacias ---------------------
                    DetalleHeadTituloCel(ref tableTituloUnidades, false, MinimumHeight);
                    DetalleHeadTituloCel(ref tableTituloUnidades, false, MinimumHeight);
                    DetalleHeadTituloCel(ref tableTituloUnidades, false, MinimumHeight);
                    DetalleHeadTituloCel(ref tableTituloUnidades, false, MinimumHeight);
                    DetalleHeadTituloCel(ref tableTituloUnidades, false, MinimumHeight);
                    DetalleHeadTituloCel(ref tableTituloUnidades, true, MinimumHeight);

                    return tableTituloUnidades;
                }
                public static PdfPTable Detalles(DataSet DsInvoiceAR)
                {
                    PdfPTable tableUnidades = new PdfPTable(6);
                    tableUnidades.WidthPercentage = 100;
                    tableUnidades.SetWidths(new float[] { 1.0f, 3.0f, 1.0f, 0.5f, 1.0f, 1.5f});

                    foreach (DataRow InvoiceLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
                    {
                        AddUnidadesFRAL(InvoiceLine, ref tableUnidades, Helpers.Fuentes.Plantilla_2_fontCustom);
                    }
                    return tableUnidades;
                }
                public static PdfPTable PiePagina(DataSet DsInvoiceAR)
                {
                    string InvcHeadNumber01 = "0";
                    string InvcHeadCharacter04 = string.Empty;
                    if (Helpers.Compartido.VerificarExistenciaColumnas(DsInvoiceAR, "InvcHead", "Number01", 0))
                        InvcHeadNumber01 = DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number01"].ToString();
                    if (Helpers.Compartido.VerificarExistenciaColumnas(DsInvoiceAR, "INvcHead", "Character04", 0))
                        InvcHeadCharacter04 = DsInvoiceAR.Tables["InvcHead"].Rows[0]["Character04"].ToString();

                    PdfPTable tabla = new PdfPTable(1);
                    tabla.WidthPercentage = 100;
                    //------**********************************************************************************************************
                    iTextSharp.text.pdf.PdfPCell celEspacio2 = new iTextSharp.text.pdf.PdfPCell(new Phrase(" ", Helpers.Fuentes.Plantilla_2_fontTitleFactura));
                    celEspacio2.Border = 0;
                    PdfPCell celEspacio3 = new PdfPCell(new Phrase("  ", Helpers.Fuentes.Plantilla_2_fontEspacio));
                    celEspacio3.Border = 0;
                    PdfPTable tableTotalesObs = new PdfPTable(3);

                    tableTotalesObs.WidthPercentage = 100;
                    tableTotalesObs.SetWidths(new float[] {2.5f, 0.01f, 1.0f });

                    PdfPTable _tableObs = new PdfPTable(1);
                    _tableObs.WidthPercentage = 100;

                    iTextSharp.text.pdf.PdfPCell celValorLetras = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("VALORES EN LETRA: {0}", Helpers.Compartido.
                        Nroenletras(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"].ToString())), Helpers.Fuentes.Plantilla_2_fontTitleFactura));
                    celValorLetras.Colspan = 1;
                    celValorLetras.Padding = 3;
                    //celValorLetras.Border = 0;
                    celValorLetras.BorderColorBottom = BaseColor.WHITE;
                    celValorLetras.HorizontalAlignment = Element.ALIGN_LEFT;
                    celValorLetras.VerticalAlignment = Element.ALIGN_TOP;

                    iTextSharp.text.pdf.PdfPCell celValorAsegurado = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("VALOR ASEGURADO: {0:C2}",
                       decimal.Parse(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubTotal"].ToString())), Helpers.Fuentes.Plantilla_2_fontTitle2));
                    celValorAsegurado.Colspan = 1;
                    celValorAsegurado.Padding = 5;
                    //celValorLetras.Border = 0;
                    celValorAsegurado.BorderColorBottom = BaseColor.WHITE;
                    celValorAsegurado.HorizontalAlignment = Element.ALIGN_LEFT;
                    celValorAsegurado.VerticalAlignment = Element.ALIGN_TOP;

                    string ObservacionCliente = string.Empty;
                    if (Helpers.Compartido.VerificarExistenciaColumnas(DsInvoiceAR, "InvcHead", "ObservacionCliente_c", 0))
                        ObservacionCliente = DsInvoiceAR.Tables["InvcHead"].Rows[0]["ObservacionCliente_c"].ToString();

                    iTextSharp.text.pdf.PdfPCell celObservaciones = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("OBSERVACIONES:\n{0}",
                        ObservacionCliente), Helpers.Fuentes.Plantilla_2_fontTitleFactura));
                    celObservaciones.Colspan = 1;
                    celObservaciones.Padding = 3;
                    //celValorLetras.Border = 0;
                    celObservaciones.BorderColorBottom = BaseColor.WHITE;
                    celObservaciones.HorizontalAlignment = Element.ALIGN_LEFT;
                    celObservaciones.VerticalAlignment = Element.ALIGN_TOP;

                    _tableObs.AddCell(celValorLetras);
                    _tableObs.AddCell(celEspacio3);
                    _tableObs.AddCell(celValorAsegurado);
                    _tableObs.AddCell(celEspacio3);
                    _tableObs.AddCell(celObservaciones);

                    iTextSharp.text.pdf.PdfPCell _celObs = new iTextSharp.text.pdf.PdfPCell(_tableObs);
                    _celObs.Border = 0;
                    tableTotalesObs.AddCell(_celObs);
                    //----------------------------------------------------------------------------------------------------------------------------

                    iTextSharp.text.pdf.PdfPCell _celEspacio2 = new iTextSharp.text.pdf.PdfPCell(new Phrase(" ", Helpers.Fuentes.Plantilla_2_fontTitleFactura));
                    _celEspacio2.Border = 0;
                    tableTotalesObs.AddCell(_celEspacio2);

                    //------------------------------------------------------------------------------------------

                    PdfPTable _tableTotales = new PdfPTable(2);
                    float[] _DimencionTotales = new float[2];
                    _DimencionTotales[0] = 1.5F;//
                    _DimencionTotales[1] = 2.5F;//

                    _tableTotales.WidthPercentage = 100;
                    _tableTotales.SetWidths(_DimencionTotales);

                    iTextSharp.text.pdf.PdfPCell _celTextSubTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase("SUBTOTAL:", Helpers.Fuentes.Plantilla_2_fontTitleFactura));
                    _celTextSubTotal.Colspan = 1;
                    _celTextSubTotal.Padding = 3;
                    _celTextSubTotal.PaddingBottom = 5;
                    _celTextSubTotal.Border = 0;
                    _celTextSubTotal.BorderWidthRight = 1;
                    _celTextSubTotal.BorderColorBottom = BaseColor.WHITE;
                    _celTextSubTotal.HorizontalAlignment = Element.ALIGN_LEFT;
                    _celTextSubTotal.VerticalAlignment = Element.ALIGN_TOP;
                    _tableTotales.AddCell(_celTextSubTotal);

                    iTextSharp.text.pdf.PdfPCell _celSubTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:C2}",
                        decimal.Parse(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubTotal"].ToString())), Helpers.Fuentes.Plantilla_2_fontTitle2));
                    _celSubTotal.Colspan = 1;
                    _celSubTotal.Padding = 3;
                    _celSubTotal.PaddingBottom = 5;
                    _celSubTotal.Border = 0;
                    _celSubTotal.BorderColorBottom = BaseColor.WHITE;
                    _celSubTotal.HorizontalAlignment = Element.ALIGN_RIGHT;
                    _celSubTotal.VerticalAlignment = Element.ALIGN_TOP;
                    _tableTotales.AddCell(_celSubTotal);

                    iTextSharp.text.pdf.PdfPCell _celTextDescuentos = new iTextSharp.text.pdf.PdfPCell(new Phrase("DESCUENTOS:", Helpers.Fuentes.Plantilla_2_fontTitleFactura));
                    _celTextDescuentos.Colspan = 1;
                    _celTextDescuentos.Padding = 3;
                    _celTextDescuentos.PaddingBottom = 5;
                    _celTextDescuentos.Border = 0;
                    _celTextDescuentos.BorderWidthRight = 1;
                    _celTextDescuentos.BorderColorBottom = BaseColor.WHITE;
                    _celTextDescuentos.HorizontalAlignment = Element.ALIGN_LEFT;
                    _celTextDescuentos.VerticalAlignment = Element.ALIGN_TOP;
                    _tableTotales.AddCell(_celTextDescuentos);

                    iTextSharp.text.pdf.PdfPCell _celDescuentos = new iTextSharp.text.pdf.PdfPCell(new Phrase(InvcHeadNumber01, Helpers.Fuentes.Plantilla_2_fontTitle2));
                    _celDescuentos.Colspan = 1;
                    _celDescuentos.Padding = 3;
                    _celDescuentos.PaddingBottom = 5;
                    _celDescuentos.Border = 0;
                    _celDescuentos.BorderColorBottom = BaseColor.WHITE;
                    _celDescuentos.HorizontalAlignment = Element.ALIGN_RIGHT;
                    _celDescuentos.VerticalAlignment = Element.ALIGN_TOP;
                    _tableTotales.AddCell(_celDescuentos);

                    iTextSharp.text.pdf.PdfPCell _celTextSubTotal2 = new iTextSharp.text.pdf.PdfPCell(new Phrase("SUBTOTAL2:", Helpers.Fuentes.Plantilla_2_fontTitleFactura));
                    _celTextSubTotal2.Colspan = 1;
                    _celTextSubTotal2.Padding = 3;
                    _celTextSubTotal2.PaddingBottom = 5;
                    _celTextSubTotal2.Border = 0;
                    _celTextSubTotal2.BorderWidthRight = 1;
                    _celTextSubTotal2.BorderColorBottom = BaseColor.WHITE;
                    _celTextSubTotal2.HorizontalAlignment = Element.ALIGN_LEFT;
                    _celTextSubTotal2.VerticalAlignment = Element.ALIGN_TOP;
                    _tableTotales.AddCell(_celTextSubTotal2);

                    iTextSharp.text.pdf.PdfPCell _celSubTotal2 = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:C2}",
                        decimal.Parse(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DocTaxAmt"].ToString())), Helpers.Fuentes.Plantilla_2_fontTitle2));
                    _celSubTotal2.Colspan = 1;
                    _celSubTotal2.Padding = 3;
                    _celSubTotal2.PaddingBottom = 5;
                    _celSubTotal2.Border = 0;
                    _celSubTotal2.BorderColorBottom = BaseColor.WHITE;
                    _celSubTotal2.HorizontalAlignment = Element.ALIGN_RIGHT;
                    _celSubTotal2.VerticalAlignment = Element.ALIGN_TOP;
                    _tableTotales.AddCell(_celSubTotal2);

                    iTextSharp.text.pdf.PdfPCell _celTextIva = new iTextSharp.text.pdf.PdfPCell(new Phrase("IVA:", Helpers.Fuentes.Plantilla_2_fontTitleFactura));
                    _celTextIva.Colspan = 1;
                    _celTextIva.Padding = 3;
                    _celTextIva.PaddingBottom = 5;
                    _celTextIva.Border = 0;
                    _celTextIva.BorderWidthRight = 1;
                    //_celTextIva.BorderColorBottom = BaseColor.BLACK;
                    _celTextIva.HorizontalAlignment = Element.ALIGN_LEFT;
                    _celTextIva.VerticalAlignment = Element.ALIGN_TOP;
                    _tableTotales.AddCell(_celTextIva);

                    iTextSharp.text.pdf.PdfPCell _celIva = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:C2}",
                        decimal.Parse(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DocWHTaxAmt"].ToString())), Helpers.Fuentes.Plantilla_2_fontTitle2));
                    _celIva.Colspan = 1;
                    _celIva.Padding = 3;
                    _celIva.PaddingBottom = 5;
                    _celIva.Border = 0;
                    //_celIva.BorderColorBottom = BaseColor.BLACK;
                    _celIva.HorizontalAlignment = Element.ALIGN_RIGHT;
                    _celIva.VerticalAlignment = Element.ALIGN_TOP;
                    _tableTotales.AddCell(_celIva);

                    iTextSharp.text.pdf.PdfPCell _celTextTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase("TOTAL:", Helpers.Fuentes.Plantilla_2_fontTitleFactura));
                    _celTextTotal.Colspan = 1;
                    _celTextTotal.Padding = 3;
                    _celTextTotal.PaddingBottom = 5;
                    _celTextTotal.Border = 0;
                    _celTextTotal.BorderWidthRight = 1;
                    _celTextTotal.BorderWidthTop = 1;
                    _celTextTotal.HorizontalAlignment = Element.ALIGN_LEFT;
                    _celTextTotal.VerticalAlignment = Element.ALIGN_TOP;
                    _tableTotales.AddCell(_celTextTotal);

                    iTextSharp.text.pdf.PdfPCell _celTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase(string.Format("{0:C2}",
                        decimal.Parse(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"].ToString())), Helpers.Fuentes.Plantilla_2_fontTitleFactura));
                    _celTotal.Colspan = 1;
                    _celTotal.Padding = 3;
                    _celTotal.PaddingBottom = 5;
                    _celTotal.Border = 0;
                    _celTotal.BorderWidthTop = 1;
                    _celTotal.HorizontalAlignment = Element.ALIGN_RIGHT;
                    _celTotal.VerticalAlignment = Element.ALIGN_TOP;
                    _tableTotales.AddCell(_celTotal);

                    iTextSharp.text.pdf.PdfPCell _celTotales = new iTextSharp.text.pdf.PdfPCell(_tableTotales);
                    tableTotalesObs.AddCell(_celTotales);
                    //------------------------------------------------------------------------
                    PdfPTable tableResolucion = new PdfPTable(1);
                    tableResolucion.WidthPercentage = 100;
                    string strResolucion = InvcHeadCharacter04;
                    iTextSharp.text.pdf.PdfPCell _celResolucion = new iTextSharp.text.pdf.PdfPCell(new Phrase(strResolucion, Helpers.Fuentes.Plantilla_2_fontTitleFactura));
                    _celResolucion.Colspan = 1;
                    _celResolucion.Padding = 3;
                    //_celResolucion.Border = 0;
                    _celResolucion.HorizontalAlignment = Element.ALIGN_JUSTIFIED_ALL;
                    _celResolucion.VerticalAlignment = Element.ALIGN_TOP;
                    tableResolucion.AddCell(_celResolucion);
                    //----------------------------------------------------------------------
                    PdfPTable tableFirmas = new PdfPTable(5);

                    float[] DimencionFirmas = new float[5];
                    DimencionFirmas[0] = 1.0F;//
                    DimencionFirmas[1] = 0.02F;//
                    DimencionFirmas[2] = 1.0F;//
                    DimencionFirmas[3] = 0.02F;//
                    DimencionFirmas[4] = 1.4F;//

                    tableFirmas.WidthPercentage = 100;
                    tableFirmas.SetWidths(DimencionFirmas);

                    PdfPTable tableDespacahdoPor = new PdfPTable(1);
                    tableDespacahdoPor.WidthPercentage = 100;
                    iTextSharp.text.pdf.PdfPCell celDespachadoPor = new iTextSharp.text.pdf.PdfPCell(new Phrase("\n\n\n\n\n\n\n\n usuario que genera la factura:" +
                        "\n______________________________________\n\nDESPACHADO POR", Helpers.Fuentes.Plantilla_2_fontTitleFactura));
                    celDespachadoPor.Colspan = 1;
                    celDespachadoPor.Padding = 3;
                    celDespachadoPor.HorizontalAlignment = Element.ALIGN_CENTER;
                    celDespachadoPor.VerticalAlignment = Element.ALIGN_TOP;
                    tableDespacahdoPor.AddCell(celDespachadoPor);

                    iTextSharp.text.pdf.PdfPCell _celDespachadoPor = new iTextSharp.text.pdf.PdfPCell(tableDespacahdoPor);
                    tableFirmas.AddCell(_celDespachadoPor);
                    //----------------------------------------------------------------------------------------------------------------------------

                    tableFirmas.AddCell(celEspacio2);

                    //------------------------------------------------------------------------------------------

                    PdfPTable tableFirmaConductor = new PdfPTable(1);
                    tableFirmaConductor.WidthPercentage = 100;
                    iTextSharp.text.pdf.PdfPCell celFirmaConductor = new iTextSharp.text.pdf.PdfPCell(new Phrase("\n\n\n\n\n\n\n\n\n " +
                        "______________________________________" +
                        "\n\nFIRMA DEL CONDUCTOR", Helpers.Fuentes.Plantilla_2_fontTitleFactura));
                    celFirmaConductor.Colspan = 1;
                    celFirmaConductor.Padding = 3;
                    //celFirmaConductor.Border = 0;
                    celFirmaConductor.HorizontalAlignment = Element.ALIGN_CENTER;
                    celFirmaConductor.VerticalAlignment = Element.ALIGN_TOP;
                    tableFirmaConductor.AddCell(celFirmaConductor);

                    iTextSharp.text.pdf.PdfPCell _celFirmaConductor = new iTextSharp.text.pdf.PdfPCell(tableFirmaConductor);
                    tableFirmas.AddCell(_celFirmaConductor);

                    //-------------------------------------------------------------------------------------
                    tableFirmas.AddCell(celEspacio2);
                    //-------------------------------------------------------------------------------------

                    PdfPTable tableSelloCliente = new PdfPTable(1);
                    //tableSelloCliente.WidthPercentage = 100;

                    iTextSharp.text.pdf.PdfPCell cel1SelloCliente = new iTextSharp.text.pdf.PdfPCell(new Phrase("Apreciado cliente este documento " +
                        "no es endolsable", Helpers.Fuentes.Plantilla_2_fontTitleFactura));
                    cel1SelloCliente.Colspan = 1;
                    cel1SelloCliente.Padding = 5;
                    cel1SelloCliente.Border = 1;
                    cel1SelloCliente.BorderWidthBottom = 1;
                    cel1SelloCliente.HorizontalAlignment = Element.ALIGN_LEFT;
                    cel1SelloCliente.VerticalAlignment = Element.ALIGN_TOP;
                    tableSelloCliente.AddCell(cel1SelloCliente);

                    iTextSharp.text.pdf.PdfPCell cel2SelloFechaRecibido = new iTextSharp.text.pdf.PdfPCell(new Phrase("Fecha de recibido:", Helpers.Fuentes.Plantilla_2_fontTitleFactura));

                    cel2SelloFechaRecibido.Colspan = 1;
                    cel2SelloFechaRecibido.Padding = 3;
                    cel2SelloFechaRecibido.Border = 0;
                    cel2SelloFechaRecibido.HorizontalAlignment = Element.ALIGN_LEFT;
                    cel2SelloFechaRecibido.VerticalAlignment = Element.ALIGN_TOP;
                    tableSelloCliente.AddCell(cel2SelloFechaRecibido);

                    iTextSharp.text.pdf.PdfPCell cel2SelloNombre = new iTextSharp.text.pdf.PdfPCell(new Phrase("Nombre:", Helpers.Fuentes.Plantilla_2_fontTitleFactura));

                    cel2SelloNombre.Colspan = 1;
                    cel2SelloNombre.Padding = 3;
                    cel2SelloNombre.Border = 0;
                    cel2SelloNombre.HorizontalAlignment = Element.ALIGN_LEFT;
                    cel2SelloNombre.VerticalAlignment = Element.ALIGN_TOP;
                    tableSelloCliente.AddCell(cel2SelloNombre);

                    iTextSharp.text.pdf.PdfPCell cel2SelloId = new iTextSharp.text.pdf.PdfPCell(new Phrase("Identificacion:", Helpers.Fuentes.Plantilla_2_fontTitleFactura));

                    cel2SelloId.Colspan = 1;
                    cel2SelloId.Padding = 3;
                    cel2SelloId.Border = 0;
                    cel2SelloId.HorizontalAlignment = Element.ALIGN_LEFT;
                    cel2SelloId.VerticalAlignment = Element.ALIGN_TOP;
                    tableSelloCliente.AddCell(cel2SelloId);

                    iTextSharp.text.pdf.PdfPCell cel2SelloFirma = new iTextSharp.text.pdf.PdfPCell(new Phrase("Firma:\n", Helpers.Fuentes.Plantilla_2_fontTitleFactura));

                    cel2SelloFirma.Colspan = 1;
                    cel2SelloFirma.Padding = 3;
                    cel2SelloFirma.Border = 0;
                    cel2SelloFirma.HorizontalAlignment = Element.ALIGN_LEFT;
                    cel2SelloFirma.VerticalAlignment = Element.ALIGN_TOP;
                    tableSelloCliente.AddCell(cel2SelloFirma);

                    iTextSharp.text.pdf.PdfPCell cel3SelloCliente = new iTextSharp.text.pdf.PdfPCell(new Phrase("______________________________________________________\n\n" +
                        "RECIBIDO CONFORME FIRMA Y SELLO DEL CLIENTE", Helpers.Fuentes.Plantilla_2_fontTitleFactura));

                    cel3SelloCliente.Colspan = 1;
                    cel3SelloCliente.Padding = 3;
                    cel3SelloCliente.Border = 0;
                    cel3SelloCliente.HorizontalAlignment = Element.ALIGN_CENTER;
                    cel3SelloCliente.VerticalAlignment = Element.ALIGN_TOP;
                    tableSelloCliente.AddCell(cel3SelloCliente);

                    iTextSharp.text.pdf.PdfPCell _celSelloCliente = new iTextSharp.text.pdf.PdfPCell(tableSelloCliente);
                    //_celSelloCliente.Border = 0;
                    //_celSelloCliente.BorderColor = BaseColor.WHITE;
                    tableFirmas.AddCell(_celSelloCliente);
                    //-----------------------------------------------------------------------------
                    PdfPCell celTableTotalesObs = new PdfPCell();
                    PdfPCell celResolucion = new PdfPCell();
                    PdfPCell celTableFirmas = new PdfPCell();
                    celTableTotalesObs.Border = 0;
                    celTableTotalesObs.AddElement(tableTotalesObs);
                    celResolucion.Border = 0;
                    celResolucion.AddElement(tableResolucion);
                    celTableFirmas.Border = 0;
                    celTableFirmas.AddElement(tableFirmas);

                    tabla.AddCell(celTableTotalesObs);
                    tabla.AddCell(celResolucion);
                    tabla.AddCell(celTableFirmas);

                    return tabla;
                }
            }
        }
    }
}
