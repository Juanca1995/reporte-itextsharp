﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
//Referenciar y usar.
using System.Data;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Web;
using System.Windows.Forms;

namespace RulesServicesDIAN2.Adquiriente
{
    public partial class pdfEstandarAR
    {

        #region Formatos EletroHom

        public bool FacturaNacionalEletroHome(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {
            bool esLocal = Helpers.Compartido.EsLocal;
            string Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            string RutaImg = $@"{AppDomain.CurrentDomain.BaseDirectory}\electro logo.png";
            NomArchivo = string.Empty;
            NomArchivo = NombreInvoice + ".pdf";

            //bool esLocal = Helpers.Compartido.EsLocal;
            //string  Ruta = HttpContext.Current.Server.MapPath("~/PDF/" + NIT + "/");
            //string  LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            //string  RutaImg = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";
            //NomArchivo = string.Empty;
            //NomArchivo = NombreInvoice + ".pdf";
            try
            {
                #region Head
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.

                Document document = new Document(iTextSharp.text.PageSize.LETTER, 20f, 10f, 30f, 30f);

                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;

                // step 3: we open the document     
                document.Open();

                PdfDiv divEspacio = new PdfDiv();
                divEspacio.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio.Height = 10;
                divEspacio.Width = 130;

                PdfDiv divEspacio2 = new PdfDiv();
                divEspacio2.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio2.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio2.Height = 2;
                divEspacio2.Width = 130;

                //FUENTES----------------------------------------------------------------------------------------------------------
                iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle2 = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustom = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontAcercade = FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitleFactura = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                //FUENTES---------------------------------------------------------------------------------------------------------

                System.Drawing.Image logo = null;
                var requestLogo = WebRequest.Create(RutaImg);

                using (var responseLogo = requestLogo.GetResponse())
                using (var streamLogo = responseLogo.GetResponseStream())
                {
                    logo = Bitmap.FromStream(streamLogo);
                }

                iTextSharp.text.Image LogoPdf2 = iTextSharp.text.Image.GetInstance(logo, BaseColor.WHITE);
                LogoPdf2.ScaleAbsolute(70.0f, 70.0f);
                LogoPdf2.Border = 0;

                #endregion
                
                #region ENCABEZADO

                PdfPTable Encabezado = new PdfPTable(4);
                Encabezado.WidthPercentage = 100;

                PdfPCell cell_logo = new PdfPCell() { Border = 0, };
                cell_logo.AddElement(LogoPdf2);
                Encabezado.AddCell(cell_logo);

                PdfPCell cell_info = new PdfPCell(new Phrase("")) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, Colspan = 2, };

                Paragraph prgInfoEmpresa2 = new Paragraph($"{(string)DsInvoiceAR.Tables["Company"].Rows[0]["Name"]}\n",
                FontFactory.GetFont(FontFactory.HELVETICA, 5.79f, iTextSharp.text.Font.NORMAL));
                prgInfoEmpresa2.Alignment = Element.ALIGN_CENTER;
                Paragraph prgInfoEmpresa1 = new Paragraph($"Direccion: {(string)DsInvoiceAR.Tables["Company"].Rows[0]["Address1"]}\n" +
                    $"PBX: {(string)DsInvoiceAR.Tables["Company"].Rows[0]["PhoneNum"]} -  " +
                    $"FAX: {(string)DsInvoiceAR.Tables["Company"].Rows[0]["FaxNum"]}\n" +
                    $"Ciudad: {(string)DsInvoiceAR.Tables["Company"].Rows[0]["City"]} - " +
                    $"Departamento: {(string)DsInvoiceAR.Tables["Company"].Rows[0]["State"]}",
                FontFactory.GetFont(FontFactory.HELVETICA, 5.79f, iTextSharp.text.Font.NORMAL));

                prgInfoEmpresa2.Alignment = Element.ALIGN_CENTER;
                prgInfoEmpresa1.Alignment = Element.ALIGN_CENTER;

                cell_info.AddElement(prgInfoEmpresa2);
                cell_info.AddElement(prgInfoEmpresa1);

                Encabezado.AddCell(cell_info);

                PdfPCell cell_factura = new PdfPCell(new Phrase("FACTURA DE VENTA\n" +
                                                                "Nro.  " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"]))
                { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, };
                Encabezado.AddCell(cell_factura);

                #endregion

                #region DATOS CUSTOMER Y FACTURA
                //------------------------------------------------------------------------------------------------
                float[] DimencionFacturar = new float[3];
                DimencionFacturar[0] = 1.0F;//
                DimencionFacturar[1] = 1.0F;//
                DimencionFacturar[2] = 1.0F;//

                iTextSharp.text.Font fontTitleFecha = FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontDMA = FontFactory.GetFont(FontFactory.HELVETICA, 6, BaseColor.GRAY);
                iTextSharp.text.Font fontDetalleFecha = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 8, iTextSharp.text.Font.NORMAL);

                #endregion

                #region Tabla Unidades
                PdfPTable tableTituloUnidades = new PdfPTable(9);
                //Dimenciones.
                float[] DimencionUnidades = new float[9];
                DimencionUnidades[0] = 0.178F;//L
                DimencionUnidades[1] = 0.5F;//Codigo
                DimencionUnidades[2] = 0.5F;//Cantidad
                DimencionUnidades[3] = 0.328F;//Unidad
                DimencionUnidades[4] = 2.0F;//Descripcion
                DimencionUnidades[5] = 0.585F;//Valor Unitario
                DimencionUnidades[6] = 0.378F;//Descuento
                DimencionUnidades[7] = 0.5F;//Valor Total
                DimencionUnidades[8] = 0.28F;//%IVA

                tableTituloUnidades.WidthPercentage = 100;
                tableTituloUnidades.SetWidths(DimencionUnidades);

                iTextSharp.text.pdf.PdfPCell celTextL = new iTextSharp.text.pdf.PdfPCell(new Phrase("L", fontTitleFactura));
                celTextL.Colspan = 1;
                celTextL.Padding = 3;
                celTextL.Border = 0;
                //celTextL.BorderWidthRight = 1;
                celTextL.BorderWidthLeft = 1;
                celTextL.BackgroundColor = BaseColor.LIGHT_GRAY;
                celTextL.HorizontalAlignment = Element.ALIGN_CENTER;
                celTextL.VerticalAlignment = Element.ALIGN_TOP;

                tableTituloUnidades.AddCell(celTextL);

                iTextSharp.text.pdf.PdfPCell celTextCodigo = new iTextSharp.text.pdf.PdfPCell(new Phrase("Código", fontTitleFactura));
                celTextCodigo.Colspan = 1;
                celTextCodigo.Padding = 3;
                celTextCodigo.BackgroundColor = BaseColor.LIGHT_GRAY;
                celTextCodigo.Border = 0;
                celTextCodigo.BorderWidthLeft = 1;
                //celDescripcion.BorderColorBottom = BaseColor.WHITE;
                celTextCodigo.HorizontalAlignment = Element.ALIGN_CENTER;
                celTextCodigo.VerticalAlignment = Element.ALIGN_TOP;

                tableTituloUnidades.AddCell(celTextCodigo);

                iTextSharp.text.pdf.PdfPCell celTextCantidad = new iTextSharp.text.pdf.PdfPCell(new Phrase("Cantidad", fontTitleFactura));
                celTextCantidad.Colspan = 1;
                celTextCantidad.Padding = 3;
                celTextCantidad.Border = 0;
                celTextCantidad.BorderWidthLeft = 1;
                celTextCantidad.BackgroundColor = BaseColor.LIGHT_GRAY;
                celTextCantidad.HorizontalAlignment = Element.ALIGN_CENTER;
                celTextCantidad.VerticalAlignment = Element.ALIGN_TOP;

                tableTituloUnidades.AddCell(celTextCantidad);

                iTextSharp.text.pdf.PdfPCell celTextUnidad = new iTextSharp.text.pdf.PdfPCell(new Phrase("Unidad", fontTitleFactura));
                celTextUnidad.Colspan = 1;
                celTextUnidad.Padding = 3;
                celTextUnidad.BackgroundColor = BaseColor.LIGHT_GRAY;
                celTextUnidad.Border = 0;
                celTextUnidad.BorderWidthLeft = 1;
                celTextUnidad.HorizontalAlignment = Element.ALIGN_CENTER;
                celTextUnidad.VerticalAlignment = Element.ALIGN_TOP;

                tableTituloUnidades.AddCell(celTextUnidad);

                iTextSharp.text.pdf.PdfPCell celTextDesc = new iTextSharp.text.pdf.PdfPCell(new Phrase("Descripción", fontTitleFactura));
                celTextDesc.Colspan = 1;
                celTextDesc.Padding = 3;
                celTextDesc.BackgroundColor = BaseColor.LIGHT_GRAY;
                celTextDesc.Border = 0;
                celTextDesc.BorderWidthLeft = 1;
                celTextDesc.HorizontalAlignment = Element.ALIGN_CENTER;
                celTextDesc.VerticalAlignment = Element.ALIGN_TOP;

                tableTituloUnidades.AddCell(celTextDesc);

                iTextSharp.text.pdf.PdfPCell celTextValorUnitario = new iTextSharp.text.pdf.PdfPCell(new Phrase("Valor Unitario", fontTitleFactura));
                celTextValorUnitario.Colspan = 1;
                celTextValorUnitario.Border = 0;
                celTextValorUnitario.BorderWidthLeft = 1;
                celTextValorUnitario.BackgroundColor = BaseColor.LIGHT_GRAY;
                celTextValorUnitario.Padding = 3;
                celTextValorUnitario.HorizontalAlignment = Element.ALIGN_CENTER;
                celTextValorUnitario.VerticalAlignment = Element.ALIGN_TOP;

                tableTituloUnidades.AddCell(celTextValorUnitario);

                iTextSharp.text.pdf.PdfPCell celTextDescuento = new iTextSharp.text.pdf.PdfPCell(new Phrase("Descuento", fontTitleFactura));
                celTextDescuento.Colspan = 1;
                celTextDescuento.Padding = 3;
                celTextDescuento.Border = 0;
                celTextDescuento.BorderWidthLeft = 1;
                celTextDescuento.BackgroundColor = BaseColor.LIGHT_GRAY;
                celTextDescuento.HorizontalAlignment = Element.ALIGN_CENTER;
                celTextDescuento.VerticalAlignment = Element.ALIGN_TOP;

                tableTituloUnidades.AddCell(celTextDescuento);

                iTextSharp.text.pdf.PdfPCell celTextValorTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase("Valor Total", fontTitleFactura));
                celTextValorTotal.Colspan = 1;
                celTextValorTotal.Padding = 3;
                celTextValorTotal.Border = 0;
                celTextValorTotal.BorderWidthLeft = 1;
                celTextValorTotal.BackgroundColor = BaseColor.LIGHT_GRAY;
                celTextValorTotal.HorizontalAlignment = Element.ALIGN_CENTER;
                celTextValorTotal.VerticalAlignment = Element.ALIGN_TOP;

                tableTituloUnidades.AddCell(celTextValorTotal);

                iTextSharp.text.pdf.PdfPCell celTextIva = new iTextSharp.text.pdf.PdfPCell(new Phrase("% IVA", fontTitleFactura));
                celTextIva.Colspan = 1;
                celTextIva.Padding = 3;
                celTextIva.Border = 0;
                celTextIva.BorderWidthLeft = 1;
                celTextIva.BorderWidthRight = 1;
                celTextIva.BackgroundColor = BaseColor.LIGHT_GRAY;
                celTextIva.HorizontalAlignment = Element.ALIGN_CENTER;
                celTextIva.VerticalAlignment = Element.ALIGN_TOP;

                tableTituloUnidades.AddCell(celTextIva);

                PdfPTable tableUnidades = new PdfPTable(9);
                tableUnidades.WidthPercentage = 100;
                tableUnidades.SetWidths(DimencionUnidades);
                decimal totalDescuento = 0;
                decimal decTotalUnd = 0;
                foreach (DataRow InvoiceLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
                {
                    if (!AddUnidadesEletroHome(InvoiceLine, ref tableUnidades, fontCustom, DsInvoiceAR, ref decTotalUnd))
                        return false;
                    if (Helpers.Compartido.VerificarExistenciaColumnas(DsInvoiceAR, "InvcDtl", "DspDocDiscount"))
                        totalDescuento += decimal.Parse((string)InvoiceLine["DspDocDiscount"]);
                }

                int numAdLineas = 6 - DsInvoiceAR.Tables["InvcDtl"].Rows.Count;
                PdfPTable tableEspacioUnidades = new PdfPTable(9);
                tableEspacioUnidades.WidthPercentage = 100;
                tableEspacioUnidades.SetWidths(DimencionUnidades);

                for (int i = 0; i < numAdLineas; i++)
                    AddUnidadesEletroHome(ref tableEspacioUnidades);

                PdfPTable tableLineaFinal = new PdfPTable(9);
                tableLineaFinal.WidthPercentage = 100;
                tableLineaFinal.SetWidths(DimencionUnidades);
                iTextSharp.text.pdf.PdfPCell LineaFinal = new iTextSharp.text.pdf.PdfPCell();
                LineaFinal.Colspan = 9;
                LineaFinal.Padding = 3;
                LineaFinal.Border = 1;
                LineaFinal.BorderColorBottom = BaseColor.WHITE;
                LineaFinal.HorizontalAlignment = Element.ALIGN_CENTER;
                LineaFinal.VerticalAlignment = Element.ALIGN_TOP;
                tableLineaFinal.AddCell(LineaFinal);
                #endregion

                #region OBSERVACIONES Y TOTALES
                PdfPTable tableObsTotales = new PdfPTable(3);
                tableObsTotales.WidthPercentage = 100;
                float[] dimTableObsTotales = new float[3];
                dimTableObsTotales[0] = 2.0f;
                dimTableObsTotales[1] = 0.8f;
                dimTableObsTotales[2] = 0.85f;
                tableObsTotales.SetWidths(dimTableObsTotales);
                //---------------------------------------------------------------------
                PdfPTable tableObsercaciones = new PdfPTable(1);
                PdfPTable tableTotalLinea = new PdfPTable(2);
                tableTotalLinea.SetWidths(new float[] { 0.5f, 1f });

                PdfPCell celTextTotalLinea = new PdfPCell(new Phrase("TOTAL UNIDADES", fontTitleFactura));
                celTextTotalLinea.Border = 0;
                celTextTotalLinea.Colspan = 1;

                PdfPCell celTextDato = new PdfPCell(new Phrase(decTotalUnd.ToString("N0"), fontTitleFactura));
                celTextDato.Border = 0;

                tableTotalLinea.AddCell(celTextTotalLinea);
                tableTotalLinea.AddCell(celTextDato);

                PdfPCell celTextObservaciones = new PdfPCell(new Phrase("OBSERVACIONES\n\n\n", fontTitleFactura));
                celTextObservaciones.Border = 0;

                tableObsercaciones.AddCell(tableTotalLinea);
                tableObsercaciones.AddCell(celTextObservaciones);

                string Observaciones = string.Empty;
                if (Helpers.Compartido.VerificarExistenciaColumnas(DsInvoiceAR, "InvcHead", "InvoiceComment", 0))
                    Observaciones = DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceComment"].ToString();

                PdfPCell celTextPedirCita = new PdfPCell(new Phrase(Observaciones, fontTitle2));
                celTextPedirCita.Border = 0;
                tableObsercaciones.AddCell(celTextPedirCita);

                PdfPCell celObservaciones = new PdfPCell(tableObsercaciones);
                tableObsTotales.AddCell(celObservaciones);
                //----------------------------------------------------------------------
                PdfPCell celFimarSello = new PdfPCell(new Phrase("\n\n\n\n_______________________________\nFirma y Sello", fontTitle2));
                celFimarSello.HorizontalAlignment = Element.ALIGN_CENTER;
                celFimarSello.VerticalAlignment = Element.ALIGN_BOTTOM;
                tableObsTotales.AddCell(celFimarSello);
                //----------------------------------------------------------------------
                PdfPTable tableTotales = new PdfPTable(2);
                float[] dimTableTotales = new float[2];
                dimTableTotales[0] = 1.1f;
                dimTableTotales[1] = 1.7f;
                tableTotales.SetWidths(dimTableTotales);

                PdfPCell celTextTotalBruto = new PdfPCell(new Phrase("Total Bruto", fontTitle2));
                celTextTotalBruto.Border = 0;
                tableTotales.AddCell(celTextTotalBruto);
                //PdfPCell celValTotalBruto = new PdfPCell(new Phrase(
                //    decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubTotal"]).ToString("N2"), fontTitleFactura));
                PdfPCell celValTotalBruto = new PdfPCell(new Phrase(decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number02"]).ToString("N2"), fontTitleFactura));
                celValTotalBruto.Border = 0;
                celValTotalBruto.HorizontalAlignment = Element.ALIGN_RIGHT;
                tableTotales.AddCell(celValTotalBruto);

                PdfPCell celTotalTextDescuento = new PdfPCell(new Phrase("Descuento", fontTitle2));
                celTotalTextDescuento.Border = 0;
                tableTotales.AddCell(celTotalTextDescuento);
                PdfPCell celTotalValDescuento = new PdfPCell(new Phrase(totalDescuento.ToString("N2"), fontTitleFactura));
                celTotalValDescuento.Border = 0;
                celTotalValDescuento.HorizontalAlignment = Element.ALIGN_RIGHT;
                tableTotales.AddCell(celTotalValDescuento);

                PdfPCell celTextTotalValorFletes = new PdfPCell(new Phrase("Valor Fletes", fontTitle2));
                celTextTotalValorFletes.Border = 0;
                tableTotales.AddCell(celTextTotalValorFletes);
                PdfPCell celValTotalValorFletes = new PdfPCell(new Phrase(decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number03"]).ToString("N2"), fontTitleFactura));
                celValTotalValorFletes.Border = 0;
                celValTotalValorFletes.HorizontalAlignment = Element.ALIGN_RIGHT;
                tableTotales.AddCell(celValTotalValorFletes);

                PdfPCell celTextSubTotal = new PdfPCell(new Phrase("Subtotal", fontTitle2));
                celTextSubTotal.Border = 0;
                tableTotales.AddCell(celTextSubTotal);
                PdfPCell celValSubTotal = new PdfPCell(new Phrase(
                    decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocSubTotal"]).ToString("N2"), fontTitleFactura));
                celValSubTotal.Border = 0;
                celValSubTotal.HorizontalAlignment = Element.ALIGN_RIGHT;
                tableTotales.AddCell(celValSubTotal);

                PdfPCell celTextValorIva = new PdfPCell(new Phrase("Valor IVA", fontTitle2));
                celTextValorIva.Border = 0;
                tableTotales.AddCell(celTextValorIva);
                var Iva = GetValImpuestoByID("01", DsInvoiceAR);
                PdfPCell celValValorIva = new PdfPCell(new Phrase(Iva.ToString("N2"), fontTitleFactura));
                celValValorIva.Border = 0;
                celValValorIva.HorizontalAlignment = Element.ALIGN_RIGHT;
                tableTotales.AddCell(celValValorIva);

                PdfPCell celTextReteIva = new PdfPCell(new Phrase("", fontTitle2));
                celTextReteIva.Border = 0;
                tableTotales.AddCell(celTextReteIva);
                PdfPCell celValReteIva = new PdfPCell(new Phrase("", fontTitleFactura));
                celValReteIva.Border = 0;
                celValReteIva.HorizontalAlignment = Element.ALIGN_RIGHT;
                tableTotales.AddCell(celValReteIva);

                PdfPCell celTotales = new PdfPCell(tableTotales);
                celTotales.Padding = 2;
                tableObsTotales.AddCell(celTotales);

                PdfPCell celTotalLetras = new PdfPCell(new Phrase(
                    $"Son: **{Nroenletras((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"])}**",
                    fontTitle2));
                celTotalLetras.Colspan = 2;
                tableObsTotales.AddCell(celTotalLetras);

                PdfPTable tableTotalFactura = new PdfPTable(2);
                tableTotalFactura.SetWidths(dimTableTotales);

                PdfPCell celTextTotal = new PdfPCell(new Phrase("Total Factura", fontTitleFactura));
                celTextTotal.BackgroundColor = BaseColor.LIGHT_GRAY;
                celTextTotal.Border = 0;
                tableTotalFactura.AddCell(celTextTotal);

                PdfPCell celValTotal = new PdfPCell(new Phrase(
                    decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"]).ToString("N2"),
                    fontTitleFactura));
                celValTotal.HorizontalAlignment = Element.ALIGN_RIGHT;
                celValTotal.BackgroundColor = BaseColor.LIGHT_GRAY;
                celValTotal.Border = 0;
                tableTotalFactura.AddCell(celValTotal);

                PdfPCell celTotal = new PdfPCell(tableTotalFactura);
                tableObsTotales.AddCell(celTotal);
                //---------------------------------------------------------------------------------------------

                PdfPTable tableResolucion = new PdfPTable(1);
                tableResolucion.WidthPercentage = 100;

                PdfPCell celResolucion = new PdfPCell(new Phrase(DsInvoiceAR.Tables["InvcHead"].Rows[0]["Character02"].ToString(),
                    fontTitle2));
                celResolucion.HorizontalAlignment = Element.ALIGN_CENTER;
                celResolucion.Border = 0;
                tableResolucion.AddCell(celResolucion);
                //------------------------------------------------------------------------------------------------------------
                PdfDiv divTextCopia = new PdfDiv();
                divTextCopia.BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.DOTTED;
                divTextCopia.Width = 582;

                PdfPTable fin = new PdfPTable(new float[] { 0.34f, 0.33f, 0.03f, 0.3f });
                fin.WidthPercentage = 100;

                PdfPCell cell_sello = new PdfPCell(new Phrase("ACEPTO:\n\n\n\n" +
                                                            "_______________________________________________\n" +
                                                            "c.c. sello", fontTitleFactura))
                { HorizontalAlignment = Element.ALIGN_LEFT };
                fin.AddCell(cell_sello);

                PdfPCell cell_leyenda = new PdfPCell(new Phrase("La presente factura de ventas se asimila para todos sus efectos\n" +
                                                                "legales a una letra de cambio según articulo 774 del C.de C.y\n" +
                                                                "causará un interes por mora mesual a la tasa máxiam legal\n" +
                                                                "autorizada.", fontCustom))
                { HorizontalAlignment = Element.ALIGN_LEFT };
                fin.AddCell(cell_leyenda);

                PdfPCell cell_line = new PdfPCell() { Border = 0, };
                fin.AddCell(cell_line);

                PdfPCell cell_totales = new PdfPCell();

                PdfPTable totalvs = new PdfPTable(2);
                totalvs.WidthPercentage = 100;

                PdfPCell vst = new PdfPCell(new Phrase("Total ventas excluida", fontTitleFactura)) { HorizontalAlignment = Element.ALIGN_LEFT, };
                totalvs.AddCell(vst);
                PdfPCell vsv = new PdfPCell(new Phrase("$ "+decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number01"]).ToString("N2"), fontTitleFactura)) { HorizontalAlignment = Element.ALIGN_RIGHT };
                totalvs.AddCell(vsv);
                cell_totales.AddElement(totalvs);

                PdfPTable tvgt = new PdfPTable(2);
                tvgt.WidthPercentage = 100;
                PdfPCell vgt = new PdfPCell(new Phrase("Total ventas gravada", fontTitleFactura)) { HorizontalAlignment = Element.ALIGN_LEFT, };
                tvgt.AddCell(vgt);
                PdfPCell vgv = new PdfPCell(new Phrase("$ "+decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number02"]).ToString("N2"), fontTitleFactura)) { HorizontalAlignment = Element.ALIGN_RIGHT };
                tvgt.AddCell(vgv);
                cell_totales.AddElement(tvgt);

                PdfPTable tiva = new PdfPTable(2);
                tiva.WidthPercentage = 100;
                PdfPCell iva = new PdfPCell(new Phrase("Iva", fontTitleFactura)) { HorizontalAlignment = Element.ALIGN_LEFT, };
                tiva.AddCell(iva);
                PdfPCell cell_iva = new PdfPCell(new Phrase("$ "+decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DocTaxAmt"]).ToString("N2"), fontTitleFactura)) { HorizontalAlignment = Element.ALIGN_RIGHT };
                tiva.AddCell(cell_iva);
                cell_totales.AddElement(tiva);

                PdfPTable ttotal = new PdfPTable(2);
                ttotal.WidthPercentage = 100;
                PdfPCell total = new PdfPCell(new Phrase("Total", fontTitleFactura)) { HorizontalAlignment = Element.ALIGN_LEFT, };
                ttotal.AddCell(total);
                PdfPCell cell_total = new PdfPCell(new Phrase("$ "+decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"]).ToString("N2"), fontTitleFactura)) { HorizontalAlignment = Element.ALIGN_RIGHT };
                ttotal.AddCell(cell_total);
                cell_totales.AddElement(ttotal);

                fin.AddCell(cell_totales);

                PdfPTable elaboro = new PdfPTable(3);
                elaboro.WidthPercentage= 100;
                PdfPCell cell_elaboro = new PdfPCell(new Phrase("_______________________________________________\n\n" +
                                                                "ELABORO", fontTitleFactura))
                { MinimumHeight=50,HorizontalAlignment=Element.ALIGN_LEFT,VerticalAlignment=Element.ALIGN_BOTTOM};
                elaboro.AddCell(cell_elaboro);
                PdfPCell elaboro2 = new PdfPCell() { Border=0,Colspan=2,};
                elaboro.AddCell(elaboro2);
                #endregion

                #region Exit
                document.Add(Encabezado);
                document.Add(divEspacio);
                document.Add(EletroHomeConstructos.FacturaNacional.DatosCliente(DsInvoiceAR, CUFE, QRInvoice));
                document.Add(divEspacio2);
                document.Add(divEspacio2);
                document.Add(tableTituloUnidades);
                document.Add(tableUnidades);
                document.Add(tableEspacioUnidades);
                //document.Add(tableLineaFinal);
                document.Add(tableObsTotales);
                document.Add(divEspacio2);
                document.Add(tableResolucion);
                //document.Add(divEspacio2);
                document.Add(divTextCopia);
                document.Add(divEspacio2);
                document.Add(divEspacio2);
                document.Add(fin);
                document.Add(divEspacio2);
                document.Add(divEspacio2);
                document.Add(elaboro);
                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();
                Helpers.Simex.AddPageNumberPagToSimexExp($"{Ruta}{NomArchivo}", dimxSimex, dimYSimex + 10, Helpers.Fuentes.SimexLeyenda);
                RutaPdf = NomArchivo;
                return true;
                #endregion
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return false;
            }
        }

        public bool NotaCreditoEletroHome(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {
            //bool esLocal = Helpers.Compartido.EsLocal;
            //string  Ruta = HttpContext.Current.Server.MapPath("~/PDF/" + NIT + "/");
            //string  LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            //string  RutaImg = "http://" + LocalIP + ":8081/images/EMPRESAS/Empresa_" + NIT + "/Logo_" + NIT + ".png";
            //NomArchivo = string.Empty;
            //NomArchivo = NombreInvoice + ".pdf";

            NomArchivo = string.Empty;
            string Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            string RutaImg = $@"{AppDomain.CurrentDomain.BaseDirectory}\LOGO_INTERBEL.png";
            string Rutacertificado = $@"{AppDomain.CurrentDomain.BaseDirectory}\electro logo.png";
            NomArchivo = NombreInvoice + ".pdf";
            try
            {
                #region Head
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.
                //Document document = new Document(iTextSharp.text.PageSize.LETTER);
                Document document = new Document(iTextSharp.text.PageSize.A4.Rotate(), 10, 10, 10, 10);
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;



                // step 3: we open the document     
                document.Open();

                PdfDiv divEspacio = new PdfDiv();
                divEspacio.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio.Height = 10;
                divEspacio.Width = 130;

                PdfDiv divEspacio2 = new PdfDiv();
                divEspacio2.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio2.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio2.Height = 2;
                divEspacio2.Width = 130;

                //FUENTES----------------------------------------------------------------------------------------------------------
                iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle3 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 24, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle2 = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustom = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustom2 = FontFactory.GetFont(FontFactory.HELVETICA, 5, iTextSharp.text.Font.NORMAL);
                //FUENTES---------------------------------------------------------------------------------------------------------

                ///objeto para el borde 
                RoundRectangle CelEventBorderRound = new RoundRectangle();

                System.Drawing.Image Logo;
                System.Drawing.Image LogoBanner = null;
                var request = WebRequest.Create(RutaImg);
                using (var response = request.GetResponse())
                using (var stream = response.GetResponseStream())
                {
                    Logo = Bitmap.FromStream(stream);
                }
                var requestBanner = WebRequest.Create(RutaImg);
                using (var responseBanner = requestBanner.GetResponse())
                using (var streamBanner = responseBanner.GetResponseStream())
                {
                    LogoBanner = Bitmap.FromStream(streamBanner);
                }


                System.Drawing.Image Logo2;
                System.Drawing.Image LogoBanner2 = null;
                var request2 = WebRequest.Create(Rutacertificado);
                using (var response2 = request2.GetResponse())
                using (var stream2 = response2.GetResponseStream())
                {
                    Logo2 = Bitmap.FromStream(stream2);
                }
                var requestBanner2 = WebRequest.Create(Rutacertificado);
                using (var responseBanner2 = requestBanner2.GetResponse())
                using (var streamBanner2 = responseBanner2.GetResponseStream())
                {
                    LogoBanner2 = Bitmap.FromStream(streamBanner2);
                }


                //Logos--------------------------------
                iTextSharp.text.Image LogoPdf3 = iTextSharp.text.Image.GetInstance(LogoBanner2, BaseColor.WHITE);
                LogoPdf3.ScaleAbsolute(90f, 90f);
                LogoPdf3.Border = 0;


                iTextSharp.text.Image LogoPdf2 = iTextSharp.text.Image.GetInstance(LogoBanner, BaseColor.WHITE);
                LogoPdf2.ScaleAbsolute(250, 250);
                LogoPdf2.Border = 0;

                iTextSharp.text.Image QRPdf = iTextSharp.text.Image.GetInstance(QRInvoice, BaseColor.WHITE);
                QRPdf.ScaleAbsolute(100, 100);
                QRPdf.Border = 0;


                PdfPTable espacio = new PdfPTable(1);
                espacio.WidthPercentage = 100;
                PdfPCell salto = new PdfPCell(new Phrase(" 0 " + " LOTE: ")) { Border = 0, MinimumHeight = 20, HorizontalAlignment = Element.ALIGN_CENTER };//se agrega el lote
                espacio.AddCell(salto);

                #endregion

                #region Header
                PdfPTable Header = new PdfPTable(1);
                Header.WidthPercentage = 100;
                PdfPCell header_master = new PdfPCell()
                {
                    CellEvent = CelEventBorderRound,
                };

                PdfPTable encabezado = new PdfPTable(3);
                encabezado.WidthPercentage = 100;
                PdfPCell logo = new PdfPCell() { Border = 0, MinimumHeight = 80, };
                logo.Border = PdfPCell.RIGHT_BORDER;
                PdfPCell empresa = new PdfPCell(new Phrase("INTERBEL")) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER };
                empresa.Border = PdfPCell.RIGHT_BORDER;
                PdfPCell tip_factura = new PdfPCell(new Phrase("NOTA Debito No.", fontTitle3)) { Border = 0, MinimumHeight = 50, HorizontalAlignment = Element.ALIGN_CENTER };

                PdfPTable info_factura = new PdfPTable(new float[] { 1.5f, 4.5f, 2.0f });
                info_factura.WidthPercentage = 100;

                PdfPCell C1 = new PdfPCell(new Phrase("")) { MinimumHeight = 50, Border = 0, };
                PdfPTable Ciudad = new PdfPTable(1);
                Ciudad.WidthPercentage = 100;

                PdfPCell Ciudad_l = new PdfPCell(new Phrase("Ciudad")) { MinimumHeight = 20, };
                Ciudad.AddCell(Ciudad_l);
                C1.AddElement(Ciudad);

                PdfPTable Recibido = new PdfPTable(1);
                Recibido.WidthPercentage = 100;
                PdfPCell Recibido_l = new PdfPCell(new Phrase("Recibido por")) { MinimumHeight = 20, };
                Recibido.AddCell(Recibido_l);
                C1.AddElement(Recibido);

                PdfPTable suma = new PdfPTable(1);
                suma.WidthPercentage = 100;
                PdfPCell suma_l = new PdfPCell(new Phrase("La suma de")) { MinimumHeight = 20, };
                Recibido.AddCell(suma_l);
                C1.AddElement(suma);

                PdfPTable concepto = new PdfPTable(1);
                concepto.WidthPercentage = 100;
                PdfPCell concepto_l = new PdfPCell(new Phrase("Por concepto de")) { MinimumHeight = 20, };
                concepto.AddCell(concepto_l);
                C1.AddElement(concepto);
                ///celta del medio del los datos del cliente 
                PdfPCell C2 = new PdfPCell(new Phrase("")) { Border = 0, };

                PdfPTable Ciudadr = new PdfPTable(1);
                Ciudadr.WidthPercentage = 100;
                PdfPCell Ciudad_lr = new PdfPCell(new Phrase(""+ DsInvoiceAR.Tables["Customer"].Rows[0]["City"])) { MinimumHeight = 20, };
                Ciudadr.AddCell(Ciudad_lr);
                C2.AddElement(Ciudadr);

                PdfPTable Recibidor = new PdfPTable(new float[] { 7.5f, 0.9f, 2.0f });
                Recibidor.WidthPercentage = 100;
                PdfPCell Recibido_lr = new PdfPCell(new Phrase("" + DsInvoiceAR.Tables["Customer"].Rows[0]["Name"])) { MinimumHeight = 20, };
                PdfPCell Recibido_lr_n = new PdfPCell(new Phrase("Nit")) { MinimumHeight = 20, HorizontalAlignment = Element.ALIGN_CENTER };
                PdfPCell Recibido_lr_v = new PdfPCell(new Phrase("" + DsInvoiceAR.Tables["Customer"].Rows[0]["CustID"])) { MinimumHeight = 20, HorizontalAlignment = Element.ALIGN_CENTER };
                Recibidor.AddCell(Recibido_lr);
                Recibidor.AddCell(Recibido_lr_n);
                Recibidor.AddCell(Recibido_lr_v);
                C2.AddElement(Recibidor);

                PdfPTable sumar = new PdfPTable(1);
                sumar.WidthPercentage = 100;
                PdfPCell suma_lr = new PdfPCell(new Phrase("" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number06"])) { MinimumHeight = 20, };
                sumar.AddCell(suma_lr);
                C2.AddElement(sumar);

                PdfPTable conceptor = new PdfPTable(1);
                conceptor.WidthPercentage = 100;
                PdfPCell concepto_lr = new PdfPCell(new Phrase("" + DsInvoiceAR.Tables["Customer"].Rows[0]["Name"])) { MinimumHeight = 20, };
                conceptor.AddCell(concepto_lr);
                C2.AddElement(conceptor);
                //tabla final de tados del clñiente 
                PdfPCell C3 = new PdfPCell(new Phrase("")) { Border = 0, };

                PdfPTable Ciudadrf = new PdfPTable(new float[] { 0.2f, 0.7f });
                Ciudadrf.WidthPercentage = 100;
                PdfPCell Ciudad_lrf = new PdfPCell(new Phrase("Fecha")) { MinimumHeight = 20, };
                PdfPCell Ciudad_lrf_v = new PdfPCell(new Phrase("" + DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).ToString("dd/mm/yyyy"))) { MinimumHeight = 20, };
                Ciudadrf.AddCell(Ciudad_lrf);
                Ciudadrf.AddCell(Ciudad_lrf_v);
                C3.AddElement(Ciudadrf);

                PdfPTable Recibidorf = new PdfPTable(new float[] { 0.2f, 0.7f });
                Recibidorf.WidthPercentage = 100;
                PdfPCell Recibido_lrf = new PdfPCell(new Phrase("Código ")) { MinimumHeight = 20, };
                PdfPCell Recibido_lrf_v = new PdfPCell(new Phrase("" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number04"])) { MinimumHeight = 20, };//codigo
                Recibidorf.AddCell(Recibido_lrf);
                Recibidorf.AddCell(Recibido_lrf_v);
                C3.AddElement(Recibidorf);

                PdfPTable sumarf = new PdfPTable(new float[] { 0.2f, 0.7f });
                sumarf.WidthPercentage = 100;
                PdfPCell suma_lrf = new PdfPCell(new Phrase("  " )) { MinimumHeight = 20, };
                PdfPCell suma_lrf_v = new PdfPCell(new Phrase("  ")) { MinimumHeight = 20, };//espacio en blanco no se definio un uso 
                sumarf.AddCell(suma_lrf);
                sumarf.AddCell(suma_lrf_v);
                C3.AddElement(sumarf);

                PdfPTable conceptorf = new PdfPTable(new float[] { 0.2f, 0.7f });
                conceptorf.WidthPercentage = 100;
                PdfPCell concepto_lrf = new PdfPCell(new Phrase(" No: ")) { MinimumHeight = 20, };
                PdfPCell concepto_lrf_v = new PdfPCell(new Phrase("" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"])) { MinimumHeight = 20, };
                conceptorf.AddCell(concepto_lrf);
                conceptorf.AddCell(concepto_lrf_v);
                C3.AddElement(conceptorf);


                info_factura.AddCell(C1);
                info_factura.AddCell(C2);
                info_factura.AddCell(C3);

                PdfPTable pieencabezado = new PdfPTable(new float[] { 1.2f, 1.2f, 1.2f, 1.2f, 2.0f, 3.0f });
                pieencabezado.WidthPercentage = 100;
                PdfPCell enpie0 = new PdfPCell(new Phrase("FACTURAS")) { Border = 0, };
                PdfPCell enpie1 = new PdfPCell(new Phrase("REFERENCIA")) { Border = 0, };
                PdfPCell enpie2 = new PdfPCell(new Phrase("CANTIDAD")) { Border = 0, };
                PdfPCell enpie3 = new PdfPCell(new Phrase("VALOR")) { Border = 0, };
                PdfPCell enpie4 = new PdfPCell(new Phrase("I.V.A  ")) { Border = 0, };
                PdfPCell enpie5 = new PdfPCell(new Phrase("DESCRIPCION ")) { Border = 0, };
                pieencabezado.AddCell(enpie0);
                pieencabezado.AddCell(enpie1);
                pieencabezado.AddCell(enpie2);
                pieencabezado.AddCell(enpie3);
                pieencabezado.AddCell(enpie4);
                pieencabezado.AddCell(enpie5);

                logo.AddElement(LogoPdf2);

                encabezado.AddCell(logo);
                encabezado.AddCell(empresa);
                encabezado.AddCell(tip_factura);
                header_master.AddElement(encabezado);
                header_master.AddElement(info_factura);
                header_master.AddElement(pieencabezado);

                Header.AddCell(header_master);
                #endregion

                #region Body
                //agregamos el cuerpo de el documento no qagregaremos los detalles de el formato 
                PdfPTable Body = new PdfPTable(1);
                Body.WidthPercentage = 100;
                PdfPCell cuerpo = new PdfPCell()
                {
                    CellEvent = CelEventBorderRound,
                    MinimumHeight = 100,
                };

                PdfPTable obs = new PdfPTable(1);
                obs.WidthPercentage = 100;
                PdfPCell observaciones = new PdfPCell(new Phrase("OBSERVACIONES")) { HorizontalAlignment = Element.ALIGN_CENTER };
                obs.AddCell(observaciones);
                cuerpo.AddElement(obs);

                PdfPTable reser = new PdfPTable(1);
                reser.WidthPercentage = 100;
                PdfPCell reservas = new PdfPCell(new Phrase("" + DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceComment"])){ HorizontalAlignment = Element.ALIGN_CENTER, MinimumHeight = 50, };
                reser.AddCell(reservas);
                cuerpo.AddElement(reser);

                PdfPTable valores = new PdfPTable(3);
                valores.WidthPercentage = 100;
                PdfPCell valoresf = new PdfPCell(new Phrase("")) { HorizontalAlignment = Element.ALIGN_CENTER, MinimumHeight = 60, };

                PdfPTable contable = new PdfPTable(1);
                contable.WidthPercentage = 100;
                PdfPCell valorUnidad = new PdfPCell(new Phrase("valor por Unidad: "+decimal.Parse((string)DsInvoiceAR.Tables["InvcDtl"].Rows[0]["UnitPrice"]).ToString("N0"))) { Border = 0, };///valor de las unidades 
                contable.AddCell(valorUnidad);
                valoresf.AddElement(contable);

                PdfPTable contable2 = new PdfPTable(1);
                contable2.WidthPercentage = 100;
                PdfPCell unidades = new PdfPCell(new Phrase("Unidades: " + decimal.Parse((string)DsInvoiceAR.Tables["InvcDtl"].Rows[0]["SellingShipQty"]).ToString("N0"))) { Border = 0, };//las unidades 
                contable2.AddCell(unidades);
                valoresf.AddElement(contable2);

                PdfPTable contable3 = new PdfPTable(1);
                contable3.WidthPercentage = 100;
                PdfPCell valor = new PdfPCell(new Phrase("valor Unidades: " + decimal.Parse((string)DsInvoiceAR.Tables["InvcDtl"].Rows[0]["DocExtPrice"]).ToString("N0"))) { Border = 0, };//valor de la unidad indfividuial 
                contable3.AddCell(valor);
                valoresf.AddElement(contable3);

                PdfPCell valoress = new PdfPCell(new Phrase("")) { HorizontalAlignment = Element.ALIGN_CENTER, MinimumHeight = 60, };


                PdfPTable contable4 = new PdfPTable(1);
                contable4.WidthPercentage = 100;
                PdfPCell descuento = new PdfPCell(new Phrase("valor Descuento: " + decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["Number05"]).ToString("N0"))) { Border = 0, };///valor de las unidades 
                contable4.AddCell(descuento);
                valoress.AddElement(contable4);

                PdfPTable contable5 = new PdfPTable(1);
                contable5.WidthPercentage = 100;
                PdfPCell iva = new PdfPCell(new Phrase("iva: " + decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DocTaxAmt"]).ToString("N0"))) { Border = 0, };//las unidades 
                contable5.AddCell(iva);
                valoress.AddElement(contable5);

                PdfPTable contable6 = new PdfPTable(1);
                contable6.WidthPercentage = 100;
                PdfPCell neto = new PdfPCell(new Phrase("Neto :"+ decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["NetWeight"]).ToString("N0"))){ Border = 0, };//valor de la unidad indfividuial
                contable6.AddCell(neto);
                valoress.AddElement(contable6);

                PdfPCell valoresa = new PdfPCell(new Phrase("")) { HorizontalAlignment = Element.ALIGN_CENTER, MinimumHeight = 60, };
                PdfPTable contable7 = new PdfPTable(1);
                contable7.WidthPercentage = 100;
                PdfPCell Rfuente = new PdfPCell(new Phrase("Rete Fuente : " + " xxxxxxxxx")) { Border = 0, };///valor de las unidades 
                contable7.AddCell(Rfuente);
                valoresa.AddElement(contable7);

                PdfPTable contable8 = new PdfPTable(1);
                contable8.WidthPercentage = 100;
                PdfPCell Riva = new PdfPCell(new Phrase("Rete iva: "+ $"{ decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DocTaxAmt"]).ToString("N0") }")) { Border = 0, };//las unidades 
                contable8.AddCell(iva);
                valoresa.AddElement(contable8);

                PdfPTable contable9 = new PdfPTable(1);
                contable9.WidthPercentage = 100;
                PdfPCell Total = new PdfPCell(new Phrase("Total : " + decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"]).ToString("N0"))) { MinimumHeight = 25, };//valor de la unidad indfividuial
                contable9.AddCell(Total);
                valoresa.AddElement(contable9);



                valores.AddCell(valoresf);
                valores.AddCell(valoress);
                valores.AddCell(valoresa);
                cuerpo.AddElement(valores);


                PdfPTable final = new PdfPTable(1);
                final.WidthPercentage = 100;
                PdfPCell finalf = new PdfPCell(new Phrase("FIRMA Y SELLO C.C. O NIT")) { VerticalAlignment = Element.ALIGN_BOTTOM, HorizontalAlignment = Element.ALIGN_RIGHT, MinimumHeight = 80, };
                final.AddCell(finalf);
                cuerpo.AddElement(final);

                Body.AddCell(cuerpo);
                #endregion

                #region Footer

                PdfPTable tableTituloUnidades = new PdfPTable(6);
                //Dimenciones.
                float[] DimencionUnidades = new float[6];
                DimencionUnidades[0] = 1.2f;//CÓDIGO
                DimencionUnidades[1] = 1.2f;//DESCRIPCION
                DimencionUnidades[2] = 1.2f;//CANTIDAD
                DimencionUnidades[3] = 1.2f;//DTO
                DimencionUnidades[4] = 2.0f;//PRECIO
                DimencionUnidades[5] = 3.0f;//PRECIO

                PdfPTable tableUnidades = new PdfPTable(6);
                tableUnidades.WidthPercentage = 100;
                tableUnidades.SetWidths(DimencionUnidades);

                foreach (DataRow InvoiceLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
                {
                    if (!AddUnidadesEletroHome(InvoiceLine, ref tableUnidades, fontCustom, DsInvoiceAR))
                        return false;
                }

                iTextSharp.text.pdf.PdfPCell LineaFinal = new iTextSharp.text.pdf.PdfPCell();
                LineaFinal.Colspan = 6;
                LineaFinal.Padding = 3;
                LineaFinal.Border = 1;
                LineaFinal.BorderColorBottom = BaseColor.WHITE;
                LineaFinal.HorizontalAlignment = Element.ALIGN_CENTER;
                LineaFinal.VerticalAlignment = Element.ALIGN_TOP;
                tableUnidades.AddCell(LineaFinal);

                #endregion

                #region Exti
                document.Add(Header);
                document.Add(tableTituloUnidades);
                document.Add(tableUnidades);
                document.Add(espacio);
                document.Add(Body);

                /*PIE DE PAGINA*/
                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();
                return true;
                #endregion
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return false;
            }
            
        }

        private bool AddUnidadesEletroHome(DataRow dataLine, ref PdfPTable table, iTextSharp.text.Font fontTitleFactura, DataSet dataSet, ref decimal TotalUnd)
        {
            try
            {
                strError += "Add InvoiceLine";
                iTextSharp.text.pdf.PdfPCell celValL = new iTextSharp.text.pdf.PdfPCell(new Phrase((string)dataLine["InvoiceLine"], fontTitleFactura));
                celValL.Colspan = 1;
                celValL.Padding = 2;
                celValL.Border = 0;
                //celTextL.BorderWidthRight = 1;
                celValL.BorderWidthLeft = 1;
                celValL.HorizontalAlignment = Element.ALIGN_CENTER;
                celValL.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValL);
                strError += "InvoiceLine OK";

                strError += "Add PartNum";
                iTextSharp.text.pdf.PdfPCell celValCodigo = new iTextSharp.text.pdf.PdfPCell(new Phrase((string)dataLine["PartNum"], fontTitleFactura));
                celValCodigo.Colspan = 1;
                celValCodigo.Padding = 2;
                celValCodigo.Border = 0;
                celValCodigo.BorderWidthLeft = 1;
                //celDescripcion.BorderColorBottom = BaseColor.WHITE;
                celValCodigo.HorizontalAlignment = Element.ALIGN_LEFT;
                celValCodigo.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValCodigo);
                strError += "Add PartNum OK";

                strError += "Add SellingShipQty";
                iTextSharp.text.pdf.PdfPCell celValCantidad = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    decimal.Parse((string)dataLine["SellingShipQty"]).ToString("N"), fontTitleFactura));
                celValCantidad.Colspan = 1;
                celValCantidad.Padding = 2;
                celValCantidad.Border = 0;
                celValCantidad.BorderWidthLeft = 1;
                celValCantidad.HorizontalAlignment = Element.ALIGN_RIGHT;
                celValCantidad.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValCantidad);
                TotalUnd += Convert.ToDecimal(dataLine["SellingShipQty"]);
                strError += "Add SellingShipQty OK";

                strError += "Add SalesUM";
                iTextSharp.text.pdf.PdfPCell celValUnidad = new iTextSharp.text.pdf.PdfPCell(new Phrase((string)dataLine["SalesUM"], fontTitleFactura));
                celValUnidad.Colspan = 1;
                celValUnidad.Padding = 2;
                celValUnidad.Border = 0;
                celValUnidad.BorderWidthLeft = 1;
                celValUnidad.HorizontalAlignment = Element.ALIGN_LEFT;
                celValUnidad.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValUnidad);
                strError += "Add SalesUM OK";

                strError += "Add LineDesc";
                iTextSharp.text.pdf.PdfPCell celValDesc = new iTextSharp.text.pdf.PdfPCell(new Phrase((string)dataLine["LineDesc"], fontTitleFactura));
                celValDesc.Colspan = 1;
                celValDesc.Padding = 2;
                celValDesc.Border = 0;
                celValDesc.BorderWidthLeft = 1;
                celValDesc.HorizontalAlignment = Element.ALIGN_LEFT;
                celValDesc.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValDesc);
                strError += "Add LineDesc OK";

                strError += "Add DocUnitPrice";
                iTextSharp.text.pdf.PdfPCell celValValorUnitario = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    decimal.Parse((string)dataLine["DocUnitPrice"]).ToString("N2"), fontTitleFactura));
                celValValorUnitario.Colspan = 1;
                celValValorUnitario.Border = 0;
                celValValorUnitario.BorderWidthLeft = 1;
                celValValorUnitario.Padding = 2;
                celValValorUnitario.HorizontalAlignment = Element.ALIGN_RIGHT;
                celValValorUnitario.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValValorUnitario);
                strError += "Add DocUnitPrice OK";

                strError += "Add DspDocLessDiscount";
                //iTextSharp.text.pdf.PdfPCell celValDescuento = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                //    decimal.Parse((string)dataLine["DspDocLessDiscount"]).ToString("N2"), fontTitleFactura));
                iTextSharp.text.pdf.PdfPCell celValDescuento = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    decimal.Parse((string)dataLine["DiscountPercent"]).ToString("N2"), fontTitleFactura));
                celValDescuento.Colspan = 1;
                celValDescuento.Padding = 2;
                celValDescuento.Border = 0;
                celValDescuento.BorderWidthLeft = 1;
                celValDescuento.HorizontalAlignment = Element.ALIGN_RIGHT;
                celValDescuento.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValDescuento);
                strError += "Add DspDocLessDiscount OK";

                strError += "Add DspDocExtPrice";
                iTextSharp.text.pdf.PdfPCell celValValorTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    decimal.Parse((string)dataLine["DspDocExtPrice"]).ToString("N2"),
                    fontTitleFactura));
                celValValorTotal.Colspan = 1;
                celValValorTotal.Padding = 2;
                celValValorTotal.Border = 0;
                celValValorTotal.BorderWidthLeft = 1;
                celValValorTotal.HorizontalAlignment = Element.ALIGN_RIGHT;
                celValValorTotal.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValValorTotal);
                strError += "Add DspDocExtPrice OK";

                strError += "Add InvoiceNum";
                iTextSharp.text.pdf.PdfPCell celValIva = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    GetPercentIdImpDIAN((string)dataLine["InvoiceNum"], (string)dataLine["InvoiceLine"], dataSet.Tables["InvcTax"]),
                    fontTitleFactura));
                celValIva.Colspan = 1;
                celValIva.Padding = 2;
                celValIva.Border = 0;
                celValIva.BorderWidthLeft = 1;
                celValIva.BorderWidthRight = 1;
                celValIva.HorizontalAlignment = Element.ALIGN_RIGHT;
                celValIva.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValIva);
                strError += "Add InvoiceNum OK";
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return false;
            }
        }

        private bool AddUnidadesEletroHome(DataRow dataLine, ref PdfPTable table, iTextSharp.text.Font fontTitleFactura, DataSet dataSet)
        {
            try
            {
                //FACTURAS
                strError += "Add InvoiceLine";
                iTextSharp.text.pdf.PdfPCell celValL = new iTextSharp.text.pdf.PdfPCell(new Phrase((string)dataLine["InvoiceLine"], fontTitleFactura));
                celValL.Colspan = 1;
                celValL.Padding = 2;
                celValL.Border = 0;
                //celTextL.BorderWidthRight = 1;
                //celValL.BorderWidthLeft = 1;
                celValL.HorizontalAlignment = Element.ALIGN_LEFT;
                celValL.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValL);
                strError += "InvoiceLine OK";

                //REFERENCIA
                strError += "Add PartNum";
                iTextSharp.text.pdf.PdfPCell celValCodigo = new iTextSharp.text.pdf.PdfPCell(new Phrase((string)dataLine["PartNumPartDescription"], fontTitleFactura));
                celValCodigo.Colspan = 1;
                celValCodigo.Padding = 2;
                celValCodigo.Border = 0;
                //celValCodigo.BorderWidthLeft = 1;
                //celDescripcion.BorderColorBottom = BaseColor.WHITE;
                celValCodigo.HorizontalAlignment = Element.ALIGN_LEFT;
                celValCodigo.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValCodigo);
                strError += "Add PartNum OK";

                //CANTIDAD
                strError += "Add SellingShipQty";
                iTextSharp.text.pdf.PdfPCell celValDescripcion = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    decimal.Parse((string)dataLine["SellingShipQty"]).ToString("N0"), fontTitleFactura));
                celValDescripcion.Colspan = 1;
                celValDescripcion.Padding = 2;
                celValDescripcion.Border = 0;
                //celValCantidad.BorderWidthLeft = 1;
                celValDescripcion.HorizontalAlignment = Element.ALIGN_LEFT;
                celValDescripcion.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValDescripcion);
                strError += "Add SellingShipQty OK";

                //VALOR
                strError += "Add SalesUM";
                iTextSharp.text.pdf.PdfPCell celValUnidad = new iTextSharp.text.pdf.PdfPCell(new Phrase("$ " +
                    decimal.Parse((string)dataLine["DocUnitPrice"]).ToString("N2"), fontTitleFactura));
                celValUnidad.Colspan = 1;
                celValUnidad.Padding = 2;
                celValUnidad.Border = 0;
                //celValUnidad.BorderWidthLeft = 1;
                celValUnidad.HorizontalAlignment = Element.ALIGN_LEFT;
                celValUnidad.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValUnidad);
                strError += "Add SalesUM OK";

                //DESCRIPCION
                strError += "Add LineDesc";
                iTextSharp.text.pdf.PdfPCell celValCantidad = new iTextSharp.text.pdf.PdfPCell(new Phrase("$ " +
                    dataLine["LineDesc"], fontTitleFactura));
                celValCantidad.Colspan = 1;
                celValCantidad.Padding = 2;
                celValCantidad.Border = 0;
                //celValDesc.BorderWidthLeft = 1;
                celValCantidad.HorizontalAlignment = Element.ALIGN_LEFT;
                celValCantidad.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValCantidad);
                strError += "Add LineDesc OK";

                strError += "Add LineDesc";
                iTextSharp.text.pdf.PdfPCell celValCantidadv = new iTextSharp.text.pdf.PdfPCell(new Phrase(""+
                    dataLine["LineDesc"], fontTitleFactura));
                celValCantidadv.Colspan = 1;
                celValCantidadv.Padding = 2;
                celValCantidadv.Border = 0;
                //celValDesc.BorderWidthLeft = 1;
                celValCantidadv.HorizontalAlignment = Element.ALIGN_LEFT;
                celValCantidadv.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValCantidadv);
                strError += "Add LineDesc OK";
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return false;
            }
        }

        private bool AddUnidadesEletroHome(ref PdfPTable table)
        {
            try
            {
                iTextSharp.text.pdf.PdfPCell celValL = new iTextSharp.text.pdf.PdfPCell(new Phrase(" "));
                celValL.Colspan = 1;
                celValL.Padding = 3;
                celValL.Border = 0;
                //celTextL.BorderWidthRight = 1;
                celValL.BorderWidthLeft = 1;
                celValL.HorizontalAlignment = Element.ALIGN_CENTER;
                celValL.VerticalAlignment = Element.ALIGN_TOP;

                table.AddCell(celValL);

                iTextSharp.text.pdf.PdfPCell celValCodigo = new iTextSharp.text.pdf.PdfPCell(new Phrase(" "));
                celValCodigo.Colspan = 1;
                celValCodigo.Padding = 3;
                celValCodigo.Border = 0;
                celValCodigo.BorderWidthLeft = 1;
                //celDescripcion.BorderColorBottom = BaseColor.WHITE;
                celValCodigo.HorizontalAlignment = Element.ALIGN_CENTER;
                celValCodigo.VerticalAlignment = Element.ALIGN_TOP;

                table.AddCell(celValCodigo);

                iTextSharp.text.pdf.PdfPCell celValCantidad = new iTextSharp.text.pdf.PdfPCell(new Phrase(" "));
                celValCantidad.Colspan = 1;
                celValCantidad.Padding = 3;
                celValCantidad.Border = 0;
                celValCantidad.BorderWidthLeft = 1;
                celValCantidad.HorizontalAlignment = Element.ALIGN_CENTER;
                celValCantidad.VerticalAlignment = Element.ALIGN_TOP;

                table.AddCell(celValCantidad);

                iTextSharp.text.pdf.PdfPCell celValUnidad = new iTextSharp.text.pdf.PdfPCell(new Phrase(" "));
                celValUnidad.Colspan = 1;
                celValUnidad.Padding = 3;
                celValUnidad.Border = 0;
                celValUnidad.BorderWidthLeft = 1;
                celValUnidad.HorizontalAlignment = Element.ALIGN_CENTER;
                celValUnidad.VerticalAlignment = Element.ALIGN_TOP;

                table.AddCell(celValUnidad);

                iTextSharp.text.pdf.PdfPCell celValDesc = new iTextSharp.text.pdf.PdfPCell(new Phrase(" "));
                celValDesc.Colspan = 1;
                celValDesc.Padding = 3;
                celValDesc.Border = 0;
                celValDesc.BorderWidthLeft = 1;
                celValDesc.HorizontalAlignment = Element.ALIGN_CENTER;
                celValDesc.VerticalAlignment = Element.ALIGN_TOP;

                table.AddCell(celValDesc);

                iTextSharp.text.pdf.PdfPCell celValValorUnitario = new iTextSharp.text.pdf.PdfPCell(new Phrase(" "));
                celValValorUnitario.Colspan = 1;
                celValValorUnitario.Border = 0;
                celValValorUnitario.BorderWidthLeft = 1;
                celValValorUnitario.Padding = 3;
                celValValorUnitario.HorizontalAlignment = Element.ALIGN_CENTER;
                celValValorUnitario.VerticalAlignment = Element.ALIGN_TOP;

                table.AddCell(celValValorUnitario);

                iTextSharp.text.pdf.PdfPCell celValDescuento = new iTextSharp.text.pdf.PdfPCell(new Phrase(" "));
                celValDescuento.Colspan = 1;
                celValDescuento.Padding = 3;
                celValDescuento.Border = 0;
                celValDescuento.BorderWidthLeft = 1;
                celValDescuento.HorizontalAlignment = Element.ALIGN_CENTER;
                celValDescuento.VerticalAlignment = Element.ALIGN_TOP;

                table.AddCell(celValDescuento);

                iTextSharp.text.pdf.PdfPCell celValValorTotal = new iTextSharp.text.pdf.PdfPCell(new Phrase(" "));
                celValValorTotal.Colspan = 1;
                celValValorTotal.Padding = 3;
                celValValorTotal.Border = 0;
                celValValorTotal.BorderWidthLeft = 1;
                celValValorTotal.HorizontalAlignment = Element.ALIGN_CENTER;
                celValValorTotal.VerticalAlignment = Element.ALIGN_TOP;

                table.AddCell(celValValorTotal);

                iTextSharp.text.pdf.PdfPCell celValIva = new iTextSharp.text.pdf.PdfPCell(new Phrase(" "));
                celValIva.Colspan = 1;
                celValIva.Padding = 3;
                celValIva.Border = 0;
                celValIva.BorderWidthLeft = 1;
                celValIva.BorderWidthRight = 1;
                celValIva.HorizontalAlignment = Element.ALIGN_CENTER;
                celValIva.VerticalAlignment = Element.ALIGN_TOP;

                table.AddCell(celValIva);
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return false;
            }
        }

        public partial class HelpersEletroHome
        {
            public class FuentesEletroHome
            {
                public static iTextSharp.text.Font Plantilla_4_fontTitleAEI
                {
                    get
                    {
                        return FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                    }
                }
            }
        }

        public class EletroHomeConstructos
        {
            public class FacturaNacional
            {
                public static PdfPTable DatosCliente(DataSet ds, string CUFE, System.Drawing.Image image)
                {
                    Helpers.Plantilla_4.DatosClienteDatos datos = new Helpers.Plantilla_4.DatosClienteDatos(ds);

                    PdfPTable tableFacturar = new PdfPTable(3);
                    //Dimenciones.
                    float[] DimencionFacturar = new float[3];
                    DimencionFacturar[0] = 1.0F;//
                    DimencionFacturar[1] = 1.0F;//
                    DimencionFacturar[2] = 1.0F;//

                    tableFacturar.WidthPercentage = 100;
                    tableFacturar.SetWidths(DimencionFacturar);
                    //----------------------------------------------------------------------------------------------
                    PdfPCell celNofactura = new PdfPCell(new Phrase(
                        $""));
                    celNofactura.HorizontalAlignment = Element.ALIGN_RIGHT;
                    celNofactura.Colspan = 3;
                    celNofactura.Padding = 3;
                    celNofactura.BorderWidthLeft = 0;
                    celNofactura.BorderWidthRight = 0;
                    tableFacturar.AddCell(celNofactura);
                    //----------------------------------------------------------------------------------------------
                    PdfPTable tableDatosCliente = new PdfPTable(1);
                    float[] DimencionFacturarA = new float[2];
                    DimencionFacturarA[0] = 1.0F;//
                    DimencionFacturarA[1] = 9.0F;//

                    tableDatosCliente.WidthPercentage = 100;
                    //-------------------------- Señores -------------------------------------------------------------------
                    PdfPCell celNombreCliente = new PdfPCell();
                    //celNombreCliente.AddElement(phrSeñores);
                    celNombreCliente.Border = 0;
                    //celNombreCliente.Padding = 0;
                    Helpers.Plantilla_4.DatosClienteTitulos(ref celNombreCliente, "Señores: ", ds.Tables["Customer"].Rows[0]["Name"].ToString());
                    tableDatosCliente.AddCell(celNombreCliente);


                    //--------------------------- NIT -------------------------------------------------------------------------

                    Chunk TituloNIT = new Chunk("NIT: ", Helpers.Fuentes.Plantilla_4_fontTitle2);
                    Chunk ValorNIT = new Chunk((string)ds.Tables["Customer"].Rows[0]["ResaleID"] + "\r\n", Helpers.Fuentes.Plantilla_4_fontTitleFactura);
                    Phrase phrNIT = new Phrase();
                    phrNIT.Add(TituloNIT);
                    phrNIT.Add(ValorNIT);
                    PdfPCell celNit = new PdfPCell();
                    celNit.AddElement(phrNIT);
                    celNit.Border = 0;
                    tableDatosCliente.AddCell(celNit);
                    //------------------- Teléfono y Fax --------------------------------------------------------------------------------------------------
                    PdfPTable tableTel = new PdfPTable(2);
                    tableTel.WidthPercentage = 100;
                    tableTel.SetWidths(new float[] { 1f, 1f });
                    tableTel.PaddingTop = 0;

                    Chunk TituloTelefono = new Chunk("Teléfono: ", Helpers.Fuentes.Plantilla_4_fontTitle2);
                    Chunk ValorTelefono = new Chunk((string)ds.Tables["Customer"].Rows[0]["PhoneNum"], Helpers.Fuentes.Plantilla_4_fontTitleFactura);
                    Phrase phrTelefono = new Phrase();
                    phrTelefono.Add(TituloTelefono);
                    phrTelefono.Add(ValorTelefono);
                    PdfPCell celValTelefonoCliente = new PdfPCell();
                    celValTelefonoCliente.AddElement(phrTelefono);
                    celValTelefonoCliente.Colspan = 1;
                    //celValTelefonoCliente.Padding = 0;
                    celValTelefonoCliente.Border = 0;
                    celValTelefonoCliente.HorizontalAlignment = Element.ALIGN_LEFT;
                    celValTelefonoCliente.VerticalAlignment = Element.ALIGN_TOP;
                    tableTel.AddCell(celValTelefonoCliente);

                    Chunk TituloFax = new Chunk("Fax: ", Helpers.Fuentes.Plantilla_4_fontTitle2);
                    Chunk ValorFax = new Chunk(ds.Tables["Customer"].Rows[0]["FaxNum"].ToString(), Helpers.Fuentes.Plantilla_4_fontTitleFactura);
                    Phrase phrFax = new Phrase();
                    phrFax.Add(TituloFax);
                    phrFax.Add(ValorFax);
                    PdfPCell celValFaxCliente = new PdfPCell();
                    celValFaxCliente.AddElement(phrFax);
                    celValFaxCliente.Colspan = 1;
                    celValFaxCliente.Padding = 0;
                    celValFaxCliente.Border = 0;
                    celValFaxCliente.HorizontalAlignment = Element.ALIGN_LEFT;
                    celValFaxCliente.VerticalAlignment = Element.ALIGN_TOP;
                    tableTel.AddCell(celValFaxCliente);

                    PdfPCell celTel = new PdfPCell(tableTel);
                    celTel.Border = 0;
                    tableDatosCliente.AddCell(celTel);
                    //--------- Evaluando forma de pago -------
                    if (datos.CelFormaPago != null)
                    {
                        tableDatosCliente.AddCell(datos.CelFormaPago);
                    }
                    //---------------- Domicilio principal -----------------------------------------------------------------------------------
                    Chunk TituloDomicilioPPal = new Chunk("Domicilio Ppal: ", Helpers.Fuentes.Plantilla_4_fontTitle2);
                    Chunk ValorDomicilioPPal = new Chunk(ds.Tables["Customer"].Rows[0]["Address1"].ToString(), Helpers.Fuentes.Plantilla_4_fontTitleFactura);
                    Phrase phrDomicilioPPal = new Phrase();
                    phrDomicilioPPal.Add(TituloDomicilioPPal);
                    phrDomicilioPPal.Add(ValorDomicilioPPal);

                    PdfPCell celDomicilioPpal = new PdfPCell(); //new PdfPCell(tableDomicilioPpal);
                    celDomicilioPpal.AddElement(phrDomicilioPPal);
                    celDomicilioPpal.Border = 0;
                    tableDatosCliente.AddCell(celDomicilioPpal);
                    //--------------------  ------------------------------------------------------------------------------------
                    PdfPCell celDomicilioEmbarque = new PdfPCell(); //new PdfPCell(tableDomicilioEmbarque);
                    Helpers.Plantilla_4.DatosClienteTitulos(ref celDomicilioEmbarque, "Domicilio : ", ds.Tables["Customer"].Rows[0]["ShortChar01"].ToString());
                    celDomicilioEmbarque.Border = 0;
                    tableDatosCliente.AddCell(celDomicilioEmbarque);
                    //------------------- Codigo Tienda ------------------------------------
                    string CodigoTienda = string.Empty;
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "CodigoTienda", 0))
                        CodigoTienda = ds.Tables["InvcHead"].Rows[0]["CodigoTienda"].ToString();
                    PdfPCell celCodigoTienda = new PdfPCell();
                    //Helpers.Plantilla_4.DatosClienteTitulos(ref celCodigoTienda, "Código Tienda: ", CodigoTienda);
                    celCodigoTienda.Border = 0;
                    tableDatosCliente.AddCell(celCodigoTienda);
                    //--------------------------------------------------------------------------------------------------------------
                    iTextSharp.text.pdf.PdfPCell celdatosCliente = new iTextSharp.text.pdf.PdfPCell(tableDatosCliente);
                    celdatosCliente.Colspan = 1;
                    tableFacturar.AddCell(celdatosCliente);
                    //--------------------------------------------------------------------------------------------------------------
                    iTextSharp.text.Font fontTitleFecha = FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL);
                    iTextSharp.text.Font fontDMA = FontFactory.GetFont(FontFactory.HELVETICA, 6, BaseColor.GRAY);
                    iTextSharp.text.Font fontDetalleFecha = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 8, iTextSharp.text.Font.NORMAL);
                    //--------------------------------------------------------------------------------------------------------------
                    PdfPTable tableToCel2 = new PdfPTable(2);
                    float[] DimencionDespacharA = new float[2];
                    DimencionDespacharA[0] = 1.0F;//
                    DimencionDespacharA[1] = 1.0F;//

                    tableToCel2.WidthPercentage = 100;
                    tableToCel2.SetWidths(DimencionDespacharA);

                    PdfPTable tableFechaEmision = new PdfPTable(1);

                    iTextSharp.text.pdf.PdfPCell celTextFechaEmision = new iTextSharp.text.pdf.PdfPCell(new Phrase("Fecha Emisión", fontTitleFecha));
                    celTextFechaEmision.Colspan = 1;
                    //celTextFechaEmision.Padding = 3;
                    celTextFechaEmision.Border = 0;
                    celTextFechaEmision.HorizontalAlignment = Element.ALIGN_CENTER;
                    celTextFechaEmision.VerticalAlignment = Element.ALIGN_TOP;
                    tableFechaEmision.AddCell(celTextFechaEmision);

                    iTextSharp.text.pdf.PdfPCell celDMA = new iTextSharp.text.pdf.PdfPCell(new Phrase("DD/MM/AAAA", fontDMA));
                    celDMA.Colspan = 1;
                    //celDMA.Padding = 3;
                    celDMA.Border = 0;
                    celDMA.HorizontalAlignment = Element.ALIGN_CENTER;
                    celDMA.VerticalAlignment = Element.ALIGN_TOP;
                    tableFechaEmision.AddCell(celDMA);

                    //------ Dato fecha y hora ------
                    //iTextSharp.text.pdf.PdfPCell celFechaEmisionVal = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    //DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).ToString("dd/MM/yyyy"), fontDetalleFecha));
                    iTextSharp.text.pdf.PdfPCell celFechaEmisionVal = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                        datos.FechayHora, fontDetalleFecha));
                    celFechaEmisionVal.Colspan = 1;
                    //celFechaEmisionVal.Padding = 3;
                    celFechaEmisionVal.Border = 0;
                    celFechaEmisionVal.HorizontalAlignment = Element.ALIGN_CENTER;
                    celFechaEmisionVal.VerticalAlignment = Element.ALIGN_TOP;
                    tableFechaEmision.AddCell(celFechaEmisionVal);
                    PdfPCell celFechaEmision = new PdfPCell(tableFechaEmision);
                    tableToCel2.AddCell(celFechaEmision);

                    //---------------------------------------------------------------------------------------------------------------
                    PdfPTable tableFechaDetalleVencimiento = new PdfPTable(1);

                    iTextSharp.text.pdf.PdfPCell celTextDetalleFechaVencimiento = new iTextSharp.text.pdf.PdfPCell(new Phrase("Fecha Vencimiento", fontTitleFecha));
                    celTextDetalleFechaVencimiento.Colspan = 1;
                    //celTextDetalleFechaVencimiento.Padding = 3;
                    celTextDetalleFechaVencimiento.Border = 0;
                    celTextDetalleFechaVencimiento.HorizontalAlignment = Element.ALIGN_CENTER;
                    celTextDetalleFechaVencimiento.VerticalAlignment = Element.ALIGN_TOP;
                    tableFechaDetalleVencimiento.AddCell(celTextDetalleFechaVencimiento);

                    tableFechaDetalleVencimiento.AddCell(celDMA);

                    iTextSharp.text.pdf.PdfPCell celFechaVencimientoVal = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                        DateTime.Parse((string)ds.Tables["InvcHead"].Rows[0]["DueDate"]).ToString("dd/MM/yyyy"), fontDetalleFecha));
                    celFechaVencimientoVal.Colspan = 1;
                    //celFechaVencimientoVal.Padding = 3;
                    celFechaVencimientoVal.Border = 0;
                    celFechaVencimientoVal.HorizontalAlignment = Element.ALIGN_CENTER;
                    celFechaVencimientoVal.VerticalAlignment = Element.ALIGN_TOP;
                    tableFechaDetalleVencimiento.AddCell(celFechaVencimientoVal);

                    PdfPCell celFechaDetalleVencimiento = new PdfPCell(tableFechaDetalleVencimiento);
                    tableToCel2.AddCell(celFechaDetalleVencimiento);
                    //-------------------------------------------------------------------------------------------------------------

                    iTextSharp.text.pdf.PdfPCell celNoPedido = new PdfPCell();
                    Helpers.Plantilla_4.DatosClienteTitulos(ref celNoPedido, "Pedido No: ", ds.Tables["InvcHead"].Rows[0]["OrderNum"].ToString());
                    tableToCel2.AddCell(celNoPedido);
                    //-------------------------------------------------------------------------------------
                    iTextSharp.text.pdf.PdfPCell celOrderCompra = new iTextSharp.text.pdf.PdfPCell();
                    Helpers.Plantilla_4.DatosClienteTitulos(ref celOrderCompra, "pedido: ", (string)ds.Tables["InvcHead"].Rows[0]["PONum"]);
                    tableToCel2.AddCell(celOrderCompra);
                    //-------------------------------------------------------------------------------------

                    PdfPCell celVendededor = new PdfPCell();//new PdfPCell(tableVendedor);
                    Helpers.Plantilla_4.DatosClienteTitulos(ref celVendededor, "Vendedor: ", ds.Tables["InvcHead"].Rows[0]["SalesRepName1"].ToString());
                    celVendededor.Colspan = 1;
                    tableToCel2.AddCell(celVendededor);
                    //-------------------------------------------------------------------------------------

                    PdfPCell celCodigoVendededor = new PdfPCell();//new PdfPCell(tableCodigoVendedor);
                    Helpers.Plantilla_4.DatosClienteTitulos(ref celCodigoVendededor, "Código Vendedor: ", ds.Tables["InvcHead"].Rows[0]["SalesRepCode1"].ToString());
                    celCodigoVendededor.Colspan = 1;
                    tableToCel2.AddCell(celCodigoVendededor);

                    PdfPCell celncuotas = new PdfPCell();//new PdfPCell(celncuotas);
                    Helpers.Plantilla_4.DatosClienteTitulos(ref celncuotas, "Numero de Cuotas: ", ds.Tables["InvcHead"].Rows[0]["Number02"].ToString());
                    celncuotas.Colspan = 1;
                    tableToCel2.AddCell(celncuotas);

                    PdfPCell celvcuotas = new PdfPCell();//new PdfPCell(celvcuotas);
                    Helpers.Plantilla_4.DatosClienteTitulos(ref celvcuotas, "Valor de Cuota: ", ds.Tables["InvcHead"].Rows[0]["Number03"].ToString());
                    celvcuotas.Colspan = 1;
                    tableToCel2.AddCell(celvcuotas);


                    PdfPCell celfpago = new PdfPCell();//new PdfPCell(celvcuotas);
                    Helpers.Plantilla_4.DatosClienteTitulos(ref celfpago, "Forma de Pago: ", ds.Tables["InvcHead"].Rows[0]["ShortChar01"].ToString());
                    celfpago.Colspan = 2;
                    tableToCel2.AddCell(celfpago);


                    //-------------- Nro Aviso -------------------------
                    PdfPCell celnroAviso = new PdfPCell();
                    string NroAviso = string.Empty;
                    if (Helpers.Compartido.VerificarExistenciaColumnas(ds, "InvcHead", "NroAviso", 0))
                        NroAviso = ds.Tables["InvcHead"].Rows[0]["NroAviso"].ToString();
                    //Helpers.Plantilla_4.DatosClienteTitulos(ref celnroAviso, "Nro Aviso: ", NroAviso);
                    celnroAviso.Colspan = 2;
                    tableToCel2.AddCell(celnroAviso);
                    //-------------------------------------------------------------------------------------
                    iTextSharp.text.pdf.PdfPCell cel2 = new iTextSharp.text.pdf.PdfPCell(tableToCel2);
                    cel2.Colspan = 1;
                    tableFacturar.AddCell(cel2);


                    /*CELL QR*/
                    Paragraph prgCufe = new Paragraph("CUFE: " + CUFE, Helpers.Fuentes.Plantilla_4_fontTitle2);
                    prgCufe.Alignment = Element.ALIGN_LEFT;

                    iTextSharp.text.Image QRPdf = iTextSharp.text.Image.GetInstance(image, BaseColor.WHITE);
                    QRPdf.ScaleAbsolute(90f, 90f);
                    QRPdf.Alignment = Element.ALIGN_CENTER;
                    //iTextSharp.text.pdf.PdfPCell celImgQR = new iTextSharp.text.pdf.PdfPCell(QRPdf);
                    iTextSharp.text.pdf.PdfPCell celImgQR = new iTextSharp.text.pdf.PdfPCell();
                    celImgQR.AddElement(QRPdf);
                    celImgQR.AddElement(prgCufe);
                    celImgQR.Colspan = 2;
                    celImgQR.Padding = 3;
                    celImgQR.Border = 0;
                    celImgQR.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                    celImgQR.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                    tableFacturar.AddCell(celImgQR);
                    //----- Retornando tabla
                    return tableFacturar;
                }
            }
        }

        #endregion

    }
}
