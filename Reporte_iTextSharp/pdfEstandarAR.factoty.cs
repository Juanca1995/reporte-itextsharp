﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
//Referenciar y usar.
using System.Data;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Web;
using System.Windows.Forms;

namespace RulesServicesDIAN2.Adquiriente
{
    public partial class pdfEstandarAR
    {
        private bool AddUnidadesFactory(DataRow dataLine, ref PdfPTable table, iTextSharp.text.Font fontTitleFactura, DataSet dataSet)
        {
            try
            {
                //codigo
                strError += "Add InvoiceLine";
                iTextSharp.text.pdf.PdfPCell celValL = new iTextSharp.text.pdf.PdfPCell(new Phrase((string)dataLine["PartNum"], fontTitleFactura));
                celValL.Colspan = 1;
                celValL.Padding = 2;
                celValL.Border = PdfPCell.RIGHT_BORDER;
                //celTextL.BorderWidthRight = 1;
                //celValL.BorderWidthLeft = 1;
                celValL.HorizontalAlignment = Element.ALIGN_CENTER;
                celValL.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValL);
                strError += "InvoiceLine OK";

                //descriccion
                strError += "Add PartNum";
                iTextSharp.text.pdf.PdfPCell celValCodigo = new iTextSharp.text.pdf.PdfPCell(new Phrase((string)dataLine["LineDesc"], fontTitleFactura));
                celValCodigo.Colspan = 1;
                celValCodigo.Padding = 2;
                celValCodigo.Border = PdfPCell.RIGHT_BORDER;
                //celValCodigo.BorderWidthLeft = 1;
                //celDescripcion.BorderColorBottom = BaseColor.WHITE;
                celValCodigo.HorizontalAlignment = Element.ALIGN_CENTER;
                celValCodigo.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValCodigo);
                strError += "Add PartNum OK";

                //cantidad
                strError += "Add SellingShipQty";
                iTextSharp.text.pdf.PdfPCell celValDescripcion = new iTextSharp.text.pdf.PdfPCell(new Phrase(
                    decimal.Parse((string)dataLine["SellingShipQty"]).ToString("N0"), fontTitleFactura));
                celValDescripcion.Colspan = 1;
                celValDescripcion.Padding = 2;
                celValDescripcion.Border = PdfPCell.RIGHT_BORDER;
                //celValCantidad.BorderWidthLeft = 1;
                celValDescripcion.HorizontalAlignment = Element.ALIGN_CENTER;
                celValDescripcion.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValDescripcion);
                strError += "Add SellingShipQty OK";

                //descuento
                strError += "Add SalesUM";
                iTextSharp.text.pdf.PdfPCell celValUnidad = new iTextSharp.text.pdf.PdfPCell(new Phrase(" % " +
                    decimal.Parse((string)dataLine["DiscountPercent"]).ToString("N0"), fontTitleFactura));
                celValUnidad.Colspan = 1;
                celValUnidad.Padding = 2;
                celValUnidad.Border = PdfPCell.RIGHT_BORDER;
                //celValUnidad.BorderWidthLeft = 1;
                celValUnidad.HorizontalAlignment = Element.ALIGN_CENTER;
                celValUnidad.VerticalAlignment = Element.ALIGN_TOP; 
                table.AddCell(celValUnidad);
                strError += "Add SalesUM OK";

                //valor unitario
                strError += "Add LineDesc";
                iTextSharp.text.pdf.PdfPCell celValCantidad = new iTextSharp.text.pdf.PdfPCell(new Phrase("$ " +
                    decimal.Parse((string)dataLine["UnitPrice"]).ToString("N0"), fontTitleFactura));
                celValCantidad.Colspan = 1;
                celValCantidad.Padding = 2;
                celValCantidad.Border = PdfPCell.RIGHT_BORDER;
                //celValDesc.BorderWidthLeft = 1;
                celValCantidad.HorizontalAlignment = Element.ALIGN_CENTER;
                celValCantidad.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValCantidad);
                strError += "Add LineDesc OK";

                //subtotaal
                strError += "Add LineDesc";
                iTextSharp.text.pdf.PdfPCell celValCantidad1 = new iTextSharp.text.pdf.PdfPCell(new Phrase("$ " +
                    decimal.Parse((string)dataLine["DspDocExtPrice"]).ToString("N0"), fontTitleFactura));
                celValCantidad1.Colspan = 1;
                celValCantidad1.Padding = 2;
                celValCantidad1.Border = 0;
                //celValDesc.BorderWidthLeft = 1;
                celValCantidad1.HorizontalAlignment = Element.ALIGN_CENTER;
                celValCantidad1.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValCantidad1);
                strError += "Add LineDesc OK";

                return true;
            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.Message;
                return false;
            }
        }

        private bool AddUnidadesNOTAFactory(DataRow dataLine, ref PdfPTable table, iTextSharp.text.Font fontTitleFactura, DataSet dataSet )
        {
            try
            {
                //codigo
                strError += "Add InvoiceLine";
                iTextSharp.text.pdf.PdfPCell celValL = new iTextSharp.text.pdf.PdfPCell(new Phrase((string)dataLine["PartNum"], fontTitleFactura));
                celValL.Colspan = 1;
                celValL.Padding = 2;
                celValL.Border = 0;
                //celTextL.BorderWidthRight = 1;
                //celValL.BorderWidthLeft = 1;
                celValL.HorizontalAlignment = Element.ALIGN_CENTER;
                celValL.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValL);
                strError += "InvoiceLine OK";

                //descripcion 
                strError += "Add PartNum";
                iTextSharp.text.pdf.PdfPCell celValCodigo = new iTextSharp.text.pdf.PdfPCell(new Phrase(""+(string)dataLine["LineDesc"], fontTitleFactura));
                celValCodigo.Colspan = 1;
                celValCodigo.Padding = 2;
                celValCodigo.Border = 0;
                //celValCodigo.BorderWidthLeft = 1;
                //celDescripcion.BorderColorBottom = BaseColor.WHITE;
                celValCodigo.HorizontalAlignment = Element.ALIGN_CENTER;
                celValCodigo.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValCodigo);
                strError += "Add PartNum OK";

                //CANTIDAD
                strError += "Add SellingShipQty";
                iTextSharp.text.pdf.PdfPCell celValDescripcion = new iTextSharp.text.pdf.PdfPCell(new Phrase(" "+
                    decimal.Parse((string)dataLine["SellingShipQty"]).ToString("N0"), fontTitleFactura));
                celValDescripcion.Colspan = 1;
                celValDescripcion.Padding = 2;
                celValDescripcion.Border = 0;
                //celValCantidad.BorderWidthLeft = 1;
                celValDescripcion.HorizontalAlignment = Element.ALIGN_RIGHT;
                celValDescripcion.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValDescripcion);
                strError += "Add SellingShipQty OK";

                //UNIDAD
                strError += "Add SalesUM";
                iTextSharp.text.pdf.PdfPCell celValUnidad = new iTextSharp.text.pdf.PdfPCell(new Phrase("  " +
                    decimal.Parse((string)dataLine["SalesUM"]).ToString("N0"), fontTitleFactura));
                celValUnidad.Colspan = 1;
                celValUnidad.Padding = 2;
                celValUnidad.Border = 0;
                //celValUnidad.BorderWidthLeft = 1;
                celValUnidad.HorizontalAlignment = Element.ALIGN_CENTER;
                celValUnidad.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValUnidad);
                strError += "Add SalesUM OK";

                //DCTO
                strError += "Add LineDesc";
                iTextSharp.text.pdf.PdfPCell celValCantidad = new iTextSharp.text.pdf.PdfPCell(new Phrase("% " +
                    decimal.Parse((string)dataLine["Number03"]).ToString("N0"), fontTitleFactura));
                celValCantidad.Colspan = 1;
                celValCantidad.Padding = 2;
                celValCantidad.Border = 0;
                //celValDesc.BorderWidthLeft = 1;
                celValCantidad.HorizontalAlignment = Element.ALIGN_CENTER;
                celValCantidad.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValCantidad);
                strError += "Add LineDesc OK";

                //IVA
                strError += "Add LineDesc";
                iTextSharp.text.pdf.PdfPCell celValNETO = new iTextSharp.text.pdf.PdfPCell(new Phrase("% " +
                    decimal.Parse((string)dataLine["Discount"]).ToString("N0"), fontTitleFactura));
                celValNETO.Colspan = 1;
                celValNETO.Padding = 2;
                celValNETO.Border = 0;
                //celValDesc.BorderWidthLeft = 1;
                celValNETO.HorizontalAlignment = Element.ALIGN_CENTER;
                celValNETO.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValNETO);
                strError += "Add LineDesc OK";

                //PRECIO
                strError += "Add LineDesc";
                iTextSharp.text.pdf.PdfPCell celValCONCEPTO = new iTextSharp.text.pdf.PdfPCell(new Phrase("$ "+(string)dataLine["UnitPrice"], fontTitleFactura));
                celValCONCEPTO.Colspan = 1;
                celValCONCEPTO.Padding = 2;
                celValCONCEPTO.Border = 0;
                //celValDesc.BorderWidthLeft = 1;
                celValCONCEPTO.HorizontalAlignment = Element.ALIGN_CENTER;
                celValCONCEPTO.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celValCONCEPTO);
                strError += "Add LineDesc OK";

                //total 
                strError += "Add LineDesc";
                iTextSharp.text.pdf.PdfPCell celtotal= new iTextSharp.text.pdf.PdfPCell(new Phrase("$ "+(string)dataLine["DspDocExtPrice"], fontTitleFactura));
                celtotal.Colspan = 1;
                celtotal.Padding = 2;
                celtotal.Border = 0;
                //celValDesc.BorderWidthLeft = 1;
                celtotal.HorizontalAlignment = Element.ALIGN_CENTER;
                celtotal.VerticalAlignment = Element.ALIGN_TOP;
                table.AddCell(celtotal);
                strError += "Add LineDesc OK";
                return true;
            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.Message;
                return false;
            }
        }

        private bool AddUnidadesFactory(ref PdfPTable table)
        {
            try
            {
                iTextSharp.text.pdf.PdfPCell celValL = new iTextSharp.text.pdf.PdfPCell(new Phrase(" "));
                celValL.Colspan = 1;
                celValL.Padding = 3;
                celValL.Border = 0;
                //celTextL.BorderWidthRight = 1;
                celValL.BorderWidthLeft = 1;
                celValL.HorizontalAlignment = Element.ALIGN_CENTER;
                celValL.VerticalAlignment = Element.ALIGN_TOP;

                table.AddCell(celValL);

                iTextSharp.text.pdf.PdfPCell celValCodigo = new iTextSharp.text.pdf.PdfPCell(new Phrase(" "));
                celValCodigo.Colspan = 1;
                celValCodigo.Padding = 3;
                celValCodigo.Border = 0;
                celValCodigo.BorderWidthLeft = 1;
                //celDescripcion.BorderColorBottom = BaseColor.WHITE;
                celValCodigo.HorizontalAlignment = Element.ALIGN_CENTER;
                celValCodigo.VerticalAlignment = Element.ALIGN_TOP;

                table.AddCell(celValCodigo);

                iTextSharp.text.pdf.PdfPCell celValCantidad = new iTextSharp.text.pdf.PdfPCell(new Phrase(" "));
                celValCantidad.Colspan = 1;
                celValCantidad.Padding = 3;
                celValCantidad.Border = 0;
                celValCantidad.BorderWidthLeft = 1;
                celValCantidad.HorizontalAlignment = Element.ALIGN_CENTER;
                celValCantidad.VerticalAlignment = Element.ALIGN_TOP;

                table.AddCell(celValCantidad);

                iTextSharp.text.pdf.PdfPCell celValUnidad = new iTextSharp.text.pdf.PdfPCell(new Phrase(" "));
                celValUnidad.Colspan = 1;
                celValUnidad.Padding = 3;
                celValUnidad.Border = 0;
                celValUnidad.BorderWidthLeft = 1;
                celValUnidad.HorizontalAlignment = Element.ALIGN_CENTER;
                celValUnidad.VerticalAlignment = Element.ALIGN_TOP;

                table.AddCell(celValUnidad);

                iTextSharp.text.pdf.PdfPCell celValDesc = new iTextSharp.text.pdf.PdfPCell(new Phrase(" "));
                celValDesc.Colspan = 1;
                celValDesc.Padding = 3;
                celValDesc.Border = 0;
                celValDesc.BorderWidthLeft = 1;
                celValDesc.HorizontalAlignment = Element.ALIGN_CENTER;
                celValDesc.VerticalAlignment = Element.ALIGN_TOP;

                table.AddCell(celValDesc);


                return true;
            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.Message;
                return false;
            }
        }

        public bool FacturadeVentaFactory(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {          
            NomArchivo = string.Empty;
            string Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            string RutaImg = $@"{AppDomain.CurrentDomain.BaseDirectory}\Logo_Factory_Cauchos.png";
            string RutaMarcaAgua = $@"{AppDomain.CurrentDomain.BaseDirectory}\marca_agua.png";
            NomArchivo = NombreInvoice + ".pdf";
            try
            {
                #region Head
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.
                Document document = new Document(iTextSharp.text.PageSize.LETTER);
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;

                // step 3: we open the document     
                document.Open();

                PdfDiv divEspacio = new PdfDiv();
                divEspacio.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio.Height = 10;
                divEspacio.Width = 130;

                PdfDiv divEspacio2 = new PdfDiv();
                divEspacio2.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio2.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio2.Height = 2;
                divEspacio2.Width = 130;

                //FUENTES----------------------------------------------------------------------------------------------------------
                iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle3 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 9, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle2 = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustom = FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitleFactura = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                //FUENTES---------------------------------------------------------------------------------------------------------

                RoundRectangle CelEventBorderRound = new RoundRectangle();


                System.Drawing.Image logo = null;
                var requestLogo = WebRequest.Create(RutaImg);

                using (var responseLogo = requestLogo.GetResponse())
                using (var streamLogo = responseLogo.GetResponseStream())
                {
                    logo = Bitmap.FromStream(streamLogo);
                }

                System.Drawing.Image logo2 = null;
                var requestLogo2 = WebRequest.Create(RutaImg);

                using (var responseLogo2 = requestLogo2.GetResponse())
                using (var streamLogo2 = responseLogo2.GetResponseStream())
                {
                    logo2 = Bitmap.FromStream(streamLogo2);
                }

                //iTextSharp.text.Image ImgLogo2 = iTextSharp.text.Image.GetInstance(logo2, BaseColor.WHITE);
                //ImgLogo2.ScaleAbsolute(130, 130);

                #endregion

                PdfPTable todo = new PdfPTable(1);
                todo.WidthPercentage = 100;
                PdfPCell cell_todo = new PdfPCell() { Border = 0, CellEvent = CelEventBorderRound, };

                #region ENCABEZADO

                PdfPTable Header = new PdfPTable(new float[] { 0.3f, 0.3f, 0.2f, 0.20f });
                Header.WidthPercentage = 100;

                //logo
                PdfPCell cell_logo = new PdfPCell() { Border = 0, };
                iTextSharp.text.Image ImgLogo2 = iTextSharp.text.Image.GetInstance(logo2, BaseColor.WHITE);
                ImgLogo2.ScaleAbsolute(150, 150);
                cell_logo.AddElement(ImgLogo2);
                Header.AddCell(cell_logo);

                //informacion de la empresa
                PdfPCell cell_info = new PdfPCell() { Border = 0, };
                Paragraph prgTitle1 = new Paragraph(string.Format("FACTORY PARTS  Y ACCESORIES S.A.A"), fontTitle) { Alignment = Element.ALIGN_CENTER, };
                Paragraph prgTitle2 = new Paragraph(string.Format("NIT. 900420346-1"), fontTitle) { Alignment = Element.ALIGN_CENTER, };
                Paragraph prgTitle3 = new Paragraph(string.Format("Calle 76No. 50B-39"), fontTitle) { Alignment = Element.ALIGN_CENTER, };
                Paragraph prgTitle4 = new Paragraph(string.Format("TELS.263 8253-581 1555"), fontTitle) { Alignment = Element.ALIGN_CENTER, };
                Paragraph prgTitle5 = new Paragraph(string.Format("e-MAIL. FACTORYPARTS2011@GMAIL.COM"), fontTitle) { Alignment = Element.ALIGN_CENTER, };
                cell_info.AddElement(prgTitle1);
                cell_info.AddElement(prgTitle2);
                cell_info.AddElement(prgTitle3);
                cell_info.AddElement(prgTitle4);
                cell_info.AddElement(prgTitle5);
                Header.AddCell(cell_info);

                //Qr
                PdfPCell cell_Qr = new PdfPCell() { Border = 0, };
                iTextSharp.text.Image ImgQR = iTextSharp.text.Image.GetInstance(QRInvoice, BaseColor.WHITE);
                ImgQR.ScaleAbsolute(60, 60);
                cell_Qr.AddElement(ImgQR);
                Header.AddCell(cell_Qr);

                //tipo de de documento
                PdfPCell cell_factura = new PdfPCell() { Border = 0, };
                PdfPTable tableFactura = new PdfPTable(3);

                //Dimenciones.
                float[] DimencionFactura = new float[3];
                DimencionFactura[0] = 1.2F;//
                DimencionFactura[1] = 4.0F;//
                DimencionFactura[2] = 1.0F;//

                tableFactura.WidthPercentage = 100;
                tableFactura.SetWidths(DimencionFactura);

                iTextSharp.text.pdf.PdfPCell celTittle = new iTextSharp.text.pdf.PdfPCell(new Phrase("FACTURA DE VENTAS\n\n", fontTitleFactura)) { Border = 0, };
                celTittle.Colspan = 3;
                celTittle.Padding = 3;
                celTittle.HorizontalAlignment = Element.ALIGN_CENTER;
                celTittle.VerticalAlignment = Element.ALIGN_TOP;
                celTittle.Border = 0;
                celTittle.BorderWidthTop = 1;
                celTittle.BorderWidthLeft = 1;
                celTittle.BorderWidthRight = 1;
                //celTittle.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableFactura.AddCell(celTittle);

                iTextSharp.text.pdf.PdfPCell celNo = new iTextSharp.text.pdf.PdfPCell(new Phrase("No. ", fontTitleFactura)) { Border = 0, };
                celNo.Colspan = 1;
                celNo.Padding = 5;
                celNo.HorizontalAlignment = Element.ALIGN_CENTER;
                celNo.VerticalAlignment = Element.ALIGN_TOP;
                celNo.Border = 0;
                celNo.BorderWidthLeft = 1;
                //celNo.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableFactura.AddCell(celNo);

                string NumLegalFactura = string.Empty;
                if (InvoiceType == "InvoiceType")
                    NumLegalFactura = DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceRef"].ToString();
                else if (InvoiceType == "CreditNoteType")
                    NumLegalFactura = DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"].ToString();
                else
                    NumLegalFactura = DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"].ToString();

                iTextSharp.text.pdf.PdfPCell celNoFactura = new iTextSharp.text.pdf.PdfPCell(new Phrase(NumLegalFactura, fontTitleFactura));
                celNoFactura.Colspan = 1;
                celNoFactura.Padding = 5;
                celNoFactura.HorizontalAlignment = Element.ALIGN_LEFT;
                celNoFactura.VerticalAlignment = Element.ALIGN_TOP;
                //celNoFactura.Border = 1;
                celNoFactura.BackgroundColor = BaseColor.WHITE;
                tableFactura.AddCell(celNoFactura);


                iTextSharp.text.pdf.PdfPCell celEspacioNoFactura = new iTextSharp.text.pdf.PdfPCell(new Phrase(" ", fontTitleFactura));
                celEspacioNoFactura.Colspan = 1;
                celEspacioNoFactura.Border = 0;
                //celNo.Padding = 3;
                celEspacioNoFactura.BorderWidthRight = 1;
                celEspacioNoFactura.HorizontalAlignment = Element.ALIGN_CENTER;
                celEspacioNoFactura.VerticalAlignment = Element.ALIGN_TOP;
                //celEspacioNoFactura.BackgroundColor = BaseColor.LIGHT_GRAY;
                tableFactura.AddCell(celEspacioNoFactura);

                iTextSharp.text.pdf.PdfPCell celRellenoNoFactura = new iTextSharp.text.pdf.PdfPCell(new Phrase(" ", fontTitleFactura));
                celRellenoNoFactura.Colspan = 3;
                celRellenoNoFactura.Padding = 3;
                celRellenoNoFactura.Border = 0;
                celRellenoNoFactura.BorderWidthLeft = 1;
                celRellenoNoFactura.BorderWidthRight = 1;
                celRellenoNoFactura.BorderWidthBottom = 1;
                //celRellenoNoFactura.BackgroundColor = BaseColor.LIGHT_GRAY;
                celRellenoNoFactura.HorizontalAlignment = Element.ALIGN_RIGHT;
                celRellenoNoFactura.VerticalAlignment = Element.ALIGN_TOP;
                tableFactura.AddCell(celRellenoNoFactura);

                iTextSharp.text.pdf.PdfPCell celConsecutivoInterno = new iTextSharp.text.pdf.PdfPCell(new Phrase("CUFE" + CUFE, fontCustom));
                celConsecutivoInterno.Colspan = 4;
                celConsecutivoInterno.BackgroundColor = BaseColor.WHITE;
                celConsecutivoInterno.HorizontalAlignment = Element.ALIGN_RIGHT;
                celConsecutivoInterno.VerticalAlignment = Element.ALIGN_TOP;
                celConsecutivoInterno.Border = 0;
                tableFactura.AddCell(celConsecutivoInterno);

                PdfDiv divNumeroFactura = new PdfDiv();
                divNumeroFactura.Float = iTextSharp.text.pdf.PdfDiv.FloatType.LEFT;
                divNumeroFactura.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divNumeroFactura.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                divNumeroFactura.PaddingLeft = 5;
                divNumeroFactura.Width = 140;
                divNumeroFactura.AddElement(tableFactura);
                cell_factura.AddElement(tableFactura);
                Header.AddCell(cell_factura);


                #endregion

                #region Body
                //CellEvent = CelEventBorderRound,

                PdfPTable cabesa = new PdfPTable(1);
                cabesa.WidthPercentage = 100;
                PdfPCell cabesa1 = new PdfPCell() { Border = 0, CellEvent = CelEventBorderRound, };

                PdfPTable Body = new PdfPTable(new float[] { 0.5f, 0.30f, 0.25f });
                Body.WidthPercentage = 100;

                PdfPCell customer = new PdfPCell() { Border = 0, };
                customer.Border = PdfPCell.RIGHT_BORDER;

                //DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceNum"].ToString(), fontTitle3));

                    PdfPTable cliente = new PdfPTable(1);
                    cliente.WidthPercentage = 100;
                    PdfPCell cliente1 = new PdfPCell(new Phrase("Cliente: " + DsInvoiceAR.Tables["Customer"].Rows[0]["Name"].ToString(), fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT };
                    cliente.AddCell(cliente1);
                    customer.AddElement(cliente);

                    PdfPTable nombnre = new PdfPTable(1);
                    nombnre.WidthPercentage = 100;
                    PdfPCell nombnre1 = new PdfPCell(new Phrase("Pais: " + DsInvoiceAR.Tables["Customer"].Rows[0]["Country"].ToString(), fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT };
                    nombnre.AddCell(nombnre1);
                    customer.AddElement(nombnre);

                    PdfPTable ciudad = new PdfPTable(1);
                    ciudad.WidthPercentage = 100;
                    PdfPCell ciudad1 = new PdfPCell(new Phrase("Ciudad: " + DsInvoiceAR.Tables["Customer"].Rows[0]["City"].ToString(), fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT };
                    ciudad.AddCell(ciudad1);
                    customer.AddElement(ciudad);

                    PdfPTable tel = new PdfPTable(1);
                    tel.WidthPercentage = 100;
                    PdfPCell tel1 = new PdfPCell(new Phrase("Telefono: " + DsInvoiceAR.Tables["Customer"].Rows[0]["PhoneNum"].ToString(), fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT };
                    tel.AddCell(tel1);
                    customer.AddElement(tel);

                    PdfPTable nit = new PdfPTable(1);
                    nit.WidthPercentage = 100;
                    PdfPCell nit1 = new PdfPCell(new Phrase("Nit: " + DsInvoiceAR.Tables["Customer"].Rows[0]["CustID"].ToString(), fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT };
                    nit.AddCell(nit1);
                    customer.AddElement(nit);


                PdfPCell lugar = new PdfPCell() { Border = 0, };
                lugar.Border = PdfPCell.RIGHT_BORDER;

                    PdfPTable fecha = new PdfPTable(1);
                    fecha.WidthPercentage = 100;
                    PdfPCell fecha1 = new PdfPCell(new Phrase("Fecha de Expedición:", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT };
                    fecha.AddCell(fecha1);
                    lugar.AddElement(fecha);

                    PdfPTable fecha_V = new PdfPTable(1);
                    fecha_V.WidthPercentage = 100; 
                     PdfPCell fecha_V1 = new PdfPCell(new Phrase(DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"].ToString(), fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, MinimumHeight=15};
                    fecha_V1.Border = PdfPCell.BOTTOM_BORDER;
                    fecha_V.AddCell(fecha_V1);
                    lugar.AddElement(fecha_V);

                    PdfPTable vendedor = new PdfPTable(1);
                    vendedor.WidthPercentage = 100;
                    PdfPCell vendedor1 = new PdfPCell(new Phrase("Vendedor", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT };
                    vendedor.AddCell(vendedor1);
                    lugar.AddElement(vendedor);

                    PdfPTable vendedor_v = new PdfPTable(1); 
                    vendedor_v.WidthPercentage = 100;
                    PdfPCell vendedor_v1 = new PdfPCell(new Phrase(DsInvoiceAR.Tables["InvcHead"].Rows[0]["SalesRepName1"].ToString(), fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT };
                    vendedor_v.AddCell(vendedor_v1);
                    lugar.AddElement(vendedor_v);

                PdfPCell vencimient = new PdfPCell() { Border = 0, };
                    PdfPTable vencimiento_ = new PdfPTable(1);
                    vencimiento_.WidthPercentage = 100;
                    PdfPCell vencimiento_1 = new PdfPCell(new Phrase("Vencimient:", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT };
                    vencimiento_.AddCell(vencimiento_1);
                    vencimient.AddElement(vencimiento_);

                    PdfPTable vencimiento_v = new PdfPTable(1);
                    vencimiento_v.WidthPercentage = 100;
                    PdfPCell vencimiento_v1 = new PdfPCell(new Phrase(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DueDate"].ToString(), fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, MinimumHeight = 15 };
                    vencimiento_v1.Border = PdfPCell.BOTTOM_BORDER;
                    vencimiento_v.AddCell(vencimiento_v1);
                    vencimient.AddElement(vencimiento_v);

                    

                    PdfPTable condiciones = new PdfPTable(1);
                    condiciones.WidthPercentage = 100;
                    PdfPCell condiciones1 = new PdfPCell(new Phrase("Condiciones:", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT };
                    condiciones.AddCell(condiciones1);
                    vencimient.AddElement(condiciones);

                    PdfPTable condiciones_v = new PdfPTable(1);
                    condiciones_v.WidthPercentage = 100;
                    PdfPCell condiciones_v1 = new PdfPCell(new Phrase(DsInvoiceAR.Tables["InvcHead"].Rows[0]["ShortChar01"].ToString(), fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT };
                    condiciones_v.AddCell(condiciones_v1);
                    vencimient.AddElement(condiciones_v);


                Body.AddCell(customer);
                Body.AddCell(lugar);
                Body.AddCell(vencimient);
                cabesa1.AddElement(Body);
                cabesa.AddCell(cabesa1);
                #endregion

                #region Unidades

                //celTittle.BackgroundColor = BaseColor.LIGHT_GRAY;
                PdfPTable cuerpomedio = new PdfPTable(1);
                cuerpomedio.WidthPercentage = 100;
                PdfPCell cuerpomedio1 = new PdfPCell() { };

                PdfPTable detalles = new PdfPTable(new float[] { 0.4f, 1.5f, 0.4f, 0.4f, 0.5f, 0.5f });
                detalles.WidthPercentage = 100;

                PdfPCell codigo = new PdfPCell(new Phrase("CÓDIGO", fontCustom)) { BackgroundColor = BaseColor.LIGHT_GRAY, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_BOTTOM, MinimumHeight = 15, };
                PdfPCell descrip = new PdfPCell(new Phrase("DESCRIPCION DEL PRODUCTO", fontCustom)) { BackgroundColor = BaseColor.LIGHT_GRAY, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_BOTTOM, MinimumHeight = 15, };
                PdfPCell unidades = new PdfPCell(new Phrase("CANTIDAD", fontCustom)) { BackgroundColor = BaseColor.LIGHT_GRAY, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_BOTTOM, MinimumHeight = 15, };
                PdfPCell dicuento = new PdfPCell(new Phrase("% DTO", fontCustom)) { BackgroundColor = BaseColor.LIGHT_GRAY, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_BOTTOM, MinimumHeight = 15, };
                PdfPCell Dto = new PdfPCell(new Phrase("PRECIO UNIT.", fontCustom)) { BackgroundColor = BaseColor.LIGHT_GRAY, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_BOTTOM, MinimumHeight = 15, };
                PdfPCell preciounitario = new PdfPCell(new Phrase("SUBTOTAL", fontCustom)) { BackgroundColor = BaseColor.LIGHT_GRAY, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_BOTTOM, MinimumHeight = 15, };

                detalles.AddCell(codigo);
                detalles.AddCell(descrip);
                detalles.AddCell(unidades);
                detalles.AddCell(dicuento);
                detalles.AddCell(Dto);
                detalles.AddCell(preciounitario);

                PdfPTable tableTituloUnidades = new PdfPTable(6);
                //Dimenciones.
                float[] DimencionUnidades = new float[6];
                DimencionUnidades[0] = 0.4f;//CÓDIGO
                DimencionUnidades[1] = 1.5f;//DESCRIPCION
                DimencionUnidades[2] = 0.4f;//CANTIDAD
                DimencionUnidades[3] = 0.4f;//DTO
                DimencionUnidades[4] = 0.5f;//PRECIO
                DimencionUnidades[5] = 0.5f;//SUBTOTAL


                PdfPTable tableUnidades = new PdfPTable(6);
                tableUnidades.WidthPercentage = 100;
                tableUnidades.SetWidths(DimencionUnidades);

                foreach (DataRow InvoiceLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
                {
                    if (!AddUnidadesFactory(InvoiceLine, ref tableUnidades, fontCustom, DsInvoiceAR))
                        return false;
                }

                iTextSharp.text.pdf.PdfPCell LineaFinal = new iTextSharp.text.pdf.PdfPCell();
                LineaFinal.Colspan = 6;
                LineaFinal.Padding = 3;
                LineaFinal.Border = 1;
                LineaFinal.BorderColorBottom = BaseColor.WHITE;
                LineaFinal.HorizontalAlignment = Element.ALIGN_CENTER;
                LineaFinal.VerticalAlignment = Element.ALIGN_TOP;
                tableUnidades.AddCell(LineaFinal);



                cuerpomedio1.AddElement(detalles);
                cuerpomedio.AddCell(cuerpomedio1);
                cuerpomedio.AddCell(tableTituloUnidades);
                cuerpomedio.AddCell(tableUnidades);

                #endregion

                #region Footer

                PdfPTable footer = new PdfPTable(new float[] { 0.7f,  0.3f,});
                footer.WidthPercentage = 100;

                PdfPCell totales1 = new PdfPCell() { Border=0,};

                PdfPTable footer1 = new PdfPTable(new float[] { 0.7f, 0.3f, });
                footer1.WidthPercentage = 100;
                PdfPCell v_l = new PdfPCell(new Phrase("Valor total en letras:   " + DsInvoiceAR.Tables["InvcDtl"].Rows[0]["ShortChar01"].ToString(), fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP, MinimumHeight= 40,};
                PdfPCell t_c = new PdfPCell(new Phrase("Total cantidades:  " + DsInvoiceAR.Tables["InvcDtl"].Rows[0]["Number01"].ToString(), fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP, };
                footer1.AddCell(v_l);
                footer1.AddCell(t_c);
                totales1.AddElement(footer1);
                
                PdfPTable footer2 = new PdfPTable(1);
                footer2.WidthPercentage = 100;
                PdfPCell v_l23 = new PdfPCell(new Phrase("Esta Factura de Venta Se asimila en sus efectos a la letra de cambio Art. 774 del codigo de comcio. RES" +
                                                        " 18762005817639 del 30 de Noviembre de 2017. Numeracion F 15888 Hasta F 50000.- IVA REGIMEN COMUN. " +
                                                        "CODIGO ACTIVIDAD PRINCIPAL 2930", fontTitleFactura))
                { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP, MinimumHeight = 06, };
                footer2.AddCell(v_l23);
                totales1.AddElement(footer2);

                PdfPCell totales2 = new PdfPCell() { Border = 0, CellEvent = CelEventBorderRound, MinimumHeight = 30, };

                PdfPTable total_dividido = new PdfPTable(2);
                total_dividido.WidthPercentage=100;

                PdfPCell t_1 = new PdfPCell() { Border=0,};
               
                PdfPTable subtotal = new PdfPTable(1); 
                subtotal.WidthPercentage = 100;
                PdfPCell subtotal1 = new PdfPCell(new Phrase("Subtoal:", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT };
                subtotal.AddCell(subtotal1);
                t_1.AddElement(subtotal);

                PdfPTable descuento = new PdfPTable(1); 
                descuento.WidthPercentage = 100;
                PdfPCell descuento1 = new PdfPCell(new Phrase("Descuento: " , fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT };
                descuento.AddCell(descuento1);
                t_1.AddElement(descuento);

                PdfPTable iva = new PdfPTable(1); 
                iva.WidthPercentage = 100;
                PdfPCell iva1 = new PdfPCell(new Phrase("IVA:", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT };
                iva.AddCell(iva1);
                t_1.AddElement(iva);

                PdfPTable retefuente = new PdfPTable(1);
                retefuente.WidthPercentage = 100;
                PdfPCell retefuente1 = new PdfPCell(new Phrase("Retefuente:", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT };
                retefuente.AddCell(retefuente1);
                t_1.AddElement(retefuente);

                PdfPTable total = new PdfPTable(1);
                total.WidthPercentage = 100;
                PdfPCell total1 = new PdfPCell(new Phrase("TOTAL:" , fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT };
                total.AddCell(total1);
                t_1.AddElement(total);

                PdfPCell t_2 = new PdfPCell() { Border = 0, };

                PdfPTable subtotal2 = new PdfPTable(1);
                subtotal2.WidthPercentage = 100;
                PdfPCell subtotal21 = new PdfPCell(new Phrase("$ " + DsInvoiceAR.Tables["InvcDtl"].Rows[0]["DspDocExtPrice"].ToString(), fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_RIGHT };
                subtotal2.AddCell(subtotal21);
                t_2.AddElement(subtotal2);

                PdfPTable descuento2 = new PdfPTable(1);
                descuento2.WidthPercentage = 100;
                PdfPCell descuento21 = new PdfPCell(new Phrase("$ " + DsInvoiceAR.Tables["InvcDtl"].Rows[0]["Number02"].ToString(), fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_RIGHT };
                descuento2.AddCell(descuento21);
                t_2.AddElement(descuento2);

                PdfPTable iva2 = new PdfPTable(1);
                iva2.WidthPercentage = 100;
                PdfPCell iva21 = new PdfPCell(new Phrase("$ " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["DocTaxAmt"].ToString(), fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_RIGHT };
                iva2.AddCell(iva21);
                t_2.AddElement(iva2);

                PdfPTable retefuente2 = new PdfPTable(1);
                retefuente2.WidthPercentage = 100;
                PdfPCell retefuente21 = new PdfPCell(new Phrase("$ " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["DocWHTaxAmt"].ToString(), fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_RIGHT };
                retefuente2.AddCell(retefuente21);
                t_2.AddElement(retefuente2);

                PdfPTable total2 = new PdfPTable(1);
                total2.WidthPercentage = 100;
                PdfPCell total21 = new PdfPCell(new Phrase("$ "+ DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"].ToString(), fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_RIGHT };
                total2.AddCell(total21);
                t_2.AddElement(total2);


                total_dividido.AddCell(t_1);
                total_dividido.AddCell(t_2);
                totales2.AddElement(total_dividido);

                footer.AddCell(totales1);
                footer.AddCell(totales2);

                cell_todo.AddElement(Header);
                cell_todo.AddElement(cabesa);
                cell_todo.AddElement(cuerpomedio);
                cell_todo.AddElement(footer);
                todo.AddCell(cell_todo);


                PdfPTable pie_info = new PdfPTable(new float[] { 0.69f, 0.31f, });
                pie_info.WidthPercentage = 100;
                PdfPCell tel_pie = new PdfPCell(new Phrase("Consignar cuenta Corriente No. 035-869999405 Davivienda a FACTORY PARTS ACCESORIES S.A.S.  " +
                    "Consignar Cuenta de Ahorros No. 097-706482-51 Bancolobia a FACTORY PARTS ACCESORIES S.A.S.  " +
                    "Consignar Cuenta de Ahorros No. 3130-30006610 Banco Agrario a FACTORY PARTS ACCESORIES S.A.S.  " +
                    "Consignar Cuenta de Ahorros No. 097 74302611 Bancolobia a FACTORY PARTS ACCESORIES S.A.S. " +
                    " ", fontCustom)) { Border = 0 , HorizontalAlignment=Element.ALIGN_LEFT};
                PdfPCell tel_pie_0 = new PdfPCell() { Border=0,};
                pie_info.AddCell(tel_pie);
                pie_info.AddCell(tel_pie_0);

                #endregion

                #region Exit

                document.Add(todo);
                document.Add(pie_info);

                //document.Add(divTextBancoCheque);
                /*PIE DE PAGINA*/
                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();
                RutaPdf = NomArchivo;
                return true;
            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.Message;
                return false;
            }
            #endregion
        }

        public bool NotaCreditoFactory(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {  
            NomArchivo = string.Empty;
            string Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
            string LocalIP = GetLocalIPv4(NetworkInterfaceType.Ethernet);
            string RutaImg = $@"{AppDomain.CurrentDomain.BaseDirectory}\Logo_Factory_Cauchos.png";
            string RutaMarcaAgua = $@"{AppDomain.CurrentDomain.BaseDirectory}\marca_agua.png";
            NomArchivo = NombreInvoice + ".pdf";
            try
            {
                #region Head
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.
                Document document = new Document(iTextSharp.text.PageSize.LETTER);
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;

                // step 3: we open the document     
                document.Open();

                PdfDiv divEspacio = new PdfDiv();
                divEspacio.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio.Height = 10;
                divEspacio.Width = 130;

                PdfDiv divEspacio2 = new PdfDiv();
                divEspacio2.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
                divEspacio2.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
                //divEspacio.BackgroundColor = BaseColor.LIGHT_GRAY;
                divEspacio2.Height = 2;
                divEspacio2.Width = 130;

                //FUENTES----------------------------------------------------------------------------------------------------------
                iTextSharp.text.Font fontTitle = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle3 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 9, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitle2 = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontCustom = FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font fontTitleFactura = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7, iTextSharp.text.Font.NORMAL);
                //FUENTES---------------------------------------------------------------------------------------------------------

                RoundRectangle CelEventBorderRound = new RoundRectangle();


                System.Drawing.Image logo = null;
                var requestLogo = WebRequest.Create(RutaImg);

                using (var responseLogo = requestLogo.GetResponse())
                using (var streamLogo = responseLogo.GetResponseStream())
                {
                    logo = Bitmap.FromStream(streamLogo);
                }

                System.Drawing.Image logo2 = null;
                var requestLogo2 = WebRequest.Create(RutaImg);

                using (var responseLogo2 = requestLogo2.GetResponse())
                using (var streamLogo2 = responseLogo2.GetResponseStream())
                {
                    logo2 = Bitmap.FromStream(streamLogo2);
                }

                //iTextSharp.text.Image ImgLogo2 = iTextSharp.text.Image.GetInstance(logo2, BaseColor.WHITE);
                //ImgLogo2.ScaleAbsolute(130, 130);
                //logo
                iTextSharp.text.Image ImgLogo2 = iTextSharp.text.Image.GetInstance(logo2, BaseColor.WHITE);
                ImgLogo2.ScaleAbsolute(200, 200);
                //Qr
                iTextSharp.text.Image ImgQR = iTextSharp.text.Image.GetInstance(QRInvoice, BaseColor.WHITE);
                ImgQR.ScaleAbsolute(60, 60);


                #endregion

                #region Header

                PdfPTable header = new PdfPTable(new float[] { 0.3f, 0.5f, 0.2f, });
                header.WidthPercentage = 100;

                PdfPCell cell_logo = new PdfPCell() { Border = 0, };
                cell_logo.AddElement(ImgLogo2);

                PdfPCell cell_mid = new PdfPCell() { Border = 0, };

                PdfPCell cell_lQr = new PdfPCell() { Border = 0, HorizontalAlignment = Element.ALIGN_RIGHT };
                cell_lQr.AddElement(ImgQR);

                header.AddCell(cell_logo);
                header.AddCell(cell_mid);
                header.AddCell(cell_lQr);


                PdfPTable info = new PdfPTable(3);
                info.WidthPercentage = 100;

                PdfPCell cell_inicio_vacio = new PdfPCell() { Border = 0, };
                cell_inicio_vacio.Border = PdfPCell.TOP_BORDER;
                info.AddCell(cell_inicio_vacio);

                PdfPCell cell_info = new PdfPCell(new Phrase("")) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER };
                cell_info.Border = PdfPCell.TOP_BORDER;
                Paragraph prgTitle1 = new Paragraph(string.Format("FACTORY PARTS  Y ACCESORIES S.A.A"), fontCustom) { Alignment = Element.ALIGN_CENTER, };
                Paragraph prgTitle2 = new Paragraph(string.Format("NIT. 900420346-1"), fontCustom) { Alignment = Element.ALIGN_CENTER, };
                Paragraph prgTitle3 = new Paragraph(string.Format("Calle 76No. 50B-39"), fontCustom) { Alignment = Element.ALIGN_CENTER, };
                Paragraph prgTitle4 = new Paragraph(string.Format("TELS.263 8253-581 1555"), fontCustom) { Alignment = Element.ALIGN_CENTER, };
                Paragraph prgTitle5 = new Paragraph(string.Format("e-MAIL. FACTORYPARTS2011@GMAIL.COM"), fontCustom) { Alignment = Element.ALIGN_CENTER, };
                Paragraph prgTitle6 = new Paragraph(string.Format(""), fontCustom) { Alignment = Element.ALIGN_CENTER, };
                cell_info.AddElement(prgTitle1);
                cell_info.AddElement(prgTitle2);
                cell_info.AddElement(prgTitle3);
                cell_info.AddElement(prgTitle4);
                cell_info.AddElement(prgTitle5);
                cell_info.AddElement(prgTitle6);
                info.AddCell(cell_info);

                PdfPCell cell_factura = new PdfPCell(new Phrase("NOTA CREDITO No. : " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceNum"].ToString(), fontCustom))
                { Border = 0, HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_BOTTOM };
                cell_factura.Border = PdfPCell.TOP_BORDER;
                info.AddCell(cell_factura);


                PdfPTable custome = new PdfPTable(new float[] { 0.7f, 0.3f });
                custome.WidthPercentage = 100;
                //agregamos datosn del cliente 
                PdfPCell datos = new PdfPCell();
                datos.Border = PdfPCell.TOP_BORDER;

                PdfPTable custom = new PdfPTable(new float[] { 0.2f, 0.8f });
                custom.WidthPercentage = 100;

                PdfPCell datos1 = new PdfPCell() { Border = 0, };

                PdfPTable cliente = new PdfPTable(1);
                cliente.WidthPercentage = 100;
                PdfPCell cliente1 = new PdfPCell(new Phrase("SEÑORES: ", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT };
                cliente.AddCell(cliente1);
                datos1.AddElement(cliente);

                PdfPTable nombnre = new PdfPTable(1);
                nombnre.WidthPercentage = 100;
                PdfPCell nombnre1 = new PdfPCell(new Phrase("NIT o C.C: ", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT };
                nombnre.AddCell(nombnre1);
                datos1.AddElement(nombnre);

                PdfPTable ciudad = new PdfPTable(1);
                ciudad.WidthPercentage = 100;
                PdfPCell ciudad1 = new PdfPCell(new Phrase("DIRECCION : ", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT };
                ciudad.AddCell(ciudad1);
                datos1.AddElement(ciudad);

                PdfPTable tel = new PdfPTable(1);
                tel.WidthPercentage = 100;
                PdfPCell tel1 = new PdfPCell(new Phrase("CIUDAD: ", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT };
                tel.AddCell(tel1);
                datos1.AddElement(tel);

                PdfPTable nit = new PdfPTable(1);
                nit.WidthPercentage = 100;
                PdfPCell nit1 = new PdfPCell(new Phrase("LA SUMA DE : ", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT };
                nit.AddCell(nit1);
                datos1.AddElement(nit);

                custom.AddCell(datos1);

                PdfPCell datos2 = new PdfPCell() { Border = 0, };

                PdfPTable cliente2 = new PdfPTable(1);
                cliente2.WidthPercentage = 100;
                PdfPCell cliente21 = new PdfPCell(new Phrase(DsInvoiceAR.Tables["Customer"].Rows[0]["Name"].ToString(), fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT };
                cliente2.AddCell(cliente21);
                datos2.AddElement(cliente2);

                PdfPTable nombnre2 = new PdfPTable(1);
                nombnre2.WidthPercentage = 100;
                PdfPCell nombnre21 = new PdfPCell(new Phrase(DsInvoiceAR.Tables["Customer"].Rows[0]["CustID"].ToString(), fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT };
                nombnre2.AddCell(nombnre21);
                datos2.AddElement(nombnre2);

                PdfPTable ciudad2 = new PdfPTable(1);
                ciudad2.WidthPercentage = 100;
                PdfPCell ciudad21 = new PdfPCell(new Phrase(DsInvoiceAR.Tables["Customer"].Rows[0]["Address1"].ToString(), fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT };
                ciudad2.AddCell(ciudad21);
                datos2.AddElement(ciudad2);

                PdfPTable tel2 = new PdfPTable(1);
                tel2.WidthPercentage = 100;
                PdfPCell tel21 = new PdfPCell(new Phrase(DsInvoiceAR.Tables["Customer"].Rows[0]["City"].ToString(), fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT };
                tel2.AddCell(tel21);
                datos2.AddElement(tel2);

                PdfPTable nit2 = new PdfPTable(1);
                nit2.WidthPercentage = 100;
                PdfPCell nit21 = new PdfPCell(new Phrase(DsInvoiceAR.Tables["InvcDtl"].Rows[0]["ShortChar01"].ToString(), fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT };
                nit2.AddCell(nit21);
                datos2.AddElement(nit2);

                custom.AddCell(datos2);

                datos.AddElement(custom);
                custome.AddCell(datos);

                //agregamos la fecha del documento 
                PdfPCell fechas = new PdfPCell();
                fechas.Border = PdfPCell.TOP_BORDER;

                PdfPTable fecha = new PdfPTable(1);
                fecha.WidthPercentage = 100;
                PdfPCell fecha1 = new PdfPCell(new Phrase("FECHA : " + DateTime.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceDate"]).ToString("dd/MM/yyyy"), fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, MinimumHeight = 30, VerticalAlignment = Element.ALIGN_BOTTOM, };
                fecha.AddCell(fecha1);
                fechas.AddElement(fecha);

                PdfPTable mont = new PdfPTable(1);
                mont.WidthPercentage = 100;
                PdfPCell mont1 = new PdfPCell(new Phrase("MONTO : " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"].ToString(), fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT };
                mont.AddCell(mont1);
                fechas.AddElement(mont);

                PdfPTable mont2 = new PdfPTable(1);
                mont2.WidthPercentage = 100;
                PdfPCell mont21 = new PdfPCell(new Phrase("VEND : ", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT };
                mont2.AddCell(mont21);
                fechas.AddElement(mont2);


                custome.AddCell(fechas);


                #endregion

                #region Body

                PdfPTable t_linea = new PdfPTable(1);
                t_linea.WidthPercentage = 100;
                PdfPCell cell_linea = new PdfPCell();
                t_linea.AddCell(cell_linea);

                PdfPTable Body = new PdfPTable(new float[] { 0.1f, 0.4f, 0.1f, 0.1f, 0.1f, 0.1f, 0.1f, 0.1f, });
                Body.WidthPercentage = 100;

                PdfPCell factura = new PdfPCell(new Phrase("CODIGO", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, };
                factura.Border = PdfPCell.BOTTOM_BORDER;
                PdfPCell valor_b = new PdfPCell(new Phrase("NOMBRE DEL PRODUCTO", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, };
                valor_b.Border = PdfPCell.BOTTOM_BORDER;
                PdfPCell iva = new PdfPCell(new Phrase("CANTIDAD", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, };
                iva.Border = PdfPCell.BOTTOM_BORDER;
                PdfPCell ret_fuente = new PdfPCell(new Phrase("UNIDAD", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, };
                ret_fuente.Border = PdfPCell.BOTTOM_BORDER;
                PdfPCell ret_iva = new PdfPCell(new Phrase("% DCTO", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_TOP, };
                ret_iva.Border = PdfPCell.BOTTOM_BORDER;
                PdfPCell descuento = new PdfPCell(new Phrase("%IVA", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, };
                descuento.Border = PdfPCell.BOTTOM_BORDER;
                PdfPCell vr_neto = new PdfPCell(new Phrase("PRECIO", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, };
                vr_neto.Border = PdfPCell.BOTTOM_BORDER;
                PdfPCell concepto = new PdfPCell(new Phrase("VALOR", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_TOP, };
                concepto.Border = PdfPCell.BOTTOM_BORDER;
                Body.AddCell(factura);
                Body.AddCell(valor_b);
                Body.AddCell(iva);
                Body.AddCell(ret_fuente);
                Body.AddCell(ret_iva);
                Body.AddCell(descuento);
                Body.AddCell(vr_neto);
                Body.AddCell(concepto);

                #endregion

                #region Unidades

                PdfPTable tableTituloUnidades = new PdfPTable(8);
                //Dimenciones.
                float[] DimencionUnidades = new float[8];
                DimencionUnidades[0] = 1.0F;//FACTURA
                DimencionUnidades[1] = 4.0F;//VALOR BRUTO
                DimencionUnidades[2] = 1.0F;//IVA
                DimencionUnidades[3] = 1.0F;//RET FUENTE
                DimencionUnidades[4] = 1.0F;//RET  IVA
                DimencionUnidades[5] = 1.0F;//DESCUENTO
                DimencionUnidades[6] = 1.0F;//VR NETO
                DimencionUnidades[7] = 1.0F;//CONCEPTO



                PdfPTable tableUnidades = new PdfPTable(8);
                tableUnidades.WidthPercentage = 100;
                tableUnidades.SetWidths(DimencionUnidades);

                foreach (DataRow InvoiceLine in DsInvoiceAR.Tables["InvcDtl"].Rows)
                {
                    if (!AddUnidadesNOTAFactory(InvoiceLine, ref tableUnidades, fontCustom, DsInvoiceAR))
                        return false;
                }

                iTextSharp.text.pdf.PdfPCell LineaFinal = new iTextSharp.text.pdf.PdfPCell();
                LineaFinal.Colspan = 8;
                LineaFinal.Padding = 3;
                LineaFinal.Border = 1;
                LineaFinal.BorderColorBottom = BaseColor.WHITE;
                LineaFinal.HorizontalAlignment = Element.ALIGN_CENTER;
                LineaFinal.VerticalAlignment = Element.ALIGN_TOP;
                tableUnidades.AddCell(LineaFinal);

                #endregion

                #region  Footer
                PdfPTable espacio = new PdfPTable(1);
                espacio.WidthPercentage = 100;
                PdfPCell cell_espacio = new PdfPCell() { Border=0,MinimumHeight=50,};
                espacio.AddCell(cell_espacio);

                PdfPTable firma = new PdfPTable(new float[] { 0.7f, 0.3f });
                firma.WidthPercentage = 100;
                PdfPCell vacio_firma = new PdfPCell() { Border = 0, };
                PdfPCell firma_ = new PdfPCell(new Phrase("", fontCustom)) { Border = 0, };
                firma.AddCell(vacio_firma);
                firma.AddCell(firma_);

                PdfPTable firmaCC = new PdfPTable(new float[] { 0.7f, 0.3f });
                firmaCC.WidthPercentage = 100;
                PdfPCell firmaCC1 = new PdfPCell(new Phrase("OBSERVACIONES : " + DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceComment"], fontCustom)) { Border = 0, };
                firma.AddCell(firmaCC1);
                PdfPCell firmaCC1_ = new PdfPCell(new Phrase("", fontCustom)) { Border = 0, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_BOTTOM, MinimumHeight = 35, };


                PdfPTable t_SUBTOTAL = new PdfPTable(2);
                t_SUBTOTAL.WidthPercentage = 100;
                PdfPCell cell_SUBTOTAL = new PdfPCell(new Phrase("SUBTOTAL:", fontCustom)) { HorizontalAlignment=Element.ALIGN_LEFT};
                t_SUBTOTAL.AddCell(cell_SUBTOTAL);
                PdfPCell cell_SUBTOTALv = new PdfPCell(new Phrase(string.Format("{0:C2}",
                    decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DocTaxAmt"]).ToString("N2")),fontCustom))
                { HorizontalAlignment=Element.ALIGN_RIGHT};
                t_SUBTOTAL.AddCell(cell_SUBTOTALv);
                firmaCC1_.AddElement(t_SUBTOTAL);

                PdfPTable t_DESCUENTO = new PdfPTable(2);
                t_DESCUENTO.WidthPercentage = 100;
                PdfPCell cell_DESCUENTO = new PdfPCell(new Phrase("DESCUENTO:", fontCustom)) { HorizontalAlignment = Element.ALIGN_LEFT };
                t_DESCUENTO.AddCell(cell_DESCUENTO);
                PdfPCell cell_DESCUENTv = new PdfPCell(new Phrase(""+Decimal.Parse((string)DsInvoiceAR.Tables["InvcDtl"].Rows[0]["Number02"]).ToString("N0"),fontCustom))
                { HorizontalAlignment = Element.ALIGN_RIGHT };
                t_DESCUENTO.AddCell(cell_DESCUENTv);
                firmaCC1_.AddElement(t_DESCUENTO);

                PdfPTable t_iva = new PdfPTable(2);
                t_iva.WidthPercentage = 100;
                PdfPCell cell_iva = new PdfPCell(new Phrase("I.V.A:", fontCustom)) { HorizontalAlignment = Element.ALIGN_LEFT };
                t_iva.AddCell(cell_iva);
                PdfPCell cell_ivav = new PdfPCell(new Phrase(string.Format("{0:C2}",
                    decimal.Parse(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DocWHTaxAmt"].ToString())), fontCustom))
                { HorizontalAlignment = Element.ALIGN_RIGHT };
                t_iva.AddCell(cell_ivav);
                firmaCC1_.AddElement(t_iva);

                PdfPTable t_rete = new PdfPTable(2);
                t_rete.WidthPercentage = 100;
                PdfPCell cell_rete = new PdfPCell(new Phrase("RETE FUENTE:", fontCustom)) { HorizontalAlignment = Element.ALIGN_LEFT };
                t_rete.AddCell(cell_rete);
                PdfPCell cell_retev = new PdfPCell(new Phrase(string.Format("{0:C2}",
                    decimal.Parse(DsInvoiceAR.Tables["InvcHead"].Rows[0]["DocWHTaxAmt"].ToString())), fontCustom))
                { HorizontalAlignment = Element.ALIGN_RIGHT };
                t_rete.AddCell(cell_retev);
                firmaCC1_.AddElement(t_rete);

                PdfPTable t_ri= new PdfPTable(2);
                t_ri.WidthPercentage = 100;
                PdfPCell cell_r = new PdfPCell(new Phrase("RETE IVA:", fontCustom)) { HorizontalAlignment = Element.ALIGN_LEFT };
                t_ri.AddCell(cell_r);
                PdfPCell cell_rv = new PdfPCell(new Phrase("", fontCustom))
                { HorizontalAlignment = Element.ALIGN_RIGHT };
                t_ri.AddCell(cell_rv);
                firmaCC1_.AddElement(t_ri);
                firma.AddCell(firmaCC1_);

                PdfPTable total_final = new PdfPTable(1);
                total_final.WidthPercentage = 100;
                PdfPCell cell_total_final = new PdfPCell(new Phrase("TOATL: " +"   $ "+ Decimal.Parse((string)DsInvoiceAR.Tables["InvcHead"].Rows[0]["DspDocInvoiceAmt"]).ToString("N2"), fontCustom)) { Border=0, HorizontalAlignment=Element.ALIGN_RIGHT};
                total_final.AddCell(cell_total_final);

                #endregion

                #region Exit

                document.Add(header);
                document.Add(info);
                document.Add(custome);
                document.Add(t_linea);
                document.Add(Body);
                document.Add(tableTituloUnidades);
                document.Add(tableUnidades);
                document.Add(espacio);
                document.Add(firma);
                document.Add(firmaCC);
                //document.Add(divTextBancoCheque);
                /*PIE DE PAGINA*/
                PdfContentByte pCb = writer.DirectContent;
                PieDePagina(ref pCb);

                writer.Flush();
                document.Close();
                RutaPdf = NomArchivo;
                return true;
                #endregion
            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.Message;
                return false;
            }       
        }

    }
}
