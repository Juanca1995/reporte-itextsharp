﻿namespace ReportesSimex.Plastinovo
{
    using Helpers;
    using iTextSharp.text;
    using iTextSharp.text.pdf;
    using System;
    //Referenciar y usar.
    using System.Data;
    using System.Drawing;
    using System.Net;

    public partial class Nacional
    {
        public static PdfPTable Encabezado(DataSet DsInvoiceAR, System.Drawing.Image QRInvoice)
        {
            PdfPTable tbPreEncabezado = new PdfPTable(2);
            tbPreEncabezado.SetWidths(new float[] { 4.64f, 1.1f });
            tbPreEncabezado.WidthPercentage = 100;
            //----- Parte 1: Logo de la compañia y su informacion --------
            PdfPTable tbLogoInfo = new PdfPTable(1);
            tbLogoInfo.WidthPercentage = 100;
            //----- Inicio Logo de la compañia -------------
            System.Drawing.Image logo = null;
            var requestLogo = WebRequest.Create($@"{AppDomain.CurrentDomain.BaseDirectory}\logoPlastinovo.png");
            using (var responseLogo = requestLogo.GetResponse())
            using (var streamLogo = responseLogo.GetResponseStream())
            {
                logo = Bitmap.FromStream(streamLogo);
            }
            PdfDiv divLogo = new PdfDiv();
            divLogo.Float = iTextSharp.text.pdf.PdfDiv.FloatType.LEFT;
            divLogo.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
            divLogo.BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID;
            divLogo.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
            divLogo.Height = 45;
            divLogo.Width = 150;
            //divLogo.BackgroundColor = BaseColor.LIGHT_GRAY;
            iTextSharp.text.Image ImgLogo = iTextSharp.text.Image.GetInstance(logo, BaseColor.WHITE);
            ImgLogo.Alignment = Element.ALIGN_CENTER;
            divLogo.BackgroundImage = ImgLogo;
            PdfPCell celLogo = new PdfPCell()
            {
                HorizontalAlignment = Element.ALIGN_LEFT,
                //Colspan = 2,
                Border = 0
            };
            celLogo.AddElement(divLogo);
            tbLogoInfo.AddCell(celLogo);
            //----- Fin Logo compañia -----------
            //------Datos de la compañia -----------
            Paragraph prgNombreEmpresa = new Paragraph(DsInvoiceAR.Tables["Company"].Rows[0]["Name"].ToString(), Fuentes.fontTitle);
            Paragraph prgNIT = new Paragraph(string.Format("NIT {0}", DsInvoiceAR.Tables["Company"].Rows[0]["StateTaxID"]), Fuentes.fontTitle);
            Paragraph prgDireccion = new Paragraph(DsInvoiceAR.Tables["Company"].Rows[0]["Address1"].ToString(), Fuentes.fontTitle);
            Paragraph prgUbicacion = new Paragraph(string.Format("{0} - {1}", DsInvoiceAR.Tables["Company"].Rows[0]["City"], DsInvoiceAR.Tables["Company"].Rows[0]["Country"]), Fuentes.fontTitle);
            Paragraph prgCodPostal = new Paragraph("CÓDIGO POSTAL 055460", Fuentes.fontTitle);
            Paragraph prgTelefono = new Paragraph(string.Format("TELÉFONO {0}", DsInvoiceAR.Tables["Company"].Rows[0]["PhoneNum"]), Fuentes.fontTitle);
            Paragraph prgEmail = new Paragraph("contabilidad@plastinovo.com", Fuentes.fontTitle);
            Paragraph prgWeb = new Paragraph("www.plastinovo.com", Fuentes.fontTitle);
            //------ Fin datos de la compañia -----------
            PdfPCell celTextInfoEmpresa = new PdfPCell()
            {
                HorizontalAlignment = Element.ALIGN_LEFT,
                //Colspan = 2,
                Border = 0,
                Padding = 0
            };
            celTextInfoEmpresa.AddElement(prgNombreEmpresa);
            celTextInfoEmpresa.AddElement(prgNIT);
            celTextInfoEmpresa.AddElement(prgDireccion);
            celTextInfoEmpresa.AddElement(prgUbicacion);
            celTextInfoEmpresa.AddElement(prgCodPostal);
            celTextInfoEmpresa.AddElement(prgTelefono);
            celTextInfoEmpresa.AddElement(prgEmail);
            celTextInfoEmpresa.AddElement(prgWeb);
            tbLogoInfo.AddCell(celTextInfoEmpresa);
            PdfPCell celLogoInfo = new PdfPCell();
            celLogoInfo.Border = 0;
            celLogoInfo.AddElement(tbLogoInfo);
            tbPreEncabezado.AddCell(celLogoInfo);
            //----- Parte 2, Código QR, numero de factura
            PdfPTable tbQRInvoice = new PdfPTable(1);
            tbQRInvoice.WidthPercentage = 100;
            //---- Código QR
            PdfDiv divQR = new PdfDiv();
            divQR.Float = iTextSharp.text.pdf.PdfDiv.FloatType.RIGHT;
            divQR.Position = iTextSharp.text.pdf.PdfDiv.PositionType.ABSOLUTE;
            divQR.BorderStyle = iTextSharp.text.pdf.PdfDiv.BorderTopStyle.SOLID;
            divQR.Display = iTextSharp.text.pdf.PdfDiv.DisplayType.BLOCK;
            divQR.Height = 75;
            divQR.Width = 75;
            divQR.BackgroundColor = BaseColor.BLUE;
            iTextSharp.text.Image ImgQR = iTextSharp.text.Image.GetInstance(QRInvoice, BaseColor.WHITE);
            ImgQR.Alignment = Element.ALIGN_RIGHT;
            divQR.BackgroundImage = ImgQR;
            var celQR = new PdfPCell()
            {
                Border = PdfPCell.NO_BORDER,
                HorizontalAlignment = Element.ALIGN_RIGHT
            };
            celQR.AddElement(divQR);
            tbQRInvoice.AddCell(celQR);
            //----- Fin código QR ---------
            //----- Informacion adicional --------
            Paragraph prgTituloFacturaEsp = new Paragraph("FACTURA DE VENTA NO.", FontFactory.GetFont("Arial", 7f, iTextSharp.text.Font.NORMAL));
            prgTituloFacturaEsp.Alignment = Element.ALIGN_RIGHT;
            Paragraph prgTituloFacturaIng = new Paragraph("INVOICE NO.", Fuentes.fontTitleLittleBold);
            prgTituloFacturaIng.Alignment = Element.ALIGN_RIGHT;
            Paragraph prgNumeroLegal = new Paragraph(string.Format("{0}", DsInvoiceAR.Tables["InvcHead"].Rows[0]["LegalNumber"]), FontFactory.GetFont("Arial", 10f, iTextSharp.text.Font.BOLD));
            prgNumeroLegal.Alignment = Element.ALIGN_RIGHT;
            Paragraph prgBlanco = new Paragraph(" ", Fuentes.fontTitle);
            Paragraph prgInvoiceNum = new Paragraph(string.Format("V: {0}", DsInvoiceAR.Tables["InvcHead"].Rows[0]["InvoiceNum"]), FontFactory.GetFont("Arial", 5, iTextSharp.text.Font.BOLD));
            prgInvoiceNum.Alignment = Element.ALIGN_RIGHT;
            PdfPCell celInfo = new PdfPCell()
            {
                HorizontalAlignment = Element.ALIGN_RIGHT,
                Border = 0,
                Padding = 0
            };
            celInfo.AddElement(prgTituloFacturaEsp);
            celInfo.AddElement(prgTituloFacturaIng);
            celInfo.AddElement(prgNumeroLegal);
            celInfo.AddElement(prgBlanco);
            celInfo.AddElement(prgInvoiceNum);


            //Phrase phTitulo = new Phrase();
            //Chunk chunk = new Chunk("Invoice No.", Fuentes.fontTitleItalic);
            //Chunk chunk2 = new Chunk("Factura", Fuentes.fontTitleBold);
            //phTitulo.Add(chunk);
            //phTitulo.Add(chunk2);
            //PdfPCell celtest = new PdfPCell();
            //celtest.AddElement(phTitulo);
            tbQRInvoice.AddCell(celInfo);
            //----- Fin informacion adicional ---------
            PdfPCell celda2 = new PdfPCell();
            celda2.Padding = 0;
            celda2.Border = 0;
            celda2.AddElement(tbQRInvoice);
            tbPreEncabezado.AddCell(celda2);
            return tbPreEncabezado;
        }
        public static PdfPTable DatosCliente(DataSet ds)
        {
            //-----------------------------------------------------------------------
            //------------------ Informacion de cliente -----------------------------
            //-----------------------------------------------------------------------
            PdfPTable tbClienteInfo = new PdfPTable(6);
            tbClienteInfo.SetWidths(new float[] { 1f, 2.2f, 1f, 1f, 1f, 1f });
            tbClienteInfo.WidthPercentage = 100;
            //--------- Doble linea
            PdfPCell celDobleLinea = new PdfPCell(new Phrase(" ", FontFactory.GetFont("Arial", 1.5f, iTextSharp.text.Font.NORMAL)));
            celDobleLinea.Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER;
            celDobleLinea.Colspan = 6;
            celDobleLinea.Padding = 0;
            tbClienteInfo.AddCell(celDobleLinea);

            //------- Nombre cliente
            Simex.AgregarDatosCliente(ref tbClienteInfo, "Cliente", "Customer", ds.Tables["Customer"].Rows[0]["Name"]);
            //------------ Codigo cliente
            Simex.AgregarDatosCliente(ref tbClienteInfo, "Código Cliente", "Customer Code", ds.Tables["Customer"].Rows[0]["CustID"]);
            //-------------- Moneda ------------------------
            Simex.AgregarDatosCliente(ref tbClienteInfo, "Moneda", "Currency", ds.Tables["Customer"].Rows[0]["CurrencyCode"]);
            //------------------ Direccion ------------------------------------
            string Direccion = string.Empty;
            Direccion += ds.Tables["Customer"].Rows[0]["Address1"].ToString() + "\r\n";
            Direccion += ds.Tables["Customer"].Rows[0]["City"].ToString() + "-" + ds.Tables["Customer"].Rows[0]["State"].ToString() + "\r\n";
            Direccion += ds.Tables["Customer"].Rows[0]["Country"].ToString();
            Simex.AgregarDatosCliente(ref tbClienteInfo, "Direccion Cliente", "Address", Direccion);
            //------------------ Incoterms ------------------------------------
            Simex.AgregarDatosCliente(ref tbClienteInfo, "Incoterms", "Incoterms", ds.Tables["InvcHead"].Rows[0]["FOB"]);
            //--------- Fecha de expedicion ------------------------------------------------------
            string FechaExpedicion = string.Empty;
            try
            {
                FechaExpedicion = ((DateTime)ds.Tables["InvcHead"].Rows[0]["InvoiceDate"]).ToString("dd/MM/yyyy");
            }
            catch { }
            Simex.AgregarDatosCliente(ref tbClienteInfo, "Fecha de expedicion", "Issue Date", FechaExpedicion);
            //-------- Teléfono ------------------------------
            Simex.AgregarDatosCliente(ref tbClienteInfo, "Teléfono", "Phone", ds.Tables["Customer"].Rows[0]["PhoneNum"]);
            //------------- Lugar de entrega -----------------------------------------
            Simex.AgregarDatosCliente(ref tbClienteInfo, "Lugar de entrega", "Place of delivery", "Medellín");
            //--------------- Fecha de vencimiento -----------------------------
            string FechaVencimiento = string.Empty;
            try
            {
                FechaVencimiento = ((DateTime)ds.Tables["InvcHead"].Rows[0]["DueDate"]).ToString("dd/MM/yyyy");
            }
            catch { }
            Simex.AgregarDatosCliente(ref tbClienteInfo, "Fecha de vencimiento", "Due Date", FechaVencimiento);
            //--------------- código fiscal -------------
            Simex.AgregarDatosCliente(ref tbClienteInfo, "Código fiscal", "Tax ID Number", ds.Tables["Customer"].Rows[0]["ResaleID"]);
            //------------------Casilla en blanco --------------
            Simex.AgregarDatosCliente(ref tbClienteInfo, " ", " ", " ");
            //------------------ Condiciones de pago -------------
            Simex.AgregarDatosCliente(ref tbClienteInfo, "NIT", "Tax ID Number", ds.Tables["Customer"].Rows[0]["ResaleID"]);

            return tbClienteInfo;
        }
        public static PdfPTable Detalles(DataSet ds)
        {
            PdfPTable tbDetalles = new PdfPTable(8);
            tbDetalles.WidthPercentage = 100;
            tbDetalles.SetWidths(new float[] { 0.5f, 1.8f, 1f, 1f, 0.4f, 1f, 1f, 1f });

            //--------- Doble linea
            PdfPCell celDobleLinea = new PdfPCell(new Phrase(" ", FontFactory.GetFont("Arial", 1.5f, iTextSharp.text.Font.NORMAL)));
            celDobleLinea.Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER;
            celDobleLinea.Colspan = 8;
            celDobleLinea.Padding = 0;
            tbDetalles.AddCell(celDobleLinea);
            //Encabezado código
            Simex.DetalleTitulos(ref tbDetalles, "Código", "Code");
            Simex.DetalleTitulos(ref tbDetalles, "Descripción", "Description");
            Simex.DetalleTitulos(ref tbDetalles, "OC", "Purchase Order");
            Simex.DetalleTitulos(ref tbDetalles, "Ref Cliente", "Item No.");
            Simex.DetalleTitulos(ref tbDetalles, "Und", "UOM");
            Simex.DetalleTitulos(ref tbDetalles, "Cantidad", "Quantity");
            Simex.DetalleTitulos(ref tbDetalles, "Vr Unitario", "Unit Price");
            Simex.DetalleTitulos(ref tbDetalles, "Total COP", "Total COP");
            //detalles de la factura
            Simex.DetalleValores(ref tbDetalles, ds);
            //---------------Total de la factura ----------
            PdfPTable tbTotal = new PdfPTable(2);
            tbTotal.WidthPercentage = 100;
            tbTotal.SetWidths(new float[] { 1f, 2f });
            //Titulo subtotal e IVA
            Paragraph prgTituloSubtotal = new Paragraph("SUBTOTAL", Fuentes.SimexDatos);
            Paragraph prgTituloIVA = new Paragraph("IVA 19%", Fuentes.SimexDatos);
            PdfPCell celTituloSubtotal = new PdfPCell();
            celTituloSubtotal.AddElement(prgTituloSubtotal);
            celTituloSubtotal.AddElement(prgTituloIVA);
            celTituloSubtotal.Border = 0;
            tbTotal.AddCell(celTituloSubtotal);
            //Valor subtotal
            string Subtotal = string.Empty;
            string IVA = string.Empty;
            Simex.ConvertirFormatoDecimal(ds.Tables["Invchead"].Rows[0]["DspDocSubTotal"], out Subtotal);
            Simex.ConvertirFormatoDecimal(ds.Tables["InvcHead"].Rows[0]["DocDspTaxAmt"], out IVA);
            Paragraph prgSubtotal = new Paragraph(Subtotal, Fuentes.SimexDatos);
            prgSubtotal.Alignment = Element.ALIGN_RIGHT;
            Paragraph prgIVA = new Paragraph(IVA, Fuentes.SimexDatos);
            prgIVA.Alignment = Element.ALIGN_RIGHT;
            PdfPCell celSubtotal = new PdfPCell();
            celSubtotal.AddElement(prgSubtotal);
            celSubtotal.AddElement(prgIVA);
            celSubtotal.HorizontalAlignment = Element.ALIGN_RIGHT;
            celSubtotal.Border = 0;
            tbTotal.AddCell(celSubtotal);

            //titulo total
            Paragraph prgTotalEsp = new Paragraph("TOTAL COP", FontFactory.GetFont("Arial", 10f, iTextSharp.text.Font.BOLDITALIC));
            Paragraph prgTotalIng = new Paragraph("TOTAL COP", FontFactory.GetFont("Arial", 7f, iTextSharp.text.Font.BOLDITALIC));
            PdfPCell celTituloTotal = new PdfPCell();
            celTituloTotal.AddElement(prgTotalEsp);
            celTituloTotal.AddElement(prgTotalIng);
            celTituloTotal.Border = 0;
            tbTotal.AddCell(celTituloTotal);
            //Valor total
            string Total = string.Empty;
            Simex.ConvertirFormatoDecimal(ds.Tables["InvcHead"].Rows[0]["DocInvoiceAmt"], out Total);
            PdfPCell celValorTotal = new PdfPCell(new Phrase(Total, FontFactory.GetFont("Arial", 10f, iTextSharp.text.Font.BOLD)));
            celValorTotal.Border = 0;
            celValorTotal.HorizontalAlignment = Element.ALIGN_RIGHT;
            celValorTotal.VerticalAlignment = Element.ALIGN_MIDDLE;
            tbTotal.AddCell(celValorTotal);
            //Agregando la tabla de total
            //celda en blanco
            Simex.CeldaBlanco(ref tbDetalles, 4);

            PdfPCell celTotal = new PdfPCell();
            celTotal.AddElement(tbTotal);
            celTotal.Colspan = 4;
            tbDetalles.AddCell(celTotal);
            //celda blanco
            Simex.CeldaBlanco(ref tbDetalles, 8);

            return tbDetalles;
        }
        public static PdfPTable Mercaderia(DataSet ds)
        {
            PdfPTable tbMercaderia = new PdfPTable(6);
            tbMercaderia.WidthPercentage = 100;
            tbMercaderia.SetWidths(new float[] { 1f, 1f, 1.2f, 1f, 1f, 1.5f });
            //--------- Doble linea
            PdfPCell celDobleLinea = new PdfPCell(new Phrase(" ", FontFactory.GetFont("Arial", 1.5f, iTextSharp.text.Font.NORMAL)));
            celDobleLinea.Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER;
            celDobleLinea.Colspan = 6;
            celDobleLinea.Padding = 0;
            tbMercaderia.AddCell(celDobleLinea);
            //-------- Titulo principal --------------------
            Paragraph prgTituloEsp = new Paragraph("MERCANCIA NUEVA Y ORIGINARIA DE COLOMBIA", Fuentes.SimexDatos);
            Paragraph prgTituloIng = new Paragraph("NEW GOODS MANUFACTURED IN COLOMBIA", Fuentes.SimexTituloIng);
            PdfPCell celTitulo = new PdfPCell();
            celTitulo.Border = 0;
            celTitulo.Colspan = 6;
            celTitulo.HorizontalAlignment = Element.ALIGN_LEFT;
            celTitulo.AddElement(prgTituloEsp);
            celTitulo.AddElement(prgTituloIng);
            tbMercaderia.AddCell(celTitulo);
            //-----------------------------------------------------------------------
            //------------------ Total cajas ---------------------
            Simex.AgregarDataMercaderia(ref tbMercaderia, "Total cajas", "Total Boxes", string.Format("{0}", 87));
            //----------------- Peso Neto ------------------------
            Simex.AgregarDataMercaderia(ref tbMercaderia, "Peso Neto", "Net Weight", string.Format("{0} KG", 3459.87));
            //---------------- Observaciones ------------------------
            Simex.AgregarDataMercaderia(ref tbMercaderia, "Observaciones", "Comments", ds.Tables["InvcHead"].Rows[0]["InvoiceComment"].ToString());
            //------------------ Dimensiones Caja ---------------------------
            Simex.AgregarDataMercaderia(ref tbMercaderia, "Dimensiones Caja", "Box Dimension", string.Format("{0}", "45x65x56"));
            //------------------ Peso bruto -----------------------
            Simex.AgregarDataMercaderia(ref tbMercaderia, "Peso Bruto", "Gross Weight", string.Format("{0} KG", 657.98));
            //------------------ Celda blanca 1 -----------------------------
            Simex.AgregarDataMercaderia(ref tbMercaderia, "", "", string.Format(" "));
            //------------------ Pos. Arancelaria ---------------------------------
            Simex.AgregarDataMercaderia(ref tbMercaderia, "Pos. Arancelaria", "Tariff Code", string.Format("56.90.87.09.90"));
            //--------------- Pos. Arancelaria Destino ------------------------
            Simex.AgregarDataMercaderia(ref tbMercaderia, "Pos. Arancelaria Destino", "Tariff Code", string.Format("56.90.87.09.90"));
            //----------------- Celda blanco 2 ---------------------------------------
            Simex.AgregarDataMercaderia(ref tbMercaderia, "", "", string.Format(" "));

            return tbMercaderia;
        }
        public static PdfPTable PiePagina(DataSet ds, string CUFE)
        {
            PdfPTable tbPiePagina = new PdfPTable(5);
            tbPiePagina.WidthPercentage = 100;
            tbPiePagina.SetWidths(new float[] { 1.8f, 0.2f, 1.8f, 0.2f, 2.5f });
            //--------- Doble linea
            Simex.DobleLinea(ref tbPiePagina, 6);
            //------ Titulos -----
            Simex.Titulos2(ref tbPiePagina, "ACEPTADA", "APPROVED BY", 2);
            Simex.Titulos2(ref tbPiePagina, "LA MERCANCÍA RELACIONADA FUE RECIBIDA REAL Y MATERIALMENTE", "GOODS INDICATED WERE RECEIVED", 3);
            //-------- Celda de la firma -----------------------
            PdfPCell celFirma1 = new PdfPCell(new Phrase(" ", FontFactory.GetFont("Arial", 30f, iTextSharp.text.Font.BOLD)));
            celFirma1.MinimumHeight = 1f;
            celFirma1.Border = PdfPCell.BOTTOM_BORDER;
            tbPiePagina.AddCell(celFirma1);
            //------------ celda separador 1 ----------------------
            PdfPCell separador1 = new PdfPCell();
            separador1.MinimumHeight = 1f;
            separador1.Border = 0;
            tbPiePagina.AddCell(separador1);
            //------------ Celda firma 2 -------------------------------
            PdfPCell celFirma2 = new PdfPCell();
            celFirma2.MinimumHeight = 1f;
            celFirma2.Border = PdfPCell.BOTTOM_BORDER;
            tbPiePagina.AddCell(celFirma2);
            //------------ celda en blanco ----------------------
            PdfPCell celFirma3 = new PdfPCell();
            celFirma3.MinimumHeight = 1f;
            celFirma3.Border = 0;
            celFirma3.Colspan = 2;
            tbPiePagina.AddCell(celFirma3);
            //----- Titulo firma y sello
            Simex.Titulos2(ref tbPiePagina, "FIRMA Y SELLO DEL VENDEDOR", "SIGNATURE AND STAMP FROM SUPPLIER", 2);
            Simex.Titulos2(ref tbPiePagina, "FIRMA Y SELLO DEL COMPRADOR", "SIGNATURE AND STAMP FROM BUYER", 3);
            //------- Doble linea ----------------------
            Simex.DobleLinea(ref tbPiePagina, 6);
            //----- Leyenda pagos efectuados -------------
            string Leyenda = "LOS PAGOS EFECTUADOS DESPUÉS DE LA FECHA DE VENCIMIENTO CAUSARÁN INTERESES DE MORA A LA TASA LEGAL VIGENTE CERTIFICADA Y AUTORIZADA POR LA SUPERINTENDENCIA FINANCIERA DE COLOMBIA.";
            PdfPCell celLeyenda = new PdfPCell(new Phrase(Leyenda, Fuentes.SimexLeyenda));
            celLeyenda.Border = 0;
            celLeyenda.Colspan = 5;
            celLeyenda.Padding = 5;
            tbPiePagina.AddCell(celLeyenda);
            //--------- Doble linea -------------------
            Simex.DobleLinea(ref tbPiePagina, 6);
            //------------- Leyenda 2 -----------------------
            Paragraph prgLey1 = new Paragraph("IVA RÉGIMEN COMÚN - AGENTE RETENEDOR DE IVA", Fuentes.SimexDatos);
            Paragraph prgLey2 = new Paragraph("SOMOS AUTO-RETENEDORES SEGUN RESOLUCIÓN 979 DE JULIO 29/87", Fuentes.SimexDatos);
            Paragraph prgLey3 = new Paragraph("GRANDES CONTRIBUYENTES RESOLUCIÓN 000076 DE DICIEMBRE 01/2016", Fuentes.SimexDatos);
            PdfPCell celLey1 = new PdfPCell();
            celLey1.Border = 0;
            celLey1.Colspan = 4;
            celLey1.AddElement(prgLey1);
            celLey1.AddElement(prgLey2);
            celLey1.AddElement(prgLey3);
            tbPiePagina.AddCell(celLey1);
            //--------------- Leyenda 3 ---------------------------
            Paragraph prgLey4 = new Paragraph(string.Format("CUFE: {0}", CUFE), Fuentes.SimexDatos);
            //Paragraph prgLey5 = new Paragraph(string.Format("Pagina {0}", ""), Fuentes.SimexDatos);
            //Paragraph prgLey6 = new Paragraph(string.Format("Page"), Fuentes.SimexTituloIng);
            PdfPCell celLey2 = new PdfPCell();
            celLey2.Border = 0;
            celLey2.AddElement(prgLey4);
            //celLey2.AddElement(prgLey5);
            //celLey2.AddElement(prgLey6);
            tbPiePagina.AddCell(celLey2);

            return tbPiePagina;
        }
    }
}
