﻿namespace RulesServicesDIAN2.Adquiriente
{
    using iTextSharp.text;
    using iTextSharp.text.pdf;
    using System;
    //Referenciar y usar.
    using System.Data;
    using System.Drawing;
    using System.IO;
    using System.Net;
    using System.Net.NetworkInformation;
    using System.Net.Sockets;
    using System.Web;
    using System.Windows.Forms;
    using ReportesSimex;

    public partial class pdfEstandarAR
    {
        public bool FacturaNacionalSimex(string NIT, string NombreInvoice, ref string RutaPdf, DataSet DsInvoiceAR, System.Drawing.Image QRInvoice, string InvoiceType, string CUFE)
        {
            bool sucess = false;
            NomArchivo = string.Empty;
            string Ruta = $@"{AppDomain.CurrentDomain.BaseDirectory}\PDF\900665411\";
            string LocalIP =  ReportesSimex.Helpers.Formatos.GetLocalIPv4(NetworkInterfaceType.Ethernet);
            NomArchivo = NombreInvoice + ".pdf";
            try
            {
                //Validamos Existencia del Directorio.
                if (!System.IO.Directory.Exists(Ruta))
                    System.IO.Directory.CreateDirectory(Ruta);

                //Valido la existencia previa de este archivo.
                if (System.IO.File.Exists(Ruta + NomArchivo))
                    System.IO.File.Delete(Ruta + NomArchivo);

                //Dimenciones del documento.

                Document document = new Document(iTextSharp.text.PageSize.LETTER);

                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Ruta + NomArchivo, FileMode.Create));
                Paragraph separator = new Paragraph("\n");
                separator.Alignment = Element.ALIGN_CENTER;
                writer.PageEvent = new ReportesSimex.Helpers.Simex.EventPageSimex(ReportesSimex.Plastinovo.Nacional.PiePagina(DsInvoiceAR, CUFE));
                // step 3: we open the document     
                document.Open();

                document.Add(ReportesSimex.Plastinovo.Nacional.Encabezado(DsInvoiceAR, QRInvoice));
                document.Add(ReportesSimex.Plastinovo.Nacional.DatosCliente(DsInvoiceAR));
                document.Add(ReportesSimex.Plastinovo.Nacional.Detalles(DsInvoiceAR));
                document.Add(ReportesSimex.Plastinovo.Nacional.Mercaderia(DsInvoiceAR));
                //document.Add(Simex.Exportacion.PiePagina(DsInvoiceAR, CUFE));
                /*PIE DE PAGINA*/
                PdfContentByte pCb = writer.DirectContent;
                ReportesSimex.Helpers.Formatos.PieDePagina(ref pCb);

                writer.Flush();
                document.Close();

                ReportesSimex.Helpers.Simex.AddPageNumberPagToSimexExp($"{Ruta}{NomArchivo}", dimxLeonisa, dimYLeonisa + 10, ReportesSimex.Helpers.Fuentes.SimexLeyenda);
                sucess = true;
            }
            catch (Exception ex)
            {
                strError += "1. Error al crear PDF: " + ex.Message;
                sucess = false;
            }
            return sucess;
        }
    }
}
