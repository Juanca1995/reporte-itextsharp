﻿namespace ReportesSimex.Helpers
{
    using iTextSharp.text.pdf;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.NetworkInformation;
    using System.Net.Sockets;
    using System.Text;
    using System.Threading.Tasks;

    public class Formatos
    {
        public static string GetLocalIPv4(NetworkInterfaceType _type)
        {
            string output = "";
            foreach (NetworkInterface item in NetworkInterface.GetAllNetworkInterfaces())
            {
                if (item.NetworkInterfaceType == _type && item.OperationalStatus == OperationalStatus.Up)
                {
                    foreach (UnicastIPAddressInformation ip in item.GetIPProperties().UnicastAddresses)
                    {
                        if (ip.Address.AddressFamily == AddressFamily.InterNetwork)
                        {
                            output = ip.Address.ToString();
                        }
                    }
                }
            }
            return output;
        }
        public static void PieDePagina(ref PdfContentByte Pcb)
        {
            try
            {
                //Pie de pagina  iTextSharp.text.pdf.ExtendedColor.LIGHT_GRAY;
                Pcb.BeginText();
                Pcb.SetFontAndSize(BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.WINANSI, false), 8);
                Pcb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "©Copy right " + DateTime.Now.Year + ". Documento creado por bythewave.co sujeto a condiciones y restricciones", 300, 15, 0);
                Pcb.EndText();
            }
            catch (Exception)
            {
            }
        }
    }
}
