﻿
namespace ReportesSimex.Helpers
{
    using iTextSharp.text;
    using iTextSharp.text.pdf;
    using System;
    using Helpers;
    using System.Data;
    using System.IO;

    public class Simex
    {
        public static void CeldaBlanco(ref PdfPTable table, int colspan)
        {
            PdfPCell celBlanco = new PdfPCell(new Phrase(" ", FontFactory.GetFont("Arial", 3f, iTextSharp.text.Font.BOLD)));
            celBlanco.Colspan = colspan;
            celBlanco.Border = 0;
            table.AddCell(celBlanco);
        }
        public static void AgregarDataMercaderia(ref PdfPTable table, string tituloEsp, string tituloIng, string data)
        {
            DetalleTitulos(ref table, tituloEsp, tituloIng);
            PdfPCell celda = new PdfPCell(new Phrase(data, Fuentes.SimexDatos));
            celda.Border = 0;
            celda.HorizontalAlignment = Element.ALIGN_LEFT;
            celda.VerticalAlignment = Element.ALIGN_MIDDLE;
            table.AddCell(celda);
        }
        public static void DetalleTitulos(ref PdfPTable table, string tituloEsp, string tituloIng)
        {
            Paragraph prgTituloEsp = new Paragraph(tituloEsp, Fuentes.SimexTituloEsp);
            Paragraph prgTituloIng = new Paragraph(tituloIng, Fuentes.SimexTituloIng);
            PdfPCell celTitulo = new PdfPCell();
            celTitulo.Border = 0;
            celTitulo.AddElement(prgTituloEsp);
            celTitulo.AddElement(prgTituloIng);
            table.AddCell(celTitulo);
        }
        public static void Titulos2(ref PdfPTable table, string tituloEsp, string tituloIng, int colspan)
        {
            Paragraph prgTituloEsp = new Paragraph(tituloEsp, Fuentes.SimexDatos);
            Paragraph prgTituloIng = new Paragraph(tituloIng, Fuentes.SimexTituloIng);
            PdfPCell celTitulo = new PdfPCell();
            celTitulo.Border = 0;
            celTitulo.AddElement(prgTituloEsp);
            celTitulo.AddElement(prgTituloIng);
            celTitulo.Colspan = colspan;
            table.AddCell(celTitulo);
        }
        public static void DetalleValores(ref PdfPTable table, DataSet ds)
        {
            string PONum = ds.Tables["InvcHead"].Rows[0]["PONum"].ToString();
            foreach (DataRow item in ds.Tables["InvcDtl"].Rows)
            {
                //---- Codigo del cliente ----
                PdfPCell celCodigo = new PdfPCell(new Phrase(item["PartNum"].ToString(), Fuentes.SimexDatos));
                celCodigo.Border = 0;
                celCodigo.HorizontalAlignment = Element.ALIGN_RIGHT;
                table.AddCell(celCodigo);
                //------ Descripcion -----
                PdfPCell celDescripcion = new PdfPCell(new Phrase(item["LineDesc"].ToString(), Fuentes.SimexDatos));
                celDescripcion.HorizontalAlignment = Element.ALIGN_LEFT;
                celDescripcion.Border = 0;
                table.AddCell(celDescripcion);
                //------ Orden de compra ------
                PdfPCell celOrdenCompra = new PdfPCell(new Phrase(PONum, Fuentes.SimexDatos));
                celOrdenCompra.HorizontalAlignment = Element.ALIGN_LEFT;
                celOrdenCompra.Border = 0;
                table.AddCell(celOrdenCompra);
                //------ referencia cliente ------
                PdfPCell celReferencia = new PdfPCell(new Phrase(item["XPartNum"].ToString(), Fuentes.SimexDatos));
                celReferencia.HorizontalAlignment = Element.ALIGN_CENTER;
                celReferencia.Border = 0;
                table.AddCell(celReferencia);
                //---- Unidad de medida, Lote
                Paragraph prgUN = new Paragraph(item["IUM"].ToString(), Fuentes.SimexDatos);
                Paragraph prgLoteEsp = new Paragraph("Lote", Fuentes.SimexTituloEsp);
                Paragraph prgLoteIng = new Paragraph("Batch", Fuentes.SimexTituloIng);
                PdfPCell celUN = new PdfPCell();
                celUN.Border = 0;
                celUN.AddElement(prgUN);
                celUN.AddElement(prgLoteEsp);
                celUN.AddElement(prgLoteIng);
                table.AddCell(celUN);
                //-------- cantidad, info lote ---------
                //Paragraph prgCant = new Paragraph(item["SellingShipQty"].ToString(), Fuentes.SimexDatos);
                string cantidad = string.Empty;
                ConvertirFormatoDecimal(item["SellingShipQty"], out cantidad);
                Paragraph prgCant = new Paragraph(cantidad, Fuentes.SimexDatos);
                Paragraph prgLote = new Paragraph(item["LotNum"].ToString(), Fuentes.SimexDatos);
                PdfPCell celCant = new PdfPCell();
                celCant.Border = 0;
                celCant.AddElement(prgCant);
                celCant.AddElement(prgLote);
                table.AddCell(celCant);
                //--------- Valor unitario ---------
                string ValorUnitario = string.Empty;
                ConvertirFormatoDecimal(item["DocUnitPrice"], out ValorUnitario);
                PdfPCell celValorUnit = new PdfPCell(new Phrase(ValorUnitario, Fuentes.SimexDatos));
                celValorUnit.HorizontalAlignment = Element.ALIGN_LEFT;
                celValorUnit.Border = 0;
                table.AddCell(celValorUnit);
                //------- Total USD ----------------
                string TotalUSD = string.Empty;
                ConvertirFormatoDecimal(item["DocExtPrice"], out TotalUSD);
                PdfPCell celTotalUSD = new PdfPCell(new Phrase(TotalUSD, Fuentes.SimexDatos));
                celTotalUSD.HorizontalAlignment = Element.ALIGN_LEFT;
                celTotalUSD.Border = 0;
                table.AddCell(celTotalUSD);
            }
        }
        public static void ConvertirFormatoDecimal(object data, out string formato)
        {
            decimal result = 0;
            formato = string.Empty;
            try
            {
                result = Convert.ToDecimal(data);
                //formato = string.Format("{0:0.00}", result);
                formato = string.Format("{0:N2}", result);
            }
            catch
            {
                formato = data.ToString();
            }
        }
        public static void DobleLinea(ref PdfPTable table, int colspan)
        {
            PdfPCell celDobleLinea = new PdfPCell(new Phrase(" ", FontFactory.GetFont("Arial", 1.5f, iTextSharp.text.Font.NORMAL)));
            celDobleLinea.Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER;
            celDobleLinea.Colspan = colspan;
            celDobleLinea.Padding = 0;
            table.AddCell(celDobleLinea);
        }
        public static void AgregarDatosCliente(ref PdfPTable table, string tituloEsp, string tituloIng, object data)
        {
            Paragraph prgTituloEsp = new Paragraph(tituloEsp, Fuentes.SimexTituloEsp);
            Paragraph prgTituloIng = new Paragraph(tituloIng, Fuentes.SimexTituloIng);
            PdfPCell celTitulo = new PdfPCell();
            celTitulo.Border = 0;
            celTitulo.AddElement(prgTituloEsp);
            celTitulo.AddElement(prgTituloIng);
            table.AddCell(celTitulo);

            PdfPCell celDato = new PdfPCell(new Phrase(string.Format("{0}", data), Fuentes.SimexDatos));
            celDato.Border = 0;
            celDato.VerticalAlignment = Element.ALIGN_MIDDLE;
            celDato.HorizontalAlignment = Element.ALIGN_LEFT;
            table.AddCell(celDato);
        }
        public static void AddPageNumberPagToSimexExp(string rutaPDF, float posicionX, float posicionY, iTextSharp.text.Font font)
        {
            byte[] bytes = File.ReadAllBytes(rutaPDF);

            //iTextSharp.text.Font blackFont = FontFactory.GetFont("Arial", 12, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
            using (MemoryStream stream = new MemoryStream())
            {
                PdfReader reader = new PdfReader(bytes);
                using (PdfStamper stamper = new PdfStamper(reader, stream))
                {
                    int pages = reader.NumberOfPages;
                    for (int i = 1; i <= pages; i++)
                    {
                        ColumnText.ShowTextAligned(
                            stamper.GetUnderContent(i), Element.ALIGN_CENTER, new Phrase($"Pagina: {i.ToString()} de {pages}\r\nPage", font), posicionX, posicionY, 0);
                    }
                }
                bytes = stream.ToArray();
            }
            File.WriteAllBytes(rutaPDF, bytes);
        }
        public class EventPageSimex : PdfPageEventHelper
        {
            PdfContentByte cb;

            // we will put the final number of pages in a template
            PdfTemplate headerTemplate, footerTemplate;

            // this is the BaseFont we are going to use for the header / footer
            BaseFont bf = null;

            // This keeps track of the creation time
            DateTime PrintTime = DateTime.Now;

            private string _header;

            public string Header
            {
                get { return _header; }
                set { _header = value; }
            }
            PdfPTable _PiePagina;
            public PdfPTable PiePagina
            {
                get
                {
                    return _PiePagina;
                }
                set
                {
                    _PiePagina = value;
                }
            }

            public EventPageSimex(PdfPTable table)
            {
                _PiePagina = table;
            }

            public override void OnOpenDocument(PdfWriter writer, Document document)
            {
                try
                {
                    PrintTime = DateTime.Now;
                    bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                    cb = writer.DirectContent;
                    headerTemplate = cb.CreateTemplate(100, 100);
                    footerTemplate = cb.CreateTemplate(50, 50);
                }
                catch (DocumentException de)
                {
                }
                catch (System.IO.IOException ioe)
                {
                }
            }

            public override void OnEndPage(iTextSharp.text.pdf.PdfWriter writer, iTextSharp.text.Document document)
            {
                base.OnEndPage(writer, document);

                //call WriteSelectedRows of PdfTable. This writes rows from PdfWriter in PdfTable
                //first param is start row. -1 indicates there is no end row and all the rows to be included to write
                //Third and fourth param is x and y position to start writing


                document.Add(_PiePagina);

                //tableEncabezado.TotalWidth = document.PageSize.Width - 35f;
                //tableEncabezado.WriteSelectedRows(0, -1, 20, document.PageSize.Height - 40, writer.DirectContentUnder);

                //tableUnidades.TotalWidth = document.PageSize.Width - 80f;
                //tableUnidades.WriteSelectedRows(0, -1, 40, document.PageSize.Height - 40, writer.DirectContentUnder);
                //pdfTab.WriteSelectedRows(0, -1, 40, document.PageSize.Height - 30, writer.DirectContent);

                //cb.MoveTo(40, document.PageSize.Height - 100);
                //cb.LineTo(document.PageSize.Width - 40, document.PageSize.Height - 100);
                //cb.Stroke();
            }

            public override void OnCloseDocument(PdfWriter writer, Document document)
            {
                base.OnCloseDocument(writer, document);

                headerTemplate.BeginText();
                headerTemplate.SetFontAndSize(bf, 12);
                headerTemplate.SetTextMatrix(0, 0);
                headerTemplate.ShowText((writer.PageNumber - 1).ToString());
                headerTemplate.EndText();
            }
        }
    }
}
