﻿namespace ReportesSimex.Helpers
{
    using iTextSharp.text;
    public class Fuentes
    {
        public static iTextSharp.text.Font fontTitleBold
        {
            get
            {
                return FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 7.57f, iTextSharp.text.Font.NORMAL);
            }
        }
        public static iTextSharp.text.Font fontTitle
        {
            get
            {
                return FontFactory.GetFont(FontFactory.HELVETICA, 7.57f, iTextSharp.text.Font.NORMAL);
            }
        }
        public static iTextSharp.text.Font fontTitleItalic
        {
            get
            {
                return FontFactory.GetFont(FontFactory.HELVETICA, 7.57f, iTextSharp.text.Font.ITALIC | iTextSharp.text.Font.BOLD);
            }
        }
        public static iTextSharp.text.Font fontTitleBold2
        {
            get
            {
                return FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 8f, iTextSharp.text.Font.NORMAL);
            }
        }
        public static iTextSharp.text.Font fontTitle2
        {
            get
            {
                return FontFactory.GetFont(FontFactory.HELVETICA, 8f, iTextSharp.text.Font.NORMAL);
            }
        }
        public static iTextSharp.text.Font fontCustom
        {
            get
            {
                return FontFactory.GetFont(FontFactory.HELVETICA, 6f, iTextSharp.text.Font.NORMAL);
            }
        }
        public static iTextSharp.text.Font fontCustomI
        {
            get
            {
                return FontFactory.GetFont(FontFactory.HELVETICA_OBLIQUE, 6f, iTextSharp.text.Font.NORMAL);
            }
        }
        public static iTextSharp.text.Font fontCustomBold
        {
            get
            {
                return FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 6, iTextSharp.text.Font.NORMAL);
            }
        }
        public static iTextSharp.text.Font fontTitleLittleBold
        {
            get
            {
                return FontFactory.GetFont(FontFactory.HELVETICA, 6f, iTextSharp.text.Font.BOLD | iTextSharp.text.Font.ITALIC);
            }
        }
        public static iTextSharp.text.Font SimexTituloEsp
        {
            get
            {
                return FontFactory.GetFont("Arial", 7f, iTextSharp.text.Font.BOLD);
            }
        }
        public static iTextSharp.text.Font SimexTituloIng
        {
            get
            {
                return FontFactory.GetFont("Arial", 6f, iTextSharp.text.Font.ITALIC);
            }
        }
        public static iTextSharp.text.Font SimexLeyenda
        {
            get
            {
                return FontFactory.GetFont("Arial", 6f, iTextSharp.text.Font.NORMAL);
            }
        }
        public static iTextSharp.text.Font SimexDatos
        {
            get
            {
                return FontFactory.GetFont("Arial", 7f, iTextSharp.text.Font.NORMAL);
            }
        }
    }
}
