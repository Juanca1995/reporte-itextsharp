﻿namespace ReportesSimex
{
    using iTextSharp.text;
    using iTextSharp.text.pdf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    //using System.Drawing;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Forms;
    using System.Web;
    using System.Xml.Linq;
    using System.Xml;
    using System.Net;
    using System.Drawing;
    using RulesServicesDIAN2.Adquiriente;

    public partial class Form1 : Form
    {
        pdfEstandarAR pdfEstandar = new pdfEstandarAR();

        //string RutaXmlInvoice = AppDomain.CurrentDomain.BaseDirectory.ToString() + "XmlInvoicePruePDF1.xml";
        //string RutaXmlInvoice = AppDomain.CurrentDomain.BaseDirectory.ToString() + "XmlInvoiceNum1617650.xml";
        string RutaXmlInvoice = AppDomain.CurrentDomain.BaseDirectory.ToString() + "XmlSimex.xml";
        XmlDocument XmlInvoice = new XmlDocument();

        public Form1()
        {
            InitializeComponent();
            XmlInvoice.Load(RutaXmlInvoice);
            ds = GenerarDataSet(RutaXmlInvoice);
        }

        private void btnGenerar_Click(object sender, EventArgs e)
        {
            System.Drawing.Image logo = null;
            //Banner
            try
            {
                //var requestBanner = WebRequest.Create($@"C:\Users\key94\source\repos\Reporte_iTextSharp\Reporte_iTextSharp\bin\Debug\imgQR.png");
                var requestBanner = WebRequest.Create($@"{AppDomain.CurrentDomain.BaseDirectory}\imgQR.png");
                using (var responseBanner = requestBanner.GetResponse())
                using (var streamBanner = responseBanner.GetResponseStream())
                {
                    logo = Bitmap.FromStream(streamBanner);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Error al cargar Logo Banner, not foud\n");
            }

            DataTable dt = ds.Tables["InvcHead"];
            var ruta = string.Empty;

            //pdfEstandar.CreatePDF();

            //pdfEstandar.FacturaNacionalLeonisa1("900665411",
            //    "FacturaNacionalLeonisa1", ref ruta, ds, logo, dt.Rows[0]["InvoiceType"].ToString(),"cufe");
            //--pdfEstandar.FacturaExportacionSimex("900665411", "FacturaExportacionSimex", ref ruta, ds, logo, dt.Rows[0]["InvoiceType"].ToString(), "cufe");
            pdfEstandar.FacturaNacionalSimex("900665411", "FacturaNacionalSimex", ref ruta, ds, logo, dt.Rows[0]["InvoiceType"].ToString(), "cufe");
            //pdfEstandar.FacturaNacionalSimex("900665411", "FacturanacionalSimex", ref ruta, ds, logo, dt.Rows[0]["InvoiceType"].ToString(), "cufe");
        }
        private DataSet GenerarDataSet(string xmlfile)
        {
            try
            {
                DataSet dataSet = new DataSet();
                StringReader xmlSR = new StringReader(xmlfile);

                System.IO.FileStream fsReadXml = new System.IO.FileStream
                (xmlfile, System.IO.FileMode.Open);
                ///Cargamos el DataSet..
                dataSet.ReadXml(fsReadXml);
                return dataSet;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Estatus:Error en GenerarDataSet: " + ex.Message + " \n");
                //PeticionWCF.Tracer += "Hora:" + DateTime.Now.ToString() + " \n\n";
                return null;
            }
        }

        DataSet ds = new DataSet();
    }
}
