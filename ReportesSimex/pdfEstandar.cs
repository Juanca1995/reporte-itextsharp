﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RulesServicesDIAN2.Adquiriente
{
    public partial class pdfEstandarAR
    {
        #region "Constructor"

        public pdfEstandarAR()
        {
            NomArchivo = string.Empty;
            strError = string.Empty;
        }

        #endregion
        #region "Atributos"

        private string NomArchivo;
        private string strError;

        #endregion

        #region "Propiedades"

        public string NomArchivoPDF
        {
            get { return NomArchivo; }
        }

        public string Error
        {
            get { return strError; }
        }

        #endregion
        float dimxLeonisa = 540f, dimYLeonisa = 7f;
    }
}
